package pckString;

import android.os.Build;
import android.text.Html;
import android.widget.TextView;

import java.util.ArrayList;

import pckInfo.InfoCollector;
import pckMath.MathUtility;

/**
 * Created by Cong on 5/4/2017.
 */

public class StringUtility {

    private static ArrayList<String> RestrictionData;

    private static void setRestrictionData(){
        RestrictionData = new ArrayList<>();

        RestrictionData.add("*");
        RestrictionData.add("Select");
        RestrictionData.add("Update");
        RestrictionData.add("Delete");
        RestrictionData.add("From");
        RestrictionData.add("Table");
        RestrictionData.add("Order");
        RestrictionData.add("Where");
        RestrictionData.add("Set");
        RestrictionData.add("Get");
        RestrictionData.add("'");
        RestrictionData.add(";");
    }

    public static void writeString(TextView tvTextView, String StringData){
        if (StringData.contains(InfoCollector.getStartNumerator())){
            StringData = StringData.replaceAll(InfoCollector.getStartNumerator(), "<sup>");
            StringData = StringData.replaceAll(InfoCollector.getEndNumerator(), "</sup>");
            StringData = StringData.replaceAll(InfoCollector.getStartDenominator(), "<sub>");
            StringData = StringData.replaceAll(InfoCollector.getEndDenominator(), "</sub>");
        }
        else {
            if (StringData.contains(InfoCollector.getStartExponential())){
                StringData = StringData.replaceAll(InfoCollector.getStartExponential(), "<sup>");
                StringData = StringData.replaceAll(InfoCollector.getEndExponential(), "</sup>");
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvTextView.setText(Html.fromHtml(StringData, 0));
        }
        else {
            tvTextView.setText(Html.fromHtml(StringData));
        }
    }

    public static void writeString(TextView tvTextView, int IntegerData){
        String StringData = String.valueOf(IntegerData);

        if (StringData.contains(InfoCollector.getStartNumerator())){
            StringData = StringData.replaceAll(InfoCollector.getStartNumerator(), "<sup>");
            StringData = StringData.replaceAll(InfoCollector.getEndNumerator(), "</sup>");
            StringData = StringData.replaceAll(InfoCollector.getStartDenominator(), "<sub>");
            StringData = StringData.replaceAll(InfoCollector.getEndDenominator(), "</sub>");
        }
        else {
            if (StringData.contains(InfoCollector.getStartExponential())){
                StringData = StringData.replaceAll(InfoCollector.getStartExponential(), "<sup>");
                StringData = StringData.replaceAll(InfoCollector.getEndExponential(), "</sup>");
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvTextView.setText(Html.fromHtml(StringData, 0));
        }
        else {
            tvTextView.setText(Html.fromHtml(StringData));
        }
    }

    public static String getTimeString(int TimeInMillisecond){
        String TimeString = "";

        int temp = TimeInMillisecond / 1000;
        int Hour = 0;
        int Minute = temp / 60;
        int Second = temp % 60;

        String StHour = "", StMinute = "", StSecond = "";

        if (Minute > 60){
            Hour = Minute / 60;
            Minute = Minute - 60 * Hour;

            StHour = String.valueOf(Hour);
        }

        if (Minute < 10){
            StMinute = "0" + String.valueOf(Minute);
        }
        else {
            StMinute = String.valueOf(Minute);
        }

        if (Second < 10){
            StSecond = "0" + String.valueOf(Second);
        }
        else {
            StSecond = String.valueOf(Second);
        }

        if (Hour > 0){
            TimeString = StHour + ":" + StMinute + ":" + StSecond;
        }
        else {
            TimeString = StMinute + ":" + StSecond;
        }

        return TimeString;
    }

    public static String writeArrayList(ArrayList<Integer> IntegerList){
        String ArrayString = "";

        int size = IntegerList.size();
        for (int i = 0; i < size; i++){
            ArrayString += String.valueOf(IntegerList.get(i)) + ", ";
        }

        ArrayString = ArrayString.trim().substring(0, ArrayString.length() - 1);

        return ArrayString;
    }

    public static boolean isAvailableString(String DataString){
        boolean isAvailable = true;

        setRestrictionData();
        int Size = RestrictionData.size();
        for (int i = 0; i < Size; i++){
            if (DataString.toLowerCase().contains(RestrictionData.get(i).toLowerCase())){
                isAvailable = false;
                break;
            }
        }

        return isAvailable;
    }
}
