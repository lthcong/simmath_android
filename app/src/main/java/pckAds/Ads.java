package pckAds;

import android.app.Activity;
import android.widget.*;
import com.adcolony.sdk.*;
import com.appnext.ads.interstitial.Interstitial;
import com.appnext.base.Appnext;
import com.startapp.android.publish.ads.banner.*;
import com.startapp.android.publish.adsCommon.*;
import com.vungle.publisher.*;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.CBLocation;
import com.chartboost.sdk.ChartboostDelegate;
import com.chartboost.sdk.Libraries.CBLogging.Level;
import com.chartboost.sdk.Model.CBError.CBClickError;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.chartboost.sdk.Tracking.CBAnalytics;
import com.chartboost.sdk.CBImpressionActivity;

import pckData.pckFirebase.LogEvents;
import pckMath.MathUtility;

/**
 * Created by Cong on 5/7/2017.
 */

public class Ads {

    //  ADCOLONY
    private static AdColonyInterstitial ADCOLONY_ADS;
    private static boolean IS_ADCOLONY_READY = true;
    private static String ADCOLONY_APP_ID = "app32b2dcf8b47d44ebb1";
    private static String ADCOLONY_ZONE_1 = "vz80cbc05bc63b4ecb9b";
    private static String ADCOLONY_ZONE_2 = "vzb188f4e1dc9b47fb8b";
    private static String ADCOLONY_ZONE_3 = "vz9ca1fc0424ed4d8f94";
    private static String ADCOLONY_ZONE_4 = "vz10f3adde999b4fa485";
    private static String ADCOLONY_ZONE_5 = "vz448e99d9533b41ec89";
    private static String ADCOLONY_ZONE_6 = "vz09894dc38411441486";
    private static String ADCOLONY_ZONE_7 = "vzcc82e1a4916e49f998";
    private static String ADCOLONY_ZONE_8 = "vzb49344bc8a0a463286";
    private static String ADCOLONY_ZONE_9 = "vz5b2c335a2ce44f7e84";
    private static String ADCOLONY_ZONE_10 = "vz7b87e45ad3104743b7";
    private static String ADCOLONY_ZONE_11 = "vza5fb1467d6974f028e";
    private static String ADCOLONY_ZONE_12 = "vz78a1a7d7f1b84ba4ab";
    private static String ADCOLONY_ZONE_13 = "vzaadb3a8c670a4677af";
    private static String ADCOLONY_ZONE_14 = "vz8901ff146bf8442d8c";
    private static String ADCOLONY_ZONE_15 = "vzae86ea5176894a1ba2";

    //  VUNGLE
    private static boolean IS_VUNGLE_READY = true;
    private static final VunglePub VUNGLE_ADS = VunglePub.getInstance();
    private static final AdConfig VUNGLE_GLOBAL_ADS_CONFIG = VUNGLE_ADS.getGlobalAdConfig();
    private static String VUNGLE_APP_ID = "590eb65e085bcd9d5d000253";
    private static String PLACEMENT_01 = "MYMATHA79450";
    private static String PLACEMENT_02 = "SECONDP88455";
    private static String PLACEMENT_03 = "THIRDPL73619";
    private static String PLACEMENT_04 = "FORTHPL18631";
    private static String PLACEMENT_05 = "PLACEME99503";
    private static String PLACEMENT_06 = "PLACEME34289";
    private static String PLACEMENT_07 = "PLACEME49226";
    private static String PLACEMENT_08 = "PLACEME16606";
    private static String PLACEMENT_09 = "PLACEME24403";
    private static String PLACEMENT_10 = "PLACEME24964";

    //  STARTAPP
    private static String STARTAPP_ID = "206373662";

    //  APPNEXT
    private static String APPNEXT_PLACEMENT_ID = "eceab4bb-cf9d-4bcd-83c9-c04b74d48621";
    private static Interstitial APPNEXT_ADS;

    //  CHARTBOOST
    private static String CHARTBOOST_APP_ID = "5b5a4281449e6d0c1a474215";
    private static String CHARTBOOST_APP_SIGNATURE = "e9a17e1f2f9a95d3c8daf45d843a6535f73c7ecc";

    //  ADS STEP
    public static int ADS_STEP = 5;
    public static int WRAPPED_ADS_STEP = 2;

    //  SET UP
    private static void setupAdColonyAds(Activity CurrentActivity){
        try {
            //  ADCOLONY
            AdColony.configure(CurrentActivity,
                    ADCOLONY_APP_ID,
                    ADCOLONY_ZONE_1,
                    ADCOLONY_ZONE_2,
                    ADCOLONY_ZONE_3,
                    ADCOLONY_ZONE_4,
                    ADCOLONY_ZONE_5,
                    ADCOLONY_ZONE_6,
                    ADCOLONY_ZONE_7,
                    ADCOLONY_ZONE_8,
                    ADCOLONY_ZONE_9,
                    ADCOLONY_ZONE_10,
                    ADCOLONY_ZONE_11,
                    ADCOLONY_ZONE_12,
                    ADCOLONY_ZONE_13,
                    ADCOLONY_ZONE_14,
                    ADCOLONY_ZONE_15);

            IS_ADCOLONY_READY = true;
        }
        catch(Exception e){
            IS_ADCOLONY_READY = false;
        }
    }

    private static void setupVungleAds(Activity CurrentActivity){
        try{
            VUNGLE_ADS.init(CurrentActivity,
                    VUNGLE_APP_ID,
                    new String[] {
                            PLACEMENT_01,
                            PLACEMENT_02,
                            PLACEMENT_03,
                            PLACEMENT_04,
                            PLACEMENT_05,
                            PLACEMENT_06,
                            PLACEMENT_07,
                            PLACEMENT_08,
                            PLACEMENT_09,
                            PLACEMENT_10},
                    new VungleInitListener() {

                        @Override
                        public void onSuccess() {
                        }
                        @Override
                        public void onFailure(Throwable e){

                        }
                    });

            IS_VUNGLE_READY = true;
        }
        catch (Exception e){
            IS_VUNGLE_READY = false;
        }
    }

    private static void setupStartApp(Activity CurrentActivity){
        try {
            StartAppSDK.init(CurrentActivity, STARTAPP_ID, true);
            StartAppAd.disableSplash();
        }
        catch (Exception e){}
    }

    private static void setupAppNext(Activity CurrentActivity){
        try {
            Appnext.init(CurrentActivity);
        }
        catch (Exception e){}
    }

    private static void setupChartboost(Activity CurrentActivity){
        try {
            Chartboost.startWithAppId(CurrentActivity, CHARTBOOST_APP_ID, CHARTBOOST_APP_SIGNATURE);
            Chartboost.onCreate(CurrentActivity);
        }
        catch (Exception e){}
    }

    public static void setupAds(Activity CurrentActivity){
        setupAdColonyAds(CurrentActivity);
        setupVungleAds(CurrentActivity);
        setupStartApp(CurrentActivity);
        setupAppNext(CurrentActivity);
        setupChartboost(CurrentActivity);
    }

    private static AdColonyInterstitialListener getInterstitialListener(){
        return new AdColonyInterstitialListener() {
            @Override
            public void onRequestFilled(AdColonyInterstitial ad) {
                ADCOLONY_ADS = ad;
            }
        };
    }

    //  SHOW ADS
    private static boolean showAdColonyAds(Activity CurrentActivity){
        boolean isShowed = true;

        try {
            AdColony.requestInterstitial(ADCOLONY_ZONE_1, getInterstitialListener());
            if (!ADCOLONY_ADS.show()) {
                AdColony.requestInterstitial(ADCOLONY_ZONE_2, getInterstitialListener());
                if (!ADCOLONY_ADS.show()) {
                    AdColony.requestInterstitial(ADCOLONY_ZONE_3, getInterstitialListener());
                    if (!ADCOLONY_ADS.show()) {
                        AdColony.requestInterstitial(ADCOLONY_ZONE_4, getInterstitialListener());
                        if (!ADCOLONY_ADS.show()) {
                            AdColony.requestInterstitial(ADCOLONY_ZONE_5, getInterstitialListener());
                            if (!ADCOLONY_ADS.show()) {
                                AdColony.requestInterstitial(ADCOLONY_ZONE_6, getInterstitialListener());
                                if (!ADCOLONY_ADS.show()) {
                                    AdColony.requestInterstitial(ADCOLONY_ZONE_7, getInterstitialListener());
                                    if (!ADCOLONY_ADS.show()) {
                                        AdColony.requestInterstitial(ADCOLONY_ZONE_8, getInterstitialListener());
                                        if (!ADCOLONY_ADS.show()) {
                                            AdColony.requestInterstitial(ADCOLONY_ZONE_9, getInterstitialListener());
                                            if (!ADCOLONY_ADS.show()) {
                                                AdColony.requestInterstitial(ADCOLONY_ZONE_10, getInterstitialListener());
                                                if (!ADCOLONY_ADS.show()) {
                                                    AdColony.requestInterstitial(ADCOLONY_ZONE_11, getInterstitialListener());
                                                    if (!ADCOLONY_ADS.show()) {
                                                        AdColony.requestInterstitial(ADCOLONY_ZONE_12, getInterstitialListener());
                                                        if (!ADCOLONY_ADS.show()) {
                                                            AdColony.requestInterstitial(ADCOLONY_ZONE_13, getInterstitialListener());
                                                            if (!ADCOLONY_ADS.show()) {
                                                                AdColony.requestInterstitial(ADCOLONY_ZONE_14, getInterstitialListener());
                                                                if (!ADCOLONY_ADS.show()) {
                                                                    AdColony.requestInterstitial(ADCOLONY_ZONE_15, getInterstitialListener());
                                                                    if (!ADCOLONY_ADS.show()) {
                                                                        isShowed = false;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e){
            isShowed = false;
            setupAdColonyAds(CurrentActivity);
        }

        return isShowed;
    }

    private static boolean showVungleAds(Activity CurrentActivity){
        boolean isShowed = false;

        try {
            VUNGLE_ADS.loadAd(PLACEMENT_01);
            if (VUNGLE_ADS.isAdPlayable(PLACEMENT_01)) {
                VUNGLE_ADS.playAd(PLACEMENT_01, VUNGLE_GLOBAL_ADS_CONFIG);

                isShowed = true;
            }
            else {
                VUNGLE_ADS.loadAd(PLACEMENT_02);
                if (VUNGLE_ADS.isAdPlayable(PLACEMENT_02)) {
                    VUNGLE_ADS.playAd(PLACEMENT_02, VUNGLE_GLOBAL_ADS_CONFIG);

                    isShowed = true;
                }
                else {
                    VUNGLE_ADS.loadAd(PLACEMENT_03);
                    if (VUNGLE_ADS.isAdPlayable(PLACEMENT_03)) {
                        VUNGLE_ADS.playAd(PLACEMENT_03, VUNGLE_GLOBAL_ADS_CONFIG);

                        isShowed = true;
                    }
                    else {
                        VUNGLE_ADS.loadAd(PLACEMENT_04);
                        if (VUNGLE_ADS.isAdPlayable(PLACEMENT_04)) {
                            VUNGLE_ADS.playAd(PLACEMENT_04, VUNGLE_GLOBAL_ADS_CONFIG);

                            isShowed = true;
                        }
                        else {
                            VUNGLE_ADS.loadAd(PLACEMENT_05);
                            if (VUNGLE_ADS.isAdPlayable(PLACEMENT_05)) {
                                VUNGLE_ADS.playAd(PLACEMENT_05, VUNGLE_GLOBAL_ADS_CONFIG);

                                isShowed = true;
                            }
                            else {
                                VUNGLE_ADS.loadAd(PLACEMENT_06);
                                if (VUNGLE_ADS.isAdPlayable(PLACEMENT_06)) {
                                    VUNGLE_ADS.playAd(PLACEMENT_06, VUNGLE_GLOBAL_ADS_CONFIG);

                                    isShowed = true;
                                }
                                else {
                                    VUNGLE_ADS.loadAd(PLACEMENT_07);
                                    if (VUNGLE_ADS.isAdPlayable(PLACEMENT_07)) {
                                        VUNGLE_ADS.playAd(PLACEMENT_07, VUNGLE_GLOBAL_ADS_CONFIG);

                                        isShowed = true;
                                    }
                                    else {
                                        VUNGLE_ADS.loadAd(PLACEMENT_08);
                                        if (VUNGLE_ADS.isAdPlayable(PLACEMENT_08)) {
                                            VUNGLE_ADS.playAd(PLACEMENT_08, VUNGLE_GLOBAL_ADS_CONFIG);

                                            isShowed = true;
                                        }
                                        else {
                                            VUNGLE_ADS.loadAd(PLACEMENT_09);
                                            if (VUNGLE_ADS.isAdPlayable(PLACEMENT_09)) {
                                                VUNGLE_ADS.playAd(PLACEMENT_09, VUNGLE_GLOBAL_ADS_CONFIG);

                                                isShowed = true;
                                            }
                                            else {
                                                VUNGLE_ADS.loadAd(PLACEMENT_10);
                                                if (VUNGLE_ADS.isAdPlayable(PLACEMENT_10)) {
                                                    VUNGLE_ADS.playAd(PLACEMENT_10, VUNGLE_GLOBAL_ADS_CONFIG);

                                                    isShowed = true;
                                                }
                                                else {
                                                    isShowed = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e){
            isShowed = false;
            setupVungleAds(CurrentActivity);
        }

        return isShowed;
    }

    private static boolean showStartAppAds(Activity CurrentActivity){
        boolean isShowed = false;

        try {
            isShowed = StartAppAd.showAd(CurrentActivity);
        }
        catch (Exception e){
            isShowed = false;
            setupStartApp(CurrentActivity);
        }

        return isShowed;
    }

    private static boolean showAppNextAds(Activity CurrentActivity){
        boolean isShowed = false;

        try {
            APPNEXT_ADS = new Interstitial(CurrentActivity, APPNEXT_PLACEMENT_ID);
            APPNEXT_ADS.loadAd();
            if (APPNEXT_ADS.isAdLoaded()) {
                isShowed = true;
                APPNEXT_ADS.showAd();
            }
        }
        catch (Exception e){
            isShowed = false;
            setupAppNext(CurrentActivity);
        }

        return isShowed;
    }

    private static boolean showChartboostAds(Activity CurrentActivity){
        boolean isShowed = false;

        try {
            Chartboost.cacheInterstitial(CBLocation.LOCATION_DEFAULT);
            if (Chartboost.hasInterstitial(CBLocation.LOCATION_DEFAULT)) {
                Chartboost.showInterstitial(CBLocation.LOCATION_DEFAULT);
                isShowed = true;
            }
        }
        catch (Exception e){
            isShowed = false;
            setupChartboost(CurrentActivity);
        }

        return isShowed;
    }

    public static void showFullScreenAds(Activity CurrentActivity){
        if (!showAppNextAds(CurrentActivity)){
            if (!showStartAppAds(CurrentActivity)){
                showChartboostAds(CurrentActivity);
            }
        }
    }

    public static void showWrappedAds(Activity CurrentActivity, LinearLayout lnlyContainer){
        Mrec StartAppMrec = new Mrec(CurrentActivity);
        RelativeLayout.LayoutParams StartAppMrecParameters = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);

        StartAppMrecParameters.addRule(RelativeLayout.CENTER_HORIZONTAL);
        StartAppMrecParameters.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        // Add to main Layout
        lnlyContainer.addView(StartAppMrec, StartAppMrecParameters);
    }

    public static boolean showVideoAds(Activity CurrentActivity){
        boolean isShowed;

        try {
            if (MathUtility.getRandomBooleanValue()){
                isShowed = showAdColonyAds(CurrentActivity);
            }
            else {
                isShowed = showVungleAds(CurrentActivity);
            }
        }
        catch (Exception e){
            isShowed = false;
            setupAdColonyAds(CurrentActivity);
            setupVungleAds(CurrentActivity);
        }

        return isShowed;
    }

    public static void showAds(Activity CurrentActivity){
        if(!showVideoAds(CurrentActivity)){
            showFullScreenAds(CurrentActivity);
        }
    }

    //  SHOW ADS RANDOMLY
    public static void showAds(Activity CurrentActivity, boolean isReady){
        if (isReady){
            showAds(CurrentActivity);
        }
    }

    //  SHOW ADS WITH ADS STEP
    public static void showAds(Activity CurrentActivity, int NUMBER){
        if (NUMBER % ADS_STEP == 0){
            showAds(CurrentActivity);
        }
    }

}
