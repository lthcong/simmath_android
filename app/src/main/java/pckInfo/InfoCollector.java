package pckInfo;

import android.app.Activity;
import android.database.Cursor;
import android.support.v4.app.Fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import adsfree.congla.android.cong.mymathapp.ComeBackFragment;
import adsfree.congla.android.cong.mymathapp.R;
import layout.BlankFragment;
import pckData.pckLocalData.DatabaseFile;
import pckMath.MathUtility;
import pckMath.pckMathTest.MathTest;
import pckMath.pckMathTest.SATTest;
import pckMath.pckMathTopics.RoundCollection;
import pckMath.pckMathTopics.Topic;
import pckMath.pckMathTopics.TopicCollection;

/**
 * Created by Cong on 4/24/2017.
 */

public class InfoCollector {

    private static String TEXT_COLOR = "#0066ff";
    private static String CHECK_MARK = "\u2713";
    private static String DIVISION_SIGN = " \u00F7 ";
    private static String SQUARE_ROOT_SIGN = "\u221A";
    private static String NOT_EQUAL_TO_SIGN = " \u2260 ";
    private static String LESS_THAN_OR_EQUAL_SIGN = " \u2264 ";
    private static String GREATER_THAN_OR_EQUAL_SIGN = " \u2265 ";
    private static String ANGLE_SIGN = "\u2220";
    private static String TRIANGLE_SIGN = "\u25B3";
    private static String MINUS_AND_PLUS_SIGN = "\u2213";
    private static String PLUS_AND_MINUS_SIGN = "\u00B1";
    private static String EMPTY_SET = "\u2205";
    private static String ELEMENT_OF = "\u2208";
    private static String NOT_AN_ELEMENT_OF = "\u2209";
    private static String INTERSECTION_SET = "\u2229";
    private static String UNION_SET = "\u222A";
    private static String PROPER_SET = "\u2282";
    private static String SUB_SET = "\u2286";
    private static String NOT_SUB_SET = "\u2288";
    private static String DIFFERENT_OF_SET = "\u2572";
    private static String INFINITY = "\u221E";
    private static String ONE_HALF = "\u00BD";
    private static String ONE_THIRD = "\u2153";
    private static String ONE_FORTH = "\u00BC";
    private static String ONE_FIFTH = "\u2155";
    private static String START_NUMERATOR = "<NUM>";
    private static String END_NUMERATOR = "</NUM>";
    private static String START_DENOMINATOR = "<DEN>";
    private static String END_DENOMINATOR = "</DEN>";
    private static String START_EXPONENTIAL = "<EXP>";
    private static String END_EXPONENTIAL = "</EXP>";
    private static String NO_SOLUTION = "No Solution.";
    private static String INFINITE_MANY_SOLUTION = "Infinite many solutions.";
    private static String PI_SIGN = "\u03C0";
    private static String THETA_SIGN = "\u03B8";

    //  PRIVACY POLICY LINK
    public static String PRIVACY_POLICY_LINK = "http://simsoftinc.blogspot.com/2018/10/privacy-policy.html";

    //  MENU
    private static boolean IS_MENU_OPENING = false;
    private static Fragment CURRENT_FRAGMENT = new ComeBackFragment();
    private static Fragment CURRENT_SUBMENU;

    //  APP TITLE
    private static String APP_TITLE = "SimMath";

    //  HISTORY
    private static int NO_SHOWED_TOPIC = 3;

    //  PROBLEM
    private static String LINKED_ID = "";

    //  USER
    private static int USER_LEVEL = 0;
    private static int USER_SELECTED_FEATURE = 0;
    private static String USER_NAME = "";

    //  MATH FOR KIDS
    private static int FIND_THE_CORRECT_NUMBER = 1;
    private static int FIND_THE_RIGHT_OPERATION = 1;

    //  MATH GAME
    private static boolean IS_NUMBER_GAME_SELECTED = true;
    private static boolean IS_FRACTION_GAME_SELECTED = false;
    private static boolean IS_PUZZLE_GAME_SELECTED = false;
    private static boolean IS_RIDDLE_GAME_SELECTED = false;

    private static boolean GAME_LEVEL_COMPLETE_STATUS = false;
    private static int NO_PUZZLE_GAME_CARD = 12;
    private static int PUZZLE_GAME_CURRENT_LEVEL = 1;
    private static int RIDDLE_GAME_LEVEL = 1;

    private static int COMPARISON_GAME_LEVEL = 1;

    //  MATH TIPS AND TRICK
    private static String TIPS_AND_TRICK_SELECTION = "";

    //  MATH_ROUND
    public static final int MATH_ROUND_INDEX = 1;
    private static int MATH_ROUND_LEVEL = 0;
    private static int MATH_ROUND_SELECTED_LEVEL = 0;

    //  WORKBOOK
    private static String WORKBOOK_ID;
    private static ArrayList<Topic> WORKBOOK_TOPICS;
    private static String WORKBOOK_DATE;
    private static int WORKBOOK_TIME = 0;
    private static int WORKBOOK_SCORE = 0;
    private static int WORKBOOK_NO_QUESTION = 0;
    private static boolean IS_WORKBOOK_HISTORY_SELECTED = false;

    //  LADDER
    private static String MATH_LADDER_ID;
    private static int MATH_LADDER_SCORE, MATH_LADDER_NO_QUESTION;
    private static Topic MATH_LADDER_TOPIC = new Topic();
    private static int MATH_LADDER_TIME = 0;
    private static int MATH_LADDER_DECREASE = 5;
    private static int MATH_LADDER_INCREASE = 2;
    private static int MATH_LADDER_MIN_REQUIREMENT = 100;
    private static boolean IS_MATH_LADDER_HISTORY_SELECTED = false;

    //  TEST
    private static String TEST_ID;
    private static int TEST_TIME = 0;
    private static int MAX_TEST_TIME = 24 * 60 * 60 * 1000;
    private static double MATH_TEST_PASSING_SCORE = 90.0;

    private static ArrayList<Topic> SELECTED_TOPICS;
    private static MathTest CURRENT_TEST;
    private static int ASSESSMENT_TOPIC = 0;

    private static boolean IS_REGULAR_TEST_SELECTED = true;
    private static boolean IS_ASSESSMENT_TEST_SELECTED = false;
    private static boolean IS_ACT_TEST_SELECTED = false;
    private static boolean IS_SAT_TEST_SELECTED = false;
    private static boolean IS_GED_TEST_SELECTED = false;

    private static int ACT_TEST_TIME = 60;
    private static int SAT_TEST_TIME = 80;
    private static int GED_TEST_TIME = 115;

    private static boolean IS_TEST_HISTORY_SELECTED = true;

    //  MATH STAGE
    public static final int MATH_STAGE_INDEX = 2;
    private static boolean IS_MATH_STAGE_SELECTED = false;
    private static String CURRENT_MATH_STAGE_ID = "";
    private static int MATH_STAGE_TOPIC = 1;
    private static int MATH_STAGE_LEVEL = 1;
    private static int MATH_STAGE_NO_WORKBOOK_QUESTION = 50;
    private static boolean IS_MATH_STAGE_HISTORY_SELECTED = false;

    //  MATH CONNECT
    private static boolean IS_MATH_CONNECT_SELECTED = false;
    private static String CURRENT_ASSIGNMENT = "";
    private static int NO_SHOWED_ASSIGNMENT = 5;

    //  MATH PRACTICE
    public static final int MATH_PRACTICE_INDEX = 3;
    private static String MATH_PRACTICE_ID;
    private static Topic MATH_PRACTICE_TOPIC;
    private static int MATH_PRACTICE_SCORE;
    private static int MATH_PRACTICE_NO_QUESTION;
    private static int MATH_PRACTICE_MIN_SCORE = 100;
    private static boolean IS_MATH_PRACTICE_HISTORY_SELECTED = false;

    //  MATH EXERCISE
    public static final int MATH_EXERCISE_INDEX = 4,
            MIN_EXERCISE_PASSING_NO_QUESTION = 100;
    public static final double MIN_EXERCISE_PASSING_SCORE = 95.0;
    private static String CURRENT_MATH_EXERCISE_ID, CURRENT_MATH_EXERCISE_LIST_ID;
    private static String MATH_EXERCISE_DATE;
    private static Topic MATH_EXERCISE_TOPIC;
    private static int MATH_EXERCISE_SECTION;
    private static int EXERCISE_LIST_NO_QUESTION = 0;
    private static int MATH_EXERCISE_TEST_TIME = 30;
    private static double EXERCISE_LIST_SCORE = 0.0;
    private static boolean IS_MATH_EXERCISE_HISTORY_SELECTED = false;

    //  GENERAL
    public static String getTextColor(){
        return TEXT_COLOR;
    }

    public static String getCheckMark(){
        return CHECK_MARK;
    }

    public static String getEmptySet(){
        return EMPTY_SET;
    }

    public static String getElementOf(){
        return ELEMENT_OF;
    }

    public static String getNotAnElementOf(){
        return NOT_AN_ELEMENT_OF;
    }

    public static String getIntersectionSet(){
        return INTERSECTION_SET;
    }

    public static String getUnionSet(){
        return UNION_SET;
    }

    public static String getProperSet(){
        return PROPER_SET;
    }

    public static String getSubSet(){
        return SUB_SET;
    }

    public static String getNotSubSet(){
        return NOT_SUB_SET;
    }

    public static String getDifferentOfSet(){
        return DIFFERENT_OF_SET;
    }

    public static String getInfinity(){
        return INFINITY;
    }

    public static String getNegativeInfinity(){
        return "-" + INFINITY;
    }

    public static String getPositiveInfinity(){
        return INFINITY;
    }

    public static String getOneHalf(){
        return ONE_HALF;
    }

    public static String getOneThird(){
        return ONE_THIRD;
    }

    public static String getOneForth(){
        return ONE_FORTH;
    }

    public static String getOneFifth(){
        return ONE_FIFTH;
    }

    public static String putExponent(int IntExponent){
        String StExponent;
        switch (IntExponent){
            case 2:
                StExponent = "\u00B2";
                break;
            case 3:
                StExponent = "\u00B3";
                break;
            case 4:
                StExponent = "\u2074";
                break;
            case 5:
                StExponent = "\u2075";
                break;
            case 6:
                StExponent = "\u2076";
                break;
            case 7:
                StExponent = "\u2077";
                break;
            case 8:
                StExponent = "\u2078";
                break;
            case 9:
                StExponent = "\u2079";
                break;
            default:
                StExponent = "";
                break;
        }

        return StExponent;
    }

    public static String getSup(String StData){
        return "<sup>" + StData + "</sup>";
    }

    public static String getSub(String StData){
        return "<sub>" + StData + "</sub>";
    }

    public static String getStartNumerator(){
        return START_NUMERATOR;
    }

    public static String getEndNumerator(){
        return END_NUMERATOR;
    }

    public static String getStartDenominator(){
        return START_DENOMINATOR;
    }

    public static String getEndDenominator(){
        return END_DENOMINATOR;
    }

    public static String getStartExponential(){
        return START_EXPONENTIAL;
    }

    public static String getEndExponential(){
        return END_EXPONENTIAL;
    }

    public static void addToSelectedTopics(Topic SelectedTopic){
        SELECTED_TOPICS.add(SelectedTopic);
    }

    public static boolean isTopicSelected(String TopicName){
        boolean isSelected = false;

        int size = SELECTED_TOPICS.size();
        for (int i = 0; i < size; i++){
            if (SELECTED_TOPICS.get(i).getName().equalsIgnoreCase(TopicName)){
                isSelected = true;
                break;
            }
        }

        return isSelected;
    }

    public static void removeFromSelectedTopics(Topic SelectedTopic){
        SELECTED_TOPICS.remove(SelectedTopic);
    }

    public static void clearSelectedTopics(){
        try {
            SELECTED_TOPICS.clear();
        }
        catch (Exception e) {
            SELECTED_TOPICS = new ArrayList<>();
        }
    }

    public static ArrayList<Topic> getSelectedTopics(){
        return SELECTED_TOPICS;
    }

    public static void setAssessmentTopic(int Topic_Index){
        ASSESSMENT_TOPIC = Topic_Index;
    }

    public static int getAssessmentTopic(){
        return ASSESSMENT_TOPIC;
    }

    public static int getNoShowedTopic(){
        return NO_SHOWED_TOPIC;
    }

    public static String getNoSolution(){
        return NO_SOLUTION;
    }

    public static String getInfiniteManySolution(){
        return INFINITE_MANY_SOLUTION;
    }

    //  SIGN
    public static String getDivisionSign(){
        return DIVISION_SIGN;
    }

    public static String getSquareRootSign(){
        return SQUARE_ROOT_SIGN;
    }

    public static String getAngleSign(){
        return ANGLE_SIGN;
    }

    public static String getTriangleSign(){
        return TRIANGLE_SIGN;
    }

    public static String getDegreeSign(){
        return START_EXPONENTIAL + "o" + END_EXPONENTIAL;
    }

    public static String getLessThanOrEqualSign(){
        return LESS_THAN_OR_EQUAL_SIGN;
    }

    public static String getGreaterThanOrEqualSign(){
        return GREATER_THAN_OR_EQUAL_SIGN;
    }

    public static String getNotEqualToSign(){
        return NOT_EQUAL_TO_SIGN;
    }

    public static String getPiSign(){
        return PI_SIGN;
    }

    public static String getThetaSign(){
        return THETA_SIGN;
    }

    public static String getExponentSign(String StExponent){
        return START_EXPONENTIAL + StExponent + END_EXPONENTIAL;
    }

    public static String getPlusAndMinusSign(){
        return PLUS_AND_MINUS_SIGN;
    }

    public static String getMinusAndPlusSign(){
        return MINUS_AND_PLUS_SIGN;
    }

    public static String getDate(){
        Calendar CurrentCalendar = Calendar.getInstance();
        return String.valueOf(CurrentCalendar.get(Calendar.MONTH) + 1) + "/"
                + String.valueOf(CurrentCalendar.get(Calendar.DATE)) + "/"
                + String.valueOf(CurrentCalendar.get(Calendar.YEAR));
    }

    //  MENU
    public static void setMenuOpenStatus(boolean isOpening){
        IS_MENU_OPENING = isOpening;
    }

    public static boolean getMenuOpenStatus(){
        return IS_MENU_OPENING;
    }

    public static void setAppTitle(String AppTitle){
        APP_TITLE = AppTitle;
    }

    public static String getAppTitle(){
        return APP_TITLE;
    }

    public static void setCurrentFragment(Fragment CurrentFragment){
        CURRENT_FRAGMENT = CurrentFragment;
    }

    public static Fragment getCurrentFragment(){
        return CURRENT_FRAGMENT;
    }

    public static void setCurrentSubmenu(Fragment CurrentSubmenu){
        CURRENT_SUBMENU = CurrentSubmenu;
    }

    public static Fragment getCurrentSubmenu(){
        return CURRENT_SUBMENU;
    }

    //  SET UP APP
    public static void setupApp(){
        //  MAIN FEATURE
        switch (USER_SELECTED_FEATURE){
            case MATH_ROUND_INDEX:
                setupMathRound();
                break;
            case MATH_STAGE_INDEX:
                setupMathStage();
                break;
            case MATH_PRACTICE_INDEX:
                setupMathPractice();
                break;
            case MATH_EXERCISE_INDEX:
                setupMathExercise();
                break;
            default:
                setupMathStage();
                break;
        }
    }

    public static int getEnterAnimation(){
        int AnimationMethod;

        switch (MathUtility.getRandomPositiveNumber_1Digit() % 4){
            case 0:
                AnimationMethod = R.anim.enter_from_left;
                break;
            case 1:
                AnimationMethod = R.anim.enter_from_right;
                break;
            case 2:
                AnimationMethod = R.anim.enter_from_top;
                break;
            case 3:
                AnimationMethod = R.anim.enter_from_bottom;
                break;
            default:
                AnimationMethod = R.anim.enter_from_left;
                break;

        }

        return AnimationMethod;
    }

    public static int getExitAnimation(){
        int AnimationMethod;

        switch (MathUtility.getRandomPositiveNumber_1Digit() % 4){
            case 0:
                AnimationMethod = R.anim.exit_to_left;
                break;
            case 1:
                AnimationMethod = R.anim.exit_to_right;
                break;
            case 2:
                AnimationMethod = R.anim.exit_to_top;
                break;
            case 3:
                AnimationMethod = R.anim.exit_to_bottom;
                break;
            default:
                AnimationMethod = R.anim.exit_to_left;
                break;

        }

        return AnimationMethod;
    }

    //  USER INFO
    public static void getUserInfo(){
        Cursor CurrentUser = DatabaseFile.getUserInfo();
        CurrentUser.moveToFirst();

        try {
            USER_NAME = CurrentUser.getString(1);
            USER_LEVEL = Integer.parseInt(CurrentUser.getString(2));
            USER_SELECTED_FEATURE = Integer.parseInt(CurrentUser.getString(3));
        }
        catch(Exception e) {
            DatabaseFile.reformUserTable();
            getUserInfo();
        }
    }

    public static int getUserLevel(){
        return USER_LEVEL;
    }

    public static int getUserSelectedFeature(){
        return USER_SELECTED_FEATURE;
    }

    public static String getUserName(){
        return USER_NAME;
    }

    public static void setupSelectedTopics(){
        SELECTED_TOPICS = new ArrayList<>();
    }

    //  KIDS
    public static void setFindTheCorrectNumber(int NumberRange){
        FIND_THE_CORRECT_NUMBER = NumberRange;
    }

    public static int getFindTheCorrectNumber(){
        return FIND_THE_CORRECT_NUMBER;
    }

    public static void setFindTheRightOperation(int Operation){
        FIND_THE_RIGHT_OPERATION = Operation;
    }

    public static int getFindTheRightOperation(){
        return FIND_THE_RIGHT_OPERATION;
    }

    //  CONNECT
    public static void setMathConnectSelection(boolean isSelected){
        IS_MATH_CONNECT_SELECTED = isSelected;
    }

    public static boolean getMathConnectSelection(){
        return IS_MATH_CONNECT_SELECTED;
    }

    public static void setCurrentAssignment(String CurrentAssignment){
        CURRENT_ASSIGNMENT = CurrentAssignment;
    }

    public static String getCurrentAssignment(){
        return CURRENT_ASSIGNMENT;
    }

    public static int getNoShowedAssignment(){
        return NO_SHOWED_ASSIGNMENT;
    }

    //  ROUND
    private static void setupMathRound(){
        Cursor CurrentUser = DatabaseFile.getUserRound();
        CurrentUser.moveToFirst();
        MATH_ROUND_LEVEL = Integer.parseInt(CurrentUser.getString(0));
    }

    public static void setMathRoundLevel(int Level){
        MATH_ROUND_LEVEL = Level;
    }

    public static int getMathRoundLevel(){
        return MATH_ROUND_LEVEL;
    }

    public static void setMathRoundSelectedLevel(int SelectedLevel){
        MATH_ROUND_SELECTED_LEVEL = SelectedLevel;
    }

    public static int getMathRoundSelectedLevel(){
        return MATH_ROUND_SELECTED_LEVEL;
    }

    //  WORKBOOK
    public static void setupWorkbook(){
        setMathStageSelection(false);
        WORKBOOK_TOPICS = new ArrayList<>();
    }

    public static void createNewWorkbook(){
        WORKBOOK_ID = MathUtility.getID();
        WORKBOOK_DATE = InfoCollector.getDate();
        WORKBOOK_TIME = 0;
        WORKBOOK_SCORE = 0;
        WORKBOOK_NO_QUESTION = 0;

        saveNewWorkbook();
    }

    private static void createNewWorkbook(Topic SelectedTopic){
        WORKBOOK_ID = MathUtility.getID();

        clearWorkbookTopics();
        addToWorkbookTopics(SelectedTopic);

        WORKBOOK_DATE = InfoCollector.getDate();
        WORKBOOK_TIME = 0;
        WORKBOOK_SCORE = 0;
        WORKBOOK_NO_QUESTION = 0;

        saveNewWorkbook();
    }

    private static void loadWorkbook(String WorkbookID, Topic SelectedTopic, int WorkTime, int Score, int NoQuestion){
        WORKBOOK_ID = WorkbookID;

        clearWorkbookTopics();
        addToWorkbookTopics(SelectedTopic);

        WORKBOOK_TIME = WorkTime;
        WORKBOOK_SCORE = Score;
        WORKBOOK_NO_QUESTION = NoQuestion;

        saveNewWorkbook();
    }

    private static void saveNewWorkbook(){
        String StTopic = "";
        int Size = WORKBOOK_TOPICS.size();
        for (int i = 0; i < Size; i++){
            StTopic += WORKBOOK_TOPICS.get(i).getName() + "\n";
        }

        DatabaseFile.addToMathWorkbookTable(WORKBOOK_ID, WORKBOOK_DATE, StTopic);
    }

    public static void setWorkbookID(String NewID){
        WORKBOOK_ID = NewID;
    }

    public static String getWorkbookID(){
        return WORKBOOK_ID;
    }

    public static void setWorkbookDate(String NewDate){
        WORKBOOK_DATE = NewDate;
    }

    public static String getWorkbookDate(){
        return WORKBOOK_DATE;
    }

    public static void addToWorkbookTopics(Topic WorkTopic){
        WORKBOOK_TOPICS.add(WorkTopic);
    }

    public static void clearWorkbookTopics(){
        try {
            WORKBOOK_TOPICS.clear();
        }
        catch (Exception e){
            WORKBOOK_TOPICS = new ArrayList<>();
        }
    }

    public static void setWorkbookScore(int Score){
        WORKBOOK_SCORE = Score;
    }

    public static int getWorkbookScore(){
        return WORKBOOK_SCORE;
    }

    public static void setWorkbookNoQuestion(int NoQuestion){
        WORKBOOK_NO_QUESTION = NoQuestion;
    }

    public static int getWorkbookNoQuestion(){
        if (WORKBOOK_NO_QUESTION == 0){
            WORKBOOK_NO_QUESTION = 1;
        }

        return WORKBOOK_NO_QUESTION;
    }

    public static void removeFromWorkbookTopics(Topic WorkbookTopic){
        WORKBOOK_TOPICS.remove(WorkbookTopic);
    }

    public static ArrayList<Topic> getWorkbookTopics(){
        return WORKBOOK_TOPICS;
    }

    public static void setWorkbookTime(int WorkbookTime){
        WORKBOOK_TIME = WorkbookTime;
    }

    public static int getWorkbookTime(){
        return WORKBOOK_TIME;
    }

    public static void setWorkbookHistorySelected(){
        IS_WORKBOOK_HISTORY_SELECTED = true;
        IS_MATH_LADDER_HISTORY_SELECTED = false;
        IS_TEST_HISTORY_SELECTED = false;
        IS_MATH_STAGE_HISTORY_SELECTED = false;
        IS_MATH_PRACTICE_HISTORY_SELECTED = false;
        IS_MATH_EXERCISE_HISTORY_SELECTED = false;
    }

    public static boolean getWorkbookHistorySelected(){
        return IS_WORKBOOK_HISTORY_SELECTED;
    }

    //  LADDER
    public static void setupMathLadder(){
        setMathStageSelection(false);
        MATH_LADDER_TOPIC = new Topic();
    }

    public static void createNewMathLadder(){
        createNewMathLadder(TopicCollection.getTopic(USER_LEVEL));
    }

    private static void createNewMathLadder(Topic SelectedTopic){
        MATH_LADDER_ID = MathUtility.getID();
        MATH_LADDER_TOPIC = SelectedTopic;
        MATH_LADDER_TIME = 0;
        MATH_LADDER_NO_QUESTION = 0;
        MATH_LADDER_SCORE = 0;

        saveNewLadder();
    }

    private static void createNewMathLadder(String MathLadderID){
        createNewMathLadder(TopicCollection.getTopic(USER_LEVEL));
        MATH_LADDER_ID = MathLadderID;

        saveNewLadder();
    }

    private static void loadMathLadder(String MathLadderID, Topic SelectedTopic, int WorkTime, int NoQuestion, int Score){
        MATH_LADDER_ID = MathLadderID;
        MATH_LADDER_TOPIC = SelectedTopic;
        MATH_LADDER_TIME = WorkTime;
        MATH_LADDER_NO_QUESTION = NoQuestion;
        MATH_LADDER_SCORE = Score;
    }

    private static void saveNewLadder(){
        DatabaseFile.addToMathLadderTable(MATH_LADDER_ID, getDate(), MATH_LADDER_TOPIC.getName());
    }

    public static void setMathLadderID(String LadderID){
        MATH_LADDER_ID = LadderID;
    }

    public static String getMathLadderID(){
        return MATH_LADDER_ID;
    }

    public static void setMathLadderTime(int WorkTime){
        MATH_LADDER_TIME = WorkTime;
    }

    public static int getMathLadderTime(){
        return MATH_LADDER_TIME;
    }

    public static void setMathLadderNoQuestion(int NoQuestion){
        MATH_LADDER_NO_QUESTION = NoQuestion;
    }

    public static int getMathLadderNoQuestion(){
        return MATH_LADDER_NO_QUESTION;
    }

    public static void setMathLadderScore(int LadderScore){
        MATH_LADDER_SCORE = LadderScore;
    }

    public static int getMathLadderScore(){
        return MATH_LADDER_SCORE;
    }

    public static void setMathLadderHistorySelected(){
        IS_WORKBOOK_HISTORY_SELECTED = false;
        IS_MATH_LADDER_HISTORY_SELECTED = true;
        IS_TEST_HISTORY_SELECTED = false;
        IS_MATH_STAGE_HISTORY_SELECTED = false;
        IS_MATH_PRACTICE_HISTORY_SELECTED = false;
        IS_MATH_EXERCISE_HISTORY_SELECTED = false;
    }

    public static boolean getMathLadderHistorySelected(){
        return IS_MATH_LADDER_HISTORY_SELECTED;
    }

    public static int getMathLadderMinRequirement(){
        return MATH_LADDER_MIN_REQUIREMENT;
    }

    public static int getMathLadderDecrease(){
        return MATH_LADDER_DECREASE;
    }

    public static int getMathLadderIncrease(){
        return MATH_LADDER_INCREASE;
    }

    public static void setMathLadderTopic(Topic MathLadderTopic){
        MATH_LADDER_TOPIC = MathLadderTopic;
    }

    public static Topic getMathLadderTopic(){
        return MATH_LADDER_TOPIC;
    }

    //  TEST
    public static void setupMathTest(){
        setMathStageSelection(false);
        setupSelectedTopics();
    }

    public static void setMathTestID(String TestID){
        TEST_ID = TestID;
    }

    private static void createNewTest(){
        TEST_ID = MathUtility.getID();
        TEST_TIME = MATH_EXERCISE_TEST_TIME;
        setupSelectedTopics();
        addToSelectedTopics(TopicCollection.getTopic(USER_LEVEL));
    }

    private static void createNewTest(Topic SelectedTopic){
        TEST_ID = MathUtility.getID();
        TEST_TIME = MATH_EXERCISE_TEST_TIME;
        setupSelectedTopics();
        addToSelectedTopics(SelectedTopic);
    }

    public static String getMathTestID(){
        return TEST_ID;
    }

    public static void setTestTime(int TestTime){
        TEST_TIME = TestTime;
    }

    public static int getTestTime(){
        return TEST_TIME;
    }

    public static int getMaxTestTime(){
        return MAX_TEST_TIME;
    }

    public static double getMathTestPassingScore(){
        return MATH_TEST_PASSING_SCORE;
    }

    public static void saveTest(MathTest CurrentTest){
        CURRENT_TEST = CurrentTest;
    }

    public static MathTest getCurrentTest(){
        return CURRENT_TEST;
    }

    public static void setAssessmentTestSelected(){
        IS_ASSESSMENT_TEST_SELECTED = true;
        IS_REGULAR_TEST_SELECTED = false;
        IS_ACT_TEST_SELECTED = false;
        IS_SAT_TEST_SELECTED = false;
        IS_GED_TEST_SELECTED = false;
    }

    public static boolean getAssessmentTestSelected(){
        return IS_ASSESSMENT_TEST_SELECTED;
    }

    public static void setRegularTestSelected(){
        IS_ASSESSMENT_TEST_SELECTED = false;
        IS_REGULAR_TEST_SELECTED = true;
        IS_ACT_TEST_SELECTED = false;
        IS_SAT_TEST_SELECTED = false;
        IS_GED_TEST_SELECTED = false;
    }

    public static boolean getRegularTestSelected(){
        return IS_REGULAR_TEST_SELECTED;
    }

    public static void setACTTestSelected(){
        IS_ASSESSMENT_TEST_SELECTED = false;
        IS_REGULAR_TEST_SELECTED = false;
        IS_ACT_TEST_SELECTED = true;
        IS_SAT_TEST_SELECTED = false;
        IS_GED_TEST_SELECTED = false;
    }

    public static boolean getACTTestSelected(){
        return IS_ACT_TEST_SELECTED;
    }

    public static void setSATTestSelected(){
        IS_ASSESSMENT_TEST_SELECTED = false;
        IS_REGULAR_TEST_SELECTED = false;
        IS_ACT_TEST_SELECTED = false;
        IS_SAT_TEST_SELECTED = true;
        IS_GED_TEST_SELECTED = false;
    }

    public static boolean getSATTestSelected(){
        return IS_SAT_TEST_SELECTED;
    }

    public static void setGEDTestSelected(){
        IS_ASSESSMENT_TEST_SELECTED = false;
        IS_REGULAR_TEST_SELECTED = false;
        IS_ACT_TEST_SELECTED = false;
        IS_SAT_TEST_SELECTED = false;
        IS_GED_TEST_SELECTED = true;
    }

    public static boolean getGEDTestSelected(){
        return IS_GED_TEST_SELECTED;
    }

    public static void setTestHistorySelected(){
        IS_WORKBOOK_HISTORY_SELECTED = false;
        IS_MATH_LADDER_HISTORY_SELECTED = false;
        IS_TEST_HISTORY_SELECTED = true;
        IS_MATH_STAGE_HISTORY_SELECTED = false;
        IS_MATH_PRACTICE_HISTORY_SELECTED = false;
        IS_MATH_EXERCISE_HISTORY_SELECTED = false;
    }

    public static boolean getTestHistorySelected(){
        return IS_TEST_HISTORY_SELECTED;
    }

    public static void setACTTestTime(){
        TEST_TIME = ACT_TEST_TIME;
    }

    public static void setSATTestTime(){
        TEST_TIME = SAT_TEST_TIME;
    }

    public static void setGEDTestTime(){
        TEST_TIME = GED_TEST_TIME;
    }

    //  STAGE
    public static void setupMathStage(){
        //  SET MATH STAGE
        setMathStageSelection(true);

        Cursor LastMathStage = DatabaseFile.getMathStage();
        if (LastMathStage.getCount() > 0){
            LastMathStage.moveToFirst();

            String LastMathStageID = LastMathStage.getString(0);

            String LastMathStageTopic = LastMathStage.getString(2);
            Topic CurrentTopic = TopicCollection.getTopic(LastMathStageTopic);

            String LastMathStageWB = LastMathStage.getString(3);
            String LastMathStageLD = LastMathStage.getString(4);

            //  WORKBOOK
            Cursor LastWorkbook = DatabaseFile.getMathWorkbook(LastMathStageWB);
            LastWorkbook.moveToFirst();

            int WorkTime = Integer.parseInt(LastWorkbook.getString(3));
            int WBLastScore = Integer.parseInt(LastWorkbook.getString(4));
            int WBNoQuestion = Integer.parseInt(LastWorkbook.getString(5));

            if (WBLastScore < MATH_STAGE_NO_WORKBOOK_QUESTION){
                setMathStageLevel(1);

                //  WORKBOOK
                loadWorkbook(LastMathStageWB, CurrentTopic, WorkTime, WBLastScore, WBNoQuestion);

                //  LADDER
                setMathLadderTopic(CurrentTopic);
                createNewMathLadder(LastMathStageLD);

                //  TEST
                TEST_ID = MathUtility.getID();
                setupSelectedTopics();
                addToSelectedTopics(CurrentTopic);
            }
            else {
                //  LADDER
                Cursor LastLadder = DatabaseFile.getMathLadder(LastMathStageLD);
                LastLadder.moveToFirst();

                int LDLastWorkTime = Integer.parseInt(LastLadder.getString(3));
                int LDLastNoQuestion = Integer.parseInt(LastLadder.getString(4));
                int LDLastScore = Integer.parseInt(LastLadder.getString(5));

                if (LDLastScore < MATH_LADDER_MIN_REQUIREMENT){
                    setMathStageLevel(2);

                    //  LADDER
                    loadMathLadder(LastMathStageLD, CurrentTopic, LDLastWorkTime, LDLastNoQuestion, LDLastScore);

                    //  TEST
                    TEST_ID = MathUtility.getID();
                    setupSelectedTopics();
                    addToSelectedTopics(CurrentTopic);
                }
                else {
                    setMathStageLevel(3);

                    Cursor LastTest = DatabaseFile.getMathStageTestDetail(LastMathStageID);
                    LastTest.moveToFirst();

                    int LastTestScore = Integer.parseInt(LastTest.getString(5));
                    if (LastTestScore < MATH_TEST_PASSING_SCORE){
                        //  TEST
                        TEST_ID = MathUtility.getID();
                        setupSelectedTopics();
                        addToSelectedTopics(CurrentTopic);
                    }
                    else {
                        createNewMathStage();
                    }
                }
            }
        }
        else {
            createNewMathStage();
        }
    }

    private static void createNewMathStage(){
        createNewMathStage(USER_LEVEL);
    }

    private static void createNewMathStage(int SelectedTopicIndex){
        //  MATH STAGE
        String StID = MathUtility.getID();
        setCurrentMathStageID(MathUtility.getID());
        String StDate = getDate();
        Topic SelectedTopic = TopicCollection.getTopic(SelectedTopicIndex);
        DatabaseFile.addToMathStageTable(StID, StDate, SelectedTopic.getName());

        setMathStageLevel(1);

        //  WORKBOOK
        createNewWorkbook(SelectedTopic);
        DatabaseFile.addWBToMathStageTable(StID, WORKBOOK_ID);

        //  LADDER
        createNewMathLadder(SelectedTopic);
        DatabaseFile.addLDToMathStageTable(StID, MATH_LADDER_ID);

        //  TEST
        TEST_ID = MathUtility.getID();
        setupSelectedTopics();
        addToSelectedTopics(SelectedTopic);
    }

    public static void setMathStageHistorySelected(){
        IS_WORKBOOK_HISTORY_SELECTED = false;
        IS_MATH_LADDER_HISTORY_SELECTED = false;
        IS_TEST_HISTORY_SELECTED = false;
        IS_MATH_STAGE_HISTORY_SELECTED = true;
        IS_MATH_PRACTICE_HISTORY_SELECTED = false;
        IS_MATH_EXERCISE_HISTORY_SELECTED = false;
    }

    public static boolean getMathStageHistorySelected(){
        return IS_MATH_STAGE_HISTORY_SELECTED;
    }

    private static void setCurrentMathStageID(String ID){
        CURRENT_MATH_STAGE_ID = ID;
    }

    public static String getCurrentMathStageID(){
        return CURRENT_MATH_STAGE_ID;
    }

    public static void setMathStageSelection(boolean isSelected){
        IS_MATH_STAGE_SELECTED = isSelected;
    }

    public static boolean getMathStageSelection(){
        return IS_MATH_STAGE_SELECTED;
    }

    public static void setMathStageTopic(int TopicIndex){
        MATH_STAGE_TOPIC = TopicIndex;
    }

    public static int getMathStageTopic(){
        return MATH_STAGE_TOPIC;
    }

    public static void setMathStageLevel(int MathStageLevel){
        MATH_STAGE_LEVEL = MathStageLevel;
    }

    public static int getMathStageLevel(){
        return MATH_STAGE_LEVEL;
    }

    public static int getMathStageNoWorkbookQuestion(){
        return MATH_STAGE_NO_WORKBOOK_QUESTION;
    }

    //  PRACTICE
    public static void setupMathPractice(){
        Cursor LastMathPractice = DatabaseFile.getMathPractice();
        if (LastMathPractice.getCount() > 0) {
            LastMathPractice.moveToFirst();

            int LastScore = Integer.parseInt(LastMathPractice.getString(3));
            if (LastScore < MATH_PRACTICE_MIN_SCORE) {
                //  LOAD INFO
                loadMathPractice(LastMathPractice.getString(0),
                        TopicCollection.getTopic(LastMathPractice.getString(2)),
                        Integer.parseInt(LastMathPractice.getString(4)),
                        LastScore);
            } else {
                createNewMathPractice();
            }
        }
        else {
            createNewMathPractice();
        }
    }

    private static void createNewMathPractice(){
        //  GET NEW INFO
        MATH_PRACTICE_ID = MathUtility.getID();
        MATH_PRACTICE_TOPIC = TopicCollection.getTopic(USER_LEVEL);
        MATH_PRACTICE_SCORE = 0;
        MATH_PRACTICE_NO_QUESTION = 0;

        //  SAVE NEW INFO
        DatabaseFile.addToMathPracticeTable(MATH_PRACTICE_ID, InfoCollector.getDate(), MATH_PRACTICE_TOPIC.getName());
    }

    public static void createNewMathPractice(Topic SelectedTopic){
        //  GET NEW INFO
        MATH_PRACTICE_ID = MathUtility.getID();
        MATH_PRACTICE_TOPIC = SelectedTopic;
        MATH_PRACTICE_SCORE = 0;
        MATH_PRACTICE_NO_QUESTION = 0;

        //  SAVE NEW INFO
        DatabaseFile.addToMathPracticeTable(MATH_PRACTICE_ID, InfoCollector.getDate(), MATH_PRACTICE_TOPIC.getName());
    }

    private static void loadMathPractice(String MathPracticeID, Topic SelectedTopic, int NoQuestion, int Score){
        MATH_PRACTICE_ID = MathPracticeID;
        MATH_PRACTICE_TOPIC = SelectedTopic;
        MATH_PRACTICE_NO_QUESTION = NoQuestion;
        MATH_PRACTICE_SCORE = Score;
    }

    public static void setMathPracticeID(String CurrentID){
        MATH_PRACTICE_ID = CurrentID;
    }

    public static String getMathPracticeID(){
        return MATH_PRACTICE_ID;
    }

    public static void setMathPracticeTopic(Topic SelectedTopic){
        MATH_PRACTICE_TOPIC = SelectedTopic;
    }

    public static Topic getMathPracticeTopic(){
        return MATH_PRACTICE_TOPIC;
    }

    public static void setMathPracticeScore(int Score){
        MATH_PRACTICE_SCORE = Score;
    }

    public static int getMathPracticeScore(){
        return MATH_PRACTICE_SCORE;
    }

    public static void setMathPracticeNoQuestion(int NoQuestion){
        MATH_PRACTICE_NO_QUESTION = NoQuestion;
    }

    public static int getMathPracticeNoQuestion(){
        if (MATH_PRACTICE_NO_QUESTION == 0){
            MATH_PRACTICE_NO_QUESTION = 1;
        }
        return MATH_PRACTICE_NO_QUESTION;
    }

    public static int getMathPracticeMinScore(){
        return MATH_PRACTICE_MIN_SCORE;
    }

    public static void setMathPracticeHistorySelected(){
        IS_WORKBOOK_HISTORY_SELECTED = false;
        IS_MATH_LADDER_HISTORY_SELECTED = false;
        IS_TEST_HISTORY_SELECTED = false;
        IS_MATH_STAGE_HISTORY_SELECTED = false;
        IS_MATH_PRACTICE_HISTORY_SELECTED = true;
        IS_MATH_EXERCISE_HISTORY_SELECTED = false;
    }

    public static boolean getMathPracticeHistorySelected(){
        return IS_MATH_PRACTICE_HISTORY_SELECTED;
    }

    //  MATH EXERCISE
    public static void setupMathExercise(){
        Cursor LastExercise = DatabaseFile.getMathExercise();
        if (LastExercise.getCount() > 0) {
            LastExercise.moveToFirst();
            String LastExerciseID = LastExercise.getString(0);

            String LastExerciseTopic = LastExercise.getString(2);
            Topic CurrentTopic = TopicCollection.getTopic(LastExerciseTopic);

            Cursor LastExerciseList = DatabaseFile.getExerciseList(LastExerciseID);
            LastExerciseList.moveToFirst();

            String ExListID = LastExerciseList.getString(0);
            int NoQuestion = Integer.parseInt(LastExerciseList.getString(1));
            double Score = Double.parseDouble(LastExerciseList.getString(2));

            if (Score < MIN_EXERCISE_PASSING_SCORE) {
                //  LOAD INFO
                MATH_EXERCISE_SECTION = 1;

                //  EXERCISE
                loadExerciseList(LastExerciseID, ExListID, NoQuestion, Score);

                //  TEST
                createNewTest(CurrentTopic);
            } else {
                //  LOAD TEST INFO
                Cursor LastTest = DatabaseFile.getMathExerciseTestDetail(LastExerciseID);
                if (LastTest.getCount() > 0) {
                    MATH_EXERCISE_SECTION = 2;

                    LastTest.moveToFirst();
                    String LastTestID = LastTest.getString(2);

                    Cursor LastTestScore = DatabaseFile.getMathTestScore(LastTestID);
                    LastTestScore.moveToFirst();
                    String LastScore = LastTestScore.getString(0);

                    if (Double.parseDouble(LastScore) < MATH_TEST_PASSING_SCORE) {
                        //  CONTINUE TESTING
                        createNewTest(CurrentTopic);
                    } else {
                        createNewMathExercise();
                    }
                }
                else {
                    createNewTest(CurrentTopic);
                }
            }
        }
        else {
            createNewMathExercise();
        }
    }

    private static void createNewMathExercise(){
        createNewMathExercise(TopicCollection.getTopic(USER_LEVEL));
    }

    public static void createNewMathExercise(Topic SelectedTopic){
        CURRENT_MATH_EXERCISE_ID = MathUtility.getID();
        MATH_EXERCISE_DATE = InfoCollector.getDate();
        MATH_EXERCISE_TOPIC = SelectedTopic;

        CURRENT_MATH_EXERCISE_LIST_ID = MathUtility.getID();
        EXERCISE_LIST_NO_QUESTION = 0;
        EXERCISE_LIST_SCORE = 0.0;

        TEST_ID = MathUtility.getID();
        TEST_TIME = MATH_EXERCISE_TEST_TIME;

        DatabaseFile.addToExerciseList(CURRENT_MATH_EXERCISE_LIST_ID, EXERCISE_LIST_NO_QUESTION, EXERCISE_LIST_SCORE);
        DatabaseFile.addToMathExercise(CURRENT_MATH_EXERCISE_ID, MATH_EXERCISE_DATE, MATH_EXERCISE_TOPIC.getName());
    }

    private static void loadExerciseList(String MathExerciseID, String ListID, int NoQuestion, double Score){
        CURRENT_MATH_EXERCISE_ID = MathExerciseID;
        CURRENT_MATH_EXERCISE_LIST_ID = ListID;
        EXERCISE_LIST_NO_QUESTION = NoQuestion;
        EXERCISE_LIST_SCORE = Score;
    }

    public static void setMathExerciseID(String ExerciseID){
        CURRENT_MATH_EXERCISE_ID = ExerciseID;
    }

    public static String getMathExerciseID(){
        return CURRENT_MATH_EXERCISE_ID;
    }

    public static void setMathExerciseListID(String ListID){
        CURRENT_MATH_EXERCISE_LIST_ID = ListID;
    }

    public static String getMathExerciseListID(){
        return CURRENT_MATH_EXERCISE_LIST_ID;
    }

    public static void setMathExerciseHistorySelected(){
        IS_WORKBOOK_HISTORY_SELECTED = false;
        IS_MATH_LADDER_HISTORY_SELECTED = false;
        IS_TEST_HISTORY_SELECTED = false;
        IS_MATH_STAGE_HISTORY_SELECTED = false;
        IS_MATH_PRACTICE_HISTORY_SELECTED = false;
        IS_MATH_EXERCISE_HISTORY_SELECTED = true;
    }

    public static boolean getMathExerciseHistorySelected(){
        return IS_MATH_EXERCISE_HISTORY_SELECTED;
    }

    public static void setMathExerciseDate(String CurrentDate){
        MATH_EXERCISE_DATE = CurrentDate;
    }

    public static String getMathExerciseDate(){
        return MATH_EXERCISE_DATE;
    }

    public static void setMathExerciseTopic(Topic SelectedTopic){
        MATH_EXERCISE_TOPIC = SelectedTopic;
    }

    public static Topic getMathExerciseTopic(){
        return MATH_EXERCISE_TOPIC;
    }

    public static void setMathExerciseSection(int MathExerciseSection){
        MATH_EXERCISE_SECTION = MathExerciseSection;
    }

    public static int getMathExerciseSection(){
        return MATH_EXERCISE_SECTION;
    }

    public static void setExerciseListNoQuestion(int NoQuestion){
        EXERCISE_LIST_NO_QUESTION = NoQuestion;
    }

    public static int getExerciseListNoQuestion(){
        return EXERCISE_LIST_NO_QUESTION;
    }

    public static void setMathExerciseScore(double Score){
        EXERCISE_LIST_SCORE = Score;
    }

    public static double getMathExerciseScore(){
        return EXERCISE_LIST_SCORE;
    }

    //  GAME
    public static void setGameLevelCompleteStatus(boolean isCompleted){
        GAME_LEVEL_COMPLETE_STATUS = isCompleted;
    }

    public static boolean getGameLevelCompleteStatus(){
        return GAME_LEVEL_COMPLETE_STATUS;
    }

    public static void setPuzzleGameSelected(){
        IS_NUMBER_GAME_SELECTED = false;
        IS_FRACTION_GAME_SELECTED = false;
        IS_PUZZLE_GAME_SELECTED = true;
        IS_RIDDLE_GAME_SELECTED = false;
    }

    public static boolean getPuzzleGameSelected(){
        return IS_PUZZLE_GAME_SELECTED;
    }

    public static void setRiddleGameSelected(){
        IS_NUMBER_GAME_SELECTED = false;
        IS_FRACTION_GAME_SELECTED = false;
        IS_PUZZLE_GAME_SELECTED = false;
        IS_RIDDLE_GAME_SELECTED = true;
    }

    public static boolean getRiddleGameSelected(){
        return IS_RIDDLE_GAME_SELECTED;
    }

    public static int getNoPuzzleGameCard(){
        return NO_PUZZLE_GAME_CARD;
    }

    public static void setPuzzleGameCurrentLevel(int GameLevel){
        PUZZLE_GAME_CURRENT_LEVEL = GameLevel;
    }

    public static int getPuzzleGameCurrentLevel(){
        return PUZZLE_GAME_CURRENT_LEVEL;
    }

    public static void setRiddleGameLevel(int Level){
        RIDDLE_GAME_LEVEL = Level;
    }

    public static int getRiddleGameLevel(){
        return RIDDLE_GAME_LEVEL;
    }

    public static void setComparisonGameNumber(int Level){
        COMPARISON_GAME_LEVEL = Level;
    }

    public static int getComparisonGameLevel(){
        return COMPARISON_GAME_LEVEL;
    }

    public static void setNumberGameSelected(){
        IS_NUMBER_GAME_SELECTED = true;
        IS_FRACTION_GAME_SELECTED = false;
        IS_PUZZLE_GAME_SELECTED = false;
        IS_RIDDLE_GAME_SELECTED = false;
    }

    public static boolean getNumberGameSelected(){
        return IS_NUMBER_GAME_SELECTED;
    }

    public static void setFractionGameSelected(){
        IS_NUMBER_GAME_SELECTED = false;
        IS_FRACTION_GAME_SELECTED = true;
        IS_PUZZLE_GAME_SELECTED = false;
        IS_RIDDLE_GAME_SELECTED = false;
    }

    public static boolean getFractionGameSelected(){
        return IS_FRACTION_GAME_SELECTED;
    }

    //  TIP AND TRICK
    public static void setTipsAndTrickSelection(String TipsAndTricks){
        TIPS_AND_TRICK_SELECTION = TipsAndTricks;
    }

    public static String getTipsAndTrickSelection(){
        return TIPS_AND_TRICK_SELECTION;
    }

    //  DATABASE - LINKED ID
    public static void setLinkedID(String LinkedID){
        LINKED_ID = LinkedID;
    }

    public static String getLinkedID(){
        return LINKED_ID;
    }

}
