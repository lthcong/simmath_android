package adsfree.congla.android.cong.mymathapp;


import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import layout.MathStageTransferFragment;
import pckData.pckFirebase.LogEvents;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckMath.pckMathTopics.TopicCollection;
import pckString.StringUtility;


/**
 * A simple {@link Fragment} subclass.
 */
public class AssignmentListFragment extends Fragment {

    private LinearLayout lnlyAssignmentContainer;
    private Button btnAll, btnComplete, btnIncomplete, btnMore;
    private int NoShowedAssignment = InfoCollector.getNoShowedAssignment(),
            NoIncreasement  = 5,
            Selection = 1;
    private Cursor AssignmentList;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stAssignmentList));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void logMathConnect(){
        LogEvents.writeToLog(this.getActivity(), "MathConnect", "MathConnectAssignmentList", "MathConnectAssignmentList");
    }

    private void logMathConnectStart(){
        LogEvents.writeToLog(this.getActivity(), "MathConnect", "MathConnectStart", "MathConnectStart");
    }

    private void showAllAssignment(){
        showSelection(btnAll);

        AssignmentList = DatabaseFile.getMathAssignment();
        showAssignmentList();
    }

    private void showCompleteAssignment(){
        showSelection(btnComplete);

        AssignmentList = DatabaseFile.getCompleteMathAssignment();
        showAssignmentList();
    }

    private void showIncompleteAssignment(){
        showSelection(btnIncomplete);

        AssignmentList = DatabaseFile.getIncompleteMathAssignment();
        showAssignmentList();
    }

    private void showAssignmentList(){
        lnlyAssignmentContainer.removeAllViews();

        int ShowedAssignment = 0;
        if (AssignmentList.getCount() > 0) {
            while (AssignmentList.moveToNext()) {
                String StTopicID = AssignmentList.getString(0);
                String StTopic = AssignmentList.getString(1);
                String StStatus = AssignmentList.getString(2);

                TextView tvTopic = new TextView(this.getActivity());
                tvTopic.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                tvTopic.setBackgroundColor(Color.WHITE);
                tvTopic.setMinHeight(200);
                tvTopic.setGravity(Gravity.CENTER|Gravity.START);
                tvTopic.setId(Integer.parseInt(StTopicID));
                tvTopic.setText(StTopic);
                if (StStatus.equalsIgnoreCase(DatabaseFile.COMPLETE_ASSIGNMENT)){
                    tvTopic.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                    tvTopic.setEnabled(false);
                }
                else {
                    tvTopic.setTextColor(Color.RED);
                    tvTopic.setOnClickListener(new AssignmentSelectionListener());
                }

                ShowedAssignment++;
                if (ShowedAssignment >= NoShowedAssignment){
                    break;
                }

                lnlyAssignmentContainer.addView(tvTopic);
            }
        }
        else {
            TextView tvNoData = new TextView(this.getActivity());
            tvNoData.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            String StNoData = "<i>" + getResources().getString(R.string.stNoData) + "</i>";
            StringUtility.writeString(tvNoData, StNoData);
            tvNoData.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
            tvNoData.setTextSize(15);
            tvNoData.setGravity(Gravity.CENTER);
            tvNoData.setMinHeight(200);

            lnlyAssignmentContainer.addView(tvNoData);
        }
    }

    private void startMathStage(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathStageTransferFragment())
                .commit();
    }

    private void clearSelection(){
        btnAll.setTextColor(Color.GRAY);
        btnComplete.setTextColor(Color.GRAY);
        btnIncomplete.setTextColor(Color.GRAY);
    }

    private void showSelection(Button btnSelectedButton){
        btnSelectedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMoreAssignment(){
        new CountDownTimer(200, 200) {
            @Override
            public void onTick(long millisUntilFinished) {
                showSelection(btnMore);
            }

            @Override
            public void onFinish() {
                btnMore.setTextColor(Color.GRAY);
                NoShowedAssignment += NoIncreasement;
                switch (Selection){
                    case 1:
                        showAllAssignment();
                        break;
                    case 2:
                        showCompleteAssignment();
                        break;
                    case 3:
                        showIncompleteAssignment();
                        break;
                    default:
                        showAllAssignment();
                        break;
                }
            }
        }.start();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        logMathConnect();
        InfoCollector.setMathConnectSelection(false);
        clearSelection();
        showAllAssignment();
    }

    public AssignmentListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_assignment_list, container, false);

        //  GET REFERENCES
        lnlyAssignmentContainer = rootView.findViewById(R.id.lnlyAssignmentContainer_AssignmentListFragment);
        btnAll = rootView.findViewById(R.id.btnAll_AssignmentListFragment);
        btnComplete = rootView.findViewById(R.id.btnComplete_AssignmentListFragment);
        btnIncomplete = rootView.findViewById(R.id.btnIncomplete_AssignmentListFragment);
        btnMore = rootView.findViewById(R.id.btnMore_AssignmentListFragment);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnAll.setOnClickListener(ButtonListener);
        btnComplete.setOnClickListener(ButtonListener);
        btnIncomplete.setOnClickListener(ButtonListener);
        btnMore.setOnClickListener(ButtonListener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btnAll_AssignmentListFragment:
                    clearSelection();
                    showAllAssignment();
                    Selection = 1;
                    break;
                case R.id.btnComplete_AssignmentListFragment:
                    clearSelection();
                    showCompleteAssignment();
                    Selection = 2;
                    break;
                case R.id.btnIncomplete_AssignmentListFragment:
                    clearSelection();
                    showIncompleteAssignment();
                    Selection = 3;
                    break;
                case R.id.btnMore_AssignmentListFragment:
                    showMoreAssignment();
                    break;
            }
        }
    }

    private class AssignmentSelectionListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            logMathConnectStart();

            //  SET UP MATH CONNECT
            InfoCollector.setMathConnectSelection(true);
            InfoCollector.setCurrentAssignment(String.valueOf(v.getId()));

            //  SET UP MATH STAGE
            InfoCollector.setupMathStage();
            InfoCollector.setMathStageTopic(TopicCollection.getTopicID(((TextView) v).getText().toString().trim()));

            startMathStage();
        }
    }

}
