package adsfree.congla.android.cong.mymathapp;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Objects;

import layout.AssessmentTestFragment;
import layout.RegularTestFragment;
import pckInfo.InfoCollector;


/**
 * A simple {@link Fragment} subclass.
 */
public class MathTestStartingFragment extends Fragment {

    private String StartingMessage = "Ready? Your Test is starting ...";
    private TextView tvStartMessage;
    private CountDownTimer cdtCountDownTime;
    private int StartIndex = 0,
            CountDownInterval = 100,
            CountDownTime = 0,
            AddTime = 5;

    private void setAppTitle(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMyStudyInfo_1));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void getTestMessage(){
        if (InfoCollector.getSATTestSelected()){
            StartingMessage = "Ready? Your SAT Test is starting ...";
        }
        else {
            if (InfoCollector.getACTTestSelected()){
                StartingMessage = "Ready? Your ACT Test is starting ...";
            }
            else {
                if (InfoCollector.getGEDTestSelected()){
                    StartingMessage = "Ready? Your GED Test is starting ...";
                }
                else {
                    if (InfoCollector.getAssessmentTestSelected()){
                        StartingMessage = "Ready? Your Assessment Test is starting ...";
                    }
                    else {
                        StartingMessage = "Ready? Your Math Test is starting ...";
                    }
                }
            }
        }
    }

    private void showStartingMessage(){
        getTestMessage();
        tvStartMessage.setText("");

        final int Length = StartingMessage.length();
        CountDownTime = (Length + AddTime) * CountDownInterval;
        StartIndex = 0;
        cdtCountDownTime = new CountDownTimer(CountDownTime, CountDownInterval) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (StartIndex < Length) {
                    tvStartMessage.append(String.valueOf(StartingMessage.charAt(StartIndex)));
                }

                StartIndex++;
            }

            @Override
            public void onFinish() {
                autoTransfer();
            }
        }.start();
    }

    private void showAssessmentTest(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new AssessmentTestFragment())
                .commit();
    }

    private void showMathTest(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new RegularTestFragment())
                .commit();
    }

    private void autoTransfer(){
        if (InfoCollector.getAssessmentTestSelected()){
            showAssessmentTest();
        }
        else {
            showMathTest();
        }
    }

    private void startup(){
        InfoCollector.setCurrentFragment(this);
        setAppTitle();
        showStartingMessage();
    }

    public MathTestStartingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_test_starting, container, false);

        //  GET REFERENCES
        tvStartMessage = rootView.findViewById(R.id.tvStartMessage_MathTestStartingFragment);

        //  START UP
        startup();
        
        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }
    
}
