package adsfree.congla.android.cong.mymathapp;


import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Objects;

import pckInfo.InfoCollector;


/**
 * A simple {@link Fragment} subclass.
 */
public class MathExerciseSectionFragment extends Fragment {

    private TextView tvSection, tvGoal;
    private CountDownTimer cdtCountDownTime;
    private String SECTION_ONE = "SECTION 1: EXERCISE", GOAL_ONE = "Goal: at least 100 problems with 95% or more.",
            SECTION_TWO = "SECTION 2: MATH TEST", GOAL_TWO = "Goal: at least 90%.";

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathExercise));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void showSectionOne(){
        tvSection.setText(SECTION_ONE);
        tvGoal.setText(GOAL_ONE);
    }

    private void showSectionTwo(){
        tvSection.setText(SECTION_TWO);
        tvGoal.setText(GOAL_TWO);
    }

    private void showInfo(){
        switch (InfoCollector.getMathExerciseSection()){
            case 1:
                showSectionOne();
                break;
            case 2:
                showSectionTwo();
                break;
            default:
                showSectionOne();
                break;
        }
    }

    private void showMathExercise(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathExerciseProblemFragment());
        ft.commit();
    }

    private void autoTransfer(){
        cdtCountDownTime = new CountDownTimer(1000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                showMathExercise();
            }
        }.start();
    }

    private void startup(){
        setupAppTile();
        InfoCollector.setCurrentFragment(this);
        showInfo();
        autoTransfer();
    }

    public MathExerciseSectionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_exercise_section, container, false);

        //  GET REFERENCES
        tvSection = rootView.findViewById(R.id.tvMathExerciseSection_MathExerciseSectionFragment);
        tvGoal = rootView.findViewById(R.id.tvMathExerciseGoal_MathExerciseSectionFragment);

        //  START UP
        startup();

        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }
}
