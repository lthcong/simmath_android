package adsfree.congla.android.cong.mymathapp;


import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import layout.BlankFragment;
import layout.ProblemDetailFragment;
import pckAds.Ads;
import pckData.pckFirebase.LogEvents;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckString.StringUtility;


/**
 * A simple {@link Fragment} subclass.
 */
public class MathExerciseHistoryFragment extends Fragment {

    private LinearLayout lnlyMathExerciseHistory;
    private Button btnMoreHistory;
    private EditText edtSearchHistory;
    private Cursor MathExerciseHistory;
    private int NoShowedHistory = InfoCollector.getNoShowedTopic();
    private ArrayList<String> MathExerciseIDCollection, TestIDCollection;

    private void setAppTitle(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathExerciseHistory));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void logMathExerciseHistoryView(){
        LogEvents.writeToLog(this.getActivity(), "ViewHistory", "WorkingHistory", "MathExerciseHistory");
    }

    private void showClearHistoryButton(){
        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.lnlybtnClearHistoryContainer_MathExerciseHistoryFragment, new ButtonClearHistoryFragment())
                .commit();
    }

    private void showHistory(){
        int ShowedHistory = 0,
                ExerciseIDIndex = 0, TestIDIndex = 0,
                Count = 0, AdsStep = Ads.WRAPPED_ADS_STEP;

        lnlyMathExerciseHistory.removeAllViews();

        MathExerciseIDCollection = new ArrayList<>();
        TestIDCollection = new ArrayList<>();

        if (MathExerciseHistory.getCount() > 0){
            while(MathExerciseHistory.moveToNext()){
                LinearLayout.LayoutParams MathExerciseLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                LinearLayout lnlyMathExerciseLayout = new LinearLayout(this.getActivity());
                lnlyMathExerciseLayout.setLayoutParams(MathExerciseLayoutParams);
                lnlyMathExerciseLayout.setOrientation(LinearLayout.VERTICAL);

                //  ADS
                Count++;
                if (Count % AdsStep == 0){
                    Ads.showWrappedAds(this.getActivity(), lnlyMathExerciseHistory);
                }

                //  DATE
                TextView tvDate = new TextView(this.getActivity());
                tvDate.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvDate.setBackgroundColor(Color.WHITE);
                tvDate.setGravity(Gravity.CENTER|Gravity.START);
                tvDate.setMinHeight(150);
                tvDate.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                //  TOPIC
                TextView tvTopic = new TextView(this.getActivity());
                tvTopic.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvTopic.setBackgroundColor(Color.WHITE);
                tvTopic.setGravity(Gravity.CENTER|Gravity.START);
                tvTopic.setMinHeight(150);
                tvTopic.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                //  EXERCISE
                TextView tvExercise = new TextView(this.getActivity());
                tvExercise.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvExercise.setBackgroundColor(Color.WHITE);
                tvExercise.setGravity(Gravity.CENTER|Gravity.START);
                tvExercise.setMinHeight(150);
                tvExercise.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                
                //  NO.QUESTION
                TextView tvExerciseNoQuestion = new TextView(this.getActivity());
                tvExerciseNoQuestion.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvExerciseNoQuestion.setBackgroundColor(Color.WHITE);
                tvExerciseNoQuestion.setGravity(Gravity.CENTER|Gravity.START);
                tvExerciseNoQuestion.setMinHeight(150);
                tvExerciseNoQuestion.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                //  SCORE
                TextView tvExerciseScore = new TextView(this.getActivity());
                tvExerciseScore.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvExerciseScore.setBackgroundColor(Color.WHITE);
                tvExerciseScore.setGravity(Gravity.CENTER|Gravity.START);
                tvExerciseScore.setMinHeight(150);
                tvExerciseScore.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                //  SHOW DETAIL
                Button btnShowDetailEX = new Button(this.getActivity());
                btnShowDetailEX.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                btnShowDetailEX.setBackgroundColor(Color.WHITE);
                btnShowDetailEX.setGravity(Gravity.CENTER|Gravity.START);
                btnShowDetailEX.setMinimumHeight(100);
                btnShowDetailEX.setMinimumWidth(350);
                btnShowDetailEX.setTextColor(Color.GRAY);

                //  TEST
                TextView tvTest = new TextView(this.getActivity());
                tvTest.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvTest.setBackgroundColor(Color.WHITE);
                tvTest.setGravity(Gravity.CENTER|Gravity.START);
                tvTest.setMinHeight(150);
                tvTest.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                //  ID
                String CurrentMathExerciseID = MathExerciseHistory.getString(0);
                MathExerciseIDCollection.add(CurrentMathExerciseID);

                //  DATE
                String StDate = "<b>Date: </b>" + MathExerciseHistory.getString(1);
                StringUtility.writeString(tvDate, StDate);
                lnlyMathExerciseLayout.addView(tvDate);

                //  TOPICS
                String StTopic = "<b>Topic(s): </b>" + MathExerciseHistory.getString(2);
                StringUtility.writeString(tvTopic, StTopic);
                lnlyMathExerciseLayout.addView(tvTopic);

                //  EXERCISE
                Cursor CurrentExerciseList = DatabaseFile.getExerciseList(MathExerciseHistory.getString(3));
                CurrentExerciseList.moveToFirst();

                MathExerciseIDCollection.add(CurrentExerciseList.getString(0));

                String StExercise = "<b>" + getResources().getString(R.string.stCircledA) + " Exercise:</b> ";
                StringUtility.writeString(tvExercise, StExercise);
                lnlyMathExerciseLayout.addView(tvExercise);

                String StExerciseNoQuestion = "<b>No. Question :</b> " + CurrentExerciseList.getString(1);
                StringUtility.writeString(tvExerciseNoQuestion, StExerciseNoQuestion);
                lnlyMathExerciseLayout.addView(tvExerciseNoQuestion);

                String StDetail = "Show Detail";
                btnShowDetailEX.setText(StDetail);
                btnShowDetailEX.setId(ExerciseIDIndex);
                btnShowDetailEX.setOnClickListener(new ShowExerciseDetailListener());
                lnlyMathExerciseLayout.addView(btnShowDetailEX);
                ExerciseIDIndex++;

                //  TEST
                Cursor CurrentTests = DatabaseFile.getMathExerciseTestDetail(CurrentMathExerciseID);
                int NoTest = CurrentTests.getCount();

                String StTest = "<b>" + getResources().getString(R.string.stCircledC) + " Test(s):</b> ";
                StringUtility.writeString(tvTest, StTest);
                lnlyMathExerciseLayout.addView(tvTest);

                for (int i = 0; i < NoTest; i++){
                    TextView tvTestScore = new TextView(this.getActivity());
                    tvTestScore.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    tvTestScore.setBackgroundColor(Color.WHITE);
                    tvTestScore.setGravity(Gravity.CENTER|Gravity.START);
                    tvTestScore.setMinHeight(150);
                    tvTestScore.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                    Cursor CurrentTest = DatabaseFile.getMathTest(CurrentTests.getString(2));

                    String StTestScore = "<b>Score :</b> " + CurrentTest.getString(5);
                    StringUtility.writeString(tvTestScore, StTestScore);
                    lnlyMathExerciseLayout.addView(tvTestScore);
                    TestIDCollection.add(TestIDIndex, CurrentTest.getString(0));

                    Button btnShowDetailTest = new Button(this.getActivity());
                    btnShowDetailTest.setText(StDetail);
                    btnShowDetailTest.setId(TestIDIndex);
                    btnShowDetailTest.setOnClickListener(new ShowTestDetailListener());
                    lnlyMathExerciseLayout.addView(btnShowDetailTest);
                    TestIDIndex++;
                }

                //  SEPARATOR
                LinearLayout.LayoutParams SeparatorParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                SeparatorParams.setMargins(0, 20, 0, 20);

                LinearLayout lnlySeparator = new LinearLayout(this.getActivity());
                lnlySeparator.setLayoutParams(SeparatorParams);
                lnlySeparator.setBackgroundColor(Color.parseColor(InfoCollector.getTextColor()));
                lnlySeparator.setMinimumHeight(5);

                //  ADD VIEW
                lnlyMathExerciseHistory.addView(lnlyMathExerciseLayout);
                lnlyMathExerciseHistory.addView(lnlySeparator);

                ShowedHistory++;
                if (ShowedHistory >= NoShowedHistory){
                    break;
                }
            }
        }
        else {
            TextView tvHistory = new TextView(this.getActivity());
            tvHistory.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            String StNoData = "<i>" + getResources().getString(R.string.stNoData) + "</i>";
            StringUtility.writeString(tvHistory, StNoData);
            tvHistory.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
            tvHistory.setTextSize(15);
            tvHistory.setGravity(Gravity.CENTER);
            tvHistory.setMinHeight(200);
            lnlyMathExerciseHistory.addView(tvHistory);
        }
    }

    private void showDetail(){
        //  REMOVE SUB MENU
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new BlankFragment())
                .commit();

        //  SHOW DETAIL
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new ProblemDetailFragment())
                .commit();
    }

    private void searchHistory(){
        //  SEARCH HISTORY
        String SearchInfo = edtSearchHistory.getText().toString().trim();
        if (SearchInfo.length() == 0){
            MathExerciseHistory = DatabaseFile.getMathExercise();
        }
        else {
            MathExerciseHistory = DatabaseFile.searchMathExerciseHistory(SearchInfo);
        }

        //  SHOW HISTORY
        showHistory();
    }

    private void setSelection(final Button ClickedButton){
        //  SET SELECTION
        ClickedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        //  CLEAR SELECTION
        new CountDownTimer(100, 100) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                ClickedButton.setTextColor(Color.GRAY);
            }
        }.start();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        logMathExerciseHistoryView();
        setAppTitle();

        searchHistory();
        showClearHistoryButton();
    }

    public MathExerciseHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_exercise_history, container, false);

        //  GET LISTENER
        lnlyMathExerciseHistory = rootView.findViewById(R.id.lnlyHistoryContainer_MathExerciseHistoryFragment);
        edtSearchHistory = rootView.findViewById(R.id.edtSearchHistory_MathExerciseHistoryFragment);
        btnMoreHistory = rootView.findViewById(R.id.btnMoreHistory_MathExerciseHistoryFragment);
        btnMoreHistory.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener button_listener = new ButtonClickListener();
        btnMoreHistory.setOnClickListener(button_listener);

        SearchListener HistorySearch = new SearchListener();
        edtSearchHistory.addTextChangedListener(HistorySearch);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            setSelection((Button) v);

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnMoreHistory_MathExerciseHistoryFragment:
                    NoShowedHistory += 2;
                    searchHistory();
                    break;
            }
        }
    }

    private class ShowExerciseDetailListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  LINKED ID
            InfoCollector.setLinkedID(MathExerciseIDCollection.get(v.getId()));

            //  DETAIL
            InfoCollector.setWorkbookHistorySelected();
            showDetail();
        }
    }

    private class ShowTestDetailListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  LINKED ID
            InfoCollector.setLinkedID(TestIDCollection.get(v.getId()));

            //  DETAIL
            InfoCollector.setTestHistorySelected();
            showDetail();
        }
    }

    private class SearchListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            searchHistory();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

}
