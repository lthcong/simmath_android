package adsfree.congla.android.cong.mymathapp;

import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.vungle.publisher.VunglePub;

import layout.BlankFragment;
import layout.MainMenuFragment;
import pckAds.Ads;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckMath.pckMathFormula.FormulaCollection;
import pckMath.pckMathTopics.RoundCollection;
import pckMath.pckMathTopics.TopicCollection;

public class MyMathAppActivity extends FragmentActivity {

    private final VunglePub VUNGLE_ADS = VunglePub.getInstance();
    private Button btnMenu;

    private void showMenuSelection(){
        btnMenu.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        new CountDownTimer(100, 100) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                btnMenu.setTextColor(Color.GRAY);
            }
        }.start();
    }

    private void getReferences(){
        btnMenu = findViewById(R.id.btnMenu_MyMathAppActivity);
        btnMenu.setTextColor(Color.GRAY);
    }

    private void setListener(){
        MenuSelectionListener MenuListener = new MenuSelectionListener();
        btnMenu.setOnClickListener(MenuListener);
    }

    private void hideSubmenu(){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new BlankFragment());
        ft.commit();
    }

    private void addMenu(){
        hideSubmenu();
        showMenuSelection();

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MainMenuFragment());
        ft.commit();
    }

    private void hideMenu(){
        if (InfoCollector.getCurrentSubmenu() != null){
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
            ft.replace(R.id.lnlySubMenuContainer_MyMathAppActivity, InfoCollector.getCurrentSubmenu());
            ft.commit();
        }
        else {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
            ft.replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new BlankFragment());
            ft.commit();
        }

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, InfoCollector.getCurrentFragment());
        ft.commit();
    }

    private void showWelcome(){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new WelcomeFragment());
        ft.commit();
    }

    private void setAppTitle(){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void setupApp(){
        //  TOPICS lIST
        TopicCollection.setTopicList();

        //  ROUND LIST
        RoundCollection.setupRoundList();

        //  FORMULA
        FormulaCollection.setFormulaList();

        //  SET UP ADS
        Ads.setupAds(this);

        //  MENU
        hideMenu();

        //  DATABASE
        DatabaseFile.createOrOpenDatabase(this);

        //  LOAD USER INFO
        InfoCollector.getUserInfo();

        //  SET UP APP
        InfoCollector.setupApp();
    }

    private void startUp(){
        try {
            //  APP TITLE
            setAppTitle();

            //  GET REFERENCES
            getReferences();

            //  SET LISTENER
            setListener();

            //  SET UP
            setupApp();

            //  SHOW WELCOME
            showWelcome();
        }
        catch (Exception e){
            Crashlytics.logException(e);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_math_app);

        //  START UP
        startUp();
    }

    @Override
    protected void onPause() {
        super.onPause();
        VUNGLE_ADS.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        VUNGLE_ADS.onResume();
    }

    @Override
    public void onBackPressed() {
        StartAppAd.onBackPressed(this);
        super.onBackPressed();
    }

    private class MenuSelectionListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //SET SELECTION
            showMenuSelection();

            switch (v.getId()){
                case R.id.btnMenu_MyMathAppActivity:
                    //  SHOW MENU
                    if (InfoCollector.getMenuOpenStatus()){
                        InfoCollector.setMenuOpenStatus(false);
                        hideMenu();
                    }
                    else {
                        InfoCollector.setMenuOpenStatus(true);
                        addMenu();
                    }
                    break;
            }
        }
    }

}
