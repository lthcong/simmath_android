package adsfree.congla.android.cong.mymathapp;


import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import layout.BlankFragment;
import layout.ProblemDetailFragment;
import pckAds.Ads;
import pckData.pckFirebase.LogEvents;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckString.StringUtility;


/**
 * A simple {@link Fragment} subclass.
 */
public class MathStageHistoryFragment extends Fragment {

    private LinearLayout lnlyMathStageHistory;
    private Button btnMoreHistory;
    private EditText edtSearchHistory;
    private Cursor MathStageHistory;
    private int NoShowedHistory = InfoCollector.getNoShowedTopic();
    private ArrayList<String> MathStageIDCollection, WorkbookIDCollection, LadderIDCollection, TestIDCollection;

    private void setAppTitle(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathStageHistory));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void logMathStageHistoryView(){
        LogEvents.writeToLog(this.getActivity(), "ViewHistory", "WorkingHistory", "MathStageHistory");
    }

    private void showClearHistoryButton(){
        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.lnlybtnClearHistoryContainer_MathStageHistoryFragment, new ButtonClearHistoryFragment())
                .commit();
    }

    private void showHistory(){
        int ShowedHistory = 0,
                WorkbookIDIndex = 0, LadderIDIndex = 0, TestIDIndex = 0,
                Count = 0, AdsStep = Ads.WRAPPED_ADS_STEP;

        lnlyMathStageHistory.removeAllViews();

        MathStageIDCollection = new ArrayList<>();
        WorkbookIDCollection = new ArrayList<>();
        LadderIDCollection = new ArrayList<>();
        TestIDCollection = new ArrayList<>();

        if (MathStageHistory.getCount() > 0){
            while(MathStageHistory.moveToNext()){
                LinearLayout.LayoutParams MathStageLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                LinearLayout lnlyMathStageLayout = new LinearLayout(this.getActivity());
                lnlyMathStageLayout.setLayoutParams(MathStageLayoutParams);
                lnlyMathStageLayout.setOrientation(LinearLayout.VERTICAL);

                //  ADS
                Count++;
                if (Count % AdsStep == 0){
                    Ads.showWrappedAds(this.getActivity(), lnlyMathStageHistory);
                }

                TextView tvDate = new TextView(this.getActivity());
                tvDate.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvDate.setBackgroundColor(Color.WHITE);
                tvDate.setGravity(Gravity.CENTER|Gravity.START);
                tvDate.setMinHeight(150);
                tvDate.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                TextView tvTopic = new TextView(this.getActivity());
                tvTopic.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvTopic.setBackgroundColor(Color.WHITE);
                tvTopic.setGravity(Gravity.CENTER|Gravity.START);
                tvTopic.setMinHeight(150);
                tvTopic.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                //  WORKBOOK
                TextView tvWorkbook = new TextView(this.getActivity());
                tvWorkbook.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvWorkbook.setBackgroundColor(Color.WHITE);
                tvWorkbook.setGravity(Gravity.CENTER|Gravity.START);
                tvWorkbook.setMinHeight(150);
                tvWorkbook.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                TextView tvWorkbookNoQuestion = new TextView(this.getActivity());
                tvWorkbookNoQuestion.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvWorkbookNoQuestion.setBackgroundColor(Color.WHITE);
                tvWorkbookNoQuestion.setGravity(Gravity.CENTER|Gravity.START);
                tvWorkbookNoQuestion.setMinHeight(150);
                tvWorkbookNoQuestion.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                Button btnShowDetailWB = new Button(this.getActivity());
                btnShowDetailWB.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                btnShowDetailWB.setBackgroundColor(Color.WHITE);
                btnShowDetailWB.setGravity(Gravity.CENTER|Gravity.START);
                btnShowDetailWB.setMinimumHeight(100);
                btnShowDetailWB.setMinimumWidth(350);
                btnShowDetailWB.setTextColor(Color.GRAY);

                //  LADDER
                TextView tvLadder = new TextView(this.getActivity());
                tvLadder.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvLadder.setBackgroundColor(Color.WHITE);
                tvLadder.setGravity(Gravity.CENTER|Gravity.START);
                tvLadder.setMinHeight(150);
                tvLadder.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                TextView tvLadderNoQuestion = new TextView(this.getActivity());
                tvLadderNoQuestion.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvLadderNoQuestion.setBackgroundColor(Color.WHITE);
                tvLadderNoQuestion.setGravity(Gravity.CENTER|Gravity.START);
                tvLadderNoQuestion.setMinHeight(150);
                tvLadderNoQuestion.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                TextView tvLadderScore = new TextView(this.getActivity());
                tvLadderScore.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvLadderScore.setBackgroundColor(Color.WHITE);
                tvLadderScore.setGravity(Gravity.CENTER|Gravity.START);
                tvLadderScore.setMinHeight(150);
                tvLadderScore.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                Button btnShowDetailLD = new Button(this.getActivity());
                btnShowDetailLD.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                btnShowDetailLD.setBackgroundColor(Color.WHITE);
                btnShowDetailLD.setGravity(Gravity.CENTER|Gravity.START);
                btnShowDetailLD.setMinimumHeight(100);
                btnShowDetailLD.setMinimumWidth(350);
                btnShowDetailLD.setTextColor(Color.GRAY);

                //  TEST
                TextView tvTest = new TextView(this.getActivity());
                tvTest.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvTest.setBackgroundColor(Color.WHITE);
                tvTest.setGravity(Gravity.CENTER|Gravity.START);
                tvTest.setMinHeight(150);
                tvTest.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                //  ID
                String CurrentMathStageID = MathStageHistory.getString(0);
                MathStageIDCollection.add(CurrentMathStageID);

                //  DATE
                String StDate = "<b>Date: </b>" + MathStageHistory.getString(1);
                StringUtility.writeString(tvDate, StDate);
                lnlyMathStageLayout.addView(tvDate);

                //  TOPICS
                String StTopic = "<b>Topic(s): </b>" + MathStageHistory.getString(2);
                StringUtility.writeString(tvTopic, StTopic);
                lnlyMathStageLayout.addView(tvTopic);

                //  WORKBOOK
                Cursor CurrentWorkbook = DatabaseFile.getMathWorkbook(MathStageHistory.getString(3));
                CurrentWorkbook.moveToFirst();

                WorkbookIDCollection.add(CurrentWorkbook.getString(0));

                String StWorkbook = "<b>" + getResources().getString(R.string.stCircledA) + " Workbook:</b> ";
                StringUtility.writeString(tvWorkbook, StWorkbook);
                lnlyMathStageLayout.addView(tvWorkbook);

                String StWorkbookNoQuestion = "<b>No. Question :</b> " + CurrentWorkbook.getString(4);
                StringUtility.writeString(tvWorkbookNoQuestion, StWorkbookNoQuestion);
                lnlyMathStageLayout.addView(tvWorkbookNoQuestion);

                String StDetail = "Show Detail";
                btnShowDetailWB.setText(StDetail);
                btnShowDetailWB.setId(WorkbookIDIndex);
                btnShowDetailWB.setOnClickListener(new ShowWorkbookDetailListener());
                lnlyMathStageLayout.addView(btnShowDetailWB);
                WorkbookIDIndex++;

                //  LADDER
                Cursor CurrentLadder = DatabaseFile.getMathWorkbook(MathStageHistory.getString(4));
                CurrentLadder.moveToFirst();

                LadderIDCollection.add(CurrentLadder.getString(0));

                String StLadder = "<b>" + getResources().getString(R.string.stCircledB) + " Ladder:</b> ";
                StringUtility.writeString(tvLadder, StLadder);
                lnlyMathStageLayout.addView(tvLadder);

                String StLadderNoQuestion = "<b>No. Question :</b> " + CurrentLadder.getString(4);
                StringUtility.writeString(tvLadderNoQuestion, StLadderNoQuestion);
                lnlyMathStageLayout.addView(tvLadderNoQuestion);

                String StLadderScore = "<b>No. Question :</b> " + CurrentLadder.getString(5);
                StringUtility.writeString(tvLadderScore, StLadderScore);
                lnlyMathStageLayout.addView(tvLadderScore);

                btnShowDetailLD.setText(StDetail);
                btnShowDetailLD.setId(LadderIDIndex);
                btnShowDetailLD.setOnClickListener(new ShowLadderDetailListener());
                lnlyMathStageLayout.addView(btnShowDetailLD);
                LadderIDIndex++;

                //  TEST
                Cursor CurrentTests = DatabaseFile.getMathStageTestDetail(CurrentMathStageID);
                int NoTest = CurrentTests.getCount();

                String StTest = "<b>" + getResources().getString(R.string.stCircledC) + " Test:</b> ";
                StringUtility.writeString(tvTest, StTest);
                lnlyMathStageLayout.addView(tvTest);

                for (int i = 0; i < NoTest; i++){
                    TextView tvTestScore = new TextView(this.getActivity());
                    tvTestScore.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    tvTestScore.setBackgroundColor(Color.WHITE);
                    tvTestScore.setGravity(Gravity.CENTER|Gravity.START);
                    tvTestScore.setMinHeight(150);
                    tvTestScore.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                    Cursor CurrentTest = DatabaseFile.getMathTest(CurrentTests.getString(2));
                    String StTestScore = "<b>Score :</b> " + CurrentTest.getString(5);
                    StringUtility.writeString(tvTestScore, StTestScore);
                    lnlyMathStageLayout.addView(tvTestScore);
                    TestIDCollection.add(TestIDIndex, CurrentTest.getString(0));

                    Button btnShowDetailTest = new Button(this.getActivity());
                    btnShowDetailTest.setText(StDetail);
                    btnShowDetailTest.setId(TestIDIndex);
                    btnShowDetailTest.setOnClickListener(new ShowTestDetailListener());
                    lnlyMathStageLayout.addView(btnShowDetailTest);
                    TestIDIndex++;
                }

                //  SEPARATOR
                LinearLayout.LayoutParams SeparatorParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                SeparatorParams.setMargins(0, 20, 0, 20);

                LinearLayout lnlySeparator = new LinearLayout(this.getActivity());
                lnlySeparator.setLayoutParams(SeparatorParams);
                lnlySeparator.setBackgroundColor(Color.parseColor(InfoCollector.getTextColor()));
                lnlySeparator.setMinimumHeight(5);

                //  ADD VIEW
                lnlyMathStageHistory.addView(lnlyMathStageLayout);
                lnlyMathStageHistory.addView(lnlySeparator);

                ShowedHistory++;
                if (ShowedHistory >= NoShowedHistory){
                    break;
                }
            }
        }
        else {
            TextView tvHistory = new TextView(this.getActivity());
            tvHistory.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            String StNoData = "<i>" + getResources().getString(R.string.stNoData) + "</i>";
            StringUtility.writeString(tvHistory, StNoData);
            tvHistory.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
            tvHistory.setTextSize(15);
            tvHistory.setGravity(Gravity.CENTER);
            tvHistory.setMinHeight(200);
            lnlyMathStageHistory.addView(tvHistory);
        }
    }

    private void showDetail(){
        //  REMOVE SUB MENU
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new BlankFragment())
                .commit();

        //  SHOW DETAIL
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new ProblemDetailFragment())
                .commit();
    }

    private void searchHistory(){
        //  SEARCH HISTORY
        String SearchInfo = edtSearchHistory.getText().toString().trim();
        if (SearchInfo.length() == 0){
            MathStageHistory = DatabaseFile.getMathStage();
        }
        else {
            MathStageHistory = DatabaseFile.searchMathStageHistory(SearchInfo);
        }

        //  SHOW HISTORY
        showHistory();
    }

    private void setSelection(final Button ClickedButton){
        //  SET SELECTION
        ClickedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        //  CLEAR SELECTION
        new CountDownTimer(100, 100) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                ClickedButton.setTextColor(Color.GRAY);
            }
        }.start();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        logMathStageHistoryView();
        setAppTitle();

        searchHistory();
        showClearHistoryButton();
    }

    public MathStageHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_stage_history, container, false);

        //  GET LISTENER
        lnlyMathStageHistory = rootView.findViewById(R.id.lnlyHistoryContainer_MathStageHistoryFragment);
        edtSearchHistory = rootView.findViewById(R.id.edtSearchHistory_MathStageHistoryFragment);
        btnMoreHistory = rootView.findViewById(R.id.btnMoreHistory_MathStageHistoryFragment);
        btnMoreHistory.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener button_listener = new ButtonClickListener();
        btnMoreHistory.setOnClickListener(button_listener);

        SearchListener HistorySearch = new SearchListener();
        edtSearchHistory.addTextChangedListener(HistorySearch);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            setSelection((Button) v);

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnMoreHistory_MathStageHistoryFragment:
                    NoShowedHistory += 2;
                    searchHistory();
                    break;
            }
        }
    }

    private class ShowWorkbookDetailListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  LINKED ID
            InfoCollector.setLinkedID(WorkbookIDCollection.get(v.getId()));

            //  DETAIL
            InfoCollector.setWorkbookHistorySelected();
            showDetail();
        }
    }

    private class ShowLadderDetailListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  LINKED ID
            InfoCollector.setLinkedID(LadderIDCollection.get(v.getId()));

            //  DETAIL
            InfoCollector.setMathLadderHistorySelected();
            showDetail();
        }
    }

    private class ShowTestDetailListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  LINKED ID
            InfoCollector.setLinkedID(TestIDCollection.get(v.getId()));

            //  DETAIL
            InfoCollector.setTestHistorySelected();
            showDetail();
        }
    }

    private class SearchListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            searchHistory();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

}
