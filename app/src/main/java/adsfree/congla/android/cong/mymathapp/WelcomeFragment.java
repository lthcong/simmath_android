package adsfree.congla.android.cong.mymathapp;


import android.database.Cursor;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;


/**
 * A simple {@link Fragment} subclass.
 */
public class WelcomeFragment extends Fragment {

    private TextView tvWelcome;
    private CountDownTimer cdtCountDownTime;
    private String WelcomeMessage = "Welcome to SimMath.";
    private int StartIndex, Length, CountDownInterval = 100, CountDownTime;

    private void showInitialSetup(){
        try {
            FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
            ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new InitialSetupFragment());
            ft.commit();
        }
        catch (Exception e){}
    }

    private void showComeBack(){
        try {
            FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
            ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new ComeBackFragment());
            ft.commit();
        }
        catch (Exception e){}
    }

    private void autoTransfer(){
        //  ALREADY SET UP
        if(InfoCollector.getUserLevel() > DatabaseFile.INITIAL_USER_LEVEL){
            showComeBack();
        }
        else {
            //  FIRST TIME
            showInitialSetup();
        }
    }

    private void showWelcomeMessage(){
        StartIndex = 0;
        Length = WelcomeMessage.length();
        CountDownTime = (Length + 10) * CountDownInterval;
        tvWelcome.setText("");

        cdtCountDownTime = new CountDownTimer(CountDownTime, CountDownInterval) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (StartIndex < Length) {
                    tvWelcome.append(String.valueOf(WelcomeMessage.charAt(StartIndex)));
                }

                StartIndex++;
            }

            @Override
            public void onFinish() {
                autoTransfer();
            }
        }.start();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        showWelcomeMessage();
    }

    public WelcomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_welcome, container, false);

        //  GET REFERENCES
        tvWelcome = rootView.findViewById(R.id.tvWelcome_WelcomeFragment);

        //  START UP
        startUp();

        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

}
