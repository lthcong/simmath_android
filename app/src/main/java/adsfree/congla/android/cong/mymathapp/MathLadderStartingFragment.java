package adsfree.congla.android.cong.mymathapp;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Objects;

import layout.MathLadderFragment;
import pckInfo.InfoCollector;


/**
 * A simple {@link Fragment} subclass.
 */
public class MathLadderStartingFragment extends Fragment {

    private String StartingMessage = "Ready? Your Ladder is starting ...";
    private TextView tvStartMessage;
    private CountDownTimer cdtCountDownTime;
    private int StartIndex = 0,
            CountDownInterval = 100,
            CountDownTime = 0,
            AddTime = 5;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathLadder_Name));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void showStartingMessage(){
        tvStartMessage.setText("");

        final int Length = StartingMessage.length();
        CountDownTime = (Length + AddTime) * CountDownInterval;
        StartIndex = 0;
        cdtCountDownTime = new CountDownTimer(CountDownTime, CountDownInterval) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (StartIndex < Length) {
                    tvStartMessage.append(String.valueOf(StartingMessage.charAt(StartIndex)));
                }

                StartIndex++;
            }

            @Override
            public void onFinish() {
                showMathLadder();
            }
        }.start();
    }

    private void showMathLadder(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathLadderFragment());
        ft.commit();
    }

    private void startup(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        showStartingMessage();
    }

    public MathLadderStartingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_ladder_starting, container, false);

        //  GET REFERENCES
        tvStartMessage = rootView.findViewById(R.id.tvStartMessage_MathLadderStartingFragment);

        //  START UP
        startup();

        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

}
