package adsfree.congla.android.cong.mymathapp;


import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

import layout.HistorySubMenuFragment;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;


/**
 * A simple {@link Fragment} subclass.
 */
public class ButtonClearHistoryFragment extends Fragment {

    private Button btnYes, btnNo, btnClear;
    private Dialog ConfirmDialog;

    private void showHistory(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction().setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new HistorySubMenuFragment())
                .commit();
    }

    private void clearHistory(){
        DatabaseFile.clearHistory(this.getActivity());
    }

    private void setupClearHistoryConfirmFragment(){
        ConfirmDialog = new Dialog(Objects.requireNonNull(this.getActivity()));
        ConfirmDialog.setContentView(R.layout.fragment_clear_history_confirm);

        btnYes = ConfirmDialog.findViewById(R.id.btnYes_ClearHistoryConfirmFragment);
        btnYes.setTextColor(Color.GRAY);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                clearHistory();
                ConfirmDialog.dismiss();
                showHistory();
            }
        });

        btnNo = ConfirmDialog.findViewById(R.id.btnNo_ClearHistoryConfirmFragment);
        btnNo.setTextColor(Color.GRAY);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                ConfirmDialog.dismiss();
                showHistory();
            }
        });

    }

    private void showClearHistoryConfirm(){
        setupClearHistoryConfirmFragment();
        ConfirmDialog.show();
    }

    public ButtonClearHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_button_clear_history, container, false);

        //  GET REFERENCES
        btnClear = rootView.findViewById(R.id.btnClearHistory_ButtonClearHistoryFragment);
        btnClear.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnClear.setOnClickListener(ButtonListener);

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnClearHistory_ButtonClearHistoryFragment:
                    showClearHistoryConfirm();
                    break;
            }
        }
    }

}
