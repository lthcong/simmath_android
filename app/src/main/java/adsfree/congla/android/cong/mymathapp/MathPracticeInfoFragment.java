package adsfree.congla.android.cong.mymathapp;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import layout.BlankFragment;
import pckInfo.InfoCollector;


/**
 * A simple {@link Fragment} subclass.
 */
public class MathPracticeInfoFragment extends Fragment {

    private Button btnStart;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathPractice));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void showTopicSelection(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathPracticeTopicSelectionFragment())
                .commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        btnStart.setTextColor(Color.GRAY);
    }

    public MathPracticeInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_practice_info, container, false);

        //  GET REFERENCES
        btnStart = rootView.findViewById(R.id.btnStart_MathPracticeInfoFragment);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnStart.setOnClickListener(ButtonListener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnStart_MathPracticeInfoFragment:
                    showTopicSelection();
                    break;
            }
        }
    }

}
