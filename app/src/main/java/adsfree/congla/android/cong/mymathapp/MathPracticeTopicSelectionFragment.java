package adsfree.congla.android.cong.mymathapp;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import pckAds.Ads;
import pckInfo.InfoCollector;
import pckMath.pckMathTopics.Topic;
import pckMath.pckMathTopics.TopicCollection;


/**
 * A simple {@link Fragment} subclass.
 */
public class MathPracticeTopicSelectionFragment extends Fragment {

    private LinearLayout lnlyTopicsContainer;
    private EditText edtSearchYourTopic;
    private Button btnAll, btnBasic, btnIntermediate, btnPreAlgebra, btnAlgebra1, btnAlgebra2,
            btnStart;
    private ArrayList<Topic> TopicList;
    private Topic SelectedTopic;
    private ArrayList<TextView> TextViewList;
    private int SelectedSection;

    private void setAppTitle(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stTopicSelection));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void setButtonClickSelection(Button btnClickedButton){
        btnClickedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void clearSectionSelection(){
        btnAll.setTextColor(Color.GRAY);
        btnBasic.setTextColor(Color.GRAY);
        btnIntermediate.setTextColor(Color.GRAY);
        btnPreAlgebra.setTextColor(Color.GRAY);
        btnAlgebra1.setTextColor(Color.GRAY);
        btnAlgebra2.setTextColor(Color.GRAY);
    }

    private void searchBySection(Button btnSelectedSection){
        //  CLEAR SELECTION
        clearSectionSelection();

        //  SET SELECTION
        setButtonClickSelection(btnSelectedSection);

        //  SHOW TOPIC
        TopicList = TopicCollection.searchBySection(SelectedSection);
        showTopicsList();
    }

    private void searchTopic(){
        //  START SEARCHING
        String SearchName = edtSearchYourTopic.getText().toString().trim();
        if (SearchName.length() > 0) {
            TopicList = TopicCollection.searchTopic(SearchName);
        }
        else {
            TopicList = TopicCollection.getTopicList();
        }

        //  SHOW RESULT
        showTopicsList();
    }

    private void showTopicsList(){
        int Count = 0, AdsStep = Ads.WRAPPED_ADS_STEP * 5;
        TextViewList = new ArrayList<>();

        lnlyTopicsContainer.removeAllViews();

        int size = TopicList.size();
        for (int i = 0; i < size; i++){
            LinearLayout.LayoutParams TopicsListParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            LinearLayout lnlyTopicListLayout = new LinearLayout(this.getActivity());
            lnlyTopicListLayout.setLayoutParams(TopicsListParams);
            lnlyTopicListLayout.setOrientation(LinearLayout.VERTICAL);
            lnlyTopicListLayout.setBackgroundColor(Color.parseColor(InfoCollector.getTextColor()));

            //  ADS
            Count++;
            if (Count % AdsStep == 0){
                Ads.showWrappedAds(this.getActivity(), lnlyTopicsContainer);
            }

            TextView tvTopicName = new TextView(this.getActivity());
            tvTopicName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            tvTopicName.setBackgroundColor(Color.WHITE);
            tvTopicName.setMinHeight(200);
            tvTopicName.setGravity(Gravity.CENTER|Gravity.START);
            tvTopicName.setId(i);
            tvTopicName.setOnClickListener(new TopicSelectionListener());

            String TopicName = TopicList.get(i).getName();
            tvTopicName.setText(TopicName);
            tvTopicName.setTextColor(Color.GRAY);
            tvTopicName.setTextSize(15);

            TextViewList.add(tvTopicName);

            lnlyTopicListLayout.addView(tvTopicName);

            lnlyTopicsContainer.addView(lnlyTopicListLayout);
        }

    }

    private void clearTopicSelection(){
        int size = TextViewList.size();

        for (int i = 0; i < size; i++){
            TextViewList.get(i).setTextColor(Color.GRAY);
            TextViewList.get(i).setText(TopicList.get(i).getName());
        }
    }

    private void startMathPractice(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathPracticeStartingFragment())
                .commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setAppTitle();
        TopicList = TopicCollection.getTopicList();
        showTopicsList();
    }

    public MathPracticeTopicSelectionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_practice_topic_selection, container, false);

        //  GET REFERENCES
        edtSearchYourTopic = rootView.findViewById(R.id.edtSearchYourTopic_MathPracticeTopicSelectionFragment);
        lnlyTopicsContainer = rootView.findViewById(R.id.lnlyTopicsContainer_MathPracticeTopicSelectionFragment);

        btnStart = rootView.findViewById(R.id.btnStart_MathPracticeTopicSelectionFragment);
        btnStart.setTextColor(Color.GRAY);

        btnAll = rootView.findViewById(R.id.btnAll_MathPracticeTopicSelectionFragment);
        btnAll.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        btnBasic = rootView.findViewById(R.id.btnBasic_MathPracticeTopicSelectionFragment);
        btnBasic.setTextColor(Color.GRAY);

        btnIntermediate = rootView.findViewById(R.id.btnIntermediate_MathPracticeTopicSelectionFragment);
        btnIntermediate.setTextColor(Color.GRAY);

        btnPreAlgebra = rootView.findViewById(R.id.btnPreAlgebra_MathPracticeTopicSelectionFragment);
        btnPreAlgebra.setTextColor(Color.GRAY);

        btnAlgebra1 = rootView.findViewById(R.id.btnAlgebra_1_MathPracticeTopicSelectionFragment);
        btnAlgebra1.setTextColor(Color.GRAY);

        btnAlgebra2 = rootView.findViewById(R.id.btnAlgebra_2_MathPracticeTopicSelectionFragment);
        btnAlgebra2.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonListener button_listener = new ButtonListener();
        btnAll.setOnClickListener(button_listener);
        btnBasic.setOnClickListener(button_listener);
        btnIntermediate.setOnClickListener(button_listener);
        btnPreAlgebra.setOnClickListener(button_listener);
        btnAlgebra1.setOnClickListener(button_listener);
        btnAlgebra2.setOnClickListener(button_listener);
        btnStart.setOnClickListener(button_listener);

        SearchListener search_listener = new SearchListener();
        edtSearchYourTopic.addTextChangedListener(search_listener);

        //  START UP
        startUp();
        
        return rootView;
    }

    private class ButtonListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnAll_MathPracticeTopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_All;
                    searchBySection((Button) v);
                    break;
                case R.id.btnBasic_MathPracticeTopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_Basic;
                    searchBySection((Button) v);
                    break;
                case R.id.btnIntermediate_MathPracticeTopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_Intermediate;
                    searchBySection((Button) v);
                    break;
                case R.id.btnPreAlgebra_MathPracticeTopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_PreAlgebra;
                    searchBySection((Button) v);
                    break;
                case R.id.btnAlgebra_1_MathPracticeTopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_Algebra_1;
                    searchBySection((Button) v);
                    break;
                case R.id.btnAlgebra_2_MathPracticeTopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_Algebra_2;
                    searchBySection((Button) v);
                    break;
                case R.id.btnStart_MathPracticeTopicSelectionFragment:
                    setButtonClickSelection((Button) v);
                    InfoCollector.createNewMathPractice(SelectedTopic);
                    startMathPractice();
                    break;
            }
        }
    }

    private class TopicSelectionListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  CLEAR SELECTION
            clearTopicSelection();

            //  SET SELECTION
            String TopicName = ((TextView) v).getText().toString().trim();
            String CheckMark = getResources().getString(R.string.stCheckMark);
            SelectedTopic = TopicList.get(v.getId());

            String StSelectedTopic = CheckMark + "\t\t" + TopicName;
            ((TextView) v).setText(StSelectedTopic);
            ((TextView) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            InfoCollector.setMathPracticeTopic(SelectedTopic);
        }
    }

    private class SearchListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            searchTopic();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

}
