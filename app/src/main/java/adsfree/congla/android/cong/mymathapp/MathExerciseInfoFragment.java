package adsfree.congla.android.cong.mymathapp;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import pckInfo.InfoCollector;


/**
 * A simple {@link Fragment} subclass.
 */
public class MathExerciseInfoFragment extends Fragment {

    private Button btnStart;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathExercise));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void setSelection(Button btnSelectedButton){
        btnSelectedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void startup(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        btnStart.setTextColor(Color.GRAY);
    }

    private void showMathExerciseTopicSelection(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathExerciseTopicSelection());
        ft.commit();
    }

    public MathExerciseInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_exercise_info, container, false);

        //  GET REFERENCES
        btnStart = rootView.findViewById(R.id.btnStart_MathExerciseInfoFragment);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnStart.setOnClickListener(ButtonListener);

        //  START UP
        startup();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            setSelection((Button) v);

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnStart_MathExerciseInfoFragment:
                    showMathExerciseTopicSelection();
                    break;
            }
        }
    }

}
