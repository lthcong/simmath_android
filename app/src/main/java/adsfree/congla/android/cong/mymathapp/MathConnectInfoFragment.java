package adsfree.congla.android.cong.mymathapp;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import pckData.pckFirebase.LogEvents;
import pckInfo.InfoCollector;
import pckString.StringUtility;


/**
 * A simple {@link Fragment} subclass.
 */
public class MathConnectInfoFragment extends Fragment {

    private LinearLayout lnlyMoreInfo;
    private TextView tvMoreInfo;
    private Button btnStart;

    private boolean isShowing = false;
    private static final String HIDE_INFO = "Hide info.";
    private static final String MORE_INFO = "More info.";

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathConnect));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void logMathConnect(){
        LogEvents.writeToLog(this.getActivity(), "MathConnect", "MathConnectInfo", "MathConnectInfo");
    }

    private void hideMoreInfo(){
        isShowing = false;
        tvMoreInfo.setText("");
        String HideInfo = "<u>" + MORE_INFO + "</u>";
        StringUtility.writeString(tvMoreInfo, HideInfo);

        lnlyMoreInfo.setVisibility(View.GONE);
    }

    private void showMoreInfo(){
        isShowing = true;
        tvMoreInfo.setText("");
        String HideInfo = "<u>" + HIDE_INFO + "</u>";
        StringUtility.writeString(tvMoreInfo, HideInfo);

        lnlyMoreInfo.setVisibility(View.VISIBLE);
    }

    private void showMathStageInfo(){
        if (isShowing){
            hideMoreInfo();
        }
        else {
            showMoreInfo();
        }
    }

    private void clearButtonSelection(){
        btnStart.setTextColor(Color.GRAY);
    }

    private void setButtonSelection(Button btnSelectedButton){
        btnSelectedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMathConnect(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathConnectFragment());
        ft.commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        logMathConnect();
        hideMoreInfo();
        clearButtonSelection();
    }

    public MathConnectInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_connect_info, container, false);

        //  GET REFERENCES
        lnlyMoreInfo = rootView.findViewById(R.id.lnlyLearnMore_MathConnectInfoFragment);
        tvMoreInfo = rootView.findViewById(R.id.tvLearnMore_MathConnectInfoFragment);
        btnStart = rootView.findViewById(R.id.btnStart_MathConnectInfoFragment);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        tvMoreInfo.setOnClickListener(ButtonListener);
        btnStart.setOnClickListener(ButtonListener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tvLearnMore_MathConnectInfoFragment:
                    showMathStageInfo();
                    break;
                case R.id.btnStart_MathConnectInfoFragment:
                    clearButtonSelection();
                    setButtonSelection((Button) v);
                    showMathConnect();
                    break;
            }
        }
    }

}
