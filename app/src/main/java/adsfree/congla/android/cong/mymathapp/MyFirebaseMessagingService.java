package adsfree.congla.android.cong.mymathapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.google.firebase.messaging.*;

import pckData.pckLocalData.DatabaseFile;

/**
 * Created by Cong on 11/29/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private String REMINDER = "Reminder";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        String Title = remoteMessage.getNotification().getTitle();
        if (Title.toLowerCase().contains(REMINDER.toLowerCase())) {
            if (DatabaseFile.getIncompleteMathAssignment().moveToFirst()) {

                Intent intent = new Intent(this, MyMathAppActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

                Notification.Builder notificationBuilder = new Notification.Builder(this);
                notificationBuilder.setContentTitle(Title);
                notificationBuilder.setContentText(remoteMessage.getNotification().getBody());
                notificationBuilder.setAutoCancel(true);
                notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
                notificationBuilder.setContentIntent(pendingIntent);

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                if (notificationManager != null) {
                    notificationManager.notify(0, notificationBuilder.build());
                }
            }
        }
        else {
            Intent intent = new Intent(this, MyMathAppActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            Notification.Builder notificationBuilder = new Notification.Builder(this);
            notificationBuilder.setContentTitle(Title);
            notificationBuilder.setContentText(remoteMessage.getNotification().getBody());
            notificationBuilder.setAutoCancel(true);
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
            notificationBuilder.setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.notify(0, notificationBuilder.build());
            }
        }
    }
}