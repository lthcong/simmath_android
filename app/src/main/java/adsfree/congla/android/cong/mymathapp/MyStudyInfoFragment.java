package adsfree.congla.android.cong.mymathapp;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import layout.MathStageTransferFragment;
import pckInfo.InfoCollector;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyStudyInfoFragment extends Fragment {

    private LinearLayout lnlyMathStage, lnlyMathRound, lnlyMathPractice, lnlyMathExercise;
    private Button btnBack, btnNext;
    private int SelectedFeature;

    private void setAppTitle(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathTest_Name));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void clearSelection(){
        btnBack.setTextColor(Color.GRAY);
        btnNext.setTextColor(Color.GRAY);        
    }
    
    private void setSelection(Button btnSelectedButton){
        btnSelectedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }
    
    private void hideInfo(){
        lnlyMathRound.setVisibility(View.GONE);
        lnlyMathStage.setVisibility(View.GONE);
        lnlyMathPractice.setVisibility(View.GONE);
        lnlyMathExercise.setVisibility(View.GONE);
    }
    
    private void showInfo(){
        //  HIDE ALL INFO
        hideInfo();

        //  SHOW SELECTED FEATURE INFO
        switch(SelectedFeature){
            case InfoCollector.MATH_ROUND_INDEX:
                lnlyMathRound.setVisibility(View.VISIBLE);
                break;
            case InfoCollector.MATH_STAGE_INDEX:
                lnlyMathStage.setVisibility(View.VISIBLE);
                break;
            case InfoCollector.MATH_PRACTICE_INDEX:
                lnlyMathPractice.setVisibility(View.VISIBLE);
                break;
            case InfoCollector.MATH_EXERCISE_INDEX:
                lnlyMathExercise.setVisibility(View.VISIBLE);
                break;
            default:
                lnlyMathStage.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void showComeBackFragment(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new AppSettingFragment())
                .commit();
    }

    private void startMathStage(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathStageTransferFragment())
                .commit();
    }

    private void startMathRound(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathRoundStartingFragment())
                .commit();
    }

    private void startMathExercise(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathExerciseStartingFragment())
                .commit();
    }

    private void startMathPractice(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathPracticeStartingFragment())
                .commit();
    }

    private void startMyStudy(){
        switch(SelectedFeature){
            case InfoCollector.MATH_ROUND_INDEX:
                startMathRound();
                break;
            case InfoCollector.MATH_STAGE_INDEX:
                startMathStage();
                break;
            case InfoCollector.MATH_PRACTICE_INDEX:
                startMathPractice();
                break;
            case InfoCollector.MATH_EXERCISE_INDEX:
                startMathExercise();
                break;
            default:
                startMathStage();
                break;
        }
    }
    
    private void starUp(){
        InfoCollector.setCurrentFragment(this);
        setAppTitle();
        SelectedFeature = InfoCollector.getUserSelectedFeature();
        showInfo();
        clearSelection();
    }

    public MyStudyInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_my_study_info, container, false);

        //  GET REFERENCES
        lnlyMathRound = rootView.findViewById(R.id.lnlyMathRound_MyStudyInfoFragment);
        lnlyMathStage = rootView.findViewById(R.id.lnlyMathStage_MyStudyInfoFragment);
        lnlyMathPractice = rootView.findViewById(R.id.lnlyMathPractice_MyStudyInfoFragment);
        lnlyMathExercise = rootView.findViewById(R.id.lnlyMathExercise_MyStudyInfoFragment);        
        
        btnBack = rootView.findViewById(R.id.btnPrevious_MyStudyInfoFragment);
        btnNext = rootView.findViewById(R.id.btnNext_MyStudyInfoFragment);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnBack.setOnClickListener(ButtonListener);
        btnNext.setOnClickListener(ButtonListener);

        //  START UP
        starUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            setSelection((Button) v);

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnPrevious_MyStudyInfoFragment:
                    showComeBackFragment();
                    break;
                case R.id.btnNext_MyStudyInfoFragment:
                    startMyStudy();
                    break;
            }
        }
    }

}
