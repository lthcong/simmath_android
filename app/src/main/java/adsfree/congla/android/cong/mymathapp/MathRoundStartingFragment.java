package adsfree.congla.android.cong.mymathapp;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Objects;

import layout.MathRoundQuestionFragment;
import pckInfo.InfoCollector;


/**
 * A simple {@link Fragment} subclass.
 */
public class MathRoundStartingFragment extends Fragment {

    private String StartingMessage = "Ready? Math Round is starting ...";
    private TextView tvStartMessage;
    private CountDownTimer cdtCountDownTime;
    private int StartIndex = 0,
            CountDownInterval = 100,
            CountDownTime = 0,
            AddTime = 5;

    private void setAppTitle(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathRound));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void showStartingMessage(){
        tvStartMessage.setText("");

        final int Length = StartingMessage.length();
        CountDownTime = (Length + AddTime) * CountDownInterval;
        StartIndex = 0;
        cdtCountDownTime = new CountDownTimer(CountDownTime, CountDownInterval) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (StartIndex < Length) {
                    tvStartMessage.append(String.valueOf(StartingMessage.charAt(StartIndex)));
                }

                StartIndex++;
            }

            @Override
            public void onFinish() {
                showMathRound();
            }
        }.start();
    }
    
    private void showMathRound(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathRoundQuestionFragment())
                .commit();
    }
    
    private void startup(){
        InfoCollector.setCurrentFragment(this);
        setAppTitle();
        showStartingMessage();
    }

    public MathRoundStartingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_round_starting, container, false);

        //  GET REFERENCES
        tvStartMessage = rootView.findViewById(R.id.tvStartMessage_MathRoundStartingFragment);

        //  START UP
        startup();

        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

}
