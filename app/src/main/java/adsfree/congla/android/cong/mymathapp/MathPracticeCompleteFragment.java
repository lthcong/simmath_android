package adsfree.congla.android.cong.mymathapp;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

import layout.HistorySubMenuFragment;
import pckInfo.InfoCollector;


/**
 * A simple {@link Fragment} subclass.
 */
public class MathPracticeCompleteFragment extends Fragment {

    private Button btnContinueSign, btnContinue, btnShowRecordSign, btnShowRecord;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathPractice));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void setSelection(Button btnSelectedButton){
        btnSelectedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void setContinueStudySelection(){
        setSelection(btnContinueSign);
        setSelection(btnContinue);
    }

    private void setShowRecordSelection(){
        setSelection(btnShowRecordSign);
        setSelection(btnShowRecord);
    }

    private void continueStudy(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathPracticeStartingFragment());
        ft.commit();
    }

    private void showRecord(){
        InfoCollector.setMathPracticeHistorySelected();
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new HistorySubMenuFragment());
        ft.commit();
    }

    private void setupButton(){
        btnContinueSign.setTextColor(Color.GRAY);
        btnContinue.setTextColor(Color.GRAY);
        btnShowRecordSign.setTextColor(Color.GRAY);
        btnShowRecord.setTextColor(Color.GRAY);
    }

    private void startup(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        setupButton();
    }

    public MathPracticeCompleteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_practice_complete, container, false);

        //  GET REFERENCES
        btnContinueSign = rootView.findViewById(R.id.btnContinueStudySign_MathPracticeCompleteFragment);
        btnContinue = rootView.findViewById(R.id.btnContinueStudy_MathPracticeCompleteFragment);
        btnShowRecordSign = rootView.findViewById(R.id.btnShowMyRecordSign_MathPracticeCompleteFragment);
        btnShowRecord = rootView.findViewById(R.id.btnShowMyRecord_MathPracticeCompleteFragment);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnContinueSign.setOnClickListener(ButtonListener);
        btnContinue.setOnClickListener(ButtonListener);
        btnShowRecordSign.setOnClickListener(ButtonListener);
        btnShowRecord.setOnClickListener(ButtonListener);

        //  START UP
        startup();
        
        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnContinueStudySign_MathPracticeCompleteFragment:
                    setContinueStudySelection();
                    InfoCollector.setupMathPractice();
                    continueStudy();
                    break;
                case R.id.btnContinueStudy_MathPracticeCompleteFragment:
                    setContinueStudySelection();
                    InfoCollector.setupMathPractice();
                    continueStudy();
                    break;
                case R.id.btnShowMyRecordSign_MathPracticeCompleteFragment:
                    setShowRecordSelection();
                    showRecord();
                    break;
                case R.id.btnShowMyRecord_MathPracticeCompleteFragment:
                    setShowRecordSelection();
                    showRecord();
                    break;
            }
        }
    }

}
