package adsfree.congla.android.cong.mymathapp;


import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import layout.AssessmentTestFragment;
import layout.MathRoundTopicSelecionFragment;
import layout.MathStageTopicSelectionFragment;
import pckData.pckFirebase.LogEvents;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckString.StringUtility;


/**
 * A simple {@link Fragment} subclass.
 */
public class UserInfoCollectionFragment extends Fragment {

    private TextView tvUserNameError;
    private EditText edtUserName;
    private Button  btnMathStage, btnMathRound, btnMathPractice, btnMathExercise,
            btnTakeAssessmentTestSign, btnTakeAssessmentTest,
            btnSelectStartingLevelSign, btnSelectStartingLevel,
            btnShowMePrivacyPolicySign, btnShowMePrivacyPolicy,
            btnExploreNewFeatureSign, btnExploreNewFeature;
    private TextView  tvMathStage, tvMathStageInfo,
            tvMathRound, tvMathRoundInfo,
            tvMathPractice, tvMathPracticeInfo, 
            tvMathExercise, tvMathExerciseInfo;
    private String UserName = "";

    private void setSelection(Button btnSelectedButton){
        btnSelectedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void clearSelection(Button btnSelectedButton){
        btnSelectedButton.setTextColor(Color.GRAY);
    }

    private void logEvents(){
        LogEvents.writeToLog(this.getActivity(), "NewUserRegistered", "NewUserRegistered", "NewUserRegistered");
    }

    private void showUserName(){
        edtUserName.setText(InfoCollector.getUserName());
    }

    private boolean getUserName(){
        boolean isAvailable;
        UserName = edtUserName.getText().toString().trim();

        if (UserName.length() > 0){
            if (StringUtility.isAvailableString(UserName)){
                isAvailable = true;
                hideInvalidUserNameError();
            }
            else {
                isAvailable = false;
                showInvalidUserNameError();
            }
        }
        else {
            isAvailable = false;
            showInvalidUserNameError();
        }

        return isAvailable;
    }

    private void showInvalidUserNameError(){
        edtUserName.requestFocus();
        tvUserNameError.setVisibility(View.VISIBLE);
    }

    private void hideInvalidUserNameError(){
        tvUserNameError.setVisibility(View.GONE);
    }

    private void setAssessmentTestSelection(){
        setSelection(btnTakeAssessmentTestSign);
        setSelection(btnTakeAssessmentTest);
    }

    private void showAssessmentTest(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new AssessmentTestFragment());
        ft.commit();
    }

    private void showMathRoundTopicSelection(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathRoundTopicSelecionFragment());
        ft.commit();
    }

    private void showMathStageTopicSelection(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathStageTopicSelectionFragment());
        ft.commit();
    }

    private void showMathPracticeTopicSelection(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathPracticeTopicSelectionFragment());
        ft.commit();
    }

    private void showMathExerciseTopicSelection(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathExerciseTopicSelection());
        ft.commit();
    }

    private void setStartingLevelSelection(){
        setSelection(btnSelectStartingLevel);
        setSelection(btnSelectStartingLevelSign);
    }
    
    private void showStartingLevelSelection(){
        switch (InfoCollector.getUserSelectedFeature()){
            case InfoCollector.MATH_ROUND_INDEX:
                showMathRoundTopicSelection();
                break;
            case InfoCollector.MATH_STAGE_INDEX:
                showMathStageTopicSelection();
                break;
            case InfoCollector.MATH_PRACTICE_INDEX:
                showMathPracticeTopicSelection();
                break;
            case InfoCollector.MATH_EXERCISE_INDEX:
                showMathExerciseTopicSelection();
                break;
            default:
                showMathStageTopicSelection();
                break;
        }
    }
    
    private void showMyStudyInfo(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MyStudyInfoFragment())
                .commit();
    }
    
    private void clearMainFeatureSelection(){
        btnMathRound.setTextColor(Color.GRAY);
        tvMathRound.setTextColor(Color.GRAY);
        tvMathRoundInfo.setVisibility(View.GONE);
        
        btnMathStage.setTextColor(Color.GRAY);
        tvMathStage.setTextColor(Color.GRAY);
        tvMathStageInfo.setVisibility(View.GONE);
        
        btnMathPractice.setTextColor(Color.GRAY);
        tvMathPractice.setTextColor(Color.GRAY);
        tvMathPracticeInfo.setVisibility(View.GONE);
        
        btnMathExercise.setTextColor(Color.GRAY);
        tvMathExercise.setTextColor(Color.GRAY);
        tvMathExerciseInfo.setVisibility(View.GONE);
    }

    private void showCurrentMainFeature(){
        //  CLEAR SELECTION
        clearMainFeatureSelection();

        //  SET SELECTION
        switch (InfoCollector.getUserSelectedFeature()){
            case InfoCollector.MATH_ROUND_INDEX:
                btnMathRound.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                tvMathRound.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                tvMathRoundInfo.setVisibility(View.VISIBLE);
                break;
            case InfoCollector.MATH_STAGE_INDEX:
                btnMathStage.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                tvMathStage.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                tvMathStageInfo.setVisibility(View.VISIBLE);
                break;
            case InfoCollector.MATH_PRACTICE_INDEX:
                btnMathPractice.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                tvMathPractice.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                tvMathPracticeInfo.setVisibility(View.VISIBLE);
                break;
            case InfoCollector.MATH_EXERCISE_INDEX:
                btnMathExercise.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                tvMathExercise.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                tvMathExerciseInfo.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void setShowPrivacyPolicySelection(){
        setSelection(btnShowMePrivacyPolicySign);
        setSelection(btnShowMePrivacyPolicy);
    }

    private void showPrivacyPolicy(){
        Intent PrivacyLinkIntent = new Intent(Intent.ACTION_VIEW);
        PrivacyLinkIntent.setData(Uri.parse(InfoCollector.PRIVACY_POLICY_LINK));
        startActivity(PrivacyLinkIntent);
    }

    private void setNewFeatureSelection(){
        setSelection(btnExploreNewFeatureSign);
        setSelection(btnExploreNewFeature);
    }

    private void showNewFeature(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new WhatsNewFragment())
                .commit();
    }

    private void setupUI(){
        tvUserNameError.setVisibility(View.GONE);

        clearSelection(btnTakeAssessmentTestSign);
        clearSelection(btnTakeAssessmentTest);
        clearSelection(btnSelectStartingLevelSign);
        clearSelection(btnSelectStartingLevel);
        clearSelection(btnShowMePrivacyPolicySign);
        clearSelection(btnShowMePrivacyPolicy);
        clearSelection(btnExploreNewFeatureSign);
        clearSelection(btnExploreNewFeature);
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        showUserName();
        showCurrentMainFeature();
        setupUI();
    }

    public UserInfoCollectionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_user_info_collection, container, false);

        //  GET REFERENCES
        tvUserNameError = rootView.findViewById(R.id.tvEnterUserNameError_UserInfoCollectionFragment);
        edtUserName = rootView.findViewById(R.id.edtEnterYourName_UserInfoCollectionFragment);

        btnMathRound = rootView.findViewById(R.id.btnMathRound_UserInfoCollectionFragment);
        tvMathRound = rootView.findViewById(R.id.tvMathRound_UserInfoCollectionFragment);
        tvMathRoundInfo = rootView.findViewById(R.id.tvMathRoundInfo_UserInfoCollectionFragment);
        
        btnMathStage = rootView.findViewById(R.id.btnMathStage_UserInfoCollectionFragment);
        tvMathStage = rootView.findViewById(R.id.tvMathStage_UserInfoCollectionFragment);
        tvMathStageInfo = rootView.findViewById(R.id.tvMathStageInfo_UserInfoCollectionFragment);
        
        btnMathPractice = rootView.findViewById(R.id.btnMathPractice_UserInfoCollectionFragment);
        tvMathPractice = rootView.findViewById(R.id.tvMathPractice_UserInfoCollectionFragment);
        tvMathPracticeInfo = rootView.findViewById(R.id.tvMathPracticeInfo_UserInfoCollectionFragment);
        
        btnMathExercise = rootView.findViewById(R.id.btnMathExercise_UserInfoCollectionFragment);
        tvMathExercise = rootView.findViewById(R.id.tvMathExercise_UserInfoCollectionFragment);
        tvMathExerciseInfo = rootView.findViewById(R.id.tvMathExerciseInfo_UserInfoCollectionFragment);

        btnTakeAssessmentTestSign = rootView.findViewById(R.id.btnTakeTheAssessmentTestSign_UserInfoCollectionFragment);
        btnTakeAssessmentTest = rootView.findViewById(R.id.btnTakeTheAssessmentTest_UserInfoCollectionFragment);
        btnSelectStartingLevelSign = rootView.findViewById(R.id.btnSelectMyStartingLevelSign_UserInfoCollectionFragment);
        btnSelectStartingLevel = rootView.findViewById(R.id.btnSelectMyStartingLevel_UserInfoCollectionFragment);
        btnShowMePrivacyPolicySign = rootView.findViewById(R.id.btnShowMePrivacyPolicySign_UserInfoCollectionFragment);
        btnShowMePrivacyPolicy = rootView.findViewById(R.id.btnShowMePrivacyPolicy_UserInfoCollectionFragment);
        btnExploreNewFeatureSign = rootView.findViewById(R.id.btnExploreNewFeatureSign_UserInfoCollectionFragment);
        btnExploreNewFeature = rootView.findViewById(R.id.btnExploreNewFeature_UserInfoCollectionFragment);

        //  SET LISTENER
        UserChangeListener ChangeListener = new UserChangeListener();
        edtUserName.addTextChangedListener(ChangeListener);

        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnTakeAssessmentTestSign.setOnClickListener(ButtonListener);
        btnTakeAssessmentTest.setOnClickListener(ButtonListener);
        btnSelectStartingLevelSign.setOnClickListener(ButtonListener);
        btnSelectStartingLevel.setOnClickListener(ButtonListener);
        btnShowMePrivacyPolicySign.setOnClickListener(ButtonListener);
        btnShowMePrivacyPolicy.setOnClickListener(ButtonListener);
        btnExploreNewFeatureSign.setOnClickListener(ButtonListener);
        btnExploreNewFeature.setOnClickListener(ButtonListener);

        MainFeatureSelectionListener MainFeatureSelection = new MainFeatureSelectionListener();
        btnMathRound.setOnClickListener(MainFeatureSelection);
        tvMathRound.setOnClickListener(MainFeatureSelection);
        tvMathRoundInfo.setOnClickListener(MainFeatureSelection);
        
        btnMathStage.setOnClickListener(MainFeatureSelection);
        tvMathStage.setOnClickListener(MainFeatureSelection);
        tvMathStageInfo.setOnClickListener(MainFeatureSelection);
        
        btnMathPractice.setOnClickListener(MainFeatureSelection);
        tvMathPractice.setOnClickListener(MainFeatureSelection);
        tvMathPracticeInfo.setOnClickListener(MainFeatureSelection);
        
        btnMathExercise.setOnClickListener(MainFeatureSelection);
        tvMathExercise.setOnClickListener(MainFeatureSelection);
        tvMathExerciseInfo.setOnClickListener(MainFeatureSelection);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnTakeTheAssessmentTestSign_UserInfoCollectionFragment:
                    setAssessmentTestSelection();
                    if (getUserName()){
                        showAssessmentTest();
                    }
                    else {
                        clearSelection((Button) v);
                    }
                    break;
                case R.id.btnTakeTheAssessmentTest_UserInfoCollectionFragment:
                    setAssessmentTestSelection();
                    if (getUserName()){
                        showAssessmentTest();
                    }
                    else {
                        clearSelection((Button) v);
                    }
                    break;
                case R.id.btnSelectMyStartingLevelSign_UserInfoCollectionFragment:
                    setStartingLevelSelection();
                    if (getUserName()){
                        showStartingLevelSelection();
                    }
                    else {
                        clearSelection((Button) v);
                    }
                    break;
                case R.id.btnSelectMyStartingLevel_UserInfoCollectionFragment:
                    setStartingLevelSelection();
                    if (getUserName()){
                        showStartingLevelSelection();
                    }
                    else {
                        clearSelection((Button) v);
                    }
                    break;
                case R.id.btnShowMePrivacyPolicySign_UserInfoCollectionFragment:
                    setShowPrivacyPolicySelection();
                    showPrivacyPolicy();
                    break;
                case R.id.btnShowMePrivacyPolicy_UserInfoCollectionFragment:
                    setShowPrivacyPolicySelection();
                    showPrivacyPolicy();
                    break;
                case R.id.btnExploreNewFeatureSign_UserInfoCollectionFragment:
                    setNewFeatureSelection();
                    showNewFeature();
                    break;
                case R.id.btnExploreNewFeature_UserInfoCollectionFragment:
                    setNewFeatureSelection();
                    showNewFeature();
                    break;
            }
        }
    }

    private class MainFeatureSelectionListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnMathRound_UserInfoCollectionFragment:
                    DatabaseFile.updateUserSelectedFeature(InfoCollector.MATH_ROUND_INDEX);
                    InfoCollector.getUserInfo();
                    InfoCollector.setupApp();
                    showCurrentMainFeature();
                    break;
                case R.id.tvMathRound_UserInfoCollectionFragment:
                    DatabaseFile.updateUserSelectedFeature(InfoCollector.MATH_ROUND_INDEX);
                    InfoCollector.getUserInfo();
                    InfoCollector.setupApp();
                    showCurrentMainFeature();
                    break;
                case R.id.tvMathRoundInfo_UserInfoCollectionFragment:
                    showMyStudyInfo();
                    break;
                case R.id.btnMathStage_UserInfoCollectionFragment:
                    DatabaseFile.updateUserSelectedFeature(InfoCollector.MATH_STAGE_INDEX);
                    InfoCollector.getUserInfo();
                    InfoCollector.setupApp();
                    showCurrentMainFeature();
                    break;
                case R.id.tvMathStage_UserInfoCollectionFragment:
                    DatabaseFile.updateUserSelectedFeature(InfoCollector.MATH_STAGE_INDEX);
                    InfoCollector.getUserInfo();
                    InfoCollector.setupApp();
                    showCurrentMainFeature();
                    break;
                case R.id.tvMathStageInfo_UserInfoCollectionFragment:
                    showMyStudyInfo();
                    break;
                case R.id.btnMathPractice_UserInfoCollectionFragment:
                    DatabaseFile.updateUserSelectedFeature(InfoCollector.MATH_PRACTICE_INDEX);
                    InfoCollector.getUserInfo();
                    InfoCollector.setupApp();
                    showCurrentMainFeature();
                    break;
                case R.id.tvMathPractice_UserInfoCollectionFragment:
                    DatabaseFile.updateUserSelectedFeature(InfoCollector.MATH_PRACTICE_INDEX);
                    InfoCollector.getUserInfo();
                    InfoCollector.setupApp();
                    showCurrentMainFeature();
                    break;
                case R.id.tvMathPracticeInfo_UserInfoCollectionFragment:
                    showMyStudyInfo();
                    break;
                case R.id.btnMathExercise_UserInfoCollectionFragment:
                    DatabaseFile.updateUserSelectedFeature(InfoCollector.MATH_EXERCISE_INDEX);
                    InfoCollector.getUserInfo();
                    InfoCollector.setupApp();
                    showCurrentMainFeature();
                    break;
                case R.id.tvMathExercise_UserInfoCollectionFragment:
                    DatabaseFile.updateUserSelectedFeature(InfoCollector.MATH_EXERCISE_INDEX);
                    InfoCollector.getUserInfo();
                    InfoCollector.setupApp();
                    showCurrentMainFeature();
                    break;
                case R.id.tvMathExerciseInfo_UserInfoCollectionFragment:
                    showMyStudyInfo();
                    break;
            }
        }
    }

    private class UserChangeListener implements TextWatcher{

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (getUserName()){
                DatabaseFile.updateUserName(UserName);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            logEvents();
        }
    }

}
