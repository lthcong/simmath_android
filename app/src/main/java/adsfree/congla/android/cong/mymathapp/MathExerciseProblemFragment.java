package adsfree.congla.android.cong.mymathapp;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckMathTest.MathQuestion;
import pckMath.pckMathTopics.Topic;
import pckString.StringUtility;


/**
 * A simple {@link Fragment} subclass.
 */
public class MathExerciseProblemFragment extends Fragment {

    private TextView tvTopicName,
            tvScore,
            tvQuestionNumber,
            tvQuestionContent,
            tvResult,
            tvAnswerA, tvAnswerB, tvAnswerC, tvAnswerD;
    private Button btnCheck, btnNext;
    private Topic CurrentTopic;
    private MathQuestion Question;
    private String YourAnswer, Result;
    private String MathExerciseID, ExListID;
    private int QuestionNumber = 1, NoRightAnswer = 0,
            RequiredNoQuestion = InfoCollector.MIN_EXERCISE_PASSING_NO_QUESTION;
    private double Score = 0.0,
                    PassingScore = InfoCollector.MIN_EXERCISE_PASSING_SCORE;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathExercise));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void showTopicName(){
        String TopicName = "<b>Topic: </b>" + CurrentTopic.getName();
        StringUtility.writeString(tvTopicName, TopicName);
    }

    private void showScore(){
        StringUtility.writeString(tvScore, String.valueOf(Score));
    }

    private void showQuestionNumber(){
        String StQuestionNumber = "Question " + String.valueOf(QuestionNumber) + ": ";
        StringUtility.writeString(tvQuestionNumber, StQuestionNumber);
    }

    private void showQuestionContent(){
        //  GET QUESTION
        Question = new MathQuestion(CurrentTopic.getProblem());

        //  SHOW QUESTION
        StringUtility.writeString(tvQuestionContent, Question.getQuestion());

        //  HIDE RESULT
        tvResult.setVisibility(View.GONE);

        //  ENABLE ANSWER SELECTION
        enableAnswerSelection();

        //  ANSWER A
        String StAnswerA = getResources().getString(R.string.stCircledA) + ": " + Question.getAnswerA();
        StringUtility.writeString(tvAnswerA, StAnswerA);
        tvAnswerA.setTextColor(Color.GRAY);

        //  ANSWER B
        String StAnswerB = getResources().getString(R.string.stCircledB) + ": " + Question.getAnswerB();
        StringUtility.writeString(tvAnswerB, StAnswerB);
        tvAnswerB.setTextColor(Color.GRAY);

        //  ANSWER C
        String StAnswerC = getResources().getString(R.string.stCircledC) + ": " + Question.getAnswerC();
        StringUtility.writeString(tvAnswerC, StAnswerC);
        tvAnswerC.setTextColor(Color.GRAY);

        //  ANSWER D
        String StAnswerD = getResources().getString(R.string.stCircledD) + ": " + Question.getAnswerD();
        StringUtility.writeString(tvAnswerD, StAnswerD);
        tvAnswerD.setTextColor(Color.GRAY);

        //  SHOW CHECK BUTTON
        btnCheck.setVisibility(View.VISIBLE);

        //  HIDE NEXT BUTTON
        btnNext.setVisibility(View.GONE);
    }

    private void showQuestion(){
        showScore();
        showQuestionNumber();
        showQuestionContent();
    }

    private void showNewQuestion(){
        QuestionNumber++;
        showQuestion();
    }

    private void disableAnswerSelection(){
        tvAnswerA.setEnabled(false);
        tvAnswerB.setEnabled(false);
        tvAnswerC.setEnabled(false);
        tvAnswerD.setEnabled(false);
    }

    private void enableAnswerSelection(){
        tvAnswerA.setEnabled(true);
        tvAnswerB.setEnabled(true);
        tvAnswerC.setEnabled(true);
        tvAnswerD.setEnabled(true);
    }

    private void clearAnswerSelection(){
        tvAnswerA.setTextColor(Color.GRAY);
        tvAnswerB.setTextColor(Color.GRAY);
        tvAnswerC.setTextColor(Color.GRAY);
        tvAnswerD.setTextColor(Color.GRAY);
    }

    private String getCorrectAnswerMessage(){
        String CorrectAnswer;

        switch (MathUtility.getRandomPositiveNumber_2Digit() % 6){
            case 0:
                CorrectAnswer = "GREAT JOB.";
                break;
            case 1:
                CorrectAnswer = "WONDERFUL.";
                break;
            case 2:
                CorrectAnswer = "FANTASTIC.";
                break;
            case 3:
                CorrectAnswer = "NICE ANSWER.";
                break;
            case 4:
                CorrectAnswer = "GOOD WORK.";
                break;
            default:
                CorrectAnswer = "CORRECT.";
                break;
        }

        return CorrectAnswer;
    }

    private void getYourAnswer(TextView tvSelectedAnswer){
        //  CLEAR SELECTION
        clearAnswerSelection();

        //  SET SELECTION
        tvSelectedAnswer.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        //  GET YOUR ANSWER
        String SelectedAnswer = tvSelectedAnswer.getText().toString().trim();
        if (SelectedAnswer.contains(getResources().getString(R.string.stCircledA))) {
            YourAnswer = Question.getAnswerA();
        }
        else {
            if (SelectedAnswer.contains(getResources().getString(R.string.stCircledB))) {
                YourAnswer = Question.getAnswerB();
            }
            else {
                if (SelectedAnswer.contains(getResources().getString(R.string.stCircledC))) {
                    YourAnswer = Question.getAnswerC();
                }
                else {
                    YourAnswer = Question.getAnswerD();
                }
            }
        }

        //  SAVE ANSWER
        Question.setYourAnswer(YourAnswer);
    }

    private void updateScore(){
        Score = ((double) NoRightAnswer) / ((double) QuestionNumber);
        Score = MathUtility.round_1_Decimal(Score);
    }

    private void checkAnswer(){
        //  DISABLE ANSWER SELECTION
        disableAnswerSelection();

        //  CHECK ANSWER SELECTION
        if (YourAnswer.equalsIgnoreCase(Question.getRightAnswer())){
            NoRightAnswer++;

            tvResult.setVisibility(View.VISIBLE);
            tvResult.setText(getCorrectAnswerMessage());
            tvResult.setTextColor(Color.GREEN);

            Result = "Right";
            Question.setResult(true);
        }
        else {
            String IncorrectAnswer = "NOT CORRECT. THE CORRECT ANSWER IS " + Question.getRightAnswer().toUpperCase();

            tvResult.setVisibility(View.VISIBLE);
            tvResult.setText(IncorrectAnswer);
            tvResult.setTextColor(Color.RED);

            Result = "Wrong";
            Question.setResult(false);
        }

        //  UPDATE SCORE
        updateScore();

        //  SAVE INFO
        saveInfo();

        //  HIDE CHECK BUTTON
        btnCheck.setVisibility(View.GONE);

        //  SHOW NEXT BUTTON
        btnNext.setVisibility(View.VISIBLE);
    }

    private void saveInfo(){
        //  UPDATE MATH EXERCISE INFO
        DatabaseFile.updateExerciseList(ExListID, String.valueOf(QuestionNumber), String.valueOf(Score));

        //  SAVE MATH PROBLEM
        DatabaseFile.addToMathProblemTable(MathExerciseID,
                Question.getQuestion(),
                Question.getYourAnswer(),
                Question.getRightAnswer(),
                Result);
    }

    private void showSectionFragment(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathExerciseSectionFragment())
                .commit();
    }

    private void startup(){
        setupAppTile();

        InfoCollector.setCurrentFragment(this);
        //  GET INFO
        MathExerciseID = InfoCollector.getMathExerciseID();
        ExListID = InfoCollector.getMathExerciseListID();

        CurrentTopic = InfoCollector.getMathExerciseTopic();
        Score = InfoCollector.getMathExerciseScore();
        QuestionNumber = InfoCollector.getExerciseListNoQuestion();

        //  SHOW INFO
        showTopicName();

        //  SHOW QUESTION
        showQuestion();
    }

    public MathExerciseProblemFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_exercise_problem, container, false);

        //  GET REFERENCES
        tvTopicName = rootView.findViewById(R.id.tvTopicName_MathExerciseProblemFragment);
        tvScore = rootView.findViewById(R.id.tvScore_MathExerciseProblemFragment);
        tvQuestionNumber = rootView.findViewById(R.id.tvQuestionNumber_MathExerciseProblemFragment);
        tvQuestionContent = rootView.findViewById(R.id.tvQuestionContent_MathExerciseProblemFragment);
        tvResult = rootView.findViewById(R.id.tvAnswerResult_MathExerciseProblemFragment);

        tvAnswerA = rootView.findViewById(R.id.tvAnswerA_MathExerciseProblemFragment);
        tvAnswerA.setTextColor(Color.GRAY);

        tvAnswerB = rootView.findViewById(R.id.tvAnswerB_MathExerciseProblemFragment);
        tvAnswerB.setTextColor(Color.GRAY);

        tvAnswerC = rootView.findViewById(R.id.tvAnswerC_MathExerciseProblemFragment);
        tvAnswerC.setTextColor(Color.GRAY);

        tvAnswerD = rootView.findViewById(R.id.tvAnswerD_MathExerciseProblemFragment);
        tvAnswerD.setTextColor(Color.GRAY);

        btnCheck = rootView.findViewById(R.id.btnCheck_MathExerciseProblemFragment);
        btnCheck.setTextColor(Color.GRAY);

        btnNext =rootView.findViewById(R.id.btnNext_MathExerciseProblemFragment);
        btnNext.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        tvAnswerA.setOnClickListener(ButtonListener);
        tvAnswerB.setOnClickListener(ButtonListener);
        tvAnswerC.setOnClickListener(ButtonListener);
        tvAnswerD.setOnClickListener(ButtonListener);
        btnCheck.setOnClickListener(ButtonListener);
        btnNext.setOnClickListener(ButtonListener);

        //  START UP
        startup();
        
        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tvAnswerA_MathExerciseProblemFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.tvAnswerB_MathExerciseProblemFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.tvAnswerC_MathExerciseProblemFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.tvAnswerD_MathExerciseProblemFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.btnCheck_MathExerciseProblemFragment:
                    checkAnswer();
                    break;
                case R.id.btnNext_MathExerciseProblemFragment:
                    if ((Score >= PassingScore) && (QuestionNumber >= RequiredNoQuestion)){
                        //  COMPLETE
                        InfoCollector.setMathExerciseSection(2);
                        showSectionFragment();
                    }
                    else {
                        //  CONTINUE
                        showNewQuestion();
                    }
                    break;
            }
        }
    }
    
}
