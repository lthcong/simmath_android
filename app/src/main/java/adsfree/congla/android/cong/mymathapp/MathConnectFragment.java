package adsfree.congla.android.cong.mymathapp;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

import pckInfo.InfoCollector;


/**
 * A simple {@link Fragment} subclass.
 */
public class MathConnectFragment extends Fragment {

    private Button btnNewAssignmentSign, btnAssignmentListSign,
                    btnNewAssignment, btnAssignmentList;

    private void setAppTitle(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathConnect));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void clearButtonSelection(){
        btnNewAssignmentSign.setTextColor(Color.GRAY);
        btnNewAssignment.setTextColor(Color.GRAY);
        btnAssignmentListSign.setTextColor(Color.GRAY);
        btnAssignmentList.setTextColor(Color.GRAY);
    }

    private void setNewAssignmentSelection(){
        clearButtonSelection();

        btnNewAssignmentSign.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnNewAssignment.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showNewAssignment(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new CreateNewAssignmentFragment());
        ft.commit();
    }

    private void setAssignmentListSelection(){
        clearButtonSelection();

        btnAssignmentListSign.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnAssignmentList.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showAssignmentList(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new AssignmentListFragment());
        ft.commit();
    }

    private void startUp(){
        setAppTitle();
        InfoCollector.setCurrentFragment(this);
        clearButtonSelection();
    }

    public MathConnectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_connect, container, false);

        //  GET REFERENCES
        btnNewAssignmentSign = rootView.findViewById(R.id.btnNewAssignmentSign_MathConnectFragment);
        btnNewAssignment = rootView.findViewById(R.id.btnNewAssignment_MathConnectFragment);
        btnAssignmentListSign = rootView.findViewById(R.id.btnAssignmentListSign_MathConnectFragment);
        btnAssignmentList = rootView.findViewById(R.id.btnAssignmentList_MathConnectFragment);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnNewAssignmentSign.setOnClickListener(ButtonListener);
        btnNewAssignment.setOnClickListener(ButtonListener);
        btnAssignmentListSign.setOnClickListener(ButtonListener);
        btnAssignmentList.setOnClickListener(ButtonListener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btnNewAssignmentSign_MathConnectFragment:
                    setNewAssignmentSelection();
                    showNewAssignment();
                    break;
                case R.id.btnNewAssignment_MathConnectFragment:
                    setNewAssignmentSelection();
                    showNewAssignment();
                    break;
                case R.id.btnAssignmentListSign_MathConnectFragment:
                    setAssignmentListSelection();
                    showAssignmentList();
                    break;
                case R.id.btnAssignmentList_MathConnectFragment:
                    setAssignmentListSelection();
                    showAssignmentList();
                    break;
            }
        }
    }

}
