package adsfree.congla.android.cong.mymathapp;


import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import layout.BlankFragment;
import layout.ProblemDetailFragment;
import pckAds.Ads;
import pckData.pckFirebase.LogEvents;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckString.StringUtility;


/**
 * A simple {@link Fragment} subclass.
 */
public class MathPracticeHistoryFragment extends Fragment {

    private LinearLayout lnlyMathPracticeHistory;
    private Button btnMoreHistory;
    private EditText edtSearchHistory;
    private Cursor MathPracticeHistory;
    private int NoShowedHistory = InfoCollector.getNoShowedTopic();
    private ArrayList<String> IDCollection;

    private void setAppTitle(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathPracticeHistory));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void logMathPracticeHistoryView(){
        LogEvents.writeToLog(this.getActivity(), "ViewHistory", "MathPracticeHistory", "MathPracticeHistory");
    }

    private void showClearHistoryButton(){
        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.lnlybtnClearHistoryContainer_MathPracticeHistoryFragment, new ButtonClearHistoryFragment())
                .commit();
    }

    private void showHistory(){
        int ShowedHistory = 0, IDIndex = 0, Count = 0, AdsStep = Ads.WRAPPED_ADS_STEP;

        lnlyMathPracticeHistory.removeAllViews();
        IDCollection = new ArrayList<>();

        if (MathPracticeHistory.getCount() > 0){
            while(MathPracticeHistory.moveToNext()){
                LinearLayout.LayoutParams MathPracticeLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                LinearLayout lnlyMathPracticeLayout = new LinearLayout(this.getActivity());
                lnlyMathPracticeLayout.setLayoutParams(MathPracticeLayoutParams);
                lnlyMathPracticeLayout.setOrientation(LinearLayout.VERTICAL);

                //  ADS
                Count++;
                if (Count % AdsStep == 0){
                    Ads.showWrappedAds(this.getActivity(), lnlyMathPracticeHistory);
                }

                TextView tvDate = new TextView(this.getActivity());
                tvDate.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvDate.setBackgroundColor(Color.WHITE);
                tvDate.setGravity(Gravity.CENTER|Gravity.START);
                tvDate.setMinHeight(150);
                tvDate.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                TextView tvTopic = new TextView(this.getActivity());
                tvTopic.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvTopic.setBackgroundColor(Color.WHITE);
                tvTopic.setGravity(Gravity.CENTER|Gravity.START);
                tvTopic.setMinHeight(150);
                tvTopic.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                TextView tvScore = new TextView(this.getActivity());
                tvScore.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvScore.setBackgroundColor(Color.WHITE);
                tvScore.setGravity(Gravity.CENTER|Gravity.START);
                tvScore.setMinHeight(150);
                tvScore.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                TextView tvNoQuestion = new TextView(this.getActivity());
                tvNoQuestion.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvNoQuestion.setBackgroundColor(Color.WHITE);
                tvNoQuestion.setGravity(Gravity.CENTER|Gravity.START);
                tvNoQuestion.setMinHeight(150);
                tvNoQuestion.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                Button btnShowDetail = new Button(this.getActivity());
                btnShowDetail.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                btnShowDetail.setBackgroundColor(Color.WHITE);
                btnShowDetail.setGravity(Gravity.CENTER|Gravity.START);
                btnShowDetail.setMinimumHeight(100);
                btnShowDetail.setMinimumWidth(350);
                btnShowDetail.setTextColor(Color.GRAY);

                //  ID
                IDCollection.add(MathPracticeHistory.getString(0));

                //  DATE
                String StDate = "<b>Date: </b>" + MathPracticeHistory.getString(1);
                StringUtility.writeString(tvDate, StDate);
                lnlyMathPracticeLayout.addView(tvDate);

                //  TOPICS
                String StTopic = "<b>Topic(s): </b>" + MathPracticeHistory.getString(2);
                StringUtility.writeString(tvTopic, StTopic);
                lnlyMathPracticeLayout.addView(tvTopic);

                //  SCORE
                String StScore = "<b>Score: </b>" + MathPracticeHistory.getString(3);
                StringUtility.writeString(tvScore, StScore);
                lnlyMathPracticeLayout.addView(tvScore);

                //  NO QUESTION
                String StNoQuestion = "<b>No Question: </b>" + MathPracticeHistory.getString(4);
                StringUtility.writeString(tvNoQuestion, StNoQuestion);
                lnlyMathPracticeLayout.addView(tvNoQuestion);

                //  SHOW DETAIL
                String StDetail = "Show Detail";
                btnShowDetail.setText(StDetail);
                btnShowDetail.setId(IDIndex);
                btnShowDetail.setOnClickListener(new ShowDetailListener());
                lnlyMathPracticeLayout.addView(btnShowDetail);
                IDIndex++;

                //  SEPARATOR
                LinearLayout.LayoutParams SeparatorParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                SeparatorParams.setMargins(0, 20, 0, 20);

                LinearLayout lnlySeparator = new LinearLayout(this.getActivity());
                lnlySeparator.setLayoutParams(SeparatorParams);
                lnlySeparator.setBackgroundColor(Color.parseColor(InfoCollector.getTextColor()));
                lnlySeparator.setMinimumHeight(5);

                //  ADD VIEW
                lnlyMathPracticeHistory.addView(lnlyMathPracticeLayout);
                lnlyMathPracticeHistory.addView(lnlySeparator);

                ShowedHistory++;
                if (ShowedHistory >= NoShowedHistory){
                    break;
                }
            }
        }
        else {
            TextView tvHistory = new TextView(this.getActivity());
            tvHistory.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            String StNoData = "<i>" + getResources().getString(R.string.stNoData) + "</i>";
            StringUtility.writeString(tvHistory, StNoData);
            tvHistory.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
            tvHistory.setTextSize(15);
            tvHistory.setGravity(Gravity.CENTER);
            tvHistory.setMinHeight(200);
            lnlyMathPracticeHistory.addView(tvHistory);
        }
    }

    private void removeSubmenu(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new BlankFragment());
        ft.commit();
    }

    private void showMathPracticeDetail(){
        //  REMOVE SUB MENU
        removeSubmenu();

        //  SHOW DETAIL
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new ProblemDetailFragment());
        ft.commit();
    }

    private void searchHistory(){
        //  SEARCH HISTORY
        String SearchInfo = edtSearchHistory.getText().toString().trim();
        if (SearchInfo.length() == 0){
            MathPracticeHistory = DatabaseFile.getMathPractice();
        }
        else {
            MathPracticeHistory = DatabaseFile.searchMathPracticeHistory(SearchInfo);
        }

        //  SHOW HISTORY
        showHistory();
    }

    private void setSelection(final Button ClickedButton){
        //  SET SELECTION
        ClickedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        //  CLEAR SELECTION
        new CountDownTimer(100, 100) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                ClickedButton.setTextColor(Color.GRAY);
            }
        }.start();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        logMathPracticeHistoryView();
        setAppTitle();

        searchHistory();
        showClearHistoryButton();
    }

    public MathPracticeHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_practice_history, container, false);

        //  GET LISTENER
        lnlyMathPracticeHistory = rootView.findViewById(R.id.lnlyHistoryContainer_MathPracticeHistoryFragment);
        edtSearchHistory = rootView.findViewById(R.id.edtSearchHistory_MathPracticeHistoryFragment);

        btnMoreHistory = rootView.findViewById(R.id.btnMoreHistory_MathPracticeHistoryFragment);
        btnMoreHistory.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener button_listener = new ButtonClickListener();
        btnMoreHistory.setOnClickListener(button_listener);

        SearchListener HistorySearch = new SearchListener();
        edtSearchHistory.addTextChangedListener(HistorySearch);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            setSelection((Button) v);

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnMoreHistory_MathPracticeHistoryFragment:
                    NoShowedHistory += 2;
                    searchHistory();
                    break;
            }
        }
    }

    private class ShowDetailListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  LINKED ID
            InfoCollector.setLinkedID(IDCollection.get(v.getId()));

            //  DETAIL
            InfoCollector.setMathPracticeHistorySelected();
            showMathPracticeDetail();
        }
    }

    private class SearchListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            searchHistory();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

}
