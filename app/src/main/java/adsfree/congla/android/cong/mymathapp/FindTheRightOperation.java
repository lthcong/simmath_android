package adsfree.congla.android.cong.mymathapp;


import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import pckAds.Ads;
import pckInfo.InfoCollector;
import pckString.StringUtility;


/**
 * A simple {@link Fragment} subclass.
 */
public class FindTheRightOperation extends Fragment {

    private Button btnAdd, btnSub, btnMul, btnDiv, btnMix, 
                btnAnswerA, btnAnswerB, btnAnswerC, btnAnswerD;
    private TextView tvQuestion, tvResult;
    private CountDownTimer cdtCountDownTime;
    private pckMath.pckMathForKid.FindTheRightOperation Problem;
    private int NoQuestion = 1;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stFindTheRightOperation));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void showAds(){
        Ads.showAds(this.getActivity(), NoQuestion);
    }

    private void setSelection(Button btnSelectedButton){
        btnSelectedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }
    
    private void clearProblemType(){
        btnAdd.setTextColor(Color.GRAY);
        btnSub.setTextColor(Color.GRAY);
        btnMul.setTextColor(Color.GRAY);
        btnDiv.setTextColor(Color.GRAY);
        btnMix.setTextColor(Color.GRAY);
    }
    
    private void clearAnswerSelection(){
        btnAnswerA.setTextColor(Color.GRAY);
        btnAnswerB.setTextColor(Color.GRAY);
        btnAnswerC.setTextColor(Color.GRAY);
        btnAnswerD.setTextColor(Color.GRAY);
    }
    
    private void showProblem(){
        Problem = new pckMath.pckMathForKid.FindTheRightOperation();

        tvQuestion.setText("");
        StringUtility.writeString(tvQuestion, Problem.getQuestion());
        
        tvResult.setVisibility(View.GONE);
        
        clearAnswerSelection();
        btnAnswerA.setText(Problem.getAnswerA());
        btnAnswerB.setText(Problem.getAnswerB());
        btnAnswerC.setText(Problem.getAnswerC());
        btnAnswerD.setText(Problem.getAnswerD());
    }

    private void showNewProblem(){
        cdtCountDownTime = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                NoQuestion++;
                showProblem();
                showAds();
            }
        }.start();
    }

    private void showAddProblem(){
        clearProblemType();
        setSelection(btnAdd);
        InfoCollector.setFindTheRightOperation(1);
        showProblem();
    }

    private void showSubProblem(){
        clearProblemType();
        setSelection(btnSub);
        InfoCollector.setFindTheRightOperation(2);
        showProblem();
    }

    private void showMulProblem(){
        clearProblemType();
        setSelection(btnMul);
        InfoCollector.setFindTheRightOperation(3);
        showProblem();
    }

    private void showDivProblem(){
        clearProblemType();
        setSelection(btnDiv);
        InfoCollector.setFindTheRightOperation(4);
        showProblem();
    }

    private void showMixProblem(){
        clearProblemType();
        setSelection(btnMix);
        InfoCollector.setFindTheRightOperation(5);
        showProblem();
    }

    private void checkAnswer(Button btnSelectedAnswer){
        //  RESULT
        String YourAnswer = btnSelectedAnswer.getText().toString().trim();
        if (YourAnswer.equalsIgnoreCase(Problem.getRightAnswer())){
            tvResult.setVisibility(View.VISIBLE);
            tvResult.setText("CORRECT");
            tvResult.setTextColor(Color.GREEN);
        }
        else {
            tvResult.setVisibility(View.VISIBLE);
            tvResult.setText("NOT CORRECT");
            tvResult.setTextColor(Color.RED);
        }

        //  NEW PROBLEM
        showNewProblem();
    }
    
    private void startup(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        showAddProblem();
    }

    public FindTheRightOperation() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_find_the_right_operation, container, false);
        
        //  GET REFERENCES
        tvQuestion = rootView.findViewById(R.id.tvQuestionContent_FindTheRightOperationFragment);
        tvResult = rootView.findViewById(R.id.tvResult_FindTheRightOperationFragment);
        btnAdd = rootView.findViewById(R.id.btnAdd_FindTheRightOperationFragment);
        btnSub = rootView.findViewById(R.id.btnSub_FindTheRightOperationFragment);
        btnMul = rootView.findViewById(R.id.btnMul_FindTheRightOperationFragment);
        btnDiv = rootView.findViewById(R.id.btnDiv_FindTheRightOperationFragment);
        btnMix = rootView.findViewById(R.id.btnMix_FindTheRightOperationFragment);
        btnAnswerA = rootView.findViewById(R.id.btnAnswerA_FindTheRightOperationFragment);
        btnAnswerB = rootView.findViewById(R.id.btnAnswerB_FindTheRightOperationFragment);
        btnAnswerC = rootView.findViewById(R.id.btnAnswerC_FindTheRightOperationFragment);
        btnAnswerD = rootView.findViewById(R.id.btnAnswerD_FindTheRightOperationFragment);
        
        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnAdd.setOnClickListener(ButtonListener);
        btnSub.setOnClickListener(ButtonListener);
        btnMul.setOnClickListener(ButtonListener);
        btnDiv.setOnClickListener(ButtonListener);
        btnMix.setOnClickListener(ButtonListener);
        btnAnswerA.setOnClickListener(ButtonListener);
        btnAnswerB.setOnClickListener(ButtonListener);
        btnAnswerC.setOnClickListener(ButtonListener);
        btnAnswerD.setOnClickListener(ButtonListener);
        
        //  START UP
        startup();
        
        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btnAdd_FindTheRightOperationFragment:
                    showAddProblem();
                    break;
                case R.id.btnSub_FindTheRightOperationFragment:
                    showSubProblem();
                    break;
                case R.id.btnMul_FindTheRightOperationFragment:
                    showMulProblem();
                    break;
                case R.id.btnDiv_FindTheRightOperationFragment:
                    showDivProblem();
                    break;
                case R.id.btnMix_FindTheRightOperationFragment:
                    showMixProblem();
                    break;
                case R.id.btnAnswerA_FindTheRightOperationFragment:
                    clearAnswerSelection();
                    setSelection((Button) v);
                    checkAnswer((Button) v);
                    break;
                case R.id.btnAnswerB_FindTheRightOperationFragment:
                    clearAnswerSelection();
                    setSelection((Button) v);
                    checkAnswer((Button) v);
                    break;
                case R.id.btnAnswerC_FindTheRightOperationFragment:
                    clearAnswerSelection();
                    setSelection((Button) v);
                    checkAnswer((Button) v);
                    break;
                case R.id.btnAnswerD_FindTheRightOperationFragment:
                    clearAnswerSelection();
                    setSelection((Button) v);
                    checkAnswer((Button) v);
                    break;
            }
        }
    }

}
