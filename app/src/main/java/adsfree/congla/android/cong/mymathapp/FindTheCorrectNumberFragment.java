package adsfree.congla.android.cong.mymathapp;


import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import pckAds.Ads;
import pckInfo.InfoCollector;
import pckMath.pckMathForKid.FindTheCorrectNumber;
import pckString.StringUtility;


/**
 * A simple {@link Fragment} subclass.
 */
public class FindTheCorrectNumberFragment extends Fragment {

    private Button btnZeroToTen, btnElevenToTwenty,
        btnFirstNumber, btnSecondNumber, btnThirdNumber, btnForthNumber;
    private TextView tvQuestion, tvResult;
    private CountDownTimer cdtCountDownTime;
    private FindTheCorrectNumber Problem;
    private int NoQuestion = 1;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stFindTheCorrectNumber));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void showAds(){
        Ads.showAds(this.getActivity(), NoQuestion);
    }

    private void showProblem(){
        Problem = new FindTheCorrectNumber();

        tvQuestion.setText("");
        StringUtility.writeString(tvQuestion, Problem.getQuestion());

        tvResult.setVisibility(View.GONE);

        clearAnswerSelection();
        btnFirstNumber.setText(Problem.getAnswerA());
        btnSecondNumber.setText(Problem.getAnswerB());
        btnThirdNumber.setText(Problem.getAnswerC());
        btnForthNumber.setText(Problem.getAnswerD());
    }

    private void showNewProblem(){
        cdtCountDownTime = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                NoQuestion++;
                showProblem();
                showAds();
            }
        }.start();
    }

    private void clearProblemType(){
        btnZeroToTen.setTextColor(Color.GRAY);
        btnElevenToTwenty.setTextColor(Color.GRAY);
    }

    private void showZeroToTenProblem(){
        clearProblemType();
        setSelection(btnZeroToTen);
        InfoCollector.setFindTheCorrectNumber(1);
        showProblem();
    }

    private void showElevenToTwentyProblem(){
        clearProblemType();
        setSelection(btnElevenToTwenty);
        InfoCollector.setFindTheCorrectNumber(2);
        showProblem();
    }

    private void setSelection(Button btnSelectedButton){
        btnSelectedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void clearAnswerSelection(){
        btnFirstNumber.setTextColor(Color.GRAY);
        btnSecondNumber.setTextColor(Color.GRAY);
        btnThirdNumber.setTextColor(Color.GRAY);
        btnForthNumber.setTextColor(Color.GRAY);
    }

    private void checkAnswer(Button btnSelectedAnswer){
        //  RESULT
        String YourAnswer = btnSelectedAnswer.getText().toString().trim();
        if (YourAnswer.equalsIgnoreCase(Problem.getRightAnswer())){
            tvResult.setVisibility(View.VISIBLE);
            tvResult.setText("CORRECT");
            tvResult.setTextColor(Color.GREEN);
        }
        else {
            tvResult.setVisibility(View.VISIBLE);
            tvResult.setText("NOT CORRECT");
            tvResult.setTextColor(Color.RED);
        }

        //  NEW PROBLEM
        showNewProblem();
    }

    private void startup(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        showZeroToTenProblem();
    }

    public FindTheCorrectNumberFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_find_the_correct_number, container, false);

        //  GET REFERENCES
        btnZeroToTen = rootView.findViewById(R.id.btnZeroToTen_FindTheCorrectNumberFragment);
        btnElevenToTwenty = rootView.findViewById(R.id.btnElevenToTwenty_FindTheCorrectNumberFragment);
        btnFirstNumber = rootView.findViewById(R.id.btnFirstNumber_FindTheCorrectNumberFragment);
        btnSecondNumber = rootView.findViewById(R.id.btnSecondNumber_FindTheCorrectNumberFragment);
        btnThirdNumber = rootView.findViewById(R.id.btnThirdNumber_FindTheCorrectNumberFragment);
        btnForthNumber = rootView.findViewById(R.id.btnForthNumber_FindTheCorrectNumberFragment);

        tvQuestion = rootView.findViewById(R.id.tvQuestionContent_FindTheCorrectNumberFragment);
        tvResult = rootView.findViewById(R.id.tvResult_FindTheCorrectNumberFragment);
        
        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnZeroToTen.setOnClickListener(ButtonListener);
        btnElevenToTwenty.setOnClickListener(ButtonListener);
        btnFirstNumber.setOnClickListener(ButtonListener);
        btnSecondNumber.setOnClickListener(ButtonListener);
        btnThirdNumber.setOnClickListener(ButtonListener);
        btnForthNumber.setOnClickListener(ButtonListener);
        
        //  START UP
        startup();

        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btnZeroToTen_FindTheCorrectNumberFragment:
                    showZeroToTenProblem();
                    break;
                case R.id.btnElevenToTwenty_FindTheCorrectNumberFragment:
                    showElevenToTwentyProblem();
                    break;
                case R.id.btnFirstNumber_FindTheCorrectNumberFragment:
                    clearAnswerSelection();
                    setSelection((Button) v);
                    checkAnswer((Button) v);
                    break;
                case R.id.btnSecondNumber_FindTheCorrectNumberFragment:
                    clearAnswerSelection();
                    setSelection((Button) v);
                    checkAnswer((Button) v);
                    break;
                case R.id.btnThirdNumber_FindTheCorrectNumberFragment:
                    clearAnswerSelection();
                    setSelection((Button) v);
                    checkAnswer((Button) v);
                    break;
                case R.id.btnForthNumber_FindTheCorrectNumberFragment:
                    clearAnswerSelection();
                    setSelection((Button) v);
                    checkAnswer((Button) v);
                    break;
            }
        }
    }

}
