package adsfree.congla.android.cong.mymathapp;


import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;


/**
 * A simple {@link Fragment} subclass.
 */
public class WhatsNewFragment extends Fragment {

    private Button btnContinue;

    private void setAppTitle(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stWhatNew));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void showInitialSetup(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new InitialSetupFragment())
                .commit();
    }

    private void showComeBack(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new UserInfoCollectionFragment())
                .commit();
    }

    private void continueStudy(){
        //  ALREADY SET UP
        if(InfoCollector.getUserLevel() > DatabaseFile.INITIAL_USER_LEVEL){
            showComeBack();
        }
        else {
            //  FIRST TIME
            showInitialSetup();
        }
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setAppTitle();
        btnContinue.setTextColor(Color.GRAY);
    }

    public WhatsNewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_whats_new, container, false);

        //  GET REFERENCES
        btnContinue = rootView.findViewById(R.id.btnContinue_WhatsNewFragment);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnContinue.setOnClickListener(ButtonListener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnContinue_WhatsNewFragment:
                    continueStudy();
                    break;
            }
        }
    }

}
