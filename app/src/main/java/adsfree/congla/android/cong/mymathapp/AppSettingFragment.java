package adsfree.congla.android.cong.mymathapp;


import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import pckAds.Ads;
import pckData.pckFirebase.LogEvents;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckMathTopics.TopicCollection;
import pckString.StringUtility;


/**
 * A simple {@link Fragment} subclass.
 */
public class AppSettingFragment extends Fragment {

    private EditText edtUserName;
    private TextView tvCurrentLevel;
    private Button btnBack, btnNext;
    private Button  btnMathStage, btnMathRound, btnMathPractice, btnMathExercise;
    private TextView  tvMathStage, tvMathStageInfo,
            tvMathRound, tvMathRoundInfo,
            tvMathPractice, tvMathPracticeInfo,
            tvMathExercise, tvMathExerciseInfo;
    private String UserName;
    private int CurrentLevel, MaxLevel = TopicCollection.getMaxTopicLevel();

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stSetting));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void setSelection(Button btnSelectedButton){
        btnSelectedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        clearSelection(btnSelectedButton);
    }

    private void clearSelection(final Button btnSelectedButton){
        new CountDownTimer(100, 100) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                btnSelectedButton.setTextColor(Color.GRAY);
            }
        }.start();
    }

    private void showCurrentUsername(){
        edtUserName.setText(InfoCollector.getUserName());
    }

    private void clearMainFeatureSelection(){
        btnMathRound.setTextColor(Color.GRAY);
        tvMathRound.setTextColor(Color.GRAY);
        tvMathRoundInfo.setVisibility(View.GONE);

        btnMathStage.setTextColor(Color.GRAY);
        tvMathStage.setTextColor(Color.GRAY);
        tvMathStageInfo.setVisibility(View.GONE);

        btnMathPractice.setTextColor(Color.GRAY);
        tvMathPractice.setTextColor(Color.GRAY);
        tvMathPracticeInfo.setVisibility(View.GONE);

        btnMathExercise.setTextColor(Color.GRAY);
        tvMathExercise.setTextColor(Color.GRAY);
        tvMathExerciseInfo.setVisibility(View.GONE);
    }

    private void showCurrentMainFeature(){
        //  CLEAR SELECTION
        clearMainFeatureSelection();

        //  SET SELECTION
        switch (InfoCollector.getUserSelectedFeature()){
            case InfoCollector.MATH_ROUND_INDEX:
                btnMathRound.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                tvMathRound.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                tvMathRoundInfo.setVisibility(View.VISIBLE);
                break;
            case InfoCollector.MATH_STAGE_INDEX:
                btnMathStage.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                tvMathStage.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                tvMathStageInfo.setVisibility(View.VISIBLE);
                break;
            case InfoCollector.MATH_PRACTICE_INDEX:
                btnMathPractice.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                tvMathPractice.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                tvMathPracticeInfo.setVisibility(View.VISIBLE);
                break;
            case InfoCollector.MATH_EXERCISE_INDEX:
                btnMathExercise.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                tvMathExercise.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                tvMathExerciseInfo.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void showMyStudyInfo(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MyStudyInfoFragment());
        ft.commit();
    }

    private void showCurrentLevel(){
        Cursor UserLevel = DatabaseFile.getUserLevel();
        UserLevel.moveToFirst();

        CurrentLevel = Integer.parseInt(UserLevel.getString(0).trim());
        tvCurrentLevel.setText(TopicCollection.getTopic(CurrentLevel).getName());
    }

    private void increaseLevel(){
        CurrentLevel += 1;
        if (CurrentLevel > MaxLevel){
            CurrentLevel = MaxLevel;
        }
        tvCurrentLevel.setText(TopicCollection.getTopic(CurrentLevel).getName());

        DatabaseFile.updateUserLevel(CurrentLevel);
    }

    private void decreaseLevel(){
        CurrentLevel -= 1;
        if (CurrentLevel < 1){
            CurrentLevel = 1;
        }
        tvCurrentLevel.setText(TopicCollection.getTopic(CurrentLevel).getName());

        DatabaseFile.updateUserLevel(CurrentLevel);
    }

    private boolean getUserName(){
        boolean isAvailable = false;
        UserName = edtUserName.getText().toString().trim();

        if (UserName.length() > 0){
            if (StringUtility.isAvailableString(UserName)){
                isAvailable = true;
            }
        }

        return isAvailable;
    }

    private void logUserNameChanged(){
        LogEvents.writeToLog(this.getActivity(), "UserNameChanged", "UserNameChanged", "UserNameChanged");
    }

    private void logUserFeatureChanged(String Feature){
        String NewFeature = "UserSwitchTo" + Feature;
        LogEvents.writeToLog(this.getActivity(), "MainFeatureChanged", NewFeature, NewFeature);
    }

    private void logUserLevelChanged(){
        LogEvents.writeToLog(this.getActivity(), "UserLevelChanged", "UserLevelChanged", "UserLevelChanged");
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        showCurrentUsername();
        showCurrentMainFeature();
        showCurrentLevel();

        btnBack.setTextColor(Color.GRAY);
        btnNext.setTextColor(Color.GRAY);

        Ads.showAds(this.getActivity(), MathUtility.getRandomBooleanValue());
    }

    public AppSettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_app_setting, container, false);

        //  GET REFERENCES
        edtUserName = rootView.findViewById(R.id.edtEnterYourName_AppSettingFragment);
        tvCurrentLevel = rootView.findViewById(R.id.tvCurrentLevel_AppSettingFragment);

        btnMathRound = rootView.findViewById(R.id.btnMathRound_AppSettingFragment);
        tvMathRound = rootView.findViewById(R.id.tvMathRound_AppSettingFragment);
        tvMathRoundInfo = rootView.findViewById(R.id.tvMathRoundInfo_AppSettingFragment);

        btnMathStage = rootView.findViewById(R.id.btnMathStage_AppSettingFragment);
        tvMathStage = rootView.findViewById(R.id.tvMathStage_AppSettingFragment);
        tvMathStageInfo = rootView.findViewById(R.id.tvMathStageInfo_AppSettingFragment);

        btnMathPractice = rootView.findViewById(R.id.btnMathPractice_AppSettingFragment);
        tvMathPractice = rootView.findViewById(R.id.tvMathPractice_AppSettingFragment);
        tvMathPracticeInfo = rootView.findViewById(R.id.tvMathPracticeInfo_AppSettingFragment);

        btnMathExercise = rootView.findViewById(R.id.btnMathExercise_AppSettingFragment);
        tvMathExercise = rootView.findViewById(R.id.tvMathExercise_AppSettingFragment);
        tvMathExerciseInfo = rootView.findViewById(R.id.tvMathExerciseInfo_AppSettingFragment);
        
        btnBack = rootView.findViewById(R.id.btnBack_AppSettingFragment);
        btnNext = rootView.findViewById(R.id.btnNext_AppSettingFragment);

        //  SET LISTENER
        UserNameChangedListener UserNameListener = new UserNameChangedListener();
        edtUserName.addTextChangedListener(UserNameListener);

        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnMathRound.setOnClickListener(ButtonListener);
        tvMathRound.setOnClickListener(ButtonListener);
        tvMathRoundInfo.setOnClickListener(ButtonListener);

        btnMathStage.setOnClickListener(ButtonListener);
        tvMathStage.setOnClickListener(ButtonListener);
        tvMathStageInfo.setOnClickListener(ButtonListener);

        btnMathPractice.setOnClickListener(ButtonListener);
        tvMathPractice.setOnClickListener(ButtonListener);
        tvMathPracticeInfo.setOnClickListener(ButtonListener);

        btnMathExercise.setOnClickListener(ButtonListener);
        tvMathExercise.setOnClickListener(ButtonListener);
        tvMathExerciseInfo.setOnClickListener(ButtonListener);
        
        btnBack.setOnClickListener(ButtonListener);
        btnNext.setOnClickListener(ButtonListener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnMathRound_AppSettingFragment:
                    logUserFeatureChanged("MathRound");
                    DatabaseFile.updateUserSelectedFeature(InfoCollector.MATH_ROUND_INDEX);
                    InfoCollector.getUserInfo();
                    InfoCollector.setupApp();
                    showCurrentMainFeature();
                    break;
                case R.id.tvMathRound_AppSettingFragment:
                    logUserFeatureChanged("MathRound");
                    DatabaseFile.updateUserSelectedFeature(InfoCollector.MATH_ROUND_INDEX);
                    InfoCollector.getUserInfo();
                    InfoCollector.setupApp();
                    showCurrentMainFeature();
                    break;
                case R.id.tvMathRoundInfo_AppSettingFragment:
                    showMyStudyInfo();
                    break;
                case R.id.btnMathStage_AppSettingFragment:
                    logUserFeatureChanged("MathStage");
                    DatabaseFile.updateUserSelectedFeature(InfoCollector.MATH_STAGE_INDEX);
                    InfoCollector.getUserInfo();
                    InfoCollector.setupApp();
                    showCurrentMainFeature();
                    break;
                case R.id.tvMathStage_AppSettingFragment:
                    logUserFeatureChanged("MathStage");
                    DatabaseFile.updateUserSelectedFeature(InfoCollector.MATH_STAGE_INDEX);
                    InfoCollector.getUserInfo();
                    InfoCollector.setupApp();
                    showCurrentMainFeature();
                    break;
                case R.id.tvMathStageInfo_AppSettingFragment:
                    showMyStudyInfo();
                    break;
                case R.id.btnMathPractice_AppSettingFragment:
                    logUserFeatureChanged("MathPractice");
                    DatabaseFile.updateUserSelectedFeature(InfoCollector.MATH_PRACTICE_INDEX);
                    InfoCollector.getUserInfo();
                    InfoCollector.setupApp();
                    showCurrentMainFeature();
                    break;
                case R.id.tvMathPractice_AppSettingFragment:
                    logUserFeatureChanged("MathPractice");
                    DatabaseFile.updateUserSelectedFeature(InfoCollector.MATH_PRACTICE_INDEX);
                    InfoCollector.getUserInfo();
                    InfoCollector.setupApp();
                    showCurrentMainFeature();
                    break;
                case R.id.tvMathPracticeInfo_AppSettingFragment:
                    showMyStudyInfo();
                    break;
                case R.id.btnMathExercise_AppSettingFragment:
                    logUserFeatureChanged("MathExercise");
                    DatabaseFile.updateUserSelectedFeature(InfoCollector.MATH_EXERCISE_INDEX);
                    InfoCollector.getUserInfo();
                    InfoCollector.setupApp();
                    showCurrentMainFeature();
                    break;
                case R.id.tvMathExercise_AppSettingFragment:
                    logUserFeatureChanged("MathExercise");
                    DatabaseFile.updateUserSelectedFeature(InfoCollector.MATH_EXERCISE_INDEX);
                    InfoCollector.getUserInfo();
                    InfoCollector.setupApp();
                    showCurrentMainFeature();
                    break;
                case R.id.tvMathExerciseInfo_AppSettingFragment:
                    showMyStudyInfo();
                    break;
                case R.id.btnBack_AppSettingFragment:
                    decreaseLevel();
                    logUserLevelChanged();
                    setSelection((Button) v);
                    break;
                case R.id.btnNext_AppSettingFragment:
                    increaseLevel();
                    logUserLevelChanged();
                    setSelection((Button) v);
                    break;
            }
        }
    }

    private class UserNameChangedListener implements TextWatcher{

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (getUserName()){
                DatabaseFile.updateUserName(UserName);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            logUserNameChanged();
            InfoCollector.getUserInfo();
        }
    }

}
