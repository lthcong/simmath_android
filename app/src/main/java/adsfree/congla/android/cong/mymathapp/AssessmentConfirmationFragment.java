package adsfree.congla.android.cong.mymathapp;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import layout.MathStageTransferFragment;
import pckInfo.InfoCollector;


/**
 * A simple {@link Fragment} subclass.
 */
public class AssessmentConfirmationFragment extends Fragment {

    private Button btnGoHome, btnContinue;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stAssessmentConfirmation));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();;
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void goHome(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();;
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new ComeBackFragment());
        ft.commit();
    }

    private void continueMathStage(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();;
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathStageTransferFragment());
        ft.commit();
    }

    private void continueMathRound(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();;
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathRoundStartingFragment());
        ft.commit();
    }

    private void continueMathPractice(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();;
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathPracticeStartingFragment());
        ft.commit();
    }

    private void continueMathExercise(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();;
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathExerciseStartingFragment());
        ft.commit();
    }

    private void goContinue(){
        switch (InfoCollector.getUserSelectedFeature()){
            case InfoCollector.MATH_ROUND_INDEX:
                continueMathRound();
                break;
            case InfoCollector.MATH_STAGE_INDEX:
                continueMathStage();
                break;
            case InfoCollector.MATH_PRACTICE_INDEX:
                continueMathPractice();
                break;
            case InfoCollector.MATH_EXERCISE_INDEX:
                continueMathExercise();
                break;
            default:
                continueMathStage();
                break;
        }
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        btnGoHome.setTextColor(Color.GRAY);
        btnContinue.setTextColor(Color.GRAY);
    }

    public AssessmentConfirmationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_assessment_confirmation, container, false);

        //  GET REFERENCES
        btnGoHome = rootView.findViewById(R.id.btnGoHome_AssessmentConfirmationFragment);
        btnContinue = rootView.findViewById(R.id.btnContinue_AssessmentConfirmationFragment);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnGoHome.setOnClickListener(ButtonListener);
        btnContinue.setOnClickListener(ButtonListener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnGoHome_AssessmentConfirmationFragment:
                    goHome();
                    break;
                case R.id.btnContinue_AssessmentConfirmationFragment:
                    goContinue();
                    break;
            }
        }
    }

}
