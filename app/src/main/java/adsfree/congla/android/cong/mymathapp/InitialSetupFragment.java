package adsfree.congla.android.cong.mymathapp;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import layout.MyMathAppIntroFragment;
import pckInfo.InfoCollector;


/**
 * A simple {@link Fragment} subclass.
 */
public class InitialSetupFragment extends Fragment {

    private Button btnHelpMeSetupSign, btnHelpMeSetup, btnShowAllAppsSign, btnShowAllApps;

    private void setAppTitle(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stSetting));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void setSelection(Button btnSelectedButton){
        btnSelectedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void setHelpMeSetupSelection(){
        setSelection(btnHelpMeSetupSign);
        setSelection(btnHelpMeSetup);
    }

    private void setShowAllAppsSelection(){
        setSelection(btnShowAllAppsSign);
        setSelection(btnShowAllApps);
    }

    private void showUserInfoCollection(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new UserInfoCollectionFragment());
        ft.commit();
    }

    private void showAllApps(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MyMathAppIntroFragment());
        ft.commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setAppTitle();

        btnHelpMeSetupSign.setTextColor(Color.GRAY);
        btnHelpMeSetup.setTextColor(Color.GRAY);
        btnShowAllAppsSign.setTextColor(Color.GRAY);
        btnShowAllApps.setTextColor(Color.GRAY);
    }

    public InitialSetupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_initial_setup, container, false);

        //  GET REFERENCES
        btnHelpMeSetupSign = rootView.findViewById(R.id.btnHelpMeSetupSign_InitialSetupFragment);
        btnHelpMeSetup = rootView.findViewById(R.id.btnHelpMeSetup_InitialSetupFragment);
        btnShowAllAppsSign = rootView.findViewById(R.id.btnShowAllAppsSign_InitialSetupFragment);
        btnShowAllApps = rootView.findViewById(R.id.btnShowAllApps_InitialSetupFragment);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnHelpMeSetupSign.setOnClickListener(ButtonListener);
        btnHelpMeSetup.setOnClickListener(ButtonListener);
        btnShowAllAppsSign.setOnClickListener(ButtonListener);
        btnShowAllApps.setOnClickListener(ButtonListener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnHelpMeSetupSign_InitialSetupFragment:
                    setHelpMeSetupSelection();
                    showUserInfoCollection();
                    break;
                case R.id.btnHelpMeSetup_InitialSetupFragment:
                    setHelpMeSetupSelection();
                    showUserInfoCollection();
                    break;
                case R.id.btnShowAllAppsSign_InitialSetupFragment:
                    setShowAllAppsSelection();
                    showAllApps();
                    break;
                case R.id.btnShowAllApps_InitialSetupFragment:
                    setShowAllAppsSelection();
                    showAllApps();
                    break;
            }
        }
    }

}
