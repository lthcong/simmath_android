package adsfree.congla.android.cong.mymathapp;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import pckInfo.InfoCollector;


/**
 * A simple {@link Fragment} subclass.
 */
public class AppTileFragment extends Fragment {


    private TextView tvAppTile;

    private void startup(){
        tvAppTile.setText(InfoCollector.getAppTitle().toUpperCase());
    }

    public AppTileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_app_tile, container, false);

        //  GET REFERENCES
        tvAppTile = rootView.findViewById(R.id.tvAppTitle_AppTileFragment);

        //  START UP
        startup();

        return rootView;
    }

}
