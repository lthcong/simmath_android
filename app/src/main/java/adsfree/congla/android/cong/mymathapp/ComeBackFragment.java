package adsfree.congla.android.cong.mymathapp;


import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

import layout.HistorySubMenuFragment;
import layout.MathStageTransferFragment;
import layout.MyMathAppIntroFragment;
import pckData.pckFirebase.LogEvents;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckMath.pckMathTopics.TopicCollection;
import pckString.StringUtility;


/**
 * A simple {@link Fragment} subclass.
 */
public class ComeBackFragment extends Fragment {

    private TextView tvWelcomeBack, tvCurrentLevel;
    private Button btnContinueStudySign, btnContinueStudy,
                    btnShowMyRecordSign, btnShowMyRecord,
                    btnShowAllAppsSign, btnShowAllApps;

    private void setAppTitle(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stWelcomeBack));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void logEvents(){
        LogEvents.writeToLog(this.getActivity(), "UserReturned", "UserReturned", "UserReturned");
    }

    private void setSelection(Button btnSelectedButton){
        btnSelectedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void continueMathRound(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathRoundStartingFragment());
        ft.commit();
    }

    private void continueMathStage(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathStageTransferFragment());
        ft.commit();
    }

    private void continueMathPractice(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathPracticeStartingFragment());
        ft.commit();
    }

    private void continueMathExercise(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathExerciseStartingFragment());
        ft.commit();
    }

    private void continueStudy(){
        switch (InfoCollector.getUserSelectedFeature()){
            case 1:
                continueMathRound();
                break;
            case 2:
                continueMathStage();
                break;
            case 3:
                continueMathPractice();
                break;
            case 4:
                continueMathExercise();
                break;
            default:
                continueMathStage();
                break;
        }
    }
    
    private void setContinueStudySelection(){
        setSelection(btnContinueStudy);
        setSelection(btnContinueStudySign);
    }

    private void setShowMyRecordSelection(){
        setSelection(btnShowMyRecord);
        setSelection(btnShowMyRecordSign);
    }

    private void setShowAllAppsSelection(){
        setSelection(btnShowAllApps);
        setSelection(btnShowAllAppsSign);
    }
    
    private void setupUI(){
        btnContinueStudySign.setTextColor(Color.GRAY);
        btnContinueStudy.setTextColor(Color.GRAY);
        btnShowMyRecordSign.setTextColor(Color.GRAY);
        btnShowMyRecord.setTextColor(Color.GRAY);
        btnShowAllAppsSign.setTextColor(Color.GRAY);
        btnShowAllApps.setTextColor(Color.GRAY);
    }

    private void showUserInfo(){
        String stWelcomeBack = "Welcome back, " + InfoCollector.getUserName();
        StringUtility.writeString(tvWelcomeBack, stWelcomeBack);

        String stCurrentLevel = "Current level: " + TopicCollection.getTopic(InfoCollector.getUserLevel()).getName();
        StringUtility.writeString(tvCurrentLevel, stCurrentLevel);
    }

    private void showMyRecord(){
        InfoCollector.setWorkbookHistorySelected();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new HistorySubMenuFragment());
        ft.commit();
    }

    private void showAllApps(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MyMathAppIntroFragment());
        ft.commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setAppTitle();
        setupUI();
        showUserInfo();
        logEvents();

    }

    public ComeBackFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_come_back, container, false);

        //  GET REFERENCES
        tvWelcomeBack = rootView.findViewById(R.id.tvWelcomeBack_ComeBackFragment);
        tvCurrentLevel = rootView.findViewById(R.id.tvCurrentLevel_ComeBackFragment);
        btnContinueStudySign = rootView.findViewById(R.id.btnContinueSign_ComeBackFragment);
        btnContinueStudy = rootView.findViewById(R.id.btnContinue_ComeBackFragment);
        btnShowMyRecordSign = rootView.findViewById(R.id.btnShowMyRecordSign_ComeBackFragment);
        btnShowMyRecord = rootView.findViewById(R.id.btnShowMyRecord_ComeBackFragment);
        btnShowAllAppsSign = rootView.findViewById(R.id.btnShowAllAppsSign_ComeBackFragment);
        btnShowAllApps = rootView.findViewById(R.id.btnShowAllApps_ComeBackFragment);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnContinueStudySign.setOnClickListener(ButtonListener);
        btnContinueStudy.setOnClickListener(ButtonListener);
        btnShowMyRecordSign.setOnClickListener(ButtonListener);
        btnShowMyRecord.setOnClickListener(ButtonListener);
        btnShowAllAppsSign.setOnClickListener(ButtonListener);
        btnShowAllApps.setOnClickListener(ButtonListener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btnContinueSign_ComeBackFragment:
                    setContinueStudySelection();
                    InfoCollector.setupApp();
                    continueStudy();
                    break;
                case R.id.btnContinue_ComeBackFragment:
                    setContinueStudySelection();
                    InfoCollector.setupApp();
                    continueStudy();
                    break;
                case R.id.btnShowMyRecordSign_ComeBackFragment:
                    setShowMyRecordSelection();
                    showMyRecord();
                    break;
                case R.id.btnShowMyRecord_ComeBackFragment:
                    setShowMyRecordSelection();
                    showMyRecord();
                    break;
                case R.id.btnShowAllAppsSign_ComeBackFragment:
                    setShowAllAppsSelection();
                    showAllApps();
                    break;
                case R.id.btnShowAllApps_ComeBackFragment:
                    setShowAllAppsSelection();
                    showAllApps();
                    break;
            }
        }
    }

}
