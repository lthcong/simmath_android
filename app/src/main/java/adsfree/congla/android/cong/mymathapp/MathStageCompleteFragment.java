package adsfree.congla.android.cong.mymathapp;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import layout.HistorySubMenuFragment;
import layout.MathStageTransferFragment;
import pckInfo.InfoCollector;


/**
 * A simple {@link Fragment} subclass.
 */
public class MathStageCompleteFragment extends Fragment {

    private Button btnContinueSign, btnContinue, btnShowRecordSign, btnShowRecord;

    private void setAppTitle(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathStage_Name));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void setSelection(Button btnSelectedButton){
        btnSelectedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void setContinueStudySelection(){
        setSelection(btnContinueSign);
        setSelection(btnContinue);
    }

    private void setShowRecordSelection(){
        setSelection(btnShowRecordSign);
        setSelection(btnShowRecord);
    }

    private void continueStudy(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathStageTransferFragment())
                .commit();
    }

    private void showRecord(){
        InfoCollector.setMathStageHistorySelected();
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new HistorySubMenuFragment())
                .commit();
    }

    private void setupButton(){
        btnContinueSign.setTextColor(Color.GRAY);
        btnContinue.setTextColor(Color.GRAY);
        btnShowRecordSign.setTextColor(Color.GRAY);
        btnShowRecord.setTextColor(Color.GRAY);
    }

    private void startup(){
        InfoCollector.setCurrentFragment(this);
        setAppTitle();
        setupButton();
    }

    public MathStageCompleteFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_stage_complete, container, false);

        //  GET REFERENCES
        btnContinueSign = rootView.findViewById(R.id.btnContinueStudySign_MathStageCompleteFragment);
        btnContinue = rootView.findViewById(R.id.btnContinueStudy_MathStageCompleteFragment);
        btnShowRecordSign = rootView.findViewById(R.id.btnShowMyRecordSign_MathStageCompleteFragment);
        btnShowRecord = rootView.findViewById(R.id.btnShowMyRecord_MathStageCompleteFragment);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnContinueSign.setOnClickListener(ButtonListener);
        btnContinue.setOnClickListener(ButtonListener);
        btnShowRecordSign.setOnClickListener(ButtonListener);
        btnShowRecord.setOnClickListener(ButtonListener);

        //  START UP
        startup();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnContinueStudySign_MathStageCompleteFragment:
                    setContinueStudySelection();
                    InfoCollector.setupMathStage();
                    continueStudy();
                    break;
                case R.id.btnContinueStudy_MathStageCompleteFragment:
                    setContinueStudySelection();
                    InfoCollector.setupMathStage();
                    continueStudy();
                    break;
                case R.id.btnShowMyRecordSign_MathStageCompleteFragment:
                    setShowRecordSelection();
                    showRecord();
                    break;
                case R.id.btnShowMyRecord_MathStageCompleteFragment:
                    setShowRecordSelection();
                    showRecord();
                    break;
            }
        }
    }

}
