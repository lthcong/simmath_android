package pckData.pckFirebase;

import android.app.Activity;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by Cong on 1/10/2018.
 */

public class LogEvents {

    public static void writeToLog(Activity CurrentActivity, String EventName, String BundleParams, String BundleValue){
        try {
            FirebaseAnalytics EventsLog = FirebaseAnalytics.getInstance(CurrentActivity);

            Bundle NewBundle = new Bundle();
            NewBundle.putString(BundleParams, BundleValue);

            EventsLog.logEvent(EventName, NewBundle);
        }
        catch (Exception e){}
    }

}
