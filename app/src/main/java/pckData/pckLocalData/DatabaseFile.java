package pckData.pckLocalData;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import pckData.pckFirebase.LogEvents;
import pckMath.MathUtility;
import pckMath.pckMathTopics.RoundCollection;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Cong on 9/8/2017.
 */

public class DatabaseFile {

    //  ERROR
    private static final String NOT_ENOUGH_MEMORY_ERROR = "Not enough memory space.";

    //  DATABASE
    private static final String DATABASE_NAME = "MyMathAppDatabase.db";
    private static SQLiteDatabase MY_MATH_APP_DATABASE;

    //  USER
    private static final String USER_TABLE = "UserTable",
            USER_ID = "ID",
            INITIAL_USER_ID = "01",
            USER_NAME = "Name",
            USER_LEVEL = "Level",
            USER_SELECTED_FEATURE = "SelectedFeature";
    public static int INITIAL_USER_LEVEL = 1, DEFAULT_USER_SELECTED_FEATURE = 2;
    public static String INITIAL_USER_NAME = "New User";

    //  MATH ROUND
    private static final String MATH_ROUND_TABLE_NAME = "MathRoundTable",
            MATH_ROUND_USER_ID = "ID",
            USER_ROUND = "Round";

    //  MATH ASSIGNMENT
    private static final String MATH_ASSIGNMENT_TABLE = "MathAssignmentTable",
            MATH_ASSIGNMENT_ID = "ID",
            MATH_ASSIGNMENT_TOPIC = "Topic",
            MATH_ASSIGNMENT_STATUS = "Status",
            INCOMPLETE_ASSIGNMENT = "Incomplete";
    public static final String COMPLETE_ASSIGNMENT = "Complete";

    //  MATH WORKBOOK
    private static final String MATH_WORKBOOK_TABLE_NAME = "MathWorkbookTable",
            MATH_WORKBOOK_ID = "ID",
            MATH_WORKBOOK_DATE = "Date",
            MATH_WORKBOOK_TOPIC = "Topic",
            MATH_WORKBOOK_TIME = "Time",
            MATH_WORKBOOK_SCORE = "Score",
            MATH_WORKBOOK_NO_QUESTION = "NoQuestion";

    //  MATH LADDER
    private static final String MATH_LADDER_TABLE_NAME = "MathLadderTable",
            MATH_LADDER_ID = "ID",
            MATH_LADDER_DATE = "Date",
            MATH_LADDER_TOPIC = "Topic",
            MATH_LADDER_TIME = "Time",
            MATH_LADDER_NO_QUESTION = "NoQuestion",
            MATH_LADDER_SCORE = "Score";

    //  MATH TEST
    private static final String MATH_TEST_TABLE_NAME = "MathTestTable",
            MATH_TEST_ID = "ID",
            MATH_TEST_NAME = "Name",
            MATH_TEST_DATE = "Date",
            MATH_TEST_TOPIC = "Topic",
            MATH_TEST_TIME = "Time",
            MATH_TEST_SCORE = "Score";

    //  MATH STAGE
    private static final String MATH_STAGE_TABLE_NAME = "MathStageTable",
            MATH_STAGE_ID = "ID",
            MATH_STAGE_DATE = "Date",
            MATH_STAGE_TOPIC = "Topic",
            MATH_STAGE_WB_ID = "WorkbookID",
            MATH_STAGE_LD_ID = "LadderID";

    //  MATH STAGE TEST DETAIL
    private static final String MATH_STAGE_TEST_DETAIL_TABLE_NAME = "MathStageTestDetailTable",
            MATH_STAGE_TEST_DETAIL_ID = "ID",
            MATH_STAGE_TEST_DETAIL_STAGE_ID = "MathStageID",
            MATH_STAGE_TEST_DETAIL_TEST_ID = "TestID";


    //  MATH PRACTICE
    private static final String MATH_PRACTICE_TABLE_NAME = "MathPracticeTable",
            MATH_PRACTICE_ID = "ID",
            MATH_PRACTICE_DATE = "Date",
            MATH_PRACTICE_TOPIC = "Topic",
            MATH_PRACTICE_SCORE = "Score",
            MATH_PRACTICE_NO_QUESTION = "NoQuestion";

    //  EXERCISE LIST
    private static final String EXERCISE_LIST_TABLE_NAME = "ExerciseListTable",
            EXERCISE_LIST_ID = "ID",
            EXERCISE_LIST_NO_QUESTION = "NoQuestion",
            EXERCISE_LIST_SCORE = "Score";

    //  MATH EXERCISE
    private static final String MATH_EXERCISE_TABLE_NAME = "MathExerciseTable",
            MATH_EXERCISE_ID = "ID",
            MATH_EXERCISE_DATE = "Date",
            MATH_EXERCISE_TOPIC = "Topic",
            MATH_EXERCISE_LIST_ID = "ListID";

    //  EXERCISE TEST DETAIL
    private static final String MATH_EXERCISE_TEST_DETAIL_TABLE_NAME = "MathExerciseTestDetailTable",
            MATH_EXERCISE_TEST_DETAIL_ID = "ID",
            MATH_EXERCISE_TEST_DETAIL_EXERCISE_ID = "MathExerciseID",
            MATH_EXERCISE_TEST_DETAIL_TEST_ID = "TestID";

    //  MATH PROBLEM
    private static final String MATH_PROBLEM_TABLE_NAME = "MathProblemTable",
            MATH_PROBLEM_ID = "ID",
            MATH_PROBLEM_LINKED_ID = "LinkedID",
            MATH_PROBLEM_QUESTION = "Question",
            MATH_PROBLEM_YOUR_ANSWER = "YourAnswer",
            MATH_PROBLEM_RIGHT_ANSWER = "RightAnswer",
            MATH_PROBLEM_RESULT = "Result";

    //  MATH PROBLEM RECORD
    private static final String MATH_PROBLEM_RECORD_TABLE_NAME = "MathProblemRecordTable",
            MATH_PROBLEM_RECORD_ID = " ID",
            MATH_PROBLEM_RECORD_TOPIC = "Topic",
            MATH_PROBLEM_RECORD_PROBLEM_TYPE = "Type",
            MATH_PROBLEM_RECORD_SCORE = "Score";

    //  CREATE DATABASE
    public static void createOrOpenDatabase(Activity CurrentActivity){
        try {
            MY_MATH_APP_DATABASE = CurrentActivity.openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);

            //  USER
            createUserTable();
            try{
                addNewUser();
            }
            catch (Exception e){}

            //  MATH ROUND
            createMathRoundTable();
            try {
                addToMathRoundTable();
            }
            catch (Exception e){}

            //  MATH PROBLEM
            createMathProblemTable();

            //  MATH ASSIGNMENT
            createMathAssignmentTable();

            //  MATH WORKBOOK
            createMathWorkbookTable();

            //  MATH LADDER
            createMathLadderTable();

            //  MATH TEST
            createMathTestTable();

            //  MATH STAGE
            createMathStageTablesGroup();

            //  MATH PRACTICE
            createMathPracticeTable();

            //  MATH EXERCISE
            createMathExerciseTablesGroup();

            //  AUTO SYNC DATA
            autoSyncData();
        }
        catch (Exception e){
            Toast.makeText(CurrentActivity, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    //  DELETE DATABASE
    private static void deleteDatabase(){
        try {
            clearUserTable();
            clearMathAssignmentTable();
            clearMathRoundTable();
            clearWorkbookTable();
            clearLadderTable();
            clearTestTable();
            clearMathStageTable();
            clearMathPracticeTable();
            clearMathExerciseTable();
            clearProblemTable();
        }
        catch (Exception e){}
    }

    //  REFORM DATABASE
    public static void reformDatabase(Activity CurrentActivity){
        try {
            deleteDatabase();
            createOrOpenDatabase(CurrentActivity);
        }
        catch (Exception e){}
    }

    //  AUTO SYNC DATA
    private static void autoSyncData(){
        //  GET USER LEVEL
        Cursor UserLevel = getUserLevel();
        UserLevel.moveToFirst();
        int CurrentLevel = Integer.parseInt(UserLevel.getString(0));

        //  SYNC MATH ROUND
        Cursor UserRound = getUserRound();
        UserRound.moveToFirst();
        int CurrentRound = Integer.parseInt(UserRound.getString(0));
        int MinRoundLevel = RoundCollection.getRoundList().get(CurrentLevel).getMinLevel();

        if (CurrentRound < MinRoundLevel){
            updateUserRound(MinRoundLevel);
        }
    }

    //  USER TABLE
    private static void createUserTable(){
        MY_MATH_APP_DATABASE.execSQL("CREATE TABLE IF NOT EXISTS "
                + USER_TABLE + "("
                + USER_ID + " VARCHAR PRIMARY KEY, "
                + USER_NAME + " VARCHAR, "
                + USER_LEVEL + " VARCHAR, "
                + USER_SELECTED_FEATURE + " VARCHAR);"
        );
    }

    private static void clearUserTable(){
        try {
            MY_MATH_APP_DATABASE.execSQL("Drop table " + USER_TABLE + ";");
        }
        catch (Exception e){}
    }
    
    public static void reformUserTable(){
        //  SAVE INFO
        Cursor CurrentUser = getUserInfo();
        CurrentUser.moveToFirst();
        
        String UserName = CurrentUser.getString(1);
        String UserLevel = CurrentUser.getString(2);

        //  CLEAR TABLE
        clearUserTable();

        //  CREATE NEW TABLE
        createUserTable();

        //  LOAD INFO
        addNewUser(UserName, UserLevel);

        //  SYNC DATA
        autoSyncData();
    }

    public static Cursor getUserInfo(){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + USER_TABLE
                        + " WHERE " + USER_ID + " = '" + INITIAL_USER_ID + "'"
                ,null);
    }

    public static Cursor getUserName(){
        return MY_MATH_APP_DATABASE.rawQuery("Select " + USER_NAME
                        + " from " + USER_TABLE
                        + " WHERE " + USER_ID + " = '" + INITIAL_USER_ID + "'"
                ,null);
    }

    public static Cursor getUserLevel(){
        return MY_MATH_APP_DATABASE.rawQuery("Select " + USER_LEVEL
                        + " from " + USER_TABLE
                        + " WHERE " + USER_ID + " = '" + INITIAL_USER_ID + "'"
                ,null);
    }

    public static Cursor getUserSelectedFeature(){
        return MY_MATH_APP_DATABASE.rawQuery("Select " + USER_SELECTED_FEATURE
                        + " from " + USER_TABLE
                        + " WHERE " + USER_ID + " = '" + INITIAL_USER_ID + "'"
                ,null);
    }

    private static void addNewUser(){
        MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + USER_TABLE
                + " VALUES('" + INITIAL_USER_ID
                + "', '" + INITIAL_USER_NAME
                + "', '" + String.valueOf(INITIAL_USER_LEVEL)
                + "', '" + String.valueOf(DEFAULT_USER_SELECTED_FEATURE) + "');"
        );
    }

    public static void addNewUser(String UserName, String UserLevel){
        MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + USER_TABLE
                + " VALUES('" + INITIAL_USER_ID + "', '" + UserName + "', '" + UserLevel + "', '2');"
        );
    }

    public static void updateUserName(String NewUserName){
        MY_MATH_APP_DATABASE.execSQL("Update " + USER_TABLE
                + " SET " + USER_NAME + " = '" + String.valueOf(NewUserName) + "' "
                + " WHERE " + USER_ID + " = '" + INITIAL_USER_ID + "';");
    }

    public static void updateUserLevel(int NewUserLevel){
        MY_MATH_APP_DATABASE.execSQL("Update " + USER_TABLE
                + " SET " + USER_LEVEL + " = '" + String.valueOf(NewUserLevel) + "' "
                + " WHERE " + USER_ID + " = '" + INITIAL_USER_ID + "';");
    }

    public static void updateUserSelectedFeature(int NewFeature){
        MY_MATH_APP_DATABASE.execSQL("Update " + USER_TABLE
                + " SET " + USER_SELECTED_FEATURE + " = '" + String.valueOf(NewFeature) + "' "
                + " WHERE " + USER_ID + " = '" + INITIAL_USER_ID + "';");
    }

    //  CLEAR HISTORY
    public static void clearHistory(Activity CurrentActivity){
        try {
            //  CLEAR
            clearMathRoundTable();
            clearWorkbookTable();
            clearLadderTable();
            clearTestTable();
            clearMathStageTable();
            clearMathPracticeTable();
            clearMathExerciseTable();
            clearProblemTable();

            //  RECREATE
            createMathRoundTable();
            createMathWorkbookTable();
            createMathLadderTable();
            createMathTestTable();
            createMathStageTablesGroup();
            createMathPracticeTable();
            createMathExerciseTablesGroup();
            createMathProblemTable();

            // LOG
            LogEvents.writeToLog(CurrentActivity, "ClearedHistory", "ClearedHistory", "ClearHistory");
        }
        catch (Exception e){}
    }

    //  MATH ROUND
    private static void createMathRoundTable(){
        MY_MATH_APP_DATABASE.execSQL("CREATE TABLE IF NOT EXISTS "
                + MATH_ROUND_TABLE_NAME + "("
                + MATH_ROUND_USER_ID + " VARCHAR PRIMARY KEY, "
                + USER_ROUND + " VARCHAR);"
        );
    }

    public static void clearMathRoundTable(){
        try {
            MY_MATH_APP_DATABASE.execSQL("Drop table " + MATH_ROUND_TABLE_NAME + ";");
        }
        catch (Exception e){}
    }

    public static void addToMathRoundTable(){
        MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_ROUND_TABLE_NAME
                + " VALUES('" + INITIAL_USER_ID + "', '1');"
        );
    }

    public static Cursor getUserRound(){
        return MY_MATH_APP_DATABASE.rawQuery("Select " + USER_ROUND
                        + " from " + MATH_ROUND_TABLE_NAME
                        + " WHERE " + MATH_ROUND_USER_ID + " = '" + INITIAL_USER_ID + "'"
                ,null);
    }

    public static void updateUserRound(int NewRound){
        MY_MATH_APP_DATABASE.execSQL("Update " + MATH_ROUND_TABLE_NAME
                + " SET " + USER_ROUND + " = '" + String.valueOf(NewRound) + "' "
                + " WHERE " + MATH_ROUND_USER_ID + " = '" + INITIAL_USER_ID + "';");
    }

    //  MATH ASSIGNMENT
    private static void createMathAssignmentTable(){
        MY_MATH_APP_DATABASE.execSQL("CREATE TABLE IF NOT EXISTS "
                + MATH_ASSIGNMENT_TABLE + "("
                + MATH_ASSIGNMENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + MATH_ASSIGNMENT_TOPIC + " VARCHAR, "
                + MATH_ASSIGNMENT_STATUS + " VARCHAR);"
        );
    }

    public static void clearMathAssignmentTable(){
        try {
            MY_MATH_APP_DATABASE.execSQL("Drop table " + MATH_ASSIGNMENT_TABLE + ";");
        }
        catch (Exception e){}
    }

    public static Cursor getMathAssignment(){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + MATH_ASSIGNMENT_TABLE
                        + " ORDER BY " + MATH_ASSIGNMENT_ID + " DESC",
                null);
    }

    public static Cursor getIncompleteMathAssignment(){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + MATH_ASSIGNMENT_TABLE
                        + " WHERE " + MATH_ASSIGNMENT_STATUS + " = '" + INCOMPLETE_ASSIGNMENT + "'",
                null);
    }

    public static Cursor getCompleteMathAssignment(){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + MATH_ASSIGNMENT_TABLE
                        + " WHERE " + MATH_ASSIGNMENT_STATUS + " = '" + COMPLETE_ASSIGNMENT + "'"
                        + " ORDER BY " + MATH_ASSIGNMENT_ID + " DESC",
                null);
    }

    public static void addNewAssignment(String AssignmentTopic){
        MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_ASSIGNMENT_TABLE
                + "(" + MATH_ASSIGNMENT_TOPIC + ", "
                + MATH_ASSIGNMENT_STATUS + ")"
                + " VALUES("
                + "'" + AssignmentTopic + "', "
                + "'" + INCOMPLETE_ASSIGNMENT + "'"
                + ");"
        );
    }

    public static void updateCompleteAssignment(String AssignmentID){
        MY_MATH_APP_DATABASE.execSQL("Update " + MATH_ASSIGNMENT_TABLE
                + " SET " + MATH_ASSIGNMENT_STATUS + " = '" + COMPLETE_ASSIGNMENT + "' "
                + " WHERE " + MATH_ASSIGNMENT_ID + " = '" + AssignmentID + "';");
    }

    //  MATH PROBLEM
    private static void createMathProblemTable(){
        MY_MATH_APP_DATABASE.execSQL("CREATE TABLE IF NOT EXISTS "
                + MATH_PROBLEM_TABLE_NAME + "("
                + MATH_PROBLEM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + MATH_PROBLEM_LINKED_ID + " VARCHAR, "
                + MATH_PROBLEM_QUESTION + " VARCHAR, "
                + MATH_PROBLEM_YOUR_ANSWER + " VARCHAR, "
                + MATH_PROBLEM_RIGHT_ANSWER + " VARCHAR, "
                + MATH_PROBLEM_RESULT + " VARCHAR);"
        );
    }

    public static void clearProblemTable(){
        try {
            MY_MATH_APP_DATABASE.execSQL("Drop table " + MATH_PROBLEM_TABLE_NAME + ";");
        }
        catch (Exception e){}
    }

    public static Cursor getProblemByLinkedID(String LinkedID){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + MATH_PROBLEM_TABLE_NAME
                + " WHERE " + MATH_PROBLEM_LINKED_ID + " = " + LinkedID, null);
    }

    public static void addToMathProblemTable(String LinkedID,
                                             String Question,
                                             String YourAnswer,
                                             String RightAnswer,
                                             String Result){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_PROBLEM_TABLE_NAME
                    +"("
                    + MATH_PROBLEM_LINKED_ID + ","
                    + MATH_PROBLEM_QUESTION + ","
                    + MATH_PROBLEM_YOUR_ANSWER + ","
                    + MATH_PROBLEM_RIGHT_ANSWER + ","
                    + MATH_PROBLEM_RESULT
                    + ")"
                    + " VALUES("
                    + "'" + LinkedID + "', "
                    + "'" + Question + "', "
                    + "'" + YourAnswer + "', "
                    + "'" + RightAnswer + "', "
                    + "'" + Result + "'"
                    + ");"
            );
        }
        catch (Exception e){ }
    }

    //  MATH WORKBOOK
    private static void createMathWorkbookTable(){
        MY_MATH_APP_DATABASE.execSQL("CREATE TABLE IF NOT EXISTS "
                + MATH_WORKBOOK_TABLE_NAME + "("
                + MATH_WORKBOOK_ID + " VARCHAR PRIMARY KEY, "
                + MATH_WORKBOOK_DATE + " VARCHAR, "
                + MATH_WORKBOOK_TOPIC + " VARCHAR, "
                + MATH_WORKBOOK_TIME + " VARCHAR, "
                + MATH_WORKBOOK_SCORE + " VARCHAR, "
                + MATH_WORKBOOK_NO_QUESTION + " VARCHAR"
                + ");"
        );
    }

    public static void clearWorkbookTable(){
        try {
            MY_MATH_APP_DATABASE.execSQL("Drop table " + MATH_WORKBOOK_TABLE_NAME + ";");
        }
        catch (Exception e){}
    }

    public static Cursor getMathWorkbook(){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + MATH_WORKBOOK_TABLE_NAME + " ORDER BY " + MATH_WORKBOOK_ID + " DESC",
                null);
    }

    public static Cursor getMathWorkbook(String WorkbookID){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_WORKBOOK_TABLE_NAME
                        + " WHERE " + MATH_WORKBOOK_ID + " = " + WorkbookID,
                null);
    }

    public static Cursor searchWorkbookHistoryByTopic(String SearchInfo){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_WORKBOOK_TABLE_NAME
                        + " WHERE " + MATH_WORKBOOK_TOPIC + " LIKE '%" + SearchInfo + "%'",
                null);
    }

    public static Cursor searchWorkbookHistoryByDate(String SearchInfo){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_WORKBOOK_TABLE_NAME
                        + " WHERE " + MATH_WORKBOOK_DATE + " LIKE '%" + SearchInfo + "%'",
                null);
    }

    public static Cursor searchWorkbookHistoryByNoQuestion(String SearchInfo){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_WORKBOOK_TABLE_NAME
                        + " WHERE " + MATH_WORKBOOK_NO_QUESTION + " LIKE '%" + SearchInfo + "%'",
                null);
    }

    public static Cursor searchWorkbookHistory(String SearchInfo){
        Cursor SearchResult = searchWorkbookHistoryByTopic(SearchInfo);
        if (SearchResult.getCount() == 0){
            SearchResult = searchWorkbookHistoryByDate(SearchInfo);
            if (SearchResult.getCount() == 0){
                SearchResult = searchWorkbookHistoryByNoQuestion(SearchInfo);
            }
        }

        return SearchResult;
    }

    public static void addToMathWorkbookTable(String MathWorkbookID,
                                              String Date,
                                              String Topic){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_WORKBOOK_TABLE_NAME
                    + " VALUES("
                    + "'" + MathWorkbookID + "', "
                    + "'" + Date + "', "
                    + "'" + Topic + "', "
                    + "'0', "
                    + "'0', "
                    + "'0'"
                    + ");"
            );
        }
        catch (Exception e){ }
    }

    public static void addToMathWorkbookTable(String MathWorkbookID,
                                              String Date,
                                              String Topic,
                                              String Time,
                                              String Score,
                                              String NoQuestion){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_WORKBOOK_TABLE_NAME
                    + " VALUES("
                    + "'" + MathWorkbookID + "', "
                    + "'" + Date + "', "
                    + "'" + Topic + "', "
                    + "'" + Time + "', "
                    + "'" + Score + "', "
                    + "'" + NoQuestion + "'"
                    + ");"
            );
        }
        catch (Exception e){ }
    }

    public static void addToMathWorkbookTable(String MathWorkbookID,
                                              String Date,
                                              String Topic,
                                              String Time,
                                              int Score,
                                              int NoQuestion){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_WORKBOOK_TABLE_NAME
                    + " VALUES("
                    + "'" + MathWorkbookID + "', "
                    + "'" + Date + "', "
                    + "'" + Topic + "', "
                    + "'" + Time + "', "
                    + "'" + String.valueOf(Score) + "', "
                    + "'" + String.valueOf(NoQuestion) + "'"
                    + ");"
            );
        }
        catch (Exception e){ }
    }

    public static void updateMathWorkbookTime(String MathWorkbookID, int WorkTime){
        MY_MATH_APP_DATABASE.execSQL("Update " + MATH_WORKBOOK_TABLE_NAME
                + " SET " + MATH_WORKBOOK_TIME + " = '" + String.valueOf(WorkTime) + "' "
                + " WHERE " + MATH_WORKBOOK_ID + " = '" + MathWorkbookID + "';");
    }

    public static void updateMathWorkbookScore(String MathWorkbookID, int Score){
        MY_MATH_APP_DATABASE.execSQL("Update " + MATH_WORKBOOK_TABLE_NAME
                + " SET " + MATH_WORKBOOK_SCORE + " = '" + String.valueOf(Score) + "' "
                + " WHERE " + MATH_WORKBOOK_ID + " = '" + MathWorkbookID + "';");
    }

    public static void updateMathWorkbookNoQuestion(String MathWorkbookID, int NoQuestion){
        MY_MATH_APP_DATABASE.execSQL("Update " + MATH_WORKBOOK_TABLE_NAME
                + " SET " + MATH_WORKBOOK_NO_QUESTION + " = '" + String.valueOf(NoQuestion) + "' "
                + " WHERE " + MATH_WORKBOOK_ID + " = '" + MathWorkbookID + "';");
    }

    public static void updateMathWorkbook(String MathWorkbookID, int Score, int NoQuestion){
        updateMathWorkbookScore(MathWorkbookID, Score);
        updateMathWorkbookNoQuestion(MathWorkbookID, NoQuestion);
    }

    public static void updateMathWorkbook(String MathWorkbookID, int WorkTime, int Score, int NoQuestion){
        updateMathWorkbookTime(MathWorkbookID, WorkTime);
        updateMathWorkbookScore(MathWorkbookID, Score);
        updateMathWorkbookNoQuestion(MathWorkbookID, NoQuestion);
    }

    //  MATH LADDER
    private static void createMathLadderTable(){
        MY_MATH_APP_DATABASE.execSQL("CREATE TABLE IF NOT EXISTS "
                + MATH_LADDER_TABLE_NAME + "("
                + MATH_LADDER_ID + " VARCHAR PRIMARY KEY, "
                + MATH_LADDER_DATE + " VARCHAR, "
                + MATH_LADDER_TOPIC + " VARCHAR, "
                + MATH_LADDER_TIME + " VARCHAR, "
                + MATH_LADDER_NO_QUESTION + " VARCHAR, "
                + MATH_LADDER_SCORE + " VARCHAR);"
        );
    }

    public static void clearLadderTable(){
        try {
            MY_MATH_APP_DATABASE.execSQL("Drop table " + MATH_LADDER_TABLE_NAME + ";");
        }
        catch (Exception e){}
    }

    public static Cursor getMathLadder(){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + MATH_LADDER_TABLE_NAME + " ORDER BY " + MATH_LADDER_ID + " DESC",
                null);
    }

    public static Cursor getMathLadder(String MathLadderID){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_LADDER_TABLE_NAME
                        + " WHERE " + MATH_LADDER_ID + " = " + MathLadderID,
                null);
    }

    public static Cursor searchMathLadderByTopic(String SearchInfo){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_LADDER_TABLE_NAME
                        + " WHERE " + MATH_LADDER_TOPIC + " LIKE '%" + SearchInfo + "%'",
                null);
    }

    public static Cursor searchMathLadderByDate(String SearchInfo){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_LADDER_TABLE_NAME
                        + " WHERE " + MATH_LADDER_DATE + " LIKE '%" + SearchInfo + "%'",
                null);
    }

    public static Cursor searchMathLadderByScoreOrNoQuestion(String SearchInfo){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_LADDER_TABLE_NAME
                        + " WHERE " + MATH_LADDER_SCORE + " LIKE '%" + SearchInfo + "%'"
                        + " OR " + MATH_LADDER_NO_QUESTION + " LIKE '%" + SearchInfo + "%'",
                null);
    }

    public static Cursor searchMathLadderHistory(String SearchInfo){
        Cursor SearchResult = searchMathLadderByTopic(SearchInfo);
        if (SearchResult.getCount() == 0){
            SearchResult = searchMathLadderByDate(SearchInfo);
            if (SearchResult.getCount() == 0){
                SearchResult = searchMathLadderByScoreOrNoQuestion(SearchInfo);
            }
        }

        return SearchResult;
    }

    public static void addToMathLadderTable(String MathLadderID,
                                            String Date,
                                            String Topic,
                                            String Time,
                                            String NoQuestion,
                                            String Score){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_LADDER_TABLE_NAME
                    + " VALUES("
                    + "'" + MathLadderID + "', "
                    + "'" + Date + "', "
                    + "'" + Topic + "', "
                    + "'" + Time + "', "
                    + "'" + NoQuestion + "', "
                    + "'" + Score + "'"
                    + ");"
            );
        }
        catch (Exception e){ }
    }

    public static void addToMathLadderTable(String MathLadderID,
                                            String Date,
                                            String Topic){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_LADDER_TABLE_NAME
                    + " VALUES("
                    + "'" + MathLadderID + "', "
                    + "'" + Date + "', "
                    + "'" + Topic + "', "
                    + "'0', "
                    + "'0', "
                    + "'0'"
                    + ");"
            );
        }
        catch (Exception e){ }
    }

    private static void updateMathLadderWorkTime(String MathLadderID, int WorkTime){
        try {
            MY_MATH_APP_DATABASE.execSQL("Update " + MATH_LADDER_TABLE_NAME
                    + " SET " + MATH_LADDER_TIME + " = '" + String.valueOf(WorkTime) + "' "
                    + " WHERE " + MATH_LADDER_ID + " = '" + MathLadderID + "';");
        }
        catch (Exception E) {}
    }

    private static void updateMathLadderNoQuestion(String MathLadderID, int NoQuestion){
        try {
            MY_MATH_APP_DATABASE.execSQL("Update " + MATH_LADDER_TABLE_NAME
                    + " SET " + MATH_LADDER_NO_QUESTION + " = '" + String.valueOf(NoQuestion) + "' "
                    + " WHERE " + MATH_LADDER_ID + " = '" + MathLadderID + "';");
        }
        catch (Exception E) {}
    }

    private static void updateMathLadderScore(String MathLadderID, int Score){
        try {
            MY_MATH_APP_DATABASE.execSQL("Update " + MATH_LADDER_TABLE_NAME
                    + " SET " + MATH_LADDER_SCORE + " = '" + String.valueOf(Score) + "' "
                    + " WHERE " + MATH_LADDER_ID + " = '" + MathLadderID + "';");
        }
        catch (Exception E) {}
    }
    
    public static void updateToMathLadderTable(String MathLadderID,
                                            int Time,
                                            int NoQuestion,
                                            int Score){
        try {
            updateMathLadderWorkTime(MathLadderID, Time);
            updateMathLadderNoQuestion(MathLadderID, NoQuestion);
            updateMathLadderScore(MathLadderID, Score);
        }
        catch (Exception e){ }
    }

    //  MATH TEST
    private static void createMathTestTable(){
        MY_MATH_APP_DATABASE.execSQL("CREATE TABLE IF NOT EXISTS "
                + MATH_TEST_TABLE_NAME + "("
                + MATH_TEST_ID + " VARCHAR PRIMARY KEY, "
                + MATH_TEST_NAME + " VARCHAR, "
                + MATH_TEST_DATE + " VARCHAR, "
                + MATH_TEST_TOPIC + " VARCHAR, "
                + MATH_TEST_TIME + " VARCHAR, "
                + MATH_TEST_SCORE + " VARCHAR"
                + ");"
        );
    }

    public static void clearTestTable(){
        try {
            MY_MATH_APP_DATABASE.execSQL("Drop table " + MATH_TEST_TABLE_NAME + ";");
        }
        catch (Exception e){}
    }

    public static Cursor getMathTest(){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + MATH_TEST_TABLE_NAME + " ORDER BY " + MATH_TEST_ID + " DESC",
                null);
    }

    public static Cursor getMathTest(String TestID){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + MATH_TEST_TABLE_NAME
                        + " WHERE " + MATH_TEST_ID + " = " + TestID
                        + " ORDER BY " + MATH_TEST_ID + " DESC",
                null);
    }

    public static Cursor getMathTestScore(String TestID){
        return MY_MATH_APP_DATABASE.rawQuery("Select " + MATH_TEST_SCORE + " from " + MATH_TEST_TABLE_NAME
                        + " WHERE " + MATH_TEST_ID + " = " + TestID,
                null);
    }

    public static Cursor searchTestByTopic(String SearchInfo){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_TEST_TABLE_NAME
                        + " WHERE " + MATH_TEST_TOPIC + " LIKE '%" + SearchInfo + "%'",
                null);
    }

    public static Cursor searchTestByDate(String SearchInfo){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_TEST_TABLE_NAME
                        + " WHERE " + MATH_TEST_DATE + " LIKE '%" + SearchInfo + "%'",
                null);
    }

    public static Cursor searchTestByType(String SearchInfo){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_TEST_TABLE_NAME
                        + " WHERE " + MATH_TEST_NAME + " LIKE '%" + SearchInfo + "%'",
                null);
    }

    public static Cursor searchTestByScore(String SearchInfo){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_TEST_TABLE_NAME
                        + " WHERE " + MATH_TEST_SCORE + " LIKE '%" + SearchInfo + "%'",
                null);
    }

    public static Cursor searchTestHistory(String SearchInfo){
        Cursor SearchResult = searchTestByTopic(SearchInfo);
        if (SearchResult.getCount() == 0){
            SearchResult = searchTestByDate(SearchInfo);
            if (SearchResult.getCount() == 0){
                SearchResult = searchTestByType(SearchInfo);
                if (SearchResult.getCount() == 0){
                    SearchResult = searchTestByScore(SearchInfo);
                }
            }
        }

        return SearchResult;
    }

    public static void addToMathTestTable(String MathTestID,
                                          String TestName,
                                          String TestDate,
                                          String Topic,
                                          String Time,
                                          String Score){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_TEST_TABLE_NAME
                    + " VALUES("
                    + "'" + MathTestID + "', "
                    + "'" + TestName + "', "
                    + "'" + TestDate + "', "
                    + "'" + Topic + "', "
                    + "'" + Time + "', "
                    + "'" + Score + "'"
                    + ");"
            );
        }
        catch (Exception e1){
            try {
                MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_TEST_TABLE_NAME
                        + " VALUES("
                        + "'" + MathTestID + "', "
                        + "'" + TestDate + "', "
                        + "'" + Topic + "', "
                        + "'" + Time + "', "
                        + "'" + Score + "'"
                        + ");"
                );
            }
            catch (Exception e2){}
        }
    }

    //  MATH STAGE
    private static void createMathStageTablesGroup(){
        createMathStageTable();
        createMathStageTestDetailTable();
    }

    private static void createMathStageTable(){
        MY_MATH_APP_DATABASE.execSQL("CREATE TABLE IF NOT EXISTS "
                + MATH_STAGE_TABLE_NAME + "("
                + MATH_STAGE_ID + " VARCHAR PRIMARY KEY, "
                + MATH_STAGE_DATE + " VARCHAR, "
                + MATH_STAGE_TOPIC + " VARCHAR, "
                + MATH_STAGE_WB_ID + " VARCHAR, "
                + MATH_STAGE_LD_ID + " VARCHAR"
                + ");"
        );
    }

    private static void createMathStageTestDetailTable(){
        MY_MATH_APP_DATABASE.execSQL("CREATE TABLE IF NOT EXISTS "
                + MATH_STAGE_TEST_DETAIL_TABLE_NAME + "("
                + MATH_STAGE_TEST_DETAIL_ID + " VARCHAR PRIMARY KEY, "
                + MATH_STAGE_TEST_DETAIL_STAGE_ID + " VARCHAR, "
                + MATH_STAGE_TEST_DETAIL_TEST_ID + " VARCHAR"
                + ");"
        );
    }

    public static void clearMathStageTable(){
        try {
            MY_MATH_APP_DATABASE.execSQL("Drop table " + MATH_STAGE_TABLE_NAME + ";");
            MY_MATH_APP_DATABASE.execSQL("Drop table " + MATH_STAGE_TEST_DETAIL_TABLE_NAME + ";");
        }
        catch (Exception e){}
    }

    public static Cursor getMathStage(){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + MATH_STAGE_TABLE_NAME
                        + " ORDER BY " + MATH_STAGE_ID + " DESC",
                null);
    }

    public static Cursor getMathStage(String MathStageID){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + MATH_STAGE_TABLE_NAME
                        + " WHERE " + MATH_STAGE_ID + " = " + MathStageID,
                null);
    }

    public static Cursor getMathStageTestDetail(String MathStageID){
        return MY_MATH_APP_DATABASE.rawQuery("Select " + MATH_STAGE_TEST_DETAIL_TEST_ID
                        + " from " + MATH_STAGE_TEST_DETAIL_TABLE_NAME
                        + " WHERE " + MATH_STAGE_TEST_DETAIL_STAGE_ID + " = " + MathStageID,
                null);
    }

    public static Cursor searchMathStageByTopic(String SearchInfo){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_STAGE_TABLE_NAME
                        + " WHERE " + MATH_STAGE_TOPIC + " LIKE '%" + SearchInfo + "%'",
                null);
    }

    public static Cursor searchMathStageByDate(String SearchInfo){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_STAGE_TABLE_NAME
                        + " WHERE " + MATH_STAGE_DATE + " LIKE '%" + SearchInfo + "%'",
                null);
    }

    public static Cursor searchMathStageHistory(String SearchInfo){
        Cursor SearchResult = searchMathStageByTopic(SearchInfo);
        if (SearchResult.getCount() == 0){
            SearchResult = searchMathStageByDate(SearchInfo);
        }

        return SearchResult;
    }

    public static void addToMathStageTable(String ID,
                                           String Date,
                                           String Topic){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_TEST_TABLE_NAME
                    +"("
                    + MATH_STAGE_ID + ","
                    + MATH_STAGE_DATE + ","
                    + MATH_STAGE_TOPIC
                    + ")"
                    + " VALUES("
                    + "'" + ID + "', "
                    + "'" + Date + "', "
                    + "'" + Topic + "'"
                    + ");"
            );
        }
        catch (Exception e){}
    }

    public static void addWBToMathStageTable(String MathStageID,
                                             String WorkbookID){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_TEST_TABLE_NAME
                    +"(" + MATH_STAGE_WB_ID + ")"
                    + " VALUES('" + WorkbookID + "'"
                    + " WHERE " + MATH_STAGE_ID + " = " + MathStageID
                    + ");"
            );
        }
        catch (Exception e){}
    }

    public static void addLDToMathStageTable(String MathStageID,
                                             String LadderID){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_TEST_TABLE_NAME
                    +"(" + MATH_STAGE_LD_ID + ")"
                    + " VALUES('" + LadderID + "'"
                    + " WHERE " + MATH_STAGE_ID + " = " + MathStageID
                    + ");"
            );
        }
        catch (Exception e){}
    }

    public static void addTestToMathStageTable(String MathStageID,
                                               String TestID){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_STAGE_TEST_DETAIL_TABLE_NAME
                    + " VALUES("
                    + "'" + MathUtility.getID() + "', "
                    + "'" + MathStageID + "', "
                    + "'" + TestID + "'"
                    + ");"
            );
        }
        catch (Exception e){}
    }

    //  MATH PRACTICE
    private static void createMathPracticeTable(){
        MY_MATH_APP_DATABASE.execSQL("CREATE TABLE IF NOT EXISTS "
                + MATH_PRACTICE_TABLE_NAME + "("
                + MATH_PRACTICE_ID + " VARCHAR PRIMARY KEY, "
                + MATH_PRACTICE_DATE + " VARCHAR, "
                + MATH_PRACTICE_TOPIC + " VARCHAR, "
                + MATH_PRACTICE_SCORE + " VARCHAR, "
                + MATH_PRACTICE_NO_QUESTION + " VARCHAR"
                + ");"
        );
    }

    public static void clearMathPracticeTable(){
        try {
            MY_MATH_APP_DATABASE.execSQL("Drop table " + MATH_PRACTICE_TABLE_NAME + ";");
        }
        catch (Exception e){}
    }

    public static Cursor getMathPractice(){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + MATH_PRACTICE_TABLE_NAME
                        + " ORDER BY " + MATH_PRACTICE_ID + " DESC",
                null);
    }

    public static Cursor getMathPractice(String MathPracticeID) {
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + MATH_PRACTICE_TABLE_NAME
                        + " WHERE " + MATH_PRACTICE_ID + " = " + MathPracticeID,
                null);
    }

    public static Cursor getMathPracticeScore(){
        return MY_MATH_APP_DATABASE.rawQuery("Select " + MATH_PRACTICE_SCORE + " from " + MATH_PRACTICE_TABLE_NAME
                        + " ORDER BY " + MATH_PRACTICE_ID + " DESC",
                null);
    }

    public static Cursor getMathPracticeScore(String MathPracticeID) {
        return MY_MATH_APP_DATABASE.rawQuery("Select " + MATH_PRACTICE_SCORE + " from " + MATH_PRACTICE_TABLE_NAME
                        + " WHERE " + MATH_PRACTICE_ID + " = " + MathPracticeID,
                null);
    }

    public static Cursor getMathPracticeNoQuestion(){
        return MY_MATH_APP_DATABASE.rawQuery("Select " + MATH_PRACTICE_NO_QUESTION + " from " + MATH_PRACTICE_TABLE_NAME
                        + " ORDER BY " + MATH_PRACTICE_ID + " DESC",
                null);
    }

    public static Cursor getMathPracticeNoQuestion(String MathPracticeID) {
        return MY_MATH_APP_DATABASE.rawQuery("Select " + MATH_PRACTICE_NO_QUESTION + " from " + MATH_PRACTICE_TABLE_NAME
                        + " WHERE " + MATH_PRACTICE_ID + " = " + MathPracticeID,
                null);
    }

    public static Cursor searchMathPracticeByTopic(String SearchInfo){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_PRACTICE_TABLE_NAME
                        + " WHERE " + MATH_PRACTICE_TOPIC + " LIKE '%" + SearchInfo + "%'",
                null);
    }

    public static Cursor searchMathPracticeByDate(String SearchInfo){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_PRACTICE_TABLE_NAME
                        + " WHERE " + MATH_PRACTICE_DATE + " LIKE '%" + SearchInfo + "%'",
                null);
    }

    public static Cursor searchMathPracticeByNoQuestion(String SearchInfo){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_PRACTICE_TABLE_NAME
                        + " WHERE " + MATH_PRACTICE_NO_QUESTION + " LIKE '%" + SearchInfo + "%'",
                null);
    }

    public static Cursor searchMathPracticeHistory(String SearchInfo){
        Cursor SearchResult = searchMathPracticeByTopic(SearchInfo);
        if (SearchResult.getCount() == 0){
            SearchResult = searchMathPracticeByDate(SearchInfo);
            if (SearchResult.getCount() == 0){
                SearchResult = searchMathPracticeByNoQuestion(SearchInfo);
            }
        }

        return SearchResult;
    }

    public static void addToMathPracticeTable(String ID,
                                              String Date,
                                              String Topic){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_PRACTICE_TABLE_NAME
                    + " VALUES("
                    + "'" + ID + "', "
                    + "'" + Date + "', "
                    + "'" + Topic + "', "
                    + "'0', "
                    + "'0'"
                    + ");"
            );
        }
        catch (Exception e){}
    }

    public static void addToMathPracticeTable(String ID,
                                              String Date,
                                              String Topic,
                                              String Score,
                                              String NoQuestion){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_PRACTICE_TABLE_NAME
                    + " VALUES("
                    + "'" + ID + "', "
                    + "'" + Date + "', "
                    + "'" + Topic + "', "
                    + "'" + Score + "', "
                    + "'" + NoQuestion + "'"
                    + ");"
            );
        }
        catch (Exception e){}
    }

    public static void addToMathPracticeTable(String ID,
                                              String Date,
                                              String Topic,
                                              int Score,
                                              int NoQuestion){
        try {
            addToMathPracticeTable(ID, Date, Topic, String.valueOf(Score), String.valueOf(NoQuestion));
        }
        catch (Exception e){}
    }

    public static void updateMathPracticeNoQuestion(String ID,
                                                    String NoQuestion){
        try {
            MY_MATH_APP_DATABASE.execSQL("Update " + MATH_PRACTICE_TABLE_NAME
                    + " SET " + MATH_PRACTICE_NO_QUESTION + " = '" + NoQuestion + "' "
                    + " WHERE " + MATH_PRACTICE_ID + " = '" + ID + "';");
        }
        catch (Exception e){}
    }

    public static void updateMathPracticeScore(String ID,
                                                    String Score){
        try {
            MY_MATH_APP_DATABASE.execSQL("Update " + MATH_PRACTICE_TABLE_NAME
                    + " SET " + MATH_PRACTICE_NO_QUESTION + " = '" + Score + "' "
                    + " WHERE " + MATH_PRACTICE_ID + " = '" + ID + "';");
        }
        catch (Exception e){}
    }

    public static void updateMathPracticeNoQuestion(String ID,
                                                    int Score,
                                                    int NoQuestion){
        try {
            updateMathPracticeScore(ID, String.valueOf(Score));
            updateMathPracticeNoQuestion(ID, String.valueOf(NoQuestion));
        }
        catch (Exception e){}
    }

    //  EXERCISE LIST
    private static void createExerciseListTable(){
        MY_MATH_APP_DATABASE.execSQL("CREATE TABLE IF NOT EXISTS "
                + EXERCISE_LIST_TABLE_NAME + "("
                + EXERCISE_LIST_ID + " VARCHAR PRIMARY KEY, "
                + EXERCISE_LIST_NO_QUESTION + " VARCHAR, "
                + EXERCISE_LIST_SCORE + " VARCHAR"
                + ");"
        );
    }

    public static Cursor getExerciseList(){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + EXERCISE_LIST_TABLE_NAME
                        + " ORDER BY " + MATH_EXERCISE_ID + " DESC",
                null);
    }

    public static Cursor getExerciseList(String ListID){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + EXERCISE_LIST_TABLE_NAME
                        + " WHERE " + MATH_EXERCISE_ID + " = " + ListID,
                null);
    }

    public static void addToExerciseList(String ListID,
                                         String NoQuestion,
                                         String Score){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + EXERCISE_LIST_TABLE_NAME
                    + " VALUES("
                    + "'" + ListID + "', "
                    + "'" + NoQuestion + "', "
                    + "'" + Score + "'"
                    + ");"
            );
        }
        catch (Exception e){}
    }

    public static void addToExerciseList(String ListID,
                                         int NoQuestion,
                                         double Score){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + EXERCISE_LIST_TABLE_NAME
                    + " VALUES("
                    + "'" + ListID + "', "
                    + "'" + String.valueOf(NoQuestion) + "', "
                    + "'" + String.valueOf(Score) + "'"
                    + ");"
            );
        }
        catch (Exception e){}
    }

    public static void addToExerciseList(String ListID){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + EXERCISE_LIST_TABLE_NAME
                    + " VALUES("
                    + "'" + ListID + "', "
                    + "'0', "
                    + "'0'"
                    + ");"
            );
        }
        catch (Exception e){}
    }

    public static void updateNoQuestionInExerciseList(String ListID,
                                                       String NoQuestion){
        try {
            MY_MATH_APP_DATABASE.execSQL("Update " + EXERCISE_LIST_TABLE_NAME
                    + " SET " + EXERCISE_LIST_NO_QUESTION + " = '" + NoQuestion + "' "
                    + " WHERE " + EXERCISE_LIST_ID + " = '" + ListID + "';");
        }
        catch (Exception e){}
    }

    public static void updateScoreInExerciseList(String ListID,
                                                  String Score){
        try {
            MY_MATH_APP_DATABASE.execSQL("Update " + EXERCISE_LIST_TABLE_NAME
                    + " SET " + EXERCISE_LIST_SCORE + " = '" + Score + "' "
                    + " WHERE " + EXERCISE_LIST_ID + " = '" + ListID + "';");
        }
        catch (Exception e){}
    }

    public static void updateExerciseList(String ListID,
                                          String NoQuestion,
                                          String Score){
        updateNoQuestionInExerciseList(ListID, NoQuestion);
        updateScoreInExerciseList(ListID, Score);
    }

    //  MATH EXERCISE
    private static void createMathExerciseTablesGroup(){
        createExerciseListTable();
        createMathExerciseTable();
        createMathExerciseTestDetailTable();
    }

    private static void createMathExerciseTable(){
        MY_MATH_APP_DATABASE.execSQL("CREATE TABLE IF NOT EXISTS "
                + MATH_EXERCISE_TABLE_NAME + "("
                + MATH_EXERCISE_ID + " VARCHAR PRIMARY KEY, "
                + MATH_EXERCISE_DATE + " VARCHAR, "
                + MATH_EXERCISE_TOPIC + " VARCHAR, "
                + MATH_EXERCISE_LIST_ID + " VARCHAR"
                + ");"
        );
    }

    private static void createMathExerciseTestDetailTable(){
        MY_MATH_APP_DATABASE.execSQL("CREATE TABLE IF NOT EXISTS "
                + MATH_EXERCISE_TEST_DETAIL_TABLE_NAME + "("
                + MATH_EXERCISE_TEST_DETAIL_ID + " VARCHAR PRIMARY KEY, "
                + MATH_EXERCISE_TEST_DETAIL_EXERCISE_ID + " VARCHAR, "
                + MATH_EXERCISE_TEST_DETAIL_TEST_ID + " VARCHAR"
                + ");"
        );
    }

    public static void clearMathExerciseTable(){
        try {
            MY_MATH_APP_DATABASE.execSQL("Drop table " + MATH_EXERCISE_TABLE_NAME + ";");
            MY_MATH_APP_DATABASE.execSQL("Drop table " + EXERCISE_LIST_TABLE_NAME + ";");
            MY_MATH_APP_DATABASE.execSQL("Drop table " + MATH_EXERCISE_TEST_DETAIL_TABLE_NAME + ";");
        }
        catch (Exception e){}
    }

    public static Cursor getMathExercise(){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + MATH_EXERCISE_TABLE_NAME
                        + " ORDER BY " + MATH_EXERCISE_ID + " DESC",
                null);
    }

    public static Cursor getMathExercise(String ListID){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + MATH_EXERCISE_TABLE_NAME
                        + " WHERE " + MATH_EXERCISE_ID + " = " + ListID,
                null);
    }

    public static Cursor getMathExerciseTestDetail(String MathExerciseID){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from " + MATH_EXERCISE_TEST_DETAIL_TABLE_NAME
                        + " WHERE " + MATH_EXERCISE_TEST_DETAIL_EXERCISE_ID + " = " + MathExerciseID,
                null);
    }

    public static Cursor searchMathExerciseByTopic(String SearchInfo){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_EXERCISE_TABLE_NAME
                        + " WHERE " + MATH_EXERCISE_TOPIC + " LIKE '%" + SearchInfo + "%'",
                null);
    }

    public static Cursor searchMathExerciseByDate(String SearchInfo){
        return MY_MATH_APP_DATABASE.rawQuery("Select * from "
                        + MATH_EXERCISE_TABLE_NAME
                        + " WHERE " + MATH_EXERCISE_DATE + " LIKE '%" + SearchInfo + "%'",
                null);
    }

    public static Cursor searchMathExerciseHistory(String SearchInfo){
        Cursor SearchResult = searchMathExerciseByTopic(SearchInfo);
        if (SearchResult.getCount() == 0){
            SearchResult = searchMathExerciseByDate(SearchInfo);
        }

        return SearchResult;
    }

    public static void addToMathExercise(String ExerciseID,
                                         String ExerciseDate,
                                         String ExerciseTopic,
                                         String ListID,
                                         String TestID){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_EXERCISE_TABLE_NAME
                    + " VALUES("
                    + "'" + ExerciseID + "', "
                    + "'" + ExerciseDate + "', "
                    + "'" + ExerciseTopic + "', "
                    + "'" + ListID + "', "
                    + "'" + TestID + "'"
                    + ");"
            );
        }
        catch (Exception e){}
    }

    public static void addToMathExercise(String ExerciseID,
                                         String ExerciseDate,
                                         String ExerciseTopic){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_EXERCISE_TABLE_NAME
                    + " VALUES("
                    + "'" + ExerciseID + "', "
                    + "'" + ExerciseDate + "', "
                    + "'" + ExerciseTopic + "', "
                    + "'0', "
                    + "'0'"
                    + ");"
            );
        }
        catch (Exception e){}
    }

    public static void updateMathExercise(String ExerciseID,
                                          String ListID,
                                          String TestID){
        updateListIDInMathExercise(ExerciseID, ListID);
        updateTestIDInMathExercise(ExerciseID, TestID);
    }

    public static void updateListIDInMathExercise(String ExerciseID,
                                                  String ListID){
        try {
            MY_MATH_APP_DATABASE.execSQL("Update " + MATH_EXERCISE_TABLE_NAME
                    + " SET " + MATH_EXERCISE_LIST_ID + " = '" + ListID + "' "
                    + " WHERE " + MATH_EXERCISE_ID + " = '" + ExerciseID + "';");
        }
        catch (Exception e){}
    }

    public static void updateTestIDInMathExercise(String ExerciseID,
                                                  String TestID){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_EXERCISE_TEST_DETAIL_TABLE_NAME
                    + " VALUES("
                    + "'" + MathUtility.getID() + "', "
                    + "'" + ExerciseID + "', "
                    + "'" + TestID + "'"
                    + ");"
            );
        }
        catch (Exception e){}
    }

    //  MATH PROBLEM RECORD
    private static void createMathProblemRecordTable(){
        MY_MATH_APP_DATABASE.execSQL("CREATE TABLE IF NOT EXISTS "
                + MATH_PROBLEM_RECORD_TABLE_NAME + "("
                + MATH_PROBLEM_RECORD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + MATH_PROBLEM_RECORD_TOPIC + " VARCHAR, "
                + MATH_PROBLEM_RECORD_PROBLEM_TYPE + " VARCHAR, "
                + MATH_PROBLEM_RECORD_SCORE + " VARCHAR"
                + ");"
        );
    }

    public static void addToMathProblemRecordTable(String TopicName, String ProblemType){
        try {
            MY_MATH_APP_DATABASE.execSQL("INSERT INTO " + MATH_PROBLEM_RECORD_TABLE_NAME
                    +"("
                    + MATH_PROBLEM_RECORD_TOPIC + ","
                    + MATH_PROBLEM_RECORD_PROBLEM_TYPE + ","
                    + MATH_PROBLEM_RECORD_SCORE
                    + ")"
                    + " VALUES("
                    + "'" + TopicName + "', "
                    + "'" + ProblemType + "', "
                    + "'0'"
                    + ");"
            );
        }
        catch (Exception e){ }
    }

    public static void updateMathProblemScore(String TopicName, String ProblemType, String NewScore){
        try {
            MY_MATH_APP_DATABASE.execSQL("Update " + MATH_PROBLEM_RECORD_TABLE_NAME
                    + " SET " + MATH_PROBLEM_RECORD_SCORE + " = '" + NewScore + "' "
                    + " WHERE " + MATH_PROBLEM_RECORD_TOPIC + " = '" + TopicName + "'"
                    + " AND " + MATH_PROBLEM_RECORD_PROBLEM_TYPE + " = '" + ProblemType + "';"
            );
        }
        catch (Exception e){}
    }

    public static Cursor getProblemScore(String TopicName, String ProblemType){
        return MY_MATH_APP_DATABASE.rawQuery("Select " + MATH_PROBLEM_RECORD_SCORE + " from " + MATH_PROBLEM_RECORD_TABLE_NAME
                        + " WHERE " + MATH_PROBLEM_RECORD_TOPIC + " = " + TopicName
                        + " AND " + MATH_PROBLEM_RECORD_PROBLEM_TYPE + " = " + ProblemType,
                null);
    }

}
