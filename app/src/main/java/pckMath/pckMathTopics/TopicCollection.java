package pckMath.pckMathTopics;

import java.util.ArrayList;

import adsfree.congla.android.cong.mymathapp.R;
import pckMath.MathProblem;
import pckMath.pckProblemCollection.pckAlgebraOne.ReviewAlgebraOne;
import pckMath.pckProblemCollection.pckAlgebraOne.pckAlgebraicExpression.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckImaginaryAndComplexNumbers.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckCubic.ReviewCubicAndQuartic;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckCubic.pckEquation.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckCubic.pckFunction.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckFunction.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckLinearFunction.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.ReviewQuadraticFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckFunction.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckInequality.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuartic.pckEquation.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckSet.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.ReviewPointAndLine;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.pckBasicLine.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.pckFindingEquation.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.pckFindingIntercept.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.ReviewLinearEquationAndInequality;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.pckEquation.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.pckInequality.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.pckSystemOfLinearEquation.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.ReviewRadicalAndRationalExpression;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckIntroToPolynomial.ReviewPolynomial;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckIntroToPolynomial.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRationalPolynomial.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRadicalPolynomial.*;
import pckMath.pckProblemCollection.pckAlgebraTwo.ReviewAlgebra2Level;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.ReviewAnalyticGeometry;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction.ReviewRationalAndRadicalFunction;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction.pckRadical.pckEquation.*;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction.pckRadical.pckFunction.*;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction.pckRational.pckEquation.*;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction.pckRational.pckFunction.*;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckCircle.*;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckEllipses.*;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckParabolas.*;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckHyperbolas.*;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.ReviewExponentialAndLogarithmic;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckExponential.*;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckLogarithm.*;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckMatrix.*;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckSequencesAndSeries.ReviewSequencesAndSeries;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckSequencesAndSeries.pckSequences.*;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckSequencesAndSeries.pckSerices.*;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckTrigonometry.*;
import pckMath.pckProblemCollection.pckBasicMath.ReviewBasicLevel;
import pckMath.pckProblemCollection.pckBasicMath.pckDecimal.*;
import pckMath.pckProblemCollection.pckBasicMath.pckDecimal.pckAdd.*;
import pckMath.pckProblemCollection.pckBasicMath.pckDecimal.pckConvertingFractionIntoDecimal.*;
import pckMath.pckProblemCollection.pckBasicMath.pckDecimal.pckDiv.*;
import pckMath.pckProblemCollection.pckBasicMath.pckDecimal.pckMul.*;
import pckMath.pckProblemCollection.pckBasicMath.pckDecimal.pckPlaceValue.*;
import pckMath.pckProblemCollection.pckBasicMath.pckDecimal.pckSub.*;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.*;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckAdd.*;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckDiv.*;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckMul.*;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckSub.*;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckTypeOfFraction.*;
import pckMath.pckProblemCollection.pckBasicMath.pckInteger.*;
import pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckAdd.*;
import pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckDiv.*;
import pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckMul.*;
import pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckOrderOfOperations.*;
import pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckPlaceValue.*;
import pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckSub.*;
import pckMath.pckProblemCollection.pckIntermediateMath.ReviewIntermediateLevel;
import pckMath.pckProblemCollection.pckIntermediateMath.pckAbsoluteValue.*;
import pckMath.pckProblemCollection.pckIntermediateMath.pckExponential.*;
import pckMath.pckProblemCollection.pckIntermediateMath.pckCrossMultiply.*;
import pckMath.pckProblemCollection.pckIntermediateMath.pckCrossCanceling.*;
import pckMath.pckProblemCollection.pckIntermediateMath.pckOppositeOperations.*;
import pckMath.pckProblemCollection.pckIntermediateMath.pckRoot.*;
import pckMath.pckProblemCollection.pckIntermediateMath.pckFactor.*;
import pckMath.pckProblemCollection.pckPreAlgebra.ReviewPreAlgebraLevel;
import pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry.*;
import pckMath.pckProblemCollection.pckPreAlgebra.pckRatioAndPercentage.*;
import pckMath.pckProblemCollection.pckPreAlgebra.pckStatistic.*;


/**
 * Created by Cong on 4/26/2017.
 */

public class TopicCollection {

    //  SECTION
    private static int BASIC = R.string.stMathSection_Basic;
    private static int INTERMEDIATE = R.string.stMathSection_Intermediate;
    private static int PRE_ALGEBRA = R.string.stMathSection_PreAlgebra;
    private static int ALGEBRA_1 = R.string.stMathSection_Algebra_1;
    private static int ALGEBRA_2 = R.string.stMathSection_Algebra_2;

    private static ArrayList<Topic> TopicList;

    public static void setTopicList(){
        TopicList = new ArrayList<>();

        //  WHOLE NUMBER
        TopicList.add(new Topic(BASIC, "Adding 2 whole numbers without carrying: 1 or 2 digit.", new AddingWithoutCarryingWith1Or2Digit()));
        TopicList.add(new Topic(BASIC, "Adding 2 whole numbers without carrying: 2 or 3 digit.", new AddingWithoutCarryingWith2Or3Digit()));
        TopicList.add(new Topic(BASIC, "Adding 2 whole numbers without carrying.", new AddingWithoutCarrying(), false));
        TopicList.add(new Topic(BASIC, "Adding 2 whole numbers with carrying: 1 or 2 digit.", new AddingWithCarryingWith1Or2Digit()));
        TopicList.add(new Topic(BASIC, "Adding 2 whole numbers with carrying: 2 or 3 digit.", new AddingWithCarryingWith2Or3Digit()));
        TopicList.add(new Topic(BASIC, "Adding 2 whole numbers with carrying.", new AddingWithCarrying(), false));
        TopicList.add(new Topic(BASIC, "Word problem with adding whole numbers.", new WordProblemWithAddingInteger()));
        TopicList.add(new Topic(BASIC, "Review: Adding numbers.", new ReviewAddingWholeNumbers(), false));

        TopicList.add(new Topic(BASIC, "Subtracting 2 whole numbers without borrowing: 1 or 2 digit.", new SubtractingWithoutBorrowingWith1Or2Digit()));
        TopicList.add(new Topic(BASIC, "Subtracting 2 whole numbers without borrowing: 2 or 3 digit.", new SubtractingWithoutBorrowingWith2Or3Digit()));
        TopicList.add(new Topic(BASIC, "Subtracting 2 whole numbers without borrowing.", new SubtractingWithoutBorrowing(), false));
        TopicList.add(new Topic(BASIC, "Subtracting 2 whole numbers with borrowing: 1 or 2 digit.", new SubtractingWithBorrowingWith1Or2Digit()));
        TopicList.add(new Topic(BASIC, "Subtracting 2 whole numbers with borrowing: 2 or 3 digit.", new SubtractingWithBorrowingWith2Or3Digit()));
        TopicList.add(new Topic(BASIC, "Subtracting 2 whole numbers with borrowing.", new SubtractingWithBorrowing(), false));
        TopicList.add(new Topic(BASIC, "Word problem with subtracting whole numbers.", new WordProblemWithSubtractingInteger()));
        TopicList.add(new Topic(BASIC, "Review: Subtracting numbers.", new ReviewSubtractingWholeNumbers(), false));

        TopicList.add(new Topic(BASIC, "Place value: composing numbers.", new ComposingNumbers()));
        TopicList.add(new Topic(BASIC, "Place value: decomposing numbers.", new DecomposingNumbers()));

        TopicList.add(new Topic(BASIC, "Multiplying 2 whole numbers without carrying: 1 or 2 digit.", new MultiplyingWithoutCarryingWith1Or2Digit()));
        TopicList.add(new Topic(BASIC, "Multiplying 2 whole numbers without carrying: 2 or 3 digit.", new MultiplyingWithoutCarryingWith2Or3Digit()));
        TopicList.add(new Topic(BASIC, "Multiplying 2 whole numbers without carrying.", new MultiplyingWithoutCarrying(), false));
        TopicList.add(new Topic(BASIC, "Multiplying 2 whole numbers with carrying: 1 or 2 digit.", new MultiplyingWithCarryingWith1Or2Digit()));
        TopicList.add(new Topic(BASIC, "Multiplying 2 whole numbers with carrying: 2 or 3 digit.", new MultiplyingWithCarryingWith2Or3Digit()));
        TopicList.add(new Topic(BASIC, "Multiplying 2 whole numbers with carrying.", new MultiplyingWithCarrying(), false));
        TopicList.add(new Topic(BASIC, "Word problem with multiplying whole numbers.", new WordProblemWithMultiplyingInteger()));
        TopicList.add(new Topic(BASIC, "Review: Multiplying numbers.", new ReviewMultiplyingWholeNumbers(), false));

        TopicList.add(new Topic(BASIC, "Dividing 2 whole numbers without remainder.", new DividingWithoutRemainder()));
        TopicList.add(new Topic(BASIC, "Dividing 2 whole numbers with remainder.", new DividingWithRemainder()));
        TopicList.add(new Topic(BASIC, "Word problem with dividing whole numbers.", new WordProblemWithDividingInteger()));
        TopicList.add(new Topic(BASIC, "Review: Dividing numbers.", new ReviewDividingWholeNumbers(), false));

        TopicList.add(new Topic(BASIC, "Order of operations: 2 or 3 operations without parentheses.", new OrderOfOperationsWithoutParentheses()));
        TopicList.add(new Topic(BASIC, "Order of operations: 2 or 3 operations with parentheses.", new OrderOfOperationsWithParentheses()));
        TopicList.add(new Topic(BASIC, "Review: Order of operations.", new ReviewOrderOfOperationsWithWholeNumbers(), false));

        TopicList.add(new Topic(BASIC, "Review: Operations with whole number.", new ReviewOperationsWithWholeNumber(), false));

        //  FRACTION
        TopicList.add(new Topic(BASIC, "Types of fraction: proper, improper, mixed number.", new TypeOfFraction()));
        TopicList.add(new Topic(BASIC, "Fraction operations: reciprocal, equivalent, converting between fraction and mixed number.", new FractionOperations()));

        TopicList.add(new Topic(BASIC, "Adding fraction with the same denominator.", new AddingFractionWithTheSameDenominator()));
        TopicList.add(new Topic(BASIC, "Adding fraction with the different denominator.", new AddingFractionWithDifferentDenominator()));
        TopicList.add(new Topic(BASIC, "Adding fraction with mixed numbers.", new AddingWithMixedNumber()));
        TopicList.add(new Topic(BASIC, "Word problem with adding fractions.", new WordProblemWithAddingFraction()));
        TopicList.add(new Topic(BASIC, "Review: Adding fractions.", new ReviewAddingFraction(), false));

        TopicList.add(new Topic(BASIC, "Subtracting fraction with the same denominator.", new SubtractingFractionWithTheSameDenominator()));
        TopicList.add(new Topic(BASIC, "Subtracting fraction with the different denominator.", new SubtractingFractionWithDifferentDenominator()));
        TopicList.add(new Topic(BASIC, "Subtracting fraction with mixed numbers.", new SubtractingWithMixedNumber()));
        TopicList.add(new Topic(BASIC, "Word problem with subtracting fractions.", new WordProblemWithSubtractingFraction()));
        TopicList.add(new Topic(BASIC, "Review: Subtracting fractions.", new ReviewSubtractingFraction(), false));

        TopicList.add(new Topic(BASIC, "Multiplying fraction.", new MultiplyingFraction()));
        TopicList.add(new Topic(BASIC, "Multiplying fraction with mixed number.", new MultiplyingFractionAndMixedNumber()));
        TopicList.add(new Topic(BASIC, "Word problem with multiplying fractions.", new WordProblemWithMultiplyingFraction()));
        TopicList.add(new Topic(BASIC, "Review: Multiplying fractions.", new ReviewMultiplyingFraction(), false));

        TopicList.add(new Topic(BASIC, "Dividing fraction.", new DividingFraction()));
        TopicList.add(new Topic(BASIC, "Dividing fraction with mixed number.", new DividingFractionAndMixedNumber()));
        TopicList.add(new Topic(BASIC, "Word problem with dividing fractions.", new WordProblemWithDividingFraction()));
        TopicList.add(new Topic(BASIC, "Review: Dividing fractions.", new ReviewDividingFraction(), false));

        TopicList.add(new Topic(BASIC, "Review: Operations with fraction.", new ReviewOperationsWithFraction(), false));

        //  DECIMAL
        TopicList.add(new Topic(BASIC, "Place value of a decimal number: tens vs tenths, hundreds vs hundredths, thousands vs thousandths.",
                new PlaceValue()));

        TopicList.add(new Topic(BASIC, "Adding decimal number.", new AddingDecimal()));
        TopicList.add(new Topic(BASIC, "Word problem with adding decimal number.", new WordProblemWithAddingDecimal()));

        TopicList.add(new Topic(BASIC, "Subtracting decimal number.", new SubtractingDecimal()));
        TopicList.add(new Topic(BASIC, "Word problem with subtracting decimal number.", new WordProblemWithSubtractingDecimal()));

        TopicList.add(new Topic(BASIC, "Multiplying decimal number.", new MultiplyingDecimal()));
        TopicList.add(new Topic(BASIC, "Word problem with multiplying decimal number.", new WordProblemWithMultiplyingDecimal()));

        TopicList.add(new Topic(BASIC, "Dividing decimal number.", new DividingDecimal()));
        TopicList.add(new Topic(BASIC, "Word problem with dividing decimal number.", new WordProblemWithDividingDecimal()));
        TopicList.add(new Topic(BASIC, "Converting from Fraction into Decimal.", new ConvertFromFractionIntoDecimal()));

        TopicList.add(new Topic(BASIC, "Review: Operations with decimal number.", new ReviewOperationsWithDecimal(), false));

        //  INTEGER
        TopicList.add(new Topic(BASIC, "Adding with negative number.", new AddingWithNegativeNumber()));
        TopicList.add(new Topic(BASIC, "Subtracting with negative number.", new SubtractingWithNegativeNumber()));
        TopicList.add(new Topic(BASIC, "Multiplying with negative number.", new MultiplyingWithNegativeNumber()));
        TopicList.add(new Topic(BASIC, "Dividing with negative number.", new DividingWithNegativeNumber()));
        TopicList.add(new Topic(BASIC, "Order of operations with negative number.", new OrderOfOperationsWithNegativeNumber()));

        TopicList.add(new Topic(BASIC, "Review: Operations with integer.", new ReviewOperationsWithInteger(), false));

        TopicList.add(new Topic(BASIC, "Review: Basic level.", new ReviewBasicLevel(), false));

        //  INTRO TO EXPONENTIAL
        TopicList.add(new Topic(INTERMEDIATE, "Basic exponential problem: finding the base, exponent, value.", new ExponentialProblem()));
        TopicList.add(new Topic(INTERMEDIATE, "Operations with Exponent: Multiplying.", new MultiplyingWithExponent()));
        TopicList.add(new Topic(INTERMEDIATE, "Operations with Exponent: Dividing.", new DividingWithExponent()));
        TopicList.add(new Topic(INTERMEDIATE, "Order of operations with exponent.", new OrderOfOperationsWithExponent()));

        TopicList.add(new Topic(INTERMEDIATE, "Review: Operations with exponent.", new ReviewOperationsWithExponent(), false));

        //  ABSOLUTE VALUE
        TopicList.add(new Topic(INTERMEDIATE, "Absolute value.", new AbsoluteValueProblem()));

        //  INTRO TO SQUARE ROOT AND CUBE ROOT
        TopicList.add(new Topic(INTERMEDIATE, "Square root and Cube root.", new RootProblem()));
        TopicList.add(new Topic(INTERMEDIATE, "Operations with root: Adding.", new AddingWithRoot()));
        TopicList.add(new Topic(INTERMEDIATE, "Operations with root: Subtracting.", new SubtractingWithRoot()));
        TopicList.add(new Topic(INTERMEDIATE, "Operations with root: Multiplying.", new MultiplyingWithRoot()));
        TopicList.add(new Topic(INTERMEDIATE, "Operations with root: Dividing.", new DividingWithRoot()));
        TopicList.add(new Topic(INTERMEDIATE, "Review: Operations with root.", new ReviewOperationsWithRoot(), false));

        //  OPPOSITE OPERATIONS AND CROSS MULTIPLY
        TopicList.add(new Topic(INTERMEDIATE, "Finding the x value using opposite operation.", new OppositeOperations()));
        TopicList.add(new Topic(INTERMEDIATE, "Cross multiply.", new CrossMultiplyingProblem()));

        //  FACTOR AND CROSS CANCELING
        TopicList.add(new Topic(INTERMEDIATE, "Factor, prime factor.", new FactorProblem()));
        TopicList.add(new Topic(INTERMEDIATE, "Cross canceling.", new CrossCancelingProblem()));

        TopicList.add(new Topic(INTERMEDIATE, "Review: Intermediate level.", new ReviewIntermediateLevel(), false));

        //  RATIO AND PERCENTAGE
        TopicList.add(new Topic(PRE_ALGEBRA, "Ratio and Proportion.", new RatioAndProportion()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Unit rate.", new UnitRate()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Solving proportion by cross multiplying.", new SolvingProportionByCrossMultiplying()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Percentage.", new Percentage()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Finding increase and decrease percentage.", new IncreaseAndDecreasePercentage()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Converting between percent, decimal, fraction.", new ConvertingBetweenDecimalFractionPercent()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Review: Ratio and percentage.", new ReviewRatioAndPercentage(), false));

        //  STATISTIC AND PROBABILITY
        TopicList.add(new Topic(PRE_ALGEBRA, "Mean, median, range and mode.", new MeanMedianRangeMode()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Stem and Leaf Plot.", new StemAndLeaf()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Possibility.", new Possibility()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Probability.", new Probability()));
        TopicList.add(new Topic(PRE_ALGEBRA, "And, Or probability.", new AndOrProbability()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Probability of complementary and mutually exclusive events.", new ComplementaryAndMutuallyExclusiveProbability()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Probability of compound events.", new CompoundEventProbability()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Review: Statistic and probability.", new ReviewStatisticAndProbability(), false));

        //  GEOMETRY
        TopicList.add(new Topic(PRE_ALGEBRA, "Finding the measure of a line of segment.", new FindingTheMeasureOfALineOfSegment()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Finding the measure of an angle: supplement, complement, vertical angle.", new FindingTheMeasureOfAnAngle()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Finding the sum of all the interior angle of a polygon.", new FindingTheSumOfInteriorAnglesOfAPolygon()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Finding perimeter and area of triangle, rectangle, square.", new FindingTheAreaAndPerimeterOfTriangleRectangleSquare()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Finding perimeter and area of trapezoid, parallelogram, rhombus.", new FindingTheAreaAndPerimeterOfTrapezoidParallelogramRhombus()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Review: Shape and angle.", new ReviewShapeAndAngle(), false));

        TopicList.add(new Topic(PRE_ALGEBRA, "Pythagorean theorem.", new PythagoreanTheorem()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Special triangle: 30 - 60 - 90 triangle.", new ThirtySixtyNinetyTriangle()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Special triangle: 45 - 45 - 90 triangle.", new FortyFiveFortyFiveNinetyTriangle()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Similar triangle.", new SimilarTriangle()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Review: Triangle.", new ReviewTriangle(), false));

        TopicList.add(new Topic(PRE_ALGEBRA, "Finding the circumference and area of a circle.", new CircumferenceAndAreaOfCircle()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Circle and Angles: finding the measure of a central angle, inscribed angle.", new CircleAndAngle_CentralAndInscribedAngle()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Circle and Angles: finding the measure of a chord - tangent angle, " +
                "chord - chord angle, secant - secant angle, " +
                "tangent - secant angle.", new CircleAndAngle_TangentsChordsSecantsAngle()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Finding the measure of the segments formed by chord, tangent, secant.", new FindingTheMeasureOfSegmentsFormedByChordTangentSecant()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Review: Circle.", new ReviewCircle(), false));

        TopicList.add(new Topic(PRE_ALGEBRA, "Area of an irregular polygon.", new AreaOfIrregularShape()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Finding the surface area of a regular solid.", new SurfaceAreaOfRegularSolid()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Finding the volume of a regular solid.", new VolumeOfRegularSolid()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Finding the volume of an irregular solid.", new VolumeOfIrregularSolid()));
        TopicList.add(new Topic(PRE_ALGEBRA, "Review: Area and volume.", new ReviewCircle(), false));

        TopicList.add(new Topic(PRE_ALGEBRA, "Review: Geometry.", new ReviewGeometry(), false));

        TopicList.add(new Topic(PRE_ALGEBRA, "Review: Pre-Algebra level.", new ReviewPreAlgebraLevel(), false));

        //  BASIC ALGEBRA
        TopicList.add(new Topic(ALGEBRA_1, "Evaluate algebraic expression.", new EvaluateAlgebraicExpression()));
        TopicList.add(new Topic(ALGEBRA_1, "Combine like terms.", new CombineLikeTerms()));
        TopicList.add(new Topic(ALGEBRA_1, "Multiplying with variable.", new MultiplyingWithVariable()));
        TopicList.add(new Topic(ALGEBRA_1, "Dividing with variable.", new DividingWithVariable()));
        TopicList.add(new Topic(ALGEBRA_1, "Review: Basic algebra.", new ReviewBasicAlgebra(), false));

        //  POLYNOMIAL
        TopicList.add(new Topic(ALGEBRA_1, "Adding polynomial.", new AddingPolynomial()));
        TopicList.add(new Topic(ALGEBRA_1, "Subtracting polynomial.", new SubtractingPolynomial()));
        TopicList.add(new Topic(ALGEBRA_1, "Multiplying polynomial.", new MultiplyingPolynomial()));
        TopicList.add(new Topic(ALGEBRA_1, "Dividing polynomial.", new DividingPolynomial()));
        TopicList.add(new Topic(ALGEBRA_1, "Factoring polynomial.", new FactoringPolynomial()));
        TopicList.add(new Topic(ALGEBRA_1, "Review: Polynomial.", new ReviewPolynomial(), false));

        //  RATIONAL EXPRESSION
        TopicList.add(new Topic(ALGEBRA_1, "Simplify rational expression.", new SimplifyRationalPolynomial()));
        TopicList.add(new Topic(ALGEBRA_1, "Adding rational expression.", new AddingRationalPolynomial()));
        TopicList.add(new Topic(ALGEBRA_1, "Subtracting rational expression.", new SubtractingRationalPolynomial()));
        TopicList.add(new Topic(ALGEBRA_1, "Multiplying rational expression.", new MultiplyingRationalPolynomial()));
        TopicList.add(new Topic(ALGEBRA_1, "Dividing rational expression.", new DividingRationalPolynomial()));

        //  RADICAL EXPRESSION
        TopicList.add(new Topic(ALGEBRA_1, "Simplify radical expression without variable.", new SimplifyRadicalExpressionWithoutVariable()));
        TopicList.add(new Topic(ALGEBRA_1, "Simplify radical expression with variable.", new SimplifyRadicalExpressionWithVariable()));

        TopicList.add(new Topic(ALGEBRA_1, "Review: Radical and rational expression.", new ReviewRadicalAndRationalExpression(), false));

        //  LINEAR EQUATION
        TopicList.add(new Topic(ALGEBRA_1, "Solving linear equation.", new SolvingLinearEquation()));
        TopicList.add(new Topic(ALGEBRA_1, "Solving linear equation with absolute value.", new SolvingLinearEquationWithAbsoluteValue()));
        TopicList.add(new Topic(ALGEBRA_1, "Solving word problem with linear equation.", new WordProblemWithLinearEquation()));

        //  SYSTEM OF LINEAR EQUATION
        TopicList.add(new Topic(ALGEBRA_1, "Solving system of linear equations: substitution method.", new SolvingSystemOfLinearEquations()));
        TopicList.add(new Topic(ALGEBRA_1, "Solving system of linear equations: elimination method.", new SolvingSystemOfLinearEquations()));
        TopicList.add(new Topic(ALGEBRA_1, "Solving system of linear equations with 3 variables.", new SolvingSystemOfLinearEquationWith3Variables()));
        TopicList.add(new Topic(ALGEBRA_1, "Solving word problem with system of linear equations.", new WordProblemWithSystemOfLinearEquations()));

        //  LINEAR INEQUALITY
        TopicList.add(new Topic(ALGEBRA_1, "Solving linear inequality.", new SolvingLinearInequality()));
        TopicList.add(new Topic(ALGEBRA_1, "Solving compound inequalities.", new SolvingCompoundInequalities()));
        TopicList.add(new Topic(ALGEBRA_1, "Solving linear inequality with absolute value.", new SolvingLinearInequalityWithAbsoluteValue()));

        TopicList.add(new Topic(ALGEBRA_1, "Review: Linear equation and inequality.", new ReviewLinearEquationAndInequality(), false));

        //  LINE
        TopicList.add(new Topic(ALGEBRA_1, "Finding the distance and midpoint between two points.", new FindingDistanceAndMidpoint()));
        TopicList.add(new Topic(ALGEBRA_1, "Finding the slope of a line, x and y intercept.", new FindingSlopeAndIntercepts()));
        TopicList.add(new Topic(ALGEBRA_1, "Finding the intercept of 2 lines by linear equation.", new FindingTheInterceptOfTwoLinesByLinearEquation()));
        TopicList.add(new Topic(ALGEBRA_1, "Finding the intercept of 2 lines by system of linear equations.", new FindingInterceptOfTwoLines()));
        TopicList.add(new Topic(ALGEBRA_1, "Finding the equation of the line with given information.", new FindingEquationWithGivenInfo()));
        TopicList.add(new Topic(ALGEBRA_1, "Finding the parallel or perpendicular line.", new FindingPerpendicularAndParallelLine()));
        TopicList.add(new Topic(ALGEBRA_1, "Transforming between different types of equation: standard <=> slope intercept <=> point slope.", new TransformingEquation()));

        TopicList.add(new Topic(ALGEBRA_1, "Review: Point and line.", new ReviewPointAndLine(), false));

        //  COMPLEX NUMBER
        TopicList.add(new Topic(ALGEBRA_1, "Finding the value of a complex expression.", new FindingTheValueOfComplexNumber()));

        //  INTRO TO SET
        TopicList.add(new Topic(ALGEBRA_1, "Finding the value of a set statement.", new FindingResultOfASetStatement()));
        TopicList.add(new Topic(ALGEBRA_1, "Finding the value of a set operations.", new FindingTheResultOfASetOperation()));
        TopicList.add(new Topic(ALGEBRA_1, "Writing solution in set/interval notation.", new WriteSolutionInSetNotation()));
        TopicList.add(new Topic(ALGEBRA_1, "Finding function in list of ordered pairs.", new BeginningFunction()));
        TopicList.add(new Topic(ALGEBRA_1, "Transforming function: shift, compress, stretch, reflect.", new TransformingFunction()));

        //  LINEAR FUNCTION
        TopicList.add(new Topic(ALGEBRA_1, "Finding point, slope, range, domain of a linear function.", new FindThePointSlopeDomainRangeOfLinearFunction()));
        TopicList.add(new Topic(ALGEBRA_1, "Transforming linear function: shift left, right, up, down, reflect.", new TransformingLinearFunction()));
        TopicList.add(new Topic(ALGEBRA_1, "Manipulating linear function: adding, subtracting, multiplying, dividing, composing.", new ManipulatingLinearFunction()));
        TopicList.add(new Topic(ALGEBRA_1, "Finding the linear function with given information: slope, points, intercept.", new FindLinearFunction()));
        TopicList.add(new Topic(ALGEBRA_1, "Finding the intersection of 2 linear functions.", new FindingTheIntersectionOfLinearFunction()));
        TopicList.add(new Topic(ALGEBRA_1, "Word problem with linear functions.", new WordProblemWithLinearFunction()));
        TopicList.add(new Topic(ALGEBRA_1, "Review: Linear function.", new ReviewLinearFunction(), false));

        //  QUADRATIC FUNCTION
        TopicList.add(new Topic(ALGEBRA_1, "Finding point, vertex, min/ max value, domain, range of a quadratic function.", new FindingThePointVertexMinMaxDomainRangeOfQuadraticFunction()));
        TopicList.add(new Topic(ALGEBRA_1, "Transforming quadratic function: shift left, right, up, down, reflect.", new TransformingQuadraticFunction()));
        TopicList.add(new Topic(ALGEBRA_1, "Manipulating quadratic function: adding, subtracting, multiplying, dividing, composing.", new ManipulatingQuadraticFunction()));
        TopicList.add(new Topic(ALGEBRA_1, "Rewrite quadratic function in vertex form.", new RewriteQuadraticFunctionInVertexForm()));
        TopicList.add(new Topic(ALGEBRA_1, "Word problem with quadratic function.", new WordProblemWithQuadraticFunction()));
        TopicList.add(new Topic(ALGEBRA_1, "Solving quadratic equation by factoring.", new SolvingQuadraticEquationByFactoring()));
        TopicList.add(new Topic(ALGEBRA_1, "Solving quadratic equation by completing the square.", new SolvingQuadraticEquation()));
        TopicList.add(new Topic(ALGEBRA_1, "Solving quadratic equation by quadratic formula.", new SolvingQuadraticEquation()));
        TopicList.add(new Topic(ALGEBRA_1, "Finding the intersection of 2 functions: quadratic vs linear.", new FindingTheIntersectionOfQuadraticFunction()));
        TopicList.add(new Topic(ALGEBRA_1, "Word problem with quadratic equation.", new WordProblemWithQuadraticEquation()));
        TopicList.add(new Topic(ALGEBRA_1, "Solving quadratic inequality.", new SolvingQuadraticInequality()));
        TopicList.add(new Topic(ALGEBRA_1, "Review: Quadratic function.", new ReviewQuadraticFunction(), false));

        //  CUBIC FUNCTION
        TopicList.add(new Topic(ALGEBRA_1, "Finding point, range, domain of a cubic function.", new FindingPointDomainRangeOfCubicFunction()));
        TopicList.add(new Topic(ALGEBRA_1, "Transforming cubic function: shift left, right, up, down, reflect.", new TransformingCubicFunction()));
        TopicList.add(new Topic(ALGEBRA_1, "Solving cubic equation.", new SolvingCubicEquation()));
        TopicList.add(new Topic(ALGEBRA_1, "Finding the intersection of 2 functions (by using cubic equation): cubic vs quadratic vs linear.", new FindingTheIntersectionsOfCubicFunctionByEquation()));
        TopicList.add(new Topic(ALGEBRA_1, "Finding the intersection of 2 functions (by graphing calculator): cubic vs quadratic vs linear.", new FindingTheIntersectionsOfCubicFunctionByCalculator(), false));

        //  QUARTIC EQUATION
        TopicList.add(new Topic(ALGEBRA_1, "Solving quartic equation.", new SolvingQuarticEquation()));

        TopicList.add(new Topic(ALGEBRA_1, "Review: Cubic function and quartic equation.", new ReviewCubicAndQuartic(), false));

        TopicList.add(new Topic(ALGEBRA_1, "Review: Algebra 1 level.", new ReviewAlgebraOne(), false));

        //  EXPONENTIAL AND LOGARITHMIC
        TopicList.add(new Topic(ALGEBRA_2, "Converting between exponential and logarithm.", new ConvertingBetweenExpAndLog()));
        TopicList.add(new Topic(ALGEBRA_2, "Solve exponential equation without logarithm.", new SolvingExponentialEquationWithoutLog()));
        TopicList.add(new Topic(ALGEBRA_2, "Solve exponential equation with logarithm.", new SolvingExponentialEquationWithLog()));
        TopicList.add(new Topic(ALGEBRA_2, "Word problem with exponential equation.", new SolvingWordProblemWithExponentialEquation()));

        TopicList.add(new Topic(ALGEBRA_2, "Simplify logarithmic expression.", new SimplifyingLogarithmExpression()));
        TopicList.add(new Topic(ALGEBRA_2, "Solve logarithmic equation.", new SolvingLogarithmEquation()));

        TopicList.add(new Topic(ALGEBRA_2, "Review: Exponential and logarithmic.", new ReviewExponentialAndLogarithmic(), false));

        //  RATIONAL AND RADICAL FUNCTION
        TopicList.add(new Topic(ALGEBRA_2, "Finding domain, range, asymptotes of a rational function.", new FindingRangeDomainAsymptoteOfRationalFunction()));
        TopicList.add(new Topic(ALGEBRA_2, "Solving rational equation.", new SolvingRationalEquation()));

        TopicList.add(new Topic(ALGEBRA_2, "Finding domain, range of a radical function.", new FindingDomainAndRangeOfRadicalFunction()));
        TopicList.add(new Topic(ALGEBRA_2, "Solving radical equation.", new SolvingRadicalEquation()));

        TopicList.add(new Topic(ALGEBRA_2, "Review: Rational and radical function.", new ReviewRationalAndRadicalFunction(), false));

        //  MATRIX
        TopicList.add(new Topic(ALGEBRA_2, "Finding the inverse matrix.", new FindingTheInverseMatrix()));
        TopicList.add(new Topic(ALGEBRA_2, "Adding two matrices.", new AddingMatrix()));
        TopicList.add(new Topic(ALGEBRA_2, "Subtracting two matrices.", new SubtractingMatrix()));
        TopicList.add(new Topic(ALGEBRA_2, "Multiplying two matrices.", new MultiplyingMatrix()));
        TopicList.add(new Topic(ALGEBRA_2, "Dividing two matrices.", new DividingMatrix()));
        TopicList.add(new Topic(ALGEBRA_2, "Operations with matrices.", new OperationsWithMatrix()));
        TopicList.add(new Topic(ALGEBRA_2, "Solving system of equations by matrix.", new SolvingSystemOfEquationByMatrix()));
        TopicList.add(new Topic(ALGEBRA_2, "Review: Matrix.", new ReviewMatrix(), false));

        //  TRIGONOMETRY
        TopicList.add(new Topic(ALGEBRA_2, "Finding sin, cosine, tan, cot of an angle in a right triangle.", new FindingTrigValueInATriangle()));
        TopicList.add(new Topic(ALGEBRA_2, "Solving geometry problem using the sine law.", new FindingValueUsingSineLaw()));
        TopicList.add(new Topic(ALGEBRA_2, "Solving geometry problem using the cosine law.", new FindingValueUsingCosineLaw()));
        TopicList.add(new Topic(ALGEBRA_2, "Solving word problem using the cosine and sine law.", new WordProblemWithSineAndCosineLaw()));
        TopicList.add(new Topic(ALGEBRA_2, "Finding the measure of an angle using inverse trig function.", new InverseTrig()));
        TopicList.add(new Topic(ALGEBRA_2, "Finding the trig value of an angle using trig identities.", new TrigIdentities()));
        TopicList.add(new Topic(ALGEBRA_2, "Solving trig equation.", new SolvingTrigEquation()));
        TopicList.add(new Topic(ALGEBRA_2, "Review: Matrix.", new ReviewTrigonometry(), false));

        //  CIRCLE, PARABOLAS, ELLIPSES AND HYPERBOLAS
        TopicList.add(new Topic(ALGEBRA_2, "Finding the center and radius of a circle.", new FindingCenterRadiusOfCircle()));
        TopicList.add(new Topic(ALGEBRA_2, "Finding the equation of a circle.", new FindingEquationOfACircle()));
        TopicList.add(new Topic(ALGEBRA_2, "Converting the equation of a circle: standard vs general.", new ConvertingCircleEquation()));
        TopicList.add(new Topic(ALGEBRA_2, "Finding the equation of the tangent of a circle.", new FindingTheTangentOfACircle()));
        
        TopicList.add(new Topic(ALGEBRA_2, "Finding the vertex, focus, axis of symmetry, directrix of a parabolas.", new FindingParabolasElement()));
        TopicList.add(new Topic(ALGEBRA_2, "Finding equation of a parabolas.", new FindingTheEquationOfAParabolas()));
        
        TopicList.add(new Topic(ALGEBRA_2, "Finding the center, vertex, focus of an ellipses.", new FindingEllipsesElement()));
        TopicList.add(new Topic(ALGEBRA_2, "Finding equation of an ellipses.", new FindingEquationOfAnEllipses()));
        
        TopicList.add(new Topic(ALGEBRA_2, "Finding the center, vertex, focus of a hyperbolas.", new FindingHyperbolasElement()));
        TopicList.add(new Topic(ALGEBRA_2, "Finding equation of a hyperbolas.", new FindingHyperbolasEquation()));

        TopicList.add(new Topic(ALGEBRA_2, "Review: Circle, parabolas, ellipses, hyperbolas.", new ReviewAnalyticGeometry(), false));

        //  SEQUENCES AND SERIES
        TopicList.add(new Topic(ALGEBRA_2, "Arithmetic sequences.", new ArithmeticSequences()));
        TopicList.add(new Topic(ALGEBRA_2, "Geometric sequences.", new GeometricSequences()));

        TopicList.add(new Topic(ALGEBRA_2, "Arithmetic series.", new ArithmeticSeries()));
        TopicList.add(new Topic(ALGEBRA_2, "Geometric series.", new GeometricSeries()));

        TopicList.add(new Topic(ALGEBRA_2, "Review: sequences and series.", new ReviewSequencesAndSeries(), false));

        TopicList.add(new Topic(ALGEBRA_2, "Review: Algebra 2 level.", new ReviewAlgebra2Level(), false));
    }

    public static ArrayList<Topic> getTopicList(){
        return TopicList;
    }

    public static Topic getTopic(int TopicID){
        return TopicList.get(TopicID - 1);
    }

    public static Topic getTopic(String TopicName){
        Topic SearchingTopic = new Topic();

        int size = TopicList.size();
        for (int i = 0; i < size; i++){
            if (TopicList.get(i).getName().contains(TopicName)){
                SearchingTopic = TopicList.get(i);
                break;
            }
        }

        return SearchingTopic;
    }

    public static int getTopicID(String TopicName){
        int TopicID = 0;

        int size = TopicList.size();
        for (int i = 0; i < size; i++){
            if (TopicList.get(i).getName().contains(TopicName)){
                TopicID = i;
                break;
            }
        }

        return TopicID;
    }

    public static ArrayList<Topic> searchBySection(int SectionName){
        ArrayList<Topic> Result = new ArrayList<>();

        if (SectionName == R.string.stMathSection_All){
            Result = TopicList;
        }
        else {
            int size = TopicList.size();
            for (int i = 0; i < size; i++) {
                Topic SearchedTopic = TopicList.get(i);
                if (SearchedTopic.getSection() == SectionName) {
                    Result.add(SearchedTopic);
                }
            }
        }

        return Result;
    }

    public static ArrayList<Topic> searchTopic(String StName){
        ArrayList<Topic> Result = new ArrayList<>();

        int size = TopicList.size();
        for (int i = 0; i < size; i++){
            Topic SearchedTopic = TopicList.get(i);
            if (SearchedTopic.getName().trim().toLowerCase().contains(StName.trim().toLowerCase())){
                Result.add(SearchedTopic);
            }
        }

        return Result;
    }

    public static int getMaxTopicLevel(){
        return TopicList.size();
    }

    public static MathProblem getProblemFromTopicName(String TopicName){
        MathProblem Problem = TopicList.get(0).getProblem();

        int size = TopicList.size();
        for (int i = 0; i < size; i++){
            if (TopicList.get(i).getName().equalsIgnoreCase(TopicName)){
                Problem = TopicList.get(i).getProblem();
            }
        }

        return Problem;
    }

}
