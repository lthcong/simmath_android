package pckMath.pckMathTopics;

import java.util.ArrayList;

import adsfree.congla.android.cong.mymathapp.R;

/**
 * Created by Cong on 10/4/2017.
 */

public class RoundCollection {

    private static ArrayList<Round> RoundList;

    public static void setupRoundList(){
        RoundList = new ArrayList<>();

        ArrayList<Topic> TopicList = TopicCollection.getTopicList();
        int size = TopicList.size();
        int i = 1;
        for (int j = 0; j < size; j++){
            RoundList.add(new Round(i, i + 4, TopicList.get(j)));
            i = i + 5;
        }
    }

    public static ArrayList<Round> getRoundList(){
        return RoundList;
    }

    public static ArrayList<Round> getRoundList(String TopicName){
        ArrayList<Round> Result = new ArrayList<>();

        int size = RoundList.size();
        for (int i = 0; i < size; i++){
            Round SearchingRound = RoundList.get(i);
            if (SearchingRound.getLevelTopic().getName().trim().toLowerCase().contains(TopicName.trim().toLowerCase())){
                Result.add(SearchingRound);
            }
        }

        return Result;
    }

    public static ArrayList<Round> getRoundList(int SectionName){
        ArrayList<Round> Result = new ArrayList<>();

        if (SectionName == R.string.stMathSection_All){
            Result = RoundList;
        }
        else {
            int size = RoundList.size();
            for (int i = 0; i < size; i++) {
                Round SearchingRound = RoundList.get(i);
                if (SearchingRound.getLevelTopic().getSection() == SectionName) {
                    Result.add(SearchingRound);
                }
            }
        }

        return Result;
    }

    public static Topic getTopic(int Level){
        Topic SelectedTopic = RoundList.get(0).getLevelTopic();

        int size = RoundList.size();
        for (int i = 0; i < size; i++){
            if (Level <= RoundList.get(i).getMaxLevel()){
                SelectedTopic = RoundList.get(i).getLevelTopic();
                break;
            }
        }

        return SelectedTopic;
    }

}
