package pckMath.pckMathTopics;

import pckMath.MathProblem;

/**
 * Created by Cong on 4/26/2017.
 */

public class Topic {

    private int SECTION_NAME, CHAPTER;
    private String NAME;
    private MathProblem PROBLEM;
    private boolean IS_ASSESSMENT;

    public Topic(){
        SECTION_NAME = 0;
        CHAPTER = 0;
        NAME = "";
    }

    public Topic(String Name){
        SECTION_NAME = 0;
        CHAPTER = 0;
        NAME = Name;
    }

    public Topic(int SectionName, String Name){
        SECTION_NAME = SectionName;
        CHAPTER = 0;
        NAME = Name;
    }

    public Topic(int SectionName, int Chapter, String Name){
        SECTION_NAME = SectionName;
        CHAPTER = Chapter;
        NAME = Name;
    }

    public Topic(int SectionName, String Name, MathProblem Problem){
        SECTION_NAME = SectionName;
        NAME = Name;
        PROBLEM = Problem;
        IS_ASSESSMENT = true;
    }

    public Topic(int SectionName, int Chapter, String Name, MathProblem Problem){
        SECTION_NAME = SectionName;
        CHAPTER = Chapter;
        NAME = Name;
        PROBLEM = Problem;
        IS_ASSESSMENT = true;
    }

    public Topic(int SectionName, String Name, MathProblem Problem, boolean isAssessment){
        SECTION_NAME = SectionName;
        NAME = Name;
        PROBLEM = Problem;
        IS_ASSESSMENT = isAssessment;
    }

    public Topic(int SectionName, int Chapter, String Name, MathProblem Problem, boolean isAssessment){
        SECTION_NAME = SectionName;
        CHAPTER = Chapter;
        NAME = Name;
        PROBLEM = Problem;
        IS_ASSESSMENT = isAssessment;
    }

    public int getSection(){
        return SECTION_NAME;
    }

    public int getChapter(){
        return CHAPTER;
    }

    public String getName(){
        return NAME;
    }

    public MathProblem getProblem(){
        return PROBLEM.createProblem();
    }

    public boolean isAssessment(){
        return IS_ASSESSMENT;
    }

    public boolean isNull(){
        boolean NullTopic = false;

        if (NAME.trim().length() == 0){
            NullTopic = true;
        }

        return NullTopic;
    }
}
