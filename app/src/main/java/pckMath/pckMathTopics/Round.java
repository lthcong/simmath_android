package pckMath.pckMathTopics;

/**
 * Created by Cong on 10/4/2017.
 */

public class Round {

    private int MIN_LEVEL, MAX_LEVEL;
    private Topic LEVEL_TOPIC;

    public Round(int MinLevel, int MaxLevel, Topic LevelTopic){
        MIN_LEVEL = MinLevel;
        MAX_LEVEL = MaxLevel;
        LEVEL_TOPIC = LevelTopic;
    }

    public int getMinLevel(){
        return MIN_LEVEL;
    }

    public int getMaxLevel(){
        return MAX_LEVEL;
    }

    public Topic getLevelTopic(){
        return LEVEL_TOPIC;
    }

}
