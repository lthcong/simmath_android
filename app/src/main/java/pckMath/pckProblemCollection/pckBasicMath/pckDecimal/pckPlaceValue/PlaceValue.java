package pckMath.pckProblemCollection.pckBasicMath.pckDecimal.pckPlaceValue;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 5/16/2017.
 */

public class PlaceValue extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public PlaceValue(){
        createProblem();
        swapAnswer();
    }

    public PlaceValue(String Question,
                           String RightAnswer,
                           String AnswerA,
                           String AnswerB,
                           String AnswerC,
                           String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblemType = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblemType){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }

        return new PlaceValue(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  TENTHS PLACE
    private void createProblem_01(){
        double Number = MathUtility.getRandomDecimalNumber();
        String StNumber = String.valueOf(Number);
        int DecimalIndex = StNumber.indexOf(".");
        int Result;
        try{
            Result = Integer.parseInt(StNumber.substring(DecimalIndex + 1, DecimalIndex + 2));
        }
        catch (Exception e){
            Result = 0;
        }


        QUESTION = "Find the number at the tenths place of " + StNumber;
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);

        if (Result > 0){
            ANSWER_D = String.valueOf(Result - 1);
        }
        else {
            ANSWER_D = String.valueOf(Result + 3);
        }
    }

    //  HUNDREDTHS PLACE
    private void createProblem_02(){
        double Number = MathUtility.getRandomDecimalNumber();
        String StNumber = String.valueOf(Number);
        int DecimalIndex = StNumber.indexOf(".");
        int Result;
        try{
            Result = Integer.parseInt(StNumber.substring(DecimalIndex + 2, DecimalIndex + 3));
        }
        catch (Exception e){
            Result = 0;
        }


        QUESTION = "Find the number at the hundredths place of " + StNumber;
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);

        if (Result > 0){
            ANSWER_D = String.valueOf(Result - 1);
        }
        else {
            ANSWER_D = String.valueOf(Result + 3);
        }
    }

    //  THOUSANDTHS PLACE
    private void createProblem_03(){
        double Number = MathUtility.getRandomDecimalNumber();
        String StNumber = String.valueOf(Number);
        int DecimalIndex = StNumber.indexOf(".");
        int Result;
        try{
            Result = Integer.parseInt(StNumber.substring(DecimalIndex + 3, DecimalIndex + 4));
        }
        catch (Exception e){
            Result = 0;
        }


        QUESTION = "Find the number at the thousandths place of " + StNumber;
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);

        if (Result > 0){
            ANSWER_D = String.valueOf(Result - 1);
        }
        else {
            ANSWER_D = String.valueOf(Result + 3);
        }
    }

    //  ONES
    private void createProblem_04(){
        double Number = MathUtility.getRandomDecimalNumber();
        String StNumber = String.valueOf(Number);
        int DecimalIndex = StNumber.indexOf(".");
        int Result;
        try{
            Result = Integer.parseInt(StNumber.substring(DecimalIndex - 1, DecimalIndex));
        }
        catch (Exception e){
            Result = 0;
        }


        QUESTION = "Find the number at the ones place of " + StNumber;
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);

        if (Result > 0){
            ANSWER_D = String.valueOf(Result - 1);
        }
        else {
            ANSWER_D = String.valueOf(Result + 3);
        }
    }

    //  TENS
    private void createProblem_05(){
        double Number = MathUtility.getRandomDecimalNumber();
        String StNumber = String.valueOf(Number);
        int DecimalIndex = StNumber.indexOf(".");
        int Result;
        try{
            Result = Integer.parseInt(StNumber.substring(DecimalIndex - 2, DecimalIndex - 1));
        }
        catch (Exception e){
            Result = 0;
        }


        QUESTION = "Find the number at the tens place of " + StNumber;
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);

        if (Result > 0){
            ANSWER_D = String.valueOf(Result - 1);
        }
        else {
            ANSWER_D = String.valueOf(Result + 3);
        }
    }

    //  HUNDREDS
    private void createProblem_06(){
        double Number = MathUtility.getRandomDecimalNumber();
        String StNumber = String.valueOf(Number);
        int DecimalIndex = StNumber.indexOf(".");
        int Result;
        try{
            Result = Integer.parseInt(StNumber.substring(DecimalIndex - 3, DecimalIndex - 2));
        }
        catch (Exception e){
            Result = 0;
        }


        QUESTION = "Find the number at the hundreds place of " + StNumber;
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);

        if (Result > 0){
            ANSWER_D = String.valueOf(Result - 1);
        }
        else {
            ANSWER_D = String.valueOf(Result + 3);
        }
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
