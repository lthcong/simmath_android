package pckMath.pckProblemCollection.pckBasicMath.pckDecimal.pckAdd;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 5/16/2017.
 */

public class AddingDecimal extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public AddingDecimal(){
        createProblem();
        swapAnswer();
    }

    public AddingDecimal(String Question,
                       String RightAnswer,
                       String AnswerA,
                       String AnswerB,
                       String AnswerC,
                       String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 11;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            default:
                createProblem_01();
                break;
        }

        return new AddingDecimal(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  1 DEC + 1 DEC
    private void createProblem_01(){
        double NumberA = MathUtility.getRandomDecimalNumber_1Digit();
        double NumberB = MathUtility.getRandomDecimalNumber_1Digit();

        double Result = MathUtility.round_1_Decimal(NumberA + NumberB);

        QUESTION = String.valueOf(NumberA) + " + " + String.valueOf(NumberB) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Result - 0.1));
    }

    //  2 DEC + 1 DEC
    private void createProblem_02(){
        double NumberA = MathUtility.getRandomDecimalNumber_2Digit();
        double NumberB = MathUtility.getRandomDecimalNumber_1Digit();

        double Result = MathUtility.round_2_Decimal(NumberA + NumberB);

        QUESTION = String.valueOf(NumberA) + " + " + String.valueOf(NumberB) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 0.01));
    }

    //  1 DEC + 2 DEC
    private void createProblem_03(){
        double NumberA = MathUtility.getRandomDecimalNumber_1Digit();
        double NumberB = MathUtility.getRandomDecimalNumber_2Digit();

        double Result = MathUtility.round_2_Decimal(NumberA + NumberB);

        QUESTION = String.valueOf(NumberA) + " + " + String.valueOf(NumberB) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 0.01));
    }

    //  2 DEC + 2 DEC
    private void createProblem_04(){
        double NumberA = MathUtility.getRandomDecimalNumber_2Digit();
        double NumberB = MathUtility.getRandomDecimalNumber_2Digit();

        double Result = MathUtility.round_2_Decimal(NumberA + NumberB);

        QUESTION = String.valueOf(NumberA) + " + " + String.valueOf(NumberB) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 0.01));
    }

    //  3 DEC + 1 DEC
    private void createProblem_05(){
        double NumberA = MathUtility.getRandomDecimalNumber_3Digit();
        double NumberB = MathUtility.getRandomDecimalNumber_1Digit();

        double Result = MathUtility.round_3_Decimal(NumberA + NumberB);

        QUESTION = String.valueOf(NumberA) + " + " + String.valueOf(NumberB) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_3_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_3_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_3_Decimal(Result + 0.01));
    }

    //  1 DEC + 3 DEC
    private void createProblem_06(){
        double NumberA = MathUtility.getRandomDecimalNumber_1Digit();
        double NumberB = MathUtility.getRandomDecimalNumber_3Digit();

        double Result = MathUtility.round_3_Decimal(NumberA + NumberB);

        QUESTION = String.valueOf(NumberA) + " + " + String.valueOf(NumberB) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_3_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_3_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_3_Decimal(Result + 0.01));
    }

    //  3 DEC + 2 DEC
    private void createProblem_07(){
        double NumberA = MathUtility.getRandomDecimalNumber_3Digit();
        double NumberB = MathUtility.getRandomDecimalNumber_2Digit();

        double Result = MathUtility.round_3_Decimal(NumberA + NumberB);

        QUESTION = String.valueOf(NumberA) + " + " + String.valueOf(NumberB) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_3_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_3_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_3_Decimal(Result + 0.01));
    }

    //  2 DEC + 3 DEC
    private void createProblem_08(){
        double NumberA = MathUtility.getRandomDecimalNumber_2Digit();
        double NumberB = MathUtility.getRandomDecimalNumber_3Digit();

        double Result = MathUtility.round_3_Decimal(NumberA + NumberB);

        QUESTION = String.valueOf(NumberA) + " + " + String.valueOf(NumberB) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_3_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_3_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_3_Decimal(Result + 0.01));
    }

    //  NUM + 1 DEC
    private void createProblem_09(){
        double NumberA = (double) MathUtility.getRandomPositiveNumber();
        double NumberB = MathUtility.getRandomDecimalNumber_1Digit();

        double Result = MathUtility.round_1_Decimal(NumberA + NumberB);

        QUESTION = String.valueOf(NumberA) + " + " + String.valueOf(NumberB) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Result - 0.1));
    }

    //  NUM + 2 DEC
    private void createProblem_10(){
        double NumberA = (double) MathUtility.getRandomPositiveNumber();
        double NumberB = MathUtility.getRandomDecimalNumber_2Digit();

        double Result = MathUtility.round_2_Decimal(NumberA + NumberB);

        QUESTION = String.valueOf(NumberA) + " + " + String.valueOf(NumberB) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result - 0.1));
    }

    //  3 DEC + 3 DEC
    private void createProblem_11(){
        double NumberA = MathUtility.getRandomDecimalNumber_3Digit();
        double NumberB = MathUtility.getRandomDecimalNumber_3Digit();

        double Result = MathUtility.round_3_Decimal(NumberA + NumberB);

        QUESTION = String.valueOf(NumberA) + " + " + String.valueOf(NumberB) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_3_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_3_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_3_Decimal(Result + 0.01));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
