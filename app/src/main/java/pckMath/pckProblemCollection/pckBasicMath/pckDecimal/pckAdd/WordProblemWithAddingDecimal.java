package pckMath.pckProblemCollection.pckBasicMath.pckDecimal.pckAdd;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 5/16/2017.
 */

public class WordProblemWithAddingDecimal extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public WordProblemWithAddingDecimal(){
        createProblem();
        swapAnswer();
    }

    public WordProblemWithAddingDecimal(String Question,
                          String RightAnswer,
                          String AnswerA,
                          String AnswerB,
                          String AnswerC,
                          String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 10;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            default:
                createProblem_01();
                break;
        }

        return new WordProblemWithAddingDecimal(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());

        double Result = MathUtility.round_1_Decimal(NumberA + NumberB);


        QUESTION = "Mike rode " + String.valueOf(NumberA) + " miles at the first day, and " + String.valueOf(NumberB) + " miles at the second one. "
                + "How far did he ride?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Result + 0.01));
    }

    private void createProblem_02(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());

        double Result = MathUtility.round_1_Decimal(NumberA + NumberB);


        QUESTION = "A tree grew " + String.valueOf(NumberA) + " inches last week. And " + String.valueOf(NumberB) + " inches this week. "
                + "How long is the tree now?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Result + 0.01));
    }

    private void createProblem_03(){
        double NumberA = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_4Digit());
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());

        double Result = MathUtility.round_2_Decimal(NumberA + NumberB);


        QUESTION = "Mike drove " + String.valueOf(NumberA) + " miles to visit his parents. And " + String.valueOf(NumberB) + " miles "
                + "to the gas station. How many miles did he drive?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 0.01));
    }

    private void createProblem_04(){
        double NumberA = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_3Digit());
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());

        double Result = MathUtility.round_2_Decimal(NumberA + NumberB);


        QUESTION = "Tom bought " + String.valueOf(NumberA) + " pounds of apples and " + String.valueOf(NumberB) + " pounds of banana. "
                + "How many pounds of fruit did Tom buy?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 0.01));
    }

    private void createProblem_05(){
        double NumberA = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_4Digit()) + 100;
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_3Digit());

        double Result = MathUtility.round_2_Decimal(NumberA + NumberB);


        QUESTION = "A pig was " + String.valueOf(NumberA) + " pounds last week, and increase "
                + String.valueOf(NumberB) + " pounds this week. How heavy is it now?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 0.01));
    }

    private void createProblem_06(){
        double NumberA = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_4Digit());
        double NumberB = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_4Digit());

        double Result = MathUtility.round_2_Decimal(NumberA + NumberB);


        QUESTION = "Jack has " + String.valueOf(NumberA) + "USD in his saving. His dad gives him " + String.valueOf(NumberB) + "USD. "
                + "How much money is he having now?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 0.01));
    }

    private void createProblem_07(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_3Digit());
        double NumberB = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_3Digit());

        double Result = MathUtility.round_2_Decimal(NumberA + NumberB);


        QUESTION = "There are 2 pieces of wood. The first one is " + String.valueOf(NumberA) + " feet long. The second one is "
                + String.valueOf(NumberB) + " feet long. If both of them are put together, how long is the total length?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 0.01));
    }

    private void createProblem_08(){
        double NumberA = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_3Digit());
        double NumberB = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_3Digit());

        double Result = MathUtility.round_2_Decimal(NumberA + NumberB);


        QUESTION = "An animal drinks " + String.valueOf(NumberA) + " liters of water. Another one drinks " + String.valueOf(NumberB) + " litters. "
                + "How many litters of water do they drink?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 0.01));
    }

    private void createProblem_09(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_3Digit());
        double NumberB = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_4Digit());

        double Result = MathUtility.round_2_Decimal(NumberA + NumberB);


        QUESTION = "A water tap is draining out a pool. " + String.valueOf(NumberA) + " litters of water ath the first hour. "
                + String.valueOf(NumberB) + " litters at the second one. How many liters of water was drained out of the pool?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 0.01));
    }

    private void createProblem_10(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        double NumberB = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_3Digit());

        double Result = MathUtility.round_2_Decimal(NumberA + NumberB);


        QUESTION = "An animal consumes " + String.valueOf(NumberA) + " pounds of food. Another one consumes " + String.valueOf(NumberB)
                + " pounds. How many pounds of food do they consume?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 0.01));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
