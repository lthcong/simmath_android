package pckMath.pckProblemCollection.pckBasicMath.pckDecimal.pckMul;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 5/17/2017.
 */

public class WordProblemWithMultiplyingDecimal extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public WordProblemWithMultiplyingDecimal(){
        createProblem();
        swapAnswer();
    }

    public WordProblemWithMultiplyingDecimal(String Question,
                                              String RightAnswer,
                                              String AnswerA,
                                              String AnswerB,
                                              String AnswerC,
                                              String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 10;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            default:
                createProblem_01();
                break;
        }

        return new WordProblemWithMultiplyingDecimal(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_3Digit());
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());

        double Result = MathUtility.round_2_Decimal(NumberA * NumberB);

        QUESTION = "Jack's car can run " + String.valueOf(NumberA) + " kilometer per hours. Jame's run "
                + String.valueOf(NumberB) + " times faster. How fast Jame's car can run?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result * 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_3_Decimal(Result * 10));
        ANSWER_D = String.valueOf(MathUtility.round_3_Decimal(Result * 0.01));
    }

    private void createProblem_02(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());

        double Result = MathUtility.round_2_Decimal(NumberA * NumberB);

        QUESTION = "A water tap can drain out " + String.valueOf(NumberA) + " litters of water per hours. "
                + "How many litters of water it can drain after " + String.valueOf(NumberB) + " hours?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result * 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_3_Decimal(Result * 10));
        ANSWER_D = String.valueOf(MathUtility.round_3_Decimal(Result * 0.01));
    }

    private void createProblem_03(){
        double NumberA = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_2Digit());
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());

        double Result = MathUtility.round_3_Decimal(NumberA * NumberB);

        QUESTION = "Tom can bike " + String.valueOf(NumberA) + " miles per hours. How far can he bike after " + String.valueOf(NumberB) + " hours?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Result * 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Result * 10));
        ANSWER_D = String.valueOf(MathUtility.round_3_Decimal(Result * 0.01));
    }

    private void createProblem_04(){
        double NumberA = (double) MathUtility.getRandomPositiveNumber_2Digit();
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());

        double Result = MathUtility.round_1_Decimal(NumberA * NumberB);

        QUESTION = "Terry can type " + String.valueOf(NumberA) + " words per minute. How many words can he type in "
                + String.valueOf(NumberB) + " minutes?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_3_Decimal(Result * 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_3_Decimal(Result * 10));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Result * 0.01));
    }

    private void createProblem_05(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_4Digit());
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());

        double Result = MathUtility.round_2_Decimal(NumberA * NumberB);

        QUESTION = "An air plane can fly " + String.valueOf(NumberA) + " miles per hours. How far can it go after "
                + String.valueOf(NumberB) + " hours?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result * 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_3_Decimal(Result * 10));
        ANSWER_D = String.valueOf(MathUtility.round_3_Decimal(Result * 0.01));
    }

    private void createProblem_06(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        double NumberB = MathUtility.getRandomDecimalNumber_1Digit() + 1;

        double Result = MathUtility.round_2_Decimal(NumberA * NumberB);

        QUESTION = "A plank of wood is cutting into " + String.valueOf(NumberB) + " smaller pieces. Each of them is " + String.valueOf(NumberA)
                + " inches. How long is the original plank wood?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result * 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_3_Decimal(Result * 10));
        ANSWER_D = String.valueOf(MathUtility.round_3_Decimal(Result * 0.01));
    }

    private void createProblem_07(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());

        double Result = MathUtility.round_2_Decimal(NumberA * NumberB);

        QUESTION = "Each pound of fruit costs " + String.valueOf(NumberA) + "USD. How much do " + String.valueOf(NumberB) + " pounds of fruit cost?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result * 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_3_Decimal(Result * 10));
        ANSWER_D = String.valueOf(MathUtility.round_3_Decimal(Result * 0.01));
    }

    private void createProblem_08(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        double NumberB = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        double Result = MathUtility.round_1_Decimal(NumberA * NumberB);

        QUESTION = "A tailor needs " + String.valueOf(NumberA) + " yards of fabric to make a dress. How many yards of fabric does he need to make "
                + String.valueOf(NumberB) + " dresses?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_3_Decimal(Result * 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_3_Decimal(Result * 10));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Result * 0.01));
    }

    private void createProblem_09(){
        double NumberA = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_2Digit());
        double NumberB = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_2Digit());

        double Result = MathUtility.round_4_Decimal(NumberA * NumberB);

        QUESTION = "A recipe needs " + String.valueOf(NumberA) + " ounces of sugar. 1 ounce of sugar needs " + String.valueOf(NumberB)
                + " ounces of water? How much water does the recipe need?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result * 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result * 10));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Result * 0.01));
    }

    private void createProblem_10(){
        double NumberA = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_4Digit());
        double NumberB = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_3Digit());

        double Result = MathUtility.round_4_Decimal(NumberA * NumberB);

        QUESTION = "A car is running at the speed of " + String.valueOf(NumberA) + " kilometer per hours. How far it can go after "
                + String.valueOf(NumberB) + " hours?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result * 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result * 10));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Result * 0.01));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
