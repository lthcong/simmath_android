package pckMath.pckProblemCollection.pckBasicMath.pckDecimal.pckSub;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 5/17/2017.
 */

public class WordProblemWithSubtractingDecimal extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public WordProblemWithSubtractingDecimal(){
        createProblem();
        swapAnswer();
    }

    public WordProblemWithSubtractingDecimal(String Question,
                                         String RightAnswer,
                                         String AnswerA,
                                         String AnswerB,
                                         String AnswerC,
                                         String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 10;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            default:
                createProblem_01();
                break;
        }

        return new WordProblemWithSubtractingDecimal(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        double NumberB = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_2Digit());
        NumberA = MathUtility.round_2_Decimal(NumberA + NumberB);

        double Result = MathUtility.round_2_Decimal(NumberA - NumberB);

        QUESTION = "Yesterday, it snowed " + String.valueOf(NumberA) + " inches. Today it snows " + String.valueOf(NumberB)
                + " inches less than yesterday. How much snow do we have today?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 0.01));
    }

    private void createProblem_02(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_3Digit());
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        NumberA = MathUtility.round_1_Decimal(NumberA + NumberB);

        double Result = MathUtility.round_1_Decimal(NumberA - NumberB);

        QUESTION = "Tom has 2 trees in his garden. The first one is " + String.valueOf(NumberA) + " feet. The second one is "
                + String.valueOf(NumberB) + " feet less than the first one. How high is the second tree?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Result + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Result + 0.01));
    }

    private void createProblem_03(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        NumberA = MathUtility.round_1_Decimal(NumberA + NumberB);

        double Result = MathUtility.round_1_Decimal(NumberA - NumberB);

        QUESTION = "An animal drinks " + String.valueOf(NumberA) + " litters of water. The second one drinks " + String.valueOf(NumberB)
                + " litters of water. How much water does the first one drink more than the second one?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Result + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Result + 0.01));
    }

    private void createProblem_04(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        NumberA = MathUtility.round_1_Decimal(NumberA + NumberB);

        double Result = MathUtility.round_1_Decimal(NumberA - NumberB);

        QUESTION = "Two water tap are draining out water from a pool. The first one can drain " + String.valueOf(NumberA)
                + " litters of water per hour. The second one drains " + String.valueOf(NumberB) + " litters of water per hour. "
                + "How many litters of water the first one can drain more than the second one?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Result + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Result + 0.01));
    }

    private void createProblem_05(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_3Digit());
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_3Digit());
        NumberA = MathUtility.round_1_Decimal(NumberA + NumberB);

        double Result = MathUtility.round_1_Decimal(NumberA - NumberB);

        QUESTION = "Terry cut the rope into two pieces. The first one is " + String.valueOf(NumberA) + " inches. The second one is "
                + String.valueOf(NumberB) + " inches less than the first one. How long is the second one?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Result + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Result + 0.01));
    }

    private void createProblem_06(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        NumberA = MathUtility.round_1_Decimal(NumberA + NumberB);

        double Result = MathUtility.round_1_Decimal(NumberA - NumberB);

        QUESTION = "From Jack's home to school is " + String.valueOf(NumberA) + " miles. From home to the gas station is "
                + String.valueOf(NumberB) + " miles. How much further is it from home to school than from home to the gas station?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Result + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Result + 0.01));
    }

    private void createProblem_07(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        NumberA = MathUtility.round_1_Decimal(NumberA + NumberB);

        double Result = MathUtility.round_1_Decimal(NumberA - NumberB);

        QUESTION = "Jame has a " + String.valueOf(NumberA) + " inches long ruler. His sister broke it a part. The first one is "
                + String.valueOf(NumberB) + " inches long. How long is the second one?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Result + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Result + 0.01));
    }

    private void createProblem_08(){
        double NumberA = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_4Digit());
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_3Digit());
        NumberA = MathUtility.round_2_Decimal(NumberA + NumberB);

        double Result = MathUtility.round_2_Decimal(NumberA - NumberB);

        QUESTION = "In 2 days, a fisher man caught " + String.valueOf(NumberA) + " tons of fish. The first day he caught "
                + String.valueOf(NumberB) + " ton. How many tons of fish did he catch at the second day?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 0.01));
    }

    private void createProblem_09(){
        double NumberA = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        double NumberB = MathUtility.getDecimalNumber_1Digit(MathUtility.getRandomPositiveNumber_2Digit());
        NumberA = MathUtility.round_1_Decimal(NumberA + NumberB);

        double Result = MathUtility.round_1_Decimal(NumberA - NumberB);

        QUESTION = "Tom had " + String.valueOf(NumberA) + "USD. He spent " + String.valueOf(NumberB)
                + "USD at the coffee shop. How much money did he have left?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Result + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Result + 0.01));
    }

    private void createProblem_10(){
        double NumberA = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_2Digit());
        double NumberB = MathUtility.getDecimalNumber_2Digit(MathUtility.getRandomPositiveNumber_2Digit());
        NumberA = MathUtility.round_2_Decimal(NumberA + NumberB);

        double Result = MathUtility.round_2_Decimal(NumberA - NumberB);

        QUESTION = "Jame walked " + String.valueOf(NumberA) + " miles for morning exercise, which included " + String.valueOf(NumberB)
                + " miles more than yesterday. How far did he walk yesterday.";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 0.01));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
