package pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckTypeOfFraction;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.MixedNumber;

/**
 * Created by Cong on 5/15/2017.
 */

public class TypeOfFraction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;
    private String PROPER_FRACTION = "Proper Fraction.";
    private String IMPROPER_FRACTION = "Improper Fraction.";
    private String MIXED_NUMBER = "Mixed Number.";
    private String NEITHER = "Neither.";

    public TypeOfFraction(){
        createProblem();
        swapAnswer();
    }

    public TypeOfFraction(String Question,
                                                   String RightAnswer,
                                                   String AnswerA,
                                                   String AnswerB,
                                                   String AnswerC,
                                                   String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblemType = 3;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblemType){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            default:
                createProblem_01();
                break;
        }

        return new TypeOfFraction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  PROPER FRACTION
    private void createProblem_01(){
        Fraction ProperFraction = FractionMath.getRandomProperFraction();

        QUESTION = ProperFraction.toString() + " is a ... .";
        RIGHT_ANSWER = PROPER_FRACTION;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = IMPROPER_FRACTION;
        ANSWER_C = MIXED_NUMBER;
        ANSWER_D = NEITHER;
    }

    //  IMPROPER FRACTION
    private void createProblem_02(){
        Fraction ProperFraction = FractionMath.getRandomImproperFraction();

        QUESTION = ProperFraction.toFullString() + " is a ... .";
        RIGHT_ANSWER = IMPROPER_FRACTION;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = PROPER_FRACTION;
        ANSWER_C = MIXED_NUMBER;
        ANSWER_D = NEITHER;
    }

    //  MIXED NUMBER
    private void createProblem_03(){
        MixedNumber Number = FractionMath.getRandomMixedNumber();

        QUESTION = Number.toString() + " is a ... .";
        RIGHT_ANSWER = MIXED_NUMBER;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = IMPROPER_FRACTION;
        ANSWER_C = PROPER_FRACTION;
        ANSWER_D = NEITHER;
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }


}
