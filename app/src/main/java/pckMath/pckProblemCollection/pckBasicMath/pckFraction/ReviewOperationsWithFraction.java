package pckMath.pckProblemCollection.pckBasicMath.pckFraction;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckAdd.ReviewAddingFraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckDiv.ReviewDividingFraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckMul.ReviewMultiplyingFraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckSub.ReviewSubtractingFraction;

/**
 * Created by Cong on 9/6/2017.
 */

public class ReviewOperationsWithFraction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public ReviewOperationsWithFraction(){
        createProblem();
        swapAnswer();
    }

    public ReviewOperationsWithFraction(String Question,
                                  String RightAnswer,
                                  String AnswerA,
                                  String AnswerB,
                                  String AnswerC,
                                  String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblemType = 4;
        MathProblem Problem;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblemType){
            case 0:
                Problem = new ReviewAddingFraction();
                break;
            case 1:
                Problem = new ReviewSubtractingFraction();
                break;
            case 2:
                Problem = new ReviewMultiplyingFraction();
                break;
            case 3:
                Problem = new ReviewDividingFraction();
                break;
            default:
                Problem = new ReviewAddingFraction();
                break;
        }

        QUESTION = Problem.getQuestion();
        RIGHT_ANSWER = Problem.getRightAnswer();
        ANSWER_A = Problem.getAnswerA();
        ANSWER_B = Problem.getAnswerB();
        ANSWER_C = Problem.getAnswerC();
        ANSWER_D = Problem.getAnswerD();

        return new ReviewOperationsWithFraction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
