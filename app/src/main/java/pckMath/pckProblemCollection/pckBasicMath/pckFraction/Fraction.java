package pckMath.pckProblemCollection.pckBasicMath.pckFraction;

import pckInfo.InfoCollector;

/**
 * Created by Cong on 5/12/2017.
 */

public class Fraction {

    private int NUMERATOR, DENOMINATOR;

    public Fraction(){
        NUMERATOR = 0;
        DENOMINATOR = 1;
    }

    public Fraction(int Numerator){
        NUMERATOR = Numerator;
        DENOMINATOR = 1;
    }

    public Fraction(int Numerator, int Denominator){
        NUMERATOR = Numerator;
        DENOMINATOR = Denominator;
    }

    public void setNumerator(int Numerator){
        NUMERATOR = Numerator;
    }

    public int getNumerator(){
        return NUMERATOR;
    }

    public void setDenominator(int Denominator){
        DENOMINATOR = Denominator;
    }

    public int getDenominator(){
        return DENOMINATOR;
    }

    public double getValue(){
        double FractionValue;

        if (DENOMINATOR != 0){
            FractionValue = ((double) NUMERATOR / (double) DENOMINATOR);
        }
        else {
            FractionValue = Double.MAX_VALUE;
        }

        return FractionValue;
    }

    public boolean isEquivalent(Fraction FRACTION){
        boolean isTheSame = false;

        if (this.getValue() == FRACTION.getValue()){
            isTheSame = true;
        }

        return isTheSame;
    }

    public boolean isInteger(){
        boolean is_integer;
        if (DENOMINATOR != 0 && NUMERATOR % DENOMINATOR == 0){
            is_integer = true;
        }
        else {
            is_integer = false;
        }

        return is_integer;
    }

    public String toString(){
        String FractionString = "";

        if (DENOMINATOR != 0 && NUMERATOR % DENOMINATOR == 0) {
            FractionString = String.valueOf(NUMERATOR / DENOMINATOR);
        } else {
            FractionString = InfoCollector.getStartNumerator() + NUMERATOR + InfoCollector.getEndNumerator()
                    + "/"
                    + InfoCollector.getStartDenominator() + DENOMINATOR + InfoCollector.getEndDenominator();
        }

        return FractionString;
    }

    public String toFullString(){
        return InfoCollector.getStartNumerator() + NUMERATOR + InfoCollector.getEndNumerator()
                + "/"
                + InfoCollector.getStartDenominator() + DENOMINATOR + InfoCollector.getEndDenominator();
    }

    public String toSimpleString(){
        return String.valueOf(NUMERATOR) + "/" + String.valueOf(DENOMINATOR);
    }

    public boolean isNull(){
        boolean NullFraction = false;
        if (DENOMINATOR == 0){
            NullFraction = true;
        }

        return NullFraction;
    }

}
