package pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckDiv;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.MixedNumber;

/**
 * Created by Cong on 5/15/2017.
 */

public class WordProblemWithDividingFraction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public WordProblemWithDividingFraction(){
        createProblem();
        swapAnswer();
    }

    public WordProblemWithDividingFraction(String Question,
                                               String RightAnswer,
                                               String AnswerA,
                                               String AnswerB,
                                               String AnswerC,
                                               String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblemType = 11;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblemType){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            default:
                createProblem_01();
                break;
        }

        return new WordProblemWithDividingFraction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.getRandomFraction();

        Fraction Result = FractionMath.divideFraction(FractionB, FractionA);

        QUESTION = "A recipe requires " + FractionA.toString() + " cups of water and " + FractionB.toString() + " ounces of sugar. "
                + "How many sugar does it need for 1 cup of water?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getReciprocal(Result).toString();
    }

    private void createProblem_02(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.getRandomFraction();

        Fraction Result = FractionMath.divideFraction(FractionA, FractionB);

        QUESTION = "Jame has to run " + FractionA.toString() + " miles in " + FractionB.toString() + " hours. "
                + "How fast he has to run?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getReciprocal(Result).toString();
    }

    private void createProblem_03(){
        Fraction FractionA = FractionMath.getRandomProperFraction();
        Fraction FractionB = FractionMath.getRandomImproperFraction();

        Fraction Result = FractionMath.divideFraction(FractionB, FractionA);

        QUESTION = "The tree can grow " + FractionA.toString() + " inches. How long does it take to grow up to " + FractionB.toString() + "?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getReciprocal(Result).toString();
    }

    private void createProblem_04(){
        MixedNumber Number = FractionMath.getRandomMixedNumber();
        Fraction FRACTION = FractionMath.getRandomFraction();

        Fraction Result = FractionMath.divideFraction(FractionMath.convertToFraction(Number), FRACTION);

        QUESTION = "1 ounces of mix requires " + FRACTION.toString() + " ounces of water. How many ounces of mix can be made from " + Number.toString() + " ounces of water?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = Number.toString();
        ANSWER_C = FRACTION.toString();
        ANSWER_D = FractionMath.getReciprocal(Result).toString();
    }

    private void createProblem_05(){
        int Number = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        Fraction FRACTION = new Fraction(1, MathUtility.getRandomPositiveNumber_1Digit());

        Fraction Result = FractionMath.divideFraction(new Fraction(Number), FRACTION);

        QUESTION = "Tom wants to cut a " + String.valueOf(Number) + " feet long plank wood into smaller pieces. Each piece is "
                + FRACTION.toString() + " feet long. How many pieces can he make?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FRACTION.toString();
        ANSWER_C = String.valueOf(Number);
        ANSWER_D = FractionMath.getReciprocal(Result).toString();
    }

    private void createProblem_06(){
        int NoPieces = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int Length = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        Fraction Result = new Fraction(Length, NoPieces);
        Result = FractionMath.getSimplifiedFraction(Result);

        QUESTION = "Tom wants to cut a " + String.valueOf(Length) + " feet plank wood into " + String.valueOf(NoPieces) + " pieces. "
                + "How long is each piece?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(NoPieces);
        ANSWER_C = String.valueOf(Length);
        ANSWER_D = FractionMath.getReciprocal(Result).toString();
    }

    private void createProblem_07(){
        MixedNumber NumberA = FractionMath.getRandomMixedNumber();
        MixedNumber NumberB = FractionMath.getRandomMixedNumber();

        Fraction Result = FractionMath.divideFraction(FractionMath.convertToFraction(NumberA), FractionMath.convertToFraction(NumberB));

        QUESTION = "Mike wants to run " + NumberA.toString() + " miles at the speed of " + NumberB.toString() + " miles per hour. "
                + "How long does it take him to complete the run?" ;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = NumberA.toString();
        ANSWER_C = NumberB.toString();
        ANSWER_D = FractionMath.getReciprocal(Result).toString();
    }

    private void createProblem_08(){
        MixedNumber NumberA = FractionMath.getRandomMixedNumber();
        MixedNumber NumberB = FractionMath.getRandomMixedNumber();

        Fraction Result = FractionMath.divideFraction(FractionMath.convertToFraction(NumberA), FractionMath.convertToFraction(NumberB));

        QUESTION = "Tom can ride " + NumberA.toString() + " miles per hours. Terry rides " + NumberB.toString() + " miles per hours. "
                + "How many times can the faster rider ride than the slower one?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = NumberA.toString();
        ANSWER_C = NumberB.toString();
        ANSWER_D = FractionMath.getReciprocal(Result).toString();
    }

    private void createProblem_09(){
        Fraction FractionA = FractionMath.getRandomImproperFraction();
        Fraction FractionB = FractionMath.getRandomFraction();

        Fraction Result = FractionMath.divideFraction(FractionB, FractionA);

        QUESTION = "Jack is riding at the speed of " + FractionA.toString() + " miles per hours. How long does it take him to get to his destination at "
                + FractionB.toString() + " miles away?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getReciprocal(Result).toString();
    }

    private void createProblem_10(){
        Fraction FractionA = FractionMath.getRandomProperFraction();
        Fraction FractionB = FractionMath.getRandomProperFraction();

        Fraction Result = FractionMath.divideFraction(FractionB, FractionA);

        QUESTION = "A water tap is filling up a pool at " + FractionA.toString() + " of the pool per hour. How long does it take the tap " 
                + "to fill up " + FractionB.toString() + " of the pool?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getReciprocal(Result).toString();
    }

    private void createProblem_11(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.getRandomFraction();

        Fraction Result = FractionMath.divideFraction(FractionB, FractionA);

        QUESTION = "An animal can eat " + FractionA.toString() + " ounces of food in an hour. How long does it take the animal to finish " 
                + FractionB.toString() + " ounces of food?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getReciprocal(Result).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
