package pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckMul;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.MixedNumber;

/**
 * Created by Cong on 5/13/2017.
 */

public class MultiplyingFractionAndMixedNumber extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public MultiplyingFractionAndMixedNumber(){
        createProblem();
        swapAnswer();
    }

    public MultiplyingFractionAndMixedNumber(String Question,
                                String RightAnswer,
                                String AnswerA,
                                String AnswerB,
                                String AnswerC,
                                String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblemType = 5;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblemType){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            default:
                createProblem_01();
                break;
        }

        return new MultiplyingFractionAndMixedNumber(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  FRACTION x FRACTION = MIXED NUMBER
    private void createProblem_01(){
        Fraction FractionA = FractionMath.getRandomImproperFraction();
        Fraction FractionB = FractionMath.getRandomImproperFraction();

        Fraction Result = FractionMath.multiplyFraction(FractionA, FractionB);

        QUESTION = FractionA.toString() + " x " + FractionB.toString() + " = ?";
        if (Result.getValue() > 1.0){
            RIGHT_ANSWER = FractionMath.convertToMixedNumber(Result).toString();
        }
        else {
            RIGHT_ANSWER = Result.toString();
        }

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getRandomMixedNumber().toString();
        ANSWER_C = FractionMath.getRandomMixedNumber().toString();
        ANSWER_D = (new Fraction((FractionA.getNumerator() + FractionB.getNumerator()), (FractionA.getDenominator() + FractionB.getDenominator()))).toString();
    }

    //  FRACTION x MIXED NUMBER = FRACTION
    private void createProblem_02(){
        Fraction NumberA = FractionMath.getRandomFraction();
        MixedNumber NumberB = FractionMath.getRandomMixedNumber();

        Fraction Result = FractionMath.multiplyFraction(NumberA, FractionMath.convertToFraction(NumberB));

        QUESTION = NumberA.toString() + " x " + NumberB.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getRandomMixedNumber().toString();
        ANSWER_C = FractionMath.getRandomMixedNumber().toString();
        ANSWER_D = FractionMath.convertToFraction(NumberB).toString();
    }

    //  FRACTION x MIXED NUMBER = MIXED NUMBER
    private void createProblem_03(){
        Fraction NumberA = FractionMath.getRandomFraction();
        MixedNumber NumberB = FractionMath.getRandomMixedNumber();

        MixedNumber Result = FractionMath.convertToMixedNumber(FractionMath.multiplyFraction(NumberA, FractionMath.convertToFraction(NumberB)));

        QUESTION = NumberA.toString() + " x " + NumberB.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getRandomMixedNumber().toString();
        ANSWER_C = FractionMath.getRandomMixedNumber().toString();
        ANSWER_D = FractionMath.convertToFraction(NumberB).toString();
    }

    //  MIXED NUMBER x MIXED NUMBER = FRACTION
    private void createProblem_04(){
        MixedNumber NumberA = FractionMath.getRandomMixedNumber();
        MixedNumber NumberB = FractionMath.getRandomMixedNumber();

        Fraction Result = FractionMath.multiplyFraction(FractionMath.convertToFraction(NumberA), FractionMath.convertToFraction(NumberB));

        QUESTION = NumberA.toString() + " x " + NumberB.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getRandomMixedNumber().toString();
        ANSWER_C = FractionMath.getRandomMixedNumber().toString();
        ANSWER_D = FractionMath.convertToFraction(NumberB).toString();
    }

    //  MIXED NUMBER x MIXED NUMBER = MIXED NUMBER
    private void createProblem_05(){
        MixedNumber NumberA = FractionMath.getRandomMixedNumber();
        MixedNumber NumberB = FractionMath.getRandomMixedNumber();

        MixedNumber Result = FractionMath.convertToMixedNumber(FractionMath.multiplyFraction(FractionMath.convertToFraction(NumberA), FractionMath.convertToFraction(NumberB)));

        QUESTION = NumberA.toString() + " x " + NumberB.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getRandomMixedNumber().toString();
        ANSWER_C = FractionMath.getRandomMixedNumber().toString();
        ANSWER_D = FractionMath.convertToFraction(NumberB).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
