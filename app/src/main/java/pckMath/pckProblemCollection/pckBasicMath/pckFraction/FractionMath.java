package pckMath.pckProblemCollection.pckBasicMath.pckFraction;

import pckMath.MathUtility;

/**
 * Created by Cong on 5/12/2017.
 */

public class FractionMath {

    public static Fraction getRandomUnitFraction(){
        return new Fraction(1, MathUtility.getRandomPositiveNumber_1Digit());
    }

    public static Fraction getRandomProperFraction(){
        int Numerator = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Denominator = MathUtility.getRandomPositiveNumber_1Digit();
        Denominator = Denominator + Numerator;
        return getSimplifiedFraction(new Fraction(Numerator, Denominator));
    }

    public static Fraction getRandomImproperFraction(){
        int Numerator = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Denominator = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        Numerator = Numerator + Denominator;
        return getSimplifiedFraction(new Fraction(Numerator, Denominator));
    }

    public static Fraction getRandomFraction(){
        Fraction RandomFraction = getRandomProperFraction();
        if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0){
            RandomFraction = getRandomImproperFraction();
        }
        return RandomFraction;
    }

    public static MixedNumber getRandomMixedNumber(){
        return new MixedNumber(MathUtility.getRandomPositiveNumber_1Digit(), getRandomProperFraction());
    }

    public static MixedNumber convertToMixedNumber(Fraction FRACTION){
        int WholeNumber = FRACTION.getNumerator() / FRACTION.getDenominator();
        int Numerator = FRACTION.getNumerator() % FRACTION.getDenominator();
        int Denominator = FRACTION.getDenominator();

        return new MixedNumber(WholeNumber, getSimplifiedFraction(new Fraction(Numerator, Denominator)));
    }

    public static Fraction convertToFraction(MixedNumber MIXED_NUMBER){
        int Numerator = MIXED_NUMBER.getFraction().getDenominator() * MIXED_NUMBER.getWholeNumber() + MIXED_NUMBER.getFraction().getNumerator();
        int Denominator = MIXED_NUMBER.getFraction().getDenominator();
        return getSimplifiedFraction(new Fraction(Numerator, Denominator));
    }

    public static Fraction getReciprocal(Fraction FRACTION){
        return new Fraction(FRACTION.getDenominator(), FRACTION.getNumerator());
    }

    public static double getDecimalValue(Fraction FRACTION){
        return ((double) FRACTION.getNumerator()) / ((double) FRACTION.getDenominator());
    }

    public static Fraction findEquivalentFraction(Fraction FRACTION){
        int number = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        return new Fraction(FRACTION.getNumerator() * number, FRACTION.getDenominator() * number);
    }

    public static Fraction findEquivalentFraction(Fraction FRACTION, int Multiple){
        return new Fraction(FRACTION.getNumerator() * Multiple, FRACTION.getDenominator() * Multiple);
    }

    public static Fraction findInequivalentFraction(Fraction FRACTION){
        int number = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        return new Fraction(FRACTION.getNumerator() * number, FRACTION.getDenominator() * (number + 1));
    }

    public static Fraction getSimplifiedFraction(Fraction FRACTION){
        int GCF = MathUtility.findGCF(FRACTION.getNumerator(), FRACTION.getDenominator());
        if (FRACTION.getDenominator() < 0){
            GCF = GCF * -1;
        }

        return new Fraction(FRACTION.getNumerator() / GCF, FRACTION.getDenominator() / GCF);
    }

    public static Fraction addFractionWithoutSimplify(Fraction FRACTION_1, Fraction FRACTION_2){
        if (FRACTION_1.getDenominator() == 0 || FRACTION_2.getDenominator() == 0){
            throw new ArithmeticException();
        }

        int LCD = MathUtility.findLCM(FRACTION_1.getDenominator(), FRACTION_2.getDenominator());
        int MultipleOne = LCD / FRACTION_1.getDenominator();
        int MultipleTwo = LCD / FRACTION_2.getDenominator();
        return new Fraction((FRACTION_1.getNumerator() * MultipleOne + FRACTION_2.getNumerator() * MultipleTwo), LCD);
    }

    public static Fraction addFraction(Fraction FRACTION_1, Fraction FRACTION_2){
        return getSimplifiedFraction(addFractionWithoutSimplify(FRACTION_1, FRACTION_2));
    }

    public static Fraction subtractFractionWithoutSimplify(Fraction FRACTION_1, Fraction FRACTION_2){
        int LCD = MathUtility.findLCM(FRACTION_1.getDenominator(), FRACTION_2.getDenominator());
        int MultipleOne = LCD / FRACTION_1.getDenominator();
        int MultipleTwo = LCD / FRACTION_2.getDenominator();
        return new Fraction((FRACTION_1.getNumerator() * MultipleOne - FRACTION_2.getNumerator() * MultipleTwo), LCD);
    }

    public static Fraction subtractFraction(Fraction FRACTION_1, Fraction FRACTION_2){
        return getSimplifiedFraction(subtractFractionWithoutSimplify(FRACTION_1, FRACTION_2));
    }

    public static Fraction multiplyFractionWithoutSimplify(Fraction FRACTION_1, Fraction FRACTION_2){
        return new Fraction(FRACTION_1.getNumerator() * FRACTION_2.getNumerator(), FRACTION_1.getDenominator() * FRACTION_2.getDenominator());
    }

    public static Fraction multiplyFraction(Fraction FRACTION_1, Fraction FRACTION_2){
        return getSimplifiedFraction(multiplyFractionWithoutSimplify(FRACTION_1, FRACTION_2));
    }

    public static Fraction divideFractionWithoutSimplify(Fraction FRACTION_1, Fraction FRACTION_2){
        Fraction Temp = getReciprocal(FRACTION_2);
        return new Fraction(FRACTION_1.getNumerator() * Temp.getNumerator(), FRACTION_1.getDenominator() * Temp.getDenominator());
    }

    public static Fraction divideFraction(Fraction FRACTION_1, Fraction FRACTION_2){
        return getSimplifiedFraction(divideFractionWithoutSimplify(FRACTION_1, FRACTION_2));
    }

    public static Fraction pow(Fraction FRACTION, int EXPONENT){
        Fraction POW = new Fraction(1, 1);
        int exp = Math.abs(EXPONENT);
        if (exp > 0) {
            for (int i = 1; i <= exp; i++) {
                POW = multiplyFraction(POW, FRACTION);
            }
        }

        if (EXPONENT < 0){
            POW = getReciprocal(POW);
        }

        return POW;
    }

}
