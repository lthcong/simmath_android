package pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckSub;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 5/13/2017.
 */

public class SubtractingFractionWithDifferentDenominator extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SubtractingFractionWithDifferentDenominator(){
        createProblem();
        swapAnswer();
    }

    public SubtractingFractionWithDifferentDenominator(String Question,
                                                   String RightAnswer,
                                                   String AnswerA,
                                                   String AnswerB,
                                                   String AnswerC,
                                                   String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblemType = 3;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblemType){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SubtractingFractionWithDifferentDenominator(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  FRACTION - FRACTION = FRACTION
    private void createProblem_01(){
        int NumeratorA = MathUtility.getRandomPositiveNumber_1Digit();
        int DenominatorA = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int NumeratorB = MathUtility.getRandomPositiveNumber_1Digit();
        int DenominatorB = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        while (true){
            if (DenominatorA != DenominatorB){
                break;
            }
            else {
                DenominatorA = MathUtility.getRandomPositiveNumber_1Digit() + 1;
                DenominatorB = MathUtility.getRandomPositiveNumber_1Digit() + 1;
            }
        }

        Fraction FractionA = new Fraction(NumeratorA, DenominatorA);
        Fraction FractionB = new Fraction(NumeratorB, DenominatorB);

        FractionA = FractionMath.addFraction(FractionA, FractionB);

        Fraction Result = FractionMath.subtractFraction(FractionA, FractionB);

        QUESTION = FractionA.toString() + " - " + FractionB.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getRandomImproperFraction().toString();
        ANSWER_C = FractionMath.getRandomImproperFraction().toString();
        ANSWER_D = (new Fraction(Math.abs(NumeratorA - NumeratorB), Math.abs(DenominatorA - DenominatorB))).toString();
    }

    //  FRACTION - INTEGER = FRACTION
    private void createProblem_02(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = new Fraction(MathUtility.getRandomPositiveNumber_1Digit());

        FractionA = FractionMath.addFraction(FractionA, FractionB);
        Fraction Result = FractionMath.subtractFraction(FractionA, FractionB);

        QUESTION = FractionA.toString() + " - " + FractionB.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getRandomImproperFraction().toString();
        ANSWER_C = FractionMath.getRandomImproperFraction().toString();
        ANSWER_D = (new Fraction((FractionA.getNumerator() + FractionB.getNumerator()), (FractionA.getDenominator() + FractionB.getDenominator()))).toString();
    }

    //  INTEGER - FRACTION = FRACTION
    private void createProblem_03(){
        Fraction FractionA = new Fraction(MathUtility.getRandomPositiveNumber_1Digit());
        Fraction FractionB = FractionMath.getRandomFraction();

        FractionA = FractionMath.addFraction(FractionA, FractionB);
        Fraction Result = FractionMath.subtractFraction(FractionA, FractionB);

        QUESTION = FractionA.toString() + " - " + FractionB.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getRandomImproperFraction().toString();
        ANSWER_C = FractionMath.getRandomImproperFraction().toString();
        ANSWER_D = (new Fraction((FractionA.getNumerator() + FractionB.getNumerator()), (FractionA.getDenominator() + FractionB.getDenominator()))).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }


}
