package pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckSub;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 5/12/2017.
 */

public class SubtractingFractionWithTheSameDenominator extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SubtractingFractionWithTheSameDenominator(){
        createProblem();
        swapAnswer();
    }

    public SubtractingFractionWithTheSameDenominator(String Question,
                                                 String RightAnswer,
                                                 String AnswerA,
                                                 String AnswerB,
                                                 String AnswerC,
                                                 String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblemType = 4;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblemType){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SubtractingFractionWithTheSameDenominator(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int NumeratorA = MathUtility.getRandomPositiveNumber_1Digit();
        int NumeratorB = MathUtility.getRandomPositiveNumber_1Digit();

        int Denominator = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        Fraction FractionA = new Fraction(NumeratorA, Denominator);
        Fraction FractionB = new Fraction(NumeratorB, Denominator);

        FractionA = FractionMath.addFraction(FractionA, FractionB);

        Fraction Result = FractionMath.subtractFraction(FractionA, FractionB);

        QUESTION = FractionA.toString() + " - " + FractionB.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_D = FractionMath.getRandomProperFraction().toString();
        ANSWER_C = FractionMath.getRandomImproperFraction().toString();
        ANSWER_D = (new Fraction((NumeratorA + NumeratorB), Denominator)).toString();
    }

    private void createProblem_02(){
        int NumeratorA = MathUtility.getRandomPositiveNumber_1Digit();
        int NumeratorB = MathUtility.getRandomPositiveNumber_1Digit();

        int Denominator = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        Fraction FractionA = new Fraction(NumeratorA, Denominator);
        Fraction FractionB = new Fraction(NumeratorB, Denominator);

        FractionA = FractionMath.addFraction(FractionA, FractionB);

        Fraction Result = FractionMath.subtractFraction(FractionA, FractionB);

        QUESTION = "? - " + FractionB.toString() + " = " + Result.toString();
        RIGHT_ANSWER = FractionA.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = Result.toString();
        ANSWER_C = FractionMath.getRandomImproperFraction().toString();
        ANSWER_D = (new Fraction((NumeratorA + NumeratorB), Denominator)).toString();
    }

    private void createProblem_03(){
        int NumeratorA = MathUtility.getRandomPositiveNumber_1Digit();
        int NumeratorB = MathUtility.getRandomPositiveNumber_1Digit();

        int Denominator = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        Fraction FractionA = new Fraction(NumeratorA, Denominator);
        Fraction FractionB = new Fraction(NumeratorB, Denominator);

        FractionA = FractionMath.addFraction(FractionA, FractionB);

        Fraction Result = FractionMath.subtractFraction(FractionA, FractionB);

        QUESTION = FractionA.toString() + " - ? = " + Result.toString();
        RIGHT_ANSWER = FractionB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_D = FractionMath.getRandomProperFraction().toString();
        ANSWER_C = Result.toString();
        ANSWER_D = (new Fraction((NumeratorA + NumeratorB), Denominator)).toString();
    }

    private void createProblem_04(){
        int NumeratorA = MathUtility.getRandomPositiveNumber_1Digit();
        int NumeratorB = MathUtility.getRandomPositiveNumber_1Digit();

        int Denominator = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        Fraction FractionA = new Fraction(NumeratorA, Denominator);
        Fraction FractionB = new Fraction(NumeratorB, Denominator);

        FractionA = FractionMath.addFraction(FractionA, FractionB);

        Fraction Result = FractionMath.subtractFraction(FractionA, FractionB);

        QUESTION = "? - ? = ?";
        RIGHT_ANSWER = FractionA.toString() + ", " + FractionB.toString() + ", " + Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_D = FractionMath.getRandomProperFraction().toString();
        ANSWER_C = FractionMath.getRandomImproperFraction().toString();
        ANSWER_D = FractionA.toString() + ", " + FractionB.toString() + ", " + (new Fraction((NumeratorA + NumeratorB), Denominator)).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
