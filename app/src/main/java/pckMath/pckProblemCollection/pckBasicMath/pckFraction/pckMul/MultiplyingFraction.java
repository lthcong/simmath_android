package pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckMul;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 5/13/2017.
 */

public class MultiplyingFraction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public MultiplyingFraction(){
        createProblem();
        swapAnswer();
    }

    public MultiplyingFraction(String Question,
                                                   String RightAnswer,
                                                   String AnswerA,
                                                   String AnswerB,
                                                   String AnswerC,
                                                   String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblemType = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblemType){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }

        return new MultiplyingFraction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  FRACTION x FRACTION = FRACTION
    private void createProblem_01(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.getRandomFraction();

        Fraction Result = FractionMath.multiplyFraction(FractionA, FractionB);

        QUESTION = FractionA.toString() + " x " + FractionB.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getRandomFraction().toString();
        ANSWER_C = FractionMath.getRandomFraction().toString();
        ANSWER_D = (new Fraction((FractionA.getNumerator() + FractionB.getNumerator()), (FractionA.getDenominator() + FractionB.getDenominator()))).toString();
    }

    //  FRACTION x FRACTION = FRACTION
    private void createProblem_02(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.getRandomFraction();

        Fraction Result = FractionMath.multiplyFraction(FractionA, FractionB);

        QUESTION = "? x " + FractionB.toString() + " = " + Result.toString();
        RIGHT_ANSWER = FractionA.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = Result.toString();
        ANSWER_C = FractionMath.getRandomFraction().toString();
        ANSWER_D = (new Fraction((FractionA.getNumerator() + FractionB.getNumerator()), (FractionA.getDenominator() + FractionB.getDenominator()))).toString();
    }

    //  FRACTION x FRACTION = FRACTION
    private void createProblem_03(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.getRandomFraction();

        Fraction Result = FractionMath.multiplyFraction(FractionA, FractionB);

        QUESTION = FractionA.toString() + " x ? = " + Result.toString();
        RIGHT_ANSWER = FractionB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getRandomFraction().toString();
        ANSWER_C = Result.toString();
        ANSWER_D = (new Fraction((FractionA.getNumerator() + FractionB.getNumerator()), (FractionA.getDenominator() + FractionB.getDenominator()))).toString();
    }

    //  NUMBER x FRACTION = FRACTION
    private void createProblem_04(){
        Fraction FractionA = new Fraction(MathUtility.getRandomPositiveNumber_1Digit());
        Fraction FractionB = FractionMath.getRandomFraction();

        Fraction Result = FractionMath.multiplyFraction(FractionA, FractionB);

        QUESTION = FractionA.toString() + " x " + FractionB.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getRandomFraction().toString();
        ANSWER_C = FractionMath.getRandomFraction().toString();
        ANSWER_D = (new Fraction((FractionA.getNumerator() + FractionB.getNumerator()), (FractionA.getDenominator() + FractionB.getDenominator()))).toString();
    }

    //  NUMBER x FRACTION = FRACTION
    private void createProblem_05(){
        Fraction FractionA = new Fraction(MathUtility.getRandomPositiveNumber_1Digit());
        Fraction FractionB = FractionMath.getRandomFraction();

        Fraction Result = FractionMath.multiplyFraction(FractionA, FractionB);

        QUESTION = "? x " + FractionB.toString() + " = " + Result.toString();
        RIGHT_ANSWER = FractionA.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = Result.toString();
        ANSWER_C = FractionMath.getRandomFraction().toString();
        ANSWER_D = (new Fraction((FractionA.getNumerator() + FractionB.getNumerator()), (FractionA.getDenominator() + FractionB.getDenominator()))).toString();
    }

    //  NUMBER x FRACTION = FRACTION
    private void createProblem_06(){
        Fraction FractionA = new Fraction(MathUtility.getRandomPositiveNumber_1Digit());
        Fraction FractionB = FractionMath.getRandomFraction();

        Fraction Result = FractionMath.multiplyFraction(FractionA, FractionB);

        QUESTION = FractionA.toString() + " x ? = " + Result.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getRandomFraction().toString();
        ANSWER_C = Result.toString();
        ANSWER_D = (new Fraction((FractionA.getNumerator() + FractionB.getNumerator()), (FractionA.getDenominator() + FractionB.getDenominator()))).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }


}
