package pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckMul;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.MixedNumber;

/**
 * Created by Cong on 5/14/2017.
 */

public class WordProblemWithMultiplyingFraction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public WordProblemWithMultiplyingFraction(){
        createProblem();
        swapAnswer();
    }

    public WordProblemWithMultiplyingFraction(String Question,
                                          String RightAnswer,
                                          String AnswerA,
                                          String AnswerB,
                                          String AnswerC,
                                          String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblemType = 11;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblemType){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            default:
                createProblem_01();
                break;
        }

        return new WordProblemWithMultiplyingFraction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        Fraction FRACTION = FractionMath.getRandomProperFraction();
        int Number = FRACTION.getDenominator() * MathUtility.getRandomPositiveNumber_1Digit();

        int Result = (int) FractionMath.multiplyFraction(new Fraction(Number), FRACTION).getValue();

        QUESTION = "There are " + String.valueOf(Number) + " students in school. " + FRACTION.toString() + " of them are boys. "
                + "How many boys are there in the school?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Number);
        ANSWER_C = FRACTION.toString();
        ANSWER_D = FractionMath.getRandomFraction().toString();
    }

    private void createProblem_02(){
        Fraction FRACTION = FractionMath.getRandomProperFraction();
        int Number = FRACTION.getDenominator() * MathUtility.getRandomPositiveNumber_1Digit();

        int Result = (int) FractionMath.multiplyFraction(new Fraction(Number), FRACTION).getValue();
        Result = Number - Result;

        QUESTION = "There are " + String.valueOf(Number) + " students in school. " + FRACTION.toString() + " of them are boys. "
                + "How many girls are there in the school?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Number);
        ANSWER_C = FRACTION.toString();
        ANSWER_D = FractionMath.getRandomFraction().toString();
    }

    private void createProblem_03(){
        Fraction FRACTION = FractionMath.getRandomProperFraction();
        int Number = FRACTION.getDenominator() * MathUtility.getRandomPositiveNumber_1Digit();

        int Result = (int) FractionMath.multiplyFraction(new Fraction(Number), FRACTION).getValue();

        QUESTION = "Tom's father is raising duck and chicken. " + FRACTION.toString() + " of them are chicken. "
                + "How many chicken is Tom's father raising?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Number);
        ANSWER_C = FRACTION.toString();
        ANSWER_D = FractionMath.getRandomFraction().toString();
    }

    private void createProblem_04(){
        Fraction FRACTION = FractionMath.getRandomProperFraction();
        int Number = FRACTION.getDenominator() * MathUtility.getRandomPositiveNumber_1Digit();

        int Result = (int) FractionMath.multiplyFraction(FRACTION, new Fraction(Number)).getValue();

        QUESTION = "Terry bought some fruit. He got the total of " + String.valueOf(Number) + ", "
                + FRACTION.toString() + " of them are apples. How many apples did he buy?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FRACTION.toString();
        ANSWER_C = String.valueOf(Result);
        ANSWER_D = FractionMath.getRandomImproperFraction().toString();
    }

    private void createProblem_05(){
        Fraction FRACTION = FractionMath.getRandomProperFraction();
        MixedNumber Number = FractionMath.getRandomMixedNumber();

        Fraction Result = FractionMath.multiplyFraction(FRACTION, FractionMath.convertToFraction(Number));

        QUESTION = "Tom is running for morning exercise. His purpose is to run " + Number.toString() + " mile per hour. "
                + "How far can he run in " + FRACTION.toString() + " hours.";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FRACTION.toString();
        ANSWER_C = String.valueOf(Number);
        ANSWER_D = FractionMath.getReciprocal(Result).toString();

    }

    private void createProblem_06(){
        Fraction FractionA = FractionMath.getRandomProperFraction();
        Fraction FractionB = FractionMath.getRandomProperFraction();

        Fraction Sum = FractionMath.addFraction(FractionA, FractionB);

        int Number = Sum.getDenominator() * MathUtility.getRandomPositiveNumber_1Digit();
        int Result = (int) FractionMath.multiplyFraction(FractionA, new Fraction(Number)).getValue();

        QUESTION = "Tom bought the total of " + String.valueOf(Number) + " fruit. "
                + FractionA.toString() + " of them are apples, " + FractionB.toString() + " of them are banana. The rest is strawberry. "
                + "How many apples did Tom buy?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = (new Fraction(FractionA.getNumerator(), FractionB.getDenominator())).toString();
    }

    private void createProblem_07(){
        Fraction FractionA = FractionMath.getRandomProperFraction();
        Fraction FractionB = FractionMath.getRandomProperFraction();

        Fraction Sum = FractionMath.addFraction(FractionA, FractionB);

        int Number = Sum.getDenominator() * MathUtility.getRandomPositiveNumber_1Digit();
        int Result = (int) FractionMath.multiplyFraction(FractionB, new Fraction(Number)).getValue();

        QUESTION = "Tom bought the total of " + String.valueOf(Number) + " fruit. "
                + FractionA.toString() + " of them are apple, " + FractionB.toString() + " of them are banana. The rest is strawberry. "
                + "How many bananas did Tom buy?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = (new Fraction(FractionA.getNumerator(), FractionB.getDenominator())).toString();
    }

    private void createProblem_08(){
        Fraction FractionA = FractionMath.getRandomProperFraction();
        Fraction FractionB = FractionMath.getRandomProperFraction();

        Fraction Sum = FractionMath.addFraction(FractionA, FractionB);

        int Number = Sum.getDenominator() * MathUtility.getRandomPositiveNumber_1Digit();
        int Result = Number - (int) FractionMath.multiplyFraction(FractionA, new Fraction(Number)).getValue() + (int) FractionMath.multiplyFraction(FractionB, new Fraction(Number)).getValue();

        QUESTION = "Tom bought the total of " + String.valueOf(Number) + " fruit. "
                + FractionA.toString() + " of them are apples, " + FractionB.toString() + " of them are banana. The rest is strawberry. "
                + "How many strawberries did Tom buy?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = (new Fraction(FractionA.getNumerator(), FractionB.getDenominator())).toString();
    }

    private void createProblem_09(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.getRandomFraction();

        Fraction Result = FractionMath.multiplyFraction(FractionA, FractionB);

        QUESTION = "A recipe requires " + FractionA.toString() + " cups of sugar. Each cup of sugar requires "
                + FractionB.toString() + " ounce of water. How much water does the recipe need?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getReciprocal(Result).toString();
    }

    private void createProblem_10(){
        Fraction FRACTION = FractionMath.getRandomProperFraction();
        int Number = FRACTION.getDenominator() * MathUtility.getRandomPositiveNumber_1Digit();
        int Result = (int) FractionMath.multiplyFraction(FRACTION, new Fraction(Number)).getValue();

        QUESTION = "A rope has the length of " + String.valueOf(Number) + " feet. Tom decide to cut off the unused part which is "
                + FRACTION.toString() + " of the rope. How long did Tom cut off?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Number);
        ANSWER_C = FRACTION.toString();
        ANSWER_D = FractionMath.getReciprocal(FRACTION).toString();
    }

    private void createProblem_11(){
        Fraction FRACTION = FractionMath.getRandomProperFraction();
        int Number = FRACTION.getDenominator() * MathUtility.getRandomPositiveNumber_1Digit();
        int Result = Number - (int) FractionMath.multiplyFraction(FRACTION, new Fraction(Number)).getValue();

        QUESTION = "A rope has the length of " + String.valueOf(Number) + " feet. Tom decide to cut off the unused part which is "
                + FRACTION.toString() + " of the rope. How long is the rope after removing the unused part?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Number);
        ANSWER_C = FRACTION.toString();
        ANSWER_D = FractionMath.getReciprocal(FRACTION).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
