package pckMath.pckProblemCollection.pckBasicMath.pckFraction;

/**
 * Created by Cong on 5/12/2017.
 */

public class MixedNumber {

    private int WHOLE_NUMBER;
    private Fraction FRACTION;

    public MixedNumber(){
        WHOLE_NUMBER = 0;
        FRACTION = new Fraction();
    }

    public MixedNumber(int WholeNumber){
        WHOLE_NUMBER = WholeNumber;
        FRACTION = new Fraction(0, 1);
    }

    public MixedNumber(int WholeNumber, Fraction FRACTION){
        WHOLE_NUMBER = WholeNumber;
        this.FRACTION = FRACTION;
    }

    public void setWholeNumber(int WholeNumber){
        WHOLE_NUMBER = WholeNumber;
    }

    public int getWholeNumber(){
        return WHOLE_NUMBER;
    }

    public void setFraction(Fraction FRACTION){
        this.FRACTION = FRACTION;
    }

    public Fraction getFraction(){
        return FRACTION;
    }

    public double getValue(){
        return (WHOLE_NUMBER + FRACTION.getValue());
    }

    public boolean isEquivalent(MixedNumber NUMBER){
        boolean isTheSame = false;

        if (this.getValue() == NUMBER.getValue()){
            isTheSame = true;
        }

        return isTheSame;
    }

    public boolean isInteger(){
        boolean is_integer;
        if (FRACTION.isInteger()){
            is_integer = true;
        }
        else {
            is_integer = false;
        }

        return is_integer;
    }

    public String toString(){
        String MixedNumberString = "";

        if (FRACTION.getNumerator() % FRACTION.getDenominator() == 0){
            MixedNumberString = String.valueOf(WHOLE_NUMBER + (FRACTION.getNumerator() / FRACTION.getDenominator()));
        }
        else {
            if (WHOLE_NUMBER == 0){
                MixedNumberString = FRACTION.toString();
            }
            else {
                MixedNumberString = String.valueOf(WHOLE_NUMBER) + FRACTION.toString();
            }
        }

        return MixedNumberString;
    }

    public String toFullString(){
        return String.valueOf(WHOLE_NUMBER) + FRACTION.toFullString();
    }

    public String toSimpleString(){
        return String.valueOf(WHOLE_NUMBER) + " " + FRACTION.toSimpleString();
    }

    public boolean isNull(){
        boolean NullMixedNumber = false;
        if (FRACTION.isNull()){
            NullMixedNumber = true;
        }

        return NullMixedNumber;
    }

}
