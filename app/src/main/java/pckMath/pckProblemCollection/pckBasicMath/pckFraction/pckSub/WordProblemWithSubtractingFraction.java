package pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckSub;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.MixedNumber;

/**
 * Created by Cong on 5/13/2017.
 */

public class WordProblemWithSubtractingFraction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public WordProblemWithSubtractingFraction(){
        createProblem();
        swapAnswer();
    }

    public WordProblemWithSubtractingFraction(String Question,
                                          String RightAnswer,
                                          String AnswerA,
                                          String AnswerB,
                                          String AnswerC,
                                          String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblemType = 10;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblemType){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            default:
                createProblem_01();
                break;
        }

        return new WordProblemWithSubtractingFraction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        Fraction FractionA = FractionMath.getRandomProperFraction();
        Fraction FractionB = FractionMath.getRandomProperFraction();

        FractionA = FractionMath.addFraction(FractionA, FractionB);

        Fraction Result = FractionMath.subtractFraction(FractionA, FractionB);

        QUESTION = "Tom and his sister ate " + FractionA.toString() + " of a pizza. Tom ate " + FractionB.toString()
                + ". How much of the pizza did his sister eat?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getReciprocal(Result).toString();
    }

    private void createProblem_02(){
        Fraction FractionA = new Fraction(MathUtility.getRandomPositiveNumber_1Digit());
        Fraction FractionB = FractionMath.getRandomProperFraction();

        Fraction Result = FractionMath.subtractFraction(FractionA, FractionB);

        QUESTION = "A rope has the length of " + FractionA.toString() + " meters. Jame cut it into 2 pieces. The first one is " + FractionB.toString()
                + " meters long. How long is the second piece?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getReciprocal(Result).toString();
    }

    private void createProblem_03(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.getRandomFraction();

        FractionA = FractionMath.addFraction(FractionA, FractionB);

        Fraction Result = FractionMath.subtractFraction(FractionA, FractionB);

        QUESTION = "Tim and his brother ran " + FractionA.toString() + " miles for exercise. Tim ran " + FractionB.toString() + " miles. "
                + "How many miles did his brother run?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getReciprocal(Result).toString();
    }

    private void createProblem_04(){
        MixedNumber NumberA = FractionMath.getRandomMixedNumber();
        MixedNumber NumberB = FractionMath.getRandomMixedNumber();

        NumberA = FractionMath.convertToMixedNumber(FractionMath.addFraction(FractionMath.convertToFraction(NumberA), FractionMath.convertToFraction(NumberB)));

        MixedNumber Result = FractionMath.convertToMixedNumber(FractionMath.subtractFraction(FractionMath.convertToFraction(NumberA), FractionMath.convertToFraction(NumberB)));

        QUESTION = "John and his friend ate " + NumberA.toString() + " pan cakes. His friend ate " + NumberB.toString() + " ones. "
                + "How many pan cakes did John eat?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = NumberA.toString();
        ANSWER_C = NumberB.toString();
        ANSWER_D = FractionMath.getRandomFraction().toString();
    }

    private void createProblem_05(){
        MixedNumber NumberA = FractionMath.getRandomMixedNumber();
        MixedNumber NumberB = FractionMath.getRandomMixedNumber();

        NumberA = FractionMath.convertToMixedNumber(FractionMath.addFraction(FractionMath.convertToFraction(NumberA), FractionMath.convertToFraction(NumberB)));

        MixedNumber Result = FractionMath.convertToMixedNumber(FractionMath.subtractFraction(FractionMath.convertToFraction(NumberA), FractionMath.convertToFraction(NumberB)));

        QUESTION = "It takes " + NumberA.toString() + " minutes to go from home to the bus stop and to school. "
                + "From home to the bus stop is " + NumberB.toString() + " minutes. How long does it take from to go from the bus stop to school?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = NumberA.toString();
        ANSWER_C = NumberB.toString();
        ANSWER_D = FractionMath.getRandomFraction().toString();
    }

    private void createProblem_06(){
        MixedNumber NumberA = FractionMath.getRandomMixedNumber();
        MixedNumber NumberB = FractionMath.getRandomMixedNumber();

        NumberA = FractionMath.convertToMixedNumber(FractionMath.addFraction(FractionMath.convertToFraction(NumberA), FractionMath.convertToFraction(NumberB)));

        MixedNumber Result = FractionMath.convertToMixedNumber(FractionMath.subtractFraction(FractionMath.convertToFraction(NumberA), FractionMath.convertToFraction(NumberB)));

        QUESTION = "Tom drive " + NumberA.toString() + " miles from his house to the gas station and to his grandma house. "
                +  "From the gas station to his grandma house is " + NumberB.toString() + " miles. How far is it from his house to the gas station?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = NumberA.toString();
        ANSWER_C = NumberB.toString();
        ANSWER_D = FractionMath.getRandomFraction().toString();
    }

    private void createProblem_07(){
        Fraction FractionA = new Fraction(1);
        Fraction FractionB = FractionMath.getRandomProperFraction();

        Fraction Result = FractionMath.subtractFraction(FractionA, FractionB);

        QUESTION = "Jim ate " + FractionB.toString() + " of a chocolate bar. How much of the chocolate bar were left?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getReciprocal(Result).toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionA.toString();
    }

    private void createProblem_08(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.getRandomFraction();

        FractionA = FractionMath.addFraction(FractionA, FractionB);

        Fraction Result = FractionMath.subtractFraction(FractionA, FractionB);

        QUESTION = "Tom and Terry are running for morning exercise. Tom runs " + FractionA.toString() + " miles. Terry runs " + FractionB.toString() + " miles. "
                + "How much further does Tom run than Terry?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getReciprocal(Result).toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionA.toString();
    }

    private void createProblem_09(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.getRandomFraction();

        FractionA = FractionMath.addFraction(FractionA, FractionB);

        Fraction Result = FractionMath.subtractFraction(FractionA, FractionB);

        QUESTION = "It takes Jack " + FractionA.toString() + " minutes to go from home to the gas station. and "
                + FractionB.toString() + " minutes to go from home to the library. "
                + "How much longer does it take him to go from home to the gas station than going to the library?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getReciprocal(Result).toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionA.toString();
    }

    private void createProblem_10(){
        Fraction FractionA = FractionMath.getRandomProperFraction();
        Fraction FractionB = FractionMath.getRandomProperFraction();;

        FractionA = FractionMath.addFraction(FractionA, FractionB);

        Fraction Result = FractionMath.subtractFraction(FractionA, FractionB);

        QUESTION = "Tom ate " + FractionA.toString() + " of a pizza. He ate " + FractionB.toString() + " of the pizza more than his sister. "
                + "How much of the pizza did his sister eat?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getReciprocal(Result).toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionA.toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
