package pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckTypeOfFraction;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.MixedNumber;

/**
 * Created by Cong on 5/15/2017.
 */

public class FractionOperations extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FractionOperations(){
        createProblem();
        swapAnswer();
    }

    public FractionOperations(String Question,
                           String RightAnswer,
                           String AnswerA,
                           String AnswerB,
                           String AnswerC,
                           String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblemType = 7;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblemType){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FractionOperations(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  FRACTION => MIXED NUMBER
    private void createProblem_01(){
        Fraction FRACTION = FractionMath.getRandomImproperFraction();
        MixedNumber Result = FractionMath.convertToMixedNumber(FRACTION);

        QUESTION = "Convert " + FRACTION.toFullString() + " to mixed number:";
        RIGHT_ANSWER = Result.toFullString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FRACTION.toFullString();
        ANSWER_C = FractionMath.getReciprocal(FRACTION).toFullString();
        ANSWER_D = FractionMath.getRandomProperFraction().toFullString();
    }

    //  MIXED NUMBER => FACTION
    private void createProblem_02(){
        MixedNumber Number = FractionMath.getRandomMixedNumber();
        Fraction Result = FractionMath.convertToFraction(Number);

        QUESTION = "Convert " + Number.toString() + " to fraction:";
        RIGHT_ANSWER = Result.toFullString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = Number.toFullString();
        ANSWER_C = FractionMath.getReciprocal(Result).toFullString();
        ANSWER_D = FractionMath.getRandomProperFraction().toFullString();
    }

    //  RECIPROCAL
    private void createProblem_03(){
        Fraction FRACTION = FractionMath.getRandomFraction();
        Fraction Result = FractionMath.getReciprocal(FRACTION);

        QUESTION = "Find the reciprocal of " + FRACTION.toString();
        RIGHT_ANSWER = Result.toFullString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FRACTION.toString();
        ANSWER_C = FractionMath.findEquivalentFraction(FRACTION).toFullString();
        ANSWER_D = FractionMath.findInequivalentFraction(FRACTION).toFullString();;
    }

    //  EQUIVALENT
    private void createProblem_04(){
        Fraction FRACTION = FractionMath.getRandomFraction();
        Fraction Result = FractionMath.findEquivalentFraction(FRACTION);

        QUESTION = "Find the equivalent fraction of " + FRACTION.toString();
        RIGHT_ANSWER = Result.toFullString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FRACTION.toString();
        ANSWER_C = FractionMath.getReciprocal(FRACTION).toFullString();
        ANSWER_D = FractionMath.findInequivalentFraction(FRACTION).toFullString();;
    }

    //  INEQUIVALENT
    private void createProblem_05(){
        Fraction FRACTION = FractionMath.getRandomFraction();
        Fraction Result = FractionMath.findInequivalentFraction(FRACTION);

        QUESTION = "Find the inequivalent fraction of " + FRACTION.toString();
        RIGHT_ANSWER = Result.toFullString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.findEquivalentFraction(FRACTION, 2).toFullString();
        ANSWER_C = FractionMath.findEquivalentFraction(FRACTION, 4).toFullString();
        ANSWER_D = FractionMath.findEquivalentFraction(FRACTION, 3).toFullString();
    }

    //  LCD
    private void createProblem_06(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.getRandomFraction();

        Fraction Result = FractionMath.addFraction(FractionA, FractionB);

        QUESTION = "Find the least common denominator (LCD) of " + FractionA.toString() + " and " + FractionB.toString() + ".";
        RIGHT_ANSWER = String.valueOf(Result.getDenominator());

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result.getNumerator());
        ANSWER_C = String.valueOf(FractionA.getNumerator());
        ANSWER_D = String.valueOf(FractionB.getNumerator());
    }

    //  SIMPLIFY
    private void createProblem_07(){
        Fraction FractionA = FractionMath.getSimplifiedFraction(FractionMath.getRandomFraction());
        Fraction FractionB = FractionMath.findEquivalentFraction(FractionA, MathUtility.getRandomPositiveNumber_1Digit() + 1);

        Fraction Result = FractionA;

        QUESTION = "Simplify this fraction: " + FractionB.toFullString();
        RIGHT_ANSWER = Result.toFullString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.findEquivalentFraction(FractionB, MathUtility.getRandomPositiveNumber_1Digit() + 1).toFullString();
        ANSWER_C = FractionMath.getRandomFraction().toFullString();
        ANSWER_D = FractionMath.getRandomFraction().toFullString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
