package pckMath.pckProblemCollection.pckBasicMath.pckFraction.pckAdd;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.MixedNumber;

/**
 * Created by Cong on 5/12/2017.
 */

public class WordProblemWithAddingFraction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public WordProblemWithAddingFraction(){
        createProblem();
        swapAnswer();
    }

    public WordProblemWithAddingFraction(String Question,
                                                   String RightAnswer,
                                                   String AnswerA,
                                                   String AnswerB,
                                                   String AnswerC,
                                                   String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblemType = 11;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblemType){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            default:
                createProblem_01();
                break;
        }

        return new WordProblemWithAddingFraction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){

        Fraction FractionA = FractionMath.getRandomProperFraction();
        Fraction FractionB = FractionMath.getRandomProperFraction();

        Fraction Result = FractionMath.addFraction(FractionA, FractionB);

        QUESTION = "Tim ate " + FractionA.toString() + " of the pizza. John ate " + FractionB.toString() + " of the pizza. "
                + "How much of the pizza did they eat?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getRandomMixedNumber().toString();
    }

    private void createProblem_02(){
        Fraction FractionA = FractionMath.getRandomProperFraction();
        Fraction FractionB = FractionMath.getRandomProperFraction();

        Fraction Result = FractionMath.addFraction(FractionA, FractionB);

        QUESTION = "Tom took " + FractionA.toString() + " of the printing paper. Terry took " + FractionB.toString() + " of the printing paper. "
                + "How much of the printing paper did they take?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getRandomMixedNumber().toString();
    }

    private void createProblem_03(){
        Fraction FractionA = FractionMath.getRandomProperFraction();
        Fraction FractionB = FractionMath.getRandomProperFraction();

        Fraction Result = FractionMath.addFraction(FractionA, FractionB);

        QUESTION = "Jim cut " + FractionA.toString() + " of the rope. His brother cut " + FractionB.toString()
                + ". How much of the rope was cut?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getRandomProperFraction().toString();
    }

    private void createProblem_04(){
        Fraction FractionA = FractionMath.getRandomProperFraction();
        Fraction FractionB = FractionMath.getRandomProperFraction();

        Fraction Result = FractionMath.addFraction(FractionA, FractionB);

        QUESTION = "Tim took " + FractionA.toString() + " of the pencil from the box. His sister took " + FractionB.toString()
                + ". How much of the pencil did they take?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getRandomProperFraction().toString();
    }

    private void createProblem_05(){
        Fraction FractionA = FractionMath.getRandomProperFraction();
        Fraction FractionB = FractionMath.getRandomProperFraction();

        Fraction Result = FractionMath.addFraction(FractionA, FractionB);

        QUESTION = "John walk " + FractionA.toString() + " of a mile from home to the bus stop. And "
                + FractionB.toString() + " of a mile from the bus stop to his office. How far does he have to walk to get to his office?" ;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getRandomProperFraction().toString();
    }

    private void createProblem_06(){
        Fraction FractionA = FractionMath.getRandomProperFraction();
        Fraction FractionB = FractionMath.getRandomProperFraction();

        Fraction Result = FractionMath.addFraction(FractionA, FractionB);

        QUESTION = "Jame run " + FractionA.toString() + " of a mile from home to the park. And "
                + FractionB.toString() + " of a mile from the park to the store. "
                + "How much of a mile did Jame run to get to the store?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getRandomProperFraction().toString();
    }

    private void createProblem_07(){
        MixedNumber NumberA = FractionMath.getRandomMixedNumber();
        MixedNumber NumberB = FractionMath.getRandomMixedNumber();

        MixedNumber Result = FractionMath.convertToMixedNumber(FractionMath.addFraction(FractionMath.convertToFraction(NumberA), FractionMath.convertToFraction(NumberB)));

        QUESTION = "It takes Jim " + NumberA.toString() + " minutes to get to the bus stop. And " + NumberB.toString() + " minutes to get to the school. "
                + "How long does it take Jim to go from home to school?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = NumberA.toString();
        ANSWER_C = NumberA.toString();
        ANSWER_D = (new Fraction(NumberA.getFraction().getNumerator(), NumberB.getFraction().getDenominator())).toString();
    }

    private void createProblem_08(){
        MixedNumber NumberA = FractionMath.getRandomMixedNumber();
        MixedNumber NumberB = FractionMath.getRandomMixedNumber();

        MixedNumber Result = FractionMath.convertToMixedNumber(FractionMath.addFraction(FractionMath.convertToFraction(NumberA), FractionMath.convertToFraction(NumberB)));

        QUESTION = "Jack spent " + NumberA.toString() + " dollars at the bookstore. And " + NumberB.toString() + " at the fast food restaurant. "
                + "How much money did he spend in total?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = NumberA.toString();
        ANSWER_C = NumberA.toString();
        ANSWER_D = (new Fraction(NumberA.getFraction().getNumerator(), NumberB.getFraction().getDenominator())).toString();
    }

    private void createProblem_09(){
        MixedNumber NumberA = FractionMath.getRandomMixedNumber();
        MixedNumber NumberB = FractionMath.getRandomMixedNumber();

        MixedNumber Result = FractionMath.convertToMixedNumber(FractionMath.addFraction(FractionMath.convertToFraction(NumberA), FractionMath.convertToFraction(NumberB)));

        QUESTION = "Tom ate " + NumberA.toString() + " pan cakes. His brother ate " + NumberB.toString() + " ones. "
                + "How many pan cakes did they eat?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = NumberA.toString();
        ANSWER_C = NumberA.toString();
        ANSWER_D = (new Fraction(NumberA.getFraction().getNumerator(), NumberB.getFraction().getDenominator())).toString();
    }

    private void createProblem_10(){
        MixedNumber NumberA = FractionMath.getRandomMixedNumber();
        MixedNumber NumberB = FractionMath.getRandomMixedNumber();

        MixedNumber Result = FractionMath.convertToMixedNumber(FractionMath.addFraction(FractionMath.convertToFraction(NumberA), FractionMath.convertToFraction(NumberB)));

        QUESTION = "Tom drove " + NumberA.toString() + " miles to the gas station and " + NumberB.toString() + " miles to visit his grandma. "
                + "How many miles did he drive in total?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = NumberA.toString();
        ANSWER_C = NumberA.toString();
        ANSWER_D = (new Fraction(NumberA.getFraction().getNumerator(), NumberB.getFraction().getDenominator())).toString();
    }

    private void createProblem_11(){
        MixedNumber NumberA = FractionMath.getRandomMixedNumber();
        MixedNumber NumberB = FractionMath.getRandomMixedNumber();

        MixedNumber Result = FractionMath.convertToMixedNumber(FractionMath.addFraction(FractionMath.convertToFraction(NumberA), FractionMath.convertToFraction(NumberB)));

        QUESTION = "Jane used " + NumberA.toString() + " paper sheets to take note from her chemistry class. And " + NumberB.toString()
                + " sheets for her history class. How many sheets of paper did she use to take note?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = NumberA.toString();
        ANSWER_C = NumberA.toString();
        ANSWER_D = (new Fraction(NumberA.getFraction().getNumerator(), NumberB.getFraction().getDenominator())).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }
}
