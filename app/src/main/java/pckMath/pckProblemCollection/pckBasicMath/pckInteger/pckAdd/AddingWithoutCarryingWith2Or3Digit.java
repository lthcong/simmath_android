package pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckAdd;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 9/2/2017.
 */

public class AddingWithoutCarryingWith2Or3Digit extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public AddingWithoutCarryingWith2Or3Digit(){
        createProblem();
        swapAnswer();
    }

    public AddingWithoutCarryingWith2Or3Digit(String Question,
                                              String RightAnswer,
                                              String AnswerA,
                                              String AnswerB,
                                              String AnswerC,
                                              String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 8;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }

        return new AddingWithoutCarryingWith2Or3Digit(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int c = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;

        int d = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int e = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;

        if (b + d >= 10 && c + e >= 10){
            createProblem_01();
        }

        a = a * 100 + b * 10 + c;
        b = d * 10 + e;

        int result = a + b;

        QUESTION = String.valueOf(a) + " + " + String.valueOf(b) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_02(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int c = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;

        int d = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int e = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;

        if (b + d >= 10 && c + e >= 10){
            createProblem_02();
        }

        a = a * 100 + b * 10 + c;
        b = d * 10 + e;

        int result = a + b;

        QUESTION = "? + " + String.valueOf(b) + " = " + String.valueOf(result);
        RIGHT_ANSWER = String.valueOf(a);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a + 1);
        ANSWER_C = String.valueOf(a + 2);
        ANSWER_D = String.valueOf(a - 1);
    }

    private void createProblem_03(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int c = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;

        int d = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int e = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;

        if (b + d >= 10 && c + e >= 10){
            createProblem_03();
        }

        a = a * 100 + b * 10 + c;
        b = d * 10 + e;

        int result = a + b;

        QUESTION = String.valueOf(a) + " + ? = " + String.valueOf(result);
        RIGHT_ANSWER = String.valueOf(b);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(b + 1);
        ANSWER_C = String.valueOf(b + 2);
        ANSWER_D = String.valueOf(b - 1);
    }

    private void createProblem_04(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int c = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;

        int d = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int e = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;

        if (b + d >= 10 && c + e >= 10){
            createProblem_04();
        }

        a = a * 100 + b * 10 + c;
        b = d * 10 + e;

        int result = a + b;

        QUESTION = "? + ? = ?";
        RIGHT_ANSWER = String.valueOf(a) + ", " + String.valueOf(b) + ", " + String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a + 1) + ", " + String.valueOf(b) + ", " + String.valueOf(result);
        ANSWER_C = String.valueOf(a) + ", " + String.valueOf(b + 1) + ", " + String.valueOf(result);
        ANSWER_D = String.valueOf(a) + ", " + String.valueOf(b) + ", " + String.valueOf(result + 1);
    }

    private void createProblem_05(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int c = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;

        int d = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int e = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int f = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;

        if (a + d >= 10 && b + e >= 10 && c + f >= 10){
            createProblem_05();
        }

        a = a * 100 + b * 10 + c;
        b = d * 100 + e * 10 + f;

        int result = a + b;

        QUESTION = String.valueOf(a) + " + " + String.valueOf(b) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_06(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int c = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;

        int d = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int e = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int f = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;

        if (a + d >= 10 && b + e >= 10 && c + f >= 10){
            createProblem_06();
        }

        a = a * 100 + b * 10 + c;
        b = d * 100 + e * 10 + f;

        int result = a + b;

        QUESTION = "? + " + String.valueOf(b) + " = " + String.valueOf(result);
        RIGHT_ANSWER = String.valueOf(a);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a + 1);
        ANSWER_C = String.valueOf(a + 2);
        ANSWER_D = String.valueOf(a - 1);
    }

    private void createProblem_07(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int c = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;

        int d = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int e = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int f = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;

        if (a + d >= 10 && b + e >= 10 && c + f >= 10){
            createProblem_07();
        }

        a = a * 100 + b * 10 + c;
        b = d * 100 + e * 10 + f;

        int result = a + b;

        QUESTION = String.valueOf(a) + " + ? = " + String.valueOf(result);
        RIGHT_ANSWER = String.valueOf(b);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(b + 1);
        ANSWER_C = String.valueOf(b + 2);
        ANSWER_D = String.valueOf(b - 1);
    }

    private void createProblem_08(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int c = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;

        int d = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int e = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;
        int f = MathUtility.getRandomPositiveNumber_1Digit() % 5 + 1;

        if (a + d >= 10 && b + e >= 10 && c + f >= 10){
            createProblem_08();
        }

        a = a * 100 + b * 10 + c;
        b = d * 100 + e * 10 + f;

        int result = a + b;

        QUESTION = "? + ? = ?";
        RIGHT_ANSWER = String.valueOf(a) + ", " + String.valueOf(b) + ", " + String.valueOf(result) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a + 1) + ", " + String.valueOf(b) + ", " + String.valueOf(result) + ".";
        ANSWER_C = String.valueOf(a) + ", " + String.valueOf(b + 1) + ", " + String.valueOf(result) + ".";
        ANSWER_D = String.valueOf(a - 1) + ", " + String.valueOf(b) + ", " + String.valueOf(result + 1) + ".";
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
