package pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckMul;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 4/28/2017.
 */

public class WordProblemWithMultiplyingInteger extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public WordProblemWithMultiplyingInteger(){
        createProblem();
        swapAnswer();
    }

    public WordProblemWithMultiplyingInteger(String Question,
                                    String RightAnswer,
                                    String AnswerA,
                                    String AnswerB,
                                    String AnswerC,
                                    String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 12;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            default:
                createProblem_01();
                break;
        }

        return new WordProblemWithMultiplyingInteger(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        int result = a * b;

        QUESTION = "Tom wants to plant some trees in the garden. "
                + String.valueOf(a) + " trees in a row, and " + String.valueOf(b) + " rows. "
                + "How many trees can he plant?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_02(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        int result = a * b;

        QUESTION = "Jim cuts the pizza into " + String.valueOf(a) + " pieces. There are " + String.valueOf(b) + " pizzas. "
                + "How many pieces are there in total?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_03(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        int result = a * b;

        QUESTION = "Each pound of fruit costs $" + String.valueOf(a) + ". How much does" + String.valueOf(b) + " pounds cost?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_04(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() * 3;
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        int result = a * b;

        QUESTION = "There are " + String.valueOf(a) + " students in a class. "
                + "School has " + String.valueOf(b) + "classes. How many students does the school have?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_05(){
        int a = MathUtility.getRandomPositiveNumber_3Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();

        int result = a * b;

        QUESTION = "There are " + String.valueOf(a) + " on the shelf. The library has " + String.valueOf(b) + " shelves. "
                + "How many books does the library have?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_06(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() * 3;
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        int result = a * b;

        QUESTION = "There are " + String.valueOf(a) + " cakes in the box. " + String.valueOf(b) + " boxes in total. "
                + "How many cakes are there in these boxes?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_07(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();

        int result = a * b;

        QUESTION = "There are " + String.valueOf(a) + " rows of chair in the hall. " + String.valueOf(b) + " chairs in a row."
                + "How many chairs are there in the hall?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_08(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() * 2;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int result = a * b;

        QUESTION = "There are " + String.valueOf(b) + " tables at the birthday party. Each table was set up for " + String.valueOf(a) + " people. "
                + "How many people are invited to the party?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_09(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();

        int result = a * b;

        QUESTION = "The farmer has " + String.valueOf(a) + " apple trees. Each of them gives " + String.valueOf(b) + " apples. "
                + "How many apples can the farmer get from these trees?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_10(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int result = a * b;

        QUESTION = "The class has " + String.valueOf(a) + " tables. Each table has enough room for " + String.valueOf(b) + " students. "
                + "How many students are there in the class?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_11(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int result = a * b;

        QUESTION = "Helen has to do " + String.valueOf(a) + " Math homework problems per day. How many problems does she have to do "
                + "in " + String.valueOf(b) + " days?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_12(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = 6;

        int result = a * b;

        QUESTION = "Helen has to do " + String.valueOf(a) + " Math homework problems per day. "
                + "How many problems does she have to do in a week (Monday to Friday)?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
