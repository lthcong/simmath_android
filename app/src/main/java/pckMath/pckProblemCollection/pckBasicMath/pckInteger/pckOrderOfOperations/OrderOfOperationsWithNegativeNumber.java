package pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckOrderOfOperations;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 5/18/2017.
 */

public class OrderOfOperationsWithNegativeNumber extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;
    private String ADD = "<b> + </b>", SUB = "<b> - </b>", MUL = "<b> x </b>", DIV = "<b>" + InfoCollector.getDivisionSign() + "</b>";
    
    public OrderOfOperationsWithNegativeNumber(){
        createProblem();
        swapAnswer();
    }

    public OrderOfOperationsWithNegativeNumber(String Question,
                                                String RightAnswer,
                                                String AnswerA,
                                                String AnswerB,
                                                String AnswerC,
                                                String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 50;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            case 14:
                createProblem_15();
                break;
            case 15:
                createProblem_16();
                break;
            case 16:
                createProblem_17();
                break;
            case 17:
                createProblem_18();
                break;
            case 18:
                createProblem_19();
                break;
            case 19:
                createProblem_20();
                break;
            case 20:
                createProblem_21();
                break;
            case 21:
                createProblem_22();
                break;
            case 22:
                createProblem_23();
                break;
            case 23:
                createProblem_24();
                break;
            case 24:
                createProblem_25();
                break;
            case 25:
                createProblem_26();
                break;
            case 26:
                createProblem_27();
                break;
            case 27:
                createProblem_28();
                break;
            case 28:
                createProblem_29();
                break;
            case 29:
                createProblem_30();
                break;
            case 30:
                createProblem_31();
                break;
            case 31:
                createProblem_32();
                break;
            case 32:
                createProblem_33();
                break;
            case 33:
                createProblem_34();
                break;
            case 34:
                createProblem_35();
                break;
            case 35:
                createProblem_36();
                break;
            case 36:
                createProblem_37();
                break;
            case 37:
                createProblem_38();
                break;
            case 38:
                createProblem_39();
                break;
            case 39:
                createProblem_40();
                break;
            case 40:
                createProblem_41();
                break;
            case 41:
                createProblem_42();
                break;
            case 42:
                createProblem_43();
                break;
            case 43:
                createProblem_44();
                break;
            case 44:
                createProblem_45();
                break;
            case 45:
                createProblem_46();
                break;
            case 46:
                createProblem_47();
                break;
            case 47:
                createProblem_48();
                break;
            case 48:
                createProblem_49();
                break;
            case 49:
                createProblem_50();
                break;
            default:
                createProblem_01();
                break;
        }

        return new OrderOfOperationsWithNegativeNumber(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  a + b x c
    private void createProblem_01(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int result = a + b * c;

        QUESTION = String.valueOf(a) + " + " + String.valueOf(b) + " x " + String.valueOf(c) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a - b x c
    private void createProblem_02(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        a = a + b * c;

        int result = a - b * c;

        QUESTION = String.valueOf(a) + " - " + String.valueOf(b) + " x " + String.valueOf(c) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a + b / c
    private void createProblem_03(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        b = b * c;

        int result = a + b / c;

        QUESTION = String.valueOf(a) + " + " + String.valueOf(b) + InfoCollector.getDivisionSign() + String.valueOf(c) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a - b / c
    private void createProblem_04(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        b = b * c;

        int result = a - b / c;

        QUESTION = String.valueOf(a) + " - " + String.valueOf(b) + InfoCollector.getDivisionSign() + String.valueOf(c) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a + b x c - d
    private void createProblem_05(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        int result = a + b * c - d;

        QUESTION = String.valueOf(a) + " + " + String.valueOf(b) + " x " + String.valueOf(c) + " - " + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a + b x c + d
    private void createProblem_06(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        int result = a + b * c + d;

        QUESTION = String.valueOf(a) + " + " + String.valueOf(b) + " x " + String.valueOf(c) + " + " + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a + b x c / d
    private void createProblem_07(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        if (b * c % d != 0){
            b = d * b;
        }

        int result = a + b * c / d;

        QUESTION = String.valueOf(a) + " + " + String.valueOf(b) + " x " + String.valueOf(c) + InfoCollector.getDivisionSign() + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a - b x c + d
    private void createProblem_08(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        a = a + b * c;

        int result = a - b * c + d;

        QUESTION = String.valueOf(a) + " - " + String.valueOf(b) + " x " + String.valueOf(c) + " + " + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a - b x c - d
    private void createProblem_09(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        a = a + b * c;

        int result = a - b * c - d;

        QUESTION = String.valueOf(a) + " - " + String.valueOf(b) + " x " + String.valueOf(c) + " - " + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a x b + c x d
    private void createProblem_10(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        int result = a * b + c * d;

        QUESTION = String.valueOf(a) + " x " + String.valueOf(b) + " + " + String.valueOf(c) + " x " + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a x b - c / d
    private void createProblem_11(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        
        c = c * d;

        int result = a * b - c / d;

        QUESTION = String.valueOf(a) + " x " + String.valueOf(b) + " - " + String.valueOf(c) + InfoCollector.getDivisionSign() + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a x b + c / d
    private void createProblem_12(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        c = c * d;

        int result = a * b + c / d;

        QUESTION = String.valueOf(a) + " x " + String.valueOf(b) + " + " + String.valueOf(c) + InfoCollector.getDivisionSign() + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a / b + c / d
    private void createProblem_13(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        a = a * b;
        c = c * d;

        int result = a / b + c / d;

        QUESTION = String.valueOf(a) + InfoCollector.getDivisionSign() + String.valueOf(b) + " + " + String.valueOf(c) + InfoCollector.getDivisionSign() + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a / b - c / d
    private void createProblem_14(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        a = a * b;
        c = c * d;

        int result = a / b - c / d;

        QUESTION = String.valueOf(a) + InfoCollector.getDivisionSign() + String.valueOf(b) + " - " + String.valueOf(c) + InfoCollector.getDivisionSign() + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a + b / c - d
    private void createProblem_15(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        b = b * c;

        int result = a + b / c - d;

        QUESTION = String.valueOf(a) + " + " + String.valueOf(b) + InfoCollector.getDivisionSign() + String.valueOf(c) + " - " + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a + b / c + d
    private void createProblem_16(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        b = b * c;

        int result = a + b / c + d;

        QUESTION = String.valueOf(a) + " + " + String.valueOf(b) + InfoCollector.getDivisionSign() + String.valueOf(c) + " + " + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a - b / c + d
    private void createProblem_17(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        b = b * c;

        int result = a - b / c + d;

        QUESTION = String.valueOf(a) + " - " + String.valueOf(b) + InfoCollector.getDivisionSign() + String.valueOf(c) + " + " + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a - b / c - d
    private void createProblem_18(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        b = b * c;

        int result = a - b / c - d;

        QUESTION = String.valueOf(a) + " - " + String.valueOf(b) + InfoCollector.getDivisionSign() + String.valueOf(c) + " - " + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a x b / c + d
    private void createProblem_19(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        if (a * b % c != 0) {
            b = b * c;
        }

        int result = a * b / c + d;

        QUESTION = String.valueOf(a) + " x " + String.valueOf(b) + InfoCollector.getDivisionSign() + String.valueOf(c) + " + " + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a / b * c + d
    private void createProblem_20(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        a = a * b;

        int result = a / b * c + d;

        QUESTION = String.valueOf(a) + InfoCollector.getDivisionSign() + String.valueOf(b) + " x " + String.valueOf(c) + " + " + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  (a + b) x c
    private void createProblem_21(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int result = (a + b) * c;

        QUESTION = "(" + String.valueOf(a) + " + " + String.valueOf(b) + ") x " + String.valueOf(c) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  (a - b) x c
    private void createProblem_22(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int result = (a - b) * c;

        QUESTION = "(" + String.valueOf(a) + " - " + String.valueOf(b) + ") x " + String.valueOf(c) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  (a + b) / c
    private void createProblem_23(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        if ((a + b) % c != 0){
            a = a * c;
            b = b * c;
        }

        int result = (a + b) / c;

        QUESTION = "(" + String.valueOf(a) + " + " + String.valueOf(b) + ")" + InfoCollector.getDivisionSign() + String.valueOf(c) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  (a - b) / c
    private void createProblem_24(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        a = a * c;

        int result = (a - b) / c;

        QUESTION = "(" + String.valueOf(a) + " - " + String.valueOf(b) + ")" + InfoCollector.getDivisionSign() + String.valueOf(c) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a + b x (c - d)
    private void createProblem_25(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        int result = a + b * (c - d);

        QUESTION = String.valueOf(a) + " + " + String.valueOf(b) + " x (" + String.valueOf(c) + " - " + String.valueOf(d) + ") = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  (a + b) x (c + d)
    private void createProblem_26(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        int result = (a + b) * (c + d);

        QUESTION = "(" + String.valueOf(a) + " + " + String.valueOf(b) + ") x (" + String.valueOf(c) + " + " + String.valueOf(d) + ") = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a + b x (c / d)
    private void createProblem_27(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        c = c * d;

        int result = a + b * c / d;

        QUESTION = String.valueOf(a) + " + " + String.valueOf(b) + " x (" + String.valueOf(c) + InfoCollector.getDivisionSign() + String.valueOf(d) + ") = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  (a - b) x c + d
    private void createProblem_28(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        int result = (a - b) * c + d;

        QUESTION = "(" + String.valueOf(a) + " - " + String.valueOf(b) + ") x " + String.valueOf(c) + " + " + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  (a - b) x (c - d)
    private void createProblem_29(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        int result = (a - b) * (c - d);

        QUESTION = "(" + String.valueOf(a) + " - " + String.valueOf(b) + ") x (" + String.valueOf(c) + " - " + String.valueOf(d) + ") = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a x (b + c) x d
    private void createProblem_30(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        int result = a * (b + c) * d;

        QUESTION = String.valueOf(a) + " x (" + String.valueOf(b) + " + " + String.valueOf(c) + ") x " + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a x (b - c) / d
    private void createProblem_31(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        if ((a * b) % d != 0){
            a = a  * d;
        }

        int result = a * (b - c) / d;

        QUESTION = String.valueOf(a) + " x (" + String.valueOf(b) + " - " + String.valueOf(c) + ")" + InfoCollector.getDivisionSign() + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a x (b + c) / d
    private void createProblem_32(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        if (a * (b + c) % d != 0){
            a = a * d;
        }

        int result = a * (b + c) / d;

        QUESTION = String.valueOf(a) + " x (" + String.valueOf(b) + " + " + String.valueOf(c) + ")" + InfoCollector.getDivisionSign() + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  (a / b + c) / d
    private void createProblem_33(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        if ((a / b + c) % d != 0){
            a = a * d;
            c = c * d;
        }
        a = a * b;

        int result = (a / b + c) / d;

        QUESTION = "(" + String.valueOf(a) + InfoCollector.getDivisionSign() + String.valueOf(b) + " + " + String.valueOf(c) + ")" + InfoCollector.getDivisionSign() + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  (a / (b - c)) / d
    private void createProblem_34(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        a = a * d;
        a = a * b;

        int result = (a / (b - c)) / d;

        QUESTION = "(" + String.valueOf(a) + InfoCollector.getDivisionSign() + "(" + String.valueOf(b) + " - " + String.valueOf(c) + "))" + InfoCollector.getDivisionSign() + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  (a + b) / (c - d)
    private void createProblem_35(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        if ((a + b) % c != 0){
            a = a * c;
            b = b * c;
        }
        
        int result = (a + b) / (c - d);

        QUESTION = "(" + String.valueOf(a) + " + " + String.valueOf(b) + ")" + InfoCollector.getDivisionSign() + "(" + String.valueOf(c) + " - " + String.valueOf(d) + ") = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  (a + b) / c + d
    private void createProblem_36(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        if ((a + b) % c != 0){
            a = a * c;
            b = b * c;
        }

        int result = (a + b) / c + d;

        QUESTION = "(" + String.valueOf(a) + " + " + String.valueOf(b) + ")" + InfoCollector.getDivisionSign() + String.valueOf(c) + " + " + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a - b / (c + d)
    private void createProblem_37(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        b = b * (c + d);

        int result = a - b / (c + d);

        QUESTION = String.valueOf(a) + " - " + String.valueOf(b) + InfoCollector.getDivisionSign() + "(" + String.valueOf(c) + " + " + String.valueOf(d) + ") = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  (a - b) / (c - d)
    private void createProblem_38(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        a = a * c;

        int result = (a - b) / (c - d);

        QUESTION = "(" + String.valueOf(a) + " - " + String.valueOf(b) + ")" + InfoCollector.getDivisionSign() + "(" + String.valueOf(c) + " - " + String.valueOf(d) + ") = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a x (b / c + d)
    private void createProblem_39(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        b = b * c;

        int result = a * (b / c + d);

        QUESTION = String.valueOf(a) + " x (" + String.valueOf(b) + InfoCollector.getDivisionSign() + String.valueOf(c) + " + " + String.valueOf(d) + ") = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    //  a / b * (c + d)
    private void createProblem_40() {
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        a = a * b;

        int result = a / b * (c + d);

        QUESTION = String.valueOf(a) + InfoCollector.getDivisionSign() + String.valueOf(b) + " x (" + String.valueOf(c) + " + " + String.valueOf(d) + ") = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 2);
        ANSWER_D = String.valueOf(result - 1);
    }    

    //  a + b x c
    private void createProblem_41(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int result = a + b * c;

        QUESTION = String.valueOf(a) + " ? " + String.valueOf(b) + " ? " + String.valueOf(c) + " = " + String.valueOf(result);
        RIGHT_ANSWER = ADD + " and " + MUL;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = ADD + " and " + SUB;
        ANSWER_C = SUB + " and " + MUL;
        ANSWER_D = ADD + " and " + DIV;
    }

    //  a - b / c
    private void createProblem_42(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        b = b * c;

        int result = a - b / c;

        QUESTION = String.valueOf(a) + " ? " + String.valueOf(b) + " ? " + String.valueOf(c) + " = " + String.valueOf(result);
        RIGHT_ANSWER = SUB + " and " + DIV;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SUB + " and " + ADD;
        ANSWER_C = ADD + " and " + DIV;
        ANSWER_D = SUB + " and " + SUB;
    }

    //  a + b x c / d
    private void createProblem_43(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        if (b * c % d != 0){
            b = d * b;
        }

        int result = a + b * c / d;

        QUESTION = String.valueOf(a) + " ? " + String.valueOf(b) + " ? " + String.valueOf(c) + " ? " + String.valueOf(d) + " = " + String.valueOf(result);
        RIGHT_ANSWER = ADD + ", " + MUL + ", " + DIV;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = ADD + ", " + MUL + ", " + SUB;
        ANSWER_C = ADD + ", " + DIV + ", " + MUL;
        ANSWER_D = SUB + ", " + MUL + ", " + DIV;
    }

    //  a - b / c + d
    private void createProblem_44(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        b = b * c;

        int result = a - b / c + d;

        QUESTION = String.valueOf(a) + " ? " + String.valueOf(b) + " ? " + String.valueOf(c) + " ? " + String.valueOf(d) + " = " + String.valueOf(result);
        RIGHT_ANSWER = SUB + ", " + DIV + ", " + ADD;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SUB + ", " + DIV + ", " + SUB;
        ANSWER_C = ADD + ", " + DIV + ", " + MUL;
        ANSWER_D = SUB + ", " + ADD + ", " + SUB;
    }

    //  a / b x c + d
    private void createProblem_45(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        a = a * b;

        int result = a / b * c + d;

        QUESTION = String.valueOf(a) + " ? " + String.valueOf(b) + " ? " + String.valueOf(c) + " ? " + String.valueOf(d) + " = " + String.valueOf(result);
        RIGHT_ANSWER = DIV + ", " + MUL + ", " + ADD;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = DIV + ", " + MUL + ", " + SUB;
        ANSWER_C = ADD + ", " + MUL + ", " + ADD;
        ANSWER_D = DIV + ", " + ADD + ", " + ADD;

        if (result > 1){
            ANSWER_D = String.valueOf(result - 1);
        }
    }

    //  (a + b) x c
    private void createProblem_46(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int result = (a + b) * c;

        QUESTION = "(" + String.valueOf(a) + " ? " + String.valueOf(b) + ") ? " + String.valueOf(c) + " = " + String.valueOf(result);
        RIGHT_ANSWER = ADD + " and " + MUL;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = ADD + " and " + SUB;
        ANSWER_C = ADD + " and " + ADD;
        ANSWER_D = MUL + " and " + MUL;
    }

    //  a + b x (c - d)
    private void createProblem_47(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        int result = a + b * (c - d);

        QUESTION = String.valueOf(a) + " ? " + String.valueOf(b) + " ? (" + String.valueOf(c) + " ? " + String.valueOf(d) + ") = " + String.valueOf(result);
        RIGHT_ANSWER = ADD + ", " + MUL + ", " + SUB;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = ADD + ", " + MUL + ", " + DIV;
        ANSWER_C = ADD + ", " + ADD + ", " + SUB;
        ANSWER_D = ADD + ", " + ADD + ", " + DIV;
    }

    //  (a / (b - c)) / d
    private void createProblem_48(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        a = a * d;
        a = a * b;

        int result = (a / (b - c)) / d;

        QUESTION = "(" + String.valueOf(a) + " ? " + "(" + String.valueOf(b) + " ? " + String.valueOf(c) + "))" + " ? " + String.valueOf(d) + " = " + String.valueOf(result);
        RIGHT_ANSWER = DIV + ", " + SUB + ", " + DIV;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = DIV + ", " + ADD + ", " + DIV;
        ANSWER_C = DIV + ", " + MUL + ", " + DIV;
        ANSWER_D = DIV + ", " + SUB + ", " + ADD;
    }

    //  a x (b / c + d)
    private void createProblem_49(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        b = b * c;

        int result = a * (b / c + d);

        QUESTION = String.valueOf(a) + " ? (" + String.valueOf(b) + " ? " + String.valueOf(c) + " ? " + String.valueOf(d) + ") = " + String.valueOf(result);
        RIGHT_ANSWER = MUL + ", " + DIV + ", " + ADD;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = MUL + ", " + ADD + ", " + DIV;
        ANSWER_C = DIV + ", " + DIV + ", " + DIV;
        ANSWER_D = MUL + ", " + ADD + ", " + ADD;
    }

    //  a / b * (c + d)
    private void createProblem_50(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        a = a * b;

        int result = a / b * (c + d);

        QUESTION = String.valueOf(a) + " ? " + String.valueOf(b) + " ? (" + String.valueOf(c) + " ? " + String.valueOf(d) + ") = " + String.valueOf(result);
        RIGHT_ANSWER = DIV + ", " + MUL + ", " + ADD;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = DIV + ", " + ADD + ", " + DIV;
        ANSWER_C = DIV + ", " + DIV + ", " + ADD;
        ANSWER_D = MUL + ", " + ADD + ", " + ADD;
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
