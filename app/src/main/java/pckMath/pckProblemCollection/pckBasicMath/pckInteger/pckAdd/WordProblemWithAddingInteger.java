package pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckAdd;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 4/26/2017.
 */

public class WordProblemWithAddingInteger extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public WordProblemWithAddingInteger(){
        createProblem();
        swapAnswer();
    }

    public WordProblemWithAddingInteger(String Question,
                                  String RightAnswer,
                                  String AnswerA,
                                  String AnswerB,
                                  String AnswerC,
                                  String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 16;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            case 14:
                createProblem_15();
                break;
            case 15:
                createProblem_16();
                break;
            default:
                createProblem_01();
                break;
        }

        return new WordProblemWithAddingInteger(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int result = a + b;

        QUESTION = "Jack has " + String.valueOf(a) + " pencils. He buys " + String.valueOf(b) + " more. How many pencils does he have in total?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_02(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int result = a + b;

        QUESTION = "There are " + String.valueOf(a) + " students in the Math club. "
                + String.valueOf(b) + " students joined the club. "
                + "How many students are there in the Math club.";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_03(){
        int a = MathUtility.getRandomPositiveNumber_3Digit();
        int b = MathUtility.getRandomPositiveNumber_3Digit();
        int result = a + b;

        QUESTION = "Jim's father is raising " + String.valueOf(a) + " chickens, his uncle has " + String.valueOf(b) + " ones. "
                + "How many chickens are they raising all together?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_04(){
        int a = MathUtility.getRandomPositiveNumber_4Digit();
        int b = MathUtility.getRandomPositiveNumber_3Digit();
        int result = a + b;

        QUESTION = "The university has " + String.valueOf(a) + " students. " +
                String.valueOf(b) + " freshmen are coming. "
                + "How many student does the university have in total?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_05(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int result = a + b;

        QUESTION = "There are " + String.valueOf(a) + " books on the shelf. " + String.valueOf(b) + " more on the table. "
                + "How many books are there in total?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_06(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int result = a + b;

        QUESTION = "Tom spent $" + String.valueOf(a) + " in the bookstore. And $" + String.valueOf(b) + " in a fast food restaurant. "
                + "How much money did Tom spend in total?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_07(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() * 2;
        int b = MathUtility.getRandomPositiveNumber_1Digit() * 2;
        int result = a + b;

        QUESTION = "It takes Jim " + String.valueOf(a) + " minutes to go from home to the bookstore. "
                + "And " + String.valueOf(b) + " minutes from the bookstore to the post office. "
                + "How long does it take Jim to go home from the post office if he drive the same way?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_08(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int result = a + b;

        QUESTION = "Add " + String.valueOf(a) + " to a number equals " + String.valueOf(result) + ". What is that number?";
        RIGHT_ANSWER = String.valueOf(b);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(b + 1);
        ANSWER_C = String.valueOf(b + 2);
        ANSWER_D = String.valueOf(b - 1);
    }

    private void createProblem_09(){
        int a = MathUtility.getRandomPositiveNumber_4Digit();
        int b = MathUtility.getRandomPositiveNumber_3Digit();
        int result = a + b;

        QUESTION = "There are " + String.valueOf(a) + " people at the meeting. " + String.valueOf(b) + " are coming. "
                + "How many people are there in total?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_10(){
        int a = MathUtility.getRandomPositiveNumber_3Digit();
        int b = MathUtility.getRandomPositiveNumber_3Digit();
        int result = a + b;

        QUESTION = "A tablet costs $" + String.valueOf(a) + ". A phone costs $" + String.valueOf(b)
                + ". How much money you have to pay to buy the both of them?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_11(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() * 2;
        int result = a + b;

        QUESTION = "Mike has " + String.valueOf(a) + " apples. His father buys " + String.valueOf(b) + " more. "
                + "How many apples do they have now?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_12(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int result = a + b;

        QUESTION = "Steve has " + String.valueOf(a) + " gold fish. His friend has " + String.valueOf(b) + ". How many fish do they have?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_13(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int result = a + b;

        QUESTION = "Adding a number to " + String.valueOf(b) + " equals " + String.valueOf(result) + ". What is that number?";
        RIGHT_ANSWER = String.valueOf(a);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a + 1);
        ANSWER_C = String.valueOf(a + 2);
        ANSWER_D = String.valueOf(a - 1);
    }

    private void createProblem_14(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int result = a + b;

        QUESTION = "Adding the first number to the second number equals the third one. Find these numbers.";
        RIGHT_ANSWER = String.valueOf(a) + ", " + String.valueOf(b) + ", " + String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a + 1) + ", " + String.valueOf(b) + ", " + String.valueOf(result);
        ANSWER_C = String.valueOf(a) + ", " + String.valueOf(b + 1) + ", " + String.valueOf(result);
        ANSWER_D = String.valueOf(a) + ", " + String.valueOf(b) + ", " + String.valueOf(result + 1);
    }

    private void createProblem_15(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();
        int result = a + b;

        QUESTION = "Jame and Jack are doing Math homework. Jame has to do " + String.valueOf(a) + " problems, and Jack has to do "
                + String.valueOf(b) + " problems. How many problems do they need to finish?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 5);
        ANSWER_C = String.valueOf(result + 10);
        ANSWER_D = String.valueOf(result - 10);
    }

    private void createProblem_16(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();
        int result = a + b;

        QUESTION = "Jack has to do " + String.valueOf(a) + " Maths homework problems, and " + String.valueOf(b)
                + " Chemistry problems. How many problems does he have to do in total?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 5);
        ANSWER_C = String.valueOf(result + 10);
        ANSWER_D = String.valueOf(result - 10);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
