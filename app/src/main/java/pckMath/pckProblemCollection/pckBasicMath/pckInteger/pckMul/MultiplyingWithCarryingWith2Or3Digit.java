package pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckMul;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 9/2/2017.
 */

public class MultiplyingWithCarryingWith2Or3Digit extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public MultiplyingWithCarryingWith2Or3Digit(){
        createProblem();
        swapAnswer();
    }

    public MultiplyingWithCarryingWith2Or3Digit(String Question,
                                   String RightAnswer,
                                   String AnswerA,
                                   String AnswerB,
                                   String AnswerC,
                                   String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 3;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            default:
                createProblem_01();
                break;
        }

        return new MultiplyingWithCarryingWith2Or3Digit(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        int c, d, loop_time = 0;
        while (true){
            loop_time++;

            c = MathUtility.getRandomPositiveNumber_1Digit();
            if (c * a > 10 && c * b > 10){
                break;
            }
            else {
                if (loop_time <= 100){
                    b = 8;
                    c = 7;
                    break;
                }
            }
        }

        loop_time = 0;
        while (true){
            loop_time++;

            d = MathUtility.getRandomPositiveNumber_1Digit();
            if (d * a > 10 && d * b > 10){
                break;
            }
            else {
                if (loop_time <= 100){
                    a = 6;
                    d = 9;
                    break;
                }
            }
        }

        a = a * 10 + b;
        b = c * 10 + d;

        int result = a * b;

        QUESTION = String.valueOf(a) + " x " + String.valueOf(b) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_02(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        int d, e, f, loop_time = 0;
        while (true){
            loop_time++;

            d = MathUtility.getRandomPositiveNumber_1Digit();
            if (d * a > 10 && d * b > 10 && d * c > 10){
                break;
            }
            else {
                if (loop_time <= 100){
                    a = 5;
                    b = 6;
                    c = 8;
                    d = 4;
                    break;
                }
            }
        }

        loop_time = 0;
        while (true){
            loop_time++;

            e = MathUtility.getRandomPositiveNumber_1Digit();
            if (e * a > 10 && e * b > 10 && e * c > 10){
                break;
            }
            else {
                if (loop_time <= 100){
                    e = 7;
                    break;
                }
            }
        }

        loop_time = 0;
        while (true){
            loop_time++;

            f = MathUtility.getRandomPositiveNumber_1Digit();
            if (f * a > 10 && f * b > 10 && f * c > 10){
                break;
            }
            else {
                if (loop_time <= 100){
                    f = 4;
                    break;
                }
            }
        }

        a = a * 100 + b * 10 + c;
        b = d * 100 + e * 10 + f;

        int result = a * b;

        QUESTION = String.valueOf(a) + " x " + String.valueOf(b) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_03(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        int d, e, loop_time = 0;
        while (true){
            loop_time++;

            d = MathUtility.getRandomPositiveNumber_1Digit();
            if (d * a > 10 && d * b > 10 && d * c > 10){
                break;
            }
            else {
                if (loop_time <= 100){
                    a = 5;
                    b = 6;
                    c = 7;
                    d = 8;
                    break;
                }
            }
        }

        loop_time = 0;
        while (true){
            loop_time++;

            e = MathUtility.getRandomPositiveNumber_1Digit();
            if (e * a > 10 && e * b > 10 && e * c > 10){
                break;
            }
            else {
                if (loop_time <= 100){
                    e = 9;
                    break;
                }
            }
        }

        a = a * 100 + b * 10 + c;
        b = d * 10 + e;

        int result = a * b;

        QUESTION = String.valueOf(a) + " x " + String.valueOf(b) + " = ?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
