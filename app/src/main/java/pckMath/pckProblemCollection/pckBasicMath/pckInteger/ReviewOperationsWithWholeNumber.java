package pckMath.pckProblemCollection.pckBasicMath.pckInteger;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckAdd.ReviewAddingWholeNumbers;
import pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckDiv.ReviewDividingWholeNumbers;
import pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckMul.ReviewMultiplyingWholeNumbers;
import pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckOrderOfOperations.ReviewOrderOfOperationsWithWholeNumbers;
import pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckSub.ReviewSubtractingWholeNumbers;

/**
 * Created by Cong on 9/5/2017.
 */

public class ReviewOperationsWithWholeNumber extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;
    private String ADD = "<b> + </b>", SUB = "<b> - </b>", MUL = "<b> x </b>", DIV = "<b>" + InfoCollector.getDivisionSign() + "</b>";

    public ReviewOperationsWithWholeNumber(){
        createProblem();
        swapAnswer();
    }

    public ReviewOperationsWithWholeNumber(String Question,
                                                  String RightAnswer,
                                                  String AnswerA,
                                                  String AnswerB,
                                                  String AnswerC,
                                                  String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        MathProblem Problem;
        int NoProblem = 9;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                Problem = new ReviewAddingWholeNumbers();
                break;
            case 1:
                Problem = new ReviewSubtractingWholeNumbers();
                break;
            case 2:
                Problem = new ReviewMultiplyingWholeNumbers();
                break;
            case 3:
                Problem = new ReviewDividingWholeNumbers();
                break;
            case 4:
                Problem = new ReviewOrderOfOperationsWithWholeNumbers();
                break;
            case 5:
                Problem = createProblem_ExtraAdd();
                break;
            case 6:
                Problem = createProblem_ExtraSub();
                break;
            case 7:
                Problem = createProblem_ExtraMul();
                break;
            case 8:
                Problem = createProblem_ExtraDiv();
                break;
            default:
                Problem = new ReviewAddingWholeNumbers();
                break;
        }

        QUESTION = Problem.getQuestion();
        RIGHT_ANSWER = Problem.getRightAnswer();
        ANSWER_A = Problem.getAnswerA();
        ANSWER_B = Problem.getAnswerB();
        ANSWER_C = Problem.getAnswerC();
        ANSWER_D = Problem.getAnswerD();

        return new ReviewOperationsWithWholeNumber(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private MathProblem createProblem_ExtraAdd(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = a + b;

        QUESTION = String.valueOf(a) + " ? " + String.valueOf(b) + " = " + String.valueOf(c);
        RIGHT_ANSWER = ADD;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SUB;
        ANSWER_C = MUL;
        ANSWER_D = DIV;

        return this;
    }

    private MathProblem createProblem_ExtraSub(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        a = a + b;
        int c = a - b;

        QUESTION = String.valueOf(a) + " ? " + String.valueOf(b) + " = " + String.valueOf(c);
        RIGHT_ANSWER = SUB;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = ADD;
        ANSWER_C = MUL;
        ANSWER_D = DIV;

        return this;
    }

    private MathProblem createProblem_ExtraMul(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int c = a * b;

        QUESTION = String.valueOf(a) + " ? " + String.valueOf(b) + " = " + String.valueOf(c);
        RIGHT_ANSWER = MUL;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SUB;
        ANSWER_C = ADD;
        ANSWER_D = DIV;

        return this;
    }

    private MathProblem createProblem_ExtraDiv(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        a = a * b;
        int c = a / b;

        QUESTION = String.valueOf(a) + " ? " + String.valueOf(b) + " = " + String.valueOf(c);
        RIGHT_ANSWER = DIV;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SUB;
        ANSWER_C = MUL;
        ANSWER_D = ADD;

        return this;
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
