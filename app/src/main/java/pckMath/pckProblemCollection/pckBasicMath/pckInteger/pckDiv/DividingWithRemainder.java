package pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckDiv;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 4/29/2017.
 */

public class DividingWithRemainder extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public DividingWithRemainder(){
        createProblem();
        swapAnswer();
    }

    public DividingWithRemainder(String Question,
                                     String RightAnswer,
                                     String AnswerA,
                                     String AnswerB,
                                     String AnswerC,
                                     String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 7;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            default:
                createProblem_01();
                break;
        }

        return new DividingWithRemainder(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        a = a * b + (MathUtility.getRandomPositiveNumber_1Digit() % (a - 1) + 1);

        int result = a / b;
        int remainder = a % b;

        QUESTION = String.valueOf(a) + InfoCollector.getDivisionSign() + String.valueOf(b) + " = ?";
        RIGHT_ANSWER = String.valueOf(result) + "R" + String.valueOf(remainder);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1) + "R" + String.valueOf(remainder);
        ANSWER_C = String.valueOf(result - 1) + "R" + String.valueOf(remainder);
        ANSWER_D = String.valueOf(result) + "R" + String.valueOf(remainder + 1);
    }

    private void createProblem_02(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        int result = a / b;
        int remainder = a % b;

        QUESTION = String.valueOf(a) + InfoCollector.getDivisionSign() + String.valueOf(b) + " = ?";

        if (remainder == 0) {
            RIGHT_ANSWER = String.valueOf(result);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(result + 1);
            ANSWER_C = String.valueOf(result + 2);
            ANSWER_D = String.valueOf(result + 3);

            if (result > 1) {
                ANSWER_D = String.valueOf(result - 1);
            }
        }
        else {
            RIGHT_ANSWER = String.valueOf(result) + "R" + String.valueOf(remainder);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(result + 1) + "R" + String.valueOf(remainder);
            ANSWER_C = String.valueOf(result) + "R" + String.valueOf(remainder + 1);
            ANSWER_D = String.valueOf(result + 1) + "R" + String.valueOf(remainder - 1);
        }
    }

    private void createProblem_03(){
        int a = MathUtility.getRandomPositiveNumber_3Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        int result = a / b;
        int remainder = a % b;

        QUESTION = String.valueOf(a) + InfoCollector.getDivisionSign() + String.valueOf(b) + " = ?";

        if (remainder == 0) {
            RIGHT_ANSWER = String.valueOf(result);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(result + 1);
            ANSWER_C = String.valueOf(result + 2);
            ANSWER_D = String.valueOf(result + 3);

            if (result > 1) {
                ANSWER_D = String.valueOf(result - 1);
            }
        }
        else {
            RIGHT_ANSWER = String.valueOf(result) + "R" + String.valueOf(remainder);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(result + 1) + "R" + String.valueOf(remainder);
            ANSWER_C = String.valueOf(result) + "R" + String.valueOf(remainder + 1);
            ANSWER_D = String.valueOf(result + 1) + "R" + String.valueOf(remainder - 1);
        }
    }

    private void createProblem_04(){
        int a;
        int b = MathUtility.getRandomPositiveNumber_2Digit();
        a = MathUtility.getRandomPositiveNumber_2Digit() + b;

        int result = a / b;
        int remainder = a % b;

        QUESTION = String.valueOf(a) + InfoCollector.getDivisionSign() + String.valueOf(b) + " = ?";

        if (remainder == 0) {
            RIGHT_ANSWER = String.valueOf(result);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(result + 1);
            ANSWER_C = String.valueOf(result + 2);
            ANSWER_D = String.valueOf(result + 3);

            if (result > 1) {
                ANSWER_D = String.valueOf(result - 1);
            }
        }
        else {
            RIGHT_ANSWER = String.valueOf(result) + "R" + String.valueOf(remainder);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(result + 1) + "R" + String.valueOf(remainder);
            ANSWER_C = String.valueOf(result) + "R" + String.valueOf(remainder + 1);
            ANSWER_D = String.valueOf(result + 1) + "R" + String.valueOf(remainder - 1);
        }
    }

    private void createProblem_05(){
        int a = MathUtility.getRandomPositiveNumber_3Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();

        int result = a / b;
        int remainder = a % b;

        QUESTION = String.valueOf(a) + InfoCollector.getDivisionSign() + String.valueOf(b) + " = ?";

        if (remainder == 0) {
            RIGHT_ANSWER = String.valueOf(result);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(result + 1);
            ANSWER_C = String.valueOf(result + 2);
            ANSWER_D = String.valueOf(result + 3);

            if (result > 1) {
                ANSWER_D = String.valueOf(result - 1);
            }
        }
        else {
            RIGHT_ANSWER = String.valueOf(result) + "R" + String.valueOf(remainder);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(result + 1) + "R" + String.valueOf(remainder);
            ANSWER_C = String.valueOf(result) + "R" + String.valueOf(remainder + 1);
            ANSWER_D = String.valueOf(result + 1) + "R" + String.valueOf(remainder - 1);
        }
    }

    private void createProblem_06(){
        int a = MathUtility.getRandomPositiveNumber_4Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();

        int result = a / b;
        int remainder = a % b;

        QUESTION = String.valueOf(a) + InfoCollector.getDivisionSign() + String.valueOf(b) + " = ?";

        if (remainder == 0) {
            RIGHT_ANSWER = String.valueOf(result);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(result + 1);
            ANSWER_C = String.valueOf(result + 2);
            ANSWER_D = String.valueOf(result + 3);

            if (result > 1) {
                ANSWER_D = String.valueOf(result - 1);
            }
        }
        else {
            RIGHT_ANSWER = String.valueOf(result) + "R" + String.valueOf(remainder);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(result + 1) + "R" + String.valueOf(remainder);
            ANSWER_C = String.valueOf(result) + "R" + String.valueOf(remainder + 1);
            ANSWER_D = String.valueOf(result + 1) + "R" + String.valueOf(remainder - 1);
        }
    }

    private void createProblem_07(){
        int a = MathUtility.getRandomPositiveNumber_4Digit();
        int b = MathUtility.getRandomPositiveNumber_3Digit();

        int result = a / b;
        int remainder = a % b;

        QUESTION = String.valueOf(a) + InfoCollector.getDivisionSign() + String.valueOf(b) + " = ?";

        if (remainder == 0) {
            RIGHT_ANSWER = String.valueOf(result);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(result + 1);
            ANSWER_C = String.valueOf(result + 2);
            ANSWER_D = String.valueOf(result + 3);

            if (result > 1) {
                ANSWER_D = String.valueOf(result - 1);
            }
        }
        else {
            RIGHT_ANSWER = String.valueOf(result) + "R" + String.valueOf(remainder);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(result + 1) + "R" + String.valueOf(remainder);
            ANSWER_C = String.valueOf(result) + "R" + String.valueOf(remainder + 1);
            ANSWER_D = String.valueOf(result + 1) + "R" + String.valueOf(remainder - 1);
        }
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
