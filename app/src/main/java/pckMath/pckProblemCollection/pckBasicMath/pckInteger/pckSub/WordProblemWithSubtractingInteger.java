package pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckSub;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 4/27/2017.
 */

public class WordProblemWithSubtractingInteger extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public WordProblemWithSubtractingInteger(){
        createProblem();
        swapAnswer();
    }

    public WordProblemWithSubtractingInteger(String Question,
                                     String RightAnswer,
                                     String AnswerA,
                                     String AnswerB,
                                     String AnswerC,
                                     String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 15;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            case 14:
                createProblem_15();
                break;
            default:
                createProblem_01();
                break;
        }

        return new WordProblemWithSubtractingInteger(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        a = a + b;
        int result = a - b;

        QUESTION = "Jim and Tom have the total of " + String.valueOf(a) + " pencils. Jim has " + String.valueOf(result)
                + ". How many pencil does Tom have?";
        RIGHT_ANSWER = String.valueOf(b);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(b + 1);
        ANSWER_C = String.valueOf(b + 2);
        ANSWER_D = String.valueOf(b - 1);
    }

    private void createProblem_02(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();
        a = a + b;
        int result = a - b;

        QUESTION = "Jack's uncle has " + String.valueOf(a) + " chickens. His dad has " + String.valueOf(b)
                + ". How many chickens does Jack's uncle have more than his father?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_03(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        a = a + b;
        int result = a - b;

        QUESTION = "There are " + String.valueOf(a) + " apples in his kitchen. Tom and his friends ate " + String.valueOf(b) + " of them. "
                + "How many apples are left in kitchen?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_04(){
        int a = MathUtility.getRandomPositiveNumber_3Digit();
        int b = MathUtility.getRandomPositiveNumber_3Digit();
        a = a + b;
        int result = a - b;

        QUESTION = "A phone costs $" + String.valueOf(a) + ". A tablet costs $" + String.valueOf(b)
                + ". How much more expensive is the phone than the tablet?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_05(){
        int a = MathUtility.getRandomPositiveNumber_4Digit();
        int b = MathUtility.getRandomPositiveNumber_3Digit();
        a = a + b;
        int result = a - b;

        QUESTION = "There are " + String.valueOf(a) + " students in the university. " + String.valueOf(b) + " of them will graduate. "
                + "How many students are left?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_06(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() * 2;
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        a = a + b;
        int result = a - b;

        QUESTION = "There are " + String.valueOf(a) + " students on the school bus. " + String.valueOf(b) + " went down. "
                + "How many students are left?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_07(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        a = a + b;
        int result = a - b;

        QUESTION = String.valueOf(b) + " taken away from " + String.valueOf(a) + " equals what number?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_08(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        a = a + b;
        int result = a - b;

        QUESTION = "What number taken away from " + String.valueOf(a) + " equals to " + String.valueOf(result) + "?";
        RIGHT_ANSWER = String.valueOf(b);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(b + 1);
        ANSWER_C = String.valueOf(b + 2);
        ANSWER_D = String.valueOf(b - 1);
    }

    private void createProblem_09(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        a = a + b;
        int result = a - b;

        QUESTION = String.valueOf(b) + " taken away from what number equals to " + String.valueOf(result) + "?";
        RIGHT_ANSWER = String.valueOf(a);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a + 1);
        ANSWER_C = String.valueOf(a + 2);
        ANSWER_D = String.valueOf(a - 1);
    }

    private void createProblem_10(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        a = a + b;
        int result = a - b;

        QUESTION = "Tom has " + String.valueOf(a) + " eggs. Jim has " + String.valueOf(b)
                + ". How many eggs does Tom has more than Jim?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_11(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        a = a + b;
        int result = a - b;

        QUESTION = "Team A planted " + String.valueOf(a) + " trees. Team B planted " + String.valueOf(b)
                + ". How many trees did team A plat more than team B?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_12(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() * 2;
        int b = MathUtility.getRandomPositiveNumber_1Digit() * 2;
        a = a + b;
        int result = a - b;

        QUESTION = "It takes " + String.valueOf(a) + " minutes to go from home to the mall. "
                + "And " + String.valueOf(b) + " minutes to go from home to the church. "
                + "How much longer does it take to go to the mall than go to the church";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_13(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() * 2;
        int b = MathUtility.getRandomPositiveNumber_1Digit() * 2;
        a = a + b;
        int result = a - b;

        QUESTION = "Jim cuts the rope into two pieces. The first one is " + String.valueOf(a) + " feet long. "
                + "The second piece is " + String.valueOf(b) + " feet long. "
                + "How much longer is the first one than the second?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_14(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();
        a = a + b;
        int result = a - b;

        QUESTION = "Jame has " + String.valueOf(a) + " Math homework problems. Jack has " + String.valueOf(b)
                + " problems less than Jame. How many homework problems does Jack have?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_15(){
        int a = MathUtility.getRandomPositiveNumber_2Digit() * 2;
        int b = a + MathUtility.getRandomPositiveNumber_1Digit();
        int result = a - b;

        QUESTION = "Jame has " + String.valueOf(a) + " Math homework problems. Jack has " + String.valueOf(b)
                + " problems. How many homework problems does Jack have more than Jame?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
