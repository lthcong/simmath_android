package pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckDiv;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 4/29/2017.
 */

public class WordProblemWithDividingInteger extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public WordProblemWithDividingInteger(){
        createProblem();
        swapAnswer();
    }

    public WordProblemWithDividingInteger(String Question,
                                  String RightAnswer,
                                  String AnswerA,
                                  String AnswerB,
                                  String AnswerC,
                                  String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 12;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            default:
                createProblem_01();
                break;
        }

        return new WordProblemWithDividingInteger(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() * 2;
        int b = MathUtility.getRandomPositiveNumber_1Digit() * 2;
        a = a * b;

        int result = a / b;

        QUESTION = "Tom has " + String.valueOf(a) + " cookies. He wants to share these cookies with " + String.valueOf(b - 1) + " friends. "
                + "How many cookies can each of them have?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_02(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        a = a * b;

        int result = a / b;

        QUESTION = "Tim has " + String.valueOf(a) + " books and " + String.valueOf(b) + " shelves. "
                + "How many books can he put on one shelf?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_03(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() * 2;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        a = a * b;

        int result = a / b;

        QUESTION = "Terry invites " + String.valueOf(a) + " friends to his party. He has " + String.valueOf(b) + " tables. "
                + "How many people should be in each table so everyone can have a seat?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_04(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();
        a = a * b;

        int result = a / b;

        QUESTION = "A farmer gets " + String.valueOf(a) + " apples from his garden. Each of the tree gives " + String.valueOf(b) + " apples. "
                + "How many apple trees does the farmer have?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_05(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();
        a = a * b;

        int result = a / b;

        QUESTION = "A library has " + String.valueOf(a) + " books. If all the books are put on the shelves. "
                + "Each of them can hold " + String.valueOf(b) + " books. How many shelves does the library have?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_06(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int b = MathUtility.getRandomPositiveNumber_2Digit();
        a = a * b;

        int result = a / b;

        QUESTION = "Tom has to put the cupcake into the boxes. He has " + String.valueOf(a) + " cupcakes, and " + String.valueOf(b) + " boxes. "
                + "How many cupcakes can he put in one box?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_07(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();
        a = a * b;

        int result = a / b;

        QUESTION = String.valueOf(a) + " people are invited to the meeting. The hall has " + String.valueOf(b) + " rows of chair. "
                + "How many people should be in one row so everyone can have a seat?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_08(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();
        a = a * b;

        int result = a / b;

        QUESTION = "Tom wants to plant some tree in the garden. He has " + String.valueOf(a) + " trees. He can plant " + String.valueOf(b) + " rows. "
                + "How many trees can he plant in one row?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_09(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();
        a = a * b;

        int result = a / b;

        QUESTION = "The school has " + String.valueOf(a) + " students want to take the Art class. One class can have " + String.valueOf(b) + " students. "
                + "How many classes should be opened for all the students?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_10(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();
        a = a * b;

        int result = a / b;

        QUESTION = "The parking place has the total of " + String.valueOf(a) + " slots. There are " + String.valueOf(b) + " slots in a row. "
                + "How many rows are there in the parking places?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_11(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 2;
        a = a * b;

        int result = a / b;

        QUESTION = "Tom has to finish " + String.valueOf(a) + " Math homework problems in " + String.valueOf(b) + " days. "
                + "How many problems does he have to do per day?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_12(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        a = a * b;

        int result = a / b;

        QUESTION = "Tom has to finish " + String.valueOf(a) + " Math homework problems. " +
                "He can do " + String.valueOf(b) + " problems per day. How long does it take him to finish all the problems?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
