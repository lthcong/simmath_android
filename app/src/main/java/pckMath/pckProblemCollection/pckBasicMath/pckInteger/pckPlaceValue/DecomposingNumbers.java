package pckMath.pckProblemCollection.pckBasicMath.pckInteger.pckPlaceValue;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 4/28/2017.
 */

public class DecomposingNumbers extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public DecomposingNumbers(){
        createProblem();
        swapAnswer();
    }

    public DecomposingNumbers(String Question,
                             String RightAnswer,
                             String AnswerA,
                             String AnswerB,
                             String AnswerC,
                             String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 8;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }

        return new DecomposingNumbers(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  DECOMPOSING
    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        int result = a * 10 + b;

        QUESTION = String.valueOf(result) + " = ?";
        RIGHT_ANSWER = String.valueOf(a) + " ten(s) and " + String.valueOf(b) + " one(s)";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(b) + " ten(s) and " + String.valueOf(a) + " one(s)";
        ANSWER_C = String.valueOf(result) + " tens";
        ANSWER_D = String.valueOf(a) + " ten(s) and 0 one";
    }

    //  DECOMPOSING
    private void createProblem_02(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        int result = a * 10 + b;

        int temp = MathUtility.getRandomNumber_1Digit() % a + 1;

        QUESTION = String.valueOf(result) + " = ?";
        RIGHT_ANSWER = String.valueOf(a - temp) + " ten(s) and " + String.valueOf(b + temp * 10) + " one(s)";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(b) + " ten(s) and " + String.valueOf(a) + " one(s)";
        ANSWER_C = String.valueOf(result) + " tens";
        ANSWER_D = String.valueOf(a) + " ten(s) and 0 one";
    }

    //  DECOMPOSING
    private void createProblem_03(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        int result = a * 100 + b * 10 + c;

        QUESTION = String.valueOf(result) + " = ?";
        RIGHT_ANSWER = String.valueOf(a) + " hundred(s), " + String.valueOf(b) + " ten(s), " + String.valueOf(c) + " one(s).";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a) + " hundred(s), " + String.valueOf(b + c) + " one(s).";
        ANSWER_C = String.valueOf(b) + " hundred(s), " + String.valueOf(c) + " ten(s)";
        ANSWER_D = String.valueOf(a + b) + " ten(s), " + String.valueOf(c) + " one(s).";
    }

    //  DECOMPOSING
    private void createProblem_04(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        int result = a * 100 + b * 10 + c;
        int temp = MathUtility.getRandomPositiveNumber_1Digit() % a + 1;

        QUESTION = String.valueOf(result) + " = ?";
        RIGHT_ANSWER = String.valueOf(a - temp) + " hundred(s), " + String.valueOf(temp * 10 + b) + " ten(s), " + String.valueOf(c) + " one(s).";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a) + " hundred(s), " + String.valueOf(b + c) + " one(s).";
        ANSWER_C = String.valueOf(b) + " hundred(s), " + String.valueOf(c) + " ten(s)";
        ANSWER_D = String.valueOf(a + b) + " ten(s), " + String.valueOf(c) + " one(s).";
    }

    //  DECOMPOSING
    private void createProblem_05(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        int result = a * 100 + b * 10 + c;
        int temp = MathUtility.getRandomPositiveNumber_1Digit() % b + 1;

        QUESTION = String.valueOf(result) + " = ?";
        RIGHT_ANSWER = String.valueOf(a) + " hundred(s), " + String.valueOf(b - temp) + " ten(s), " + String.valueOf(c + temp * 10) + " one(s).";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a) + " hundred(s), " + String.valueOf(b + c) + " one(s).";
        ANSWER_C = String.valueOf(b) + " hundred(s), " + String.valueOf(c) + " ten(s)";
        ANSWER_D = String.valueOf(a + b) + " ten(s), " + String.valueOf(c) + " one(s).";
    }

    //  DECOMPOSING
    private void createProblem_06(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        int result = a * 100 + b * 10 + c;

        QUESTION = String.valueOf(result) + " = ?";
        RIGHT_ANSWER = String.valueOf(a) + " hundred(s), " + String.valueOf(b * 10 + c) + " one(s).";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a) + " hundred(s), " + String.valueOf(b + c) + " one(s).";
        ANSWER_C = String.valueOf(b) + " hundred(s), " + String.valueOf(c) + " ten(s)";
        ANSWER_D = String.valueOf(a + b) + " ten(s), " + String.valueOf(c) + " one(s).";
    }

    //  DECOMPOSING
    private void createProblem_07(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        int result = a * 100 + b * 10 + c;

        QUESTION = String.valueOf(result) + " = ?";
        RIGHT_ANSWER = String.valueOf(a * 10 + b) + " ten(s), " + String.valueOf(c) + " one(s).";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a) + " hundred(s), " + String.valueOf(b + c) + " one(s).";
        ANSWER_C = String.valueOf(b) + " hundred(s), " + String.valueOf(c) + " ten(s)";
        ANSWER_D = String.valueOf(a + b) + " ten(s), " + String.valueOf(c) + " one(s).";
    }

    //  DECOMPOSING
    private void createProblem_08(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        int result = a * 100 + b * 10 + c;
        int temp = MathUtility.getRandomPositiveNumber_1Digit() % b + 1;

        QUESTION = String.valueOf(result) + " = ?";
        RIGHT_ANSWER = String.valueOf(a * 10 + temp) + " ten(s), " + String.valueOf((b - temp) * 10 + c) + " one(s).";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a) + " hundred(s), " + String.valueOf(b + c) + " one(s).";
        ANSWER_C = String.valueOf(b) + " hundred(s), " + String.valueOf(c) + " ten(s)";
        ANSWER_D = String.valueOf(a + b) + " ten(s), " + String.valueOf(c) + " one(s).";
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
