package pckMath.pckProblemCollection.pckAlgebraTwo.pckSequencesAndSeries.pckSerices;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;
import pckMath.pckProblemCollection.pckIntermediateMath.pckExponential.Exponential;

/**
 * Created by Cong on 8/19/2017.
 */

public class GeometricSeries extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public GeometricSeries(){
        createProblem();
        swapAnswer();
    }

    public GeometricSeries(String Question,
                            String RightAnswer,
                            String AnswerA,
                            String AnswerB,
                            String AnswerC,
                            String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 8;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }
        return new GeometricSeries(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int Term_1 = MathUtility.getRandomNumber_1Digit();
        int Ratio = MathUtility.getRandomNumber_1Digit();
        int NthTerm = MathUtility.getRandomPositiveNumber_1Digit();

        int Term_N = Term_1 * (new Exponential(Ratio, NthTerm - 1)).getValue();

        QUESTION = "Given a<sub>1</sub> = " + String.valueOf(Term_1) + ", r = " + String.valueOf(Ratio) + ". a" + InfoCollector.getSub(String.valueOf(NthTerm)) + " = ?";
        RIGHT_ANSWER = String.valueOf(Term_N);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Term_N + 1);
        ANSWER_C = String.valueOf(Term_N - 1);
        ANSWER_D = String.valueOf(Term_N + 2);
    }

    private void createProblem_02(){
        int Term_1 = MathUtility.getRandomNumber_1Digit();
        int Ratio = MathUtility.getRandomNumber_1Digit();
        int NthTerm = MathUtility.getRandomPositiveNumber_1Digit();

        int Term_N = Term_1 * (new Exponential(Ratio, NthTerm - 1)).getValue();

        QUESTION = "Given a<sub>1</sub> = " + String.valueOf(Term_1) + ", r = " + String.valueOf(Ratio) + ". a<sub>n</sub> = " + String.valueOf(Term_N) + ". n = ?";
        RIGHT_ANSWER = String.valueOf(NthTerm);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(NthTerm + 1);
        ANSWER_C = String.valueOf(NthTerm - 1);
        ANSWER_D = String.valueOf(NthTerm + 2);
    }

    private void createProblem_03(){
        int Term_1 = MathUtility.getRandomNumber_1Digit();
        int Ratio = MathUtility.getRandomNumber_1Digit();
        int NthTerm = MathUtility.getRandomPositiveNumber_1Digit();

        int Term_N = Term_1 * (new Exponential(Ratio, NthTerm - 1)).getValue();

        QUESTION = "a" + InfoCollector.getSub(String.valueOf(NthTerm)) + " = " + String.valueOf(Term_N)
                + ", r = " + String.valueOf(Ratio) + ". a<sub>1</sub> = ?";
        RIGHT_ANSWER = String.valueOf(Term_1);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Term_1 + 1);
        ANSWER_C = String.valueOf(Term_1 - 1);
        ANSWER_D = String.valueOf(Term_1 + 2);
    }

    private void createProblem_04(){
        int Term_1 = MathUtility.getRandomNumber_1Digit();
        int Ratio = MathUtility.getRandomNumber_1Digit();
        int NthTerm = MathUtility.getRandomPositiveNumber_1Digit();

        int Term_N = Term_1 * (new Exponential(Ratio, NthTerm - 1)).getValue();

        QUESTION = "a" + InfoCollector.getSub(String.valueOf(NthTerm)) + " = " + String.valueOf(Term_N)
                + ", a<sub>1</sub> = " + String.valueOf(Term_1) + ". r = ?";
        RIGHT_ANSWER = String.valueOf(Ratio);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Ratio + 1);
        ANSWER_C = String.valueOf(Ratio - 1);
        ANSWER_D = String.valueOf(Ratio + 2);
    }

    private void createProblem_05(){
        int Term_1 = MathUtility.getRandomNumber_1Digit();
        int Ratio = MathUtility.getRandomNumber_1Digit();
        int NthTerm = MathUtility.getRandomPositiveNumber_1Digit();

        if (Ratio == 1){
            createProblem_05();
        }

        double Sum = (double) Term_1 * (double) (1 - (new Exponential(Ratio, NthTerm)).getValue()) / (double) (1 - Ratio);
        Sum = MathUtility.round_4_Decimal(Sum);

        QUESTION = "a<sub>1</sub> = " + String.valueOf(Term_1) + ", r = " + String.valueOf(Ratio) + ", S" + InfoCollector.getSub(String.valueOf(NthTerm)) + " = ?";
        RIGHT_ANSWER = String.valueOf(Sum);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Sum + 3.0));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Sum - 5.0));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Sum + 5.0));
    }

    private void createProblem_06(){
        int Term_1 = MathUtility.getRandomNumber_1Digit();
        int Ratio = MathUtility.getRandomNumber_1Digit();
        int NthTerm = MathUtility.getRandomPositiveNumber_1Digit();

        if (Ratio == 1){
            createProblem_06();
        }

        double Sum = (double) Term_1 * (double) (1 - (new Exponential(Ratio, NthTerm)).getValue()) / (double) (1 - Ratio);
        Sum = MathUtility.round_4_Decimal(Sum);

        QUESTION = "S" + InfoCollector.getSub(String.valueOf(NthTerm)) + " = " + String.valueOf(Term_1) + ", r = " + String.valueOf(Ratio) + ", a<sub>1</sub> = ?";
        RIGHT_ANSWER = String.valueOf(Term_1);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Term_1 + 1);
        ANSWER_C = String.valueOf(Term_1 - 1);
        ANSWER_D = String.valueOf(Term_1 + 2);
    }

    private void createProblem_07(){
        Fraction Term_1 = FractionMath.getRandomFraction();
        Fraction Ratio = FractionMath.getRandomUnitFraction();

        Fraction Term_2 = FractionMath.multiplyFraction(Term_1, Ratio);
        Fraction Term_3 = FractionMath.multiplyFraction(Term_2, Ratio);
        Fraction Term_4 = FractionMath.multiplyFraction(Term_3, Ratio);
        Fraction Term_5 = FractionMath.multiplyFraction(Term_4, Ratio);

        Fraction Sum = FractionMath.divideFraction(Term_1,
                FractionMath.subtractFraction(new Fraction(1, 1), Ratio));

        QUESTION = "Find the sum of the infinite geometric sequences:<br>"
                + Term_1.toString() + ", " + Term_2.toString() + ", " + Term_3.toString() + ", " + Term_4.toString() + ", "
                + Term_5.toString() + ", ..." ;
        RIGHT_ANSWER = Sum.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getRandomFraction().toString();
        ANSWER_C = FractionMath.getRandomFraction().toString();
        ANSWER_D = FractionMath.getRandomFraction().toString();
    }

    private void createProblem_08(){
        Fraction Term_1 = new Fraction(MathUtility.getRandomNumber_1Digit());
        Fraction Ratio = FractionMath.getRandomUnitFraction();

        Fraction Term_2 = FractionMath.multiplyFraction(Term_1, Ratio);
        Fraction Term_3 = FractionMath.multiplyFraction(Term_2, Ratio);
        Fraction Term_4 = FractionMath.multiplyFraction(Term_3, Ratio);
        Fraction Term_5 = FractionMath.multiplyFraction(Term_4, Ratio);

        Fraction Sum = FractionMath.divideFraction(Term_1,
                FractionMath.subtractFraction(new Fraction(1, 1), Ratio));

        QUESTION = "Find the sum of the infinite geometric sequences:<br>"
                + Term_1.toString() + ", " + Term_2.toString() + ", " + Term_3.toString() + ", " + Term_4.toString() + ", "
                + Term_5.toString() + ", ..." ;
        RIGHT_ANSWER = Sum.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getRandomFraction().toString();
        ANSWER_C = FractionMath.getRandomFraction().toString();
        ANSWER_D = FractionMath.getRandomFraction().toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
