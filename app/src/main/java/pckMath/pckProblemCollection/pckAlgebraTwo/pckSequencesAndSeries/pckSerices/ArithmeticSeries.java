package pckMath.pckProblemCollection.pckAlgebraTwo.pckSequencesAndSeries.pckSerices;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 8/19/2017.
 */

public class ArithmeticSeries extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public ArithmeticSeries(){
        createProblem();
        swapAnswer();
    }

    public ArithmeticSeries(String Question,
                               String RightAnswer,
                               String AnswerA,
                               String AnswerB,
                               String AnswerC,
                               String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 4;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            default:
                createProblem_01();
                break;
        }
        return new ArithmeticSeries(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int Term_1 = MathUtility.getRandomNumber_1Digit();
        int Diff = MathUtility.getRandomNumber_1Digit();
        int NthTerm = MathUtility.getRandomPositiveNumber();

        int Term_N = Term_1 + (NthTerm - 1) * Diff;

        QUESTION = "Given a<sub>1</sub> = " + String.valueOf(Term_1) + ", d = " + String.valueOf(Diff)
                + ". a" + InfoCollector.getSub(String.valueOf(NthTerm)) + " = ?";
        RIGHT_ANSWER = String.valueOf(Term_N);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Term_N + 1);
        ANSWER_C = String.valueOf(Term_N - 1);
        ANSWER_D = String.valueOf(Term_N * -1);
    }

    private void createProblem_02(){
        int Term_1 = MathUtility.getRandomNumber_1Digit();
        int Diff = MathUtility.getRandomNumber_1Digit();
        int NthTerm = MathUtility.getRandomPositiveNumber();

        int Term_N = Term_1 + (NthTerm - 1) * Diff;

        QUESTION = "Given a<sub>1</sub> = " + String.valueOf(Term_1) + ", d = " + String.valueOf(Diff)
                + ". a<sub>n</sub> = " + String.valueOf(Term_N) + ". n = ?";
        RIGHT_ANSWER = String.valueOf(NthTerm);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(NthTerm + 1);
        ANSWER_C = String.valueOf(NthTerm - 1);
        ANSWER_D = String.valueOf(NthTerm + 2);
    }

    private void createProblem_03(){
        int Term_1 = MathUtility.getRandomNumber_1Digit();
        int Diff = MathUtility.getRandomNumber_1Digit();
        int NthTerm = MathUtility.getRandomPositiveNumber();

        int Term_N = Term_1 + (NthTerm - 1) * Diff;

        double Sum = (double) (NthTerm) / 2.0  * (double) (Term_1 * Term_N);
        Sum = MathUtility.round_1_Decimal(Sum);

        QUESTION = "Find the sum of the first " + String.valueOf(NthTerm) + " terms. Given a<sub>1</sub> = " + String.valueOf(Term_1)
                + ", a" + InfoCollector.getSub(String.valueOf(NthTerm)) + " = " + String.valueOf(Term_N) + ".";
        RIGHT_ANSWER = String.valueOf(Sum);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Sum + 1.5));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Sum - 1.5));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Sum + 1.0));
    }

    private void createProblem_04(){
        int Term_1 = MathUtility.getRandomNumber_1Digit();
        int Diff = MathUtility.getRandomNumber_1Digit();
        int NthTerm = MathUtility.getRandomPositiveNumber();

        int Term_N = Term_1 + (NthTerm - 1) * Diff;

        double Sum = (double) (NthTerm) / 2.0  * (double) (Term_1 * Term_N);
        Sum = MathUtility.round_1_Decimal(Sum);

        QUESTION = "Given S<sub>n</sub> = " + String.valueOf(Sum)
                + ", a<sub>1</sub> = " + String.valueOf(Term_1)
                + ", a<sub>n</sub> = " + String.valueOf(Term_N) + ". n = ?";
        RIGHT_ANSWER = String.valueOf(NthTerm);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(NthTerm + 1);
        ANSWER_C = String.valueOf(NthTerm + 2);
        ANSWER_D = String.valueOf(NthTerm - 1);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
