package pckMath.pckProblemCollection.pckAlgebraTwo.pckSequencesAndSeries.pckSequences;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 8/19/2017.
 */

public class ArithmeticSequences extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public ArithmeticSequences(){
        createProblem();
        swapAnswer();
    }

    public ArithmeticSequences(String Question,
                               String RightAnswer,
                               String AnswerA,
                               String AnswerB,
                               String AnswerC,
                               String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }
        return new ArithmeticSequences(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int Diff = MathUtility.getRandomNumber_1Digit();
        int Term_1 = MathUtility.getRandomNumber_1Digit();
        int Term_2 = Term_1 + Diff;
        int Term_3 = Term_2 + Diff;
        int Term_4 = Term_3 + Diff;
        int Term_5 = Term_4 + Diff;

        QUESTION = "Find the 5th term of the sequence: " + String.valueOf(Term_1) + ", " + String.valueOf(Term_2) + ", "
                + String.valueOf(Term_3) + ", " + String.valueOf(Term_4) + ", ...";
        RIGHT_ANSWER = String.valueOf(Term_5);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Term_4);
        ANSWER_C = String.valueOf(Term_5 + Diff);
        ANSWER_D = String.valueOf(Term_1 - Diff);
    }

    private void createProblem_02(){
        int Diff = MathUtility.getRandomNumber_1Digit();
        int Term_1 = MathUtility.getRandomNumber_1Digit();
        int Term_2 = Term_1 + Diff;
        int Term_3 = Term_2 + Diff;
        int Term_4 = Term_3 + Diff;
        int Term_5 = Term_4 + Diff;

        QUESTION = "Find the difference of the sequence: " + String.valueOf(Term_1) + ", " + String.valueOf(Term_2) + ", "
                + String.valueOf(Term_3) + ", " + String.valueOf(Term_4) + ", " + String.valueOf(Term_5) + ".";
        RIGHT_ANSWER = String.valueOf(Diff);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Diff * -1);
        ANSWER_C = String.valueOf(Diff + 1);
        ANSWER_D = String.valueOf(Diff - 1);
    }

    private void createProblem_03(){
        int Diff = MathUtility.getRandomNumber_1Digit();
        int Term_1 = MathUtility.getRandomNumber_1Digit();
        int Term_2 = Term_1 + Diff;
        int Term_3 = Term_2 + Diff;
        int Term_4 = Term_3 + Diff;
        int Term_5 = Term_4 + Diff;

        QUESTION = "Find the first 5 terms of the sequence. Given a<sub>1</sub>" + String.valueOf(Term_1) + ", d = " + String.valueOf(Diff);
        RIGHT_ANSWER = String.valueOf(Term_1) + ", " + String.valueOf(Term_2) + ", "
                + String.valueOf(Term_3) + ", " + String.valueOf(Term_4) + ", " + String.valueOf(Term_5) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Term_1) + ", " + String.valueOf(Term_2 + 1) + ", "
                + String.valueOf(Term_3) + ", " + String.valueOf(Term_4) + ", " + String.valueOf(Term_5) + ".";
        ANSWER_C = String.valueOf(Term_1) + ", " + String.valueOf(Term_2) + ", "
                + String.valueOf(Term_3) + ", " + String.valueOf(Term_4 + 2) + ", " + String.valueOf(Term_5) + ".";
        ANSWER_D = String.valueOf(Term_1) + ", " + String.valueOf(Term_2) + ", "
                + String.valueOf(Term_3 + 1) + ", " + String.valueOf(Term_4) + ", " + String.valueOf(Term_5) + ".";
    }

    private void createProblem_04(){
        int a = MathUtility.getRandomNumber_1Digit();
        int n = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomNumber_1Digit();

        int Result = a * n + b;

        QUESTION = "a<sub>n</sub> = " + String.valueOf(a) + "n + " + String.valueOf(b) + ".<br>"
                + "Find a" + InfoCollector.getSub(String.valueOf(n)) + ".";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result - 1);
        ANSWER_D = String.valueOf(Result + 2);
    }

    private void createProblem_05(){
        Fraction Diff = FractionMath.getRandomFraction();
        Fraction Term_1 = FractionMath.getRandomFraction();
        Fraction Term_2 = FractionMath.addFraction(Term_1, Diff);
        Fraction Term_3 = FractionMath.addFraction(Term_2, Diff);
        Fraction Term_4 = FractionMath.addFraction(Term_3, Diff);
        Fraction Term_5 = FractionMath.addFraction(Term_4, Diff);

        QUESTION = "Find the 5th term of the sequence: " + Term_1.toString() + ", " + Term_2.toString() + ", "
                + Term_3.toString() + ", " + Term_4.toString() + ", ...";
        RIGHT_ANSWER = Term_5.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = Term_4.toString();
        ANSWER_C = FractionMath.addFraction(Term_5, Diff).toString();
        ANSWER_D = FractionMath.subtractFraction(Term_1, Diff).toString();
    }

    private void createProblem_06(){
        Fraction a = FractionMath.getRandomUnitFraction();
        int n = MathUtility.getRandomPositiveNumber_2Digit();
        Fraction b = FractionMath.getRandomUnitFraction();

        Fraction Result = FractionMath.addFraction(FractionMath.multiplyFraction(a, new Fraction(n)), b);

        QUESTION = "a<sub>n</sub> = " + a.toString() + " * n + " + b.toString() + ".<br>"
                + "Find a" + InfoCollector.getSub(String.valueOf(n)) + ".";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.subtractFraction(Result, new Fraction(1)).toString();
        ANSWER_C = FractionMath.addFraction(Result, new Fraction(1)).toString();
        ANSWER_D = FractionMath.addFraction(Result, new Fraction(2)).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
