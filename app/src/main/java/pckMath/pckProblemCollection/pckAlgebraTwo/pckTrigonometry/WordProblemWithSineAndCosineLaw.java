package pckMath.pckProblemCollection.pckAlgebraTwo.pckTrigonometry;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 8/14/2017.
 */

public class WordProblemWithSineAndCosineLaw extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public WordProblemWithSineAndCosineLaw(){
        createProblem();
        swapAnswer();
    }

    public WordProblemWithSineAndCosineLaw(String Question,
                                       String RightAnswer,
                                       String AnswerA,
                                       String AnswerB,
                                       String AnswerC,
                                       String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 9;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            default:
                createProblem_01();
                break;
        }
        return new WordProblemWithSineAndCosineLaw(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int PathA = MathUtility.getRandomPositiveNumber_1Digit() + 30;
        int PathB = MathUtility.getRandomPositiveNumber_1Digit() + 30;
        int Angle = MathUtility.getRandomAngleLessThan90Degree();

        double Distance = (double) (PathA * PathA + PathB * PathB) - (double) (2 * PathA * PathB) * Math.cos(MathUtility.toRadian(Angle));
        Distance = MathUtility.round_2_Decimal(Math.sqrt(Distance));

        QUESTION = "Two ships left the same port. Ship A went " + String.valueOf(PathA) + "km, ship B went " + String.valueOf(PathB) + "km. "
                + "The angle between the two paths is " + String.valueOf(Angle) + InfoCollector.getDegreeSign() + ". Find the distance between the two ships.";
        RIGHT_ANSWER = String.valueOf(Distance);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Distance + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Distance - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Distance + 15.0));
    }

    private void createProblem_02(){
        int PathA = MathUtility.getRandomPositiveNumber_2Digit() + 100;
        int PathB = MathUtility.getRandomPositiveNumber_2Digit() + 100;
        int Angle = MathUtility.getRandomAngleLessThan90Degree();

        double Distance = (double) (PathA * PathA + PathB * PathB) - (double) (2 * PathA * PathB) * Math.cos(MathUtility.toRadian(Angle));
        Distance = MathUtility.round_2_Decimal(Math.sqrt(Distance));

        QUESTION = "Two delivery trucks left the same warehouse. Truck A is heading to the city " + String.valueOf(PathA)
                + "km away. Truck B is heading to another city " + String.valueOf(PathB) + "km away. "
                + "The angle between the two paths is " + String.valueOf(Angle) + InfoCollector.getDegreeSign() + ". Find the distance between the two cities.";
        RIGHT_ANSWER = String.valueOf(Distance);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Distance + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Distance - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Distance + 15.0));
    }

    private void createProblem_03(){
        int PathA = MathUtility.getRandomPositiveNumber_2Digit() + 300;
        int PathB = MathUtility.getRandomPositiveNumber_2Digit() + 300;
        int Angle = MathUtility.getRandomAngleLessThan90Degree();

        double Distance = (double) (PathA * PathA + PathB * PathB) - (double) (2 * PathA * PathB) * Math.cos(MathUtility.toRadian(Angle));
        Distance = MathUtility.round_2_Decimal(Math.sqrt(Distance));

        QUESTION = "Two airplanes left the same airport. The first airplane destination is " + String.valueOf(PathA) + "km away, the second airplane destination is " + String.valueOf(PathB) + "km away. "
                + "The angle between the two paths is " + String.valueOf(Angle) + InfoCollector.getDegreeSign() + ". Find the distance between the two destinations.";
        RIGHT_ANSWER = String.valueOf(Distance);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Distance + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Distance - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Distance + 15.0));
    }

    private void createProblem_04(){
        int PathA = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int PathB = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Angle = MathUtility.getRandomAngleLessThan90Degree();

        double Distance = (double) (PathA * PathA + PathB * PathB) - (double) (2 * PathA * PathB) * Math.cos(MathUtility.toRadian(Angle));
        Distance = MathUtility.round_2_Decimal(Math.sqrt(Distance));

        QUESTION = "Two friends are running on 2 different paths. The first one goes " + String.valueOf(PathA) + " miles. The second one goes " + String.valueOf(PathB) + " miles. "
                + "The angle formed by the 2 paths is " + String.valueOf(Angle) + InfoCollector.getDegreeSign() + ". Find the distance between them.";
        RIGHT_ANSWER = String.valueOf(Distance);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Distance + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Distance - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Distance + 15.0));
    }

    private void createProblem_05(){
        int PathA = MathUtility.getRandomPositiveNumber_1Digit() + 30;
        int PathB = MathUtility.getRandomPositiveNumber_1Digit() + 30;
        int Angle = MathUtility.getRandomAngleLessThan90Degree();

        double Distance = (double) (PathA * PathA + PathB * PathB) - (double) (2 * PathA * PathB) * Math.cos(MathUtility.toRadian(Angle));
        Distance = MathUtility.round_2_Decimal(Math.sqrt(Distance));

        QUESTION = "Two ships left the same port. Ship A went " + String.valueOf(PathA) + "km. The distance between the 2 ships is " + String.valueOf(Distance) + " km. "
                + "The angle between the two paths is " + String.valueOf(Angle) + InfoCollector.getDegreeSign() + ". How far did ship B go (round to the nearest kilometer)?";
        RIGHT_ANSWER = String.valueOf(PathB);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(PathB + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(PathB - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(PathB + 15.0));
    }

    private void createProblem_06(){
        int PathA = MathUtility.getRandomPositiveNumber_2Digit() + 100;
        int PathB = MathUtility.getRandomPositiveNumber_2Digit() + 100;
        int Angle = MathUtility.getRandomAngleLessThan90Degree();

        double Distance = (double) (PathA * PathA + PathB * PathB) - (double) (2 * PathA * PathB) * Math.cos(MathUtility.toRadian(Angle));
        Distance = MathUtility.round_2_Decimal(Math.sqrt(Distance));

        QUESTION = "Two delivery trucks left the same warehouse. Truck A is heading to the city " + String.valueOf(PathA) + "km away. Truck B is heading to another city. "
                + "The 2 cities are " + String.valueOf(Distance) + "km away from each other. "
                + "The angle between the two paths is " + String.valueOf(Angle) + InfoCollector.getDegreeSign() + ". How far does the truck B have to go (round to the nearest kilometer)?";
        RIGHT_ANSWER = String.valueOf(PathB);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(PathB + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(PathB - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(PathB + 15.0));
    }

    private void createProblem_07(){
        int PathA = MathUtility.getRandomPositiveNumber_2Digit() + 300;
        int PathB = MathUtility.getRandomPositiveNumber_2Digit() + 300;
        int Angle = MathUtility.getRandomAngleLessThan90Degree();

        double Distance = (double) (PathA * PathA + PathB * PathB) - (double) (2 * PathA * PathB) * Math.cos(MathUtility.toRadian(Angle));
        Distance = MathUtility.round_2_Decimal(Math.sqrt(Distance));

        QUESTION = "Two airplanes left the same airport, and heading 2 different destinations which are " + String.valueOf(Distance) + "km apart. "
                + "The first airplane has to go " + String.valueOf(PathA) + "km away. "
                + "The angle between the two paths is " + String.valueOf(Angle) + InfoCollector.getDegreeSign() + ". How far does the second airplane have to go (round to the nearest kilometer)?";
        RIGHT_ANSWER = String.valueOf(PathB);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(PathB + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(PathB - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(PathB + 15.0));
    }

    private void createProblem_08(){
        int AB = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int A = MathUtility.getRandomAngleLessThan90Degree();
        int C = MathUtility.getRandomAngleLessThan90Degree();

        double BC = (double) (AB) / Math.sin(C) * Math.sin(MathUtility.toRadian(A));
        BC = MathUtility.round_2_Decimal(BC);

        QUESTION = "The distance between the cities A and B is " + String.valueOf(AB)
                + " miles. The angle formed by the road from A to B and the road from A to C is " + String.valueOf(A) + InfoCollector.getDegreeSign()
                + ". The angle formed by the road from A to C and the road from B to C is " + String.valueOf(C) + InfoCollector.getDegreeSign()
                + "Find the distance between B and C.";
        RIGHT_ANSWER = String.valueOf(BC);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(BC + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(BC - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(BC + 15.0));
    }

    private void createProblem_09(){
        int AB = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int A = MathUtility.getRandomPositiveNumber_1Digit() + 70;
        int C = MathUtility.getRandomPositiveNumber_1Digit() + 20;

        double BC = (double) (AB) / Math.sin(C) * Math.sin(MathUtility.toRadian(A));
        BC = MathUtility.round_2_Decimal(BC);

        QUESTION = "Jame try to find the height of a tree. He found that the angle formed by the tree and the ground is " + String.valueOf(A) + InfoCollector.getDegreeSign()
                + ". The distance between the top of the tree and the top of the shadow is " + String.valueOf(AB) + " feet. "
                + ". The angle formed by the top of the tree to the top of the shadow and the ground " + String.valueOf(C) + InfoCollector.getDegreeSign()
                + ". How height is the tree?";
        RIGHT_ANSWER = String.valueOf(BC);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(BC + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(BC - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(BC + 15.0));
    }

    @Override
    public String getQuestion() {
        QUESTION = InfoCollector.getTriangleSign() + "ABC is a right triangle at A. " + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
