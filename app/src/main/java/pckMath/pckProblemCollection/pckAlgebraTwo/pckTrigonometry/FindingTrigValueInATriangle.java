package pckMath.pckProblemCollection.pckAlgebraTwo.pckTrigonometry;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 8/14/2017.
 */

public class FindingTrigValueInATriangle extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingTrigValueInATriangle(){
        createProblem();
        swapAnswer();
    }

    public FindingTrigValueInATriangle(String Question,
                        String RightAnswer,
                        String AnswerA,
                        String AnswerB,
                        String AnswerC,
                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 10;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            default:
                createProblem_01();
                break;
        }
        return new FindingTrigValueInATriangle(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  SIN
    private void createProblem_01(){
        int Opposite = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int Hypotenuse = Opposite + MathUtility.getRandomPositiveNumber_1Digit();

        double SinValue = (double) (Opposite) / (double) (Hypotenuse);
        SinValue = MathUtility.round_4_Decimal(SinValue);

        QUESTION = "Find the sine of B. Given the hypotenuse is " + String.valueOf(Hypotenuse)
                + ", and the opposite is " + String.valueOf(Opposite) + ".";
        RIGHT_ANSWER = String.valueOf(SinValue);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(SinValue + 0.05));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(SinValue - 0.05));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(SinValue + 1.05));
    }

    //  SIN
    private void createProblem_02(){
        int B = MathUtility.getRandomAngleLessThan90Degree();
        int Hypotenuse = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double Opposite = (double) Hypotenuse * Math.sin(MathUtility.toRadian((double) B));
        Opposite = MathUtility.round_2_Decimal(Opposite);

        QUESTION = "Given the hypotenuse is " + String.valueOf(Hypotenuse)
                + ", and " + InfoCollector.getAngleSign() + "B = " + String.valueOf(B) + InfoCollector.getDegreeSign()
                + ". Find the length of the opposite side of angle B.";
        RIGHT_ANSWER = String.valueOf(Opposite);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Opposite + 0.05));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Opposite - 0.05));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Opposite + 1.05));
    }

    //  SIN
    private void createProblem_03(){
        int B = MathUtility.getRandomAngleLessThan90Degree();
        int Opposite = MathUtility.getRandomPositiveNumber_1Digit();

        double Hypotenuse = (double) Opposite / Math.sin(MathUtility.toRadian((double) B));
        Hypotenuse = MathUtility.round_2_Decimal(Hypotenuse);

        QUESTION = InfoCollector.getAngleSign() + "B = " + String.valueOf(B) + InfoCollector.getDegreeSign()
                + "and the opposite is " + String.valueOf(Opposite)
                + ". Find the length of the hypotenuse.";
        RIGHT_ANSWER = String.valueOf(Hypotenuse);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Hypotenuse + 0.05));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Hypotenuse - 0.05));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Hypotenuse + 1.05));
    }

    //  COS
    private void createProblem_04(){
        int Adjacent = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int Hypotenuse = Adjacent + MathUtility.getRandomPositiveNumber_1Digit();

        double CosineValue = (double) (Adjacent) / (double) (Hypotenuse);
        CosineValue = MathUtility.round_4_Decimal(CosineValue);

        QUESTION = "Find the cosine of B. Given the hypotenuse is " + String.valueOf(Hypotenuse)
                + ", and the adjacent is " + String.valueOf(Adjacent) + ".";
        RIGHT_ANSWER = String.valueOf(CosineValue);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(CosineValue + 0.05));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(CosineValue - 0.05));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(CosineValue + 1.05));
    }

    //  COS
    private void createProblem_05(){
        int B = MathUtility.getRandomAngleLessThan90Degree();
        int Hypotenuse = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double Adjacent = (double) Hypotenuse * Math.cos(MathUtility.toRadian((double) B));
        Adjacent = MathUtility.round_2_Decimal(Adjacent);

        QUESTION = "Given the hypotenuse is " + String.valueOf(Hypotenuse)
                + ", and " + InfoCollector.getAngleSign() + "B = " + String.valueOf(B) + InfoCollector.getDegreeSign()
                + ". Find the length of the opposite side of angle B.";
        RIGHT_ANSWER = String.valueOf(Adjacent);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Adjacent + 0.05));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Adjacent - 0.05));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Adjacent + 1.05));
    }

    //  COS
    private void createProblem_06(){
        int B = MathUtility.getRandomAngleLessThan90Degree();
        int Adjacent = MathUtility.getRandomPositiveNumber_1Digit();

        double Hypotenuse = (double) Adjacent / Math.cos(MathUtility.toRadian((double) B));
        Hypotenuse = MathUtility.round_2_Decimal(Hypotenuse);

        QUESTION = InfoCollector.getAngleSign() + "B = " + String.valueOf(B) + InfoCollector.getDegreeSign()
                + "and the adjacent is " + String.valueOf(Adjacent)
                + ". Find the length of the hypotenuse.";
        RIGHT_ANSWER = String.valueOf(Hypotenuse);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Hypotenuse + 0.05));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Hypotenuse - 0.05));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Hypotenuse + 1.05));
    }

    //  TANGENT
    private void createProblem_07(){
        int Adjacent = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int Opposite = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double TangentValue = (double) (Opposite) / (double) (Adjacent);
        TangentValue = MathUtility.round_4_Decimal(TangentValue);

        QUESTION = "Find the tangent of B. Given the opposite is " + String.valueOf(Opposite)
                + ", and the adjacent is " + String.valueOf(Adjacent) + ".";
        RIGHT_ANSWER = String.valueOf(TangentValue);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(TangentValue + 0.05));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(TangentValue - 0.05));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(TangentValue + 1.05));
    }

    //  TANGENT
    private void createProblem_08(){
        int B = MathUtility.getRandomAngleLessThan90Degree();
        int Opposite = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double Adjacent = (double) Opposite / Math.tan(MathUtility.toRadian((double) B));
        Adjacent = MathUtility.round_2_Decimal(Adjacent);

        QUESTION = InfoCollector.getAngleSign() + "B = " + String.valueOf(B) + InfoCollector.getDegreeSign()
                + "and the opposite is " + String.valueOf(Opposite)
                + ". Find the length of the adjacent.";
        RIGHT_ANSWER = String.valueOf(Adjacent);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Adjacent + 0.05));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Adjacent - 0.05));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Adjacent + 1.05));
    }

    //  TANGENT
    private void createProblem_09(){
        int B = MathUtility.getRandomAngleLessThan90Degree();
        int Adjacent = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double Opposite = (double) Adjacent * Math.tan(MathUtility.toRadian((double) B));
        Opposite = MathUtility.round_2_Decimal(Opposite);

        QUESTION = InfoCollector.getAngleSign() + "B = " + String.valueOf(B) + InfoCollector.getDegreeSign()
                + "and the adjacent is " + String.valueOf(Adjacent)
                + ". Find the length of the opposite.";
        RIGHT_ANSWER = String.valueOf(Opposite);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Opposite + 0.05));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Opposite - 0.05));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Opposite + 1.05));
    }

    //  COTANGENT
    private void createProblem_10(){
        int Adjacent = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int Opposite = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double CotangentValue = (double) (Adjacent) / (double) (Opposite);
        CotangentValue = MathUtility.round_4_Decimal(CotangentValue);

        QUESTION = "Find the cotangent of B. Given the opposite is " + String.valueOf(Opposite)
                + ", and the adjacent is " + String.valueOf(Adjacent) + ".";
        RIGHT_ANSWER = String.valueOf(CotangentValue);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(CotangentValue + 0.05));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(CotangentValue - 0.05));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(CotangentValue + 1.05));
    }

    @Override
    public String getQuestion() {
        QUESTION = InfoCollector.getTriangleSign() + "ABC is a right triangle at A. " + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
