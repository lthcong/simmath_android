package pckMath.pckProblemCollection.pckAlgebraTwo.pckTrigonometry;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;
import pckMath.pckProblemCollection.pckIntermediateMath.pckRoot.Root;

/**
 * Created by Cong on 8/16/2017.
 */

public class TrigIdentities extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public TrigIdentities(){
        createProblem();
        swapAnswer();
    }

    public TrigIdentities(String Question,
                                    String RightAnswer,
                                    String AnswerA,
                                    String AnswerB,
                                    String AnswerC,
                                    String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 17;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            case 14:
                createProblem_15();
                break;
            case 15:
                createProblem_16();
                break;
            case 16:
                createProblem_17();
                break;
            default:
                createProblem_01();
                break;
        }
        return new TrigIdentities(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  SIN => COS
    private void createProblem_01(){
        Fraction Sine = FractionMath.getRandomProperFraction();
        double Cosine = FractionMath.subtractFraction(new Fraction(1, 1), FractionMath.multiplyFraction(Sine, Sine)).getValue();
        Cosine = MathUtility.round_4_Decimal(Math.sqrt(Cosine));

        QUESTION = "Given sin(" + InfoCollector.getThetaSign() + ") = " + Sine.toString() + ". Find cos(" + InfoCollector.getThetaSign() + ") (round 4 decimal places).";
        RIGHT_ANSWER = String.valueOf(Cosine);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Cosine + 0.5));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Cosine + 1.5));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Cosine - 0.5));
    }

    //  SIN => CSC
    private void createProblem_02(){
        Fraction Sine = FractionMath.getRandomProperFraction();
        Fraction Cosecant = FractionMath.getReciprocal(Sine);

        QUESTION = "Given sin(" + InfoCollector.getThetaSign() + ") = " + Sine.toString() + ". Find csc(" + InfoCollector.getThetaSign() + ").";
        RIGHT_ANSWER = Cosecant.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getRandomImproperFraction().toString();
        ANSWER_C = FractionMath.getRandomImproperFraction().toString();
        ANSWER_D = FractionMath.getRandomImproperFraction().toString();
    }

    //  COS => SIN
    private void createProblem_03(){
        Fraction Cosine = FractionMath.getRandomProperFraction();
        double Sine = FractionMath.subtractFraction(new Fraction(1, 1), FractionMath.multiplyFraction(Cosine, Cosine)).getValue();
        Sine = MathUtility.round_4_Decimal(Math.sqrt(Sine));

        QUESTION = "Given cos(" + InfoCollector.getThetaSign() + ") = " + Cosine.toString() + ". Find sin(" + InfoCollector.getThetaSign() + ") (round 4 decimal places).";
        RIGHT_ANSWER = String.valueOf(Sine);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Sine + 0.5));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Sine + 1.5));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Sine - 0.5));
    }

    //  COS => SEC
    private void createProblem_04(){
        Fraction Cosine = FractionMath.getRandomProperFraction();
        Fraction Secant = FractionMath.getReciprocal(Cosine);

        QUESTION = "Given cos(" + InfoCollector.getThetaSign() + ") = " + Cosine.toString() + ". Find sec(" + InfoCollector.getThetaSign() + ").";
        RIGHT_ANSWER = Secant.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getRandomImproperFraction().toString();
        ANSWER_C = FractionMath.getRandomImproperFraction().toString();
        ANSWER_D = FractionMath.getRandomImproperFraction().toString();
    }

    // TAN => SEC
    private void createProblem_05(){
        Fraction Tangent = FractionMath.getRandomFraction();
        double Secant = FractionMath.addFraction(FractionMath.multiplyFraction(Tangent, Tangent), new Fraction(1, 1)).getValue();
        Secant = MathUtility.round_4_Decimal(Math.sqrt(Secant));

        QUESTION = "Given tan(" + InfoCollector.getThetaSign() + ") = " + Tangent.toString() + ". Find sec(" + InfoCollector.getThetaSign() + ") (round 4 decimal places).";
        RIGHT_ANSWER = String.valueOf(Secant);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Secant + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Secant - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Secant + 1.5));
    }

    //  TAN => COTANGENT
    private void createProblem_06(){
        Fraction Tangent = FractionMath.getRandomFraction();
        Fraction Cotangent = FractionMath.getReciprocal(Tangent);

        QUESTION = "Given tan(" + InfoCollector.getThetaSign() + ") = " + Tangent.toString() + ". Find cot(" + InfoCollector.getThetaSign() + ").";
        RIGHT_ANSWER = Cotangent.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getRandomImproperFraction().toString();
        ANSWER_C = FractionMath.getRandomImproperFraction().toString();
        ANSWER_D = FractionMath.getRandomImproperFraction().toString();
    }

    // COT => CSC
    private void createProblem_07(){
        Fraction Cotangent = FractionMath.getRandomFraction();
        double Cosecant = FractionMath.addFraction(FractionMath.multiplyFraction(Cotangent, Cotangent), new Fraction(1, 1)).getValue();
        Cosecant = MathUtility.round_4_Decimal(Math.sqrt(Cosecant));

        QUESTION = "Given cot(" + InfoCollector.getThetaSign() + ") = " + Cotangent.toString() + ". Find csc(" + InfoCollector.getThetaSign() + ") (round 4 decimal places).";
        RIGHT_ANSWER = String.valueOf(Cosecant);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Cosecant + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Cosecant - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Cosecant + 1.5));
    }

    //  SIN(x) => SIN(2x)
    private void createProblem_08(){
        Fraction Sine = FractionMath.getRandomProperFraction();
        double Cosine = FractionMath.subtractFraction(new Fraction(1, 1), FractionMath.multiplyFraction(Sine, Sine)).getValue();
        double DoubleSine = 2.0 * Sine.getValue() * Cosine;
        DoubleSine = MathUtility.round_4_Decimal(DoubleSine);

        QUESTION = "Given sin(" + InfoCollector.getThetaSign() + ") = " + Sine.toString() + ". Find sin(2" + InfoCollector.getThetaSign() + ") (round 4 decimal places).";
        RIGHT_ANSWER = String.valueOf(DoubleSine);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(DoubleSine + 0.5));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(DoubleSine + 1.5));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Sine.getValue() * 2.0));
    }

    //  SIN(x) => COS(2x)
    private void createProblem_09(){
        Fraction Sine = FractionMath.getRandomProperFraction();
        double DoubleCosine = 1.0 - 2.0 * Sine.getValue() * Sine.getValue();
        DoubleCosine = MathUtility.round_4_Decimal(DoubleCosine);

        QUESTION = "Given sin(" + InfoCollector.getThetaSign() + ") = " + Sine.toString() + ". Find cos(2" + InfoCollector.getThetaSign() + ") (round 4 decimal places).";
        RIGHT_ANSWER = String.valueOf(DoubleCosine);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(DoubleCosine + 0.5));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(DoubleCosine + 1.5));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(DoubleCosine - 1.5));
    }

    //  COS(x) => COS(2x)
    private void createProblem_10(){
        Fraction Cosine = FractionMath.getRandomProperFraction();
        double DoubleCosine = 2.0 * Cosine.getValue() * Cosine.getValue() - 1.0;
        DoubleCosine = MathUtility.round_4_Decimal(DoubleCosine);

        QUESTION = "Given cos(" + InfoCollector.getThetaSign() + ") = " + Cosine.toString() + ". Find cos(2" + InfoCollector.getThetaSign() + ") (round 4 decimal places).";
        RIGHT_ANSWER = String.valueOf(DoubleCosine);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Cosine.getValue() * 2.0));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(DoubleCosine + 1.5));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(DoubleCosine - 1.5));
    }

    //  TAN(x) => TAN(2x)
    private void createProblem_11(){
        Fraction Tangent = FractionMath.getRandomProperFraction();
        double DoubleTangent = (2.0 * Tangent.getValue()) / (1.0 - Tangent.getValue() * Tangent.getValue());
        DoubleTangent = MathUtility.round_4_Decimal(DoubleTangent);

        QUESTION = "Given tan(" + InfoCollector.getThetaSign() + ") = " + Tangent.toString() + ". Find tan(2" + InfoCollector.getThetaSign() + ") (round 4 decimal places).";
        RIGHT_ANSWER = String.valueOf(DoubleTangent);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Tangent.getValue() * 2.0));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(DoubleTangent + 1.5));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(DoubleTangent - 1.5));
    }

    //  SIN(x + y)
    private void createProblem_12(){
        Fraction SinX = FractionMath.getRandomProperFraction();
        double CosX = 1.0 - SinX.getValue() * SinX.getValue();
        CosX = MathUtility.round_4_Decimal(Math.sqrt(CosX));

        Fraction SinY = FractionMath.getRandomProperFraction();
        double CosY = 1.0 - SinY.getValue() * SinY.getValue();
        CosY = MathUtility.round_4_Decimal(Math.sqrt(CosY));

        double Result = SinX.getValue() * CosY + CosX * SinY.getValue();
        Result = MathUtility.round_4_Decimal(Result);

        QUESTION = "Sin(x) = " + SinX.toString() + ", sin(y) = " + SinY.toString() + ". Find sin(x + y).";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Result + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Result - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Result + 2.0));
    }

    //  SIN(x - y)
    private void createProblem_13(){
        Fraction SinX = FractionMath.getRandomProperFraction();
        double CosX = 1.0 - SinX.getValue() * SinX.getValue();
        CosX = MathUtility.round_4_Decimal(Math.sqrt(CosX));

        Fraction SinY = FractionMath.getRandomProperFraction();
        double CosY = 1.0 - SinY.getValue() * SinY.getValue();
        CosY = MathUtility.round_4_Decimal(Math.sqrt(CosY));

        double Result = SinX.getValue() * CosY - CosX * SinY.getValue();
        Result = MathUtility.round_4_Decimal(Result);

        QUESTION = "Sin(x) = " + SinX.toString() + ", sin(y) = " + SinY.toString() + ". Find sin(x - y).";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Result + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Result - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Result + 2.0));
    }

    //  COS(x + y)
    private void createProblem_14(){
        Fraction SinX = FractionMath.getRandomProperFraction();
        double CosX = 1.0 - SinX.getValue() * SinX.getValue();
        CosX = MathUtility.round_4_Decimal(Math.sqrt(CosX));

        Fraction SinY = FractionMath.getRandomProperFraction();
        double CosY = 1.0 - SinY.getValue() * SinY.getValue();
        CosY = MathUtility.round_4_Decimal(Math.sqrt(CosY));

        double Result = CosX * CosY - SinX.getValue() * SinY.getValue();
        Result = MathUtility.round_4_Decimal(Result);

        QUESTION = "Sin(x) = " + SinX.toString() + ", sin(y) = " + SinY.toString() + ". Find cos(x + y).";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Result + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Result - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Result + 2.0));
    }

    //  COS(x - y)
    private void createProblem_15(){
        Fraction SinX = FractionMath.getRandomProperFraction();
        double CosX = 1.0 - SinX.getValue() * SinX.getValue();
        CosX = MathUtility.round_4_Decimal(Math.sqrt(CosX));

        Fraction SinY = FractionMath.getRandomProperFraction();
        double CosY = 1.0 - SinY.getValue() * SinY.getValue();
        CosY = MathUtility.round_4_Decimal(Math.sqrt(CosY));

        double Result = CosX * CosY + SinX.getValue() * SinY.getValue();
        Result = MathUtility.round_4_Decimal(Result);

        QUESTION = "Sin(x) = " + SinX.toString() + ", sin(y) = " + SinY.toString() + ". Find cos(x - y).";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Result + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Result - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Result + 2.0));
    }

    //  TAN(x + y)
    private void createProblem_16(){
        Fraction TangentX = FractionMath.getRandomFraction();
        Fraction TangentY = FractionMath.getRandomFraction();

        double Result = (TangentX.getValue() + TangentY.getValue()) / (1.0 - TangentX.getValue() * TangentY.getValue());

        QUESTION = "Tan(x) = " + TangentX.toString() + ", tan(y) = " + TangentY.toString() + ". Find tan(x + y).";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Result + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Result - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Result + 2.0));
    }

    //  TAN(x - y)
    private void createProblem_17(){
        Fraction TangentX = FractionMath.getRandomFraction();
        Fraction TangentY = FractionMath.getRandomFraction();

        double Result = (TangentX.getValue() - TangentY.getValue()) / (1.0 + TangentX.getValue() * TangentY.getValue());

        QUESTION = "Tan(x) = " + TangentX.toString() + ", tan(y) = " + TangentY.toString() + ". Find tan(x - y).";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(Result + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(Result - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(Result + 2.0));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
