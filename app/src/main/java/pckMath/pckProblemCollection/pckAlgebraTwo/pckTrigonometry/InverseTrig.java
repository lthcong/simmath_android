package pckMath.pckProblemCollection.pckAlgebraTwo.pckTrigonometry;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 8/16/2017.
 */

public class InverseTrig extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public InverseTrig(){
        createProblem();
        swapAnswer();
    }

    public InverseTrig(String Question,
                        String RightAnswer,
                        String AnswerA,
                        String AnswerB,
                        String AnswerC,
                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }
        return new InverseTrig(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  SIN
    private void createProblem_01(){
        int Angle = MathUtility.getRandomAngleLessThan90Degree();
        double SineValue = MathUtility.round_5_Decimal(Math.sin(MathUtility.toRadian(Angle)));

        QUESTION = "Given sin(" + InfoCollector.getThetaSign() + ") = " + String.valueOf(SineValue) + ". "
                + InfoCollector.getThetaSign() + " = ?";
        RIGHT_ANSWER = String.valueOf(Angle) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Angle + 10) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(Angle + 20) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(Angle - 10) + InfoCollector.getDegreeSign();
    }

    //  COS
    private void createProblem_02(){
        int Angle = MathUtility.getRandomAngleLessThan90Degree();
        double SineValue = MathUtility.round_5_Decimal(Math.cos(MathUtility.toRadian(Angle)));

        QUESTION = "Given cos(" + InfoCollector.getThetaSign() + ") = " + String.valueOf(SineValue) + ". "
                + InfoCollector.getThetaSign() + " = ?";
        RIGHT_ANSWER = String.valueOf(Angle) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Angle + 10) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(Angle + 20) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(Angle - 10) + InfoCollector.getDegreeSign();
    }

    //  TAN
    private void createProblem_03(){
        int Angle = MathUtility.getRandomAngleLessThan90Degree();
        double SineValue = MathUtility.round_5_Decimal(Math.tan(MathUtility.toRadian(Angle)));

        QUESTION = "Given tan(" + InfoCollector.getThetaSign() + ") = " + String.valueOf(SineValue) + ". "
                + InfoCollector.getThetaSign() + " = ?";
        RIGHT_ANSWER = String.valueOf(Angle) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Angle + 10) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(Angle + 20) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(Angle - 10) + InfoCollector.getDegreeSign();
    }

    //  SIN
    private void createProblem_04(){
        int Opposite = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int Hypotenuse = MathUtility.getRandomPositiveNumber_1Digit() + Opposite;

        double SineValue = (double) Opposite / (double) Hypotenuse;
        double Angle = Math.asin(SineValue);
        Angle = MathUtility.round_1_Decimal(MathUtility.toDegree(Angle));

        QUESTION = InfoCollector.getTriangleSign() + "ABC is a right triangle at A. BC = " + String.valueOf(Hypotenuse)
                + ", AB = " + String.valueOf(Opposite) + ". Find the measure of the angle C.";
        RIGHT_ANSWER = String.valueOf(Angle) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Angle + 10)) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Angle + 20)) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Angle - 10)) + InfoCollector.getDegreeSign();
    }

    //  COS
    private void createProblem_05(){
        int Adjacent = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int Hypotenuse = MathUtility.getRandomPositiveNumber_1Digit() + Adjacent;

        double CosineValue = (double) Adjacent / (double) Hypotenuse;
        double Angle = Math.asin(CosineValue);
        Angle = MathUtility.round_1_Decimal(MathUtility.toDegree(Angle));

        QUESTION = InfoCollector.getTriangleSign() + "ABC is a right triangle at A. BC = " + String.valueOf(Hypotenuse)
                + ", AC = " + String.valueOf(Adjacent) + ". Find the measure of the angle C.";
        RIGHT_ANSWER = String.valueOf(Angle) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Angle + 10)) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Angle + 20)) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Angle - 10)) + InfoCollector.getDegreeSign();
    }

    //  TAN
    private void createProblem_06(){
        int Adjacent = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int Opposite = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double TanValue = (double) Opposite / (double) Adjacent;
        double Angle = Math.atan(TanValue);
        Angle = MathUtility.round_1_Decimal(MathUtility.toDegree(Angle));

        QUESTION = InfoCollector.getTriangleSign() + "ABC is a right triangle at A. AB = " + String.valueOf(Opposite)
                + ", AC = " + String.valueOf(Adjacent) + ". Find the measure of the angle C.";
        RIGHT_ANSWER = String.valueOf(Angle) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Angle + 10)) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Angle + 20)) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Angle - 10)) + InfoCollector.getDegreeSign();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
