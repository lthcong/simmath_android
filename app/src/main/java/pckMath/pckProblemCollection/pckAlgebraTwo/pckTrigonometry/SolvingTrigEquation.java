package pckMath.pckProblemCollection.pckAlgebraTwo.pckTrigonometry;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 8/16/2017.
 */

public class SolvingTrigEquation extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SolvingTrigEquation(){
        createProblem();
        swapAnswer();
    }

    public SolvingTrigEquation(String Question,
                       String RightAnswer,
                       String AnswerA,
                       String AnswerB,
                       String AnswerC,
                       String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 30;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            case 14:
                createProblem_15();
                break;
            case 15:
                createProblem_16();
                break;
            case 16:
                createProblem_17();
                break;
            case 17:
                createProblem_18();
                break;
            case 18:
                createProblem_19();
                break;
            case 19:
                createProblem_20();
                break;
            case 20:
                createProblem_21();
                break;
            case 21:
                createProblem_22();
                break;
            case 22:
                createProblem_23();
                break;
            case 23:
                createProblem_24();
                break;
            case 24:
                createProblem_25();
                break;
            case 25:
                createProblem_26();
                break;
            case 26:
                createProblem_27();
                break;
            case 27:
                createProblem_28();
                break;
            case 28:
                createProblem_29();
                break;
            case 29:
                createProblem_30();
                break;
            default:
                createProblem_01();
                break;
        }
        return new SolvingTrigEquation(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  Sin(x) = a
    private void createProblem_01(){
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "sin(x)";
        String RightSide = TrigMath.getSinValue(Angle);

        TrigAngleFamily Result = new TrigAngleFamily(Angle, 2);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  a * Sin(x) = b * a
    private void createProblem_02(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = String.valueOf(Multiple) + "sin(x)";
        String RightSide = "(" + TrigMath.getSinValue(Angle) + " * " + String.valueOf(Multiple) + ")";

        TrigAngleFamily Result = new TrigAngleFamily(Angle, 2);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  Sin(x + y) = a
    private void createProblem_03(){
        TrigAngle Addition = TrigMath.getRandomTrigAngle();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "sin(x" + " + " + Addition.toString() + ")";
        String RightSide = TrigMath.getSinValue(Angle);

        TrigAngleFamily Result = new TrigAngleFamily(TrigMath.sub(Angle, Addition), 2);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  Sin(a * x) = b
    private void createProblem_04(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "sin(" + String.valueOf(Multiple) + "x)";
        String RightSide = TrigMath.getSinValue(Angle);

        TrigAngleFamily Result = TrigMath.div(new TrigAngleFamily(Angle, 2), Multiple);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  Sin(a * x + y) = b
    private void createProblem_05(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Addition = TrigMath.getRandomTrigAngle();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "sin(" + String.valueOf(Multiple) + "x + " + Addition.toString() + ")";
        String RightSide = TrigMath.getSinValue(Angle);

        TrigAngleFamily Result = TrigMath.div(new TrigAngleFamily(TrigMath.sub(Angle, Addition), 2), Multiple);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  Cos(x) = a
    private void createProblem_06(){
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "cos(x)";
        String RightSide = TrigMath.getCosValue(Angle);

        TrigAngleFamily Result = new TrigAngleFamily(Angle, 2);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  a * Cos(x) = b * a
    private void createProblem_07(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = String.valueOf(Multiple) + "cos(x)";
        String RightSide = "(" + TrigMath.getCosValue(Angle) + " * " + String.valueOf(Multiple) + ")";

        TrigAngleFamily Result = new TrigAngleFamily(Angle, 2);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  Cos(x + y) = a
    private void createProblem_08(){
        TrigAngle Addition = TrigMath.getRandomTrigAngle();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "cos(x" + " + " + Addition.toString() + ")";
        String RightSide = TrigMath.getCosValue(Angle);

        TrigAngleFamily Result = new TrigAngleFamily(TrigMath.sub(Angle, Addition), 2);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  Cos(a * x) = b
    private void createProblem_09(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "cos(" + String.valueOf(Multiple) + "x)";
        String RightSide = TrigMath.getCosValue(Angle);

        TrigAngleFamily Result = TrigMath.div(new TrigAngleFamily(Angle, 2), Multiple);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  Cos(a * x + y) = b
    private void createProblem_10(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Addition = TrigMath.getRandomTrigAngle();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "cos(" + String.valueOf(Multiple) + "x + " + Addition.toString() + ")";
        String RightSide = TrigMath.getCosValue(Angle);

        TrigAngleFamily Result = TrigMath.div(new TrigAngleFamily(TrigMath.sub(Angle, Addition), 2), Multiple);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  Tan(x) = a
    private void createProblem_11(){
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "tan(x)";
        String RightSide = TrigMath.getTanValue(Angle);

        TrigAngleFamily Result = new TrigAngleFamily(Angle, 1);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
    }

    //  a * Tan(x) = b * a
    private void createProblem_12(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = String.valueOf(Multiple) + "tan(x)";
        String RightSide = "(" + TrigMath.getTanValue(Angle) + " * " + String.valueOf(Multiple) + ")";

        TrigAngleFamily Result = new TrigAngleFamily(Angle, 1);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
    }

    //  Tan(x + y) = a
    private void createProblem_13(){
        TrigAngle Addition = TrigMath.getRandomTrigAngle();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "tan(x" + " + " + Addition.toString() + ")";
        String RightSide = TrigMath.getTanValue(Angle);

        TrigAngleFamily Result = new TrigAngleFamily(TrigMath.sub(Angle, Addition), 1);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
    }

    //  Tan(a * x) = b
    private void createProblem_14(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "tan(" + String.valueOf(Multiple) + "x)";
        String RightSide = TrigMath.getTanValue(Angle);

        TrigAngleFamily Result = TrigMath.div(new TrigAngleFamily(Angle, 1), Multiple);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
    }

    //  Tan(a * x + y) = b
    private void createProblem_15(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Addition = TrigMath.getRandomTrigAngle();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "tan(" + String.valueOf(Multiple) + "x + " + Addition.toString() + ")";
        String RightSide = TrigMath.getTanValue(Angle);

        TrigAngleFamily Result = TrigMath.div(new TrigAngleFamily(TrigMath.sub(Angle, Addition), 1), Multiple);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
    }

    //  Cot(x) = a
    private void createProblem_16(){
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "cot(x)";
        String RightSide = TrigMath.getCotValue(Angle);

        TrigAngleFamily Result = new TrigAngleFamily(Angle, 1);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
    }

    //  a * Cot(x) = b * a
    private void createProblem_17(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = String.valueOf(Multiple) + "cot(x)";
        String RightSide = "(" + TrigMath.getCotValue(Angle) + " * " + String.valueOf(Multiple) + ")";

        TrigAngleFamily Result = new TrigAngleFamily(Angle, 1);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
    }

    //  Cot(x + y) = a
    private void createProblem_18(){
        TrigAngle Addition = TrigMath.getRandomTrigAngle();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "cot(x" + " + " + Addition.toString() + ")";
        String RightSide = TrigMath.getCotValue(Angle);

        TrigAngleFamily Result = new TrigAngleFamily(TrigMath.sub(Angle, Addition), 1);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
    }

    //  Cot(a * x) = b
    private void createProblem_19(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "cot(" + String.valueOf(Multiple) + "x)";
        String RightSide = TrigMath.getCotValue(Angle);

        TrigAngleFamily Result = TrigMath.div(new TrigAngleFamily(Angle, 1), Multiple);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
    }

    //  Cot(a * x + y) = b
    private void createProblem_20(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Addition = TrigMath.getRandomTrigAngle();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "cot(" + String.valueOf(Multiple) + "x + " + Addition.toString() + ")";
        String RightSide = TrigMath.getCotValue(Angle);

        TrigAngleFamily Result = TrigMath.div(new TrigAngleFamily(TrigMath.sub(Angle, Addition), 1), Multiple);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 1)).toString();
    }

    //  Sec(x) = a
    private void createProblem_21(){
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "sec(x)";
        String RightSide = TrigMath.getSecValue(Angle);

        TrigAngleFamily Result = new TrigAngleFamily(Angle, 2);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  a * Sec(x) = b * a
    private void createProblem_22(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = String.valueOf(Multiple) + "sec(x)";
        String RightSide = "(" + TrigMath.getSecValue(Angle) + " * " + String.valueOf(Multiple) + ")";

        TrigAngleFamily Result = new TrigAngleFamily(Angle, 2);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  Sec(x + y) = a
    private void createProblem_23(){
        TrigAngle Addition = TrigMath.getRandomTrigAngle();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "sec(x" + " + " + Addition.toString() + ")";
        String RightSide = TrigMath.getSecValue(Angle);

        TrigAngleFamily Result = new TrigAngleFamily(TrigMath.sub(Angle, Addition), 2);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  Sec(a * x) = b
    private void createProblem_24(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "sec(" + String.valueOf(Multiple) + "x)";
        String RightSide = TrigMath.getSecValue(Angle);

        TrigAngleFamily Result = TrigMath.div(new TrigAngleFamily(Angle, 2), Multiple);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  Sec(a * x + y) = b
    private void createProblem_25(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Addition = TrigMath.getRandomTrigAngle();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "sec(" + String.valueOf(Multiple) + "x + " + Addition.toString() + ")";
        String RightSide = TrigMath.getSecValue(Angle);

        TrigAngleFamily Result = TrigMath.div(new TrigAngleFamily(TrigMath.sub(Angle, Addition), 2), Multiple);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  Csc(x) = a
    private void createProblem_26(){
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "csc(x)";
        String RightSide = TrigMath.getCscValue(Angle);

        TrigAngleFamily Result = new TrigAngleFamily(Angle, 2);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  a * Csc(x) = b * a
    private void createProblem_27(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = String.valueOf(Multiple) + "csc(x)";
        String RightSide = "(" + TrigMath.getCscValue(Angle) + " * " + String.valueOf(Multiple) + ")";

        TrigAngleFamily Result = new TrigAngleFamily(Angle, 2);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  Csc(x + y) = a
    private void createProblem_28(){
        TrigAngle Addition = TrigMath.getRandomTrigAngle();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "csc(x" + " + " + Addition.toString() + ")";
        String RightSide = TrigMath.getCscValue(Angle);

        TrigAngleFamily Result = new TrigAngleFamily(TrigMath.sub(Angle, Addition), 2);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  Csc(a * x) = b
    private void createProblem_29(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "csc(" + String.valueOf(Multiple) + "x)";
        String RightSide = TrigMath.getCscValue(Angle);

        TrigAngleFamily Result = TrigMath.div(new TrigAngleFamily(Angle, 2), Multiple);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }

    //  Csc(a * x + y) = b
    private void createProblem_30(){
        int Multiple = MathUtility.getRandomNumber_1Digit();
        TrigAngle Addition = TrigMath.getRandomTrigAngle();
        TrigAngle Angle = TrigMath.getRandomSpecialTrigAngle();
        String LeftSide = "csc(" + String.valueOf(Multiple) + "x + " + Addition.toString() + ")";
        String RightSide = TrigMath.getCscValue(Angle);

        TrigAngleFamily Result = TrigMath.div(new TrigAngleFamily(TrigMath.sub(Angle, Addition), 2), Multiple);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_C = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
        ANSWER_D = (new TrigAngleFamily(TrigMath.getRandomTrigAngle(), 2)).toString();
    }
    
    @Override
    public String getQuestion() {
        QUESTION = "Solve this equation:<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
