package pckMath.pckProblemCollection.pckAlgebraTwo.pckTrigonometry;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 8/14/2017.
 */

public class FindingValueUsingCosineLaw extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingValueUsingCosineLaw(){
        createProblem();
        swapAnswer();
    }

    public FindingValueUsingCosineLaw(String Question,
                                   String RightAnswer,
                                   String AnswerA,
                                   String AnswerB,
                                   String AnswerC,
                                   String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 3;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            default:
                createProblem_01();
                break;
        }
        return new FindingValueUsingCosineLaw(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  COSINE LAW: a^2 = b^2 + c^2 - 2bcCos(A)
    private void createProblem_01(){
        int A = MathUtility.getRandomAngleLessThan90Degree();

        int b = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int c = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double a = (double) (b * b) + (double) (c * c) - 2.0 * (double) (b * c) * Math.cos(MathUtility.toRadian(A));
        a = MathUtility.round_2_Decimal(Math.sqrt(a));

        QUESTION = InfoCollector.getAngleSign() + "A = " + String.valueOf(A) + InfoCollector.getDegreeSign()
                + ", b = " + String.valueOf(b) + ", c = " + String.valueOf(c) + ". Find the length of the side a.";
        RIGHT_ANSWER = String.valueOf(a);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(a + 0.05));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(a - 0.05));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(a + 1.05));
    }

    //  COSINE LAW: b^2 = a^2 + c^2 - 2acCos(B)
    private void createProblem_02(){
        int B = MathUtility.getRandomAngleLessThan90Degree();

        int a = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int c = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double b = (double) (a * a) + (double) (c * c) - 2.0 * (double) (a * c) * Math.cos(MathUtility.toRadian(B));
        b = MathUtility.round_2_Decimal(Math.sqrt(b));

        QUESTION = InfoCollector.getAngleSign() + "B = " + String.valueOf(B) + InfoCollector.getDegreeSign()
                + ", a = " + String.valueOf(a) + ", c = " + String.valueOf(c) + ". Find the length of the side b.";
        RIGHT_ANSWER = String.valueOf(b);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(b + 0.05));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(b - 0.05));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(b + 1.05));
    }

    //  COSINE LAW: Cos(B) = (b^2 - a^2 - c^2) / (-2ac)
    private void createProblem_03(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int c = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        if (a + b <= c){
            createProblem_03();
        }

        double CosineB = (double) (b * b - a * a - c * c) / (double) (-2 * a * c);
        CosineB = MathUtility.round_2_Decimal(CosineB);

        QUESTION = "a = " + String.valueOf(a) + ", b = " + String.valueOf(b) + ", c = " + String.valueOf(c)
                + ". Find the cosine value of B.";
        RIGHT_ANSWER = String.valueOf(CosineB);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(CosineB + 0.05));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(CosineB - 0.05));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(CosineB + 1.05));
    }

    @Override
    public String getQuestion() {
        QUESTION = InfoCollector.getTriangleSign() + "ABC is an acute triangle. a, b, c are the opposite sides of the angles A, B, and C. "
                + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
