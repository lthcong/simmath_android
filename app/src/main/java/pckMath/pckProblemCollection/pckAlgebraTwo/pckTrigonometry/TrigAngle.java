package pckMath.pckProblemCollection.pckAlgebraTwo.pckTrigonometry;

import pckInfo.InfoCollector;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;

/**
 * Created by Cong on 8/17/2017.
 */

public class TrigAngle {

    private Fraction COEFFICIENT;

    public TrigAngle(){
        COEFFICIENT = new Fraction();
    }

    public TrigAngle(int Coefficient){
        COEFFICIENT = new Fraction(Coefficient);
    }

    public TrigAngle(Fraction Coefficient){
        COEFFICIENT = Coefficient;
    }

    public void setCoefficient(int Coefficient){
        COEFFICIENT = new Fraction(Coefficient);
    }

    public void setCoefficient(Fraction Coefficient){
        COEFFICIENT = Coefficient;
    }

    public Fraction getCoefficient(){
        return COEFFICIENT;
    }

    public String toString(){
        String AngleString;
        if (COEFFICIENT.getValue() == 0.0){
            AngleString = "0";
        }
        else {
            AngleString = COEFFICIENT.toString() + InfoCollector.getPiSign();
        }

        return AngleString;
    }
}
