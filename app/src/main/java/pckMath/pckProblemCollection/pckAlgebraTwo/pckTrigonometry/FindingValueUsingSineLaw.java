package pckMath.pckProblemCollection.pckAlgebraTwo.pckTrigonometry;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 8/14/2017.
 */

public class FindingValueUsingSineLaw extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingValueUsingSineLaw(){
        createProblem();
        swapAnswer();
    }

    public FindingValueUsingSineLaw(String Question,
                                    String RightAnswer,
                                    String AnswerA,
                                    String AnswerB,
                                    String AnswerC,
                                    String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 4;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            default:
                createProblem_01();
                break;
        }
        return new FindingValueUsingSineLaw(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  SINE LAW: a/SIN(a) = b/SIN(b) => b
    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int A = MathUtility.getRandomAngleLessThan90Degree();
        int B = MathUtility.getRandomAngleLessThan90Degree();

        double b = (double) a / Math.sin(MathUtility.toRadian((double) A)) * Math.sin(MathUtility.toRadian((double) B));
        b = MathUtility.round_2_Decimal(b);

        QUESTION = InfoCollector.getAngleSign() + "A = " + String.valueOf(A) + InfoCollector.getDegreeSign() + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(B) + InfoCollector.getDegreeSign()
                + ", and the a side is " + String.valueOf(a) + ". Find the length of the b side.";
        RIGHT_ANSWER = String.valueOf(b);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(b + 0.05));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(b - 0.05));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(b + 1.05));
    }

    //  SINE LAW: a/SIN(a) = b/SIN(b) => SIN(b)
    private void createProblem_02(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int A = MathUtility.getRandomAngleLessThan90Degree();

        int b = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        double SinB = (double) b / ((double) a / Math.sin(MathUtility.toRadian((double) A)));
        SinB = MathUtility.round_2_Decimal(SinB);

        QUESTION = InfoCollector.getAngleSign() + "A = " + String.valueOf(A) + InfoCollector.getDegreeSign() + ", "
                + "the side a is " + String.valueOf(a) + ", and the side b is " + String.valueOf(b)
                + ". Find the sin value of the angle B.";
        RIGHT_ANSWER = String.valueOf(SinB);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(SinB + 0.05));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(SinB - 0.05));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(SinB + 1.05));
    }

    //  SINE LAW: c/SIN(C) = b/SIN(b) => b
    private void createProblem_03(){
        int c = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int C = MathUtility.getRandomAngleLessThan90Degree();
        int B = MathUtility.getRandomAngleLessThan90Degree();

        double b = (double) c / Math.sin(MathUtility.toRadian((double) C)) * Math.sin(MathUtility.toRadian((double) B));
        b = MathUtility.round_2_Decimal(b);

        QUESTION = InfoCollector.getAngleSign() + "C = " + String.valueOf(C) + InfoCollector.getDegreeSign() + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(B) + InfoCollector.getDegreeSign()
                + ", and the c side is " + String.valueOf(c) + ". Find the length of the b side.";
        RIGHT_ANSWER = String.valueOf(b);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(b + 0.05));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(b - 0.05));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(b + 1.05));
    }

    //  SINE LAW: a/SIN(a) = c/SIN(C) => SIN(C)
    private void createProblem_04(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int A = MathUtility.getRandomAngleLessThan90Degree();

        int c = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        double SinC = (double) c / ((double) a / Math.sin(MathUtility.toRadian((double) A)));
        SinC = MathUtility.round_2_Decimal(SinC);

        QUESTION = InfoCollector.getAngleSign() + "A = " + String.valueOf(A) + InfoCollector.getDegreeSign() + ", "
                + "the side a is " + String.valueOf(a) + ", and the side c is " + String.valueOf(c)
                + ". Find the sin value of the angle B.";
        RIGHT_ANSWER = String.valueOf(SinC);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(SinC + 0.05));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(SinC - 0.05));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(SinC + 1.05));
    }

    @Override
    public String getQuestion() {
        QUESTION = InfoCollector.getTriangleSign() + "ABC is an acute triangle. a, b, c are the opposite sides of the angles A, B, and C. "
                + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
