package pckMath.pckProblemCollection.pckAlgebraTwo.pckTrigonometry;

import pckInfo.InfoCollector;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;

/**
 * Created by Cong on 8/17/2017.
 */

public class TrigAngleFamily {

    private TrigAngle ANGLE;
    private Fraction FAMILY_COEFFICIENT;

    public TrigAngleFamily(){
        ANGLE = new TrigAngle();
        FAMILY_COEFFICIENT = new Fraction();
    }

    public TrigAngleFamily(TrigAngle Angle){
        ANGLE = Angle;
        FAMILY_COEFFICIENT = new Fraction();
    }

    public TrigAngleFamily(TrigAngle Angle, int Coefficient){
        ANGLE = Angle;
        FAMILY_COEFFICIENT = new Fraction(Coefficient);
    }

    public TrigAngleFamily(TrigAngle Angle, Fraction Coefficient){
        ANGLE = Angle;
        FAMILY_COEFFICIENT = Coefficient;
    }

    public void setAngle(TrigAngle Angle){
        ANGLE = Angle;
    }

    public TrigAngle getAngle(){
        return ANGLE;
    }

    public void setCoefficient(int Coefficient){
        FAMILY_COEFFICIENT = new Fraction(Coefficient);
    }

    public void setCoefficient(Fraction Coefficient){
        FAMILY_COEFFICIENT = Coefficient;
    }

    public Fraction getCoefficient(){
        return FAMILY_COEFFICIENT;
    }

    public String toString(){
        String FamilyString;
        if (FAMILY_COEFFICIENT.getValue() == 0.0){
            FamilyString = ANGLE.toString();
        }
        else {
            if (FAMILY_COEFFICIENT.getValue() == 1.0){
                FamilyString = ANGLE.toString() + " + " + InfoCollector.getPiSign() + "k";
            }
            else {
                FamilyString = ANGLE.toString() + " + " + String.valueOf(FAMILY_COEFFICIENT) + InfoCollector.getPiSign() + "k";
            }
        }

        return FamilyString;
    }
}
