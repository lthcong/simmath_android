package pckMath.pckProblemCollection.pckAlgebraTwo.pckTrigonometry;

import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 8/17/2017.
 */

public class TrigMath {

    public static TrigAngle getRandomTrigAngle(){
        return new TrigAngle(FractionMath.getRandomProperFraction());
    }

    public static TrigAngle getRandomSpecialTrigAngle(){
        int NoAngle = 17;
        TrigAngle RandomAngle;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoAngle){
            case 0:
                RandomAngle = new TrigAngle();
                break;
            case 1:
                RandomAngle = new TrigAngle(new Fraction(1, 6));
                break;
            case 2:
                RandomAngle = new TrigAngle(new Fraction(1, 4));
                break;
            case 3:
                RandomAngle = new TrigAngle(new Fraction(1, 3));
                break;
            case 4:
                RandomAngle = new TrigAngle(new Fraction(1, 2));
                break;
            case 5:
                RandomAngle = new TrigAngle(new Fraction(2, 3));
                break;
            case 6:
                RandomAngle = new TrigAngle(new Fraction(3, 4));
                break;
            case 7:
                RandomAngle = new TrigAngle(new Fraction(5, 6));
                break;
            case 8:
                RandomAngle = new TrigAngle(new Fraction(1, 1));
                break;
            case 9:
                RandomAngle = new TrigAngle(new Fraction(7, 6));
                break;
            case 10:
                RandomAngle = new TrigAngle(new Fraction(5, 4));
                break;
            case 11:
                RandomAngle = new TrigAngle(new Fraction(4, 3));
                break;
            case 12:
                RandomAngle = new TrigAngle(new Fraction(3, 2));
                break;
            case 13:
                RandomAngle = new TrigAngle(new Fraction(5, 3));
                break;
            case 14:
                RandomAngle = new TrigAngle(new Fraction(7, 4));
                break;
            case 15:
                RandomAngle = new TrigAngle(new Fraction(11, 6));
                break;
            case 16:
                RandomAngle = new TrigAngle(new Fraction(2, 1));
                break;
            default:
                RandomAngle = new TrigAngle();
                break;
        }

        return RandomAngle;
    }

    public static String getSinValue(TrigAngle Angle){
        String SinValue = "0";
        if (Angle.getCoefficient().getValue() == 0){
            SinValue = "0";
        }
        else {
            if (Angle.getCoefficient().getValue() == (1.0 / 6.0)){
                SinValue = new Fraction(1, 2).toString();
            }
            else {
                if (Angle.getCoefficient().getValue() == (1.0 / 4.0)){
                    SinValue = InfoCollector.getSup(InfoCollector.getSquareRootSign() + "(2)") + "/" + InfoCollector.getSub("2");
                }
                else {
                    if (Angle.getCoefficient().getValue() == (1.0 / 3.0)){
                        SinValue = InfoCollector.getSup(InfoCollector.getSquareRootSign() + "(3)") + "/" + InfoCollector.getSub("2");
                    }
                    else {
                        if (Angle.getCoefficient().getValue() == (1.0 / 2.0)){
                            SinValue = "1";
                        }
                        else {
                            if (Angle.getCoefficient().getValue() == (2.0 / 3.0)){
                                SinValue = InfoCollector.getSup(InfoCollector.getSquareRootSign() + "(3)") + "/" + InfoCollector.getSub("2");
                            }
                            else {
                                if (Angle.getCoefficient().getValue() == (3.0 / 4.0)){
                                    SinValue = InfoCollector.getSup(InfoCollector.getSquareRootSign() + "(2)") + "/" + InfoCollector.getSub("2");
                                }
                                else {
                                    if (Angle.getCoefficient().getValue() == (5.0 / 6.0)){
                                        SinValue = new Fraction(1, 2).toString();
                                    }
                                    else {
                                        if (Angle.getCoefficient().getValue() == 1.0){
                                            SinValue = "0";
                                        }
                                        else {
                                            if (Angle.getCoefficient().getValue() == (7.0 / 6.0)){
                                                SinValue = new Fraction(-1, 2).toString();
                                            }
                                            else {
                                                if (Angle.getCoefficient().getValue() == (5.0 / 4.0)){
                                                    SinValue = InfoCollector.getSup("-" + InfoCollector.getSquareRootSign() + "(2)") + "/" + InfoCollector.getSub("2");
                                                }
                                                else {
                                                    if (Angle.getCoefficient().getValue() == (4.0 / 3.0)){
                                                        SinValue = InfoCollector.getSup("-" + InfoCollector.getSquareRootSign() + "(3)") + "/" + InfoCollector.getSub("2");
                                                    }
                                                    else {
                                                        if (Angle.getCoefficient().getValue() == (3.0 / 2.0)){
                                                            SinValue = "-1";
                                                        }
                                                        else {
                                                            if (Angle.getCoefficient().getValue() == (5.0 / 3.0)){
                                                                SinValue = InfoCollector.getSup("-" + InfoCollector.getSquareRootSign() + "(3)") + "/" + InfoCollector.getSub("2");
                                                            }
                                                            else {
                                                                if (Angle.getCoefficient().getValue() == (7.0 / 4.0)){
                                                                    SinValue = InfoCollector.getSup("-" + InfoCollector.getSquareRootSign() + "(2)") + "/" + InfoCollector.getSub("2");
                                                                }
                                                                else {
                                                                    if (Angle.getCoefficient().getValue() == (11.0 / 6.0)){
                                                                        SinValue = new Fraction(-1, 2).toString();
                                                                    }
                                                                    else {
                                                                        if (Angle.getCoefficient().getValue() == 2.0){
                                                                            SinValue = "0";
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return SinValue;
    }

    public static String getSinValue(TrigAngleFamily AngleFamily){
        return getSinValue(AngleFamily.getAngle());
    }

    public static String getCosValue(TrigAngle Angle){
        String CosValue = "1";
        if (Angle.getCoefficient().getValue() == 0){
            CosValue = "1";
        }
        else {
            if (Angle.getCoefficient().getValue() == (1.0 / 6.0)){
                CosValue = InfoCollector.getSup(InfoCollector.getSquareRootSign() + "(3)") + "/" + InfoCollector.getSub("2");
            }
            else {
                if (Angle.getCoefficient().getValue() == (1.0 / 4.0)){
                    CosValue = InfoCollector.getSup(InfoCollector.getSquareRootSign() + "(2)") + "/" + InfoCollector.getSub("2");
                }
                else {
                    if (Angle.getCoefficient().getValue() == (1.0 / 3.0)){
                        CosValue = new Fraction(1, 2).toString();
                    }
                    else {
                        if (Angle.getCoefficient().getValue() == (1.0 / 2.0)){
                            CosValue = "0";
                        }
                        else {
                            if (Angle.getCoefficient().getValue() == (2.0 / 3.0)){
                                CosValue = new Fraction(-1, 2).toString();
                            }
                            else {
                                if (Angle.getCoefficient().getValue() == (3.0 / 4.0)){
                                    CosValue = InfoCollector.getSup("-" + InfoCollector.getSquareRootSign() + "(2)") + "/" + InfoCollector.getSub("2");
                                }
                                else {
                                    if (Angle.getCoefficient().getValue() == (5.0 / 6.0)){
                                        CosValue = InfoCollector.getSup("-" + InfoCollector.getSquareRootSign() + "(3)") + "/" + InfoCollector.getSub("2");
                                    }
                                    else {
                                        if (Angle.getCoefficient().getValue() == 1.0){
                                            CosValue = "-1";
                                        }
                                        else {
                                            if (Angle.getCoefficient().getValue() == (7.0 / 6.0)){
                                                CosValue = InfoCollector.getSup("-" + InfoCollector.getSquareRootSign() + "(3)") + "/" + InfoCollector.getSub("2");
                                            }
                                            else {
                                                if (Angle.getCoefficient().getValue() == (5.0 / 4.0)){
                                                    CosValue = InfoCollector.getSup("-" + InfoCollector.getSquareRootSign() + "(2)") + "/" + InfoCollector.getSub("2");
                                                }
                                                else {
                                                    if (Angle.getCoefficient().getValue() == (4.0 / 3.0)){
                                                        CosValue = new Fraction(-1, 2).toString();
                                                    }
                                                    else {
                                                        if (Angle.getCoefficient().getValue() == (3.0 / 2.0)){
                                                            CosValue = "0";
                                                        }
                                                        else {
                                                            if (Angle.getCoefficient().getValue() == (5.0 / 3.0)){
                                                                CosValue = new Fraction(1, 2).toString();
                                                            }
                                                            else {
                                                                if (Angle.getCoefficient().getValue() == (7.0 / 4.0)){
                                                                    CosValue = InfoCollector.getSup(InfoCollector.getSquareRootSign() + "(2)") + "/" + InfoCollector.getSub("2");
                                                                }
                                                                else {
                                                                    if (Angle.getCoefficient().getValue() == (11.0 / 6.0)){
                                                                        CosValue = InfoCollector.getSup(InfoCollector.getSquareRootSign() + "(3)") + "/" + InfoCollector.getSub("2");
                                                                    }
                                                                    else {
                                                                        if (Angle.getCoefficient().getValue() == 2.0){
                                                                            CosValue = "1";
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return CosValue;
    }

    public static String getCosValue(TrigAngleFamily AngleFamily){
        return getCosValue(AngleFamily.getAngle());
    }

    public static String getTanValue(TrigAngle Angle){
        String TanValue = "0";
        if (Angle.getCoefficient().getValue() == 0){
            TanValue = "0";
        }
        else {
            if (Angle.getCoefficient().getValue() == (1.0 / 6.0)){
                TanValue = InfoCollector.getSup(InfoCollector.getSquareRootSign() + "(3)") + "/" + InfoCollector.getSub("3");
            }
            else {
                if (Angle.getCoefficient().getValue() == (1.0 / 4.0)){
                    TanValue = "1";
                }
                else {
                    if (Angle.getCoefficient().getValue() == (1.0 / 3.0)){
                        TanValue = InfoCollector.getSquareRootSign() + "(3)";
                    }
                    else {
                        if (Angle.getCoefficient().getValue() == (1.0 / 2.0)){
                            TanValue = InfoCollector.getInfinity();
                        }
                        else {
                            if (Angle.getCoefficient().getValue() == (2.0 / 3.0)){
                                TanValue = "-" + InfoCollector.getSquareRootSign() + "(3)";
                            }
                            else {
                                if (Angle.getCoefficient().getValue() == (3.0 / 4.0)){
                                    TanValue = "-1";
                                }
                                else {
                                    if (Angle.getCoefficient().getValue() == (5.0 / 6.0)){
                                        TanValue = InfoCollector.getSup("-" + InfoCollector.getSquareRootSign() + "(3)") + "/" + InfoCollector.getSub("3");
                                    }
                                    else {
                                        if (Angle.getCoefficient().getValue() == 1.0){
                                            TanValue = "0";
                                        }
                                        else {
                                            if (Angle.getCoefficient().getValue() == (7.0 / 6.0)){
                                                TanValue = InfoCollector.getSup(InfoCollector.getSquareRootSign() + "(3)") + "/" + InfoCollector.getSub("3");
                                            }
                                            else {
                                                if (Angle.getCoefficient().getValue() == (5.0 / 4.0)){
                                                    TanValue = "1";
                                                }
                                                else {
                                                    if (Angle.getCoefficient().getValue() == (4.0 / 3.0)){
                                                        TanValue = InfoCollector.getSquareRootSign() + "(3)";
                                                    }
                                                    else {
                                                        if (Angle.getCoefficient().getValue() == (3.0 / 2.0)){
                                                            TanValue = InfoCollector.getInfinity();
                                                        }
                                                        else {
                                                            if (Angle.getCoefficient().getValue() == (5.0 / 3.0)){
                                                                TanValue = "-" + InfoCollector.getSquareRootSign() + "(3)";
                                                            }
                                                            else {
                                                                if (Angle.getCoefficient().getValue() == (7.0 / 4.0)){
                                                                    TanValue = "-1";
                                                                }
                                                                else {
                                                                    if (Angle.getCoefficient().getValue() == (11.0 / 6.0)){
                                                                        TanValue = "-" + InfoCollector.getSup(InfoCollector.getSquareRootSign() + "(3)") + "/" + InfoCollector.getSub("3");
                                                                    }
                                                                    else {
                                                                        if (Angle.getCoefficient().getValue() == 2.0){
                                                                            TanValue = "0";
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return TanValue;
    }

    public static String getTanValue(TrigAngleFamily AngleFamily){
        return getTanValue(AngleFamily.getAngle());
    }

    public static String getCotValue(TrigAngle Angle){
        String CotValue = "0";
        if (Angle.getCoefficient().getValue() == 0){
            CotValue = InfoCollector.getInfinity();
        }
        else {
            if (Angle.getCoefficient().getValue() == (1.0 / 6.0)){
                CotValue = InfoCollector.getSquareRootSign() + "(3)";
            }
            else {
                if (Angle.getCoefficient().getValue() == (1.0 / 4.0)){
                    CotValue = "1";
                }
                else {
                    if (Angle.getCoefficient().getValue() == (1.0 / 3.0)){
                        CotValue = InfoCollector.getSup(InfoCollector.getSquareRootSign() + "(3)") + "/" + InfoCollector.getSub("3");
                    }
                    else {
                        if (Angle.getCoefficient().getValue() == (1.0 / 2.0)){
                            CotValue = "0";
                        }
                        else {
                            if (Angle.getCoefficient().getValue() == (2.0 / 3.0)){
                                CotValue = "-" + InfoCollector.getSup(InfoCollector.getSquareRootSign() + "(3)") + "/" + InfoCollector.getSub("3");
                            }
                            else {
                                if (Angle.getCoefficient().getValue() == (3.0 / 4.0)){
                                    CotValue = "-1";
                                }
                                else {
                                    if (Angle.getCoefficient().getValue() == (5.0 / 6.0)){
                                        CotValue = "-" + InfoCollector.getSquareRootSign() + "(3)";
                                    }
                                    else {
                                        if (Angle.getCoefficient().getValue() == 1.0){
                                            CotValue = InfoCollector.getInfinity();
                                        }
                                        else {
                                            if (Angle.getCoefficient().getValue() == (7.0 / 6.0)){
                                                CotValue = InfoCollector.getSquareRootSign() + "(3)";
                                            }
                                            else {
                                                if (Angle.getCoefficient().getValue() == (5.0 / 4.0)){
                                                    CotValue = "1";
                                                }
                                                else {
                                                    if (Angle.getCoefficient().getValue() == (4.0 / 3.0)){
                                                        CotValue = InfoCollector.getSup(InfoCollector.getSquareRootSign() + "(3)") + "/" + InfoCollector.getSub("3");
                                                    }
                                                    else {
                                                        if (Angle.getCoefficient().getValue() == (3.0 / 2.0)){
                                                            CotValue = "0";
                                                        }
                                                        else {
                                                            if (Angle.getCoefficient().getValue() == (5.0 / 3.0)){
                                                                CotValue = "-" + InfoCollector.getSup(InfoCollector.getSquareRootSign() + "(3)") + "/" + InfoCollector.getSub("3");
                                                            }
                                                            else {
                                                                if (Angle.getCoefficient().getValue() == (7.0 / 4.0)){
                                                                    CotValue = "-1";
                                                                }
                                                                else {
                                                                    if (Angle.getCoefficient().getValue() == (11.0 / 6.0)){
                                                                        CotValue = "-" + InfoCollector.getSquareRootSign() + "(3)";
                                                                    }
                                                                    else {
                                                                        if (Angle.getCoefficient().getValue() == 2.0){
                                                                            CotValue = InfoCollector.getInfinity();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return CotValue;
    }

    public static String getCotValue(TrigAngleFamily AngleFamily){
        return getCotValue(AngleFamily.getAngle());
    }

    public static String getSecValue(TrigAngle Angle){
        String SecValue = "1";
        if (Angle.getCoefficient().getValue() == 0){
            SecValue = "1";
        }
        else {
            if (Angle.getCoefficient().getValue() == (1.0 / 6.0)){
                SecValue = InfoCollector.getSup("2") + "/" + InfoCollector.getSub(InfoCollector.getSquareRootSign() + "(3)");
            }
            else {
                if (Angle.getCoefficient().getValue() == (1.0 / 4.0)){
                    SecValue = InfoCollector.getSquareRootSign() + "(2)";
                }
                else {
                    if (Angle.getCoefficient().getValue() == (1.0 / 3.0)){
                        SecValue = "2";
                    }
                    else {
                        if (Angle.getCoefficient().getValue() == (1.0 / 2.0)){
                            SecValue = InfoCollector.getInfinity();
                        }
                        else {
                            if (Angle.getCoefficient().getValue() == (2.0 / 3.0)){
                                SecValue = "-2";
                            }
                            else {
                                if (Angle.getCoefficient().getValue() == (3.0 / 4.0)){
                                    SecValue = "-" + InfoCollector.getSquareRootSign() + "(2)";
                                }
                                else {
                                    if (Angle.getCoefficient().getValue() == (5.0 / 6.0)){
                                        SecValue = InfoCollector.getSup("-2") + "/" + InfoCollector.getSub(InfoCollector.getSquareRootSign() + "(3)");
                                    }
                                    else {
                                        if (Angle.getCoefficient().getValue() == 1.0){
                                            SecValue = "-1";
                                        }
                                        else {
                                            if (Angle.getCoefficient().getValue() == (7.0 / 6.0)){
                                                SecValue = InfoCollector.getSup("-2") + "/" + InfoCollector.getSub(InfoCollector.getSquareRootSign() + "(3)");
                                            }
                                            else {
                                                if (Angle.getCoefficient().getValue() == (5.0 / 4.0)){
                                                    SecValue = "-" + InfoCollector.getSquareRootSign() + "(2)";
                                                }
                                                else {
                                                    if (Angle.getCoefficient().getValue() == (4.0 / 3.0)){
                                                        SecValue = "-2";
                                                    }
                                                    else {
                                                        if (Angle.getCoefficient().getValue() == (3.0 / 2.0)){
                                                            SecValue = InfoCollector.getInfinity();
                                                        }
                                                        else {
                                                            if (Angle.getCoefficient().getValue() == (5.0 / 3.0)){
                                                                SecValue = "2";
                                                            }
                                                            else {
                                                                if (Angle.getCoefficient().getValue() == (7.0 / 4.0)){
                                                                    SecValue = InfoCollector.getSquareRootSign() + "(2)";
                                                                }
                                                                else {
                                                                    if (Angle.getCoefficient().getValue() == (11.0 / 6.0)){
                                                                        SecValue = InfoCollector.getSup("2") + "/" + InfoCollector.getSub(InfoCollector.getSquareRootSign() + "(3)");
                                                                    }
                                                                    else {
                                                                        if (Angle.getCoefficient().getValue() == 2.0){
                                                                            SecValue = "1";
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return SecValue;
    }

    public static String getSecValue(TrigAngleFamily AngleFamily){
        return getSecValue(AngleFamily.getAngle());
    }

    public static String getCscValue(TrigAngle Angle){
        String CscValue = "1";
        if (Angle.getCoefficient().getValue() == 0){
            CscValue = InfoCollector.getInfinity();
        }
        else {
            if (Angle.getCoefficient().getValue() == (1.0 / 6.0)){
                CscValue = "2";
            }
            else {
                if (Angle.getCoefficient().getValue() == (1.0 / 4.0)){
                    CscValue = InfoCollector.getSquareRootSign() + "(2)";
                }
                else {
                    if (Angle.getCoefficient().getValue() == (1.0 / 3.0)){
                        CscValue = InfoCollector.getSup("2") + "/" + InfoCollector.getSub(InfoCollector.getSquareRootSign() + "(3)");
                    }
                    else {
                        if (Angle.getCoefficient().getValue() == (1.0 / 2.0)){
                            CscValue = "1";
                        }
                        else {
                            if (Angle.getCoefficient().getValue() == (2.0 / 3.0)){
                                CscValue = InfoCollector.getSup("2") + "/" + InfoCollector.getSub(InfoCollector.getSquareRootSign() + "(3)");
                            }
                            else {
                                if (Angle.getCoefficient().getValue() == (3.0 / 4.0)){
                                    CscValue = InfoCollector.getSquareRootSign() + "(2)";
                                }
                                else {
                                    if (Angle.getCoefficient().getValue() == (5.0 / 6.0)){
                                        CscValue = "2";
                                    }
                                    else {
                                        if (Angle.getCoefficient().getValue() == 1.0){
                                            CscValue = InfoCollector.getInfinity();
                                        }
                                        else {
                                            if (Angle.getCoefficient().getValue() == (7.0 / 6.0)){
                                                CscValue = "-2";
                                            }
                                            else {
                                                if (Angle.getCoefficient().getValue() == (5.0 / 4.0)){
                                                    CscValue = "-" + InfoCollector.getSquareRootSign() + "(2)";
                                                }
                                                else {
                                                    if (Angle.getCoefficient().getValue() == (4.0 / 3.0)){
                                                        CscValue = InfoCollector.getSup("-2") + "/" + InfoCollector.getSub(InfoCollector.getSquareRootSign() + "(3)");
                                                    }
                                                    else {
                                                        if (Angle.getCoefficient().getValue() == (3.0 / 2.0)){
                                                            CscValue = "-1";
                                                        }
                                                        else {
                                                            if (Angle.getCoefficient().getValue() == (5.0 / 3.0)){
                                                                CscValue = InfoCollector.getSup("-2") + "/" + InfoCollector.getSub(InfoCollector.getSquareRootSign() + "(3)");
                                                            }
                                                            else {
                                                                if (Angle.getCoefficient().getValue() == (7.0 / 4.0)){
                                                                    CscValue = "-" + InfoCollector.getSquareRootSign() + "(2)";
                                                                }
                                                                else {
                                                                    if (Angle.getCoefficient().getValue() == (11.0 / 6.0)){
                                                                        CscValue = "-2";
                                                                    }
                                                                    else {
                                                                        if (Angle.getCoefficient().getValue() == 2.0){
                                                                            CscValue = InfoCollector.getInfinity();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return CscValue;
    }

    public static String getCscValue(TrigAngleFamily AngleFamily){
        return getCscValue(AngleFamily.getAngle());
    }
    
    public static TrigAngle add(TrigAngle AngleA, TrigAngle AngleB){
        return new TrigAngle(FractionMath.addFraction(AngleA.getCoefficient(), AngleB.getCoefficient()));
    }

    public static TrigAngle sub(TrigAngle AngleA, TrigAngle AngleB){
        return new TrigAngle(FractionMath.subtractFraction(AngleA.getCoefficient(), AngleB.getCoefficient()));
    }

    public static TrigAngle mul(TrigAngle Angle, int Multiple){
        return new TrigAngle(FractionMath.multiplyFraction(Angle.getCoefficient(), new Fraction(Multiple)));
    }

    public static TrigAngle mul(TrigAngle Angle, Fraction Multiple){
        return new TrigAngle(FractionMath.multiplyFraction(Angle.getCoefficient(), Multiple));
    }

    public static TrigAngleFamily mul(TrigAngleFamily AngleFamily, int Multiple){
        return new TrigAngleFamily(mul(AngleFamily.getAngle(), Multiple),
                FractionMath.multiplyFraction(AngleFamily.getCoefficient(), new Fraction(Multiple)));
    }

    public static TrigAngleFamily mul(TrigAngleFamily AngleFamily, Fraction Multiple){
        return new TrigAngleFamily(mul(AngleFamily.getAngle(), Multiple),
                FractionMath.multiplyFraction(AngleFamily.getCoefficient(), Multiple));
    }

    public static TrigAngle div(TrigAngle Angle, int Multiple){
        return new TrigAngle(FractionMath.divideFraction(Angle.getCoefficient(), new Fraction(Multiple)));
    }

    public static TrigAngle div(TrigAngle Angle, Fraction Multiple){
        return new TrigAngle(FractionMath.divideFraction(Angle.getCoefficient(), Multiple));
    }

    public static TrigAngleFamily div(TrigAngleFamily AngleFamily, int Multiple){
        return new TrigAngleFamily(div(AngleFamily.getAngle(), Multiple),
                FractionMath.divideFraction(AngleFamily.getCoefficient(), new Fraction(Multiple)));
    }

    public static TrigAngleFamily div(TrigAngleFamily AngleFamily, Fraction Multiple){
        return new TrigAngleFamily(div(AngleFamily.getAngle(), Multiple),
                FractionMath.divideFraction(AngleFamily.getCoefficient(), Multiple));
    }

}
