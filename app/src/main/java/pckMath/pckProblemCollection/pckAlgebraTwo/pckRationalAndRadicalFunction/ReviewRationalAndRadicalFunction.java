package pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction.pckRadical.pckEquation.SolvingRadicalEquation;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction.pckRadical.pckFunction.FindingDomainAndRangeOfRadicalFunction;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction.pckRational.pckEquation.SolvingRationalEquation;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction.pckRational.pckFunction.FindingRangeDomainAsymptoteOfRationalFunction;

/**
 * Created by Cong on 9/6/2017.
 */

public class ReviewRationalAndRadicalFunction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public ReviewRationalAndRadicalFunction(){
        createProblem();
        swapAnswer();
    }

    public ReviewRationalAndRadicalFunction(String Question,
                                           String RightAnswer,
                                           String AnswerA,
                                           String AnswerB,
                                           String AnswerC,
                                           String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        MathProblem Problem;
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                Problem = new FindingRangeDomainAsymptoteOfRationalFunction();
                break;
            case 1:
                Problem = new SolvingRationalEquation();
                break;
            case 2:
                Problem = new FindingDomainAndRangeOfRadicalFunction();
                break;
            case 3:
                Problem = new SolvingRadicalEquation();
                break;
            case 4:
                Problem = new SolvingRadicalEquation();
                break;
            case 5:
                Problem = new SolvingRationalEquation();
                break;
            default:
                Problem = new SolvingRationalEquation();
                break;
        }

        QUESTION = Problem.getQuestion();
        RIGHT_ANSWER = Problem.getRightAnswer();
        ANSWER_A = Problem.getAnswerA();
        ANSWER_B = Problem.getAnswerB();
        ANSWER_C = Problem.getAnswerC();
        ANSWER_D = Problem.getAnswerD();

        return new ReviewRationalAndRadicalFunction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
