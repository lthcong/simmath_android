package pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction.pckRadical.pckFunction;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicExpression;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.Variable;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.Interval;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.SetMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.LinearEquation;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.LinearInequality;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRadicalPolynomial.RadicalPolynomial;

/**
 * Created by Cong on 8/9/2017.
 */

public class FindingDomainAndRangeOfRadicalFunction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingDomainAndRangeOfRadicalFunction(){
        createProblem();
        swapAnswer();
    }

    public FindingDomainAndRangeOfRadicalFunction(String Question,
                                                         String RightAnswer,
                                                         String AnswerA,
                                                         String AnswerB,
                                                         String AnswerC,
                                                         String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 12;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            default:
                createProblem_01();
                break;
        }
        return new FindingDomainAndRangeOfRadicalFunction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  DOMAIN   sqrt(ax + b)
    private void createProblem_01(){
        Polynomial Coefficient = new Polynomial();
        Polynomial Expression = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        RadicalPolynomial FunctionExpression = new RadicalPolynomial();
        FunctionExpression.setCoefficient(Coefficient);
        FunctionExpression.setRootNumber(2);
        FunctionExpression.setExpression(Expression);

        RadicalFunction Function = new RadicalFunction(FunctionExpression);

        QUESTION = "Find the domain of the function: <br>" + Function.toString();
        RIGHT_ANSWER = (new LinearInequality(InfoCollector.getGreaterThanOrEqualSign(), Expression)).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new LinearInequality(InfoCollector.getLessThanOrEqualSign(), Expression)).getSolution();
        ANSWER_C = (new LinearInequality("<", Expression)).getSolution();
        ANSWER_D = (new LinearInequality(">", Expression)).getSolution();
    }

    //  DOMAIN   sqrt(ax + b)
    private void createProblem_02(){
        Polynomial Coefficient = new Polynomial();
        Polynomial Expression = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        RadicalPolynomial FunctionExpression = new RadicalPolynomial();
        FunctionExpression.setCoefficient(Coefficient);
        FunctionExpression.setRootNumber(2);
        FunctionExpression.setExpression(Expression);

        RadicalFunction Function = new RadicalFunction(FunctionExpression);

        QUESTION = "Find the domain of the function: <br>" + Function.toString();
        RIGHT_ANSWER = SetMath.convertInequalityToInterval((new LinearInequality(InfoCollector.getGreaterThanOrEqualSign(), Expression)).getInequalitySolution()).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_C = (new Interval(false, Integer.MIN_VALUE, Integer.MAX_VALUE, false)).toString();
        ANSWER_D = (new Interval(false, Integer.MIN_VALUE, (new LinearEquation(Expression)).getSolutionNumber(), true)).toString();
    }

    //  DOMAIN   sqrt(ax^2 + bx + c)
    private void createProblem_03(){
        Polynomial Coefficient = new Polynomial();
        Polynomial Expression = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Expression = AlgebraMath.mul(Expression, Expression);

        RadicalPolynomial FunctionExpression = new RadicalPolynomial();
        FunctionExpression.setCoefficient(Coefficient);
        FunctionExpression.setRootNumber(2);
        FunctionExpression.setExpression(Expression);

        RadicalFunction Function = new RadicalFunction(FunctionExpression);

        QUESTION = "Find the domain of the function: <br>" + Function.toString();
        RIGHT_ANSWER = (new Interval(false, Integer.MIN_VALUE, Integer.MAX_VALUE, false)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_C = (new Interval(true, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_D = (new Interval(false, Integer.MIN_VALUE, (new LinearEquation(Expression)).getSolutionNumber(), true)).toString();
    }

    //  DOMAIN   sqrt(ax + b)^2
    private void createProblem_04(){
        Polynomial Coefficient = new Polynomial();
        Polynomial Expression = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        RadicalPolynomial FunctionExpression = new RadicalPolynomial();
        FunctionExpression.setCoefficient(Coefficient);
        FunctionExpression.setRootNumber(2);
        FunctionExpression.setExpression(Expression);

        RadicalFunction Function = new RadicalFunction(FunctionExpression);

        QUESTION = "Find the domain of the function: <br>" + Function.toString() + InfoCollector.putExponent(2);
        RIGHT_ANSWER = (new Interval(false, Integer.MIN_VALUE, Integer.MAX_VALUE, false)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_C = (new Interval(true, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_D = (new Interval(false, Integer.MIN_VALUE, (new LinearEquation(Expression)).getSolutionNumber(), true)).toString();
    }

    //  DOMAIN   cbrt(ax + b)
    private void createProblem_05(){
        Polynomial Coefficient = new Polynomial();
        Polynomial Expression = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        RadicalPolynomial FunctionExpression = new RadicalPolynomial();
        FunctionExpression.setCoefficient(Coefficient);
        FunctionExpression.setRootNumber(3);
        FunctionExpression.setExpression(Expression);

        RadicalFunction Function = new RadicalFunction(FunctionExpression);

        QUESTION = "Find the domain of the function: <br>" + Function.toString();
        RIGHT_ANSWER = (new Interval(false, Integer.MIN_VALUE, Integer.MAX_VALUE, false)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_C = (new Interval(true, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_D = (new Interval(false, Integer.MIN_VALUE, (new LinearEquation(Expression)).getSolutionNumber(), true)).toString();
    }

    //  RANGE   sqrt(ax + b)
    private void createProblem_06(){
        Polynomial Coefficient = new Polynomial();
        Polynomial Expression = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        RadicalPolynomial FunctionExpression = new RadicalPolynomial();
        FunctionExpression.setCoefficient(Coefficient);
        FunctionExpression.setRootNumber(2);
        FunctionExpression.setExpression(Expression);

        RadicalFunction Function = new RadicalFunction(FunctionExpression);

        QUESTION = "Find the range of the function: <br>" + Function.toString();
        RIGHT_ANSWER = (new Interval(false, Integer.MIN_VALUE, Integer.MAX_VALUE, false)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_C = (new Interval(true, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_D = (new Interval(false, Integer.MIN_VALUE, (new LinearEquation(Expression)).getSolutionNumber(), true)).toString();
    }

    //  RANGE   cbrt(ax + b)
    private void createProblem_07(){
        Polynomial Coefficient = new Polynomial();
        Polynomial Expression = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        RadicalPolynomial FunctionExpression = new RadicalPolynomial();
        FunctionExpression.setCoefficient(Coefficient);
        FunctionExpression.setRootNumber(3);
        FunctionExpression.setExpression(Expression);

        RadicalFunction Function = new RadicalFunction(FunctionExpression);

        QUESTION = "Find the range of the function: <br>" + Function.toString();
        RIGHT_ANSWER = (new Interval(false, Integer.MIN_VALUE, Integer.MAX_VALUE, false)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_C = (new Interval(true, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_D = (new Interval(false, Integer.MIN_VALUE, (new LinearEquation(Expression)).getSolutionNumber(), true)).toString();
    }

    //  RANGE   sqrt(ax + b) + a
    private void createProblem_08(){
        Polynomial Coefficient = new Polynomial();
        Polynomial Expression = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        int Number = MathUtility.getRandomNumber_1Digit();

        RadicalPolynomial FunctionExpression = new RadicalPolynomial();
        FunctionExpression.setCoefficient(Coefficient);
        FunctionExpression.setRootNumber(2);
        FunctionExpression.setExpression(Expression);

        RadicalFunction Function = new RadicalFunction(FunctionExpression);

        QUESTION = "Find the range of the function: <br>" + Function.toString() + " + " + String.valueOf(Number);
        RIGHT_ANSWER = (new Interval(true, Number, Integer.MAX_VALUE, false)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, Number, Integer.MAX_VALUE, false)).toString();
        ANSWER_C = (new Interval(true, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_D = (new Interval(false, Integer.MIN_VALUE, (new LinearEquation(Expression)).getSolutionNumber(), true)).toString();
    }

    //  RANGE   a * sqrt(ax + b) + a
    private void createProblem_09(){
        Polynomial Coefficient = new Polynomial();
        AlgebraicNumber TempNumber = new AlgebraicNumber();
        TempNumber.setCoefficient(MathUtility.getRandomPositiveNumber_1Digit() + 1);
        TempNumber.addVariable(new Variable("x", 0));
        AlgebraicExpression TempExpression = new AlgebraicExpression();
        TempExpression.addNumber(TempNumber);
        Coefficient.setExpression(TempExpression);

        Polynomial Expression = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        int Number = MathUtility.getRandomNumber_1Digit();

        RadicalPolynomial FunctionExpression = new RadicalPolynomial();
        FunctionExpression.setCoefficient(Coefficient);
        FunctionExpression.setRootNumber(2);
        FunctionExpression.setExpression(Expression);

        RadicalFunction Function = new RadicalFunction(FunctionExpression);

        QUESTION = "Find the range of the function: <br>" + Function.toString() + " + " + String.valueOf(Number);
        RIGHT_ANSWER = (new Interval(true, Number, Integer.MAX_VALUE, false)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, Number, Integer.MAX_VALUE, false)).toString();
        ANSWER_C = (new Interval(true, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_D = (new Interval(false, Integer.MIN_VALUE, (new LinearEquation(Expression)).getSolutionNumber(), true)).toString();
    }

    //  RANGE   -a * sqrt(ax + b) + a
    private void createProblem_10(){
        Polynomial Coefficient = new Polynomial();
        AlgebraicNumber TempNumber = new AlgebraicNumber();
        TempNumber.setCoefficient(MathUtility.getRandomNegativeNumber_1Digit());
        TempNumber.addVariable(new Variable("x", 0));
        AlgebraicExpression TempExpression = new AlgebraicExpression();
        TempExpression.addNumber(TempNumber);
        Coefficient.setExpression(TempExpression);

        Polynomial Expression = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        int Number = MathUtility.getRandomNumber_1Digit();

        RadicalPolynomial FunctionExpression = new RadicalPolynomial();
        FunctionExpression.setCoefficient(Coefficient);
        FunctionExpression.setRootNumber(2);
        FunctionExpression.setExpression(Expression);

        RadicalFunction Function = new RadicalFunction(FunctionExpression);

        QUESTION = "Find the range of the function: <br>" + Function.toString() + " + " + String.valueOf(Number);
        RIGHT_ANSWER = (new Interval(false, Integer.MIN_VALUE, Number, true)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, Number, Integer.MAX_VALUE, false)).toString();
        ANSWER_C = (new Interval(true, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_D = (new Interval(false, Integer.MIN_VALUE, (new LinearEquation(Expression)).getSolutionNumber(), true)).toString();
    }

    //  RANGE   a * sqrt(ax + b)
    private void createProblem_11(){
        Polynomial Coefficient = new Polynomial();
        AlgebraicNumber TempNumber = new AlgebraicNumber();
        TempNumber.setCoefficient(MathUtility.getRandomPositiveNumber_1Digit() + 1);
        TempNumber.addVariable(new Variable("x", 0));
        AlgebraicExpression TempExpression = new AlgebraicExpression();
        TempExpression.addNumber(TempNumber);
        Coefficient.setExpression(TempExpression);

        Polynomial Expression = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        RadicalPolynomial FunctionExpression = new RadicalPolynomial();
        FunctionExpression.setCoefficient(Coefficient);
        FunctionExpression.setRootNumber(2);
        FunctionExpression.setExpression(Expression);

        RadicalFunction Function = new RadicalFunction(FunctionExpression);

        QUESTION = "Find the range of the function: <br>" + Function.toString();
        RIGHT_ANSWER = (new Interval(true, 0, Integer.MAX_VALUE, false)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_C = (new Interval(true, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_D = (new Interval(false, Integer.MIN_VALUE, (new LinearEquation(Expression)).getSolutionNumber(), true)).toString();
    }

    //  RANGE   -a * sqrt(ax + b)
    private void createProblem_12(){
        Polynomial Coefficient = new Polynomial();
        AlgebraicNumber TempNumber = new AlgebraicNumber();
        TempNumber.setCoefficient(MathUtility.getRandomNegativeNumber_1Digit());
        TempNumber.addVariable(new Variable("x", 0));
        AlgebraicExpression TempExpression = new AlgebraicExpression();
        TempExpression.addNumber(TempNumber);
        Coefficient.setExpression(TempExpression);

        Polynomial Expression = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        RadicalPolynomial FunctionExpression = new RadicalPolynomial();
        FunctionExpression.setCoefficient(Coefficient);
        FunctionExpression.setRootNumber(2);
        FunctionExpression.setExpression(Expression);

        RadicalFunction Function = new RadicalFunction(FunctionExpression);

        QUESTION = "Find the range of the function: <br>" + Function.toString();
        RIGHT_ANSWER = (new Interval(false, Integer.MIN_VALUE, 0, true)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_C = (new Interval(true, (new LinearEquation(Expression)).getSolutionNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_D = (new Interval(false, Integer.MIN_VALUE, (new LinearEquation(Expression)).getSolutionNumber(), true)).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
