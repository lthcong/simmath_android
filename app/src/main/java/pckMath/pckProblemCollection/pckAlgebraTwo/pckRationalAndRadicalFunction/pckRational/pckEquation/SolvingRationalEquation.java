package pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction.pckRational.pckEquation;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicExpression;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.Variable;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.LinearEquation;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRationalPolynomial.RationalPolynomial;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 8/9/2017.
 */

public class SolvingRationalEquation extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SolvingRationalEquation(){
        createProblem();
        swapAnswer();
    }

    public SolvingRationalEquation(String Question,
                                                         String RightAnswer,
                                                         String AnswerA,
                                                         String AnswerB,
                                                         String AnswerC,
                                                         String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 7;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            default:
                createProblem_01();
                break;
        }
        return new SolvingRationalEquation(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  A/B = 0
    private void createProblem_01(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        if (PolA.toString().equalsIgnoreCase(PolB.toString())){
            createProblem_01();
        }

        RationalEquation Equation = new RationalEquation(PolA, PolB);
        String Result = (new LinearEquation(PolA)).getSolution();

        QUESTION = Equation.toString();
        RIGHT_ANSWER = Result;

        ANSWER_A= Result;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString();
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  AB/B = 0
    private void createProblem_02(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        if (PolA.toString().equalsIgnoreCase(PolB.toString())){
            createProblem_02();
        }

        RationalEquation Equation = new RationalEquation(AlgebraMath.mul(PolA, PolB), PolB);
        String Result = (new LinearEquation(PolA)).getSolution();

        QUESTION = Equation.toString();
        RIGHT_ANSWER = Result;

        ANSWER_A= Result;
        ANSWER_B = (new LinearEquation(PolA)).getSolution() + ", " + (new LinearEquation(PolB)).getSolution();
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", " + "x = " + FractionMath.getRandomFraction().toString();
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  1/A = 0
    private void createProblem_03(){
        Polynomial PolA = new Polynomial();
        AlgebraicNumber TempNumber = new AlgebraicNumber();
        TempNumber.setCoefficient(MathUtility.getRandomNumber_1Digit());
        TempNumber.addVariable(new Variable("x", 0));
        AlgebraicExpression TempExpression = new AlgebraicExpression();
        TempExpression.addNumber(TempNumber);
        PolA.setExpression(TempExpression);

        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        RationalEquation Equation = new RationalEquation(PolA, PolB);
        String Result = InfoCollector.getNoSolution();

        QUESTION = Equation.toString();
        RIGHT_ANSWER = Result;

        ANSWER_A= Result;
        ANSWER_B = InfoCollector.getInfiniteManySolution();
        ANSWER_C = (new LinearEquation(PolB)).getSolution();
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  A/AB = 0
    private void createProblem_04(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        RationalEquation Equation = new RationalEquation(PolA, AlgebraMath.mul(PolA, PolB));
        String Result = InfoCollector.getNoSolution();

        QUESTION = Equation.toString();
        RIGHT_ANSWER = Result;

        ANSWER_A= Result;
        ANSWER_B = InfoCollector.getInfiniteManySolution();
        ANSWER_C = (new LinearEquation(PolB)).getSolution();
        ANSWER_D = (new LinearEquation(PolA)).getSolution();
    }

    //  AB/AC = 0
    private void createProblem_05(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolC = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        RationalEquation Equation = new RationalEquation(AlgebraMath.mul(PolA, PolB), AlgebraMath.mul(PolA, PolC));
        String Result = (new LinearEquation(PolB)).getSolution();

        QUESTION = Equation.toString();
        RIGHT_ANSWER = Result;

        ANSWER_A= Result;
        ANSWER_B = (new LinearEquation(PolB)).getSolution() + ", " + (new LinearEquation(PolA)).getSolution();
        ANSWER_C = (new LinearEquation(PolC)).getSolution() + ", " + (new LinearEquation(PolA)).getSolution();
        ANSWER_D = (new LinearEquation(PolA)).getSolution();
    }

    //  A/B = C/B
    private void createProblem_06(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolC = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        RationalPolynomial LeftSide = new RationalPolynomial(PolA, PolB);
        RationalPolynomial RightSide = new RationalPolynomial(PolC, PolB);
        String Result = (new LinearEquation(AlgebraMath.sub(PolA, PolC))).getSolution();
        if (Result.equalsIgnoreCase((new LinearEquation(PolB).getSolution()))){
            Result = InfoCollector.getNoSolution();
        }

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result;

        ANSWER_A= Result;
        ANSWER_B = (new LinearEquation(PolB)).getSolution() + ", " + (new LinearEquation(PolA)).getSolution();
        ANSWER_C = (new LinearEquation(PolC)).getSolution() + ", " + (new LinearEquation(PolA)).getSolution();
        ANSWER_D = (new LinearEquation(PolA)).getSolution();
    }

    //  A/B = C/D
    private void createProblem_07(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolC = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolD = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        RationalPolynomial LeftSide = new RationalPolynomial(PolA, PolB);
        RationalPolynomial RightSide = new RationalPolynomial(PolC, PolD);
        String Result = QuadraticMath.getSolution(new QuadraticEquation(AlgebraMath.sub(AlgebraMath.mul(PolA, PolD), AlgebraMath.mul(PolB, PolC)))).toString();
        Result = Result.substring(1, Result.length() - 1);

        QUESTION = LeftSide + " = " + RightSide;
        RIGHT_ANSWER = Result;

        ANSWER_A= Result;
        ANSWER_B = (new LinearEquation(PolB)).getSolution() + ", " + (new LinearEquation(PolA)).getSolution();
        ANSWER_C = (new LinearEquation(PolC)).getSolution() + ", " + (new LinearEquation(PolA)).getSolution();
        ANSWER_D = (new LinearEquation(PolA)).getSolution();
    }

    @Override
    public String getQuestion() {
        QUESTION = "Solve this equation:<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
