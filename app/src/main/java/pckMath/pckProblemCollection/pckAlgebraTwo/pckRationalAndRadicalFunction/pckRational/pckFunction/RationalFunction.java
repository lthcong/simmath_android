package pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction.pckRational.pckFunction;

import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRationalPolynomial.RationalPolynomial;

/**
 * Created by Cong on 8/8/2017.
 */

public class RationalFunction {

    private RationalPolynomial FUNCTION_EXPRESSION;

    public RationalFunction(){
        FUNCTION_EXPRESSION = new RationalPolynomial();
    }

    public RationalFunction(Polynomial TopExpression, Polynomial BottomExpression){
        FUNCTION_EXPRESSION = new RationalPolynomial(TopExpression, BottomExpression);
    }

    public void setFunctionExpression(RationalPolynomial FunctionExpression){
        FUNCTION_EXPRESSION = FunctionExpression;
    }

    public RationalPolynomial getFunctionExpression(){
        return FUNCTION_EXPRESSION;
    }

    public String toString(){
        return "y = " + FUNCTION_EXPRESSION.toString();
    }

}
