package pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction.pckRadical.pckEquation;

import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRadicalPolynomial.RadicalPolynomial;

/**
 * Created by Cong on 8/8/2017.
 */

public class RadicalEquation {

    private RadicalPolynomial LEFT_SIDE, RIGHT_SIDE;

    public RadicalEquation(){
        LEFT_SIDE = new RadicalPolynomial();
        RIGHT_SIDE = new RadicalPolynomial();
    }

    public void setLeftSide(RadicalPolynomial LeftSide){
        LEFT_SIDE = LeftSide;
    }

    public RadicalPolynomial getLeftSide(){
        return LEFT_SIDE;
    }

    public void setRightSide(RadicalPolynomial RightSide){
        RIGHT_SIDE = RightSide;
    }

    public RadicalPolynomial getRightSide(){
        return RIGHT_SIDE;
    }

    public String toString(){
        return LEFT_SIDE.toString() + " = " + RIGHT_SIDE;
    }

}
