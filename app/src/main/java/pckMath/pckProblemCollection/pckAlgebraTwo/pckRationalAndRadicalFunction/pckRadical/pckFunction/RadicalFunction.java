package pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction.pckRadical.pckFunction;

import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRadicalPolynomial.RadicalPolynomial;

/**
 * Created by Cong on 8/8/2017.
 */

public class RadicalFunction {

    private RadicalPolynomial RADICAL_EXPRESSION;

    public RadicalFunction(){
        RADICAL_EXPRESSION = new RadicalPolynomial();
    }

    public RadicalFunction(RadicalPolynomial Expression){
        RADICAL_EXPRESSION = Expression;
    }

    public void setExpression(RadicalPolynomial Expression){
        RADICAL_EXPRESSION = Expression;
    }

    public RadicalPolynomial getExpression(){
        return RADICAL_EXPRESSION;
    }

    public String toString(){
        return "y = " + RADICAL_EXPRESSION.toString();
    }

}
