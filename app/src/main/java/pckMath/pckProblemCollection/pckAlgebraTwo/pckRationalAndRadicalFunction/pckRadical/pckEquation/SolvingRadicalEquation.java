package pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction.pckRadical.pckEquation;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicExpression;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.Variable;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.LinearEquation;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRadicalPolynomial.RadicalPolynomial;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 8/10/2017.
 */

public class SolvingRadicalEquation extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SolvingRadicalEquation(){
        createProblem();
        swapAnswer();
    }

    public SolvingRadicalEquation(String Question,
                                   String RightAnswer,
                                   String AnswerA,
                                   String AnswerB,
                                   String AnswerC,
                                   String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 5;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            default:
                createProblem_01();
                break;
        }
        return new SolvingRadicalEquation(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  sqrt(ax + b) = 0
    private void createProblem_01(){
        RadicalPolynomial LeftSide = new RadicalPolynomial();
        LeftSide.setRootNumber(2);
        LeftSide.setExpression(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1));

        RadicalPolynomial RightSide = new RadicalPolynomial();

        RadicalEquation Equation = new RadicalEquation();
        Equation.setLeftSide(LeftSide);
        Equation.setRightSide(RightSide);

        QUESTION = Equation.toString();
        RIGHT_ANSWER = (new LinearEquation(LeftSide.getExpression())).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1))).getSolution();
        ANSWER_C = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1))).getSolution();
        ANSWER_D = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1))).getSolution();
    }

    //  sqrt(ax + b) = a
    private void createProblem_02(){
        RadicalPolynomial LeftSide = new RadicalPolynomial();
        LeftSide.setRootNumber(2);
        LeftSide.setExpression(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1));

        int RightSideNumber = MathUtility.getRandomPositiveNumber_1Digit();

        RadicalPolynomial RightSide = new RadicalPolynomial();
        AlgebraicNumber TempNumber = new AlgebraicNumber();
        TempNumber.setCoefficient(RightSideNumber);
        TempNumber.addVariable(new Variable("x", 0));
        AlgebraicExpression TempExpression = new AlgebraicExpression();
        TempExpression.addNumber(TempNumber);
        Polynomial TempPolynomial = new Polynomial();
        TempPolynomial.setExpression(TempExpression);
        RightSide.setCoefficient(TempPolynomial);

        TempNumber = new AlgebraicNumber();
        TempNumber.setCoefficient(RightSideNumber * RightSideNumber);
        TempNumber.addVariable(new Variable("x", 0));
        TempExpression = new AlgebraicExpression();
        TempExpression.addNumber(TempNumber);
        Polynomial TempRightSide = new Polynomial();
        TempRightSide.setExpression(TempExpression);

        RadicalEquation Equation = new RadicalEquation();
        Equation.setLeftSide(LeftSide);
        Equation.setRightSide(RightSide);

        QUESTION = Equation.toString();
        RIGHT_ANSWER = (new LinearEquation(AlgebraMath.sub(LeftSide.getExpression(), TempRightSide))).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1))).getSolution();
        ANSWER_C = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1))).getSolution();
        ANSWER_D = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1))).getSolution();
    }

    //  a * sqrt(ax + b) = b
    private void createProblem_03(){
        AlgebraicNumber TempNumber;
        AlgebraicExpression TempExpression;
        Polynomial TempPolynomial;

        //  LEFT SIDE

        int LeftSideNumber = MathUtility.getRandomPositiveNumber_1Digit();
        TempNumber = new AlgebraicNumber();
        TempNumber.setCoefficient(LeftSideNumber);
        TempNumber.addVariable(new Variable("x", 0));
        TempExpression = new AlgebraicExpression();
        TempExpression.addNumber(TempNumber);
        TempPolynomial = new Polynomial();
        TempPolynomial.setExpression(TempExpression);
        RadicalPolynomial LeftSide = new RadicalPolynomial();
        LeftSide.setCoefficient(TempPolynomial);
        LeftSide.setRootNumber(2);
        LeftSide.setExpression(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1));

        //  RIGHT SIDE

        int RightSideNumber = MathUtility.getRandomPositiveNumber_1Digit() * LeftSideNumber;

        RadicalPolynomial RightSide = new RadicalPolynomial();
        TempNumber = new AlgebraicNumber();
        TempNumber.setCoefficient(RightSideNumber);
        TempNumber.addVariable(new Variable("x", 0));
        TempExpression = new AlgebraicExpression();
        TempExpression.addNumber(TempNumber);
        TempPolynomial = new Polynomial();
        TempPolynomial.setExpression(TempExpression);
        RightSide.setCoefficient(TempPolynomial);

        TempNumber = new AlgebraicNumber();
        TempNumber.setCoefficient(RightSideNumber / LeftSideNumber * RightSideNumber / LeftSideNumber);
        TempNumber.addVariable(new Variable("x", 0));
        TempExpression = new AlgebraicExpression();
        TempExpression.addNumber(TempNumber);
        Polynomial TempRightSide = new Polynomial();
        TempRightSide.setExpression(TempExpression);

        RadicalEquation Equation = new RadicalEquation();
        Equation.setLeftSide(LeftSide);
        Equation.setRightSide(RightSide);

        QUESTION = Equation.toString();
        RIGHT_ANSWER = (new LinearEquation(AlgebraMath.sub(LeftSide.getExpression(), TempRightSide))).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1))).getSolution();
        ANSWER_C = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1))).getSolution();
        ANSWER_D = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1))).getSolution();
    }

    //  sqrt(ax + b) = sqrt(ax + b)
    private void createProblem_04(){
        RadicalPolynomial LeftSide = new RadicalPolynomial();
        LeftSide.setRootNumber(2);
        LeftSide.setExpression(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1));

        RadicalPolynomial RightSide = new RadicalPolynomial();
        RightSide.setRootNumber(2);
        RightSide.setExpression(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1));

        RadicalEquation Equation = new RadicalEquation();
        Equation.setLeftSide(LeftSide);
        Equation.setRightSide(RightSide);


        QUESTION = Equation.toString();
        RIGHT_ANSWER = (new LinearEquation(AlgebraMath.sub(LeftSide.getExpression(), RightSide.getExpression()))).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1))).getSolution();
        ANSWER_C = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1))).getSolution();
        ANSWER_D = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1))).getSolution();
    }

    //  sqrt(ax^2 + bx + c) = 0
    private void createProblem_05(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial Pol = AlgebraMath.mul(PolA, PolB);

        RadicalPolynomial LeftSide = new RadicalPolynomial();
        LeftSide.setRootNumber(2);
        LeftSide.setExpression(Pol);

        RadicalPolynomial RightSide = new RadicalPolynomial();

        RadicalEquation Equation = new RadicalEquation();
        Equation.setLeftSide(LeftSide);
        Equation.setRightSide(RightSide);

        String Result = QuadraticMath.getSolution(new QuadraticEquation(Pol)).toString();
        Result = Result.substring(1, Result.length() - 1);

        QUESTION = Equation.toString();
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "x = " + FractionMath.getRandomUnitFraction().toString() + ", x = " + FractionMath.getRandomUnitFraction().toString();
    }

    @Override
    public String getQuestion() {
        QUESTION = "Solve this equation:<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
