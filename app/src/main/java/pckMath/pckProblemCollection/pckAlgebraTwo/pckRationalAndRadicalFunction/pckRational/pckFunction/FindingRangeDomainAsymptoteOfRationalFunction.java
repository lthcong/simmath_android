package pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction.pckRational.pckFunction;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicExpression;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.Variable;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.FunctionMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckLinearFunction.LinearFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.LinearEquation;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 8/8/2017.
 */

public class FindingRangeDomainAsymptoteOfRationalFunction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingRangeDomainAsymptoteOfRationalFunction(){
        createProblem();
        swapAnswer();
    }

    public FindingRangeDomainAsymptoteOfRationalFunction(String Question,
                                             String RightAnswer,
                                             String AnswerA,
                                             String AnswerB,
                                             String AnswerC,
                                             String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 8;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }
        return new FindingRangeDomainAsymptoteOfRationalFunction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  DOMAIN   1:1
    private void createProblem_01(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        if (PolA.toString().equalsIgnoreCase(PolB.toString())){
            createProblem_01();
        }

        RationalFunction Function = new RationalFunction(PolA, PolB);

        QUESTION = "Find the domain of the function: <br>" + Function.toString();
        RIGHT_ANSWER = "x" + InfoCollector.getNotEqualToSign() + (new LinearEquation(PolB)).getSolutionNumber();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x" + InfoCollector.getNotEqualToSign() + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x" + InfoCollector.getNotEqualToSign() + FractionMath.getRandomFraction().toString();
        ANSWER_D = "x" + InfoCollector.getNotEqualToSign() + FractionMath.getRandomUnitFraction().toString();
    }

    //  DOMAIN  1:1
    private void createProblem_02(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.mul(PolA, MathUtility.getRandomNumber_1Digit());

        RationalFunction Function = new RationalFunction(PolA, PolB);
        String Number = (new LinearEquation(PolB)).getSolutionNumber();

        QUESTION = "Find the domain of the function: <br>" + Function.toString();
        RIGHT_ANSWER = "(-" + InfoCollector.getInfinity() + ", " + Number + ") "
                + InfoCollector.getUnionSet() + " (" + Number + InfoCollector.getInfinity() + ")";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "(-" + InfoCollector.getInfinity() + ", " + FractionMath.getRandomFraction().toString() + ") "
                + InfoCollector.getUnionSet() + " (" + FractionMath.getRandomUnitFraction().toString() + InfoCollector.getInfinity() + ")";
        ANSWER_C = "(-" + InfoCollector.getInfinity() + ", " + FractionMath.getRandomFraction().toString() + ") "
                + InfoCollector.getUnionSet() + " (" + FractionMath.getRandomUnitFraction().toString() + InfoCollector.getInfinity() + ")";
        ANSWER_D = "(-" + InfoCollector.getInfinity() + ", " + FractionMath.getRandomFraction().toString() + ") "
                + InfoCollector.getUnionSet() + " (" + FractionMath.getRandomUnitFraction().toString() + InfoCollector.getInfinity() + ")";
    }

    //  RANGE   1:1
    private void createProblem_03(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        if (PolA.toString().equalsIgnoreCase(PolB.toString())){
            createProblem_01();
        }

        RationalFunction Function = new RationalFunction(PolA, PolB);

        String Number;
        int a = PolA.getExpression().getNumber(0).getCoefficient();
        int b = PolB.getExpression().getNumber(0).getCoefficient();
        if (a % b == 0){
            Number = String.valueOf((a / b));
        }
        else {
            Number = FractionMath.getSimplifiedFraction((new Fraction(a, b))).toString();
        }


        QUESTION = "Find the range of the function: <br>" + Function.toString();
        RIGHT_ANSWER = "(-" + InfoCollector.getInfinity() + ", " + Number + ") "
                + InfoCollector.getUnionSet() + " (" + Number + InfoCollector.getInfinity() + ")";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "(-" + InfoCollector.getInfinity() + ", " + FractionMath.getRandomFraction().toString() + ") "
                + InfoCollector.getUnionSet() + " (" + FractionMath.getRandomUnitFraction().toString() + InfoCollector.getInfinity() + ")";
        ANSWER_C = "(-" + InfoCollector.getInfinity() + ", " + FractionMath.getRandomFraction().toString() + ") "
                + InfoCollector.getUnionSet() + " (" + FractionMath.getRandomUnitFraction().toString() + InfoCollector.getInfinity() + ")";
        ANSWER_D = "(-" + InfoCollector.getInfinity() + ", " + FractionMath.getRandomFraction().toString() + ") "
                + InfoCollector.getUnionSet() + " (" + FractionMath.getRandomUnitFraction().toString() + InfoCollector.getInfinity() + ")";
    }

    //  RANGE   0:1
    private void createProblem_04(){
        Polynomial PolA = new Polynomial();
        AlgebraicNumber TempNumber = new AlgebraicNumber();
        TempNumber.setCoefficient(MathUtility.getRandomNumber_1Digit());
        TempNumber.addVariable(new Variable("x", 0));
        AlgebraicExpression TempExpression = new AlgebraicExpression();
        TempExpression.addNumber(TempNumber);
        PolA.setExpression(TempExpression);

        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        RationalFunction Function = new RationalFunction(PolA, PolB);
        String Number = "0";

        QUESTION = "Find the range of the function: <br>" + Function.toString();
        RIGHT_ANSWER = "(-" + InfoCollector.getInfinity() + ", " + Number + ") "
                + InfoCollector.getUnionSet() + " (" + Number + InfoCollector.getInfinity() + ")";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "(-" + InfoCollector.getInfinity() + ", " + FractionMath.getRandomFraction().toString() + ") "
                + InfoCollector.getUnionSet() + " (" + FractionMath.getRandomUnitFraction().toString() + InfoCollector.getInfinity() + ")";
        ANSWER_C = "(-" + InfoCollector.getInfinity() + ", " + FractionMath.getRandomFraction().toString() + ") "
                + InfoCollector.getUnionSet() + " (" + FractionMath.getRandomUnitFraction().toString() + InfoCollector.getInfinity() + ")";
        ANSWER_D = "(-" + InfoCollector.getInfinity() + ", " + FractionMath.getRandomFraction().toString() + ") "
                + InfoCollector.getUnionSet() + " (" + FractionMath.getRandomUnitFraction().toString() + InfoCollector.getInfinity() + ")";
    }

    //  RANGE   1:2
    private void createProblem_05(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.mul(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1));

        RationalFunction Function = new RationalFunction(PolA, PolB);
        String Number = "0";

        QUESTION = "Find the range of the function: <br>" + Function.toString();
        RIGHT_ANSWER = "(-" + InfoCollector.getInfinity() + ", " + Number + ") "
                + InfoCollector.getUnionSet() + " (" + Number + InfoCollector.getInfinity() + ")";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "(-" + InfoCollector.getInfinity() + ", " + FractionMath.getRandomFraction().toString() + ") "
                + InfoCollector.getUnionSet() + " (" + FractionMath.getRandomUnitFraction().toString() + InfoCollector.getInfinity() + ")";
        ANSWER_C = "(-" + InfoCollector.getInfinity() + ", " + FractionMath.getRandomFraction().toString() + ") "
                + InfoCollector.getUnionSet() + " (" + FractionMath.getRandomUnitFraction().toString() + InfoCollector.getInfinity() + ")";
        ANSWER_D = "(-" + InfoCollector.getInfinity() + ", " + FractionMath.getRandomFraction().toString() + ") "
                + InfoCollector.getUnionSet() + " (" + FractionMath.getRandomUnitFraction().toString() + InfoCollector.getInfinity() + ")";
    }

    // VERTICAL ASYMPTOTE
    private void createProblem_06(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        if (PolA.toString().equalsIgnoreCase(PolB.toString())){
            createProblem_01();
        }

        RationalFunction Function = new RationalFunction(PolA, PolB);

        QUESTION = "Find the vertical asymptote of the function: <br>" + Function.toString();
        RIGHT_ANSWER = "x = " + (new LinearEquation(PolB)).getSolutionNumber();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1))).getSolutionNumber();
        ANSWER_C = "x = " + (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1))).getSolutionNumber();
        ANSWER_D = "x = " + (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1))).getSolutionNumber();
    }

    // HORIZONTAL ASYMPTOTE
    private void createProblem_07(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        if (PolA.toString().equalsIgnoreCase(PolB.toString())){
            createProblem_01();
        }

        RationalFunction Function = new RationalFunction(PolA, PolB);
        String Number;
        int a = PolA.getExpression().getNumber(0).getCoefficient();
        int b = PolB.getExpression().getNumber(0).getCoefficient();
        if (a % b == 0){
            Number = String.valueOf((a / b));
        }
        else {
            Number = FractionMath.getSimplifiedFraction((new Fraction(a, b))).toString();
        }

        QUESTION = "Find the horizontal asymptote of the function: <br>" + Function.toString();
        RIGHT_ANSWER = "y = " + Number;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "y = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "y = " + FractionMath.getRandomFraction();
    }

    //  OBLIQUE ASYMPTOTE
    private void createProblem_08(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        LinearFunction ObliqueAsymptote = new LinearFunction(PolA);
        RationalFunction Function = new RationalFunction(AlgebraMath.add(AlgebraMath.mul(PolA, PolB), MathUtility.getRandomNumber_1Digit()), PolB);

        QUESTION = "Find the oblique asymptote of the function: <br>" + Function.toString();
        RIGHT_ANSWER = ObliqueAsymptote.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
