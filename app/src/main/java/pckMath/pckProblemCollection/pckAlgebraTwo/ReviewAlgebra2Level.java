package pckMath.pckProblemCollection.pckAlgebraTwo;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.ReviewAnalyticGeometry;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.ReviewExponentialAndLogarithmic;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckMatrix.ReviewMatrix;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckRationalAndRadicalFunction.ReviewRationalAndRadicalFunction;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckSequencesAndSeries.ReviewSequencesAndSeries;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckTrigonometry.ReviewTrigonometry;

/**
 * Created by Cong on 9/7/2017.
 */

public class ReviewAlgebra2Level extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public ReviewAlgebra2Level(){
        createProblem();
        swapAnswer();
    }

    public ReviewAlgebra2Level(String Question,
                                    String RightAnswer,
                                    String AnswerA,
                                    String AnswerB,
                                    String AnswerC,
                                    String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        MathProblem Problem;
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                Problem = new ReviewExponentialAndLogarithmic();
                break;
            case 1:
                Problem = new ReviewRationalAndRadicalFunction();
                break;
            case 2:
                Problem = new ReviewMatrix();
                break;
            case 3:
                Problem = new ReviewTrigonometry();
                break;
            case 4:
                Problem = new ReviewAnalyticGeometry();
                break;
            case 5:
                Problem = new ReviewSequencesAndSeries();
                break;
            default:
                Problem = new ReviewExponentialAndLogarithmic();
                break;
        }

        QUESTION = Problem.getQuestion();
        RIGHT_ANSWER = Problem.getRightAnswer();
        ANSWER_A = Problem.getAnswerA();
        ANSWER_B = Problem.getAnswerB();
        ANSWER_C = Problem.getAnswerC();
        ANSWER_D = Problem.getAnswerD();

        return new ReviewAlgebra2Level(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
