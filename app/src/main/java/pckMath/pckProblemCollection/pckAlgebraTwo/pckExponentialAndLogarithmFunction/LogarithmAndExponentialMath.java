package pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction;

import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckExponential.ExponentialFunction;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckExponential.ExponentialNumber;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckLogarithm.LogarithmFunction;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckLogarithm.LogarithmNumber;
import pckMath.pckProblemCollection.pckIntermediateMath.pckExponential.Exponential;

/**
 * Created by Cong on 8/1/2017.
 */

public class LogarithmAndExponentialMath {

    public static ExponentialNumber getRandomExponentialNumber(){
        return new ExponentialNumber(MathUtility.getRandomPositiveNumber_1Digit() + 1, MathUtility.getRandomNumber_1Digit() + 1);
    }

    public static LogarithmNumber getRandomLogarithmicNumber(){
        return new LogarithmNumber(MathUtility.getRandomPositiveNumber_1Digit() + 1, MathUtility.getRandomPositiveNumber_1Digit() + 1);
    }

    public static ExponentialNumber convertToExponentialNumber(LogarithmNumber LogNumber){
        return new ExponentialNumber(LogNumber.getStringBase(), LogNumber.getValue(), LogNumber.getStringNumber());
    }

    public static LogarithmNumber convertToLogarithmNumber(ExponentialNumber ExpNumber){
        return new LogarithmNumber(ExpNumber.getStringBase(), ExpNumber.getValue(), ExpNumber.getExponent());
    }

}
