package pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckLogarithm;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicExpression;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.Variable;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.LinearEquation;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 8/3/2017.
 */

public class SolvingLogarithmEquation extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SolvingLogarithmEquation(){
        createProblem();
        swapAnswer();
    }

    public SolvingLogarithmEquation(String Question,
                                          String RightAnswer,
                                          String AnswerA,
                                          String AnswerB,
                                          String AnswerC,
                                          String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 8;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }
        return new SolvingLogarithmEquation(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  LOG(ax + b) = LOG(ax + b)
    private void createProblem_01(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);

        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        LogarithmNumber left_side = new LogarithmNumber(String.valueOf(Base), PolA.toString());
        LogarithmNumber right_side = new LogarithmNumber(String.valueOf(Base), PolB.toString());

        QUESTION = left_side + " = " + right_side;
        RIGHT_ANSWER = (new LinearEquation(AlgebraMath.sub(PolA, PolB))).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + FractionMath.getRandomFraction();
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  LOG(ax + b) = LOG(a)
    private void createProblem_02(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);

        AlgebraicNumber Temp = new AlgebraicNumber();
        Temp.setCoefficient(MathUtility.getRandomPositiveNumber_1Digit());
        Temp.addVariable((new Variable("x", 0)));

        AlgebraicExpression Expression = new AlgebraicExpression();
        Expression.addNumber(Temp);

        Polynomial PolB = new Polynomial();
        PolB.setExpression(Expression);

        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        LogarithmNumber left_side = new LogarithmNumber(String.valueOf(Base), PolA.toString());
        LogarithmNumber right_side = new LogarithmNumber(String.valueOf(Base), PolB.toString());

        QUESTION = left_side + " = " + right_side;
        RIGHT_ANSWER = (new LinearEquation(AlgebraMath.sub(PolA, PolB))).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + FractionMath.getRandomFraction();
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  LOG(ax + b) + LOG(ax + b) = LOG(1)
    private void createProblem_03(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        LogarithmNumber FirstNumber = new LogarithmNumber(String.valueOf(Base), PolA.toString());
        LogarithmNumber SecondNumber = new LogarithmNumber(String.valueOf(Base), PolB.toString());
        LogarithmNumber ThirdNumber = new LogarithmNumber(String.valueOf(Base), "1");

        QUESTION = FirstNumber.toString() + " + " + SecondNumber.toString() + " = " + ThirdNumber.toString();
        RIGHT_ANSWER = (new LinearEquation(PolA)).getSolution() + ", " + (new LinearEquation(PolB)).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ", " + "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ", " + "x = " + FractionMath.getRandomFraction();
        ANSWER_D = "x = " + FractionMath.getRandomFraction() + ", " + "x = " + FractionMath.getRandomFraction();
    }

    //  LOG(ax + b) + LOG(a) = LOG(ax + b)
    private void createProblem_04(){
        int Multiple = MathUtility.getRandomPositiveNumber_1Digit();
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);

        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        LogarithmNumber FirstNumber = new LogarithmNumber(String.valueOf(Base), PolA.toString());
        LogarithmNumber SecondNumber = new LogarithmNumber(String.valueOf(Base), String.valueOf(Multiple));
        LogarithmNumber ThirdNumber = new LogarithmNumber(String.valueOf(Base), PolB.toString());

        QUESTION = FirstNumber.toString() + " + " + SecondNumber.toString() + " = " + ThirdNumber.toString();
        RIGHT_ANSWER = (new LinearEquation(AlgebraMath.sub(AlgebraMath.mul(PolA, Multiple), PolB))).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + FractionMath.getRandomFraction();
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  LOG(ax + b) - LOG(a) = LOG(ax + b)
    private void createProblem_05(){
        int Multiple = MathUtility.getRandomPositiveNumber_1Digit();
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);

        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        LogarithmNumber FirstNumber = new LogarithmNumber(String.valueOf(Base), PolA.toString());
        LogarithmNumber SecondNumber = new LogarithmNumber(String.valueOf(Base), String.valueOf(Multiple));
        LogarithmNumber ThirdNumber = new LogarithmNumber(String.valueOf(Base), PolB.toString());

        QUESTION = FirstNumber.toString() + " - " + SecondNumber.toString() + " = " + ThirdNumber.toString();
        RIGHT_ANSWER = (new LinearEquation(AlgebraMath.sub(PolA, AlgebraMath.mul(PolB, Multiple)))).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + FractionMath.getRandomFraction();
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  LN(ax + b) = LN(ax + b)
    private void createProblem_06(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);

        LogarithmNumber left_side = new LogarithmNumber("e", PolA.toString());
        LogarithmNumber right_side = new LogarithmNumber("e", PolB.toString());

        QUESTION = left_side + " = " + right_side;
        RIGHT_ANSWER = (new LinearEquation(AlgebraMath.sub(PolA, PolB))).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + FractionMath.getRandomFraction();
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  LN(ax + b) + LN(a) = LN(ax + b)
    private void createProblem_07(){
        int Multiple = MathUtility.getRandomPositiveNumber_1Digit();
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);

        LogarithmNumber FirstNumber = new LogarithmNumber("e", PolA.toString());
        LogarithmNumber SecondNumber = new LogarithmNumber("e", String.valueOf(Multiple));
        LogarithmNumber ThirdNumber = new LogarithmNumber("e", PolB.toString());

        QUESTION = FirstNumber.toString() + " + " + SecondNumber.toString() + " = " + ThirdNumber.toString();
        RIGHT_ANSWER = (new LinearEquation(AlgebraMath.sub(AlgebraMath.mul(PolA, Multiple), PolB))).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + FractionMath.getRandomFraction();
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  LN(ax + b) - LN(a) = LN(ax + b)
    private void createProblem_08(){
        int Multiple = MathUtility.getRandomPositiveNumber_1Digit();
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);

        LogarithmNumber FirstNumber = new LogarithmNumber("e", PolA.toString());
        LogarithmNumber SecondNumber = new LogarithmNumber("e", String.valueOf(Multiple));
        LogarithmNumber ThirdNumber = new LogarithmNumber("e", PolB.toString());

        QUESTION = FirstNumber.toString() + " - " + SecondNumber.toString() + " = " + ThirdNumber.toString();
        RIGHT_ANSWER = (new LinearEquation(AlgebraMath.sub(PolA, AlgebraMath.mul(PolB, Multiple)))).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + FractionMath.getRandomFraction();
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    @Override
    public String getQuestion() {
        QUESTION = "Solve this equation:<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
