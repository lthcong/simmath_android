package pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckExponential;

import pckInfo.InfoCollector;

/**
 * Created by Cong on 8/3/2017.
 */

public class ExponentialNumber {

    private String BASE, EXPONENT, VALUE;

    public ExponentialNumber(){
        BASE = "1";
        EXPONENT = "x";
        VALUE = "1";
    }

    public ExponentialNumber(int Base){
        BASE = String.valueOf(Base);
        EXPONENT = "x";
        VALUE = BASE + InfoCollector.getExponentSign(EXPONENT);
    }

    public ExponentialNumber(String Base){
        BASE = Base;
        EXPONENT = "x";
        VALUE = BASE + InfoCollector.getExponentSign(EXPONENT);
    }

    public ExponentialNumber(int Base, int Exponent){
        BASE = String.valueOf(Base);
        EXPONENT = String.valueOf(Exponent);
        VALUE = BASE + InfoCollector.getExponentSign(EXPONENT);
    }

    public ExponentialNumber(int Base, String Exponent){
        BASE = String.valueOf(Base);
        EXPONENT = Exponent;
        VALUE = BASE + InfoCollector.getExponentSign(EXPONENT);
    }

    public ExponentialNumber(String Base, String Exponent){
        BASE = Base;
        EXPONENT = Exponent;
        VALUE = BASE + InfoCollector.getExponentSign(EXPONENT);
    }

    public ExponentialNumber(String Base, String Exponent, String StValue){
        BASE = Base;
        EXPONENT = Exponent;
        VALUE = StValue;
    }

    public ExponentialNumber(int Base, int Exponent, int StValue){
        BASE = String.valueOf(Base);
        EXPONENT = String.valueOf(Exponent);
        VALUE = String.valueOf(StValue);
    }

    public void setBase(int Base){
        BASE = String.valueOf(Base);
    }

    public void setBase(String Base){
        BASE = Base;
    }

    public String getStringBase(){
        return BASE;
    }

    public int getIntBase(){
        return Integer.parseInt(BASE);
    }

    public void setExponent(String Exponent){
        EXPONENT = Exponent;
    }

    public String getExponent(){
        return EXPONENT;
    }

    public void setValue(String StValue){
        VALUE = StValue;
    }

    public String getValue(){
        return VALUE;
    }

    public String toString(){
        return String.valueOf(BASE) + InfoCollector.getExponentSign(EXPONENT);
    }

}
