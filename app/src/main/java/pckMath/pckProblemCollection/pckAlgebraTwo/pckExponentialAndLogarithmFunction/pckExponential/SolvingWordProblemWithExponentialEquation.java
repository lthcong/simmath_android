package pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckExponential;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 8/4/2017.
 */

public class SolvingWordProblemWithExponentialEquation extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SolvingWordProblemWithExponentialEquation(){
        createProblem();
        swapAnswer();
    }

    public SolvingWordProblemWithExponentialEquation(String Question,
                                             String RightAnswer,
                                             String AnswerA,
                                             String AnswerB,
                                             String AnswerC,
                                             String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 11;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            default:
                createProblem_01();
                break;
        }
        return new SolvingWordProblemWithExponentialEquation(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    /*  SIMPLE INTEREST:        A = P(1 + rt)
     *  COMPOUNDED INTEREST:    A = P(1 + r/n)^(nt)
     *  COMPOUNDED CONTINUOUSLY A = A0 * e^(rt)
     */

    //  SIMPLE INTEREST
    private void createProblem_01(){
        double Principal = MathUtility.getRandomPositiveNumber_2Digit() * 1000;
        int Rate = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Time = MathUtility.getRandomPositiveNumber_1Digit();

        double Amount = Principal * (1.0 + ((double) Rate / 100.0) * (double) Time);
        Amount = MathUtility.round_2_Decimal(Amount);

        QUESTION = "A person invested $" + String.valueOf(Principal)
                + " in a simple interest account at " + String.valueOf(Rate) + "% for " + String.valueOf(Time) + " years. Find the ending balance.";
        RIGHT_ANSWER = String.valueOf(Amount);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Amount + 100.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Amount - 100.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Amount + 150.0));
    }

    //  SIMPLE INTEREST
    private void createProblem_02(){
        double Principal = MathUtility.getRandomPositiveNumber_2Digit() * 1000;
        int Rate = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Time = MathUtility.getRandomPositiveNumber_1Digit();

        double Amount = Principal * (1.0 + ((double) Rate / 100.0) * (double) Time);
        Amount = MathUtility.round_2_Decimal(Amount);

        QUESTION = "A person invested in a simple interest account at " + String.valueOf(Rate) + "%. "
                + "After " + String.valueOf(Time) + " years, the amount increased up to $" + String.valueOf(Amount)
                + ". Find the principal amount?";
        RIGHT_ANSWER = String.valueOf(Principal);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Principal + 100.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Principal - 100.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Principal + 150.0));
    }

    //  SIMPLE INTEREST
    private void createProblem_03(){
        double Principal = MathUtility.getRandomPositiveNumber_2Digit() * 1000;
        int Rate = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Time = MathUtility.getRandomPositiveNumber_1Digit();

        double Amount = Principal * (1.0 + ((double) Rate / 100.0) * (double) Time);
        Amount = MathUtility.round_2_Decimal(Amount);

        QUESTION = "A person invested $" + String.valueOf(Principal)
                + " in a simple interest account at " + String.valueOf(Rate)
                + "%. How long does it take the account to increase up to $" + String.valueOf(Amount) + "?";
        RIGHT_ANSWER = String.valueOf(Time);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Time + 1);
        ANSWER_C = String.valueOf(Time - 1);
        ANSWER_D = String.valueOf(Time + 2);
    }

    //  SIMPLE INTEREST
    private void createProblem_04(){
        double Principal = MathUtility.getRandomPositiveNumber_2Digit() * 1000;
        int Rate = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Time = MathUtility.getRandomPositiveNumber_1Digit();

        double Amount = Principal * (1.0 + ((double) Rate / 100.0) * (double) Time);
        Amount = MathUtility.round_2_Decimal(Amount);

        QUESTION = "A person invested $" + String.valueOf(Principal)
                + " in a simple interest account. After " + String.valueOf(Time) + " years, the amount increased up to $"
                + String.valueOf(Amount) + ". Find the rate.";
        RIGHT_ANSWER = String.valueOf(Rate) + "%";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Rate + 1) + "%";
        ANSWER_C = String.valueOf(Rate - 1) + "%";
        ANSWER_D = String.valueOf(Rate + 2) + "%";
    }

    //  COMPOUNDED INTEREST
    private void createProblem_05(){
        double Principal = MathUtility.getRandomPositiveNumber_2Digit() * 1000;
        int TimePerYear = 2;
        int Rate = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Time = MathUtility.getRandomPositiveNumber_1Digit();

        double Amount = Principal * Math.pow(1.0 + (((double) Rate / 100.0) / (double) TimePerYear), (double) (TimePerYear * Time));
        Amount = MathUtility.round_2_Decimal(Amount);

        QUESTION = "A person invested $" + String.valueOf(Principal)
                + " in a compounded semiannually interest account at " + String.valueOf(Rate) + "% for " + String.valueOf(Time) + " years. Find the ending balance.";
        RIGHT_ANSWER = String.valueOf(Amount);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Amount + 100.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Amount - 100.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Amount + 150.0));
    }

    //  COMPOUNDED INTEREST
    private void createProblem_06(){
        double Principal = MathUtility.getRandomPositiveNumber_2Digit() * 1000;
        int TimePerYear = 4;
        int Rate = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Time = MathUtility.getRandomPositiveNumber_1Digit();

        double Amount = Principal * Math.pow(1.0 + (((double) Rate / 100.0) / (double) TimePerYear), (double) (TimePerYear * Time));
        Amount = MathUtility.round_2_Decimal(Amount);

        QUESTION = "A person invested $" + String.valueOf(Principal)
                + " in a compounded quarterly interest account at " + String.valueOf(Rate) + "% for "
                + String.valueOf(Time) + " years. Find the ending balance.";
        RIGHT_ANSWER = String.valueOf(Amount);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Amount + 100.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Amount - 100.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Amount + 150.0));
    }

    //  COMPOUNDED INTEREST
    private void createProblem_07(){
        double Principal = MathUtility.getRandomPositiveNumber_2Digit() * 1000;
        int TimePerYear = 1;
        int Rate = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Time = MathUtility.getRandomPositiveNumber_1Digit();

        double Amount = Principal * Math.pow(1.0 + (((double) Rate / 100.0) / (double) TimePerYear), (double) (TimePerYear * Time));
        Amount = MathUtility.round_2_Decimal(Amount);

        QUESTION = "A person invested $" + String.valueOf(Principal)
                + " in a compounded annually interest account at " + String.valueOf(Rate) + "% for "
                + String.valueOf(Time) + " years. Find the ending balance.";
        RIGHT_ANSWER = String.valueOf(Amount);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Amount + 100.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Amount - 100.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Amount + 150.0));
    }

    //  COMPOUND CONTINUOUSLY
    private void createProblem_08(){
        double Principal = MathUtility.getRandomPositiveNumber_2Digit() * 1000;
        double Rate = (double) MathUtility.getRandomPositiveNumber_1Digit() + 1.0;
        int Time = MathUtility.getRandomPositiveNumber_1Digit();

        double Amount = Principal * Math.pow(Math.E, ((Rate / 100.0) * (double) Time));
        Amount = MathUtility.round_2_Decimal(Amount);

        QUESTION = "A person invested $" + String.valueOf(Principal)
                + " in an annual interest account at " + String.valueOf(Rate) + "% compounded continuously for "
                + String.valueOf(Time) + " years. Find the ending balance.";
        RIGHT_ANSWER = String.valueOf(Amount);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Amount + 100.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Amount - 100.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Amount + 150.0));
    }

    //  COMPOUND CONTINUOUSLY
    private void createProblem_09(){
        double Principal = MathUtility.getRandomPositiveNumber_2Digit() * 1000;
        double Rate = (double) MathUtility.getRandomPositiveNumber_1Digit() + 1.0;

        double Time = (Math.log(2.0) / Math.log(Math.E)) / (Rate / 100.0);
        Time = MathUtility.round_1_Decimal(Time);

        QUESTION = "A person invested $" + String.valueOf(Principal)
                + " in an annual interest account at " + String.valueOf(Rate)
                + "% compounded continuously. How long does it take the principal to double the value.";
        RIGHT_ANSWER = String.valueOf(Time);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Time + 2.0));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Time - 0.5));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Time + 1.5));
    }

    //  COMPOUND CONTINUOUSLY
    private void createProblem_10(){
        double Principal = MathUtility.getRandomPositiveNumber_2Digit() * 1000;
        double Rate = (double) MathUtility.getRandomPositiveNumber_1Digit() + 1.0;

        double Time = (Math.log(3.0) / Math.log(Math.E)) / (Rate / 100.0);
        Time = MathUtility.round_1_Decimal(Time);

        QUESTION = "A person invested $" + String.valueOf(Principal)
                + " in an annual interest account at " + String.valueOf(Rate)
                + "% compounded continuously. How long does it take the principal to triple the value.";
        RIGHT_ANSWER = String.valueOf(Time);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Time + 2.0));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Time - 0.5));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Time + 1.5));
    }

    //  COMPOUND CONTINUOUSLY
    private void createProblem_11(){
        int IncreasedTime = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Time = MathUtility.getRandomPositiveNumber_1Digit();
        double Rate = Math.log(IncreasedTime) / (Math.log(Math.E) * (double) Time);

        int SecondTime = Time + MathUtility.getRandomPositiveNumber_1Digit();
        double Initial = MathUtility.getRandomPositiveNumber_2Digit() * 1000.0;
        double Amount = Initial * Math.pow(Math.E, (Rate / 100.0) * (double) SecondTime);
        Amount = MathUtility.round_2_Decimal(Amount);

        QUESTION = "The population of the bacterial increases "
                + String.valueOf(IncreasedTime) + " times after " + String.valueOf(Time)
                + " hours. If the initial population is " + String.valueOf(Initial) + ". Find the population of the bacterial after "
                + String.valueOf(SecondTime) + " hours. Given the population after t hours is Q(t) = Q<sub>0</sub>e<sup>rt</sup>";
        RIGHT_ANSWER = String.valueOf(Amount);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Amount + 1000.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Amount - 1000.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Amount + 1500.0));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
