package pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckExponential;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicExpression;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.Variable;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.LinearEquation;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;
import pckMath.pckProblemCollection.pckIntermediateMath.pckExponential.Exponential;

/**
 * Created by Cong on 8/3/2017.
 */

public class SolvingExponentialEquationWithoutLog extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SolvingExponentialEquationWithoutLog(){
        createProblem();
        swapAnswer();
    }

    public SolvingExponentialEquationWithoutLog(String Question,
                                                String RightAnswer,
                                                String AnswerA,
                                                String AnswerB,
                                                String AnswerC,
                                                String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 7;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            default:
                createProblem_01();
                break;
        }
        return new SolvingExponentialEquationWithoutLog(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  a^x = b
    private void createProblem_01(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        Polynomial ExpA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        String left_side = (new ExponentialNumber(String.valueOf(Base), ExpA.toString())).toString();

        int ExpB = MathUtility.getRandomNumber_1Digit();
        int right_side = (new Exponential(Base, ExpB)).getValue();

        AlgebraicNumber Number = new AlgebraicNumber();
        Number.setCoefficient(ExpB);
        Number.addVariable((new Variable("x", 0)));

        AlgebraicExpression Expression = new AlgebraicExpression();
        Expression.addNumber(Number);

        Polynomial Pol = new Polynomial();
        Pol.setExpression(Expression);

        QUESTION = left_side + " = " + String.valueOf(right_side);
        RIGHT_ANSWER = (new LinearEquation(AlgebraMath.sub(ExpA, Pol))).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + FractionMath.getRandomUnitFraction();
        ANSWER_D = "x = " + FractionMath.getRandomFraction();
    }

    //  b * a^x = c
    private void createProblem_02(){
        int Multiple = MathUtility.getRandomPositiveNumber_1Digit();
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        Polynomial ExpA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        String left_side = (new ExponentialNumber(String.valueOf(Base), ExpA.toString())).toString();

        int ExpB = MathUtility.getRandomNumber_1Digit();
        int right_side = (new Exponential(Base, ExpB)).getValue() * Multiple;

        AlgebraicNumber Number = new AlgebraicNumber();
        Number.setCoefficient(ExpB);
        Number.addVariable((new Variable("x", 0)));

        AlgebraicExpression Expression = new AlgebraicExpression();
        Expression.addNumber(Number);

        Polynomial Pol = new Polynomial();
        Pol.setExpression(Expression);

        QUESTION = String.valueOf(Multiple) + " * " + left_side + " = " + String.valueOf(right_side);
        RIGHT_ANSWER = (new LinearEquation(AlgebraMath.sub(ExpA, Pol))).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + FractionMath.getRandomUnitFraction();
        ANSWER_D = "x = " + FractionMath.getRandomFraction();
    }

    //  a^x = a^x
    private void createProblem_03(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        Polynomial ExpA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial ExpB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        String left_side = (new ExponentialNumber(String.valueOf(Base), ExpA.toString())).toString();
        String right_side = (new ExponentialNumber(String.valueOf(Base), ExpB.toString())).toString();

        QUESTION = left_side + " = " + right_side;
        RIGHT_ANSWER = (new LinearEquation(AlgebraMath.sub(ExpA, ExpB))).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + FractionMath.getRandomUnitFraction();
        ANSWER_D = "x = " + FractionMath.getRandomFraction();
    }

    //  a^x * a^x = a^x
    private void createProblem_04(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        Polynomial ExpA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial ExpB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial ExpC = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        String left_side = (new ExponentialNumber(Base, ExpA.toString())).toString()
                + " * " + (new ExponentialNumber(Base, ExpB.toString())).toString();
        String right_side = (new ExponentialNumber(Base, ExpC.toString())).toString();

        QUESTION = left_side + " = " + right_side;
        RIGHT_ANSWER = (new LinearEquation(AlgebraMath.sub(AlgebraMath.add(ExpA, ExpB), ExpC))).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + FractionMath.getRandomUnitFraction();
        ANSWER_D = "x = " + FractionMath.getRandomFraction();
    }

    //  a^x * a^x = a^x * a^x
    private void createProblem_05(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        Polynomial ExpA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial ExpB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial ExpC = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial ExpD = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        String left_side = (new ExponentialNumber(Base, ExpA.toString())).toString()
                + " * " + (new ExponentialNumber(Base, ExpB.toString())).toString();
        String right_side = (new ExponentialNumber(Base, ExpC.toString())).toString()
                + " * " + (new ExponentialNumber(Base, ExpD.toString())).toString();

        QUESTION = left_side + " = " + right_side;
        RIGHT_ANSWER = (new LinearEquation(AlgebraMath.sub(AlgebraMath.add(ExpA, ExpB), AlgebraMath.add(ExpC, ExpD)))).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + FractionMath.getRandomUnitFraction();
        ANSWER_D = "x = " + FractionMath.getRandomFraction();
    }

    //  a^x = b
    private void createProblem_06(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial ExpA = AlgebraMath.mul(PolA, PolB);

        QUESTION = (new ExponentialNumber(Base, ExpA.toString())).toString() + " = 1";
        RIGHT_ANSWER = (new LinearEquation(PolA)).getSolution() + ", " + (new LinearEquation(PolB)).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ", x = " + FractionMath.getRandomFraction().toString();
        ANSWER_D = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + FractionMath.getRandomFraction().toString();
    }

    //  a^x * a^x = a^x
    private void createProblem_07(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int FirstExp = MathUtility.getRandomPositiveNumber_1Digit();
        int SecondExp = MathUtility.getRandomPositiveNumber_1Digit();
        int ThirdExp = MathUtility.getRandomPositiveNumber_1Digit();

        Polynomial ExpA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial ExpB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial ExpC = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        String left_side = (new ExponentialNumber((new Exponential(Base, FirstExp)).getValue(), ExpA.toString())).toString()
                + " * " + (new ExponentialNumber((new Exponential(Base, SecondExp)).getValue(), ExpB.toString())).toString();
        String right_side = (new ExponentialNumber((new Exponential(Base, ThirdExp)).getValue(), ExpC.toString())).toString();

        QUESTION = left_side + " = " + right_side;
        RIGHT_ANSWER = (new LinearEquation(AlgebraMath.sub(AlgebraMath.add(AlgebraMath.mul(ExpA, FirstExp)
                , AlgebraMath.mul(ExpB, SecondExp))
                , AlgebraMath.mul(ExpC, ThirdExp)))).getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + FractionMath.getRandomUnitFraction();
        ANSWER_D = "x = " + FractionMath.getRandomFraction();
    }

    @Override
    public String getQuestion() {
        QUESTION = "Solve this equation:<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
