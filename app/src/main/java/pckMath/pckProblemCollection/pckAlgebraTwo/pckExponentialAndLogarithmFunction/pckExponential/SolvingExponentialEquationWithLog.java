package pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckExponential;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.LogarithmAndExponentialMath;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckLogarithm.LogarithmNumber;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 8/3/2017.
 */

public class SolvingExponentialEquationWithLog extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SolvingExponentialEquationWithLog(){
        createProblem();
        swapAnswer();
    }

    public SolvingExponentialEquationWithLog(String Question,
                                                String RightAnswer,
                                                String AnswerA,
                                                String AnswerB,
                                                String AnswerC,
                                                String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }
        return new SolvingExponentialEquationWithLog(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  a^x = b
    private void createProblem_01(){
        String VarName = MathUtility.getVariable();
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Result = MathUtility.getRandomPositiveNumber_1Digit();

        ExponentialNumber Number = new ExponentialNumber(String.valueOf(Base), VarName, String.valueOf(Result));
        LogarithmNumber LogNumber = LogarithmAndExponentialMath.convertToLogarithmNumber(Number);

        QUESTION = Number.toString() + " = " + Number.getValue();
        RIGHT_ANSWER = LogNumber.getValue() + " = " + LogNumber.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LogNumber.getValue() + " = " + LogarithmAndExponentialMath.getRandomLogarithmicNumber().toString();
        ANSWER_C = LogNumber.getValue() + " = " + LogarithmAndExponentialMath.getRandomLogarithmicNumber().toString();
        ANSWER_D = LogNumber.getValue() + " = " + LogarithmAndExponentialMath.getRandomLogarithmicNumber().toString();
    }

    //  e^x = b
    private void createProblem_02(){
        String VarName = MathUtility.getVariable();
        int Result = MathUtility.getRandomPositiveNumber_1Digit();

        ExponentialNumber Number = new ExponentialNumber("e", VarName, String.valueOf(Result));
        LogarithmNumber LogNumber = LogarithmAndExponentialMath.convertToLogarithmNumber(Number);

        QUESTION = Number.toString() + " = " + Number.getValue();
        RIGHT_ANSWER = LogNumber.getValue() + " = " + LogNumber.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LogNumber.getValue() + " = " + LogarithmAndExponentialMath.getRandomLogarithmicNumber().toString();
        ANSWER_C = LogNumber.getValue() + " = " + LogarithmAndExponentialMath.getRandomLogarithmicNumber().toString();
        ANSWER_D = LogNumber.getValue() + " = " + LogarithmAndExponentialMath.getRandomLogarithmicNumber().toString();
    }

    //  A^(ax + b) = B
    private void createProblem_03(){
        int A = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        String VarName = MathUtility.getVariable();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int B = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        ExponentialNumber Number = new ExponentialNumber(String.valueOf(A),
                String.valueOf(a) + VarName + " + " + String.valueOf(b),
                String.valueOf(B));
        String Result = "x = (" + (new LogarithmNumber(A, B)).toString() + " - " + String.valueOf(b) + ")"
                + InfoCollector.getDivisionSign() + String.valueOf(a);

        QUESTION = Number.toString() + " = " + Number.getValue();
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = 0";
        ANSWER_C = "x = (" + (new LogarithmNumber(A + b, B + a)).toString() + " - " + String.valueOf(b) + ")"
                + InfoCollector.getDivisionSign() + String.valueOf(a);
        ANSWER_D = "x = (" + (new LogarithmNumber(A + a, B + a)).toString() + " - " + String.valueOf(B) + ")"
                + InfoCollector.getDivisionSign() + String.valueOf(A);
    }

    //  A^(ax + b) + B = C
    private void createProblem_04(){
        int A = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        String VarName = MathUtility.getVariable();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int B = MathUtility.getRandomPositiveNumber_1Digit();
        int C = MathUtility.getRandomPositiveNumber_1Digit() + B;

        QUESTION = String.valueOf(A) + InfoCollector.getExponentSign(String.valueOf(a) + VarName + " + " + String.valueOf(b))
                + " + " + String.valueOf(B) + " = " + String.valueOf(C);

        B = C - B;
        String Result = "x = (" + (new LogarithmNumber(A, B)).toString() + " - " + String.valueOf(b) + ")"
                + InfoCollector.getDivisionSign() + String.valueOf(a);
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = 0";
        ANSWER_C = "x = (" + (new LogarithmNumber(A + b, B + a)).toString() + " - " + String.valueOf(b) + ")"
                + InfoCollector.getDivisionSign() + String.valueOf(a);
        ANSWER_D = "x = (" + (new LogarithmNumber(A + a, B + a)).toString() + " - " + String.valueOf(B) + ")"
                + InfoCollector.getDivisionSign() + String.valueOf(A);
    }

    //  B * A^(ax + b) = C
    private void createProblem_05(){
        int A = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        String VarName = MathUtility.getVariable();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int B = MathUtility.getRandomPositiveNumber_1Digit();
        int C = MathUtility.getRandomPositiveNumber_1Digit() * B;

        QUESTION = String.valueOf(B) + " * " + String.valueOf(A) + InfoCollector.getExponentSign(String.valueOf(a) + VarName + " + " + String.valueOf(b))
                + " = " + String.valueOf(C);

        B = C / B;
        String Result = "x = (" + (new LogarithmNumber(A, B)).toString() + " - " + String.valueOf(b) + ")"
                + InfoCollector.getDivisionSign() + String.valueOf(a);
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = 0";
        ANSWER_C = "x = (" + (new LogarithmNumber(A + b, B + a)).toString() + " - " + String.valueOf(b) + ")"
                + InfoCollector.getDivisionSign() + String.valueOf(a);
        ANSWER_D = "x = (" + (new LogarithmNumber(A + a, B + a)).toString() + " - " + String.valueOf(B) + ")"
                + InfoCollector.getDivisionSign() + String.valueOf(A);
    }

    //  A^(ax + b) = B^(cx + d)
    private void createProblem_06(){
        String VarName = MathUtility.getVariable();

        int A = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        int B = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int c = MathUtility.getRandomPositiveNumber_1Digit();
        int d = MathUtility.getRandomPositiveNumber_1Digit();

        ExponentialNumber left_side = new ExponentialNumber(A, String.valueOf(a) + VarName + " + " + String.valueOf(b));
        ExponentialNumber right_side = new ExponentialNumber(B, String.valueOf(c) + VarName + " + " + String.valueOf(d));
        String result = VarName + " = "
                + InfoCollector.getStartNumerator() + String.valueOf(d) + "ln" + String.valueOf(B) + " - " + String.valueOf(b) + "ln" + String.valueOf(A) + InfoCollector.getEndNumerator()
                + "/"
                + InfoCollector.getStartDenominator() + String.valueOf(a) + "ln" + String.valueOf(A) + " - " + String.valueOf(c) + "ln" + String.valueOf(B) + InfoCollector.getEndDenominator();

        QUESTION = left_side.toString() + " = " + right_side.toString();
        RIGHT_ANSWER = result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = VarName + " = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = VarName + " = " + FractionMath.getRandomFraction();
        ANSWER_D = VarName + " = "
                + InfoCollector.getStartNumerator() + String.valueOf(MathUtility.getRandomNumber_1Digit()) + "ln" + String.valueOf(A) + " - " + String.valueOf(c) + "ln" + String.valueOf(B) + InfoCollector.getEndNumerator()
                + "/"
                + InfoCollector.getStartDenominator() + String.valueOf(d) + "ln" + String.valueOf(B) + " - " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + "ln" + String.valueOf(A) + InfoCollector.getEndDenominator();
    }

    @Override
    public String getQuestion() {
        QUESTION = "Solve this equation:<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
