package pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckLogarithm;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.LogarithmAndExponentialMath;

/**
 * Created by Cong on 8/3/2017.
 */

public class SimplifyingLogarithmExpression extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SimplifyingLogarithmExpression(){
        createProblem();
        swapAnswer();
    }

    public SimplifyingLogarithmExpression(String Question,
                                              String RightAnswer,
                                              String AnswerA,
                                              String AnswerB,
                                              String AnswerC,
                                              String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 8;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }
        return new SimplifyingLogarithmExpression(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  LOG(ax^2 + bx + c) = 2 LOG(ax+b)
    private void createProblem_01(){
        Polynomial Pol = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        QUESTION = (new LogarithmNumber(String.valueOf(Base), AlgebraMath.mul(Pol, Pol).toString())).toString();
        RIGHT_ANSWER = "2" + (new LogarithmNumber(String.valueOf(Base), Pol.toString())).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "2" + (LogarithmAndExponentialMath.getRandomLogarithmicNumber()).toString();
        ANSWER_C = (LogarithmAndExponentialMath.getRandomLogarithmicNumber()).toString();
        ANSWER_D = "2" + (LogarithmAndExponentialMath.getRandomLogarithmicNumber()).toString();
    }

    //  LN(ax^2 + bx + c) = 2 LN(ax+b)
    private void createProblem_02(){
        Polynomial Pol = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        QUESTION = (new LogarithmNumber("e", AlgebraMath.mul(Pol, Pol).toString())).toString();
        RIGHT_ANSWER = "2" + (new LogarithmNumber("e", Pol.toString())).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "2" + (new LogarithmNumber("e", AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1).toString())).toString();
        ANSWER_C = "2" + (new LogarithmNumber("e", AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1).toString())).toString();
        ANSWER_D = "2" + (new LogarithmNumber("e", AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1).toString())).toString();
    }

    //  LOG(ax^2 + bx + c) - LOG(ax + b) = LOG(ax + b)
    private void createProblem_03(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);

        Polynomial PolC = AlgebraMath.mul(PolA, PolB);

        QUESTION = (new LogarithmNumber("10", PolC.toString())).toString() + " - " + (new LogarithmNumber("10", PolB.toString())).toString();
        RIGHT_ANSWER = (new LogarithmNumber("10", PolA.toString())).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new LogarithmNumber("10", AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1).toString())).toString();
        ANSWER_C = (new LogarithmNumber("10", AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1).toString())).toString();
        ANSWER_D = (new LogarithmNumber("10", AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1).toString())).toString();
    }

    //  LN(ax^2 + bx + c) - LN(ax + b) = LN(ax + b)
    private void createProblem_04(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);

        Polynomial PolC = AlgebraMath.mul(PolA, PolB);

        QUESTION = (new LogarithmNumber("e", PolC.toString())).toString() + " - " + (new LogarithmNumber("e", PolB.toString())).toString();
        RIGHT_ANSWER = (new LogarithmNumber("e", PolA.toString())).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new LogarithmNumber("e", AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1).toString())).toString();
        ANSWER_C = (new LogarithmNumber("e", AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1).toString())).toString();
        ANSWER_D = (new LogarithmNumber("e", AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1).toString())).toString();
    }

    //  LOG(ax + b) + LOG(ax + b) = LOG(ax^2 + bx + c)
    private void createProblem_05(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);

        Polynomial PolC = AlgebraMath.mul(PolA, PolB);

        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        QUESTION = (new LogarithmNumber(String.valueOf(Base), PolA.toString())).toString() + " + " + (new LogarithmNumber(String.valueOf(Base), PolB.toString())).toString();
        RIGHT_ANSWER = (new LogarithmNumber(String.valueOf(Base), PolC.toString())).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new LogarithmNumber(String.valueOf(Base), AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1).toString())).toString();
        ANSWER_C = (new LogarithmNumber(String.valueOf(Base), AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1).toString())).toString();
        ANSWER_D = (new LogarithmNumber("e", AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1).toString())).toString();
    }

    //  LN(ax + b) + LN(ax + b) = LN(ax^2 + bx + c)
    private void createProblem_06(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);

        Polynomial PolC = AlgebraMath.mul(PolA, PolB);

        QUESTION = (new LogarithmNumber("e", PolA.toString())).toString() + " + " + (new LogarithmNumber("e", PolB.toString())).toString();
        RIGHT_ANSWER = (new LogarithmNumber("e", PolC.toString())).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new LogarithmNumber("e", AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1).toString())).toString();
        ANSWER_C = (new LogarithmNumber("e", AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1).toString())).toString();
        ANSWER_D = (new LogarithmNumber("e", AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1).toString())).toString();
    }

    //  LOG(ax^2 + bx + c) - LOG(ax + b) + LOG(ax + b) = LOG(ax^2 + bx + c)
    private void createProblem_07(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);
        Polynomial PolC = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);

        LogarithmNumber FirstNumber = new LogarithmNumber("10", AlgebraMath.mul(PolA, PolB).toString());
        LogarithmNumber SecondNumber = new LogarithmNumber("10", PolB.toString());
        LogarithmNumber ThirdNumber = new LogarithmNumber("10", PolC.toString());
        LogarithmNumber ForthNumber = new LogarithmNumber("10", AlgebraMath.mul(PolA, PolC).toString());

        QUESTION = FirstNumber.toString() + " - " + SecondNumber.toString() + " + " + ThirdNumber.toString();
        RIGHT_ANSWER = ForthNumber.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new LogarithmNumber("10", AlgebraMath.mul(PolB, PolB).toString()).toString();
        ANSWER_C = new LogarithmNumber("10", AlgebraMath.mul(PolC, PolB).toString()).toString();
        ANSWER_D = new LogarithmNumber("10", AlgebraMath.mul(PolA, PolA).toString()).toString();
    }

    //  LOG(ax + b) - LOG(cx + d) + LOG((cx + d) * (ex + f)) = LOG((ax + b)(ex + f))
    private void createProblem_08(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);
        Polynomial PolC = AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1);

        LogarithmNumber FirstNumber = new LogarithmNumber("10", PolA.toString());
        LogarithmNumber SecondNumber = new LogarithmNumber("10", PolB.toString());
        LogarithmNumber ThirdNumber = new LogarithmNumber("10", AlgebraMath.mul(PolB, PolC).toString());
        LogarithmNumber ForthNumber = new LogarithmNumber("10", AlgebraMath.mul(PolA, PolC).toString());

        QUESTION = FirstNumber.toString() + " - " + SecondNumber.toString() + " + " + ThirdNumber.toString();
        RIGHT_ANSWER = ForthNumber.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new LogarithmNumber("10", AlgebraMath.mul(PolB, PolB).toString()).toString();
        ANSWER_C = new LogarithmNumber("10", AlgebraMath.mul(PolC, PolB).toString()).toString();
        ANSWER_D = new LogarithmNumber("10", AlgebraMath.mul(PolA, PolA).toString()).toString();
    }

    @Override
    public String getQuestion() {
        QUESTION = "Simplify this expression:<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
