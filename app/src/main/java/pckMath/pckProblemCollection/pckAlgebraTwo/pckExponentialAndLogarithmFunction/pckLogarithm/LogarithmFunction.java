package pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckLogarithm;

import pckInfo.InfoCollector;

/**
 * Created by Cong on 8/1/2017.
 */

public class LogarithmFunction {

    private String BASE, NUMBER;
    private final String LOG = "LOG", LN = "LN";

    public LogarithmFunction(){
        BASE = "1";
        NUMBER = "1";
    }

    public LogarithmFunction(String Base){
        BASE = Base;
        NUMBER = "1";
    }

    public LogarithmFunction(int Base){
        BASE = String.valueOf(Base);
        NUMBER = "1";
    }

    public LogarithmFunction(String Base, String Number){
        BASE = Base;
        NUMBER = Number;
    }

    public LogarithmFunction(int Base, int Number){
        BASE = String.valueOf(Base);
        NUMBER = String.valueOf(Number);
    }

    public void setBase(String Base){
        BASE = Base;
    }

    public void setBase(int Base){
        BASE = String.valueOf(Base);
    }

    public int getIntBase(){
        return Integer.parseInt(BASE);
    }

    public String getStringBase(){
        return BASE;
    }

    public void setNumber(String Number){
        NUMBER = Number;
    }

    public void setNumber(int Number){
        NUMBER = String.valueOf(Number);
    }

    public int getIntNumber(){
        return Integer.parseInt(NUMBER);
    }

    public String getStringNumber(){
        return NUMBER;
    }

    public String toString(){
        String LogarithmString;
        if (BASE.equalsIgnoreCase("10")){
            LogarithmString = LOG + String.valueOf(NUMBER);
        }
        else {
            if (BASE.equalsIgnoreCase("e")){
                LogarithmString = LN + String.valueOf(NUMBER);
            }
            else {
                LogarithmString = LOG + InfoCollector.getStartDenominator() + BASE + InfoCollector.getEndDenominator() + "(" +  String.valueOf(NUMBER) + ")";
            }
        }

        LogarithmString = "y = " + LogarithmString;

        return LogarithmString;
    }
}
