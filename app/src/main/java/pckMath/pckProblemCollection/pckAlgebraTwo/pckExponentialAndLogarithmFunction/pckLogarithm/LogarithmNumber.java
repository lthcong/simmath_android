package pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckLogarithm;

import pckInfo.InfoCollector;

/**
 * Created by Cong on 8/1/2017.
 */

public class LogarithmNumber {

    //  LOGa(b) = c

    private String BASE, NUMBER, VALUE;
    private final String LOG = "log", LN = "ln";

    public LogarithmNumber(){
        BASE = "1";
        NUMBER = "1";
        VALUE = "0";
    }

    public LogarithmNumber(String Base){
        BASE = Base;
        NUMBER = "1";
        VALUE = "0";
    }

    public LogarithmNumber(int Base){
        BASE = String.valueOf(Base);
        NUMBER = "1";
        VALUE = "0";
    }

    public LogarithmNumber(String Base, String Number){
        BASE = Base;
        NUMBER = Number;
    }

    public LogarithmNumber(int Base, int Number){
        BASE = String.valueOf(Base);
        NUMBER = String.valueOf(Number);
        VALUE = toString();
    }

    public LogarithmNumber(String Base, String Number, String StValue){
        BASE = Base;
        NUMBER = Number;
        VALUE = StValue;
    }

    public LogarithmNumber(int Base, int Number, int StValue){
        BASE = String.valueOf(Base);
        NUMBER = String.valueOf(Number);
        VALUE = String.valueOf(StValue);
    }

    public void setBase(String Base){
        BASE = Base;
    }

    public void setBase(int Base){
        BASE = String.valueOf(Base);
    }

    public int getIntBase(){
        return Integer.parseInt(BASE);
    }

    public String getStringBase(){
        return BASE;
    }

    public void setNumber(String Number){
        NUMBER = Number;
    }

    public void setNumber(int Number){
        NUMBER = String.valueOf(Number);
    }

    public int getIntNumber(){
        return Integer.parseInt(NUMBER);
    }

    public String getStringNumber(){
        return NUMBER;
    }

    public void setValue(String StValue){
        VALUE = StValue;
    }

    public String getValue(){
        return VALUE;
    }

    public String toString(){
        String LogarithmString;
        if (BASE.equalsIgnoreCase("10")){
            LogarithmString = LOG + "(" +  String.valueOf(NUMBER) + ")";
        }
        else {
            if (BASE.equalsIgnoreCase("e")){
                LogarithmString = LN + "(" +  String.valueOf(NUMBER) + ")";
            }
            else {
                LogarithmString = LOG + InfoCollector.getSub(BASE) + "(" +  String.valueOf(NUMBER) + ")";
            }
        }

        return LogarithmString;
    }

}
