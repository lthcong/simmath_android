package pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckExponential;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.LogarithmAndExponentialMath;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckLogarithm.LogarithmNumber;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;
import pckMath.pckProblemCollection.pckIntermediateMath.pckExponential.Exponential;

/**
 * Created by Cong on 8/3/2017.
 */

public class ConvertingBetweenExpAndLog extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public ConvertingBetweenExpAndLog(){
        createProblem();
        swapAnswer();
    }

    public ConvertingBetweenExpAndLog(String Question,
                                          String RightAnswer,
                                          String AnswerA,
                                          String AnswerB,
                                          String AnswerC,
                                          String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 12;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            default:
                createProblem_01();
                break;
        }
        return new ConvertingBetweenExpAndLog(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  EXP -> LOG
    private void createProblem_01(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Exp = MathUtility.getRandomPositiveNumber_1Digit();
        int ExpValue = (new Exponential(Base, Exp)).getValue();

        ExponentialNumber Number = new ExponentialNumber(Base, Exp, ExpValue);
        LogarithmNumber Result = LogarithmAndExponentialMath.convertToLogarithmNumber(Number);

        QUESTION = "Convert to Logarithmic form: " + Number.toString() + " = " + Number.getValue() + ".";
        RIGHT_ANSWER = Result.toString() + " = " + Result.getValue();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + String.valueOf(MathUtility.getRandomPositiveNumber_1Digit());
        ANSWER_C = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + String.valueOf(MathUtility.getRandomPositiveNumber_1Digit());
        ANSWER_D = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + String.valueOf(MathUtility.getRandomPositiveNumber_1Digit());
    }

    //  EXP -> LOG
    private void createProblem_02(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Exp = 2;
        int ExpValue = (new Exponential(Base, Exp)).getValue();

        ExponentialNumber Number = new ExponentialNumber(String.valueOf(ExpValue), InfoCollector.getOneHalf(), String.valueOf(Base));
        LogarithmNumber Result = LogarithmAndExponentialMath.convertToLogarithmNumber(Number);

        QUESTION = "Convert to Logarithmic form: " + Number.toString() + " = " + Number.getValue() + ".";
        RIGHT_ANSWER = Result.toString() + " = " + Result.getValue();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_C = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_D = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
    }

    //  EXP -> LOG
    private void createProblem_03(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Exp = 3;
        int ExpValue = (new Exponential(Base, Exp)).getValue();

        ExponentialNumber Number = new ExponentialNumber(String.valueOf(ExpValue), InfoCollector.getOneThird(), String.valueOf(Base));
        LogarithmNumber Result = LogarithmAndExponentialMath.convertToLogarithmNumber(Number);

        QUESTION = "Convert to Logarithmic form: " + Number.toString() + " = " + Number.getValue() + ".";
        RIGHT_ANSWER = Result.toString() + " = " + Result.getValue();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_C = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_D = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
    }

    //  EXP -> LOG
    private void createProblem_04(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Exp = 4;
        int ExpValue = (new Exponential(Base, Exp)).getValue();

        ExponentialNumber Number = new ExponentialNumber(String.valueOf(ExpValue), InfoCollector.getOneForth(), String.valueOf(Base));
        LogarithmNumber Result = LogarithmAndExponentialMath.convertToLogarithmNumber(Number);

        QUESTION = "Convert to Logarithmic form: " + Number.toString() + " = " + Number.getValue() + ".";
        RIGHT_ANSWER = Result.toString() + " = " + Result.getValue();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_C = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_D = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
    }

    //  EXP -> LOG
    private void createProblem_05(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Exp = 5;
        int ExpValue = (new Exponential(Base, Exp)).getValue();

        ExponentialNumber Number = new ExponentialNumber(String.valueOf(ExpValue), InfoCollector.getOneFifth(), String.valueOf(Base));
        LogarithmNumber Result = LogarithmAndExponentialMath.convertToLogarithmNumber(Number);

        QUESTION = "Convert to Logarithmic form: " + Number.toString() + " = " + Number.getValue() + ".";
        RIGHT_ANSWER = Result.toString() + " = " + Result.getValue();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_C = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_D = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
    }

    //  EXP -> LOG
    private void createProblem_06(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Exp = 10;
        int ExpValue = (new Exponential(Base, Exp)).getValue();

        ExponentialNumber Number = new ExponentialNumber(Base, Exp, ExpValue);
        LogarithmNumber Result = LogarithmAndExponentialMath.convertToLogarithmNumber(Number);

        QUESTION = "Convert to Logarithmic form: " + Number.toString() + " = " + Number.getValue() + ".";
        RIGHT_ANSWER = Result.toString() + " = " + Result.getValue();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_C = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_D = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
    }

    //  EXP -> LOG
    private void createProblem_07(){
        double Base = Math.E;
        int Exp = MathUtility.getRandomPositiveNumber_1Digit();
        double ExpValue = Math.pow(Base, Exp);

        ExponentialNumber Number = new ExponentialNumber("e", String.valueOf(Exp), String.valueOf(ExpValue));
        LogarithmNumber Result = LogarithmAndExponentialMath.convertToLogarithmNumber(Number);

        QUESTION = "Convert to Logarithmic form: " + Number.toString() + " = " + Number.getValue() + ".";
        RIGHT_ANSWER = Result.toString() + " = " + Result.getValue();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_C = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_D = LogarithmAndExponentialMath.getRandomLogarithmicNumber() + " = " + FractionMath.getRandomUnitFraction();
    }

    //  LOG -> EXP
    private void createProblem_08(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Exp = MathUtility.getRandomPositiveNumber_1Digit();
        int ExpValue = (new Exponential(Base, Exp)).getValue();

        ExponentialNumber Result = new ExponentialNumber(Base, Exp, ExpValue);
        LogarithmNumber Number = LogarithmAndExponentialMath.convertToLogarithmNumber(Result);

        QUESTION = "Convert to Exponential form: " + Number.toString() + " = " + Number.getValue() + ".";
        RIGHT_ANSWER = Result.toString() + " = " + Result.getValue();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LogarithmAndExponentialMath.getRandomExponentialNumber() + " = " + String.valueOf(MathUtility.getRandomPositiveNumber_1Digit());
        ANSWER_C = LogarithmAndExponentialMath.getRandomExponentialNumber() + " = " + String.valueOf(MathUtility.getRandomPositiveNumber_1Digit());
        ANSWER_D = LogarithmAndExponentialMath.getRandomExponentialNumber() + " = " + String.valueOf(MathUtility.getRandomPositiveNumber_1Digit());
    }

    //  LOG -> EXP
    private void createProblem_09(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Exp = 2;
        int ExpValue = (new Exponential(Base, Exp)).getValue();

        ExponentialNumber Result = new ExponentialNumber(String.valueOf(ExpValue), InfoCollector.getOneHalf(), String.valueOf(Base));
        LogarithmNumber Number = LogarithmAndExponentialMath.convertToLogarithmNumber(Result);

        QUESTION = "Convert to Exponential form: " + Number.toString() + " = " + Number.getValue() + ".";
        RIGHT_ANSWER = Result.toString() + " = " + Result.getValue();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LogarithmAndExponentialMath.getRandomExponentialNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_C = LogarithmAndExponentialMath.getRandomExponentialNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_D = LogarithmAndExponentialMath.getRandomExponentialNumber() + " = " + FractionMath.getRandomUnitFraction();
    }

    //  LOG -> EXP
    private void createProblem_10(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Exp = 3;
        int ExpValue = (new Exponential(Base, Exp)).getValue();

        ExponentialNumber Number = new ExponentialNumber(String.valueOf(ExpValue), InfoCollector.getOneThird(), String.valueOf(Base));
        LogarithmNumber Result = LogarithmAndExponentialMath.convertToLogarithmNumber(Number);

        QUESTION = "Convert to Exponential form: " + Number.toString() + " = " + Number.getValue() + ".";
        RIGHT_ANSWER = Result.toString() + " = " + Result.getValue();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LogarithmAndExponentialMath.getRandomExponentialNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_C = LogarithmAndExponentialMath.getRandomExponentialNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_D = LogarithmAndExponentialMath.getRandomExponentialNumber() + " = " + FractionMath.getRandomUnitFraction();
    }

    //  LOG -> EXP
    private void createProblem_11(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Exp = 4;
        int ExpValue = (new Exponential(Base, Exp)).getValue();

        ExponentialNumber Number = new ExponentialNumber(String.valueOf(ExpValue), InfoCollector.getOneForth(), String.valueOf(Base));
        LogarithmNumber Result = LogarithmAndExponentialMath.convertToLogarithmNumber(Number);

        QUESTION = "Convert to Exponential form: " + Number.toString() + " = " + Number.getValue() + ".";
        RIGHT_ANSWER = Result.toString() + " = " + Result.getValue();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LogarithmAndExponentialMath.getRandomExponentialNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_C = LogarithmAndExponentialMath.getRandomExponentialNumber() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_D = LogarithmAndExponentialMath.getRandomExponentialNumber() + " = " + FractionMath.getRandomUnitFraction();
    }

    //  LOG -> EXP
    private void createProblem_12(){
        double Base = Math.E;
        int Exp = MathUtility.getRandomPositiveNumber_1Digit();
        double ExpValue = Math.pow(Base, Exp);

        ExponentialNumber Result = new ExponentialNumber("e", String.valueOf(Exp), String.valueOf(ExpValue));
        LogarithmNumber Number = LogarithmAndExponentialMath.convertToLogarithmNumber(Result);

        QUESTION = "Convert to Exponential form: " + Number.toString() + " = " + Number.getValue() + ".";
        RIGHT_ANSWER = Result.toString() + " = " + Result.getValue();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new ExponentialNumber("e", String.valueOf(MathUtility.getRandomPositiveNumber_1Digit()))).toString() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_C = (new ExponentialNumber("e", String.valueOf(MathUtility.getRandomPositiveNumber_1Digit()))).toString() + " = " + FractionMath.getRandomUnitFraction();
        ANSWER_D = (new ExponentialNumber("e", String.valueOf(MathUtility.getRandomPositiveNumber_1Digit()))).toString() + " = " + FractionMath.getRandomUnitFraction();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
