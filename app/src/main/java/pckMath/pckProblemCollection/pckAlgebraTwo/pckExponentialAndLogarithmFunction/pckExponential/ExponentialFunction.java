package pckMath.pckProblemCollection.pckAlgebraTwo.pckExponentialAndLogarithmFunction.pckExponential;

import pckInfo.InfoCollector;

/**
 * Created by Cong on 8/1/2017.
 */

public class ExponentialFunction {

    /*  y = a^x */

    private String BASE, EXPONENT;

    public ExponentialFunction(){
        BASE = "1";
        EXPONENT = "x";
    }

    public ExponentialFunction(int Base){
        BASE = String.valueOf(Base);
        EXPONENT = "x";
    }

    public ExponentialFunction(String Base){
        BASE = Base;
        EXPONENT = "x";
    }

    public ExponentialFunction(int Base, String Exponent){
        BASE = String.valueOf(Base);
        EXPONENT = Exponent;
    }

    public ExponentialFunction(String Base, String Exponent){
        BASE = Base;
        EXPONENT = Exponent;
    }

    public void setBase(int Base){
        BASE = String.valueOf(Base);
    }

    public void setBase(String Base){
        BASE = Base;
    }

    public String getStringBase(){
        return BASE;
    }

    public int getIntBase(){
        return Integer.parseInt(BASE);
    }

    public void setExponent(String Exponent){
        EXPONENT = Exponent;
    }

    public String getExponent(){
        return EXPONENT;
    }

    public String toString(){
        return "y = " + String.valueOf(BASE) + InfoCollector.getExponentSign(EXPONENT);
    }

}
