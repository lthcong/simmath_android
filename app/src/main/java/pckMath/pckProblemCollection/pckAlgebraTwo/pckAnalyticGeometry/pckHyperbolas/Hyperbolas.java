package pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckHyperbolas;

import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Line;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.LineMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 8/22/2017.
 */

public class Hyperbolas {

    //  A_VALUE -> X-AXIS       B_VALUE -> Y-AXIS
    private int A_VALUE, B_VALUE;
    private Point CENTER;
    private boolean IS_VERTICAL;

    public Hyperbolas(){
        A_VALUE = 1;
        B_VALUE = 1;
        CENTER = new Point();

        IS_VERTICAL = false;
    }

    public Hyperbolas(int a, int b){
        A_VALUE = a;
        B_VALUE = b;
        CENTER = new Point();

        IS_VERTICAL = false;
    }

    public Hyperbolas(int a, int b, Point Center){
        A_VALUE = a;
        B_VALUE = b;
        CENTER = Center;

        IS_VERTICAL = false;
    }

    public Hyperbolas(int a, int b, Point Center, boolean isVertical){
        A_VALUE = a;
        B_VALUE = b;
        CENTER = Center;

        IS_VERTICAL = isVertical;
    }

    public void setAValue(int AValue){
        A_VALUE = AValue;
    }

    public int getAValue(){
        return A_VALUE;
    }

    public void setBValue(int BValue){
        B_VALUE = BValue;
    }

    public int getBValue(){
        return B_VALUE;
    }

    public void setCenter(Point Center){
        CENTER = Center;
    }

    public void setCenter(int CenterX, int CenterY){
        CENTER = new Point(CenterX, CenterY);
    }

    public Point getCenter(){
        return CENTER;
    }

    public Point getVertex(){
        Point Vertex;
        if (IS_VERTICAL){
            if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0) {
                Vertex = new Point(CENTER.getXCoordinate(), FractionMath.addFraction(CENTER.getYCoordinate(), new Fraction(B_VALUE)));
            }
            else {
                Vertex = new Point(CENTER.getXCoordinate(), FractionMath.subtractFraction(CENTER.getYCoordinate(), new Fraction(B_VALUE)));
            }
        }
        else {
            if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0) {
                Vertex = new Point(FractionMath.addFraction(CENTER.getXCoordinate(), new Fraction(A_VALUE)), CENTER.getYCoordinate());
            }
            else {
                Vertex = new Point(FractionMath.subtractFraction(CENTER.getXCoordinate(), new Fraction(A_VALUE)), CENTER.getYCoordinate());
            }
        }

        return Vertex;
    }

    public Point getCoVertex(){
        Point CoVertex;
        if (IS_VERTICAL){
            if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0) {
                CoVertex = new Point(FractionMath.addFraction(CENTER.getXCoordinate(), new Fraction(A_VALUE)), CENTER.getYCoordinate());
            }
            else {
                CoVertex = new Point(FractionMath.subtractFraction(CENTER.getXCoordinate(), new Fraction(A_VALUE)), CENTER.getYCoordinate());
            }
        }
        else {
            if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0) {
                CoVertex = new Point(CENTER.getXCoordinate(), FractionMath.addFraction(CENTER.getYCoordinate(), new Fraction(B_VALUE)));
            }
            else {
                CoVertex = new Point(CENTER.getXCoordinate(), FractionMath.subtractFraction(CENTER.getYCoordinate(), new Fraction(B_VALUE)));
            }
        }

        return CoVertex;
    }

    public Point getFocus(){
        Point Focus;
        int c = A_VALUE * A_VALUE + B_VALUE * B_VALUE;
        c = (int) Math.sqrt((double) c);

        if (IS_VERTICAL){
            if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0){
                Focus = new Point(CENTER.getXCoordinate(), FractionMath.addFraction(CENTER.getYCoordinate(), new Fraction(c)));
            }
            else {
                Focus = new Point(CENTER.getXCoordinate(), FractionMath.subtractFraction(CENTER.getYCoordinate(), new Fraction(c)));
            }
        }
        else {
            if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0){
                Focus = new Point(FractionMath.addFraction(CENTER.getXCoordinate(), new Fraction(c)), CENTER.getYCoordinate());
            }
            else {
                Focus = new Point(FractionMath.subtractFraction(CENTER.getXCoordinate(), new Fraction(c)), CENTER.getYCoordinate());
            }
        }

        return Focus;
    }

    public Line getAsymptote(){
        Line Asymptote;
        if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0){
            Asymptote = new Line(CENTER, FractionMath.getSimplifiedFraction(new Fraction(B_VALUE, A_VALUE)));
        }
        else {
            Asymptote = new Line(CENTER, FractionMath.getSimplifiedFraction(new Fraction(0 - B_VALUE, A_VALUE)));
        }

        return LineMath.getPointSlopeToStandardForm(Asymptote);
    }

    public String toString(){
        String HyperbolasString;
        if(IS_VERTICAL){
            HyperbolasString = InfoCollector.getSup("(y - " + CENTER.getYCoordinate().toString() + ")" + InfoCollector.putExponent(2)) + "/" + InfoCollector.getSub(String.valueOf(B_VALUE * B_VALUE));
            HyperbolasString += " - ";
            HyperbolasString += InfoCollector.getSup("(x - " + CENTER.getXCoordinate().toString() + ")" + InfoCollector.putExponent(2)) + "/" + InfoCollector.getSub(String.valueOf(A_VALUE * A_VALUE));
        }
        else {
            HyperbolasString = InfoCollector.getSup("(x - " + CENTER.getXCoordinate().toString() + ")" + InfoCollector.putExponent(2)) + "/" + InfoCollector.getSub(String.valueOf(A_VALUE * A_VALUE));
            HyperbolasString += " - ";
            HyperbolasString += InfoCollector.getSup("(y - " + CENTER.getYCoordinate().toString() + ")" + InfoCollector.putExponent(2)) + "/" + InfoCollector.getSub(String.valueOf(B_VALUE * B_VALUE));
        }

        HyperbolasString += " = 1";

        return HyperbolasString;
    }
}
