package pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckEllipses;

import java.util.ArrayList;

import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 8/21/2017.
 */

public class Ellipses {

    //  A_VALUE -> X-AXIS       B_VALUE -> Y-AXIS
    private int A_VALUE, B_VALUE;
    private Point CENTER;

    public Ellipses(){
        A_VALUE = 2;
        B_VALUE = 1;
        CENTER = new Point();
    }

    public Ellipses(int AValue, int BValue){
        A_VALUE = AValue;
        B_VALUE = BValue;
        CENTER = new Point();
    }

    public Ellipses(int AValue, int BValue, Point Center){
        A_VALUE = AValue;
        B_VALUE = BValue;
        CENTER = Center;
    }

    public void setAValue(int AValue){
        A_VALUE = AValue;
    }

    public int getAValue(){
        return A_VALUE;
    }

    public void setBValue(int BValue){
        B_VALUE = BValue;
    }

    public int getBValue(){
        return B_VALUE;
    }

    public void setCenter(Point Center){
        CENTER = Center;
    }

    public void setCenter(int CenterX, int CenterY){
        CENTER = new Point(CenterX, CenterY);
    }

    public Point getCenter(){
        return CENTER;
    }

    public Point getVertex(){
        Point Vertex;
        int XCenter = CENTER.getXCoordinate().getNumerator();
        int YCenter = CENTER.getYCoordinate().getNumerator();

        if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0){
            Vertex = new Point(XCenter + A_VALUE, YCenter);
        }
        else {
            Vertex = new Point(XCenter - A_VALUE, YCenter);
        }

        return Vertex;
    }

    public Point getCoVertex(){
        Point CoVertex;
        int XCenter = CENTER.getXCoordinate().getNumerator();
        int YCenter = CENTER.getYCoordinate().getNumerator();

        if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0){
            CoVertex = new Point(XCenter, YCenter + B_VALUE);
        }
        else {
            CoVertex = new Point(XCenter, YCenter - B_VALUE);
        }

        return CoVertex;
    }

    public Point getFocus(){
        Point Focus;
        int a, b, c;
        if (A_VALUE > B_VALUE){
            a = A_VALUE;
            b = B_VALUE;
            c = a * a - b * b;
            c = (int) Math.sqrt((double) c);

            if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0){
                Focus = new Point(FractionMath.addFraction(CENTER.getXCoordinate(), new Fraction(c)), CENTER.getYCoordinate());
            }
            else {
                Focus = new Point(FractionMath.subtractFraction(CENTER.getXCoordinate(), new Fraction(c)), CENTER.getYCoordinate());
            }
        }
        else {
            a = B_VALUE;
            b = A_VALUE;
            c = a * a - b * b;
            c = (int) Math.sqrt((double) c);

            if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0) {
                Focus = new Point(CENTER.getXCoordinate(), FractionMath.addFraction(CENTER.getYCoordinate(), new Fraction(c)));
            }
            else {
                Focus = new Point(CENTER.getXCoordinate(), FractionMath.subtractFraction(CENTER.getYCoordinate(), new Fraction(c)));
            }
        }

        return Focus;
    }

    public int getMajorLength(){
        int MajorLength = 0;
        if (A_VALUE > B_VALUE){
            MajorLength = A_VALUE * 2;
        }
        else {
            MajorLength = B_VALUE * 2;
        }

        return MajorLength;
    }

    public int getMinorLength(){
        int MinorLength = 0;
        if (A_VALUE > B_VALUE){
            MinorLength = B_VALUE * 2;
        }
        else {
            MinorLength = A_VALUE * 2;
        }

        return MinorLength;
    }

    public String toString(){
        String EllipsesString;
        EllipsesString = InfoCollector.getSup("(x - " + CENTER.getXCoordinate().toString() + ")" + InfoCollector.putExponent(2)) + "/" + InfoCollector.getSub(String.valueOf(A_VALUE * A_VALUE));
        EllipsesString += " + ";
        EllipsesString += InfoCollector.getSup("(y - " + CENTER.getYCoordinate().toString() + ")" + InfoCollector.putExponent(2)) + "/" + InfoCollector.getSub(String.valueOf(B_VALUE * B_VALUE));
        EllipsesString += " = 1";

        return EllipsesString;
    }
}
