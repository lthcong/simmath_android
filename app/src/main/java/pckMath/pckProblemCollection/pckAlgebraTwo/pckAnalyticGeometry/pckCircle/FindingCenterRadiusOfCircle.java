package pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckCircle;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.LineMath;

/**
 * Created by Cong on 8/20/2017.
 */

public class FindingCenterRadiusOfCircle extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingCenterRadiusOfCircle(){
        createProblem();
        swapAnswer();
    }

    public FindingCenterRadiusOfCircle(String Question,
                        String RightAnswer,
                        String AnswerA,
                        String AnswerB,
                        String AnswerC,
                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }
        return new FindingCenterRadiusOfCircle(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        Circle _Circle = new Circle(0, 0, MathUtility.getRandomPositiveNumber_1Digit());

        QUESTION = "Find the center, and the radius of the circle:<br>" + _Circle.toStandardString();
        RIGHT_ANSWER = _Circle.getCenter().toString() + ", " + _Circle.getRadius();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_C = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_D = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
    }

    private void createProblem_02(){
        Circle _Circle = new Circle(0, 0, MathUtility.getRandomPositiveNumber_1Digit());

        QUESTION = "Find the center, and the radius of the circle:<br>" + _Circle.toGeneralString();
        RIGHT_ANSWER = _Circle.getCenter().toString() + ", " + _Circle.getRadius();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_C = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_D = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
    }

    private void createProblem_03(){
        Circle _Circle = new Circle(MathUtility.getRandomNumber_1Digit(), 0, MathUtility.getRandomPositiveNumber_1Digit());

        QUESTION = "Find the center, and the radius of the circle:<br>" + _Circle.toStandardString();
        RIGHT_ANSWER = _Circle.getCenter().toString() + ", " + _Circle.getRadius();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_C = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_D = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
    }

    private void createProblem_04(){
        Circle _Circle = new Circle(MathUtility.getRandomNumber_1Digit(), 0, MathUtility.getRandomPositiveNumber_1Digit());

        QUESTION = "Find the center, and the radius of the circle:<br>" + _Circle.toGeneralString();
        RIGHT_ANSWER = _Circle.getCenter().toString() + ", " + _Circle.getRadius();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_C = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_D = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
    }

    private void createProblem_05(){
        Circle _Circle = new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit());

        QUESTION = "Find the center, and the radius of the circle:<br>" + _Circle.toStandardString();
        RIGHT_ANSWER = _Circle.getCenter().toString() + ", " + _Circle.getRadius();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_C = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_D = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
    }

    private void createProblem_06(){
        Circle _Circle = new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit());

        QUESTION = "Find the center, and the radius of the circle:<br>" + _Circle.toGeneralString();
        RIGHT_ANSWER = _Circle.getCenter().toString() + ", " + _Circle.getRadius();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_C = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_D = LineMath.getRandomPoint().toString() + ", " + MathUtility.getRandomPositiveNumber_1Digit();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
