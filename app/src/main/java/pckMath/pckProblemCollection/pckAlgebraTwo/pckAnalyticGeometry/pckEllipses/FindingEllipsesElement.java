package pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckEllipses;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.LineMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;

/**
 * Created by Cong on 8/21/2017.
 */

public class FindingEllipsesElement extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingEllipsesElement(){
        createProblem();
        swapAnswer();
    }

    public FindingEllipsesElement(String Question,
                        String RightAnswer,
                        String AnswerA,
                        String AnswerB,
                        String AnswerC,
                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }
        return new FindingEllipsesElement(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  CENTER
    private void createProblem_01(){
        int AValue = MathUtility.getRandomPositiveNumber_1Digit(), BValue = MathUtility.getRandomPositiveNumber_1Digit();
        if (AValue == BValue){
            createProblem_01();
        }

        Point Center = LineMath.getRandomPoint();

        Ellipses Problem = new Ellipses(AValue, BValue, Center);

        QUESTION = "Find the center of the ellipses:<br>" + Problem.toString();
        RIGHT_ANSWER = Problem.getCenter().toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString();
        ANSWER_C = LineMath.getRandomPoint().toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  VERTEX
    private void createProblem_02(){
        int AValue = MathUtility.getRandomPositiveNumber_1Digit(), BValue = MathUtility.getRandomPositiveNumber_1Digit();
        if (AValue == BValue){
            createProblem_02();
        }

        Point Center = LineMath.getRandomPoint();

        Ellipses Problem = new Ellipses(AValue, BValue, Center);

        QUESTION = "Find the vertex of the ellipses:<br>" + Problem.toString();
        RIGHT_ANSWER = Problem.getVertex().toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString();
        ANSWER_C = LineMath.getRandomPoint().toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  FOCUS
    private void createProblem_03(){
        int AValue = 5, BValue = 4;
        Point Center = LineMath.getRandomPoint();

        Ellipses Problem = new Ellipses(AValue, BValue, Center);

        QUESTION = "Find the focus of the ellipses:<br>" + Problem.toString();
        RIGHT_ANSWER = Problem.getFocus().toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString();
        ANSWER_C = LineMath.getRandomPoint().toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  FOCUS
    private void createProblem_04(){
        int AValue = 3, BValue = 5;
        Point Center = LineMath.getRandomPoint();

        Ellipses Problem = new Ellipses(AValue, BValue, Center);

        QUESTION = "Find the focus of the ellipses:<br>" + Problem.toString();
        RIGHT_ANSWER = Problem.getFocus().toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString();
        ANSWER_C = LineMath.getRandomPoint().toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  FOCUS
    private void createProblem_05(){
        int AValue = 10, BValue = 6;
        Point Center = LineMath.getRandomPoint();

        Ellipses Problem = new Ellipses(AValue, BValue, Center);

        QUESTION = "Find the focus of the ellipses:<br>" + Problem.toString();
        RIGHT_ANSWER = Problem.getFocus().toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString();
        ANSWER_C = LineMath.getRandomPoint().toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  FOCUS
    private void createProblem_06(){
        int AValue = 8, BValue = 10;
        Point Center = LineMath.getRandomPoint();

        Ellipses Problem = new Ellipses(AValue, BValue, Center);

        QUESTION = "Find the focus of the ellipses:<br>" + Problem.toString();
        RIGHT_ANSWER = Problem.getFocus().toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString();
        ANSWER_C = LineMath.getRandomPoint().toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
