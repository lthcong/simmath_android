package pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckCircle.*;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckEllipses.*;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckHyperbolas.*;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckParabolas.*;

/**
 * Created by Cong on 9/6/2017.
 */

public class ReviewAnalyticGeometry extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public ReviewAnalyticGeometry(){
        createProblem();
        swapAnswer();
    }

    public ReviewAnalyticGeometry(String Question,
                              String RightAnswer,
                              String AnswerA,
                              String AnswerB,
                              String AnswerC,
                              String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        MathProblem Problem;
        int NoProblem = 9;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                Problem = new FindingEquationOfACircle();
                break;
            case 1:
                Problem = new ConvertingCircleEquation();
                break;
            case 2:
                Problem = new FindingTheTangentOfACircle();
                break;
            case 3:
                Problem = new FindingParabolasElement();
                break;
            case 4:
                Problem = new FindingTheEquationOfAParabolas();
                break;
            case 5:
                Problem = new FindingEllipsesElement();
                break;
            case 6:
                Problem = new FindingEquationOfAnEllipses();
                break;
            case 7:
                Problem = new FindingHyperbolasElement();
                break;
            case 8:
                Problem = new FindingHyperbolasEquation();
                break;
            default:
                Problem = new FindingEquationOfACircle();
                break;
        }

        QUESTION = Problem.getQuestion();
        RIGHT_ANSWER = Problem.getRightAnswer();
        ANSWER_A = Problem.getAnswerA();
        ANSWER_B = Problem.getAnswerB();
        ANSWER_C = Problem.getAnswerC();
        ANSWER_D = Problem.getAnswerD();

        return new ReviewAnalyticGeometry(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
