package pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckCircle;

import pckInfo.InfoCollector;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;

/**
 * Created by Cong on 8/20/2017.
 */

public class Circle {

    private int CENTER_X, CENTER_Y, RADIUS;

    public Circle(){
        CENTER_X = 0;
        CENTER_Y = 0;
        RADIUS = 1;
    }

    public Circle(int CenterX, int CenterY, int Radius){
        CENTER_X = CenterX;
        CENTER_Y = CenterY;
        RADIUS = Radius;
    }

    public void setCenter(int CenterX, int CenterY){
        CENTER_X = CenterX;
        CENTER_Y = CenterY;
    }

    public int getCenterX(){
        return CENTER_X;
    }

    public int getCenterY(){
        return CENTER_Y;
    }

    public Point getCenter(){
        return new Point(CENTER_X, CENTER_Y);
    }

    public void setRadius(int Radius){
        RADIUS = Radius;
    }

    public int getRadius(){
        return RADIUS;
    }

    public String toStandardString(){
        String StandardString;

        // CENTER X
        if (CENTER_X == 0){
            StandardString = "x" + InfoCollector.getSup("2");
        }
        else {
            StandardString = "(x - " + String.valueOf(CENTER_X) + ")" + InfoCollector.getSup("2");
        }

        // CENTER Y
        if (CENTER_Y == 0){
            StandardString += "+ y" + InfoCollector.getSup("2");
        }
        else {
            StandardString += "+ (y - " + String.valueOf(CENTER_Y) + ")" + InfoCollector.getSup("2");
        }

        //  RADIUS
        StandardString += " = " + String.valueOf((RADIUS * RADIUS));

        return StandardString;
    }

    public String toGeneralString(){
        String GeneralString = "";

        // CENTER X
        if (CENTER_X == 0){
            GeneralString = "x" + InfoCollector.getSup("2");
        }
        else {
            GeneralString = "x" + InfoCollector.getSup("2") + " - " + String.valueOf((2 * CENTER_X)) + "x";
        }

        // CENTER Y
        if (CENTER_Y == 0){
            GeneralString += "+ y" + InfoCollector.getSup("2");
        }
        else {
            GeneralString += "+ y" + InfoCollector.getSup("2") + " - " + String.valueOf((2 * CENTER_Y)) + "y";
        }

        //  RADIUS
        GeneralString += " + " + String.valueOf((CENTER_X * CENTER_X + CENTER_Y * CENTER_Y - RADIUS * RADIUS));

        GeneralString += " = 0";

        return GeneralString;
    }

    public String toString(){
        return toStandardString();
    }

}
