package pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckCircle;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;

/**
 * Created by Cong on 8/20/2017.
 */

public class FindingEquationOfACircle extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingEquationOfACircle(){
        createProblem();
        swapAnswer();
    }

    public FindingEquationOfACircle(String Question,
                        String RightAnswer,
                        String AnswerA,
                        String AnswerB,
                        String AnswerC,
                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }
        return new FindingEquationOfACircle(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        Circle _Circle = new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit());

        QUESTION = "Find the equation of the circle.<br>Given the center is " + _Circle.getCenter().toString()
                + "<br>and the radius is " + _Circle.getRadius() + ".";
        RIGHT_ANSWER = _Circle.toStandardString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit())).toStandardString();
        ANSWER_C = (new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit())).toStandardString();
        ANSWER_D = (new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit())).toStandardString();
    }

    private void createProblem_02(){
        Circle _Circle = new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit());

        QUESTION = "Find the equation of the circle.<br>Given the center is " + _Circle.getCenter().toString()
                + "<br>and the radius is " + _Circle.getRadius() + ".";
        RIGHT_ANSWER = _Circle.toGeneralString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit())).toGeneralString();
        ANSWER_C = (new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit())).toGeneralString();
        ANSWER_D = (new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit())).toGeneralString();
    }

    private void createProblem_03(){
        int CenterX = MathUtility.getRandomNumber_1Digit();
        int CenterY = MathUtility.getRandomNumber_1Digit();
        int X_ADD = 3, Y_ADD = 4;
        int Radius = 5;

        Circle _Circle = new Circle(CenterX, CenterY, Radius);

        Point RandomPoint = new Point(CenterX + X_ADD, CenterY + Y_ADD);

        QUESTION = "Find the equation of the circle.<br>Given the center is " + (new Point(CenterX, CenterY)).toString()
                + "<br>and pass through the point " + RandomPoint.toString() + ".";
        RIGHT_ANSWER = _Circle.toStandardString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), Radius).toStandardString();
        ANSWER_C = new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), Radius).toStandardString();
        ANSWER_D = new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), Radius).toStandardString();
    }

    private void createProblem_04(){
        int CenterX = MathUtility.getRandomNumber_1Digit();
        int CenterY = MathUtility.getRandomNumber_1Digit();
        int X_ADD = 3, Y_ADD = -4;
        int Radius = 5;

        Circle _Circle = new Circle(CenterX, CenterY, Radius);

        Point RandomPoint = new Point(CenterX + X_ADD, CenterY + Y_ADD);

        QUESTION = "Find the equation of the circle.<br>Given the center is " + (new Point(CenterX, CenterY)).toString()
                + "<br>and pass through the point " + RandomPoint.toString() + ".";
        RIGHT_ANSWER = _Circle.toGeneralString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), Radius).toGeneralString();
        ANSWER_C = new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), Radius).toGeneralString();
        ANSWER_D = new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), Radius).toGeneralString();
    }

    private void createProblem_05(){
        int CenterX = MathUtility.getRandomNumber_1Digit();
        int CenterY = MathUtility.getRandomNumber_1Digit();
        int X_ADD = 6, Y_ADD = -8;
        int Radius = 10;

        Circle _Circle = new Circle(CenterX, CenterY, Radius);

        Point RandomPoint = new Point(CenterX + X_ADD, CenterY + Y_ADD);

        QUESTION = "Find the equation of the circle.<br>Given the center is " + (new Point(CenterX, CenterY)).toString()
                + "<br>and pass through the point " + RandomPoint.toString() + ".";
        RIGHT_ANSWER = _Circle.toStandardString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), Radius).toStandardString();
        ANSWER_C = new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), Radius).toStandardString();
        ANSWER_D = new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), Radius).toStandardString();
    }

    private void createProblem_06(){
        int CenterX = MathUtility.getRandomNumber_1Digit();
        int CenterY = MathUtility.getRandomNumber_1Digit();
        int X_ADD = -6, Y_ADD = 8;
        int Radius = 10;

        Circle _Circle = new Circle(CenterX, CenterY, Radius);

        Point RandomPoint = new Point(CenterX + X_ADD, CenterY + Y_ADD);

        QUESTION = "Find the equation of the circle.<br>Given the center is " + (new Point(CenterX, CenterY)).toString()
                + "<br>and pass through the point " + RandomPoint.toString() + ".";
        RIGHT_ANSWER = _Circle.toGeneralString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), Radius).toGeneralString();
        ANSWER_C = new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), Radius).toGeneralString();
        ANSWER_D = new Circle(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), Radius).toGeneralString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
