package pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckCircle;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Line;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.LineMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 8/20/2017.
 */

public class FindingTheTangentOfACircle extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingTheTangentOfACircle(){
        createProblem();
        swapAnswer();
    }

    public FindingTheTangentOfACircle(String Question,
                        String RightAnswer,
                        String AnswerA,
                        String AnswerB,
                        String AnswerC,
                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 8;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }
        return new FindingTheTangentOfACircle(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int CenterX = MathUtility.getRandomNumber_1Digit();
        int CenterY = MathUtility.getRandomNumber_1Digit();
        int X_ADD = 3, Y_ADD = 4;
        int Radius = 5;

        Circle _Circle = new Circle(CenterX, CenterY, Radius);

        Point Center = new Point(CenterX, CenterY);
        Point RandomPoint = new Point(CenterX + X_ADD, CenterY + Y_ADD);

        Fraction Slope = LineMath.getSlope(Center, RandomPoint);
        Slope = FractionMath.divideFraction(new Fraction(1, 1), Slope);
        Line Tangent = LineMath.getPointSlopeToStandardForm(new Line(RandomPoint, Slope));

        QUESTION = "Find the tangent of the circle <br>" + _Circle.toStandardString()
                + "<br>at the point " + RandomPoint.toString() + ".";
        RIGHT_ANSWER = Tangent.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomLine().toString();
        ANSWER_C = LineMath.getRandomLine().toString();
        ANSWER_D = LineMath.getRandomLine().toString();
    }

    private void createProblem_02(){
        int CenterX = MathUtility.getRandomNumber_1Digit();
        int CenterY = MathUtility.getRandomNumber_1Digit();
        int X_ADD = -3, Y_ADD = 4;
        int Radius = 5;

        Circle _Circle = new Circle(CenterX, CenterY, Radius);

        Point Center = new Point(CenterX, CenterY);
        Point RandomPoint = new Point(CenterX + X_ADD, CenterY + Y_ADD);

        Fraction Slope = LineMath.getSlope(Center, RandomPoint);
        Slope = FractionMath.divideFraction(new Fraction(1, 1), Slope);
        Line Tangent = LineMath.getPointSlopeToStandardForm(new Line(RandomPoint, Slope));

        QUESTION = "Find the tangent of the circle <br>" + _Circle.toGeneralString()
                + "<br>at the point " + RandomPoint.toString() + ".";
        RIGHT_ANSWER = Tangent.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomLine().toString();
        ANSWER_C = LineMath.getRandomLine().toString();
        ANSWER_D = LineMath.getRandomLine().toString();
    }

    private void createProblem_03(){
        int CenterX = MathUtility.getRandomNumber_1Digit();
        int CenterY = MathUtility.getRandomNumber_1Digit();
        int X_ADD = 3, Y_ADD = -4;
        int Radius = 5;

        Circle _Circle = new Circle(CenterX, CenterY, Radius);

        Point Center = new Point(CenterX, CenterY);
        Point RandomPoint = new Point(CenterX + X_ADD, CenterY + Y_ADD);

        Fraction Slope = LineMath.getSlope(Center, RandomPoint);
        Slope = FractionMath.divideFraction(new Fraction(1, 1), Slope);
        Line Tangent = LineMath.getPointSlopeToStandardForm(new Line(RandomPoint, Slope));

        QUESTION = "Find the tangent of the circle <br>" + _Circle.toStandardString()
                + "<br>at the point " + RandomPoint.toString() + ".";
        RIGHT_ANSWER = Tangent.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomLine().toString();
        ANSWER_C = LineMath.getRandomLine().toString();
        ANSWER_D = LineMath.getRandomLine().toString();
    }

    private void createProblem_04(){
        int CenterX = MathUtility.getRandomNumber_1Digit();
        int CenterY = MathUtility.getRandomNumber_1Digit();
        int X_ADD = -3, Y_ADD = -4;
        int Radius = 5;

        Circle _Circle = new Circle(CenterX, CenterY, Radius);

        Point Center = new Point(CenterX, CenterY);
        Point RandomPoint = new Point(CenterX + X_ADD, CenterY + Y_ADD);

        Fraction Slope = LineMath.getSlope(Center, RandomPoint);
        Slope = FractionMath.divideFraction(new Fraction(1, 1), Slope);
        Line Tangent = LineMath.getPointSlopeToStandardForm(new Line(RandomPoint, Slope));

        QUESTION = "Find the tangent of the circle <br>" + _Circle.toGeneralString()
                + "<br>at the point " + RandomPoint.toString() + ".";
        RIGHT_ANSWER = Tangent.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomLine().toString();
        ANSWER_C = LineMath.getRandomLine().toString();
        ANSWER_D = LineMath.getRandomLine().toString();
    }

    private void createProblem_05(){
        int CenterX = MathUtility.getRandomNumber_1Digit();
        int CenterY = MathUtility.getRandomNumber_1Digit();
        int X_ADD = 6, Y_ADD = 8;
        int Radius = 10;

        Circle _Circle = new Circle(CenterX, CenterY, Radius);

        Point Center = new Point(CenterX, CenterY);
        Point RandomPoint = new Point(CenterX + X_ADD, CenterY + Y_ADD);

        Fraction Slope = LineMath.getSlope(Center, RandomPoint);
        Slope = FractionMath.divideFraction(new Fraction(1, 1), Slope);
        Line Tangent = LineMath.getPointSlopeToStandardForm(new Line(RandomPoint, Slope));

        QUESTION = "Find the tangent of the circle <br>" + _Circle.toStandardString()
                + "<br>at the point " + RandomPoint.toString() + ".";
        RIGHT_ANSWER = Tangent.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomLine().toString();
        ANSWER_C = LineMath.getRandomLine().toString();
        ANSWER_D = LineMath.getRandomLine().toString();
    }

    private void createProblem_06(){
        int CenterX = MathUtility.getRandomNumber_1Digit();
        int CenterY = MathUtility.getRandomNumber_1Digit();
        int X_ADD = -6, Y_ADD = 8;
        int Radius = 10;

        Circle _Circle = new Circle(CenterX, CenterY, Radius);

        Point Center = new Point(CenterX, CenterY);
        Point RandomPoint = new Point(CenterX + X_ADD, CenterY + Y_ADD);

        Fraction Slope = LineMath.getSlope(Center, RandomPoint);
        Slope = FractionMath.divideFraction(new Fraction(1, 1), Slope);
        Line Tangent = LineMath.getPointSlopeToStandardForm(new Line(RandomPoint, Slope));

        QUESTION = "Find the tangent of the circle <br>" + _Circle.toGeneralString()
                + "<br>at the point " + RandomPoint.toString() + ".";
        RIGHT_ANSWER = Tangent.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomLine().toString();
        ANSWER_C = LineMath.getRandomLine().toString();
        ANSWER_D = LineMath.getRandomLine().toString();
    }

    private void createProblem_07(){
        int CenterX = MathUtility.getRandomNumber_1Digit();
        int CenterY = MathUtility.getRandomNumber_1Digit();
        int X_ADD = 6, Y_ADD = -8;
        int Radius = 10;

        Circle _Circle = new Circle(CenterX, CenterY, Radius);

        Point Center = new Point(CenterX, CenterY);
        Point RandomPoint = new Point(CenterX + X_ADD, CenterY + Y_ADD);

        Fraction Slope = LineMath.getSlope(Center, RandomPoint);
        Slope = FractionMath.divideFraction(new Fraction(1, 1), Slope);
        Line Tangent = LineMath.getPointSlopeToStandardForm(new Line(RandomPoint, Slope));

        QUESTION = "Find the tangent of the circle <br>" + _Circle.toStandardString()
                + "<br>at the point " + RandomPoint.toString() + ".";
        RIGHT_ANSWER = Tangent.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomLine().toString();
        ANSWER_C = LineMath.getRandomLine().toString();
        ANSWER_D = LineMath.getRandomLine().toString();
    }

    private void createProblem_08(){
        int CenterX = MathUtility.getRandomNumber_1Digit();
        int CenterY = MathUtility.getRandomNumber_1Digit();
        int X_ADD = -6, Y_ADD = -8;
        int Radius = 10;

        Circle _Circle = new Circle(CenterX, CenterY, Radius);

        Point Center = new Point(CenterX, CenterY);
        Point RandomPoint = new Point(CenterX + X_ADD, CenterY + Y_ADD);

        Fraction Slope = LineMath.getSlope(Center, RandomPoint);
        Slope = FractionMath.divideFraction(new Fraction(1, 1), Slope);
        Line Tangent = LineMath.getPointSlopeToStandardForm(new Line(RandomPoint, Slope));

        QUESTION = "Find the tangent of the circle <br>" + _Circle.toGeneralString()
                + "<br>at the point " + RandomPoint.toString() + ".";
        RIGHT_ANSWER = Tangent.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomLine().toString();
        ANSWER_C = LineMath.getRandomLine().toString();
        ANSWER_D = LineMath.getRandomLine().toString();
    }
    
    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
