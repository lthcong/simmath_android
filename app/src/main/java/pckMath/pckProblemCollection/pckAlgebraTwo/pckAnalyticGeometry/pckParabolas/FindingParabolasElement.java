package pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckParabolas;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.LineMath;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 8/21/2017.
 */

public class FindingParabolasElement extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingParabolasElement(){
        createProblem();
        swapAnswer();
    }

    public FindingParabolasElement(String Question,
                        String RightAnswer,
                        String AnswerA,
                        String AnswerB,
                        String AnswerC,
                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 2;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }
        return new FindingParabolasElement(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  AXIS OF SYMMETRY
    private void createProblem_01(){
        Parabolas Problem = new Parabolas(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit());

        QUESTION = "Find the axis of symmetry of the parabolas: <br>" + Problem.toString();
        RIGHT_ANSWER = Problem.getAxisOfSymmetry();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + FractionMath.getRandomFraction().toString();
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString();
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  AXIS OF SYMMETRY
    private void createProblem_02(){
        Parabolas Problem = new Parabolas(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), false);

        QUESTION = "Find the axis of symmetry of the parabolas: <br>" + Problem.toString();
        RIGHT_ANSWER = Problem.getAxisOfSymmetry();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = " + FractionMath.getRandomFraction().toString();
        ANSWER_C = "y = " + FractionMath.getRandomFraction().toString();
        ANSWER_D = "y = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  VERTEX
    private void createProblem_03(){
        Parabolas Problem = new Parabolas(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit());

        QUESTION = "Find the vertex of the parabolas: <br>" + Problem.toString();
        RIGHT_ANSWER = Problem.getVertex().toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString();
        ANSWER_C = LineMath.getRandomPoint().toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  VERTEX
    private void createProblem_04(){
        Parabolas Problem = new Parabolas(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), false);

        QUESTION = "Find the vertex of the parabolas: <br>" + Problem.toString();
        RIGHT_ANSWER = Problem.getVertex().toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString();
        ANSWER_C = LineMath.getRandomPoint().toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  FOCUS
    private void createProblem_05(){
        Parabolas Problem = new Parabolas(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit());

        QUESTION = "Find the focus of the parabolas: <br>" + Problem.toString();
        RIGHT_ANSWER = Problem.getFocus().toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString();
        ANSWER_C = LineMath.getRandomPoint().toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  FOCUS
    private void createProblem_06(){
        Parabolas Problem = new Parabolas(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), false);

        QUESTION = "Find the focus of the parabolas: <br>" + Problem.toString();
        RIGHT_ANSWER = Problem.getFocus().toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString();
        ANSWER_C = LineMath.getRandomPoint().toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  DIRECTRIX
    private void createProblem_07(){
        Parabolas Problem = new Parabolas(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit());

        QUESTION = "Find the directrix of the parabolas: <br>" + Problem.toString();
        RIGHT_ANSWER = Problem.getDirectrix();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "y = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "y = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  DIRECTRIX
    private void createProblem_08(){
        Parabolas Problem = new Parabolas(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), false);

        QUESTION = "Find the directrix of the parabolas: <br>" + Problem.toString();
        RIGHT_ANSWER = Problem.getDirectrix();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
