package pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckParabolas;

import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 8/21/2017.
 */

public class Parabolas {

    private Fraction H_VALUE, P_VALUE, K_VALUE;
    private boolean IS_VERTICAL_PARABOLAS;

    public Parabolas(){
        H_VALUE = new Fraction(1);
        P_VALUE = new Fraction(1);
        K_VALUE = new Fraction(1);

        IS_VERTICAL_PARABOLAS = true;
    }

    public Parabolas(int h, int p, int k){
        H_VALUE = new Fraction(h);
        P_VALUE = new Fraction(p);
        K_VALUE = new Fraction(k);

        IS_VERTICAL_PARABOLAS = true;
    }

    public Parabolas(Fraction h, Fraction p, Fraction k){
        H_VALUE = h;
        P_VALUE = p;
        K_VALUE = k;

        IS_VERTICAL_PARABOLAS = true;
    }

    public Parabolas(int h, int p, int k, boolean isVerticalParabolas){
        H_VALUE = new Fraction(h);
        P_VALUE = new Fraction(p);
        K_VALUE = new Fraction(k);

        IS_VERTICAL_PARABOLAS = isVerticalParabolas;
    }

    public Parabolas(Fraction h, Fraction p, Fraction k, boolean isVerticalParabolas){
        H_VALUE = h;
        P_VALUE = p;
        K_VALUE = k;

        IS_VERTICAL_PARABOLAS = isVerticalParabolas;
    }

    public void setHValue(int h){
        H_VALUE = new Fraction(h);
    }
    
    public void setHValue(Fraction h){
        H_VALUE = h;
    }
    
    public Fraction getHValue(){
        return H_VALUE;
    }

    public void setPValue(int p){
        P_VALUE = new Fraction(p);
    }

    public void setPValue(Fraction p){
        P_VALUE = p;
    }

    public Fraction getPValue(){
        return P_VALUE;
    }

    public void setKValue(int k){
        K_VALUE = new Fraction(k);
    }

    public void setKValue(Fraction k){
        K_VALUE = k;
    }

    public Fraction getKValue(){
        return K_VALUE;
    }

    public String getAxisOfSymmetry(){
        String AxisOfSymmetry = "";
        if (IS_VERTICAL_PARABOLAS){
            AxisOfSymmetry = "x = " + H_VALUE.toString();
        }
        else {
            AxisOfSymmetry = "y = " + K_VALUE.toString();
        }

        return AxisOfSymmetry;
    }

    public Point getVertex(){
        Point Vertex;
        if (IS_VERTICAL_PARABOLAS){
            Vertex = new Point(H_VALUE, K_VALUE);
        }
        else {
            Vertex = new Point(H_VALUE, K_VALUE);
        }

        return Vertex;
    }

    public Point getFocus(){
        Point Vertex;
        if (IS_VERTICAL_PARABOLAS){
            Vertex = new Point(H_VALUE, FractionMath.addFraction(H_VALUE, P_VALUE));
        }
        else {
            Vertex = new Point(FractionMath.addFraction(H_VALUE, P_VALUE), K_VALUE);
        }

        return Vertex;
    }

    public String getDirectrix(){
        String Directrix = "";
        if (IS_VERTICAL_PARABOLAS){
            Directrix = "y = " + FractionMath.subtractFraction(K_VALUE, P_VALUE).toString();
        }
        else {
            Directrix = "x = " + FractionMath.subtractFraction(H_VALUE, P_VALUE).toString();
        }

        return Directrix;
    }

    public Point getRandomPoint(){
        Fraction XValue = new Fraction(MathUtility.getRandomNumber_1Digit());

        Fraction YValue = FractionMath.subtractFraction(XValue, H_VALUE);
        YValue = FractionMath.multiplyFraction(YValue, YValue);
        YValue = FractionMath.divideFraction(YValue, FractionMath.multiplyFraction(new Fraction(4), P_VALUE));
        YValue = FractionMath.addFraction(YValue, K_VALUE);

        return new Point(XValue, YValue);
    }

    public String toString(){
        String ParabolasString = "";
        if (IS_VERTICAL_PARABOLAS){
            ParabolasString = "(x - " + H_VALUE.toString() + ")" + InfoCollector.getSup("2") + " = "
                    + FractionMath.multiplyFraction(new Fraction(4), P_VALUE) + "(y - " + K_VALUE.toString() + ")";
        }
        else {
            ParabolasString = "(y - " + K_VALUE.toString() + ")" + InfoCollector.getSup("2") + " = "
                    + FractionMath.multiplyFraction(new Fraction(4), P_VALUE) + "(x - " + H_VALUE.toString() + ")";
        }

        return ParabolasString;
    }
}
