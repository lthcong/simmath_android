package pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckParabolas;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;

/**
 * Created by Cong on 8/21/2017.
 */

public class FindingTheEquationOfAParabolas extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingTheEquationOfAParabolas(){
        createProblem();
        swapAnswer();
    }

    public FindingTheEquationOfAParabolas(String Question,
                        String RightAnswer,
                        String AnswerA,
                        String AnswerB,
                        String AnswerC,
                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 2;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }
        return new FindingTheEquationOfAParabolas(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        boolean isVertical;
        if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0){
            isVertical = true;
        }
        else {
            isVertical = false;
        }

        Parabolas Problem = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical);

        QUESTION = "Find the equation of the parabolas.<br>Given the vertex is " + Problem.getVertex().toString()
                + "<br>and pass through the point " + Problem.getRandomPoint().toString() + ".";
        RIGHT_ANSWER = Problem.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
        ANSWER_C = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
        ANSWER_D = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
    }

    private void createProblem_02(){
        boolean isVertical;
        if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0){
            isVertical = true;
        }
        else {
            isVertical = false;
        }

        Parabolas Problem = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical);

        QUESTION = "Find the equation of the parabolas.<br>Given the vertex is " + Problem.getVertex().toString()
                + "<br>and the axis of symmetry " + Problem.getAxisOfSymmetry() + ".";
        RIGHT_ANSWER = Problem.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
        ANSWER_C = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
        ANSWER_D = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
    }

    private void createProblem_03(){
        boolean isVertical;
        if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0){
            isVertical = true;
        }
        else {
            isVertical = false;
        }

        Parabolas Problem = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical);

        QUESTION = "Find the equation of the parabolas.<br>Given the vertex is " + Problem.getVertex().toString()
                + "<br>and the focus " + Problem.getFocus().toString() + ".";
        RIGHT_ANSWER = Problem.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
        ANSWER_C = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
        ANSWER_D = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
    }

    private void createProblem_04(){
        boolean isVertical;
        if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0){
            isVertical = true;
        }
        else {
            isVertical = false;
        }

        Parabolas Problem = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical);

        QUESTION = "Find the equation of the parabolas.<br>Given the vertex is " + Problem.getVertex().toString()
                + "<br>and the directrix " + Problem.getDirectrix() + ".";
        RIGHT_ANSWER = Problem.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
        ANSWER_C = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
        ANSWER_D = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
    }

    private void createProblem_05(){
        boolean isVertical;
        if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0){
            isVertical = true;
        }
        else {
            isVertical = false;
        }

        Parabolas Problem = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical);

        QUESTION = "Find the equation of the parabolas.<br>Given the focus is " + Problem.getFocus().toString()
                + "<br>and the directrix " + Problem.getDirectrix() + ".";
        RIGHT_ANSWER = Problem.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
        ANSWER_C = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
        ANSWER_D = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
    }

    private void createProblem_06(){
        boolean isVertical;
        if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0){
            isVertical = true;
        }
        else {
            isVertical = false;
        }

        Parabolas Problem = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical);

        QUESTION = "Find the equation of the parabolas.<br>Given the axis of symmetry is " + Problem.getAxisOfSymmetry()
                + "<br>and the directrix " + Problem.getDirectrix() + ".";
        RIGHT_ANSWER = Problem.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
        ANSWER_C = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
        ANSWER_D = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
    }

    private void createProblem_07(){
        boolean isVertical;
        if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0){
            isVertical = true;
        }
        else {
            isVertical = false;
        }

        Parabolas Problem = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical);

        QUESTION = "Find the equation of the parabolas.<br>Given the focus is " + Problem.getFocus().toString()
                + "<br>and pass through the point " + Problem.getRandomPoint().toString() + ".";
        RIGHT_ANSWER = Problem.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
        ANSWER_C = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
        ANSWER_D = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
    }

    private void createProblem_08(){
        boolean isVertical;
        if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0){
            isVertical = true;
        }
        else {
            isVertical = false;
        }

        Parabolas Problem = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical);

        Point PointA = Problem.getRandomPoint();
        Point PointB = Problem.getRandomPoint();
        if (PointA.isTheSame(PointB)){
            createProblem_08();
        }

        QUESTION = "Find the equation of the parabolas.<br>Given the axis of symmetry is " + Problem.getAxisOfSymmetry()
                + "<br>and pass through two points " + PointA.toString() + ", " + PointB.toString() + ".";
        RIGHT_ANSWER = Problem.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
        ANSWER_C = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
        ANSWER_D = new Parabolas(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), isVertical).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
