package pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckHyperbolas;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.LineMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;

/**
 * Created by Cong on 8/22/2017.
 */

public class FindingHyperbolasEquation extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingHyperbolasEquation(){
        createProblem();
        swapAnswer();
    }

    public FindingHyperbolasEquation(String Question,
                        String RightAnswer,
                        String AnswerA,
                        String AnswerB,
                        String AnswerC,
                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 4;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            default:
                createProblem_01();
                break;
        }
        return new FindingHyperbolasEquation(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        boolean isVertical;
        if (MathUtility.getRandomBooleanValue()){
            isVertical = true;
        }
        else {
            isVertical = false;
        }

        int AValue = MathUtility.getRandomPositiveNumber_1Digit(), BValue = MathUtility.getRandomPositiveNumber_1Digit();
        Point Center = LineMath.getRandomPoint();

        Hyperbolas Problem = new Hyperbolas(AValue, BValue, Center, isVertical);

        QUESTION = "Find the equation of a hyperbolas with the center is " + Problem.getCenter().toString()
                + "<br>vertexes are " + Problem.getVertex().toString() + ", " + Problem.getCoVertex().toString() + ".";
        RIGHT_ANSWER = Problem.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Hyperbolas(MathUtility.getRandomPositiveNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit(), Center)).toString();
        ANSWER_C = (new Hyperbolas(MathUtility.getRandomPositiveNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit(), Center)).toString();
        ANSWER_D = (new Hyperbolas(MathUtility.getRandomPositiveNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit(), Center)).toString();
    }

    private void createProblem_02(){
        boolean isVertical;
        if (MathUtility.getRandomBooleanValue()){
            isVertical = true;
        }
        else {
            isVertical = false;
        }

        int AValue = MathUtility.getRandomPositiveNumber_1Digit(), BValue = MathUtility.getRandomPositiveNumber_1Digit();
        Point Center = LineMath.getRandomPoint();

        Hyperbolas Problem = new Hyperbolas(AValue, BValue, Center, isVertical);

        QUESTION = "Find the equation of a hyperbolas with the vertex is " + Problem.getVertex().toString()
                + "<br>and the asymptote is " + Problem.getAsymptote() + ".";
        RIGHT_ANSWER = Problem.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Hyperbolas(MathUtility.getRandomPositiveNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit(), Center)).toString();
        ANSWER_C = (new Hyperbolas(MathUtility.getRandomPositiveNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit(), Center)).toString();
        ANSWER_D = (new Hyperbolas(MathUtility.getRandomPositiveNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit(), Center)).toString();
    }

    private void createProblem_03(){
        boolean isVertical;
        if (MathUtility.getRandomBooleanValue()){
            isVertical = true;
        }
        else {
            isVertical = false;
        }

        int AValue = 4, BValue = 3;
        Point Center = LineMath.getRandomPoint();

        Hyperbolas Problem = new Hyperbolas(AValue, BValue, Center, isVertical);

        QUESTION = "Find the equation of a hyperbolas with the center is " + Problem.getCenter().toString()
                + "<br>vertex is " + Problem.getVertex().toString() + "<br>and the focus is " + Problem.getFocus().toString() + ".";
        RIGHT_ANSWER = Problem.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Hyperbolas(MathUtility.getRandomPositiveNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit(), Center)).toString();
        ANSWER_C = (new Hyperbolas(MathUtility.getRandomPositiveNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit(), Center)).toString();
        ANSWER_D = (new Hyperbolas(MathUtility.getRandomPositiveNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit(), Center)).toString();
    }

    private void createProblem_04(){
        boolean isVertical;
        if (MathUtility.getRandomBooleanValue()){
            isVertical = true;
        }
        else {
            isVertical = false;
        }

        int AValue = 6, BValue = 8;
        Point Center = LineMath.getRandomPoint();

        Hyperbolas Problem = new Hyperbolas(AValue, BValue, Center, isVertical);

        QUESTION = "Find the equation of a hyperbolas with the focus is " + Problem.getFocus().toString()
                + "<br>vertex is " + Problem.getVertex().toString() + "<br>and the asymptote is " + Problem.getAsymptote() + ".";
        RIGHT_ANSWER = Problem.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Hyperbolas(MathUtility.getRandomPositiveNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit(), Center)).toString();
        ANSWER_C = (new Hyperbolas(MathUtility.getRandomPositiveNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit(), Center)).toString();
        ANSWER_D = (new Hyperbolas(MathUtility.getRandomPositiveNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit(), Center)).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
