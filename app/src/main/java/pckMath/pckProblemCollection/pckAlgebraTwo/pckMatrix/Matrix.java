package pckMath.pckProblemCollection.pckAlgebraTwo.pckMatrix;

import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;

/**
 * Created by Cong on 8/12/2017.
 */

public class Matrix {

    private int NO_COLUMN, NO_ROW;
    private Fraction[][] MATRIX_ELEMENT;

    public Matrix(){
        NO_COLUMN = 1;
        NO_ROW = 0;

        MATRIX_ELEMENT = new Fraction[NO_ROW][NO_COLUMN];
    }

    public Matrix(int NoRow, int NoColumn){
        NO_COLUMN = NoColumn;
        NO_ROW = NoRow;

        MATRIX_ELEMENT = new Fraction[NO_ROW][NO_COLUMN];
    }

    public String getDimension(){
        return NO_ROW + " x " + NO_COLUMN;
    }

    public void setNoColumn(int NoColumn){
        NO_COLUMN = NoColumn;
    }

    public int getNoColumn(){
        return NO_COLUMN;
    }

    public void setNoRow(int NoRow){
        NO_ROW = NoRow;
    }

    public int getNoRow(){
        return NO_ROW;
    }

    public void addElement(Fraction Element, int Row, int Column){
        MATRIX_ELEMENT[Row][Column] = Element;
    }

    public void addElement(int Element, int Row, int Column){
        MATRIX_ELEMENT[Row][Column]  = new Fraction(Element);
    }

    public Fraction getElementAt(int Row, int Column){
        return MATRIX_ELEMENT[Row][Column];
    }

    public String toString(){
        String MatrixString = "";
        for(int i = 0; i < NO_ROW; i++){
            for (int j = 0; j < NO_COLUMN; j++){
                MatrixString += String.valueOf(MATRIX_ELEMENT[i][j].toString()) + " ";
            }

            MatrixString += "<br>";
        }

        return MatrixString;
    }
}
