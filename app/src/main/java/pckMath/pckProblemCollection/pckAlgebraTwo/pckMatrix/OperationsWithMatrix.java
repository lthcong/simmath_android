package pckMath.pckProblemCollection.pckAlgebraTwo.pckMatrix;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 8/14/2017.
 */

public class OperationsWithMatrix extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public OperationsWithMatrix(){
        createProblem();
        swapAnswer();
    }

    public OperationsWithMatrix(String Question,
                             String RightAnswer,
                             String AnswerA,
                             String AnswerB,
                             String AnswerC,
                             String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 2;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            default:
                createProblem_01();
                break;
        }
        return new OperationsWithMatrix(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int NoRow = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int NoColumn = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;

        Matrix MatrixA = MatrixMath.getRandomMatrix(NoRow, NoColumn);
        Matrix MatrixB = MatrixMath.getRandomMatrix(NoRow, NoColumn);

        int MultipleA = MathUtility.getRandomNumber_1Digit();
        int MultipleB = MathUtility.getRandomNumber_1Digit();

        QUESTION = String.valueOf(MultipleA) + "A + " + String.valueOf(MultipleB) + "B = ?<br>"
                + "A:<br>" + MatrixA.toString() + "<br><br>AND<br><br>B:<br>" + MatrixB.toString();
        RIGHT_ANSWER = MatrixMath.add(MatrixMath.mul(MatrixA, MultipleA), MatrixMath.mul(MatrixB, MultipleB)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = MatrixMath.getRandomMatrix(NoRow, NoColumn).toString();
        ANSWER_C = MatrixMath.getRandomMatrix(NoRow, NoColumn).toString();
        ANSWER_D = MatrixMath.getRandomMatrix(NoRow, NoColumn).toString();
    }

    private void createProblem_02(){
        int NoRow = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int NoColumn = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;

        Matrix MatrixA = MatrixMath.getRandomMatrix(NoRow, NoColumn);
        Matrix MatrixB = MatrixMath.getRandomMatrix(NoRow, NoColumn);

        Fraction MultipleA = FractionMath.getRandomUnitFraction();
        Fraction MultipleB = FractionMath.getRandomUnitFraction();

        QUESTION = MultipleA.toString() + "A + " + MultipleB.toString() + "B = ?<br>"
                + "A:<br>" + MatrixA.toString() + "<br><br>AND<br><br>B:<br>" + MatrixB.toString();
        RIGHT_ANSWER = MatrixMath.add(MatrixMath.mul(MatrixA, MultipleA), MatrixMath.mul(MatrixB, MultipleB)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = MatrixMath.getRandomMatrix(NoRow, NoColumn).toString();
        ANSWER_C = MatrixMath.getRandomMatrix(NoRow, NoColumn).toString();
        ANSWER_D = MatrixMath.getRandomMatrix(NoRow, NoColumn).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
