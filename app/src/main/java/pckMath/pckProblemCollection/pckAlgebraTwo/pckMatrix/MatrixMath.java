package pckMath.pckProblemCollection.pckAlgebraTwo.pckMatrix;

import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 8/13/2017.
 */

public class MatrixMath {

    public static Matrix getRandomMatrix(){
        int Column = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int Row = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;

        Matrix RandomMatrix = new Matrix(Row, Column);
        for (int i = 0; i < Row; i++){
            for (int j = 0; j < Column; j++){
                RandomMatrix.addElement(MathUtility.getRandomNumber_1Digit(), i, j);
            }
        }

        return RandomMatrix;
    }

    public static Matrix getRandomMatrix(int NoRow, int NoColumn){
        int Row = NoRow;
        int Column = NoColumn;

        Matrix RandomMatrix = new Matrix(Row, Column);
        for (int i = 0; i < Row; i++){
            for (int j = 0; j < Column; j++){
                RandomMatrix.addElement(MathUtility.getRandomNumber_1Digit(), i, j);
            }
        }

        return RandomMatrix;
    }

    public static Matrix add(Matrix MatrixA, Matrix MatrixB){
        int NoRow = MatrixA.getNoRow();
        int NoColumn = MatrixA.getNoColumn();
        Matrix Sum = new Matrix(NoRow, NoColumn);
        for (int i = 0; i < NoRow; i++){
            for (int j = 0; j < NoColumn; j++){
                Sum.addElement(FractionMath.addFraction(MatrixA.getElementAt(i, j), MatrixB.getElementAt(i, j)), i, j);
            }
        }

        return Sum;
    }

    public static Matrix sub(Matrix MatrixA, Matrix MatrixB){
        int NoRow = MatrixA.getNoRow();
        int NoColumn = MatrixA.getNoColumn();
        Matrix Sum = new Matrix(NoRow, NoColumn);
        for (int i = 0; i < NoRow; i++){
            for (int j = 0; j < NoColumn; j++){
                Sum.addElement(FractionMath.subtractFraction(MatrixA.getElementAt(i, j), MatrixB.getElementAt(i, j)), i, j);
            }
        }

        return Sum;
    }

    public static Matrix mul(Matrix MatrixA, int Number){
        int NoRow = MatrixA.getNoRow();
        int NoColumn = MatrixA.getNoColumn();
        Matrix Product = new Matrix(NoRow, NoColumn);
        for (int i = 0; i < NoRow; i++){
            for (int j = 0; j < NoColumn; j++){
                Product.addElement(FractionMath.multiplyFraction(MatrixA.getElementAt(i, j), new Fraction(Number)), i, j);
            }
        }

        return Product;
    }

    public static Matrix mul(Matrix MatrixA, Fraction Number){
        int NoRow = MatrixA.getNoRow();
        int NoColumn = MatrixA.getNoColumn();
        Matrix Product = new Matrix(NoRow, NoColumn);
        for (int i = 0; i < NoRow; i++){
            for (int j = 0; j < NoColumn; j++){
                Product.addElement(FractionMath.multiplyFraction(MatrixA.getElementAt(i, j), Number), i, j);
            }
        }

        return Product;
    }

    public static Matrix mul(Matrix MatrixA, Matrix MatrixB){
        int ColumnA = MatrixA.getNoColumn();
        int RowA = MatrixA.getNoRow();

        int ColumnB = MatrixB.getNoColumn();

        Matrix Product = new Matrix(MatrixA.getNoRow(), MatrixB.getNoColumn());
        for (int i = 0; i < RowA; i++){
            for (int j = 0; j < ColumnB; j++) {
                Fraction NewElement = new Fraction(0, 1);

                for (int k = 0; k < ColumnA; k++){
                    Fraction Multiply = FractionMath.multiplyFraction(MatrixA.getElementAt(i, k), MatrixB.getElementAt(k, j));
                    NewElement = FractionMath.addFraction(NewElement, Multiply);
                }

                Product.addElement(NewElement, i, j);
            }
        }

        return Product;
    }

    public static Fraction findTheDeterminant(Matrix _Matrix){
        Fraction Determinant = new Fraction();

        //  2 x 2
        if (_Matrix.getNoColumn() == 2 && _Matrix.getNoRow() == 2){
            Determinant = FractionMath.subtractFraction(FractionMath.multiplyFraction(_Matrix.getElementAt(0, 0), _Matrix.getElementAt(1, 1)),
                    FractionMath.multiplyFraction(_Matrix.getElementAt(0, 1), _Matrix.getElementAt(1, 0)));
        }
        else {
            //  3 x 3
            Fraction a = FractionMath.multiplyFraction(_Matrix.getElementAt(0, 0), FractionMath.subtractFraction(
                    FractionMath.multiplyFraction(_Matrix.getElementAt(1, 1), _Matrix.getElementAt(2, 2)),
                    FractionMath.multiplyFraction(_Matrix.getElementAt(1, 2), _Matrix.getElementAt(2, 1))));

            Fraction b = FractionMath.multiplyFraction(_Matrix.getElementAt(0, 1), FractionMath.subtractFraction(
                    FractionMath.multiplyFraction(_Matrix.getElementAt(1, 0), _Matrix.getElementAt(2, 2)),
                    FractionMath.multiplyFraction(_Matrix.getElementAt(2, 0), _Matrix.getElementAt(1, 2))));

            Fraction c = FractionMath.multiplyFraction(_Matrix.getElementAt(0, 2), FractionMath.subtractFraction(
                    FractionMath.multiplyFraction(_Matrix.getElementAt(1, 0), _Matrix.getElementAt(2, 1)),
                    FractionMath.multiplyFraction(_Matrix.getElementAt(2, 0), _Matrix.getElementAt(1, 1))));

            Determinant = FractionMath.addFraction(FractionMath.subtractFraction(a, b), c);
        }

        return Determinant;
    }

    public static Matrix findTheTransposeMatrix(Matrix _Matrix){
        int NoColumn = _Matrix.getNoColumn();
        int NoRow = _Matrix.getNoRow();
        Matrix TransposedMatrix = new Matrix(NoRow, NoColumn);
        for (int i = 0; i < NoRow; i++){
            for (int j = 0; j < NoColumn; j++){
                TransposedMatrix.addElement(_Matrix.getElementAt(i, j), j, i);
            }
        }

        return TransposedMatrix;
    }

    public static Matrix findTheInverseMatrix(Matrix _Matrix){
        Matrix InverseMatrix;

        if (_Matrix.getNoRow() == 2 && _Matrix.getNoColumn() == 2){
            InverseMatrix = findTheInverseMatrix_2By2(_Matrix);
        }
        else {
            InverseMatrix = findTheInverseMatrix_3By3(_Matrix);
        }

        return InverseMatrix;
    }

    private static Matrix findTheInverseMatrix_2By2(Matrix _Matrix){
        Matrix InverseMatrix = new Matrix(2, 2);
        Fraction InverseDeterminant = FractionMath.getReciprocal(findTheDeterminant(_Matrix));

        InverseMatrix.addElement(FractionMath.multiplyFraction(_Matrix.getElementAt(1,1), InverseDeterminant), 0, 0);
        InverseMatrix.addElement(FractionMath.multiplyFraction(FractionMath.subtractFraction(new Fraction(0, 1),_Matrix.getElementAt(0,1)), InverseDeterminant), 0, 1);
        InverseMatrix.addElement(FractionMath.multiplyFraction(FractionMath.subtractFraction(new Fraction(0, 1),_Matrix.getElementAt(1,0)), InverseDeterminant), 1, 0);
        InverseMatrix.addElement(FractionMath.multiplyFraction(_Matrix.getElementAt(0,0), InverseDeterminant), 1, 1);

        return InverseMatrix;
    }

    private static Matrix findTheInverseMatrix_3By3(Matrix _Matrix){
        Matrix InverseMatrix;

        //  DETERMINANT
        Fraction InverseDeterminant = FractionMath.getReciprocal(findTheDeterminant(_Matrix));

        //  TRANSPOSE MATRIX
        Matrix TransposeMatrix = findTheTransposeMatrix(_Matrix);

        //  SUB-MATRIX
        Matrix MatrixA = new Matrix(2, 2);
        MatrixA.addElement(TransposeMatrix.getElementAt(1,1), 0, 0);
        MatrixA.addElement(TransposeMatrix.getElementAt(1,2), 0, 1);
        MatrixA.addElement(TransposeMatrix.getElementAt(2,1), 1, 0);
        MatrixA.addElement(TransposeMatrix.getElementAt(2,2), 1, 1);

        Matrix MatrixB = new Matrix(2, 2);
        MatrixB.addElement(TransposeMatrix.getElementAt(1,0), 0, 0);
        MatrixB.addElement(TransposeMatrix.getElementAt(1,2), 0, 1);
        MatrixB.addElement(TransposeMatrix.getElementAt(2,0), 1, 0);
        MatrixB.addElement(TransposeMatrix.getElementAt(2,2), 1, 1);

        Matrix MatrixC = new Matrix(2, 2);
        MatrixC.addElement(TransposeMatrix.getElementAt(1,0), 0, 0);
        MatrixC.addElement(TransposeMatrix.getElementAt(1,1), 0, 1);
        MatrixC.addElement(TransposeMatrix.getElementAt(2,0), 1, 0);
        MatrixC.addElement(TransposeMatrix.getElementAt(2,1), 1, 1);

        Matrix MatrixD = new Matrix(2, 2);
        MatrixD.addElement(TransposeMatrix.getElementAt(0,1), 0, 0);
        MatrixD.addElement(TransposeMatrix.getElementAt(0,2), 0, 1);
        MatrixD.addElement(TransposeMatrix.getElementAt(2,1), 1, 0);
        MatrixD.addElement(TransposeMatrix.getElementAt(2,2), 1, 1);

        Matrix MatrixE = new Matrix(2, 2);
        MatrixE.addElement(TransposeMatrix.getElementAt(0,0), 0, 0);
        MatrixE.addElement(TransposeMatrix.getElementAt(0,2), 0, 1);
        MatrixE.addElement(TransposeMatrix.getElementAt(2,0), 1, 0);
        MatrixE.addElement(TransposeMatrix.getElementAt(2,2), 1, 1);

        Matrix MatrixF = new Matrix(2, 2);
        MatrixF.addElement(TransposeMatrix.getElementAt(0,0), 0, 0);
        MatrixF.addElement(TransposeMatrix.getElementAt(0,1), 0, 1);
        MatrixF.addElement(TransposeMatrix.getElementAt(2,0), 1, 0);
        MatrixF.addElement(TransposeMatrix.getElementAt(2,1), 1, 1);

        Matrix MatrixG = new Matrix(2, 2);
        MatrixG.addElement(TransposeMatrix.getElementAt(0,1), 0, 0);
        MatrixG.addElement(TransposeMatrix.getElementAt(0,2), 0, 1);
        MatrixG.addElement(TransposeMatrix.getElementAt(1,1), 1, 0);
        MatrixG.addElement(TransposeMatrix.getElementAt(1,2), 1, 1);

        Matrix MatrixH = new Matrix(2, 2);
        MatrixH.addElement(TransposeMatrix.getElementAt(0,0), 0, 0);
        MatrixH.addElement(TransposeMatrix.getElementAt(0,2), 0, 1);
        MatrixH.addElement(TransposeMatrix.getElementAt(1,0), 1, 0);
        MatrixH.addElement(TransposeMatrix.getElementAt(1,2), 1, 1);

        Matrix MatrixI = new Matrix(2, 2);
        MatrixI.addElement(TransposeMatrix.getElementAt(0,0), 0, 0);
        MatrixI.addElement(TransposeMatrix.getElementAt(0,1), 0, 1);
        MatrixI.addElement(TransposeMatrix.getElementAt(1,0), 1, 0);
        MatrixI.addElement(TransposeMatrix.getElementAt(1,1), 1, 1);

        //  COFACTORS MATRIX
        Matrix AdjMatrix = new Matrix(3, 3);

        AdjMatrix.addElement(MatrixMath.findTheDeterminant(MatrixA), 0, 0);
        AdjMatrix.addElement(FractionMath.multiplyFraction(MatrixMath.findTheDeterminant(MatrixB), new Fraction(-1, 1)), 0, 1);
        AdjMatrix.addElement(MatrixMath.findTheDeterminant(MatrixC), 0, 2);

        AdjMatrix.addElement(FractionMath.multiplyFraction(MatrixMath.findTheDeterminant(MatrixD), new Fraction(-1, 1)), 1, 0);
        AdjMatrix.addElement(MatrixMath.findTheDeterminant(MatrixE), 1, 1);
        AdjMatrix.addElement(FractionMath.multiplyFraction(MatrixMath.findTheDeterminant(MatrixF), new Fraction(-1, 1)), 1, 2);

        AdjMatrix.addElement(MatrixMath.findTheDeterminant(MatrixG), 2, 0);
        AdjMatrix.addElement(FractionMath.multiplyFraction(MatrixMath.findTheDeterminant(MatrixH), new Fraction(-1, 1)), 2, 1);
        AdjMatrix.addElement(MatrixMath.findTheDeterminant(MatrixI), 2, 2);

        InverseMatrix = MatrixMath.mul(AdjMatrix, InverseDeterminant);

        return InverseMatrix;
    }
}
