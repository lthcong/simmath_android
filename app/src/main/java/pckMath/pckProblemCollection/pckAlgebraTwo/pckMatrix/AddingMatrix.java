package pckMath.pckProblemCollection.pckAlgebraTwo.pckMatrix;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 8/14/2017.
 */

public class AddingMatrix extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public AddingMatrix(){
        createProblem();
        swapAnswer();
    }

    public AddingMatrix(String Question,
                                   String RightAnswer,
                                   String AnswerA,
                                   String AnswerB,
                                   String AnswerC,
                                   String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 2;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            default:
                createProblem_01();
                break;
        }
        return new AddingMatrix(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int NoRow = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int NoColumn = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;

        Matrix MatrixA = MatrixMath.getRandomMatrix(NoRow, NoColumn);
        Matrix MatrixB = MatrixMath.getRandomMatrix(NoRow, NoColumn);

        QUESTION = MatrixA.toString() + "<br><br>AND<br><br>" + MatrixB.toString();
        RIGHT_ANSWER = MatrixMath.add(MatrixA, MatrixB).toString();

        ANSWER_A = "<br>" + RIGHT_ANSWER;
        ANSWER_B = "<br>" + MatrixMath.getRandomMatrix(NoRow, NoColumn).toString();
        ANSWER_C = "<br>" + MatrixMath.getRandomMatrix(NoRow, NoColumn).toString();
        ANSWER_D = "<br>" + MatrixMath.getRandomMatrix(NoRow, NoColumn).toString();
    }

    @Override
    public String getQuestion() {
        QUESTION = "Find the sum of the two matrices:<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
