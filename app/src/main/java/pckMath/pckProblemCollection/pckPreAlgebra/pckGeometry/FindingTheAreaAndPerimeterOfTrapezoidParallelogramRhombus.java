package pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/10/2017.
 */

public class FindingTheAreaAndPerimeterOfTrapezoidParallelogramRhombus extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingTheAreaAndPerimeterOfTrapezoidParallelogramRhombus(){
        createProblem();
        swapAnswer();
    }

    public FindingTheAreaAndPerimeterOfTrapezoidParallelogramRhombus(String Question,
                                                                String RightAnswer,
                                                                String AnswerA,
                                                                String AnswerB,
                                                                String AnswerC,
                                                                String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 22;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            case 14:
                createProblem_15();
                break;
            case 15:
                createProblem_16();
                break;
            case 16:
                createProblem_17();
                break;
            case 17:
                createProblem_18();
                break;
            case 18:
                createProblem_19();
                break;
            case 19:
                createProblem_20();
                break;
            case 20:
                createProblem_21();
                break;
            case 21:
                createProblem_22();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FindingTheAreaAndPerimeterOfTrapezoidParallelogramRhombus(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  PERIMETER OF TRAPEZOID
    private void createProblem_01(){
        int base_1 = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int base_2 = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int side_1 = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int side_2 = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int perimeter = base_1 + base_2 + side_1 + side_2;

        QUESTION = "Find the perimeter of a trapezoid given the two bases are " + String.valueOf(base_1) + ", " + String.valueOf(base_2)
                + ", and the sides are " + String.valueOf(side_1) + ", " + String.valueOf(side_2) + ".";
        RIGHT_ANSWER = String.valueOf(perimeter);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(perimeter + 1);
        ANSWER_C = String.valueOf(perimeter - 1);
        ANSWER_D = String.valueOf(perimeter + 2);
    }

    //  PERIMETER OF TRAPEZOID
    private void createProblem_02(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomPositiveNumber_1Digit();
        int d_value = MathUtility.getRandomPositiveNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int base_1 = a_value * x_value + b_value;
        int base_2 = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int side_1 = c_value * x_value + d_value;
        int side_2 = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int perimeter = base_1 + base_2 + side_1 + side_2;

        QUESTION = "Given the perimeter of a trapezoid is " + String.valueOf(perimeter) + ".<br>"
                + "The sides are " + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ", and " + String.valueOf(side_2) + ".<br>"
                + "The bases are " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", and " + String.valueOf(base_2) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value - 2);
    }

    //  ARE OF TRAPEZOID
    private void createProblem_03(){
        int base_1 = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int base_2 = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double area = (double) ((base_1 + base_2) * height) / 2.0;
        area = MathUtility.round_1_Decimal(area);

        QUESTION = "Find the area of a trapezoid with the bases are " + String.valueOf(base_1) + ", " + String.valueOf(base_2)
                + ", and the height is " + String.valueOf(height) + ".";
        RIGHT_ANSWER = String.valueOf(area);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(area + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(area - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(area - 2.0));
    }

    //  ARE OF TRAPEZOID
    private void createProblem_04(){
        int base_1 = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int base_2 = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double area = (double) ((base_1 + base_2) * height) / 2.0;
        area = MathUtility.round_1_Decimal(area);

        QUESTION = "Find the height of a trapezoid with the bases are " + String.valueOf(base_1) + ", " + String.valueOf(base_2)
                + ", and the area is " + String.valueOf(area) + ".";
        RIGHT_ANSWER = String.valueOf(height);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(height + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(height - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(height + 2.0));
    }

    //  ARE OF TRAPEZOID
    private void createProblem_05(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int base_1 = a_value * x_value + b_value;
        int base_2 = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double area = (double) ((base_1 + base_2) * height) / 2.0;
        area = MathUtility.round_1_Decimal(area);

        QUESTION = "Given a trapezoid with the bases are " + String.valueOf(base_1)
                + ", " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ".<br>"
                + "The height is " + String.valueOf(height) + ", and the area is " + String.valueOf(area) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(x_value + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(x_value - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(x_value + 2.0));
    }

    //  ARE OF TRAPEZOID
    private void createProblem_06(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int height = a_value * x_value + b_value;
        int base_1 = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int base_2 = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double area = (double) ((base_1 + base_2) * height) / 2.0;
        area = MathUtility.round_1_Decimal(area);

        QUESTION = "Given a trapezoid with the bases are " + String.valueOf(base_1) + ", " + String.valueOf(base_2) + ", "
                + "the height is " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ".<br>"
                + "The area is " + String.valueOf(area) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(x_value + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(x_value - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(x_value + 2.0));
    }

    //  PERIMETER OF PARALLELOGRAM
    private void createProblem_07(){
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int perimeter = 2 * width + 2 * length;

        QUESTION = "Find the perimeter of a parallelogram, given the 2 consecutive sides are "
                + String.valueOf(length) + ", " + String.valueOf(width) + ".";
        RIGHT_ANSWER = String.valueOf(perimeter);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(perimeter + 1);
        ANSWER_C = String.valueOf(perimeter - 1);
        ANSWER_D = String.valueOf(perimeter + 2);
    }

    //  PERIMETER OF PARALLELOGRAM
    private void createProblem_08(){
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int perimeter = 2 * width + 2 * length;

        QUESTION = "Find the side of a parallelogram, given the consecutive side is "
                + String.valueOf(width) + ", and the perimeter is " + String.valueOf(perimeter) + ".";
        RIGHT_ANSWER = String.valueOf(length);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(length + 1);
        ANSWER_C = String.valueOf(length - 1);
        ANSWER_D = String.valueOf(length + 2);
    }

    //  PERIMETER OF PARALLELOGRAM
    private void createProblem_09(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int width = a_value * x_value + b_value;
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int perimeter = 2 * width + 2 * length;

        QUESTION = "Given a parallelogram with the 2 consecutive sides are: "
                + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", " + String.valueOf(length) + ".<br>"
                + "The perimeter is " + String.valueOf(perimeter) + ". Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value - 2);
    }

    //  PERIMETER OF PARALLELOGRAM
    private void createProblem_10(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomPositiveNumber_1Digit();
        int d_value = MathUtility.getRandomPositiveNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int width = a_value * x_value + b_value;
        int length = c_value * x_value + d_value;

        int perimeter = 2 * width + 2 * length;

        QUESTION = "Given a parallelogram with the perimeter is " + String.valueOf(perimeter) + ".<br>"
                + "The 2 consecutive sides are " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", "
                + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + "<br>."
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  AREA OF PARALLELOGRAM
    private void createProblem_11(){
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int area = width * length;

        QUESTION = "Find the area of a parallelogram, given the 2 consecutive sides are "
                + String.valueOf(length) + ", " + String.valueOf(width) + ".";
        RIGHT_ANSWER = String.valueOf(area);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(area + 1);
        ANSWER_C = String.valueOf(area - 1);
        ANSWER_D = String.valueOf(area + 2);
    }

    //  AREA OF PARALLELOGRAM
    private void createProblem_12(){
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int area = width * length;

        QUESTION = "Find the side of a parallelogram, given the consecutive side is "
                + String.valueOf(length) + ", and the area is " + String.valueOf(area) + ".";
        RIGHT_ANSWER = String.valueOf(width);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(width + 1);
        ANSWER_C = String.valueOf(width - 1);
        ANSWER_D = String.valueOf(width + 2);
    }

    //  AREA OF PARALLELOGRAM
    //  Finding the x value: (ax + b) * S = A
    private void createProblem_13(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int width = a_value * x_value + b_value;
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int area = width * length;

        QUESTION = "Given a parallelogram with the area is " + String.valueOf(area) + ", and the two consecutive sides are: "
                + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", " + String.valueOf(length) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  AREA OF PARALLELOGRAM
    //  Finding the x value: (ax + b) * S = (cx + d)
    private void createProblem_14(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int width = a_value * x_value + b_value;
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int area = width * length;

        int d_value = area - c_value * x_value;

        QUESTION = "Given a parallelogram with the area is " + String.valueOf(c_value) + "x + " + String.valueOf(d_value)
                + ", and the two consecutive sides are: "
                + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", " + String.valueOf(length) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  PERIMETER OF RHOMBUS
    private void createProblem_15(){
        int side = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int perimeter = 4 * side;

        QUESTION = "Find the perimeter of a rhombus with the side is " + String.valueOf(side) + ".";
        RIGHT_ANSWER = String.valueOf(perimeter);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(perimeter + 1);
        ANSWER_C = String.valueOf(perimeter + 2);
        ANSWER_D = String.valueOf(perimeter - 1);
    }

    //  PERIMETER OF RHOMBUS
    private void createProblem_16(){
        int side = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int perimeter = 4 * side;

        QUESTION = "Find the side of a rhombus with the perimeter is " + String.valueOf(perimeter) + ".";
        RIGHT_ANSWER = String.valueOf(side);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(side + 1);
        ANSWER_C = String.valueOf(side + 2);
        ANSWER_D = String.valueOf(side - 1);
    }

    //  PERIMETER OF RHOMBUS
    //  Finding the x value: 4(ax + b) = P
    private void createProblem_17(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int side = a_value * x_value + b_value;
        int perimeter = 4 * side;

        QUESTION = "Given a rhombus with perimeter is " + String.valueOf(perimeter) + ", and the side is "
                + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value + 2);
        ANSWER_D = String.valueOf(x_value - 1);
    }

    //  PERIMETER OF RHOMBUS
    //  Finding the x value: 4(ax + b) = (cx + d)
    private void createProblem_18(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomPositiveNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int side = a_value * x_value + b_value;
        int perimeter = 4 * side;

        int d_value = perimeter - c_value * x_value;

        QUESTION = "Given a rhombus with perimeter is " + String.valueOf(c_value) + "x + " + String.valueOf(d_value)
                + ", and the side is " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value + 2);
        ANSWER_D = String.valueOf(x_value - 1);
    }

    //  ARE OF RHOMBUS
    private void createProblem_19(){
        int diagonal_1 = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int diagonal_2 = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        double area = MathUtility.round_1_Decimal((double) (diagonal_1 * diagonal_2) / 2.0);

        QUESTION = "Find the area of a rhombus with the diagonals are " + String.valueOf(diagonal_1) + ", and " + String.valueOf(diagonal_2) + ".";
        RIGHT_ANSWER = String.valueOf(area);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(area + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(area - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(area + 2.0));
    }

    //  ARE OF RHOMBUS
    private void createProblem_20(){
        int diagonal_1 = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int diagonal_2 = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        double area = MathUtility.round_1_Decimal((double) (diagonal_1 * diagonal_2) / 2.0);

        QUESTION = "Find the first diagonal of a rhombus with the area is " + String.valueOf(area) + ", and the second diagonal is " + String.valueOf(diagonal_1) + ".";
        RIGHT_ANSWER = String.valueOf(diagonal_2);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(diagonal_2 + 1);
        ANSWER_C = String.valueOf(diagonal_2 - 1);
        ANSWER_D = String.valueOf(diagonal_2 + 2);
    }

    //  ARE OF RHOMBUS
    /*
    *   Finding the x: (ax + b) * diagonal / 2 = area
    * */
    private void createProblem_21(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();
        int diagonal_1 = a_value * x_value + b_value;

        int diagonal_2 = MathUtility.getRandomPositiveNumber_1Digit() + 4;

        double area = MathUtility.round_1_Decimal((double) (diagonal_1 * diagonal_2) / 2.0);

        QUESTION = "Given a rhombus with the first diagonal is " + String.valueOf(diagonal_1)
                + ", the second diagonal is " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ".<br>" +
                "The area is " + String.valueOf(area) + ". Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  ARE OF TRIANGLE
    /*
    *   Finding the x: (ax + b) * diagonal / 2 = (cx + d)
    * */
    private void createProblem_22(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();
        int diagonal_1 = a_value * x_value + b_value;

        int diagonal_2 = MathUtility.getRandomPositiveNumber_1Digit() + 4;

        double area = (double) (diagonal_1 * diagonal_2) / 2.0;
        area = MathUtility.round_1_Decimal(area);

        double d_value = area - c_value * x_value;
        d_value = MathUtility.round_1_Decimal(d_value);

        QUESTION = "Given a rhombus with the first diagonal is " + String.valueOf(diagonal_2)
                + ", the second diagonal is " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ".<br>"
                + "The area is " + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ". "
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
