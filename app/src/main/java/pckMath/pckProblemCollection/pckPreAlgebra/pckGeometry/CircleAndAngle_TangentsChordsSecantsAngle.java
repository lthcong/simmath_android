package pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/13/2017.
 */

public class CircleAndAngle_TangentsChordsSecantsAngle extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public CircleAndAngle_TangentsChordsSecantsAngle(){
        createProblem();
        swapAnswer();
    }

    public CircleAndAngle_TangentsChordsSecantsAngle(String Question,
                                                    String RightAnswer,
                                                    String AnswerA,
                                                    String AnswerB,
                                                    String AnswerC,
                                                    String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 15;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            case 14:
                createProblem_15();
                break;
            default:
                createProblem_01();
                break;
        }
        return new CircleAndAngle_TangentsChordsSecantsAngle(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  TANGENT - CHORD ANGLE
    private void createProblem_01(){
        int arcAB = MathUtility.getRandomAngle() * 2;
        int TangentChordAngle = arcAB / 2;

        QUESTION = "A, B are two points on the circle. Find the measure of the angle formed by the chord AB and the tangent of the circle at B. "
                + "Given the measure of the arc AB is " + String.valueOf(arcAB) + InfoCollector.getDegreeSign() + ".";
        RIGHT_ANSWER = String.valueOf(TangentChordAngle) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(TangentChordAngle + 1) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(TangentChordAngle - 1) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(TangentChordAngle + 2) + InfoCollector.getDegreeSign();
    }

    //  TANGENT - CHORD ANGLE
    private void createProblem_02(){
        int arcAB = MathUtility.getRandomAngle() * 2;
        int TangentChordAngle = arcAB / 2;

        QUESTION = "A, B are two points on the circle. Find the measure of the arc AB. "
                + "Given the measure of the angle formed by the chord AB and the tangent of the circle at B is " + String.valueOf(TangentChordAngle) + InfoCollector.getDegreeSign() + ".";
        RIGHT_ANSWER = String.valueOf(arcAB) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(arcAB + 1) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(arcAB - 1) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(arcAB + 2) + InfoCollector.getDegreeSign();
    }

    //  TANGENT - CHORD ANGLE
    //  Finding the x value: ax + b = 2 * (cx + d)
    private void createProblem_03(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int c_value = MathUtility.getRandomPositiveNumber_1Digit();
        int d_value = MathUtility.getRandomPositiveNumber_1Digit();

        int b_value = 2 * (c_value * x_value + d_value) - a_value * x_value;

        QUESTION = "A and B are two points on the circle. "
                + "Given the measure of arc AB is " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", "
                + "and the measure of the angle formed by the chord AB and the tangent at B is " + String.valueOf(c_value) + "x + " + String.valueOf(d_value)
                + ". Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  CHORD - CHORD ANGLE
    private void createProblem_04(){
        int AB = MathUtility.getRandomAngleLessThan90Degree();
        int CD = MathUtility.getRandomAngleLessThan90Degree();

        double IntersectingAngle = MathUtility.round_1_Decimal(((double) AB + (double) CD) / 2.0);

        QUESTION = "A, B, C, D are four points on the circle. The chords AC and BD intersect each other at E inside the circle. "
                + "Given the measure of the arc AB is " + String.valueOf(AB) + InfoCollector.getDegreeSign()
                + ", and the arc CD is " + String.valueOf(CD) + InfoCollector.getDegreeSign()
                + ". Find the measure of the angle " + InfoCollector.getAngleSign() + "AEB.";
        RIGHT_ANSWER = String.valueOf(IntersectingAngle) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(IntersectingAngle + 1.0)) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(IntersectingAngle - 1.0)) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(IntersectingAngle + 2.0)) + InfoCollector.getDegreeSign();
    }

    //  CHORD - CHORD ANGLE
    private void createProblem_05(){
        int AB = MathUtility.getRandomAngleLessThan90Degree();
        int CD = MathUtility.getRandomAngleLessThan90Degree();

        double IntersectingAngle = MathUtility.round_1_Decimal(((double) AB + (double) CD) / 2.0);

        QUESTION = "A, B, C, D are four points on the circle. The chords AC and BD intersect each other at E inside the circle. "
                + "Given the measure of the arc AB is " + String.valueOf(AB) + InfoCollector.getDegreeSign()
                + ", and the measure of the angle " + InfoCollector.getAngleSign() + "AEB is " + String.valueOf(IntersectingAngle) + InfoCollector.getDegreeSign()
                + ". Find the measure of the arc CD." ;
        RIGHT_ANSWER = String.valueOf(CD) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(CD - 10) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(CD + 10) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(CD + 20) + InfoCollector.getDegreeSign();
    }

    //  CHORD - CHORD ANGLE
    //  Finding the x value: [(ax + b) + (cx + d)] / 2 = ANGLE
    private void createProblem_06(){
        int IntersectingAngle = MathUtility.getRandomAngleLessThan90Degree();
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomPositiveNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();
        int d_value = 2 * IntersectingAngle - (a_value * x_value + b_value) - c_value * x_value;

        QUESTION = "A, B, C, D are four points on the circle. The chords AC and BD intersect each other at E inside the circle. "
                + "Given the measure of the angle " + InfoCollector.getAngleSign() + "AEB is " + String.valueOf(IntersectingAngle) + InfoCollector.getDegreeSign()
                + ", and the measure of the arc AB and CD are: "
                + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", "
                + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ". "
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  TANGENT - TANGENT ANGLE
    private void createProblem_07(){
        int SmallerArc = MathUtility.getRandomAngleGreaterThan90Degree();
        int BiggerArc = MathUtility.getRandomAngleGreaterThan90Degree() + SmallerArc;

        double Angle = MathUtility.round_1_Decimal((double) (BiggerArc - SmallerArc) / 2.0);

        QUESTION = "A, B are 2 points on the circle. The tangents at A and B intersect each other at C. "
                + "Find the measure of the angle " + InfoCollector.getAngleSign() + "ACB. "
                + "Given the measure of the two arcs AB are " + String.valueOf(SmallerArc) + InfoCollector.getDegreeSign()
                + ", and " + String.valueOf(BiggerArc) + InfoCollector.getDegreeSign() + ".";
        RIGHT_ANSWER = String.valueOf(Angle) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Angle + 10)) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Angle - 10)) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Angle + 20)) + InfoCollector.getDegreeSign();
    }

    //  TANGENT - TANGENT ANGLE
    private void createProblem_08(){
        int SmallerArc = MathUtility.getRandomAngleGreaterThan90Degree();
        int BiggerArc = MathUtility.getRandomAngleGreaterThan90Degree() + SmallerArc;

        double Angle = MathUtility.round_1_Decimal((double) (BiggerArc - SmallerArc) / 2.0);

        QUESTION = "A, B are 2 points on the circle. The tangents at A and B intersect each other at C. "
                + "Given the measure of the smaller arc AB is " + String.valueOf(SmallerArc) + InfoCollector.getDegreeSign()
                + ", and the measure of the angle " + InfoCollector.getAngleSign() + "ACB is " + String.valueOf(Angle) + InfoCollector.getDegreeSign()
                + "Find the measure of bigger arc AB";
        RIGHT_ANSWER = String.valueOf(BiggerArc) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(BiggerArc + 10)) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(BiggerArc - 10)) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(BiggerArc + 20)) + InfoCollector.getDegreeSign();
    }

    //  TANGENT - TANGENT ANGLE
    //  Finding the x value: [(ax + b) - (cx + d)] / 2 = ANGLE
    private void createProblem_09(){
        int IntersectingAngle = MathUtility.getRandomAngleLessThan90Degree();
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomPositiveNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();
        int d_value = (a_value * x_value + b_value) - (2 * IntersectingAngle + c_value * x_value);

        QUESTION = "A, B are two points on the circle. The tangents at A and B intersect each other at C. "
                + "Given the measure of the angle " + InfoCollector.getAngleSign() + "ACB is " + String.valueOf(IntersectingAngle) + InfoCollector.getDegreeSign()
                + ", and the measure of the smaller and the bigger arc AB is: "
                + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", "
                + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ". "
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  SECANT - SECANT ANGLE
    private void createProblem_10(){
        int SmallerArc = MathUtility.getRandomAngleLessThan90Degree();
        int BiggerArc = MathUtility.getRandomAngleGreaterThan90Degree();

        double Angle = MathUtility.round_1_Decimal((double) (BiggerArc - SmallerArc) / 2.0);

        QUESTION = "A, B, C, D are on the circle. AB and DC intersect each other at E. "
                + "Find the measure of the angle " + InfoCollector.getAngleSign() + "E. "
                + "Given the measure of the arc AD is " + String.valueOf(BiggerArc) + InfoCollector.getDegreeSign()
                + ", and the measure of the arc BC is " + String.valueOf(SmallerArc) + InfoCollector.getDegreeSign() + ".";
        RIGHT_ANSWER = String.valueOf(Angle) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Angle + 10)) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Angle - 10)) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Angle + 20)) + InfoCollector.getDegreeSign();
    }

    //  SECANT - SECANT ANGLE
    private void createProblem_11(){
        int SmallerArc = MathUtility.getRandomAngleLessThan90Degree();
        int BiggerArc = MathUtility.getRandomAngleGreaterThan90Degree();

        double Angle = MathUtility.round_1_Decimal((double) (BiggerArc - SmallerArc) / 2.0);

        QUESTION = "A, B, C, D are on the circle. AB and DC intersect each other at E. "
                + "Find the measure of the arc BC. "
                + "Given the measure of the arc AD is " + String.valueOf(BiggerArc) + InfoCollector.getDegreeSign()
                + ", and the measure of the angle E is " + String.valueOf(Angle) + InfoCollector.getDegreeSign() + ".";
        RIGHT_ANSWER = String.valueOf(SmallerArc) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(SmallerArc + 10)) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(SmallerArc - 10)) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(SmallerArc + 20)) + InfoCollector.getDegreeSign();
    }

    //  SECANT - SECANT ANGLE
    //  Finding the x value: [(ax + b) - (cx + d)] / 2 = ANGLE
    private void createProblem_12(){
        int IntersectingAngle = MathUtility.getRandomAngleLessThan90Degree();
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomPositiveNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();
        int d_value = (a_value * x_value + b_value) - (2 * IntersectingAngle + c_value * x_value);

        QUESTION = "A, B, C, D are on the circle. AB and DC intersect each other at E. "
                + "Given the measure of the angle " + InfoCollector.getAngleSign() + "E is " + String.valueOf(IntersectingAngle) + InfoCollector.getDegreeSign()
                + ", and the measure of the arcs AD and BC are: "
                + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", "
                + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ". "
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  SECANT - TANGENT ANGLE
    private void createProblem_13(){
        int BiggerArc = MathUtility.getRandomAngleGreaterThan90Degree();
        int SmallerArc = MathUtility.getRandomAngleLessThan90Degree();

        double Angle = MathUtility.round_1_Decimal((double) (BiggerArc - SmallerArc) / 2.0);

        QUESTION = "A, B, C are 3 points on the circle. The tangents at C intersect with the secant AB at D. "
                + "Given the measure of the arc AC is " + String.valueOf(BiggerArc) + InfoCollector.getDegreeSign()
                + ", and the measure of the arc BC is " + String.valueOf(SmallerArc) + InfoCollector.getDegreeSign()
                + "Find the measure of the angle " + InfoCollector.getAngleSign() + "D.";
        RIGHT_ANSWER = String.valueOf(Angle) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Angle + 10)) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Angle - 10)) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Angle + 20)) + InfoCollector.getDegreeSign();
    }

    //  SECANT - TANGENT ANGLE
    private void createProblem_14(){
        int BiggerArc = MathUtility.getRandomAngleGreaterThan90Degree();
        int SmallerArc = MathUtility.getRandomAngleLessThan90Degree();

        double Angle = MathUtility.round_1_Decimal((double) (BiggerArc - SmallerArc) / 2.0);

        QUESTION = "A, B, C are 3 points on the circle. The tangents at C intersect with the secant AB at D. "
                + "Given the measure of the arc AC is " + String.valueOf(BiggerArc) + InfoCollector.getDegreeSign()
                + ", and the measure of the angle " + InfoCollector.getAngleSign() + "D is " + String.valueOf(Angle) + InfoCollector.getDegreeSign()
                + "Find the measure of the arc BC.";
        RIGHT_ANSWER = String.valueOf(SmallerArc) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(SmallerArc + 10)) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(SmallerArc - 10)) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(SmallerArc + 20)) + InfoCollector.getDegreeSign();
    }

    //  SECANT - TANGENT ANGLE
    //  Finding the x value: [(ax + b) - (cx + d)] / 2 = ANGLE
    private void createProblem_15(){
        int IntersectingAngle = MathUtility.getRandomAngleLessThan90Degree();
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomPositiveNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();
        int d_value = (a_value * x_value + b_value) - (2 * IntersectingAngle + c_value * x_value);

        QUESTION = "A, B, C are 3 points on the circle. The tangents at C intersect with the secant AB at D. "
                + "Given the measure of the angle " + InfoCollector.getAngleSign() + "D is " + String.valueOf(IntersectingAngle) + InfoCollector.getDegreeSign()
                + ", and the measure of the arcs AC and BC are: "
                + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", "
                + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ". "
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
