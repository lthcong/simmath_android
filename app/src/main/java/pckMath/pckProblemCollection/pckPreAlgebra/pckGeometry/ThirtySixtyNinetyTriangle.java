package pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckIntermediateMath.pckRoot.Root;

/**
 * Created by Cong on 6/12/2017.
 */

public class ThirtySixtyNinetyTriangle extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public ThirtySixtyNinetyTriangle(){
        createProblem();
        swapAnswer();
    }

    public ThirtySixtyNinetyTriangle(String Question,
                               String RightAnswer,
                               String AnswerA,
                               String AnswerB,
                               String AnswerC,
                               String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 3;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            default:
                createProblem_01();
                break;
        }
        return new ThirtySixtyNinetyTriangle(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  a: shorter leg     b: longer leg   c: hypotenuse

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        double b = MathUtility.round_2_Decimal((double) a * (new Root(2, 3)).getValue());
        int c = 2 * a;

        QUESTION = "Given a 30-60-90 triangle with the shorter leg is " + String.valueOf(a) + " inches. "
                + "Find the measure of the longer leg and the hypotenuse.";
        RIGHT_ANSWER = "Longer leg: " + String.valueOf(b) + " inches. Hypotenuse: " + String.valueOf(c) + " inches.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "Longer leg: " + String.valueOf(c) + " inches. Hypotenuse: " + String.valueOf(b) + " inches.";
        ANSWER_C = "Longer leg: " + String.valueOf(MathUtility.round_2_Decimal(b + 1.0)) + " inches. Hypotenuse: " + String.valueOf(c) + " inches.";
        ANSWER_D = "Longer leg: " + String.valueOf(b) + " inches. Hypotenuse: " + String.valueOf(c + 1) + " inches.";
    }

    private void createProblem_02(){
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        double a = MathUtility.round_2_Decimal((double) b / (new Root(2, 3)).getValue());
        double c = MathUtility.round_2_Decimal(2 * a);

        QUESTION = "Given a 30-60-90 triangle with the longer leg is " + String.valueOf(b) + " inches. "
                + "Find the measure of the shorter leg and the hypotenuse.";
        RIGHT_ANSWER = "Shorter leg: " + String.valueOf(a) + " inches. Hypotenuse: " + String.valueOf(c) + " inches.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "Shorter leg: " + String.valueOf(c) + " inches. Hypotenuse: " + String.valueOf(a) + " inches.";
        ANSWER_C = "Shorter leg: " + String.valueOf(MathUtility.round_2_Decimal(a + 1.0)) + " inches. Hypotenuse: " + String.valueOf(c) + " inches.";
        ANSWER_D = "Shorter leg: " + String.valueOf(a) + " inches. Hypotenuse: " + String.valueOf(MathUtility.round_2_Decimal(c + 1.0)) + " inches.";
    }

    private void createProblem_03(){
        int c = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        double a = MathUtility.round_1_Decimal((double) c / 2.0);
        double b = MathUtility.round_2_Decimal(a * (new Root(2, 3)).getValue());

        QUESTION = "Given a 30-60-90 triangle with the hypotenuse is " + String.valueOf(c) + " inches. "
                + "Find the measure of the shorter leg and the longer leg.";
        RIGHT_ANSWER = "Shorter leg: " + String.valueOf(a) + " inches. Longer leg: " + String.valueOf(b) + " inches.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "Shorter leg: " + String.valueOf(b) + " inches. Longer leg: " + String.valueOf(a) + " inches.";
        ANSWER_C = "Shorter leg: " + String.valueOf(MathUtility.round_2_Decimal(a + 1.0)) + " inches. Longer leg: " + String.valueOf(b) + " inches.";
        ANSWER_D = "Shorter leg: " + String.valueOf(a) + " inches. Longer leg: " + String.valueOf(MathUtility.round_2_Decimal(b + 1.0)) + " inches.";
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
