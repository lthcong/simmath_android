package pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry;

import java.util.ArrayList;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckString.StringUtility;

/**
 * Created by Cong on 6/9/2017.
 */

public class FindingTheMeasureOfALineOfSegment extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingTheMeasureOfALineOfSegment(){
        createProblem();
        swapAnswer();
    }

    public FindingTheMeasureOfALineOfSegment(String Question,
                        String RightAnswer,
                        String AnswerA,
                        String AnswerB,
                        String AnswerC,
                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 9;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            default:
                createProblem_01();
                break;
        }
        return new FindingTheMeasureOfALineOfSegment(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  AB + BC = ?
    private void createProblem_01(){
        int AB = MathUtility.getRandomPositiveNumber_1Digit();
        int BC = MathUtility.getRandomPositiveNumber_1Digit();
        int AC = AB + BC;

        QUESTION = "AB, BC, AC are three line of segments. AB + BC = AC. "
                + "We have AB = " + String.valueOf(AB) + ", BC = " + String.valueOf(BC)
                + ". Find the measure of AC.";
        RIGHT_ANSWER = String.valueOf(AC);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(AC - 1);
        ANSWER_C = String.valueOf(AC + 1);
        ANSWER_D = String.valueOf(AC + 2);
    }

    //  ? + BC = AC
    private void createProblem_02(){
        int AB = MathUtility.getRandomPositiveNumber_1Digit();
        int BC = MathUtility.getRandomPositiveNumber_1Digit();
        int AC = AB + BC;

        QUESTION = "AB, BC, AC are three line of segments. AB + BC = AC. "
                + "We have AC = " + String.valueOf(AC) + ", BC = " + String.valueOf(BC)
                + ". Find the measure of AB.";
        RIGHT_ANSWER = String.valueOf(AB);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(AB - 1);
        ANSWER_C = String.valueOf(AB + 1);
        ANSWER_D = String.valueOf(AB + 2);
    }

    //  AB + ? = AC
    private void createProblem_03(){
        int AB = MathUtility.getRandomPositiveNumber_1Digit();
        int BC = MathUtility.getRandomPositiveNumber_1Digit();
        int AC = AB + BC;

        QUESTION = "AB, BC, AC are three line of segments. AB + BC = AC. "
                + "We have AC = " + String.valueOf(AC) + ", AB = " + String.valueOf(AB)
                + ". Find the measure of AB.";
        RIGHT_ANSWER = String.valueOf(BC);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(BC - 1);
        ANSWER_C = String.valueOf(BC + 1);
        ANSWER_D = String.valueOf(BC + 2);
    }

    //  AB + BC = ?
    private void createProblem_04(){
        int AB = MathUtility.getRandomPositiveNumber_2Digit();
        int BC = MathUtility.getRandomPositiveNumber_1Digit();
        int AC = AB + BC;

        QUESTION = "AB, BC, AC are three line of segments. AB + BC = AC. "
                + "We have AB = " + String.valueOf(AB) + ", BC = " + String.valueOf(BC)
                + ". Find the measure of AC.";
        RIGHT_ANSWER = String.valueOf(AC);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(AC - 1);
        ANSWER_C = String.valueOf(AC + 1);
        ANSWER_D = String.valueOf(AC + 2);
    }

    //  ? + BC = AC
    private void createProblem_05(){
        int AB = MathUtility.getRandomPositiveNumber_1Digit();
        int BC = MathUtility.getRandomPositiveNumber_2Digit();
        int AC = AB + BC;

        QUESTION = "AB, BC, AC are three line of segments. AB + BC = AC. "
                + "We have AC = " + String.valueOf(AC) + ", BC = " + String.valueOf(BC)
                + ". Find the measure of AB.";
        RIGHT_ANSWER = String.valueOf(AB);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(AB - 1);
        ANSWER_C = String.valueOf(AB + 1);
        ANSWER_D = String.valueOf(AB + 2);
    }

    //  AB + ? = AC
    private void createProblem_06(){
        int AB = MathUtility.getRandomPositiveNumber_2Digit();
        int BC = MathUtility.getRandomPositiveNumber_2Digit();
        int AC = AB + BC;

        QUESTION = "AB, BC, AC are three line of segments. AB + BC = AC. "
                + "We have AC = " + String.valueOf(AC) + ", AB = " + String.valueOf(AB)
                + ". Find the measure of AB.";
        RIGHT_ANSWER = String.valueOf(BC);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(BC - 1);
        ANSWER_C = String.valueOf(BC + 1);
        ANSWER_D = String.valueOf(BC + 2);
    }

    //  FINDING x: (ax + b) + (cx + d) = AC
    private void createProblem_07(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomPositiveNumber_1Digit();
        int d_value = MathUtility.getRandomPositiveNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int AC = (a_value * x_value + b_value) + (c_value * x_value + d_value);

        QUESTION = "AB, BC, AC are three line of segments. AB + BC = AC. <br>"
                + "AB = " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ". "
                + "BC = " + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ".<br>"
                + "AC = " + String.valueOf(AC) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  FINDING x: AB + (ax + b) = (cx + d)
    private void createProblem_08(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomPositiveNumber_1Digit();
        int d_value = MathUtility.getRandomPositiveNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        if ((c_value * x_value + d_value) - (a_value * x_value + b_value) < 0){
            c_value = a_value + MathUtility.getRandomPositiveNumber_1Digit();
            d_value = b_value + MathUtility.getRandomPositiveNumber_1Digit();
        }
        int AB = (c_value * x_value + d_value) - (a_value * x_value + b_value);

        QUESTION = "AB, BC, AC are three line of segments. AB + BC = AC. <br>"
                + "AB = " + String.valueOf(AB)
                + ". BC = " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ".<br>"
                + "AC = " + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ".<br>"
                + "Find the x value.";

        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  FINDING x: (ax + b) + (cx + d) = (ex + f)
    private void createProblem_09(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomPositiveNumber_1Digit();
        int d_value = MathUtility.getRandomPositiveNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int AC = (a_value * x_value + b_value) + (c_value * x_value + d_value);

        int e_value = MathUtility.getRandomPositiveNumber_1Digit();
        int f_value = AC - x_value * e_value;

        QUESTION = "AB, BC, AC are three line of segments. AB + BC = AC. <br>"
                + "AB = " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ". "
                + "BC = " + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ".<br>"
                + "AC = " + String.valueOf(e_value) + "x + " + String.valueOf(f_value) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
