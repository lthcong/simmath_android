package pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckIntermediateMath.pckRoot.Root;

/**
 * Created by Cong on 6/12/2017.
 */

public class CircumferenceAndAreaOfCircle extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public CircumferenceAndAreaOfCircle(){
        createProblem();
        swapAnswer();
    }

    public CircumferenceAndAreaOfCircle(String Question,
                                             String RightAnswer,
                                             String AnswerA,
                                             String AnswerB,
                                             String AnswerC,
                                             String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 8;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }
        return new CircumferenceAndAreaOfCircle(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        double Cir = MathUtility.round_2_Decimal(2.0 * 3.14 * (double) radius);

        QUESTION = "Find the circumference of a circle with the radius is " + String.valueOf(radius)+ " inches.";
        RIGHT_ANSWER = String.valueOf(Cir);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Cir + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Cir + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Cir - 0.1));
    }

    private void createProblem_02(){
        int Cir = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        double radius = MathUtility.round_2_Decimal((double) Cir / (2.0 * 3.14));

        QUESTION = "Find the radius of a circle with the circumference is " + String.valueOf(Cir)+ " inches.";
        RIGHT_ANSWER = String.valueOf(radius);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(radius + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(radius + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(radius - 0.1));
    }

    private void createProblem_03(){
        int radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        double area = MathUtility.round_2_Decimal(3.14 * (double) (radius * radius));

        QUESTION = "Find the area of a circle with the radius is " + String.valueOf(radius)+ " inches.";
        RIGHT_ANSWER = String.valueOf(area);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(area + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(area + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(area - 0.1));
    }

    private void createProblem_04(){
        int area = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        double radius = MathUtility.round_2_Decimal(Math.pow((double) area / 3.14, 0.5));

        QUESTION = "Find the radius of a circle with the area is " + String.valueOf(area)+ " inches square.";
        RIGHT_ANSWER = String.valueOf(radius);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(radius + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(radius + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(radius - 0.1));
    }

    private void createProblem_05(){
        int diameter = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        double Cir = MathUtility.round_2_Decimal(2.0 * 3.14 * ((double) diameter / 2.0));

        QUESTION = "Find the circumference of a circle with the diameter is " + String.valueOf(diameter)+ " inches.";
        RIGHT_ANSWER = String.valueOf(Cir);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Cir + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Cir + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Cir - 0.1));
    }

    private void createProblem_06(){
        int Cir = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        double diameter = MathUtility.round_2_Decimal((double) Cir / 3.14);

        QUESTION = "Find the diameter of a circle with the circumference is " + String.valueOf(Cir)+ " inches.";
        RIGHT_ANSWER = String.valueOf(diameter);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(diameter + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(diameter + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(diameter - 0.1));
    }

    private void createProblem_07(){
        int diameter = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        double radius = (double) diameter / 2.0;
        double area = MathUtility.round_2_Decimal(3.14 * radius * radius);

        QUESTION = "Find the area of a circle with the diameter is " + String.valueOf(diameter)+ " inches.";
        RIGHT_ANSWER = String.valueOf(area);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(area + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(area + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(area - 0.1));
    }

    private void createProblem_08(){
        int area = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        double diameter = MathUtility.round_2_Decimal(Math.pow((double) area / 3.14, 0.5) * 2.0);

        QUESTION = "Find the diameter of a circle with the area is " + String.valueOf(area)+ " inches square.";
        RIGHT_ANSWER = String.valueOf(diameter);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(diameter + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(diameter + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(diameter - 0.1));
    }

    @Override
    public String getQuestion() {
        QUESTION = QUESTION + "<br>Use 3.14 for Pi.";
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
