package pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/9/2017.
 */

public class FindingTheMeasureOfAnAngle extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingTheMeasureOfAnAngle(){
        createProblem();
        swapAnswer();
    }

    public FindingTheMeasureOfAnAngle(String Question,
                                              String RightAnswer,
                                              String AnswerA,
                                              String AnswerB,
                                              String AnswerC,
                                              String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 20;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            case 14:
                createProblem_15();
                break;
            case 15:
                createProblem_16();
                break;
            case 16:
                createProblem_17();
                break;
            case 17:
                createProblem_18();
                break;
            case 18:
                createProblem_19();
                break;
            case 19:
                createProblem_20();
                break;
            default:
                createProblem_01();
                break;
        }
        return new FindingTheMeasureOfAnAngle(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  COMPLEMENT ANGLE
    private void createProblem_01(){
        int AngleA = MathUtility.getRandomAngleLessThan90Degree();
        int AngleB = 90 - AngleA;

        QUESTION = InfoCollector.getAngleSign() + "A and " + InfoCollector.getAngleSign() + "B are complement. " 
                + InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA) 
                + ". Find the measure of " + InfoCollector.getAngleSign() + "B.";
        RIGHT_ANSWER = String.valueOf(AngleB) + InfoCollector.getDegreeSign();
        
        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(AngleB + 1) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(AngleB - 1) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(AngleB + 2) + InfoCollector.getDegreeSign();
    }

    //  COMPLEMENT ANGLE
    private void createProblem_02(){
        int AngleA = MathUtility.getRandomAngleLessThan90Degree();
        int AngleB = 90 - AngleA;

        QUESTION = "Find 2 complement angles.";
        RIGHT_ANSWER = InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA) + InfoCollector.getDegreeSign() + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB) + InfoCollector.getDegreeSign() + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA + 1) + InfoCollector.getDegreeSign() + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB) + InfoCollector.getDegreeSign() + ".";
        ANSWER_C = InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA) + InfoCollector.getDegreeSign() + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB + 1) + InfoCollector.getDegreeSign() + ".";
        ANSWER_D = InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA - 1) + InfoCollector.getDegreeSign() + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB + 2) + InfoCollector.getDegreeSign() + ".";
    }

    /*  COMPLEMENT ANGLE    
    * Finding x: (ax + b) + (cx + d) = 90
    * */
    private void createProblem_03(){
        int a_value = MathUtility.getRandomNumber_1Digit();
        int b_value = MathUtility.getRandomNumber_1Digit();
        int c_value = MathUtility.getRandomNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int d_value = 90 - (a_value * x_value + b_value + c_value * x_value);

        QUESTION = InfoCollector.getAngleSign() + "A and " + InfoCollector.getAngleSign() + "B are complement.<br>"
                + InfoCollector.getAngleSign() + "A = " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ".<br>"
                + "Find the x value";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    /*  COMPLEMENT ANGLE    
    * Finding x: (ax + b) + B = 90
    * */
    private void createProblem_04(){
        int a_value = MathUtility.getRandomNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();
        
        int AngleB = MathUtility.getRandomAngleLessThan90Degree();
        
        int b_value = 90 - (a_value * x_value + AngleB);

        QUESTION = InfoCollector.getAngleSign() + "A and " + InfoCollector.getAngleSign() + "B are complement.<br>"
                + InfoCollector.getAngleSign() + "A = " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB) + ".<br>"
                + "Find the x value";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  SUPPLEMENT ANGLE
    private void createProblem_05(){
        int AngleA = MathUtility.getRandomAngleLessThan90Degree();
        int AngleB = 180 - AngleA;

        QUESTION = InfoCollector.getAngleSign() + "A and " + InfoCollector.getAngleSign() + "B are supplement. "
                + InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA)
                + ". Find the measure of " + InfoCollector.getAngleSign() + "B.";
        RIGHT_ANSWER = String.valueOf(AngleB) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(AngleB + 1) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(AngleB - 1) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(AngleB + 2) + InfoCollector.getDegreeSign();
    }

    //  SUPPLEMENT ANGLE
    private void createProblem_06(){
        int AngleA = MathUtility.getRandomAngleGreaterThan90Degree();
        int AngleB = 180 - AngleA;

        QUESTION = InfoCollector.getAngleSign() + "A and " + InfoCollector.getAngleSign() + "B are supplement. "
                + InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA)
                + ". Find the measure of " + InfoCollector.getAngleSign() + "B.";
        RIGHT_ANSWER = String.valueOf(AngleB) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(AngleB + 1) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(AngleB - 1) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(AngleB + 2) + InfoCollector.getDegreeSign();
    }
    
    //  SUPPLEMENT ANGLE
    private void createProblem_07(){
        int AngleA = MathUtility.getRandomAngleLessThan90Degree();
        int AngleB = 180 - AngleA;

        QUESTION = "Find 2 supplement angles.";
        RIGHT_ANSWER = InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA) + InfoCollector.getDegreeSign() + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB) + InfoCollector.getDegreeSign() + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA + 1) + InfoCollector.getDegreeSign() + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB) + InfoCollector.getDegreeSign() + ".";
        ANSWER_C = InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA) + InfoCollector.getDegreeSign() + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB + 1) + InfoCollector.getDegreeSign() + ".";
        ANSWER_D = InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA - 1) + InfoCollector.getDegreeSign() + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB + 2) + InfoCollector.getDegreeSign() + ".";
    }

    /*  SUPPLEMENT ANGLE    
    * Finding x: (ax + b) + (cx + d) = 180
    * */
    private void createProblem_08(){
        int a_value = MathUtility.getRandomNumber_1Digit();
        int b_value = MathUtility.getRandomNumber_1Digit();
        int c_value = MathUtility.getRandomNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int d_value = 180 - (a_value * x_value + b_value + c_value * x_value);

        QUESTION = InfoCollector.getAngleSign() + "A and " + InfoCollector.getAngleSign() + "B are supplement.<br>"
                + InfoCollector.getAngleSign() + "A = " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ".<br>"
                + "Find the x value";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    /*  SUPPLEMENT ANGLE    
    * Finding x: (ax + b) + B = 180
    * */
    private void createProblem_09(){
        int a_value = MathUtility.getRandomNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int AngleB = MathUtility.getRandomAngleLessThan90Degree();

        int b_value = 180 - (a_value * x_value + AngleB);

        QUESTION = InfoCollector.getAngleSign() + "A and " + InfoCollector.getAngleSign() + "B are supplement.<br>"
                + InfoCollector.getAngleSign() + "A = " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB) + ".<br>"
                + "Find the x value";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  SUPPLEMENT ANGLE
    private void createProblem_10(){
        int AngleA = MathUtility.getRandomAngleGreaterThan90Degree();
        int AngleB = 180 - AngleA;

        QUESTION = "Find 2 supplement angles.";
        RIGHT_ANSWER = InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA) + InfoCollector.getDegreeSign() + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB) + InfoCollector.getDegreeSign() + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA + 1) + InfoCollector.getDegreeSign() + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB) + InfoCollector.getDegreeSign() + ".";
        ANSWER_C = InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA) + InfoCollector.getDegreeSign() + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB + 1) + InfoCollector.getDegreeSign() + ".";
        ANSWER_D = InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA - 1) + InfoCollector.getDegreeSign() + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB + 2) + InfoCollector.getDegreeSign() + ".";
    }

    /*  SUPPLEMENT ANGLE
    * Finding x: (ax + b) + B = 180
    * */
    private void createProblem_11(){
        int a_value = MathUtility.getRandomNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int AngleB = MathUtility.getRandomAngleGreaterThan90Degree();

        int b_value = 180 - (a_value * x_value + AngleB);

        QUESTION = InfoCollector.getAngleSign() + "A and " + InfoCollector.getAngleSign() + "B are supplement.<br>"
                + InfoCollector.getAngleSign() + "A = " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB) + ".<br>"
                + "Find the x value";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    /*  RANDOM ANGLE */
    private void createProblem_12(){
        int AngleA = MathUtility.getRandomAngle();
        int AngleB = MathUtility.getRandomAngle();
        int AngleC = AngleA + AngleB;

        QUESTION = InfoCollector.getAngleSign() + "A + " + InfoCollector.getAngleSign() + "B = " + InfoCollector.getAngleSign() + "C.<br>"
                + InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA) + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB)
                + ". Find the measure of " + InfoCollector.getAngleSign() + "C.";
        RIGHT_ANSWER = String.valueOf(AngleC) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(AngleC + 1) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(AngleC - 1) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(AngleC + 2) + InfoCollector.getDegreeSign();
    }

    /*  RANDOM ANGLE */
    private void createProblem_13(){
        int AngleA = MathUtility.getRandomAngle();
        int AngleB = MathUtility.getRandomAngle();
        int AngleC = AngleA + AngleB;

        QUESTION = InfoCollector.getAngleSign() + "A + " + InfoCollector.getAngleSign() + "B = " + InfoCollector.getAngleSign() + "C.<br>"
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB) + ", "
                + InfoCollector.getAngleSign() + "C = " + String.valueOf(AngleC)
                + ". Find the measure of " + InfoCollector.getAngleSign() + "A.";
        RIGHT_ANSWER = String.valueOf(AngleA) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(AngleA + 1) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(AngleA - 1) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(AngleA + 2) + InfoCollector.getDegreeSign();
    }

    /*  RANDOM ANGLE
    *   Finding x value: ax + b + B = C
    * */
    private void createProblem_14(){
        int a_value = MathUtility.getRandomNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomNumber_1Digit();

        int AngleB = MathUtility.getRandomAngle();
        int AngleC = a_value * x_value + b_value + AngleB;

        QUESTION = InfoCollector.getAngleSign() + "A + " + InfoCollector.getAngleSign() + "B = " + InfoCollector.getAngleSign() + "C.<br>"
                + InfoCollector.getAngleSign() + "A = " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ".<br>"
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(AngleB) + ", "
                + InfoCollector.getAngleSign() + "C = " + String.valueOf(AngleC) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    /*  RANDOM ANGLE
    *   Finding x value: (ax + b) + (cx + d) = C
    * */
    private void createProblem_15(){
        int a_value = MathUtility.getRandomNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomNumber_1Digit();
        int c_value = MathUtility.getRandomNumber_1Digit();
        int d_value = MathUtility.getRandomNumber_1Digit();

        int AngleC = (a_value * x_value + b_value) + (c_value * x_value + d_value);

        QUESTION = InfoCollector.getAngleSign() + "A + " + InfoCollector.getAngleSign() + "B = " + InfoCollector.getAngleSign() + "C.<br>"
                + InfoCollector.getAngleSign() + "A = " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ".<br>"
                + InfoCollector.getAngleSign() + "C = " + String.valueOf(AngleC) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    /*  RANDOM ANGLE
    *   Finding x value: (ax + b) + (cx + d) = (ex + f)
    * */
    private void createProblem_16(){
        int a_value = MathUtility.getRandomNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomNumber_1Digit();
        int c_value = MathUtility.getRandomNumber_1Digit();
        int d_value = MathUtility.getRandomNumber_1Digit();
        int e_value = MathUtility.getRandomPositiveNumber_1Digit();

        int f_value = (a_value * x_value + b_value) + (c_value * x_value + d_value) - (e_value * x_value);

        QUESTION = InfoCollector.getAngleSign() + "A + " + InfoCollector.getAngleSign() + "B = " + InfoCollector.getAngleSign() + "C.<br>"
                + InfoCollector.getAngleSign() + "A = " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ".<br>"
                + InfoCollector.getAngleSign() + "C = " + String.valueOf(e_value) + "x + " + String.valueOf(f_value) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    /*  VERTICAL ANGLE */
    private void createProblem_17(){
        int AngleA = MathUtility.getRandomAngle();

        QUESTION = InfoCollector.getAngleSign() + "A and " + InfoCollector.getAngleSign() + "B are vertical angles.<br>"
                + InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA)
                + ". Find the measure of " + InfoCollector.getAngleSign() + "B.";
        RIGHT_ANSWER = String.valueOf(AngleA) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(AngleA + 1) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(AngleA - 1) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(AngleA + 2) + InfoCollector.getDegreeSign();
    }

    /*  VERTICAL ANGLE */
    private void createProblem_18(){
        int AngleA = MathUtility.getRandomAngle();
        int Result = 180 - AngleA;

        QUESTION = InfoCollector.getAngleSign() + "A and " + InfoCollector.getAngleSign() + "B are vertical angles.<br>"
                + InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA)
                + ". Find the measure of the supplement angle of " + InfoCollector.getAngleSign() + "B.";
        RIGHT_ANSWER = String.valueOf(Result) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(Result - 1) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(Result + 2) + InfoCollector.getDegreeSign();
    }

    /*  VERTICAL ANGLE */
    private void createProblem_19(){
        int AngleA = MathUtility.getRandomAngle();
        int Result = 90 - AngleA;

        QUESTION = InfoCollector.getAngleSign() + "A and " + InfoCollector.getAngleSign() + "B are vertical angles.<br>"
                + InfoCollector.getAngleSign() + "A = " + String.valueOf(AngleA)
                + ". Find the measure of the complement angle of " + InfoCollector.getAngleSign() + "B.";
        RIGHT_ANSWER = String.valueOf(Result) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(Result - 1) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(Result + 2) + InfoCollector.getDegreeSign();
    }

    /*  VERTICAL ANGLE
    *   Finding the x value: ax + b = cx + d
    * */
    private void createProblem_20(){
        int a_value = MathUtility.getRandomNumber_1Digit();
        int b_value = MathUtility.getRandomNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int c_value = MathUtility.getRandomNumber_1Digit();
        int d_value = (a_value * x_value + b_value) - (c_value * x_value);

        QUESTION = InfoCollector.getAngleSign() + "A and " + InfoCollector.getAngleSign() + "B are vertical angles.<br>"
                + InfoCollector.getAngleSign() + "A = " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", "
                + InfoCollector.getAngleSign() + "B = " + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
