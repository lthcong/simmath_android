package pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/24/2017.
 */

public class VolumeOfIrregularSolid extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public VolumeOfIrregularSolid(){
        createProblem();
        swapAnswer();
    }

    public VolumeOfIrregularSolid(String Question,
                                String RightAnswer,
                                String AnswerA,
                                String AnswerB,
                                String AnswerC,
                                String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 11;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            default:
                createProblem_01();
                break;
        }

        return new VolumeOfIrregularSolid(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  RECTANGULAR - SPHERE
    private void createProblem_01(){
        int radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int length = MathUtility.getRandomPositiveNumber_1Digit() + radius;
        int width = MathUtility.getRandomPositiveNumber_1Digit() + radius;
        int height = MathUtility.getRandomPositiveNumber_1Digit() + radius;

        double Volume = (double) (length * width * height) - (double) (radius * radius * radius) * (4.0 / 3.0 * 3.14);
        Volume = MathUtility.round_2_Decimal(Volume);

        QUESTION = "A sphere (radius = " + String.valueOf(radius) + "cm) is placed inside a rectangular solid ("
                + String.valueOf(length) + "cm x " + String.valueOf(width) + "cm x " + String.valueOf(height) + "cm). "
                + "Find the volume outside the sphere of the rectangular solid.";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume - 15.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "cm" + InfoCollector.getExponentSign("3");
    }

    //  CUBE - SPHERE
    private void createProblem_02(){
        int radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int side = MathUtility.getRandomPositiveNumber_1Digit() + radius;

        double Volume = (double) (side * side * side) - (double) (radius * radius * radius) * (4.0 / 3.0 * 3.14);
        Volume = MathUtility.round_2_Decimal(Volume);

        QUESTION = "A sphere (radius = " + String.valueOf(radius) + "cm) is placed inside a cube with the side is "
                + String.valueOf(side) + "cm. Find the volume outside the sphere of the cube.";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume - 15.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "cm" + InfoCollector.getExponentSign("3");
    }

    //  SPHERE - CYLINDER
    private void createProblem_03(){
        int c_radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int c_height = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int s_radius = MathUtility.getRandomPositiveNumber_1Digit() + c_height + c_radius;

        double Volume = (double) (s_radius * s_radius * s_radius) * (4.0 / 3.0 * 3.14)
                - 3.14 * (double) (c_radius * c_radius * c_height);
        Volume = MathUtility.round_2_Decimal(Volume);

        QUESTION = "A cylinder (r = " + String.valueOf(c_radius) + "cm, h = " + String.valueOf(c_height) + "cm) is placed inside "
                + "a sphere (r = " + String.valueOf(s_radius) + "cm). "
                + "Find the volume outside the cylinder of the radius.";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume - 15.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "cm" + InfoCollector.getExponentSign("3");
    }

    //  CYLINDER - SPHERE
    private void createProblem_04(){
        int s_radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int c_radius = MathUtility.getRandomPositiveNumber_1Digit() + s_radius;
        int c_height = MathUtility.getRandomPositiveNumber_1Digit() + s_radius;

        double Volume = 3.14 * (double) (c_radius * c_radius * c_height)
                - (double) (s_radius * s_radius * s_radius) * (4.0 / 3.0 * 3.14);
        Volume = MathUtility.round_2_Decimal(Volume);

        QUESTION = "A cylinder can (r = " + String.valueOf(c_radius) + "cm, h = " + String.valueOf(c_height) + "cm) "
                + "is filled with water. Tom places a ball (r = " + String.valueOf(s_radius) + "cm) "
                + "into the can. That makes an amount of water come out. Find the volume of the remaining water.";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume - 15.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "cm" + InfoCollector.getExponentSign("3");
    }

    //  CONE - SPHERE
    private void createProblem_05(){
        int s_radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int c_height = MathUtility.getRandomPositiveNumber_1Digit() + s_radius;
        int c_radius = MathUtility.getRandomPositiveNumber_1Digit() + s_radius;

        double Volume = (double) (c_radius * c_radius * c_height) * (3.14 / 3.0)
                - (double) (s_radius * s_radius * s_radius) * (4.0 / 3.0 * 3.14);
        Volume = MathUtility.round_2_Decimal(Volume);

        QUESTION = "A sphere (r = " + String.valueOf(s_radius) + "cm) is placed inside a cone (r = " + String.valueOf(c_radius) + "cm, h = " + String.valueOf(c_height) + "cm). "
                + "Find the volume outside the sphere of the cone.";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume - 15.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "cm" + InfoCollector.getExponentSign("3");
    }

    //  CYLINDER - CONE
    private void createProblem_06(){
        int cone_radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int cone_height = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int cylinder_radius = MathUtility.getRandomPositiveNumber_1Digit() + cone_radius;
        int cylinder_height = MathUtility.getRandomPositiveNumber_1Digit() + cone_height;

        double Volume = (double) (cylinder_radius * cylinder_radius * cylinder_height) * 3.14
                - (double) (cone_radius * cone_radius * cone_height) * (3.14 / 3.0);
        Volume = MathUtility.round_2_Decimal(Volume);

        QUESTION = "A cone (r = " + String.valueOf(cone_radius) + "cm, h = " + String.valueOf(cone_height) + "cm) is placed "
                + "inside a cylinder (r = " + String.valueOf(cylinder_radius) + "cm, h = " + String.valueOf(cylinder_height) + "cm). "
                + "Find the volume outside the cone of the cylinder.";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume - 15.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "cm" + InfoCollector.getExponentSign("3");
    }

    //  CYLINDER - CUBE
    private void createProblem_07(){
        int side = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int radius = MathUtility.getRandomPositiveNumber_1Digit() + side;
        int height = MathUtility.getRandomPositiveNumber_1Digit() + side;

        double Volume = 3.14 * (double) (radius * radius * height)
                - (double) (side * side * side) * (4.0 / 3.0 * 3.14);
        Volume = MathUtility.round_2_Decimal(Volume);

        QUESTION = "A cylinder can (r = " + String.valueOf(radius) + "cm, h = " + String.valueOf(height) + "cm) "
                + "is filled with water. Terry places a cube (r = " + String.valueOf(side) + "cm) "
                + "into the can. That makes an amount of water come out. Find the volume of the remaining water.";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume - 15.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "cm" + InfoCollector.getExponentSign("3");
    }

    //  SPHERE - SPHERE
    private void createProblem_08(){
        int small_radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int long_radius = MathUtility.getRandomPositiveNumber_1Digit() + small_radius;

        double Volume = (4.0 / 3.0 * 3.14) * (double) (long_radius * long_radius * long_radius - small_radius * small_radius * small_radius);
        Volume = MathUtility.round_2_Decimal(Volume);

        QUESTION = "A sphere (r = " + String.valueOf(small_radius) + "cm) is placed inside another sphere (r = " + String.valueOf(long_radius) + "cm). "
                + "Find the volume between the spheres.";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume - 15.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "cm" + InfoCollector.getExponentSign("3");
    }

    //  RECTANGULAR - PYRAMID
    private void createProblem_09(){
        int p_side = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int p_height = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int r_length = MathUtility.getRandomPositiveNumber_1Digit() + p_side;
        int r_width = MathUtility.getRandomPositiveNumber_1Digit() + p_side;
        int r_height = MathUtility.getRandomPositiveNumber_1Digit() + p_height;

        double Volume = (double) (r_length * r_width * r_height)
                - (double) (p_side * p_side * p_height) / 3.0;
        Volume = MathUtility.round_2_Decimal(Volume);

        QUESTION = "Tom wants to decorate his fish tank ("
                + String.valueOf(r_length) + "cm x " + String.valueOf(r_width) + "cm x " + String.valueOf(r_height) + "cm)"
                + "by putting a square pyramid (side = " + String.valueOf(p_side) + "cm x " + String.valueOf(p_height) + "cm) into it. "
                + "How much water does he need to fill the tank with the pyramid?";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume - 15.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "cm" + InfoCollector.getExponentSign("3");

    }

    //  CYLINDER - CYLINDER
    private void createProblem_10(){
        int height_1 = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int radius_1 = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int height_2 = MathUtility.getRandomPositiveNumber_1Digit() + height_1;
        int radius_2 = MathUtility.getRandomPositiveNumber_1Digit() + radius_1;

        double Volume = (double) (radius_2 * radius_2 * height_2) * 3.14
                - (double) (radius_1 * radius_1 * height_1) * 3.14;
        Volume = MathUtility.round_2_Decimal(Volume);

        QUESTION = "Jame place a pop can (r = " + String.valueOf(radius_1) + "cm, h = " + String.valueOf(height_1) + "cm) "
                + "into a cylinder glass. Find the remaining volume of the glass that can be filled with water? "
                + "Given the dimension of the glass is r = " + String.valueOf(radius_1) + "cm, h = " + String.valueOf(height_1) + "cm.";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume - 15.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "cm" + InfoCollector.getExponentSign("3");
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
