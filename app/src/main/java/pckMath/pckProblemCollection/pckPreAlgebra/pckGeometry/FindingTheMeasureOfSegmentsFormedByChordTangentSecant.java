package pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 8/29/2017.
 */

public class FindingTheMeasureOfSegmentsFormedByChordTangentSecant extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingTheMeasureOfSegmentsFormedByChordTangentSecant(){
        createProblem();
        swapAnswer();
    }

    public FindingTheMeasureOfSegmentsFormedByChordTangentSecant(String Question,
                        String RightAnswer,
                        String AnswerA,
                        String AnswerB,
                        String AnswerC,
                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 9;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            default:
                createProblem_01();
                break;
        }
        return new FindingTheMeasureOfSegmentsFormedByChordTangentSecant(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int c = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int d = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        double b = (double) (c * d) / (double) a;
        b = MathUtility.round_2_Decimal(b);

        QUESTION = "The two chords AB and CD of an circle intersect each other at E. "
                + "Given AE = " + String.valueOf(a) + "cm, CE = " + String.valueOf(c) + "cm, "
                + "DE = " + String.valueOf(d) + ". Find the measure of BE.";
        RIGHT_ANSWER = String.valueOf(b) + "cm.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(b + 1)) + "cm.";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(b + 1.5)) + "cm.";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(b - 1)) + "cm.";
    }

    private void createProblem_02(){
        int r = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int a = MathUtility.getRandomPositiveNumber_1Digit() + r;
        int c = MathUtility.getRandomPositiveNumber_1Digit() + r;
        int d = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        c = c + d;

        double b = (double) (c * d) / (double) a;
        b = MathUtility.round_2_Decimal(b);

        QUESTION = "A, B, C, D are 4 points on the circle. AB and CD intersect each other at E (outside the circle). "
                + "Given EA = " + String.valueOf(a) + "cm, EC = " + String.valueOf(c) + "cm, "
                + "ED = " + String.valueOf(d) + "cm. Find the measure of EB.";
        RIGHT_ANSWER = String.valueOf(b) + "cm.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(b + 1)) + "cm.";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(b + 1.5)) + "cm.";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(b - 1)) + "cm.";
    }

    private void createProblem_03(){
        int r = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int a = MathUtility.getRandomPositiveNumber_1Digit() + r;
        int c = MathUtility.getRandomPositiveNumber_1Digit() + r;
        int d = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        c = c + d;

        double b = (double) (c * d) / (double) a;
        b = MathUtility.round_2_Decimal(b);

        double result = Math.abs((double) a - b);

        QUESTION = "A, B, C, D are 4 points on the circle. AB and CD intersect each other at E (outside the circle). "
                + "Given EA = " + String.valueOf(a) + "cm, EC = " + String.valueOf(c) + "cm, "
                + "ED = " + String.valueOf(d) + "cm. Find the measure of AB.";
        RIGHT_ANSWER = String.valueOf(result) + "cm.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(result + 1)) + "cm.";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(result + 1.5)) + "cm.";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(result - 1)) + "cm.";
    }

    private void createProblem_04(){
        int r = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int a = MathUtility.getRandomPositiveNumber_1Digit() + r;
        int c = MathUtility.getRandomPositiveNumber_1Digit() + r;
        int d = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        c = c + d;

        double b = (double) (c * d) / (double) a;
        b = MathUtility.round_2_Decimal(b);

        QUESTION = "A, B, C, D are 4 points on the circle. AB and CD intersect each other at E (outside the circle). "
                + "Given EC = " + String.valueOf(a) + "cm, EA = " + String.valueOf(c) + "cm, "
                + "EB = " + String.valueOf(d) + "cm. Find the measure of ED.";
        RIGHT_ANSWER = String.valueOf(b) + "cm.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(b + 1)) + "cm.";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(b + 1.5)) + "cm.";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(b - 1)) + "cm.";
    }

    private void createProblem_05(){
        int r = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int a = MathUtility.getRandomPositiveNumber_1Digit() + r;
        int c = MathUtility.getRandomPositiveNumber_1Digit() + r;
        int d = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        c = c + d;

        double b = (double) (c * d) / (double) a;
        b = MathUtility.round_2_Decimal(b);

        double result = Math.abs((double) a - b);

        QUESTION = "A, B, C, D are 4 points on the circle. AB and CD intersect each other at E (outside the circle). "
                + "Given EC = " + String.valueOf(a) + "cm, EA = " + String.valueOf(c) + "cm, "
                + "EB = " + String.valueOf(d) + "cm. Find the measure of CD.";
        RIGHT_ANSWER = String.valueOf(result) + "cm.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(result + 1)) + "cm.";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(result + 1.5)) + "cm.";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(result - 1)) + "cm.";
    }

    private void createProblem_06(){
        int r = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + r;

        double c = (double) (a * a) / (double) (b);
        c = MathUtility.round_2_Decimal(c);

        QUESTION = "A, B, C are 3 points on the circle. The tangent at C intersect with AB at D (outside the circle). "
                + "Given CD = " + String.valueOf(a) + "cm, DA = " + String.valueOf(b) + "cm. "
                + "Find the measure of DB.";
        RIGHT_ANSWER = String.valueOf(c) + "cm.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(c + 1)) + "cm.";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(c + 1.5)) + "cm.";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(c - 1)) + "cm.";
    }

    private void createProblem_07(){
        int r = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + r;

        double c = (double) (a * a) / (double) (b);
        c = MathUtility.round_2_Decimal(c);

        QUESTION = "A, B, C are 3 points on the circle. The tangent at C intersect with AB at D (outside the circle). "
                + "Given CD = " + String.valueOf(a) + "cm, DB = " + String.valueOf(b) + "cm. "
                + "Find the measure of DA.";
        RIGHT_ANSWER = String.valueOf(c) + "cm.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(c + 1)) + "cm.";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(c + 1.5)) + "cm.";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(c - 1)) + "cm.";
    }

    private void createProblem_08(){
        int r = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int c = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + r;

        double a = (double) (b * c);
        a = MathUtility.round_2_Decimal(Math.sqrt(a));

        QUESTION = "A, B, C are 3 points on the circle. The tangent at C intersect with AB at D (outside the circle). "
                + "Given DA = " + String.valueOf(b) + "cm, DB = " + String.valueOf(c) + "cm. "
                + "Find the measure of DC.";
        RIGHT_ANSWER = String.valueOf(c) + "cm.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(c + 1)) + "cm.";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(c + 1.5)) + "cm.";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(c - 1)) + "cm.";
    }

    private void createProblem_09(){
        int r = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + r;

        double c = (double) (a * a) / (double) (b);
        c = MathUtility.round_2_Decimal(c);

        double result = Math.abs((double) b - c);

        QUESTION = "A, B, C are 3 points on the circle. The tangent at C intersect with AB at D (outside the circle). "
                + "Given DC = " + String.valueOf(a) + "cm, DB = " + String.valueOf(b) + "cm. "
                + "Find the measure of AB.";
        RIGHT_ANSWER = String.valueOf(result) + "cm.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(result + 1)) + "cm.";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(result + 1.5)) + "cm.";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(result - 1)) + "cm.";
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
