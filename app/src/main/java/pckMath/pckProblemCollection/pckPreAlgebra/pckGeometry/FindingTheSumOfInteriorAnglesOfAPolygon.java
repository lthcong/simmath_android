package pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/9/2017.
 */

public class FindingTheSumOfInteriorAnglesOfAPolygon extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingTheSumOfInteriorAnglesOfAPolygon(){
        createProblem();
        swapAnswer();
    }

    public FindingTheSumOfInteriorAnglesOfAPolygon(String Question,
                                              String RightAnswer,
                                              String AnswerA,
                                              String AnswerB,
                                              String AnswerC,
                                              String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 4;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            default:
                createProblem_01();
                break;
        }
        return new FindingTheSumOfInteriorAnglesOfAPolygon(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  SUM OF INTERIOR ANGLES
    private void createProblem_01(){
        int Side = MathUtility.getRandomPositiveNumber_1Digit() + 4;
        int SumOfAngle = (Side - 2) * 180;

        QUESTION = "Find the sum of the interior angles of a regular polygon with " + String.valueOf(Side) + " sides.";
        RIGHT_ANSWER = String.valueOf(SumOfAngle);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(SumOfAngle + 1);
        ANSWER_C = String.valueOf(SumOfAngle + 2);
        ANSWER_D = String.valueOf(SumOfAngle - 1);
    }

    //  ONE INTERIOR ANGLE
    private void createProblem_02(){
        int Side = MathUtility.getRandomPositiveNumber_1Digit() + 4;
        double OneAngle = MathUtility.round_2_Decimal((double) ((Side - 2) * 180) / (double) Side);

        QUESTION = "Find the measure of an interior angle of a regular polygon with " + String.valueOf(Side) + " sides.";
        RIGHT_ANSWER = String.valueOf(OneAngle);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(OneAngle + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(OneAngle - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(OneAngle + 20.0));
    }

    //  NUMBER OF THE SIDES
    private void createProblem_03(){
        int Side = MathUtility.getRandomPositiveNumber_1Digit() + 4;
        int SumOfAngle = (Side - 2) * 180;

        QUESTION = "Find the number of the sides of a regular polygon with the sum of the interior angles is "
                + String.valueOf(SumOfAngle) + ".";
        RIGHT_ANSWER = String.valueOf(Side);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Side + 1);
        ANSWER_C = String.valueOf(Side + 2);
        ANSWER_D = String.valueOf(Side - 1);
    }

    //  NUMBER OF THE SIDES
    private void createProblem_04(){
        int Side = MathUtility.getRandomPositiveNumber_1Digit() + 4;
        double OneAngle = MathUtility.round_2_Decimal((double) ((Side - 2) * 180) / (double) Side);

        QUESTION = "Find the number of the sides of a regular polygon, give the measure of a interior angle is " + String.valueOf(OneAngle) + ".";
        RIGHT_ANSWER = String.valueOf(Side);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Side + 1);
        ANSWER_C = String.valueOf(Side - 1);
        ANSWER_D = String.valueOf(Side + 2);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
