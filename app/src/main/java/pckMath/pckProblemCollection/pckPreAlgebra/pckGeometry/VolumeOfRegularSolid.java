package pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/24/2017.
 */

public class VolumeOfRegularSolid extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public VolumeOfRegularSolid(){
        createProblem();
        swapAnswer();
    }

    public VolumeOfRegularSolid(String Question,
                                     String RightAnswer,
                                     String AnswerA,
                                     String AnswerB,
                                     String AnswerC,
                                     String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 11;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            default:
                createProblem_01();
                break;
        }

        return new VolumeOfRegularSolid(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  RECTANGULAR SOLID
    private void createProblem_01(){
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int Volume = length * width * height;

        QUESTION = "Find the volume of a rectangular solid. Given the length is " + String.valueOf(length) + "cm, "
                + "the width is " + String.valueOf(width) + ", and the height is " + String.valueOf(height) + "cm.";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Volume + 10) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(Volume + 20) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(Volume - 10) + "cm" + InfoCollector.getExponentSign("3");
    }

    //  CUBE
    private void createProblem_02(){
        int side = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int Volume = side * side * side;

        QUESTION = "Find the volume of a cube. Given the side is " + String.valueOf(side) + "cm.";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Volume + 10) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(Volume + 20) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(Volume - 10) + "cm" + InfoCollector.getExponentSign("3");
    }

    //  CYLINDER
    private void createProblem_03(){
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double Volume = 3.14 * (double) (radius * radius * height);
        Volume = MathUtility.round_2_Decimal(Volume);

        QUESTION = "Find the volume of a cylinder. Given the height is " + String.valueOf(height) + "cm, "
                + "and the base radius is " + String.valueOf(radius) + "cm.";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume + 20.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "cm" + InfoCollector.getExponentSign("3");
    }

    //  SPHERE
    private void createProblem_04(){
        int radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        double Volume = (4.0 / 3.0) * 3.14 * (double) (radius * radius * radius);

        QUESTION = "Find the volume of a sphere. Given the radius is " + String.valueOf(radius) + "cm.";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume + 20.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "cm" + InfoCollector.getExponentSign("3");
    }

    //  CONE
    private void createProblem_05(){
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double Volume = (1.0 / 3.0) * 3.14 * (double) (radius * radius * height);
        Volume = MathUtility.round_2_Decimal(Volume);

        QUESTION = "Find the volume of a circular cone. Given the height is " + String.valueOf(height) + "cm, "
                + "and the base radius is " + String.valueOf(radius) + "cm.";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume + 20.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "cm" + InfoCollector.getExponentSign("3");
    }

    //  SQUARE PYRAMID
    private void createProblem_06(){
        int side = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double Volume = (double) (side * side * height) / 3.0;
        Volume = MathUtility.round_2_Decimal(Volume);

        QUESTION = "Find the volume of a square pyramid. Given the height is " + String.valueOf(height) + "cm, "
                + "and the side is " + String.valueOf(side) + "cm.";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume + 20.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "cm" + InfoCollector.getExponentSign("3");
    }

    //  RECTANGULAR PYRAMID
    private void createProblem_07(){
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double Volume = (double) (length * width * height) / 3.0;
        Volume = MathUtility.round_2_Decimal(Volume);

        QUESTION = "Find the volume of a rectangular pyramid. Given the length is " + String.valueOf(length) + "cm, "
                + "the width is " + String.valueOf(width) + ", and the height is " + String.valueOf(height) + "cm.";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume + 20.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "cm" + InfoCollector.getExponentSign("3");
    }

    //  FRUSTUM OF RIGHT CIRCULAR CONE
    private void createProblem_08(){
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int small_radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int long_radius = MathUtility.getRandomPositiveNumber_1Digit() + small_radius;

        double Volume = (3.14 / 3.0) * (double) ((small_radius * small_radius + small_radius * long_radius + long_radius * long_radius) * height);
        Volume = MathUtility.round_2_Decimal(Volume);

        QUESTION = "Find the volume of a frustum of a right circular cone. Given the radius are: "
                + String.valueOf(small_radius) + "cm, " + String.valueOf(long_radius) + "cm, "
                + "and the height is " + String.valueOf(height) + "cm.";
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.5)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "cm" + InfoCollector.getExponentSign("3");
    }

    //  TRIANGULAR PYRAMID
    private void createProblem_09(){
        int base = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int Height = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double Volume = (double) (base * height * Height) / 6.0;
        Volume = MathUtility.round_2_Decimal(Volume);

        QUESTION = "Find the volume of a triangular pyramid. Given the base and the height of the base are: "
                + String.valueOf(base) + "cm, " + String.valueOf(height) + "cm, and the pyramid height is "
                + String.valueOf(Height) + "cm." ;
        RIGHT_ANSWER = String.valueOf(Volume) + "cm" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.5)) + "cm" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "cm" + InfoCollector.getExponentSign("3");
    }

    //  CYLINDER POOL
    private void createProblem_10(){
        int radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int diameter = radius * 2;
        int height = (MathUtility.getRandomPositiveNumber_1Digit() % 4) + 6;

        double Volume = (double) (radius * radius * height) * 3.14;
        Volume = MathUtility.round_2_Decimal(Volume);

        QUESTION = "A swimming pool with a cylinder shape has the diameter is " + String.valueOf(diameter) + "feet, and the height is "
                + String.valueOf(height) + "feet. If the pool is filled with water, how much water can it hold?";
        RIGHT_ANSWER = String.valueOf(Volume) + "feet" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.0)) + "feet" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Volume + 10.5)) + "feet" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Volume - 10.0)) + "feet" + InfoCollector.getExponentSign("3");

    }

    //  RECTANGULAR POOL
    private void createProblem_11(){
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 30;
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 30;
        int height = MathUtility.getRandomPositiveNumber_1Digit() % 4 + 6;

        int Volume = length * width * height;

        QUESTION = "A swimming pool with a rectangular shape has the dimension (in feet) is "
                + String.valueOf(length) + " x " + String.valueOf(width) + " x " + String.valueOf(height)
                + " . If the pool is filled with water, how much water can it hold?";
        RIGHT_ANSWER = String.valueOf(Volume) + "feet" + InfoCollector.getExponentSign("3");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Volume + 10) + "feet" + InfoCollector.getExponentSign("3");
        ANSWER_C = String.valueOf(Volume - 10) + "feet" + InfoCollector.getExponentSign("3");
        ANSWER_D = String.valueOf(Volume + 20) + "feet" + InfoCollector.getExponentSign("3");

    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
