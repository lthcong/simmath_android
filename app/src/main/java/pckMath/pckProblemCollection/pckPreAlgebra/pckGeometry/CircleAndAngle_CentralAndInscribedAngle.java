package pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/12/2017.
 */

public class CircleAndAngle_CentralAndInscribedAngle extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public CircleAndAngle_CentralAndInscribedAngle(){
        createProblem();
        swapAnswer();
    }

    public CircleAndAngle_CentralAndInscribedAngle(String Question,
                                      String RightAnswer,
                                      String AnswerA,
                                      String AnswerB,
                                      String AnswerC,
                                      String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 10;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            default:
                createProblem_01();
                break;
        }
        return new CircleAndAngle_CentralAndInscribedAngle(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  CENTRAL ANGLE
    private void createProblem_01(){
        int arcAB = MathUtility.getRandomAngle();

        QUESTION = "A, B are two points on the circle. Find the measure of the central angle "
                + InfoCollector.getAngleSign() + "AOB, given the measure of the arc AB is " + String.valueOf(arcAB) + InfoCollector.getDegreeSign() + ".";
        RIGHT_ANSWER = String.valueOf(arcAB) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(arcAB + 1) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(arcAB - 1) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(arcAB + 2) + InfoCollector.getDegreeSign();
    }

    //  CENTRAL ANGLE
    private void createProblem_02(){
        int arcAB = MathUtility.getRandomAngle();

        QUESTION = "A, B are two points on the circle. Find the measure of the arc AB. "
                + "Given the measure of the central angle is " + String.valueOf(arcAB) + InfoCollector.getDegreeSign() + ".";
        RIGHT_ANSWER = String.valueOf(arcAB) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(arcAB + 1) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(arcAB - 1) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(arcAB + 2) + InfoCollector.getDegreeSign();
    }

    //  CENTRAL ANGLE
    private void createProblem_03(){
        int CentralAngle = MathUtility.getRandomAngle();

        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int b_value = CentralAngle - a_value * x_value;

        QUESTION = "A and B are two points on the circle. The central angle is " + String.valueOf(CentralAngle) + InfoCollector.getDegreeSign()
                + ". The measure of the arc AB is " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ". Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  CENTRAL ANGLE
    private void createProblem_04(){
        int CentralAngle = MathUtility.getRandomAngle();

        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int b_value = CentralAngle - a_value * x_value;

        QUESTION = "A and B are two points on the circle. The measure of the arc AB is " + String.valueOf(CentralAngle) + InfoCollector.getDegreeSign()
                + ". The measure of the central angle is " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ". Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  INSCRIBED ANGLE
    private void createProblem_05(){
        int arcAB = MathUtility.getRandomAngle();
        double InscribedAngle = MathUtility.round_1_Decimal((double) arcAB / 2.0);

        QUESTION = "A, B, C are three points on the circle. Given the measure of the arc AB is " + String.valueOf(arcAB) + InfoCollector.getDegreeSign()
                + ". Find the measure of the inscribed angle " + InfoCollector.getAngleSign() + "ACB.";
        RIGHT_ANSWER = String.valueOf(InscribedAngle) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(InscribedAngle + 1.0)) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(InscribedAngle - 1.0)) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(InscribedAngle + 2.0)) + InfoCollector.getDegreeSign();
    }

    //  INSCRIBED ANGLE
    private void createProblem_06(){
        int InscribedAngle = MathUtility.getRandomAngle();
        int arcAB = InscribedAngle * 2;

        QUESTION = "A, B, C are three points on the circle. Given the measure of the inscribed angle " + InfoCollector.getAngleSign() + "ACB is " + String.valueOf(arcAB) + InfoCollector.getDegreeSign()
                + ". Find the measure of the arc AB.";
        RIGHT_ANSWER = String.valueOf(arcAB) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(arcAB + 1) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(arcAB + 2) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(arcAB - 1) + InfoCollector.getDegreeSign();
    }

    //  INSCRIBED ANGLE
    private void createProblem_07(){
        int arcAB = MathUtility.getRandomAngle() * 2;
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = arcAB / 2 - a_value * x_value;

        QUESTION = "A, B, C are three points on the circle. Given the measure of the arc AB is " + String.valueOf(arcAB) + InfoCollector.getDegreeSign()
                + ", and the measure of the inscribed angle " + InfoCollector.getAngleSign() + "ACB is " + String.valueOf(a_value) + "x + " + String.valueOf(b_value)
                + ". Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value + 2);
        ANSWER_D = String.valueOf(x_value - 1);
    }

    //  INSCRIBED ANGLE
    private void createProblem_08(){
        int InscribedAngle = MathUtility.getRandomAngle();
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = InscribedAngle * 2 - a_value * x_value;

        QUESTION = "A, B, C are three points on the circle. Given the measure of the inscribed angle " + InfoCollector.getAngleSign() + "ACB is " + String.valueOf(InscribedAngle) + InfoCollector.getDegreeSign()
                + ", and the measure of the arc AB is " + String.valueOf(a_value) + "x + " + String.valueOf(b_value)
                + ". Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value + 2);
        ANSWER_D = String.valueOf(x_value - 1);
    }

    //  INSCRIBED ANGLE
    private void createProblem_09(){
        int arcAB = MathUtility.getRandomAngle();
        double InscribedAngle = MathUtility.round_1_Decimal((double) arcAB / 2.0);

        QUESTION = "A, B, C are three points on the circle. Given the measure of the central angle " + InfoCollector.getAngleSign() + "AOB is " + String.valueOf(arcAB) + InfoCollector.getDegreeSign()
                + ". Find the measure of the inscribed angle " + InfoCollector.getAngleSign() + "ACB.";
        RIGHT_ANSWER = String.valueOf(InscribedAngle) + InfoCollector.getDegreeSign();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(InscribedAngle + 1.0)) + InfoCollector.getDegreeSign();
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(InscribedAngle - 1.0)) + InfoCollector.getDegreeSign();
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(InscribedAngle + 2.0)) + InfoCollector.getDegreeSign();
    }

    //  INSCRIBED ANGLE
    //  Finding the x value: ax + b = 2 * (cx + d)
    private void createProblem_10(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();

        int c_value = MathUtility.getRandomPositiveNumber_1Digit();
        int d_value = MathUtility.getRandomPositiveNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int b_value = 2 * (c_value * x_value + d_value) - a_value * x_value;

        QUESTION = "A, B, C are three points on the circle. Given the inscribed angle "
                + InfoCollector.getAngleSign() + "ACB is " + String.valueOf(c_value) + "x + " + String.valueOf(d_value)
                + ", and the central angle " + InfoCollector.getAngleSign() + "AOB is " + String.valueOf(a_value) + "x + " + String.valueOf(b_value)
                + ". Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
