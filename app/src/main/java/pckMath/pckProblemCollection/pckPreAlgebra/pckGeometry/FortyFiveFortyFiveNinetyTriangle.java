package pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckIntermediateMath.pckRoot.Root;

/**
 * Created by Cong on 6/12/2017.
 */

public class FortyFiveFortyFiveNinetyTriangle extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FortyFiveFortyFiveNinetyTriangle(){
        createProblem();
        swapAnswer();
    }

    public FortyFiveFortyFiveNinetyTriangle(String Question,
                                      String RightAnswer,
                                      String AnswerA,
                                      String AnswerB,
                                      String AnswerC,
                                      String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 2;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            default:
                createProblem_01();
                break;
        }
        return new FortyFiveFortyFiveNinetyTriangle(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int side = MathUtility.getRandomPositiveNumber_1Digit() + 20;
        double hypotenuse = MathUtility.round_2_Decimal((double) side * (new Root(2, 2)).getValue());

        QUESTION = "Find the hypotenuse of a 45-45-90 triangle with the side is " + String.valueOf(side) + " inches.";
        RIGHT_ANSWER = String.valueOf(hypotenuse);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(hypotenuse + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(hypotenuse - 0.1));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(hypotenuse + 0.2));
    }

    private void createProblem_02(){
        int hypotenuse = MathUtility.getRandomPositiveNumber_2Digit();
        double side = MathUtility.round_2_Decimal((double) hypotenuse / (new Root(2, 2)).getValue());

        QUESTION = "Find the side of a 45-45-90 triangle with the hypotenuse is " + String.valueOf(hypotenuse) + " inches.";
        RIGHT_ANSWER = String.valueOf(side);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(side + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(side - 0.1));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(side + 0.2));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
