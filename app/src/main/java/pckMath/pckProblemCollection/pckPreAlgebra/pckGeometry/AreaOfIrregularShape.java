package pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/23/2017.
 */

public class AreaOfIrregularShape extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public AreaOfIrregularShape(){
        createProblem();
        swapAnswer();
    }

    public AreaOfIrregularShape(String Question,
                                                   String RightAnswer,
                                                   String AnswerA,
                                                   String AnswerB,
                                                   String AnswerC,
                                                   String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 10;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            default:
                createProblem_01();
                break;
        }
        return new AreaOfIrregularShape(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int Side = MathUtility.getRandomPositiveNumber_1Digit() + 2;
        int Radius = MathUtility.getRandomPositiveNumber_1Digit() + Side;

        double SquareArea = (double) (Side * Side);
        double CircleArea = (double) (Radius * Radius) * 3.14;

        double Result = MathUtility.round_2_Decimal(CircleArea - SquareArea);

        QUESTION = "A square is placed inside a circle. Find the area outside the square of the circle. "
                + "Given the side of the square is " + String.valueOf(Side) + "cm, and the radius of the circle is " + String.valueOf(Radius) + "cm.";
        RIGHT_ANSWER = String.valueOf(Result) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result - 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 15.0)) + "cm" + InfoCollector.getExponentSign("2");
    }

    private void createProblem_02(){
        int Side = MathUtility.getRandomPositiveNumber_1Digit() + 2;
        int Radius = MathUtility.getRandomPositiveNumber_1Digit() + 2;

        Radius = Radius + Side;

        double SquareArea = (double) (Side * Side);
        double CircleArea = (double) (Radius * Radius) * 3.14;

        double Result = MathUtility.round_2_Decimal(SquareArea - CircleArea);

        QUESTION = "A circle is placed inside a square. Find the area outside the circle of the square. "
                + "Given the side of the square is " + String.valueOf(Side) + "cm, and the radius of the circle is " + String.valueOf(Radius) + "cm.";
        RIGHT_ANSWER = String.valueOf(Result) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result - 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 15.0)) + "cm" + InfoCollector.getExponentSign("2");
    }

    private void createProblem_03(){
        int SmallerRadius = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int BiggerRadius = MathUtility.getRandomPositiveNumber_1Digit() + SmallerRadius;

        double BiggerArea = (double) (BiggerRadius * BiggerRadius) * 3.14;
        double SmallerArea = (double) (SmallerRadius * SmallerRadius) * 3.14;

        double Result = MathUtility.round_2_Decimal(BiggerArea - SmallerArea);

        QUESTION = "A smaller circle is placed inside a bigger one. Find the area outside the smaller circle of the bigger one. "
                + "Given the smaller radius is " + String.valueOf(SmallerRadius) + "cm, and the bigger radius is " + String.valueOf(BiggerRadius) + "cm.";
        RIGHT_ANSWER = String.valueOf(Result) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result - 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 15.0)) + "cm" + InfoCollector.getExponentSign("2");
    }

    private void createProblem_04(){
        int GardenLength = MathUtility.getRandomPositiveNumber_1Digit() + 15;
        int GardenWidth = MathUtility.getRandomPositiveNumber_1Digit() + 15;

        int PathWidth = MathUtility.getRandomPositiveNumber_1Digit() + 2;

        int LandArea = (GardenLength + PathWidth * 2) * (GardenWidth + PathWidth * 2);

        QUESTION = "A rectangle garden is surrounded by a " + String.valueOf(PathWidth) + "m path. "
                + "Find the land area (garden + path). Given the length and the width of the garden is "
                + String.valueOf(GardenLength) + "m, " + String.valueOf(GardenWidth) + "m." ;
        RIGHT_ANSWER = String.valueOf(LandArea) + "m" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(LandArea + 10) + "m" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(LandArea - 10) + "m" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(LandArea - 15) + "m" + InfoCollector.getExponentSign("2");
    }

    private void createProblem_05(){
        int GardenLength = MathUtility.getRandomPositiveNumber_1Digit() + 15;
        int GardenWidth = MathUtility.getRandomPositiveNumber_1Digit() + 15;

        int PathWidth = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int LandLength = GardenLength + PathWidth * 2;
        int LandWidth = GardenWidth + PathWidth * 2;

        int PathArea = LandLength * LandWidth - GardenLength * GardenWidth;

        QUESTION = "A rectangle garden is surrounded by a path. "
                + "Find the width of the path. Given the dimension of the garden is "
                + String.valueOf(GardenLength) + "m x " + String.valueOf(GardenWidth) + "m. "
                + "And the dimension of the land (garden + path) is "
                + String.valueOf(LandLength) + "m x " + String.valueOf(LandWidth) + "m. ";
        RIGHT_ANSWER = String.valueOf(PathArea) + "m" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(PathArea + 10) + "m" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(PathArea - 10) + "m" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(PathArea + 20) + "m" + InfoCollector.getExponentSign("2");
    }

    private void createProblem_06(){
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int base = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int radius = MathUtility.getRandomPositiveNumber_1Digit() + 15;

        double Result = (double) (radius * radius) * 3.14 - (double) (base * height) / 2.0;
        Result = MathUtility.round_2_Decimal(Result);

        QUESTION = "A triangle is placed inside a circle. Find the area out side the triangle of the circle. "
                + "Given the height and the base of the triangle are " + String.valueOf(base) + "cm, " + String.valueOf(height) + "cm. "
                + "The radius of the circle is " + String.valueOf(radius) + "cm.";
        RIGHT_ANSWER = String.valueOf(Result) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result - 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 15.0)) + "cm" + InfoCollector.getExponentSign("2");
    }

    private void createProblem_07(){
        int short_diagonal = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int long_diagonal = MathUtility.getRandomPositiveNumber_1Digit() + short_diagonal;

        int side = MathUtility.getRandomPositiveNumber_1Digit() + long_diagonal;

        double Result = (double) (side * side) - (double) (short_diagonal * long_diagonal) / 2.0;
        Result = MathUtility.round_1_Decimal(Result);

        QUESTION = "A rhombus is placed inside a square with the side is " + String.valueOf(side) + "cm. "
                + "Find the area outside the rhombus of the square. Given the two diagonals of the rhombus are: "
                + String.valueOf(short_diagonal) + "cm, " + String.valueOf(long_diagonal) + "cm.";
        RIGHT_ANSWER = String.valueOf(Result) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result - 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result - 15.0)) + "cm" + InfoCollector.getExponentSign("2");
    }

    private void createProblem_08(){
        int radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int short_diagonal = MathUtility.getRandomPositiveNumber_1Digit() + radius;
        int long_diagonal = MathUtility.getRandomPositiveNumber_1Digit() + short_diagonal;

        double Result = (double) (short_diagonal * long_diagonal) / 2.0 - (double) (radius * radius) * 3.14;
        Result = MathUtility.round_2_Decimal(Result);

        QUESTION = "A circle is placed inside a rhombus. Find the area outside the circle of the rhombus. "
                + "Given radius of the circle is " + String.valueOf(radius) + "cm, and the two diagonals of the rhombus are: "
                + String.valueOf(short_diagonal) + "cm, " + String.valueOf(long_diagonal) + "cm.";
        RIGHT_ANSWER = String.valueOf(Result) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result - 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result - 20.0)) + "cm" + InfoCollector.getExponentSign("2");
    }

    private void createProblem_09(){
        int base_1 = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int base_2 = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int radius = MathUtility.getRandomPositiveNumber_1Digit() + 15;

        double Result = (double) (radius * radius) * 3.14 - (double) ((base_1 + base_2) * height) / 2.0;
        Result = MathUtility.round_2_Decimal(Result);

        QUESTION = "A trapezoid is placed inside a circle with the radius " + String.valueOf(radius) + "cm. "
                + "Find the area outside the trapezoid of the circle. Give the bases of the trapezoid are: "
                + String.valueOf(base_1) + "cm, " + String.valueOf(base_2) + "cm, and the height is " + String.valueOf(height) + "cm.";
        RIGHT_ANSWER = String.valueOf(Result) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result - 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result - 20.0)) + "cm" + InfoCollector.getExponentSign("2");
    }

    private void createProblem_10(){
        int short_diagonal = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int long_diagonal = MathUtility.getRandomPositiveNumber_1Digit() + short_diagonal;

        int length = MathUtility.getRandomPositiveNumber_1Digit() + long_diagonal;
        int width = MathUtility.getRandomPositiveNumber_1Digit() + short_diagonal;

        double Result = (double) (length - width) - (double) (short_diagonal * long_diagonal) / 2.0;
        Result = MathUtility.round_1_Decimal(Result);

        QUESTION = "A rhombus's placed inside a rectangle. Given the dimension of rectangle is " + String.valueOf(length) + "cm x " + String.valueOf(width) + "cm."
                + "and the two diagonals of the rhombus are " + String.valueOf(short_diagonal) + "cm, " + String.valueOf(length) + "cm. "
                + "Find the area outside the rhombus of the rectangle.";
        RIGHT_ANSWER = String.valueOf(Result) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Result + 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Result - 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Result + 15.0)) + "cm" + InfoCollector.getExponentSign("2");
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
