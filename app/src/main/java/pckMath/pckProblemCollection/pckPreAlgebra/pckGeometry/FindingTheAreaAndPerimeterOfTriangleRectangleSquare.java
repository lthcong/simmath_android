package pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/10/2017.
 */

public class FindingTheAreaAndPerimeterOfTriangleRectangleSquare extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingTheAreaAndPerimeterOfTriangleRectangleSquare(){
        createProblem();
        swapAnswer();
    }

    public FindingTheAreaAndPerimeterOfTriangleRectangleSquare(String Question,
                                                       String RightAnswer,
                                                       String AnswerA,
                                                       String AnswerB,
                                                       String AnswerC,
                                                       String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 14;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            case 14:
                createProblem_15();
                break;
            case 15:
                createProblem_16();
                break;
            case 16:
                createProblem_17();
                break;
            case 17:
                createProblem_18();
                break;
            case 18:
                createProblem_19();
                break;
            case 19:
                createProblem_20();
                break;
            case 20:
                createProblem_21();
                break;
            case 21:
                createProblem_22();
                break;
            case 22:
                createProblem_23();
                break;
            case 23:
                createProblem_24();
                break;
            case 24:
                createProblem_25();
                break;
            case 25:
                createProblem_26();
                break;
            case 26:
                createProblem_27();
                break;
            case 27:
                createProblem_28();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FindingTheAreaAndPerimeterOfTriangleRectangleSquare(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  ARE OF TRIANGLE
    private void createProblem_01(){
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int base = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        double area = MathUtility.round_1_Decimal((double) (base * height) / 2.0);

        QUESTION = "Find the area of a triangle with the base is " + String.valueOf(base) + ", and the height is " + String.valueOf(height) + ".";
        RIGHT_ANSWER = String.valueOf(area);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(area + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(area - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(area + 2.0));
    }

    //  ARE OF TRIANGLE
    private void createProblem_02(){
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int base = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double area = MathUtility.round_1_Decimal((double) (base * height) / 2.0);

        QUESTION = "Find the area of a triangle with the base is " + String.valueOf(base) + ", and the height is " + String.valueOf(height) + ".";
        RIGHT_ANSWER = String.valueOf(area);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(area + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(area - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(area + 2.0));
    }

    //  ARE OF TRIANGLE
    private void createProblem_03(){
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int base = MathUtility.getRandomPositiveNumber_1Digit() + 4;

        double area = MathUtility.round_1_Decimal((double) (base * height) / 2.0);

        QUESTION = "Find the height of a triangle with the base is " + String.valueOf(base) + ", and the area is " + String.valueOf(area) + ".";
        RIGHT_ANSWER = String.valueOf(height);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(height + 1);
        ANSWER_C = String.valueOf(height - 1);
        ANSWER_D = String.valueOf(height + 2);
    }

    //  ARE OF TRIANGLE
    private void createProblem_04(){
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int base = MathUtility.getRandomPositiveNumber_1Digit() + 4;

        double area = MathUtility.round_1_Decimal((double) (base * height) / 2.0);

        QUESTION = "Find the base of a triangle with the height is " + String.valueOf(height) + ", and the area is " + String.valueOf(area) + ".";
        RIGHT_ANSWER = String.valueOf(base);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(base + 1);
        ANSWER_C = String.valueOf(base - 1);
        ANSWER_D = String.valueOf(base + 2);
    }

    //  ARE OF TRIANGLE
    /*
    *   Finding the x: (ax + b) * base / 2 = area
    * */
    private void createProblem_05(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();
        int height = a_value * x_value + b_value;

        int base = MathUtility.getRandomPositiveNumber_1Digit() + 4;

        double area = MathUtility.round_1_Decimal((double) (base * height) / 2.0);

        QUESTION = "Given a triangle with the base is " + String.valueOf(base)
                + ", the height is " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ".<br>" +
                "The area is " + String.valueOf(area) + ". Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  ARE OF TRIANGLE
    /*
    *   Finding the x: (ax + b) * base / 2 = (cx + d)
    * */
    private void createProblem_06(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();
        int height = a_value * x_value + b_value;

        int base = MathUtility.getRandomPositiveNumber_1Digit() + 4;

        int c_value = MathUtility.getRandomPositiveNumber_1Digit();
        double d_value = (double) (height * base) / 2.0 - (double) (c_value * x_value);
        d_value = MathUtility.round_1_Decimal(d_value);

        QUESTION = "Given a triangle with the height is " + String.valueOf(a_value) + "x + " + String.valueOf(b_value)
                + ", the base is " + String.valueOf(base) + ".<br>"
                + "The area is " + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ".<br>" +
                "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  ARE OF TRIANGLE
    /*
    *   Finding the x: (ax + b) * height / 2 = (cx + d)
    * */
    private void createProblem_07(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();
        int height = a_value * x_value + b_value;

        int base = MathUtility.getRandomPositiveNumber_1Digit() + 4;

        int c_value = MathUtility.getRandomPositiveNumber_1Digit();
        double d_value = (double) (height * base) / 2.0 - (double) (c_value * x_value);
        d_value = MathUtility.round_1_Decimal(d_value);

        QUESTION = "Given a triangle with the base is " + String.valueOf(a_value) + "x + " + String.valueOf(b_value)
                + ", the height is " + String.valueOf(base) + ".<br>"
                + ", the area is " + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  PERIMETER OF TRIANGLE
    private void createProblem_08(){
        int a, b, c;

        while(true){
            a = MathUtility.getRandomPositiveNumber_1Digit() + 10;
            b = MathUtility.getRandomPositiveNumber_1Digit() + 10;
            c = MathUtility.getRandomPositiveNumber_1Digit() + 10;

            if (a + b > c){
                break;
            }
        }
        int perimeter = a + b + c;

        QUESTION = "Find the perimeter of a triangle with the sides:<br>"
                + "a = " + String.valueOf(a) + ", b = " + String.valueOf(b) + ", c = " + String.valueOf(c);
        RIGHT_ANSWER = String.valueOf(perimeter);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(perimeter + 1);
        ANSWER_C = String.valueOf(perimeter - 1);
        ANSWER_D = String.valueOf(perimeter + 2);
    }

    //  PERIMETER OF TRIANGLE
    /*
    *   Finding the x value: (ax + b) + (cx + d) + (ex + f) = P
    * */
    private void createProblem_09(){
        int a_value, b_value, c_value, d_value, e_value, f_value, x_value;
        int A, B, C;

        while(true){
            a_value = MathUtility.getRandomPositiveNumber_1Digit();
            b_value = MathUtility.getRandomPositiveNumber_1Digit();
            c_value = MathUtility.getRandomPositiveNumber_1Digit();
            d_value = MathUtility.getRandomPositiveNumber_1Digit();
            e_value = MathUtility.getRandomPositiveNumber_1Digit();
            f_value = MathUtility.getRandomPositiveNumber_1Digit();

            x_value = MathUtility.getRandomPositiveNumber_1Digit();

            A = a_value * x_value + b_value;
            B = c_value * x_value + d_value;
            C = e_value * x_value + f_value;

            if (A + B > C){
                break;
            }
        }
        int perimeter = A + B + C;

        QUESTION = "Give a triangle with the perimeter is " + String.valueOf(perimeter) + ", and the sides are:<br>"
                + "a = " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", "
                + "b = " + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ", "
                + "c = " + String.valueOf(e_value) + "x + " + String.valueOf(f_value) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  AREA OF RECTANGLE
    private void createProblem_10(){
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int area = length * width;

        QUESTION = "Find the area of a rectangle with the length = " + String.valueOf(length)
                + ", and the width = " + String.valueOf(width) + ".";
        RIGHT_ANSWER = String.valueOf(area);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(area + 1);
        ANSWER_C = String.valueOf(area - 1);
        ANSWER_D = String.valueOf(area + 2);
    }

    //  AREA OF RECTANGLE
    private void createProblem_11(){
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int area = length * width;

        QUESTION = "Find the length of a rectangle with the width is " + String.valueOf(width)
                + ", and the area is " + String.valueOf(area) + ".";
        RIGHT_ANSWER = String.valueOf(length);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(length + 1);
        ANSWER_C = String.valueOf(length - 1);
        ANSWER_D = String.valueOf(length + 2);
    }

    //  AREA OF RECTANGLE
    private void createProblem_12(){
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int area = length * width;

        QUESTION = "Find the width of a rectangle with the length is " + String.valueOf(length)
                + ", and the area is " + String.valueOf(area) + ".";
        RIGHT_ANSWER = String.valueOf(width);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(width + 1);
        ANSWER_C = String.valueOf(width - 1);
        ANSWER_D = String.valueOf(width + 2);
    }

    //  AREA OF RECTANGLE
    /*
    *   Finding the x value: (ax + b) * width = area
    * */
    private void createProblem_13(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int length = a_value * x_value + b_value;
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int area = length * width;

        QUESTION = "Given a rectangle with the area is " + String.valueOf(area) + ".<br>"
                + "The length is " + String.valueOf(a_value) +"x + " + String.valueOf(b_value)
                + ", and the width is " + String.valueOf(width) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  AREA OF RECTANGLE
    /*
    *   Finding the x value: (ax + b) * length = area
    * */
    private void createProblem_14(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int width = a_value * x_value + b_value;
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int area = length * width;

        QUESTION = "Given a rectangle with the area is " + String.valueOf(area) + ".<br>"
                + "The width is " + String.valueOf(a_value) +"x + " + String.valueOf(b_value)
                + ", and the length is " + String.valueOf(length) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  AREA OF RECTANGLE
    /*
    *   Finding the x value: (ax + b) * width = (cx + d)
    * */
    private void createProblem_15(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int length = a_value * x_value + b_value;
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int area = length * width;

        int d_value = area - (c_value * x_value);

        QUESTION = "Given a rectangle with the area is " + String.valueOf(c_value) +"x + " + String.valueOf(d_value) + ".<br>"
                + "The length is " + String.valueOf(a_value) +"x + " + String.valueOf(b_value)
                + ", and the width is " + String.valueOf(width) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  AREA OF RECTANGLE
    /*
    *   Finding the x value: (ax + b) * length = (cx + d)
    * */
    private void createProblem_16(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int width = a_value * x_value + b_value;
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int area = length * width;

        int d_value = area - (c_value * x_value);

        QUESTION = "Given a rectangle with the area is " + String.valueOf(c_value) +"x + " + String.valueOf(d_value) + ".<br>"
                + "The width is " + String.valueOf(a_value) +"x + " + String.valueOf(b_value)
                + ", and the length is " + String.valueOf(length) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  PERIMETER OF RECTANGLE
    private void createProblem_17(){
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int perimeter = 2 * width + 2 * length;

        QUESTION = "Find the perimeter of a rectangle, given the width is " + String.valueOf(width)
                + " and the length is " + String.valueOf(length) + ".";
        RIGHT_ANSWER = String.valueOf(perimeter);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(perimeter + 1);
        ANSWER_C = String.valueOf(perimeter - 1);
        ANSWER_D = String.valueOf(perimeter + 2);
    }

    //  PERIMETER OF RECTANGLE
    private void createProblem_18(){
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int perimeter = 2 * width + 2 * length;

        QUESTION = "Find the length of a rectangle, given the width is " + String.valueOf(width)
                + " and the perimeter is " + String.valueOf(perimeter) + ".";
        RIGHT_ANSWER = String.valueOf(length);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(length + 1);
        ANSWER_C = String.valueOf(length - 1);
        ANSWER_D = String.valueOf(length + 2);
    }

    //  PERIMETER OF RECTANGLE
    private void createProblem_19(){
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int perimeter = 2 * width + 2 * length;

        QUESTION = "Find the width of a rectangle, given the length is " + String.valueOf(length)
                + " and the perimeter is " + String.valueOf(perimeter) + ".";
        RIGHT_ANSWER = String.valueOf(width);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(width + 1);
        ANSWER_C = String.valueOf(width - 1);
        ANSWER_D = String.valueOf(width + 2);
    }

    //  PERIMETER OF RECTANGLE
    /*
    *   Finding the x value: 2(ax + b) + 2(cx + d) = P
    * */
    private void createProblem_20(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomPositiveNumber_1Digit();
        int d_value = MathUtility.getRandomPositiveNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int width = a_value * x_value + b_value;
        int length = c_value * x_value + d_value;

        int perimeter = 2 * width + 2 * length;

        QUESTION = "Given a rectangle with the perimeter is " + String.valueOf(perimeter) + ".<br>"
                + "The width is " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ", "
                + "The length is " + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + "<br>."
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  PERIMETER OF SQUARE
    private void createProblem_21(){
        int side = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int perimeter = 4 * side;

        QUESTION = "Find the perimeter of a square with the side is " + String.valueOf(side) + ".";
        RIGHT_ANSWER = String.valueOf(perimeter);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(perimeter + 1);
        ANSWER_C = String.valueOf(perimeter + 2);
        ANSWER_D = String.valueOf(perimeter - 1);
    }

    //  PERIMETER OF SQUARE
    private void createProblem_22(){
        int side = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int perimeter = 4 * side;

        QUESTION = "Find the side of a square with the perimeter is " + String.valueOf(perimeter) + ".";
        RIGHT_ANSWER = String.valueOf(side);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(side + 1);
        ANSWER_C = String.valueOf(side + 2);
        ANSWER_D = String.valueOf(side - 1);
    }

    //  PERIMETER OF SQUARE
    //  Finding the x value: 4(ax + b) = P
    private void createProblem_23(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int side = a_value * x_value + b_value;
        int perimeter = 4 * side;

        QUESTION = "Given a square with perimeter is " + String.valueOf(perimeter) + ", and the side is "
                + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value + 2);
        ANSWER_D = String.valueOf(x_value - 1);
    }

    //  PERIMETER OF SQUARE
    //  Finding the x value: 4(ax + b) = (cx + d)
    private void createProblem_24(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomPositiveNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int side = a_value * x_value + b_value;
        int perimeter = 4 * side;

        int d_value = perimeter - c_value * x_value;

        QUESTION = "Given a square with perimeter is " + String.valueOf(c_value) + "x + " + String.valueOf(d_value)
                + ", and the side is " + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value + 2);
        ANSWER_D = String.valueOf(x_value - 1);
    }

    //  AREA OF SQUARE
    private void createProblem_25(){
        int side = MathUtility.getRandomPositiveNumber_1Digit();
        int area = side * side;

        QUESTION = "Find the area of a square with the side is " + String.valueOf(side) + ".";
        RIGHT_ANSWER = String.valueOf(area);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(area + 1);
        ANSWER_C = String.valueOf(area - 1);
        ANSWER_D = String.valueOf(area + 2);
    }

    //  AREA OF SQUARE
    private void createProblem_26(){
        int side = MathUtility.getRandomPositiveNumber_1Digit();
        int area = side * side;

        QUESTION = "Find the side of a square with the area is " + String.valueOf(area) + ".";
        RIGHT_ANSWER = String.valueOf(side);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(side + 1);
        ANSWER_C = String.valueOf(side - 1);
        ANSWER_D = String.valueOf(side + 2);
    }

    //  AREA OF SQUARE
    private void createProblem_27(){
        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int side = a_value * x_value + b_value;
        int area = side * side;

        QUESTION = "Given a square with the area is " + String.valueOf(area)
                + ", and the side is " + String.valueOf(a_value) + "x + " + String.valueOf(b_value)
                + ". Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  AREA OF SQUARE
    //  Finding the x value: side * side = (ax + b)
    private void createProblem_28(){
        int side = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int area = side * side;

        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int x_value = MathUtility.getRandomPositiveNumber_1Digit();
        int b_value = area - a_value * x_value;

        QUESTION = "Given a square with the side is " + String.valueOf(side) + ", and the area is "
                + String.valueOf(a_value) + "x + " + String.valueOf(b_value) + ".<br>"
                + "Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
