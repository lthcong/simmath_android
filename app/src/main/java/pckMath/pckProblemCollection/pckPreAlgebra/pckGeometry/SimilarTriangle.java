package pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/22/2017.
 */

public class SimilarTriangle extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SimilarTriangle(){
        createProblem();
        swapAnswer();
    }

    public SimilarTriangle(String Question,
                                            String RightAnswer,
                                            String AnswerA,
                                            String AnswerB,
                                            String AnswerC,
                                            String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 8;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }
        return new SimilarTriangle(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int d = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double e = MathUtility.round_2_Decimal((double) (b * d) / (double) a);

        QUESTION = InfoCollector.getTriangleSign() + "ABC and " + InfoCollector.getTriangleSign() + "DEF are similar triangles. "
                + "Given AB = " + String.valueOf(a) + ", BC = " + String.valueOf(b) + ", and DE = " + String.valueOf(d)
                + ". Find the measure of EF.";
        RIGHT_ANSWER = String.valueOf(e);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(e + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(e + 0.01));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(e - 0.1));
    }

    private void createProblem_02(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int d = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double e = MathUtility.round_2_Decimal((double) (b * d) / (double) a);

        QUESTION = InfoCollector.getTriangleSign() + "ABC and " + InfoCollector.getTriangleSign() + "DEF are similar triangles. "
                + "Given AC = " + String.valueOf(a) + ", BC = " + String.valueOf(b) + ", and DF = " + String.valueOf(d)
                + ". Find the measure of EF.";
        RIGHT_ANSWER = String.valueOf(e);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(e + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(e + 0.01));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(e - 0.1));
    }

    private void createProblem_03(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int d = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double e = MathUtility.round_2_Decimal((double) (b * d) / (double) a);

        QUESTION = InfoCollector.getTriangleSign() + "ABC and " + InfoCollector.getTriangleSign() + "DEF are similar triangles. "
                + "Given AC = " + String.valueOf(a) + ", AB = " + String.valueOf(b) + ", and DF = " + String.valueOf(d)
                + ". Find the measure of DE.";
        RIGHT_ANSWER = String.valueOf(e);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(e + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(e + 0.01));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(e - 0.1));
    }
    
    //  Finding the x value:    (ax + b) / (cx + d) = A
    private void createProblem_04(){
        int AB = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int BC = AB * (MathUtility.getRandomPositiveNumber_1Digit() + 1);

        int ratio = AB / BC;

        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomNumber_1Digit();
        int d_value = MathUtility.getRandomPositiveNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int b_value = ratio * (c_value * x_value + d_value) - a_value * x_value;

        QUESTION = InfoCollector.getTriangleSign() + "ABC and " + InfoCollector.getTriangleSign() + "DEF are similar triangle. "
                + "Given AB = " + String.valueOf(AB) + ", and BC = " + String.valueOf(BC) + ", and "
                + "DE = " + String.valueOf(a_value) + "x + " + String.valueOf(b_value)
                + ", EF = " + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ". Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    //  Finding the x value:    (ax + b) / (cx + d) = A
    private void createProblem_05(){
        int AC = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int BC = AC * (MathUtility.getRandomPositiveNumber_1Digit() + 1);

        int ratio = AC / BC;

        int a_value = MathUtility.getRandomPositiveNumber_1Digit();
        int c_value = MathUtility.getRandomNumber_1Digit();
        int d_value = MathUtility.getRandomPositiveNumber_1Digit();

        int x_value = MathUtility.getRandomPositiveNumber_1Digit();

        int b_value = ratio * (c_value * x_value + d_value) - a_value * x_value;

        QUESTION = InfoCollector.getTriangleSign() + "ABC and " + InfoCollector.getTriangleSign() + "DEF are similar triangle. "
                + "Given AC = " + String.valueOf(AC) + ", and BC = " + String.valueOf(BC) + ", and "
                + "DF = " + String.valueOf(a_value) + "x + " + String.valueOf(b_value)
                + ", EF = " + String.valueOf(c_value) + "x + " + String.valueOf(d_value) + ". Find the x value.";
        RIGHT_ANSWER = String.valueOf(x_value);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(x_value + 1);
        ANSWER_C = String.valueOf(x_value - 1);
        ANSWER_D = String.valueOf(x_value + 2);
    }

    private void createProblem_06(){
        int ManHeight = MathUtility.getRandomPositiveNumber_2Digit() + 90;
        int ManShadow = MathUtility.getRandomPositiveNumber_2Digit() + 90;

        int TreeShadow = MathUtility.getRandomPositiveNumber_2Digit() + 150;

        double TreeHeight = MathUtility.round_2_Decimal((double) (ManHeight * TreeShadow) / (double) (ManShadow));

        QUESTION = "A man is standing next to a tree. He is " + String.valueOf(ManHeight) + "cm tall, and his shadow is " + String.valueOf(ManShadow) + "cm long. "
                + "At the same time, the tree shadow is " + String.valueOf(TreeShadow) + "cm long. Find the height of the tree.";
        RIGHT_ANSWER = String.valueOf(TreeHeight) + "cm";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(TreeHeight + 10.0)) + "cm";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(TreeHeight - 10.0)) + "cm";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(TreeHeight + 20.0)) + "cm";
    }

    private void createProblem_07(){
        int ManHeight = MathUtility.getRandomPositiveNumber_2Digit() + 90;
        int ManShadow = MathUtility.getRandomPositiveNumber_2Digit() + 90;

        int TreeHeight = MathUtility.getRandomPositiveNumber_2Digit() + 120;

        double TreeShadow = MathUtility.round_2_Decimal((double) (ManShadow * TreeHeight) / (double) (ManHeight));

        QUESTION = "A man is standing next to a tree. He is " + String.valueOf(ManHeight) + "cm tall, and his shadow is " + String.valueOf(ManShadow) + "cm long. "
                + "Find the length of the tree shadow at the same time. Given the height of the tree is " + String.valueOf(TreeHeight) + "cm.";
        RIGHT_ANSWER = String.valueOf(TreeShadow) + "cm";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(TreeShadow + 10.0)) + "cm";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(TreeShadow - 10.0)) + "cm";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(TreeShadow + 20.0)) + "cm";
    }

    private void createProblem_08(){
        int ManHeight = MathUtility.getRandomPositiveNumber_2Digit() + 90;

        int TreeHeight = MathUtility.getRandomPositiveNumber_2Digit() + 120;
        int TreeShadow = MathUtility.getRandomPositiveNumber_2Digit() + 120;

        double ManShadow = MathUtility.round_2_Decimal((double) (ManHeight * TreeShadow) / (double) (TreeHeight));

        QUESTION = "A man is standing next to a tree. He is " + String.valueOf(ManHeight) + "cm tall. "
                + "Find the length of the man shadow. Given the height of the tree is " + String.valueOf(TreeHeight)
                + "cm, and the tree shadow at the same time is " + String.valueOf(TreeShadow) + "cm.";
        RIGHT_ANSWER = String.valueOf(ManShadow) + "cm";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(ManShadow + 10.0)) + "cm";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(ManShadow - 10.0)) + "cm";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(ManShadow + 20.0)) + "cm";
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
