package pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/23/2017.
 */

public class SurfaceAreaOfRegularSolid extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SurfaceAreaOfRegularSolid(){
        createProblem();
        swapAnswer();
    }

    public SurfaceAreaOfRegularSolid(String Question,
                           String RightAnswer,
                           String AnswerA,
                           String AnswerB,
                           String AnswerC,
                           String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 11;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SurfaceAreaOfRegularSolid(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  RECTANGULAR SOLID
    private void createProblem_01(){
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int SurfaceArea = 2 * (length * width + length * height + width * height);

        QUESTION = "Find the surface area of a rectangular solid. Given the length is " + String.valueOf(length) + "cm, "
                + "the width is " + String.valueOf(width) + ", and the height is " + String.valueOf(height) + "cm.";
        RIGHT_ANSWER = String.valueOf(SurfaceArea) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(SurfaceArea + 10) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(SurfaceArea + 20) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(SurfaceArea - 10) + "cm" + InfoCollector.getExponentSign("2");
    }

    //  CUBE
    private void createProblem_02(){
        int side = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int SurfaceArea = 6 * (side * side);

        QUESTION = "Find the surface area of a cube. Given the side is " + String.valueOf(side) + "cm.";
        RIGHT_ANSWER = String.valueOf(SurfaceArea) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(SurfaceArea + 10) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(SurfaceArea + 20) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(SurfaceArea - 10) + "cm" + InfoCollector.getExponentSign("2");
    }

    //  CYLINDER
    private void createProblem_03(){
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double SurfaceArea = 2.0 * 3.14 * (double) (radius * height + radius * radius);
        SurfaceArea = MathUtility.round_2_Decimal(SurfaceArea);

        QUESTION = "Find the surface area of a cylinder. Given the height is " + String.valueOf(height) + "cm, "
                + "and the base radius is " + String.valueOf(radius) + "cm.";
        RIGHT_ANSWER = String.valueOf(SurfaceArea) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea + 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea + 20.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea - 10.0)) + "cm" + InfoCollector.getExponentSign("2");
    }

    //  SPHERE
    private void createProblem_04(){
        int radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        double SurfaceArea = 4.0 * 3.14 * (radius * radius);

        QUESTION = "Find the surface area of a sphere. Given the radius is " + String.valueOf(radius) + "cm.";
        RIGHT_ANSWER = String.valueOf(SurfaceArea) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea + 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea + 20.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea - 10.0)) + "cm" + InfoCollector.getExponentSign("2");
    }

    //  CONE
    private void createProblem_05(){
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double SurfaceArea = 3.14 * (double) (radius) * ((double) (radius) + Math.pow((double) ((radius * radius) + (height * height)), 0.5));
        SurfaceArea = MathUtility.round_2_Decimal(SurfaceArea);

        QUESTION = "Find the surface area of a circular cone. Given the height is " + String.valueOf(height) + "cm, "
                + "and the base radius is " + String.valueOf(radius) + "cm.";
        RIGHT_ANSWER = String.valueOf(SurfaceArea) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea + 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea + 20.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea - 10.0)) + "cm" + InfoCollector.getExponentSign("2");
    }

    //  CONE
    private void createProblem_11(){
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double LateralArea = 3.14 * (double) (radius) * Math.pow((radius * radius) + (height * height), 0.5);
        LateralArea = MathUtility.round_2_Decimal(LateralArea);

        QUESTION = "Find the lateral area of a circular cone. Given the height is " + String.valueOf(height) + "cm, "
                + "and the base radius is " + String.valueOf(radius) + "cm.";
        RIGHT_ANSWER = String.valueOf(LateralArea) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(LateralArea + 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(LateralArea + 20.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(LateralArea - 10.0)) + "cm" + InfoCollector.getExponentSign("2");
    }

    //  SQUARE PYRAMID
    private void createProblem_06(){
        int side = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double SurfaceArea = (double) (side * side) + 2.0 * (double) side * Math.pow(((double) (side * side) / 4.0 + (double) (height * height)), 0.5);
        SurfaceArea = MathUtility.round_2_Decimal(SurfaceArea);

        QUESTION = "Find the surface area of a square pyramid. Given the height is " + String.valueOf(height) + "cm, "
                + "and the side is " + String.valueOf(side) + "cm.";
        RIGHT_ANSWER = String.valueOf(SurfaceArea) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea + 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea + 20.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea - 10.0)) + "cm" + InfoCollector.getExponentSign("2");
    }

    //  SQUARE PYRAMID
    private void createProblem_07(){
        int side = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double LateralArea = (double) (side) * Math.pow((double) (side * side + 4 * height * height), 0.5);
        LateralArea = MathUtility.round_2_Decimal(LateralArea);

        QUESTION = "Find the lateral area of a square pyramid. Given the height is " + String.valueOf(height) + "cm, "
                + "and the side is " + String.valueOf(side) + "cm.";
        RIGHT_ANSWER = String.valueOf(LateralArea) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(LateralArea + 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(LateralArea + 20.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(LateralArea - 10.0)) + "cm" + InfoCollector.getExponentSign("2");
    }

    //  RECTANGULAR PYRAMID
    private void createProblem_08(){
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double SurfaceArea = (double) (length * width)
                + (double) (length) * Math.pow((double) (width * width) / 4.0 + (double) (height * height), 0.5)
                + (double) (width) * Math.pow((double) (length * length) / 4.0 + (double) (height * height), 0.5);
        SurfaceArea = MathUtility.round_2_Decimal(SurfaceArea);

        QUESTION = "Find the surface area of a rectangular pyramid. Given the length is " + String.valueOf(length) + "cm, "
                + "the width is " + String.valueOf(width) + ", and the height is " + String.valueOf(height) + "cm.";
        RIGHT_ANSWER = String.valueOf(SurfaceArea) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea + 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea + 20.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea - 10.0)) + "cm" + InfoCollector.getExponentSign("2");
    }

    //  RECTANGULAR PYRAMID
    private void createProblem_09(){
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double LateralArea = (double) (length) * Math.pow((double) (width * width) / 4.0 + (double) (height * height), 0.5)
                + (double) (width) * Math.pow((double) (length * length) / 4.0 + (double) (height * height), 0.5);
        LateralArea = MathUtility.round_2_Decimal(LateralArea);

        QUESTION = "Find the lateral area of a rectangular pyramid. Given the length is " + String.valueOf(length) + "cm, "
                + "the width is " + String.valueOf(width) + ", and the height is " + String.valueOf(height) + "cm.";
        RIGHT_ANSWER = String.valueOf(LateralArea) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(LateralArea + 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(LateralArea + 20.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(LateralArea - 10.0)) + "cm" + InfoCollector.getExponentSign("2");
    }

    //  FRUSTUM OF RIGHT CIRCULAR CONE
    private void createProblem_10(){
        int height = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int small_radius = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int long_radius = MathUtility.getRandomPositiveNumber_1Digit() + small_radius;

        double SurfaceArea = 3.14 *
                (double) (small_radius + long_radius) *
                Math.pow((double) ((long_radius - small_radius) * (long_radius - small_radius)) + (double) (height * height), 0.5);
        SurfaceArea = MathUtility.round_2_Decimal(SurfaceArea);

        QUESTION = "Find the surface area of a frustum of a right circular cone. Given the radius are: "
                + String.valueOf(small_radius) + "cm, " + String.valueOf(long_radius) + "cm, "
                + "and the height is " + String.valueOf(height) + "cm.";
        RIGHT_ANSWER = String.valueOf(SurfaceArea) + "cm" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea + 10.0)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea + 10.5)) + "cm" + InfoCollector.getExponentSign("2");
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(SurfaceArea - 10.0)) + "cm" + InfoCollector.getExponentSign("2");
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
