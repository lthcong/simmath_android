package pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/11/2017.
 */

public class PythagoreanTheorem extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public PythagoreanTheorem(){
        createProblem();
        swapAnswer();
    }

    public PythagoreanTheorem(String Question,
                        String RightAnswer,
                        String AnswerA,
                        String AnswerB,
                        String AnswerC,
                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 12;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            default:
                createProblem_01();
                break;
        }
        return new PythagoreanTheorem(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  RIGHT TRIANGLE
    private void createProblem_01(){
        int Side_1 = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int Side_2 = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double Hypotenuse = (double) (Side_1 * Side_1) + (double) (Side_2 * Side_2);
        Hypotenuse = MathUtility.round_2_Decimal(Math.pow(Hypotenuse, 0.5));

        QUESTION = "Given a right triangle with the 2 sides are: " + String.valueOf(Side_1) + " inches, " + String.valueOf(Side_2) + " inches. "
                + "Find the measure of the hypotenuse.";
        RIGHT_ANSWER = String.valueOf(Hypotenuse) + "inches";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Hypotenuse + 1.0)) + "inches";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Hypotenuse - 0.01)) + "inches";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Hypotenuse + 0.1)) + "inches";
    }

    //  RIGHT TRIANGLE
    private void createProblem_02(){
        int Side_1 = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int Hypotenuse = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double Side_2 = (double) (Hypotenuse * Hypotenuse) - (double) (Side_1 * Side_1);
        Side_2 = MathUtility.round_2_Decimal(Math.pow(Side_2, 0.5));

        QUESTION = "Given a right triangle with one of the sides is: " + String.valueOf(Side_1)
                + " inches, and the hypotenuse is " + String.valueOf(Hypotenuse) + " inches.<br>"
                + "Find the measure of the other side.";
        RIGHT_ANSWER = String.valueOf(Side_2) + "inches";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(Side_2 + 1.0)) + "inches";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(Side_2 - 0.01)) + "inches";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(Side_2 + 0.1)) + "inches";
    }

    //  RECTANGLE
    private void createProblem_03(){
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double diagonal = (double) (length * length) + (double) (width * width);
        diagonal = MathUtility.round_2_Decimal(Math.pow(diagonal, 0.5));

        QUESTION = "Find the diagonal of a rectangle. Give the length is " + String.valueOf(length)
                + " inches, and the width is " + String.valueOf(width) + " inches.";
        RIGHT_ANSWER = String.valueOf(diagonal) + "inches";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(diagonal + 0.01)) + "inches";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(diagonal - 0.01)) + "inches";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(diagonal + 0.02)) + "inches";
    }

    //  RECTANGLE
    private void createProblem_04(){
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int diagonal = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double width = (double) (diagonal * diagonal) - (double) (length * length);
        width = MathUtility.round_2_Decimal(Math.pow(width, 0.5));

        QUESTION = "Given a rectangle with the length is: " + String.valueOf(length)
                + " inches, and the diagonal is " + String.valueOf(diagonal) + " inches.<br>"
                + "Find the measure of the width.";
        RIGHT_ANSWER = String.valueOf(width) + "inches";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(width + 1.0)) + "inches";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(width - 0.01)) + "inches";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(width + 0.1)) + "inches";
    }

    //  SQUARE
    private void createProblem_05(){
        int side = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double diagonal = (double) (2 * side * side);
        diagonal = MathUtility.round_2_Decimal(Math.pow(diagonal, 0.5));

        QUESTION = "Find the diagonal of a square with the side is " + String.valueOf(side) + " inches.";
        RIGHT_ANSWER = String.valueOf(diagonal) + "inches";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(diagonal + 0.01)) + "inches";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(diagonal - 0.01)) + "inches";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(diagonal + 0.02)) + "inches";
    }

    //  SQUARE
    private void createProblem_06(){
        int diagonal = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double side = (double) (diagonal * diagonal) / 2.0;
        side = MathUtility.round_2_Decimal(Math.pow(side, 0.5));

        QUESTION = "Find the side of a square with the diagonal is " + String.valueOf(diagonal) + " inches.";
        RIGHT_ANSWER = String.valueOf(side) + "inches";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(side + 0.01)) + "inches";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(side - 0.01)) + "inches";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(side + 0.02)) + "inches";
    }

    // RHOMBUS
    private void createProblem_07(){
        int side = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int diagonal_1 = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double diagonal_2 = (double) (side * side) - (double) (diagonal_1 * diagonal_1) / 4.0;
        diagonal_2 = MathUtility.round_2_Decimal(Math.pow(diagonal_2, 0.5));

        diagonal_2 = diagonal_2 * 2.0;
        diagonal_2 = MathUtility.round_2_Decimal(diagonal_2);

        QUESTION = "Find the second diagonal of a rhombus with the side is " + String.valueOf(side)
                + " inches, and the first diagonal is " + String.valueOf(diagonal_1) + " inches.";
        RIGHT_ANSWER = String.valueOf(diagonal_2) + "inches";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(diagonal_2 + 0.1)) + "inches";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(diagonal_2 - 0.1)) + "inches";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(diagonal_2 + 0.2)) + "inches";
    }

    //  RHOMBUS
    private void createProblem_08(){
        int diagonal_1 = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int diagonal_2 = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double side  = (double) (diagonal_1 * diagonal_1) / 4.0 + (double) (diagonal_2 * diagonal_2) / 4.0;
        side = MathUtility.round_2_Decimal(Math.pow(side, 0.5));

        QUESTION = "Find the side of a rhombus give the diagonals are " + String.valueOf(diagonal_1) + "  inches, " + String.valueOf(diagonal_2) + " inches.";
        RIGHT_ANSWER = String.valueOf(side) + "inches";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(side + 0.1)) + "inches";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(side - 0.1)) + "inches";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(side + 0.01)) + "inches";
    }

    //  RECTANGLE GARDEN
    private void createProblem_09(){
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double diagonal = (double) (length * length) + (double) (width * width);
        diagonal = MathUtility.round_2_Decimal(Math.pow(diagonal, 0.5));

        QUESTION = "A farmer wants to make a path on the diagonal of a rectangle garden with the length is " + String.valueOf(length)
                + "yards, and the width is " + String.valueOf(width) + " yards. Find the length of the path.";
        RIGHT_ANSWER = String.valueOf(diagonal) + "yards";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(diagonal + 0.01)) + "yards";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(diagonal - 0.01)) + "yards";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(diagonal + 0.02)) + "yards";
    }

    //  SQUARE GARDEN
    private void createProblem_10(){
        int side = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double diagonal = (double) (side * side * 2);
        diagonal = MathUtility.round_2_Decimal(Math.pow(diagonal, 0.5));

        QUESTION = "A farmer wants to make a path on the diagonal of a square garden with the side is " + String.valueOf(side)
                + "yards. Find the length of the path.";
        RIGHT_ANSWER = String.valueOf(diagonal) + "yards";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(diagonal + 0.01)) + "yards";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(diagonal - 0.01)) + "yards";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(diagonal + 0.02)) + "yards";
    }

    //  RHOMBUS GARDEN
    private void createProblem_11(){
        int diagonal_1 = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int diagonal_2 = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double side = (double) (diagonal_1 * diagonal_1) / 4.0 + (double) (diagonal_2 * diagonal_2) / 4.0;
        side = MathUtility.round_2_Decimal(Math.pow(side, 0.5) * 4.0);

        QUESTION = "Jame's uncle wants to make the path surround his rhombus garden. Find the length of the path, given "
                + "the two diagonal are: " + String.valueOf(diagonal_1) + " yards and " + String.valueOf(diagonal_2) + " yards.";
        RIGHT_ANSWER = String.valueOf(side) + "yards";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(side + 0.1)) + "yards";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(side - 0.1)) + "yards";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(side + 0.01)) + "yards";
    }

    //  TREE SHADOW
    private void createProblem_12(){
        int TreeHeight = MathUtility.getRandomPositiveNumber_2Digit() + 100;
        int Distance = MathUtility.getRandomPositiveNumber_2Digit() + 200;

        double TreeShadow = (double) (Distance * Distance - TreeHeight * TreeHeight);
        TreeShadow = Math.pow(TreeShadow, 0.5);
        TreeShadow = MathUtility.round_2_Decimal(TreeShadow);

        QUESTION = "A " + String.valueOf(TreeHeight) + "cm tree casts a shadow on the ground. "
                + "Given the distance between the top of the tree and the top of the shadow is " + String.valueOf(Distance) + "cm. "
                + "Find the length of the shadow.";
        RIGHT_ANSWER = String.valueOf(TreeShadow) + "cm";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(TreeShadow + 10.0)) + "cm";
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(TreeShadow - 10.0)) + "cm";
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(TreeShadow + 15.0)) + "cm";
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
