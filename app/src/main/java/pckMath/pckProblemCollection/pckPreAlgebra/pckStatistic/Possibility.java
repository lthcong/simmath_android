package pckMath.pckProblemCollection.pckPreAlgebra.pckStatistic;

import java.util.ArrayList;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckString.StringUtility;

/**
 * Created by Cong on 6/6/2017.
 */

public class Possibility extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public Possibility(){
        createProblem();
        swapAnswer();
    }

    public Possibility(String Question,
                        String RightAnswer,
                        String AnswerA,
                        String AnswerB,
                        String AnswerC,
                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 13;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            default:
                createProblem_01();
                break;
        }
        return new Possibility(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int NoData = 3;
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "How many 3 digits number can you make from these numbers: "
                + String.valueOf(a) + ", "
                + String.valueOf(a + 1) + ", "
                + String.valueOf(a + 2) + "?";
        RIGHT_ANSWER = String.valueOf((NoData * NoData * NoData));

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.getFactorial(NoData) + 1);
        ANSWER_C = String.valueOf(MathUtility.getFactorial(NoData) + 2);
        ANSWER_D = String.valueOf(MathUtility.getFactorial(NoData) - 1);
    }

    private void createProblem_02(){
        int NoData = 4;
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "How many 3 digits number can you make from these numbers: "
                + String.valueOf(a) + ", "
                + String.valueOf(a + 1) + ", "
                + String.valueOf(a + 2) + ", "
                + String.valueOf(a + 3) + "?";
        RIGHT_ANSWER = String.valueOf((NoData * NoData * NoData));

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.getFactorial(NoData) + 1);
        ANSWER_C = String.valueOf(MathUtility.getFactorial(NoData) + 2);
        ANSWER_D = String.valueOf(MathUtility.getFactorial(NoData) - 1);
    }

    private void createProblem_03(){
        int NoAppetizer = MathUtility.getRandomPositiveNumber_1Digit() + 3;
        int NoMainDish = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int NoDesert = MathUtility.getRandomPositiveNumber_1Digit() + 3;

        int Result = NoAppetizer * (NoMainDish) * (NoMainDish - 1) * (NoMainDish - 2) * NoDesert;

        QUESTION = "A restaurant has " + String.valueOf(NoAppetizer) + " appetizers, "
                + String.valueOf(NoMainDish) + " main dishes, "
                + String.valueOf(NoDesert) + " deserts. "
                + "How many ways you can choose 1 appetizer, 3 different main dishes, 1 desert?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result - 1);
        ANSWER_D = String.valueOf(Result + 2);
    }

    private void createProblem_04(){
        int hat = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int shirt = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int jean = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        QUESTION = "Jame has " + String.valueOf(hat) + " hats, " + String.valueOf(shirt) + " shirts, and "
                + String.valueOf(jean) + " jeans. ";
        QUESTION += "How many ways can he mix his clothes?";

        int Result = hat * shirt * jean;
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result - 1);
        ANSWER_D = String.valueOf(Result + 2);
    }

    private void createProblem_05(){
        int coin = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int dice = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;

        QUESTION = "How many possible outcome can you have when flipping " + String.valueOf(coin) + " coins, and rolling "
                + String.valueOf(dice)+ " 6-sided dice at the same time?";

        int Result = coin * 2 * dice * 6;
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result - 1);
        ANSWER_D = String.valueOf(Result + 2);
    }

    private void createProblem_06(){
        int banana = MathUtility.getRandomPositiveNumber_1Digit() + 2;
        int apple = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int orange = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        QUESTION = "There are: " + String.valueOf(banana) + " bananas, "
                + String.valueOf(apple) + " apples, " + String.valueOf(orange) + " oranges. "
                + "How many ways can you make a set of 1 banana, 1 apple, and 1 orange?";

        int Result = banana * apple * orange;
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result - 1);
        ANSWER_D = String.valueOf(Result + 2);
    }

    private void createProblem_07(){
        int AToB = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int BToC = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int Result = AToB * BToC;

        QUESTION = "There are " + String.valueOf(AToB) + " ways to go from A to B. "
                + String.valueOf(BToC) + " ways to go from B to C. "
                + "How many ways are there from A to C?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result - 1);
        ANSWER_D = String.valueOf(Result + 2);
    }

    private void createProblem_08(){
        int NoDigit = 10;
        int NoLetter = 26;

        int Result = NoDigit * NoDigit * NoDigit * NoLetter * NoLetter * NoLetter;

        QUESTION = "A license plate contains 3 random digits from  0 to 9, followed by 3 random letters from A to Z. "
                + "How many license plate can be made?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result / 100);
        ANSWER_C = String.valueOf(Result * 10);
        ANSWER_D = String.valueOf(Result / 10);
    }

    private void createProblem_09(){
        int NoDigit = 10;
        int NoLetter = 26;

        int Result = NoDigit * (NoDigit - 1) * (NoDigit - 2) * NoLetter * NoLetter * NoLetter;

        QUESTION = "A license plate contains 3 different digits from  0 to 9, followed by 3 random letters from A to Z. "
                + "How many license plate can be made?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result / 100);
        ANSWER_C = String.valueOf(Result * 10);
        ANSWER_D = String.valueOf(Result / 10);
    }

    private void createProblem_10(){
        int NoDigit = 10;
        int NoLetter = 26;

        int Result = NoDigit * NoDigit * NoDigit * NoLetter * (NoLetter - 1) * (NoLetter - 2);

        QUESTION = "A license plate contains 3 random digits from  0 to 9, followed by 3 different letters from A to Z. "
                + "How many license plate can be made?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result / 100);
        ANSWER_C = String.valueOf(Result * 10);
        ANSWER_D = String.valueOf(Result / 10);
    }

    private void createProblem_11(){
        int NoDigit = 10;
        int NoLetter = 26;

        int Result = NoDigit * (NoDigit - 1) * (NoDigit - 2) * NoLetter * (NoLetter - 1) * (NoLetter - 2);

        QUESTION = "A license plate contains 3 different digits from  0 to 9, followed by 3 different letters from A to Z. "
                + "How many license plate can be made?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result / 100);
        ANSWER_C = String.valueOf(Result * 10);
        ANSWER_D = String.valueOf(Result / 10);
    }

    private void createProblem_12(){
        int NoData = 3;
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "How many 3 digits number (all digits are different from each other) can you make from these numbers: "
                + String.valueOf(a) + ", "
                + String.valueOf(a + 1) + ", "
                + String.valueOf(a + 2) + "?";
        RIGHT_ANSWER = String.valueOf(MathUtility.getFactorial(NoData));

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.getFactorial(NoData) + 1);
        ANSWER_C = String.valueOf(MathUtility.getFactorial(NoData) + 2);
        ANSWER_D = String.valueOf(MathUtility.getFactorial(NoData) - 1);
    }

    private void createProblem_13(){
        int NoData = 4;
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "How many 3 digits number (all digits are different from each other) can you make from these numbers: "
                + String.valueOf(a) + ", "
                + String.valueOf(a + 1) + ", "
                + String.valueOf(a + 2) + ", "
                + String.valueOf(a + 3) + "?";
        RIGHT_ANSWER = String.valueOf(MathUtility.getFactorial(NoData));

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.getFactorial(NoData) + 1);
        ANSWER_C = String.valueOf(MathUtility.getFactorial(NoData) + 2);
        ANSWER_D = String.valueOf(MathUtility.getFactorial(NoData) - 1);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
