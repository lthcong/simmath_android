package pckMath.pckProblemCollection.pckPreAlgebra.pckStatistic;

import java.util.ArrayList;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckString.StringUtility;

/**
 * Created by Cong on 6/6/2017.
 */

public class StemAndLeaf extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;
    private String ONE_SPACE = " ", THREE_SPACE = " |  ";

    public StemAndLeaf(){
        createProblem();
        swapAnswer();
    }

    public StemAndLeaf(String Question,
                                String RightAnswer,
                                String AnswerA,
                                String AnswerB,
                                String AnswerC,
                                String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    private ArrayList<String> getStemAndLeaf(ArrayList<Integer> DataList){
        //  INITIAL
        ArrayList<String> StemAndLeaf = new ArrayList<>();
        for (int i = 0; i < 10; i++){
            StemAndLeaf.add(i, String.valueOf(i));
        }

        //  ORDER LIST
        DataList = MathUtility.getOrderedList(DataList);

        //  CREATE STEM AND LEAF
        int size = DataList.size();
        for (int i = 0; i < size; i++){
            int Temp = DataList.get(i);
            int Tens = Temp / 10;
            int Ones = Temp % 10;
            StemAndLeaf.set(Tens, StemAndLeaf.get(Tens) + Ones);
        }

        //  TRIM LIST
        int i = 0;
        while(i < StemAndLeaf.size()){
            if (StemAndLeaf.get(i).length() == 1){
                StemAndLeaf.remove(i);
                i = 0;
            }
            else {
                i++;
            }
        }

        return StemAndLeaf;
    }

    private String printStemAndLeaf(ArrayList<String> StemAndLeaf){
        String StemAndLeafChart = "<br>";

        int size = StemAndLeaf.size();
        for (int i = 0; i < size; i++){
            String Temp = StemAndLeaf.get(i);

            String TempChart = Temp.substring(0, 1) + THREE_SPACE;

            int StLength = Temp.length();
            for (int j = 1; j < StLength; j++){
                TempChart += Temp.substring(j, j + 1) + ONE_SPACE;
            }

            StemAndLeafChart += TempChart + "<br>";
        }

        return StemAndLeafChart;
    }

    @Override
    public MathProblem createProblem() {
        createProblem_01();
        return new StemAndLeaf(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        ArrayList<Integer> DataList = MathUtility.getDataList_10Element();

        QUESTION = "Find the Stem and Leaf Plot of the data: " + StringUtility.writeArrayList(DataList);
        RIGHT_ANSWER = printStemAndLeaf(getStemAndLeaf(DataList));

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = printStemAndLeaf(getStemAndLeaf(MathUtility.getDataList_10Element()));
        ANSWER_C = printStemAndLeaf(getStemAndLeaf(MathUtility.getDataList()));
        ANSWER_D = printStemAndLeaf(getStemAndLeaf(MathUtility.getDataList()));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
