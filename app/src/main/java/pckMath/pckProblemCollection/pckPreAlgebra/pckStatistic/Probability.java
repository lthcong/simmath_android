package pckMath.pckProblemCollection.pckPreAlgebra.pckStatistic;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckString.StringUtility;

/**
 * Created by Cong on 6/7/2017.
 */

public class Probability extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public Probability(){
        createProblem();
        swapAnswer();
    }

    public Probability(String Question,
                        String RightAnswer,
                        String AnswerA,
                        String AnswerB,
                        String AnswerC,
                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 12;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            default:
                createProblem_01();
                break;
        }
        return new Probability(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int banana = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int apple = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int cherry  = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int total = banana + apple + cherry;

        double probability = (double) banana / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        QUESTION = "There are " + String.valueOf(banana ) + " bananas, " + String.valueOf(apple) + " apples, "
                + String.valueOf(cherry) + " cherries. Find the probability of getting a banana.";
        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_02(){
        int banana = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int apple = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int cherry  = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int total = banana + apple + cherry;

        double probability = (double) apple / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        QUESTION = "There are " + String.valueOf(banana ) + " bananas, " + String.valueOf(apple) + " apples, "
                + String.valueOf(cherry) + " cherries. Find the probability of getting an apple.";
        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));    }

    private void createProblem_03(){
        int banana = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int apple = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int cherry  = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int total = banana + apple + cherry;

        double probability = (double) cherry / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        QUESTION = "There are " + String.valueOf(banana ) + " bananas, " + String.valueOf(apple) + " apples, "
                + String.valueOf(cherry) + " cherries. Find the probability of getting a cherry.";

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_04(){
        int banana = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int apple = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int cherry  = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int total = banana + apple + cherry;

        double probability = (double) banana / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        QUESTION = "There are total " + String.valueOf(total) + " bananas, apples, cherries. " + String.valueOf(apple) + " apples, "
                + String.valueOf(cherry) + " cherries. Find the probability of getting a banana.";

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_05(){
        int banana = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int apple = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int cherry  = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int total = banana + apple + cherry;

        double probability = (double) apple / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        QUESTION = "There are total " + String.valueOf(total) + " bananas, apples, cherries. " + String.valueOf(banana) + " bananas, "
                + String.valueOf(cherry) + " cherries. Find the probability of getting an apple.";

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_06(){
        int banana = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int apple = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int cherry  = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int total = banana + apple + cherry;

        double probability = (double) cherry / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        QUESTION = "There are total " + String.valueOf(total) + " bananas, apples, cherries. " + String.valueOf(banana) + " bananas, "
                + String.valueOf(apple) + " apples. Find the probability of getting a cherry.";

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_07(){
        int dog = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int cat = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int gold_fish = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int total = dog + cat + gold_fish;

        double probability = (double) dog / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        QUESTION = "There are " + String.valueOf(dog) + " dogs, " + String.valueOf(cat) + " cats, and "
                + gold_fish + " gold fishes. Find the probability of getting a dog.";

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_08(){
        int dog = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int cat = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int gold_fish = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int total = dog + cat + gold_fish;

        double probability = (double) cat / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        QUESTION = "There are " + String.valueOf(dog) + " dogs, " + String.valueOf(cat) + " cats, and "
                + gold_fish + " gold fishes. Find the probability of getting a cat.";

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_09(){
        int dog = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int cat = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int gold_fish = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int total = dog + cat + gold_fish;

        double probability = (double) gold_fish / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        QUESTION = "There are " + String.valueOf(dog) + " dogs, " + String.valueOf(cat) + " cats, and "
                + gold_fish + " gold fishes. Find the probability of getting a gold fish.";

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_10(){
        int dog = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int cat = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int gold_fish = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int total = dog + cat + gold_fish;

        double probability = (double) dog / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        QUESTION = "There are total " + String.valueOf(total) + " dogs, cats, and gold fishes. "
                + String.valueOf(cat) + " cats, "
                + String.valueOf(gold_fish) + " gold fishes. Find the probability of getting a dog.";

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_11(){
        int dog = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int cat = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int gold_fish = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int total = dog + cat + gold_fish;

        double probability = (double) cat / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        QUESTION = "There are total " + String.valueOf(total) + " dogs, cats, and gold fishes. " + String.valueOf(dog) + " dogs, "
                + String.valueOf(gold_fish) + " gold fishes. Find the probability of getting a cat.";

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_12(){
        int dog = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int cat = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int gold_fish = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int total = dog + cat + gold_fish;

        double probability = (double) cat / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        QUESTION = "There are total " + String.valueOf(total) + " dogs, cats, and gold fishes. " + String.valueOf(cat) + " cats, "
                + String.valueOf(dog) + " dogs. Find the probability of getting a gold fish.";

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
