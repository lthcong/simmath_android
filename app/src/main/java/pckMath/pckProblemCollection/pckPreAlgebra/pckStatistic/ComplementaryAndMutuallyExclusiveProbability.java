package pckMath.pckProblemCollection.pckPreAlgebra.pckStatistic;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/7/2017.
 */

public class ComplementaryAndMutuallyExclusiveProbability  extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;
    private String YES = "Yes.", NO = "No.", EITHER = "Either.", NEITHER = "Neither.";

    public ComplementaryAndMutuallyExclusiveProbability(){
        createProblem();
        swapAnswer();
    }

    public ComplementaryAndMutuallyExclusiveProbability(String Question,
                                                         String RightAnswer,
                                                         String AnswerA,
                                                         String AnswerB,
                                                         String AnswerC,
                                                         String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 14;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            default:
                createProblem_01();
                break;
        }
        return new ComplementaryAndMutuallyExclusiveProbability(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int apple = MathUtility.getRandomPositiveNumber_1Digit();
        int banana = MathUtility.getRandomPositiveNumber_1Digit();
        int cherry = MathUtility.getRandomPositiveNumber_1Digit();

        int total = apple + banana + cherry;

        QUESTION = "There are " + String.valueOf(apple) + " apples, "
         + String.valueOf(banana) + " bananas, and " + String.valueOf(cherry) + " cherries. "
         + "Find the probability of not selecting an apple?";

        double probability = (double) apple / (double) total;
        probability = 1.0 - probability;
        probability = MathUtility.round_2_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_02(){
        int apple = MathUtility.getRandomPositiveNumber_1Digit();
        int banana = MathUtility.getRandomPositiveNumber_1Digit();
        int cherry = MathUtility.getRandomPositiveNumber_1Digit();

        int total = apple + banana + cherry;

        QUESTION = "There are " + String.valueOf(apple) + " apples, "
         + String.valueOf(banana) + " bananas, and " + String.valueOf(cherry) + " cherries. "
         + "Find the probability of not selecting a banana?";

        double probability = (double) banana / (double) total;
        probability = 1.0 - probability;
        probability = MathUtility.round_2_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_03(){
        int apple = MathUtility.getRandomPositiveNumber_1Digit();
        int banana = MathUtility.getRandomPositiveNumber_1Digit();
        int cherry = MathUtility.getRandomPositiveNumber_1Digit();

        int total = apple + banana + cherry;

        QUESTION = "There are " + String.valueOf(apple) + " apples, "
         + String.valueOf(banana) + " bananas, and " + String.valueOf(cherry) + " cherries. "
         + "Find the probability of not selecting a cherry?";

        double probability = (double) cherry / (double) total;
        probability = 1.0 - probability;
        probability = MathUtility.round_2_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_04(){
        int cardIndex = MathUtility.getRandomPositiveNumber_1Digit();
        
        QUESTION = "Selecting a " + String.valueOf(cardIndex) + " from a deck of 52 cards, "
                + "and selecting a black card from a deck of 52 cards. "
                + "Are these events mutually exclusive?";

        RIGHT_ANSWER = NO;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = YES;
        ANSWER_C = EITHER;
        ANSWER_D = NEITHER;
    }

    private void createProblem_05(){
        int cardIndex = MathUtility.getRandomPositiveNumber_1Digit();
        
        QUESTION = "Selecting a " + String.valueOf(cardIndex) + " from a deck of 52 cards, "
                + "and selecting a red card from a deck of 52 cards. "
                + "Are these events mutually exclusive?";

        RIGHT_ANSWER = NO;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = YES;
        ANSWER_C = EITHER;
        ANSWER_D = NEITHER;
    }

    private void createProblem_06(){
        int cardIndex = MathUtility.getRandomPositiveNumber_1Digit();
        
        QUESTION = "Selecting a " + String.valueOf(cardIndex) + " from a deck of 52 cards, "
         + "and selecting a club card from a deck of 52 cards. "
         + "Are these events mutually exclusive?";

        RIGHT_ANSWER = NO;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = YES;
        ANSWER_C = EITHER;
        ANSWER_D = NEITHER;
    }

    private void createProblem_07(){
        int cardIndex = MathUtility.getRandomPositiveNumber_1Digit();
        
        QUESTION = "Selecting a " + String.valueOf(cardIndex) + " from a deck of 52 cards, "
                + "and selecting a diamond card from a deck of 52 cards. "
                + "Are these events mutually exclusive?";

        RIGHT_ANSWER = NO;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = YES;
        ANSWER_C = EITHER;
        ANSWER_D = NEITHER;
    }

    private void createProblem_08(){
        int cardIndex = MathUtility.getRandomPositiveNumber_1Digit();
        
        QUESTION = "Selecting a " + String.valueOf(cardIndex) + " from a deck of 52 cards. "
                + "and selecting a heart card from a deck of 52 cards. "
                + "Are these events mutually exclusive?";

        RIGHT_ANSWER = NO;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = YES;
        ANSWER_C = EITHER;
        ANSWER_D = NEITHER;
    }

    private void createProblem_09(){
        int cardIndex = MathUtility.getRandomPositiveNumber_1Digit();
        
        QUESTION = "Selecting a " + String.valueOf(cardIndex) + " from a deck of 52 cards, "
                + "and selecting a spades card from a deck of 52 cards. "
                + "Are these events mutually exclusive?";

        RIGHT_ANSWER = NO;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = YES;
        ANSWER_C = EITHER;
        ANSWER_D = NEITHER;
    }

    private void createProblem_10(){
        int cardIndex = MathUtility.getRandomPositiveNumber_1Digit();
        
        QUESTION = "Selecting a red " + String.valueOf(cardIndex) + " from a deck of 52 cards, "
                + "and selecting a club card from a deck of 52 cards. "
                + "Are these events mutually exclusive?";

        RIGHT_ANSWER = NO;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = YES;
        ANSWER_C = EITHER;
        ANSWER_D = NEITHER;
    }

    private void createProblem_11(){
        int cardIndex = MathUtility.getRandomPositiveNumber_1Digit();
        QUESTION = "Selecting a " + String.valueOf(cardIndex) + " diamond. ";

        cardIndex = MathUtility.getRandomPositiveNumber_1Digit();
        QUESTION += "Selecting a " + String.valueOf(cardIndex) + " club. ";
        QUESTION += "Are these events mutually exclusive?";

        RIGHT_ANSWER = YES;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = NO;
        ANSWER_C = EITHER;
        ANSWER_D = NEITHER;
    }

    private void createProblem_12(){
        int cardIndex = MathUtility.getRandomPositiveNumber_1Digit();
        QUESTION = "Selecting a " + String.valueOf(cardIndex) + " heart. ";

        cardIndex = MathUtility.getRandomPositiveNumber_1Digit();
        QUESTION += "Selecting a " + String.valueOf(cardIndex) + " spades ";
        QUESTION += "Are these events mutually exclusive?";

        RIGHT_ANSWER = YES;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = NO;
        ANSWER_C = EITHER;
        ANSWER_D = NEITHER;
    }

    private void createProblem_13(){
        int ComSci_m = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = ComSci_m + AppMath_m;

        int ComSci_f = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = ComSci_f + AppMath_f;

        int total = Student_m + Student_f;

        QUESTION = "A college has total of " + String.valueOf(total) + " students. "
                + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
                + "The number of male students who has Computer Science major is " + String.valueOf(ComSci_m)
                + ", Applied Mathematics major is " + String.valueOf(AppMath_m) + ". "
                + "The number of female students who has Computer Science major is " + String.valueOf(ComSci_f)
                + ", Applied Mathematics major is " + String.valueOf(AppMath_f) + ". "
                + "Select a student from Computer Science major. "
                + "Select a female student from Applied Mathematics major. "
                + "Are these events mutually exclusive";

        RIGHT_ANSWER = YES;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = NO;
        ANSWER_C = EITHER;
        ANSWER_D = NEITHER;
    }

    private void createProblem_14(){
        int ComSci_m = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = ComSci_m + AppMath_m;

        int ComSci_f = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = ComSci_f + AppMath_f;

        int total = Student_m + Student_f;

        QUESTION = "A college has total of " + String.valueOf(total) + " students. "
                + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
                + "The number of male students who has Computer Science major is " + String.valueOf(ComSci_m)
                + ", Applied Mathematics major is " + String.valueOf(AppMath_m) + ". "
                + "The number of female students who has Computer Science major is " + String.valueOf(ComSci_f)
                + ", Applied Mathematics major is " + String.valueOf(AppMath_f) + ". "
                + "Select a male student from Computer Science major. "
                + "Select a female student from Applied Mathematics major. "
                + "Are these events mutually exclusive";

        RIGHT_ANSWER = YES;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = NO;
        ANSWER_C = EITHER;
        ANSWER_D = NEITHER;
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
