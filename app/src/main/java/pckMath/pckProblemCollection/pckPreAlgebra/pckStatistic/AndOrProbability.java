package pckMath.pckProblemCollection.pckPreAlgebra.pckStatistic;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/7/2017.
 */

public class AndOrProbability extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public AndOrProbability(){
        createProblem();
        swapAnswer();
    }

    public AndOrProbability(String Question,
                        String RightAnswer,
                        String AnswerA,
                        String AnswerB,
                        String AnswerC,
                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 16;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            case 14:
                createProblem_15();
                break;
            case 15:
                createProblem_16();
                break;
            default:
                createProblem_01();
                break;
        }
        return new AndOrProbability(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int ComSci_m = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = ComSci_m + AppMath_m;

        int ComSci_f = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = ComSci_f + AppMath_f;

        int total = Student_m + Student_f;

        QUESTION = "A college has total of " + String.valueOf(total) + " students. "                
             + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
             + "The number of male students who has Computer Science major is " + String.valueOf(ComSci_m)
             + ", Applied Mathematics major is " + String.valueOf(AppMath_m) + ". "
             + "The number of female students who has Computer Science major is " + String.valueOf(ComSci_f)
             + ", Applied Mathematics major is " + String.valueOf(AppMath_f) + ". "
             + "Find the probability of selecting a male student AND has Computer Science major.";

        double probability = (double) ComSci_m / (double) total;
        probability = MathUtility.round_2_Decimal(probability);
        
        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_02(){
        int ComSci_m = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = ComSci_m + AppMath_m;

        int ComSci_f = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = ComSci_f + AppMath_f;

        int total = Student_m + Student_f;

        QUESTION = "A college has total of " + String.valueOf(total) + " students. "
        + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
        + "The number of male students who has Computer Science major is " + String.valueOf(ComSci_m)
        + ", Applied Mathematics major is " + String.valueOf(AppMath_m) + ". "
        + "The number of female students who has Computer Science major is " + String.valueOf(ComSci_f)
        + ", Applied Mathematics major is " + String.valueOf(AppMath_f) + ". "
        + "Find the probability of selecting a male student AND has Applied Mathematics major.";

        double probability = (double) AppMath_m / (double) total;
        probability = MathUtility.round_2_Decimal(probability);
        
        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));    
    }

    private void createProblem_03(){
        int ComSci_m = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = ComSci_m + AppMath_m;

        int ComSci_f = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = ComSci_f + AppMath_f;

        int total = Student_m + Student_f;

        QUESTION = "A college has total of " + String.valueOf(total) + " students. "
        + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
        + "The number of male students who has Computer Science major is " + String.valueOf(ComSci_m)
        + ", Applied Mathematics major is " + String.valueOf(AppMath_m) + ". "
        + "The number of female students who has Computer Science major is " + String.valueOf(ComSci_f)
        + ", Applied Mathematics major is " + String.valueOf(AppMath_f) + ". "
        + "Find the probability of selecting a female student AND has Computer Science major.";

        double probability = (double) ComSci_f / (double) total;
        probability = MathUtility.round_2_Decimal(probability);
        
        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_04(){
        int ComSci_m = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = ComSci_m + AppMath_m;

        int ComSci_f = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = ComSci_f + AppMath_f;

        int total = Student_m + Student_f;

        QUESTION = "A college has total of " + String.valueOf(total) + " students. "
        + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
        + "The number of male students who has Computer Science major is " + String.valueOf(ComSci_m)
        + ", Applied Mathematics major is " + String.valueOf(AppMath_m) + ". "
        + "The number of female students who has Computer Science major is " + String.valueOf(ComSci_f)
        + ", Applied Mathematics major is " + String.valueOf(AppMath_f) + ". "
        + "Find the probability of selecting a female student AND has Applied Mathematics major.";

        double probability = (double) AppMath_f / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_05(){
        int Nursing_m = MathUtility.getRandomPositiveNumber_2Digit();
        int ChEng_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = Nursing_m + ChEng_m;

        int Nursing_f = MathUtility.getRandomPositiveNumber_2Digit();
        int ChEng_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = Nursing_f + ChEng_f;

        int total = Student_m + Student_f;

        QUESTION = "A college has total of " + String.valueOf(total) + " students. "
        + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
        + "The number of male students who has Nursing major is " + String.valueOf(Nursing_m)
        + ", Chemical Engineer major is " + String.valueOf(ChEng_m) + ". "
        + "The number of female students who has Nursing major is " + String.valueOf(Nursing_f)
        + ", Chemical Engineer major is " + String.valueOf(ChEng_f) + ". "
        + "Find the probability of selecting a male student AND has Nursing major.";

        double probability = (double) Nursing_m / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_06(){
        int Nursing_m = MathUtility.getRandomPositiveNumber_2Digit();
        int ChEng_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = Nursing_m + ChEng_m;

        int Nursing_f = MathUtility.getRandomPositiveNumber_2Digit();
        int ChEng_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = Nursing_f + ChEng_f;

        int total = Student_m + Student_f;

        QUESTION = "A college has total of " + String.valueOf(total) + " students. "
        + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
        + "The number of male students who has Nursing major is " + String.valueOf(Nursing_m)
        + ", Chemical Engineer major is " + String.valueOf(ChEng_m) + ". "
        + "The number of female students who has Nursing major is " + String.valueOf(Nursing_f)
        + ", Chemical Engineer major is " + String.valueOf(ChEng_f) + ". "
        + "Find the probability of selecting a male student AND has Chemical Engineer major.";

        double probability = (double) ChEng_m / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_07(){
        int Nursing_m = MathUtility.getRandomPositiveNumber_2Digit();
        int ChEng_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = Nursing_m + ChEng_m;

        int Nursing_f = MathUtility.getRandomPositiveNumber_2Digit();
        int ChEng_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = Nursing_f + ChEng_f;

        int total = Student_m + Student_f;
        
        QUESTION = "A college has total of " + String.valueOf(total) + " students. "
                + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
                + "The number of male students who has Nursing major is " + String.valueOf(Nursing_m)
                + ", Chemical Engineer major is " + String.valueOf(ChEng_m) + ". "
                + "The number of female students who has Nursing major is " + String.valueOf(Nursing_f)
                + ", Chemical Engineer major is " + String.valueOf(ChEng_f) + ". "
                + "Find the probability of selecting a female student AND has Nursing major.";

        double probability = (double) Nursing_f / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_08(){
        int Nursing_m = MathUtility.getRandomPositiveNumber_2Digit();
        int ChEng_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = Nursing_m + ChEng_m;

        int Nursing_f = MathUtility.getRandomPositiveNumber_2Digit();
        int ChEng_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = Nursing_f + ChEng_f;

        int total = Student_m + Student_f;

        QUESTION = "A college has total of " + String.valueOf(total) + " students. "
                + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
                + "The number of male students who has Nursing major is " + String.valueOf(Nursing_m)
                + ", Chemical Engineer major is " + String.valueOf(ChEng_m) + ". "
                + "The number of female students who has Nursing major is " + String.valueOf(Nursing_f)
                + ", Chemical Engineer major is " + String.valueOf(ChEng_f) + ". "
                + "Find the probability of selecting a female student AND has Chemical Engineer major.";

        double probability = (double) ChEng_f / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_09(){
        int ComSci_m = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = ComSci_m + AppMath_m;

        int ComSci_f = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = ComSci_f + AppMath_f;

        int total = Student_m + Student_f;

        QUESTION = "A college has total of " + String.valueOf(total) + " students. "
                + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
                + "The number of male students who has Computer Science major is " + String.valueOf(ComSci_m)
                + ", Applied Mathematics major is " + String.valueOf(AppMath_m) + ". "
                + "The number of female students who has Computer Science major is " + String.valueOf(ComSci_f)
                + ", Applied Mathematics major is " + String.valueOf(AppMath_f) + ". "
                + "Find the probability of selecting a male student OR has Computer Science major.";

        double probability = (double) (Student_m + (ComSci_m + ComSci_f) - ComSci_m) / (double) total;
        probability = MathUtility.round_2_Decimal(probability);
        
        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_10(){
        int ComSci_m = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = ComSci_m + AppMath_m;

        int ComSci_f = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = ComSci_f + AppMath_f;

        int total = Student_m + Student_f;

        QUESTION = "A college has total of " + String.valueOf(total) + " students. "
                + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
                + "The number of male students who has Computer Science major is " + String.valueOf(ComSci_m)
                + ", Applied Mathematics major is " + String.valueOf(AppMath_m) + ". "
                + "The number of female students who has Computer Science major is " + String.valueOf(ComSci_f)
                + ", Applied Mathematics major is " + String.valueOf(AppMath_f) + ". "
                + "Find the probability of selecting a male student OR has Applied Mathematics major.";

        double probability = (double) (Student_m + (AppMath_m + AppMath_f) - AppMath_m) / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_11(){
        int ComSci_m = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = ComSci_m + AppMath_m;

        int ComSci_f = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = ComSci_f + AppMath_f;

        int total = Student_m + Student_f;

        QUESTION = "A college has total of " + String.valueOf(total) + " students. "
                + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
                + "The number of male students who has Computer Science major is " + String.valueOf(ComSci_m)
                + ", Applied Mathematics major is " + String.valueOf(AppMath_m) + ". "
                + "The number of female students who has Computer Science major is " + String.valueOf(ComSci_f)
                + ", Applied Mathematics major is " + String.valueOf(AppMath_f) + ". "
                + "Find the probability of selecting a female student OR has Computer Science major.";

        double probability = (double) (Student_f + (ComSci_m + ComSci_f) - ComSci_f) / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_12(){
        int ComSci_m = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = ComSci_m + AppMath_m;

        int ComSci_f = MathUtility.getRandomPositiveNumber_2Digit();
        int AppMath_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = ComSci_f + AppMath_f;

        int total = Student_m + Student_f;

        QUESTION = "A college has total of " + String.valueOf(total) + " students. "
                + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
                + "The number of male students who has Computer Science major is " + String.valueOf(ComSci_m)
                + ", Applied Mathematics major is " + String.valueOf(AppMath_m) + ". "
                + "The number of female students who has Computer Science major is " + String.valueOf(ComSci_f)
                + ", Applied Mathematics major is " + String.valueOf(AppMath_f) + ". "
                + "Find the probability of selecting a female student OR has Applied Mathematics major.";

        double probability = (double) (Student_f + (AppMath_m + AppMath_f) - AppMath_f) / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_13(){
        int Nursing_m = MathUtility.getRandomPositiveNumber_2Digit();
        int ChEng_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = Nursing_m + ChEng_m;

        int Nursing_f = MathUtility.getRandomPositiveNumber_2Digit();
        int ChEng_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = Nursing_f + ChEng_f;

        int total = Student_m + Student_f;

        QUESTION = "A college has total of " + String.valueOf(total) + " students. "
                + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
                + "The number of male students who has Nursing major is " + String.valueOf(Nursing_m)
                + ", Chemical Engineer major is " + String.valueOf(ChEng_m) + ". "
                + "The number of female students who has Nursing major is " + String.valueOf(Nursing_f)
                + ", Chemical Engineer major is " + String.valueOf(ChEng_f) + ". "
                + "Find the probability of selecting a male student OR has Nursing major.";

        double probability = (double) (Student_m + (Nursing_m + Nursing_f) - Nursing_m) / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_14(){
        int Nursing_m = MathUtility.getRandomPositiveNumber_2Digit();
        int ChEng_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = Nursing_m + ChEng_m;

        int Nursing_f = MathUtility.getRandomPositiveNumber_2Digit();
        int ChEng_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = Nursing_f + ChEng_f;

        int total = Student_m + Student_f;

        QUESTION = "A college has total of " + String.valueOf(total) + " students. "
                + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
                + "The number of male students who has Nursing major is " + String.valueOf(Nursing_m)
                + ", Chemical Engineer major is " + String.valueOf(ChEng_m) + ". "
                + "The number of female students who has Nursing major is " + String.valueOf(Nursing_f)
                + ", Chemical Engineer major is " + String.valueOf(ChEng_f) + ". "
                + "Find the probability of selecting a male student OR has Chemical Engineer major.";

        double probability = (double) (Student_m + (ChEng_m + ChEng_f) - ChEng_m) / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_15(){
        int Nursing_m = MathUtility.getRandomPositiveNumber_2Digit();
        int ChEng_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = Nursing_m + ChEng_m;

        int Nursing_f = MathUtility.getRandomPositiveNumber_2Digit();
        int ChEng_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = Nursing_f + ChEng_f;

        int total = Student_m + Student_f;

        QUESTION = "A college has total of " + String.valueOf(total) + " students. "
                + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
                + "The number of male students who has Nursing major is " + String.valueOf(Nursing_m)
                + ", Chemical Engineer major is " + String.valueOf(ChEng_m) + ". "
                + "The number of female students who has Nursing major is " + String.valueOf(Nursing_f)
                + ", Chemical Engineer major is " + String.valueOf(ChEng_f) + ". "
                + "Find the probability of selecting a female student OR has Nursing major.";

        double probability = (double) (Student_f + (Nursing_m + Nursing_f) - Nursing_f) / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    private void createProblem_16(){
        int Nursing_m = MathUtility.getRandomPositiveNumber_2Digit();
        int ChEng_m = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_m = Nursing_m + ChEng_m;

        int Nursing_f = MathUtility.getRandomPositiveNumber_2Digit();
        int ChEng_f = MathUtility.getRandomPositiveNumber_2Digit();
        int Student_f = Nursing_f + ChEng_f;

        int total = Student_m + Student_f;

        QUESTION = "A college has total of " + String.valueOf(total) + " students. "
                + String.valueOf(Student_m) + " male students, " + String.valueOf(Student_f) + " female students. "
                + "The number of male students who has Nursing major is " + String.valueOf(Nursing_m)
                + ", Chemical Engineer major is " + String.valueOf(ChEng_m) + ". "
                + "The number of female students who has Nursing major is " + String.valueOf(Nursing_f)
                + ", Chemical Engineer major is " + String.valueOf(ChEng_f) + ". "
                + "Find the probability of selecting a female student OR has Chemical Engineer major.";

        double probability = (double) (Student_f + (ChEng_m + ChEng_f) - ChEng_f) / (double) total;
        probability = MathUtility.round_2_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(probability - 0.01));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
