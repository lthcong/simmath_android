package pckMath.pckProblemCollection.pckPreAlgebra.pckStatistic;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/7/2017.
 */

public class CompoundEventProbability extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public CompoundEventProbability(){
        createProblem();
        swapAnswer();
    }

    public CompoundEventProbability(String Question,
                                                         String RightAnswer,
                                                         String AnswerA,
                                                         String AnswerB,
                                                         String AnswerC,
                                                         String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 7;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            default:
                createProblem_01();
                break;
        }
        return new CompoundEventProbability(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int dice = MathUtility.getRandomPositiveNumber_1Digit() % 6 + 1;

        QUESTION = "Find the probability of getting a " + String.valueOf(dice) + " when rolling the 6-sided dice, ";
        if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0) {
            QUESTION += "and getting a tail when flipping a coin.";
        }
        else {
            QUESTION += "and getting a head when flipping a coin.";
        }

        double probability = 1.0 / 12.0;
        probability = MathUtility.round_4_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(probability - 0.01));
    }

    private void createProblem_02(){
        int card = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Find the probability of getting a " + String.valueOf(card) + " from a deck card ";
        if (MathUtility.getRandomPositiveNumber_1Digit() % 2 == 0) {
            QUESTION += "and getting a tail when flipping a coin.";
        }
        else {
            QUESTION += "and getting a head when flipping a coin.";
        }

        double probability = 4.0 / 52.0 * 1.0 / 2.0;
        probability = MathUtility.round_4_Decimal(probability);
        
        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(probability - 0.01));
    }

    private void createProblem_03(){
        int card = MathUtility.getRandomPositiveNumber_1Digit();
        int dice = MathUtility.getRandomPositiveNumber_1Digit() % 6 + 1;

        QUESTION = "Find the probability of getting a " + String.valueOf(card) + " from a deck card "
                + "and getting a " + String.valueOf(dice) + " when rolling the 6-sided dice, ";

        double probability = 4.0 / 52.0 * 1.0 / 6.0;
        probability = MathUtility.round_4_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(probability - 0.01));
    }

    private void createProblem_04(){
        int card = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Find the probability of getting a " + String.valueOf(card) + " from a deck card "
                + "and getting an even number when rolling the 6-sided dice, ";

        double probability = 4.0 / 52.0 * 1.0 / 2.0;
        probability = MathUtility.round_4_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(probability - 0.01));
    }

    private void createProblem_05(){
        int card = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Find the probability of getting a " + String.valueOf(card) + " from a deck card "
                + "and getting an odd number when rolling the 6-sided dice, ";

        double probability = 4.0 / 52.0 * 1.0 / 2.0;
        probability = MathUtility.round_4_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(probability - 0.01));
    }

    private void createProblem_06(){
        int card = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Find the probability of getting a " + String.valueOf(card) + " from a deck card "
                + "and getting a multiple of 2 when rolling the 6-sided dice, ";

        double probability = 4.0 / 52.0 * 1.0 / 2.0;
        probability = MathUtility.round_4_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(probability - 0.01));
    }

    private void createProblem_07(){
        int card = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Find the probability of getting a " + String.valueOf(card) + " from a deck card "
                + "and getting a multiple of 3 when rolling the 6-sided dice, ";

        double probability = 4.0 / 52.0 * 1.0 / 2.0;
        probability = MathUtility.round_4_Decimal(probability);

        RIGHT_ANSWER = String.valueOf(probability);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(probability + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(probability + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(probability - 0.01));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
