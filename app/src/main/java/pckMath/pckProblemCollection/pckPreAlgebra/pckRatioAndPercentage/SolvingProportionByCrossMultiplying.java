package pckMath.pckProblemCollection.pckPreAlgebra.pckRatioAndPercentage;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 6/3/2017.
 */

public class SolvingProportionByCrossMultiplying extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SolvingProportionByCrossMultiplying(){
        createProblem();
        swapAnswer();
    }

    public SolvingProportionByCrossMultiplying(String Question,
                                    String RightAnswer,
                                    String AnswerA,
                                    String AnswerB,
                                    String AnswerC,
                                    String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 8;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SolvingProportionByCrossMultiplying(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  x/a = b/c
    private void createProblem_01(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.findEquivalentFraction(FractionA);

        QUESTION = "x:" + String.valueOf(FractionA.getDenominator()) + " is the same as "
                + FractionB.getNumerator() + ":" + FractionB.getDenominator();
        RIGHT_ANSWER = String.valueOf(FractionA.getNumerator());

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(FractionA.getNumerator() + 1);
        ANSWER_C = String.valueOf(FractionA.getNumerator() - 1);
        ANSWER_D = String.valueOf(FractionA.getNumerator() + 2);
    }

    //  a/x = b/c
    private void createProblem_02(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.findEquivalentFraction(FractionA);

        QUESTION = String.valueOf(FractionA.getNumerator()) + ":x is the same as "
                + FractionB.getNumerator() + ":" + FractionB.getDenominator();
        RIGHT_ANSWER = String.valueOf(FractionA.getDenominator());

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(FractionA.getDenominator() + 1);
        ANSWER_C = String.valueOf(FractionA.getDenominator() - 1);
        ANSWER_D = String.valueOf(FractionA.getDenominator() + 2);
    }

    //  a/b = x/c
    private void createProblem_03(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.findEquivalentFraction(FractionA);

        QUESTION = FractionB.getNumerator() + ":" + FractionB.getDenominator() + " is the same as "
                + "x:" + String.valueOf(FractionA.getDenominator());
        RIGHT_ANSWER = String.valueOf(FractionA.getNumerator());

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(FractionA.getNumerator() + 1);
        ANSWER_C = String.valueOf(FractionA.getNumerator() - 1);
        ANSWER_D = String.valueOf(FractionA.getNumerator() + 2);
    }

    //  a/b = c/x
    private void createProblem_04(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.findEquivalentFraction(FractionA);

        QUESTION = FractionB.getNumerator() + ":" + FractionB.getDenominator() + " is the same as "
                + String.valueOf(FractionA.getNumerator()) + ":x";
        RIGHT_ANSWER = String.valueOf(FractionA.getDenominator());

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(FractionA.getDenominator() + 1);
        ANSWER_C = String.valueOf(FractionA.getDenominator() - 1);
        ANSWER_D = String.valueOf(FractionA.getDenominator() + 2);
    }

    //  x/a = b/c
    private void createProblem_05(){
        Fraction FractionA = FractionMath.getRandomFraction();

        int Temp = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        if (MathUtility.getRandomPositiveNumber() % 2 == 0){
            Temp = Temp * -1;
        }

        Fraction FractionB = FractionMath.findEquivalentFraction(FractionA, Temp);

        QUESTION = InfoCollector.getStartNumerator() + "x" + InfoCollector.getEndNumerator() + "/"
                + InfoCollector.getStartDenominator() + FractionA.getDenominator() + InfoCollector.getEndDenominator()
                + " = " + FractionB.toString();
        RIGHT_ANSWER = String.valueOf(FractionA.getNumerator());

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(FractionA.getNumerator() + 2);
        ANSWER_C = String.valueOf(FractionA.getNumerator() + 1);
        ANSWER_D = String.valueOf(FractionA.getNumerator() - 1);
    }

    //  a/x = b/c
    private void createProblem_06(){
        Fraction FractionA = FractionMath.getRandomFraction();

        int Temp = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        if (MathUtility.getRandomPositiveNumber() % 2 == 0){
            Temp = Temp * -1;
        }

        Fraction FractionB = FractionMath.findEquivalentFraction(FractionA, Temp);

        QUESTION = InfoCollector.getStartNumerator() + FractionA.getNumerator() + InfoCollector.getEndNumerator() + "/"
                + InfoCollector.getStartDenominator() + "x" + InfoCollector.getEndDenominator()
                + " = " + FractionB.toString();
        RIGHT_ANSWER = String.valueOf(FractionA.getDenominator());

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(FractionA.getDenominator() + 2);
        ANSWER_C = String.valueOf(FractionA.getDenominator() + 1);
        ANSWER_D = String.valueOf(FractionA.getDenominator() - 1);
    }

    //  a/b = x/c
    private void createProblem_07(){
        Fraction FractionA = FractionMath.getRandomFraction();

        int Temp = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        if (MathUtility.getRandomPositiveNumber() % 2 == 0){
            Temp = Temp * -1;
        }

        Fraction FractionB = FractionMath.findEquivalentFraction(FractionA, Temp);

        QUESTION = FractionB.toString() + " = "
                + InfoCollector.getStartNumerator() + "x" + InfoCollector.getEndNumerator() + "/"
                + InfoCollector.getStartDenominator() + FractionA.getDenominator() + InfoCollector.getEndDenominator();
        RIGHT_ANSWER = String.valueOf(FractionA.getNumerator());

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(FractionA.getNumerator() + 2);
        ANSWER_C = String.valueOf(FractionA.getNumerator() + 1);
        ANSWER_D = String.valueOf(FractionA.getNumerator() - 1);
    }

    //  a/b = c/x
    private void createProblem_08(){
        Fraction FractionA = FractionMath.getRandomFraction();

        int Temp = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        if (MathUtility.getRandomPositiveNumber() % 2 == 0){
            Temp = Temp * -1;
        }

        Fraction FractionB = FractionMath.findEquivalentFraction(FractionA, Temp);

        QUESTION = FractionB.toString() + " = "
                + InfoCollector.getStartNumerator() + FractionA.getNumerator() + InfoCollector.getEndNumerator() + "/"
                + InfoCollector.getStartDenominator() + "x" + InfoCollector.getEndDenominator();
        RIGHT_ANSWER = String.valueOf(FractionA.getDenominator());

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(FractionA.getDenominator() + 2);
        ANSWER_C = String.valueOf(FractionA.getDenominator() + 1);
        ANSWER_D = String.valueOf(FractionA.getDenominator() - 1);
    }

    @Override
    public String getQuestion() {
        QUESTION = "Find the x value: <br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
