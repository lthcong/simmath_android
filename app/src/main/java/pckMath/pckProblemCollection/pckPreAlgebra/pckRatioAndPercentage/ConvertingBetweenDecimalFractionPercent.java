package pckMath.pckProblemCollection.pckPreAlgebra.pckRatioAndPercentage;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 6/4/2017.
 */

public class ConvertingBetweenDecimalFractionPercent extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public ConvertingBetweenDecimalFractionPercent(){
        createProblem();
        swapAnswer();
    }

    public ConvertingBetweenDecimalFractionPercent(String Question,
                               String RightAnswer,
                               String AnswerA,
                               String AnswerB,
                               String AnswerC,
                               String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }

        return new ConvertingBetweenDecimalFractionPercent(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  FRACTION => DECIMAL
    private void createProblem_01(){
        Fraction FRACTION = FractionMath.getRandomFraction();
        double result = FRACTION.getValue();
        result = MathUtility.round_2_Decimal(result);

        QUESTION = "Convert " + FRACTION.toString() + " to decimal.";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(result + 0.2));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(result - 0.01));
    }

    //  FRACTION => PERCENT
    private void createProblem_02(){
        Fraction FRACTION = FractionMath.getRandomFraction();
        double result = FRACTION.getValue() * 100.0;
        result = MathUtility.round_2_Decimal(result);

        QUESTION = "Convert " + FRACTION.toString() + " to percent.";
        RIGHT_ANSWER = String.valueOf(result) + "%";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(result + 10)) + "%";;
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(result + 20)) + "%";;
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(result - 10)) + "%";;
    }

    //  DECIMAL => FRACTION
    private void createProblem_03(){
        Fraction FRACTION = FractionMath.getRandomFraction();
        double result = FRACTION.getValue();
        result = MathUtility.round_3_Decimal(result);

        QUESTION = "Convert " + String.valueOf(result) + " to fraction.";
        RIGHT_ANSWER = FRACTION.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getReciprocal(FRACTION).toString();
        ANSWER_C = (new Fraction(FRACTION.getNumerator() + 5, FRACTION.getDenominator())).toString();
        ANSWER_D = (new Fraction(FRACTION.getNumerator(), FRACTION.getDenominator() + 5)).toString();
    }

    //  DECIMAL => PERCENT
    private void createProblem_04(){
        double a = MathUtility.getRandomDecimalNumber();
        double result = MathUtility.round_2_Decimal(a * 100);

        QUESTION = "Convert " + String.valueOf(a) + " into percent.";
        RIGHT_ANSWER = String.valueOf(result) + "%";;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(result + 10.0)) + "%";;
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(result + 20.0)) + "%";;
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(result - 10.0)) + "%";;
    }

    //  PERCENT => FRACTION
    private void createProblem_05(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        Fraction result = FractionMath.getSimplifiedFraction(new Fraction(a, 100));

        QUESTION = "Convert " + String.valueOf(a) + "% into fraction.";
        RIGHT_ANSWER = result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getSimplifiedFraction(new Fraction(a + 10, 100)).toString();
        ANSWER_C = FractionMath.getSimplifiedFraction(new Fraction(a + 20, 100)).toString();
        ANSWER_D = FractionMath.getSimplifiedFraction(new Fraction(a - 10, 100)).toString();
    }

    //  PERCENT => DECIMAL
    private void createProblem_06(){
        double a = MathUtility.round_2_Decimal(MathUtility.getRandomDecimalNumber() * 100.0);
        double result = MathUtility.round_4_Decimal(a / 100.0);

        QUESTION = "Convert " + String.valueOf(a) + "% into decimal.";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_4_Decimal(result + 0.01));
        ANSWER_C = String.valueOf(MathUtility.round_4_Decimal(result + 0.02));
        ANSWER_D = String.valueOf(MathUtility.round_4_Decimal(result - 0.01));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
