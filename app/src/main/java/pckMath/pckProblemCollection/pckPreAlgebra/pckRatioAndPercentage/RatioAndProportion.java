package pckMath.pckProblemCollection.pckPreAlgebra.pckRatioAndPercentage;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 6/3/2017.
 */

public class RatioAndProportion extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public RatioAndProportion(){
        createProblem();
        swapAnswer();
    }

    public RatioAndProportion(String Question,
                                 String RightAnswer,
                                 String AnswerA,
                                 String AnswerB,
                                 String AnswerC,
                                 String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 5;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            default:
                createProblem_01();
                break;
        }

        return new RatioAndProportion(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  RATIO
    private void createProblem_01(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();

        Fraction Result = new Fraction(a, b);
        Result = FractionMath.getSimplifiedFraction(Result);

        QUESTION = "Find the ratio between " + String.valueOf(a) + " and " + String.valueOf(b) + ".";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Fraction(a, b)).toString();
        ANSWER_C = (new Fraction(b, a)).toString();
        ANSWER_D = (new Fraction(a + 1, b + 1)).toString();
    }

    //  RATIO
    private void createProblem_02(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();

        Fraction Result = new Fraction(a, b);
        Result = FractionMath.getSimplifiedFraction(Result);

        QUESTION = Result.toString() + " is the ratio between " + String.valueOf(a) + " and what number?";
        RIGHT_ANSWER = String.valueOf(b);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(b + 1);
        ANSWER_C = String.valueOf(b * -1);
        ANSWER_D = String.valueOf(b - 1);
    }

    //  RATIO
    private void createProblem_03(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();

        Fraction Result = new Fraction(a, b);
        Result = FractionMath.getSimplifiedFraction(Result);

        QUESTION = Result.toString() + " is the ratio between what number and " + String.valueOf(b) + ".";
        RIGHT_ANSWER = String.valueOf(a);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a + 1);
        ANSWER_C = String.valueOf(a * -1);
        ANSWER_D = String.valueOf(a - 1);
    }

    //  PROPORTION
    private void createProblem_04(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        Fraction FRACTION = FractionMath.getRandomProperFraction();

        Fraction Result;
        if ((a * FRACTION.getNumerator()) % FRACTION.getDenominator() == 0){
            Result = new Fraction((a * FRACTION.getNumerator()) / FRACTION.getDenominator());

            ANSWER_B = String.valueOf((int) new Fraction((a * FRACTION.getNumerator()) / FRACTION.getDenominator()).getValue() + 1);
            ANSWER_C = String.valueOf((int) new Fraction((a * FRACTION.getNumerator()) / FRACTION.getDenominator()).getValue() * -1);
            ANSWER_D = String.valueOf((int) new Fraction((a * FRACTION.getNumerator()) / FRACTION.getDenominator()).getValue() - 1);
        }
        else {
            Result = FractionMath.getSimplifiedFraction(new Fraction((a * FRACTION.getNumerator()), FRACTION.getDenominator()));

            ANSWER_B = FractionMath.getSimplifiedFraction(new Fraction((a * FRACTION.getNumerator()) + 1, FRACTION.getDenominator())).toString();
            ANSWER_C = FractionMath.getSimplifiedFraction(new Fraction((a * FRACTION.getNumerator()), FRACTION.getDenominator() + 1)).toString();
            ANSWER_D = FractionMath.getSimplifiedFraction(new Fraction((a * FRACTION.getNumerator()) + 1, FRACTION.getDenominator() + 1)).toString();
        }

        QUESTION = "What portion of " + String.valueOf(a) + " is " + FRACTION.toString() + "?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
    }

    //  PROPORTION
    private void createProblem_05(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();

        Fraction Result = FractionMath.getSimplifiedFraction(new Fraction(a, b));

        QUESTION = String.valueOf(a) + " is what portion of " + String.valueOf(b) + "?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getSimplifiedFraction(new Fraction(b, a)).toString();
        ANSWER_C = FractionMath.getSimplifiedFraction(new Fraction(a + 1, b)).toString();
        ANSWER_D = FractionMath.getSimplifiedFraction(new Fraction(a + 1, b + 1)).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
