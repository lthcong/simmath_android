package pckMath.pckProblemCollection.pckPreAlgebra.pckRatioAndPercentage;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/3/2017.
 */

public class UnitRate extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;
    private String EITHER = "Either.", NEITHER = "Neither.";

    public UnitRate(){
        createProblem();
        swapAnswer();
    }

    public UnitRate(String Question,
                               String RightAnswer,
                               String AnswerA,
                               String AnswerB,
                               String AnswerC,
                               String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 24;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            case 14:
                createProblem_15();
                break;
            case 15:
                createProblem_16();
                break;
            case 16:
                createProblem_17();
                break;
            case 17:
                createProblem_18();
                break;
            case 18:
                createProblem_19();
                break;
            case 19:
                createProblem_20();
                break;
            case 20:
                createProblem_21();
                break;
            case 21:
                createProblem_22();
                break;
            case 22:
                createProblem_23();
                break;
            case 23:
                createProblem_24();
                break;
            default:
                createProblem_01();
                break;
        }

        return new UnitRate(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_3Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double result = (double) a / (double) b;
        result = MathUtility.round_2_Decimal(result);

        QUESTION = "Jame can type " + String.valueOf(a) + " words in " + String.valueOf(b) + " minutes. "
                + "How many words can he type in 1 minute?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(result + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(result - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(result + 2.0));
    }

    private void createProblem_02(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        a = a * b;

        int result = a / b;

        QUESTION = "There are " + String.valueOf(a) + " people on " + String.valueOf(b) + " buses. "
                + "How many people are there in 1 bus?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 1);
        ANSWER_D = String.valueOf(result + 2);
    }

    private void createProblem_03(){
        int a = MathUtility.getRandomPositiveNumber_2Digit() + 100;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double result = MathUtility.round_2_Decimal((double) a / (double) b);

        QUESTION = "A car can go " + String.valueOf(a) + " miles in " + String.valueOf(b) + " hours. "
                + "How far it can go in 1 hour?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(result + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(result - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(result + 2.0));
    }

    private void createProblem_04(){
        int a = MathUtility.getRandomPositiveNumber_2Digit() + 100;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double result = MathUtility.round_2_Decimal((double) a / (double) b);

        QUESTION = "Tom earns $" + String.valueOf(a) + " in " + String.valueOf(b) + " hours. "
                + "How much can he earn in an hour?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(result + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(result - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(result + 2.0));
    }

    private void createProblem_05(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        b = a * b;

        int result = b / a;

        QUESTION = String.valueOf(a) + " apples cost $" + String.valueOf(b) + ". How much does 1 apple cost?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 1);
        ANSWER_D = String.valueOf(result + 2);
    }

    private void createProblem_06(){
        int a = MathUtility.getRandomPositiveNumber_2Digit() + 100;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double result = MathUtility.round_2_Decimal((double) a / (double) b);

        QUESTION = String.valueOf(a) + " pounds of fruit cost $" + String.valueOf(b) + ". How much does 1 pound cost?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(result + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(result - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(result + 2.0));
    }

    private void createProblem_07(){
        int a = MathUtility.getRandomPositiveNumber_2Digit() + 100;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double result = MathUtility.round_2_Decimal((double) a / (double) b);

        QUESTION = String.valueOf(b) + " T-shirts cost $" + String.valueOf(a) + ". How much does 1 T-shirt cost?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(result + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(result - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(result + 2.0));
    }

    private void createProblem_08(){
        int a = MathUtility.getRandomPositiveNumber_2Digit() + 200;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double result = MathUtility.round_2_Decimal((double) a / (double) b);

        QUESTION = String.valueOf(b) + " jackets cost $" + String.valueOf(a) + ". How much does 1 jacket cost?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(result + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(result - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(result + 2.0));
    }

    private void createProblem_09(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();

        b = a * b;

        int result = b / a;

        QUESTION = "A hall has " + String.valueOf(a) + " chairs, and " + String.valueOf(b) + " rows. "
                + "How many chairs are there in a row?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result - 1);
        ANSWER_D = String.valueOf(result + 2);
    }

    private void createProblem_10(){
        int UnitA = MathUtility.getRandomPositiveNumber_1Digit();
        int PriceA = MathUtility.getRandomPositiveNumber_2Digit();

        double RateA = MathUtility.round_2_Decimal((double) PriceA / (double) UnitA);

        int UnitB = MathUtility.getRandomPositiveNumber_1Digit();
        int PriceB = MathUtility.getRandomPositiveNumber_2Digit();

        double RateB = MathUtility.round_2_Decimal((double) PriceB / (double) UnitB);

        QUESTION = "Store A sells " + String.valueOf(UnitA) + " apples for $" + String.valueOf(PriceA)
                + ". Store B sells " + String.valueOf(UnitB) + " apples for $" + String.valueOf(PriceB)
                + ". Which store has the better price?";
        if (RateA > RateB){
            RIGHT_ANSWER = String.valueOf(RateB);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(RateA);
            ANSWER_C = EITHER;
            ANSWER_D = NEITHER;
        }
        else {
            if (RateA < RateB) {
                RIGHT_ANSWER = String.valueOf(RateA);

                ANSWER_A = RIGHT_ANSWER;
                ANSWER_B = String.valueOf(RateB);
                ANSWER_C = EITHER;
                ANSWER_D = NEITHER;
            }
            else {
                RIGHT_ANSWER = NEITHER;

                ANSWER_A = RIGHT_ANSWER;
                ANSWER_B = String.valueOf(RateB);
                ANSWER_C = EITHER;
                ANSWER_D = String.valueOf(RateA);
            }
        }
    }

    private void createProblem_11(){
        int UnitA = MathUtility.getRandomPositiveNumber_1Digit();
        int PriceA = MathUtility.getRandomPositiveNumber_2Digit();

        double RateA = MathUtility.round_2_Decimal((double) PriceA / (double) UnitA);

        int UnitB = MathUtility.getRandomPositiveNumber_1Digit();
        int PriceB = MathUtility.getRandomPositiveNumber_2Digit();

        double RateB = MathUtility.round_2_Decimal((double) PriceB / (double) UnitB);

        QUESTION = "Store A sells " + String.valueOf(UnitA) + " pounds of rice for $" + String.valueOf(PriceA)
                + ". Store B sells " + String.valueOf(UnitB) + " pounds of rice for $" + String.valueOf(PriceB)
                + ". Which store has the better price?";
        if (RateA > RateB){
            RIGHT_ANSWER = String.valueOf(RateB);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(RateA);
            ANSWER_C = EITHER;
            ANSWER_D = NEITHER;
        }
        else {
            if (RateA < RateB) {
                RIGHT_ANSWER = String.valueOf(RateA);

                ANSWER_A = RIGHT_ANSWER;
                ANSWER_B = String.valueOf(RateB);
                ANSWER_C = EITHER;
                ANSWER_D = NEITHER;
            }
            else {
                RIGHT_ANSWER = NEITHER;

                ANSWER_A = RIGHT_ANSWER;
                ANSWER_B = String.valueOf(RateB);
                ANSWER_C = EITHER;
                ANSWER_D = String.valueOf(RateA);
            }
        }
    }

    private void createProblem_12(){
        int UnitA = MathUtility.getRandomPositiveNumber_1Digit();
        int PriceA = MathUtility.getRandomPositiveNumber_2Digit();

        double RateA = MathUtility.round_2_Decimal((double) PriceA / (double) UnitA);

        int UnitB = MathUtility.getRandomPositiveNumber_1Digit();
        int PriceB = MathUtility.getRandomPositiveNumber_2Digit();

        double RateB = MathUtility.round_2_Decimal((double) PriceB / (double) UnitB);

        QUESTION = "Store A sells " + String.valueOf(UnitA) + " pounds of beef for $" + String.valueOf(PriceA)
                + ". Store B sells " + String.valueOf(UnitB) + " pounds of beef for $" + String.valueOf(PriceB)
                + ". Which store has the better price?";
        if (RateA > RateB){
            RIGHT_ANSWER = String.valueOf(RateB);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(RateA);
            ANSWER_C = EITHER;
            ANSWER_D = NEITHER;
        }
        else {
            if (RateA < RateB) {
                RIGHT_ANSWER = String.valueOf(RateA);

                ANSWER_A = RIGHT_ANSWER;
                ANSWER_B = String.valueOf(RateB);
                ANSWER_C = EITHER;
                ANSWER_D = NEITHER;
            }
            else {
                RIGHT_ANSWER = NEITHER;

                ANSWER_A = RIGHT_ANSWER;
                ANSWER_B = String.valueOf(RateB);
                ANSWER_C = EITHER;
                ANSWER_D = String.valueOf(RateA);
            }
        }
    }

    private void createProblem_13(){
        int UnitA = MathUtility.getRandomPositiveNumber_1Digit();
        int PriceA = MathUtility.getRandomPositiveNumber_2Digit();

        double RateA = MathUtility.round_2_Decimal((double) PriceA / (double) UnitA);

        int UnitB = MathUtility.getRandomPositiveNumber_1Digit();
        int PriceB = MathUtility.getRandomPositiveNumber_2Digit();

        double RateB = MathUtility.round_2_Decimal((double) PriceB / (double) UnitB);

        QUESTION = "Store A sells " + String.valueOf(UnitA) + " pounds of pork for $" + String.valueOf(PriceA)
                + ". Store B sells " + String.valueOf(UnitB) + " pounds of pork for $" + String.valueOf(PriceB)
                + ". Which store has the better price?";
        if (RateA > RateB){
            RIGHT_ANSWER = String.valueOf(RateB);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(RateA);
            ANSWER_C = EITHER;
            ANSWER_D = NEITHER;
        }
        else {
            if (RateA < RateB) {
                RIGHT_ANSWER = String.valueOf(RateA);

                ANSWER_A = RIGHT_ANSWER;
                ANSWER_B = String.valueOf(RateB);
                ANSWER_C = EITHER;
                ANSWER_D = NEITHER;
            }
            else {
                RIGHT_ANSWER = NEITHER;

                ANSWER_A = RIGHT_ANSWER;
                ANSWER_B = String.valueOf(RateB);
                ANSWER_C = EITHER;
                ANSWER_D = String.valueOf(RateA);
            }
        }
    }

    private void createProblem_14(){
        int UnitA = MathUtility.getRandomPositiveNumber_1Digit();
        int PriceA = MathUtility.getRandomPositiveNumber_2Digit();

        double RateA = MathUtility.round_2_Decimal((double) PriceA / (double) UnitA);

        int UnitB = MathUtility.getRandomPositiveNumber_1Digit();
        int PriceB = MathUtility.getRandomPositiveNumber_2Digit();

        double RateB = MathUtility.round_2_Decimal((double) PriceB / (double) UnitB);

        QUESTION = "Store A sells " + String.valueOf(UnitA) + " gallons of gas for $" + String.valueOf(PriceA)
                + ". Store B sells " + String.valueOf(UnitB) + " gallons of gas for $" + String.valueOf(PriceB)
                + ". Which store has the better price?";
        if (RateA > RateB){
            RIGHT_ANSWER = String.valueOf(RateB);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(RateA);
            ANSWER_C = EITHER;
            ANSWER_D = NEITHER;
        }
        else {
            if (RateA < RateB) {
                RIGHT_ANSWER = String.valueOf(RateA);

                ANSWER_A = RIGHT_ANSWER;
                ANSWER_B = String.valueOf(RateB);
                ANSWER_C = EITHER;
                ANSWER_D = NEITHER;
            }
            else {
                RIGHT_ANSWER = NEITHER;

                ANSWER_A = RIGHT_ANSWER;
                ANSWER_B = String.valueOf(RateB);
                ANSWER_C = EITHER;
                ANSWER_D = String.valueOf(RateA);
            }
        }
    }

    private void createProblem_15(){
        int UnitA = MathUtility.getRandomPositiveNumber_1Digit();
        int PriceA = MathUtility.getRandomPositiveNumber_2Digit();

        double RateA = MathUtility.round_2_Decimal((double) PriceA / (double) UnitA);

        int UnitB = MathUtility.getRandomPositiveNumber_1Digit();
        int PriceB = MathUtility.getRandomPositiveNumber_2Digit();

        double RateB = MathUtility.round_2_Decimal((double) PriceB / (double) UnitB);

        QUESTION = "Store A sells " + String.valueOf(UnitA) + " ounces of pain for $" + String.valueOf(PriceA)
                + ". Store B sells " + String.valueOf(UnitB) + " ounces of pain for $" + String.valueOf(PriceB)
                + ". Which store has the better price?";
        if (RateA > RateB){
            RIGHT_ANSWER = String.valueOf(RateB);

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(RateA);
            ANSWER_C = EITHER;
            ANSWER_D = NEITHER;
        }
        else {
            if (RateA < RateB) {
                RIGHT_ANSWER = String.valueOf(RateA);

                ANSWER_A = RIGHT_ANSWER;
                ANSWER_B = String.valueOf(RateB);
                ANSWER_C = EITHER;
                ANSWER_D = NEITHER;
            }
            else {
                RIGHT_ANSWER = NEITHER;

                ANSWER_A = RIGHT_ANSWER;
                ANSWER_B = String.valueOf(RateB);
                ANSWER_C = EITHER;
                ANSWER_D = String.valueOf(RateA);
            }
        }
    }

    private void createProblem_16(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int b = MathUtility.getRandomPositiveNumber_2Digit() + 100;

        int result = a * b;

        QUESTION = "Terry can type " + String.valueOf(b) + " words per minute. How many word can he type in " + String.valueOf(a) + "?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_17(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int result = a * b;

        QUESTION = "Tom earns $" + String.valueOf(b) + " per hour. How much can he earn in " + String.valueOf(a) + " hours?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_18(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        int result = a * b;

        QUESTION = "Tom wants to buy " + String.valueOf(b) + " jeans. Each of them costs $" + String.valueOf(a)
                + ". How much money does he spend?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_19(){
        int a = MathUtility.getRandomPositiveNumber_3Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        int result = a * b;

        QUESTION = "Tom's dad has " + String.valueOf(a) + " chickens. Each of them give " + String.valueOf(b) + " eggs per day. "
                + "How many eggs does he have per day?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(result + 1);
        ANSWER_C = String.valueOf(result + 2);
        ANSWER_D = String.valueOf(result - 1);
    }

    private void createProblem_20(){
        int UnitA = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int PriceA = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int UnitB = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        double PriceB = MathUtility.round_2_Decimal(((double) PriceA / (double) UnitA) * (double) UnitB);

        QUESTION = String.valueOf(UnitA) + " pounds of apple cost $" + String.valueOf(PriceA) + ". How much does "
                + String.valueOf(UnitB) + " pounds of apple cost?";
        RIGHT_ANSWER = String.valueOf(PriceB);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(PriceB + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(PriceB - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(PriceB + 2.0));
    }

    private void createProblem_21(){
        int UnitA = MathUtility.getRandomPositiveNumber_1Digit() + 20;
        int PriceA = MathUtility.getRandomPositiveNumber_1Digit() + 40;

        int UnitB = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        double PriceB = MathUtility.round_2_Decimal(((double) PriceA / (double) UnitA) * (double) UnitB);

        QUESTION = String.valueOf(UnitA) + " pounds of rice cost $" + String.valueOf(PriceA) + ". How much does "
                + String.valueOf(UnitB) + " pounds of rice cost?";
        RIGHT_ANSWER = String.valueOf(PriceB);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(PriceB + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(PriceB - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(PriceB + 2.0));
    }

    private void createProblem_22(){
        int EarnA = MathUtility.getRandomPositiveNumber_1Digit() + 20;
        int HourA = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int HourB = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        double EarnB = MathUtility.round_2_Decimal((double) EarnA / (double) HourA * (double) HourB);

        QUESTION = "Tom earns $" + String.valueOf(EarnA) + " in " + String.valueOf(HourA) + " hours. "
                + "How much can he earn in " + String.valueOf(HourB) + " hours?";
        RIGHT_ANSWER = String.valueOf(EarnB);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(EarnB + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(EarnB - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(EarnB + 2.0));
    }

    private void createProblem_23(){
        int DistantA = MathUtility.getRandomPositiveNumber_2Digit() + 100;
        int HourA = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        int HourB = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        double DistantB = MathUtility.round_2_Decimal(((double) DistantA / (double) HourA * (double) HourB));

        QUESTION = "Jack's car can go " + String.valueOf(DistantA) + " miles in " + String.valueOf(HourA) + " hours. "
                + "How far it can go in " + String.valueOf(HourB) + " hours?";
        RIGHT_ANSWER = String.valueOf(DistantB);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(DistantB + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(DistantB - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(DistantB + 20.0));
    }

    private void createProblem_24(){
        int UnitA = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int PriceA = MathUtility.getRandomPositiveNumber_2Digit() + 100;

        int UnitB = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        double PriceB = MathUtility.round_2_Decimal(((double) PriceA / (double) UnitA) * (double) UnitB);

        QUESTION = String.valueOf(UnitA) + " pairs of jean cost $" + String.valueOf(PriceA) + ". How much does "
                + String.valueOf(UnitB) + " pairs of jean cost?";
        RIGHT_ANSWER = String.valueOf(PriceB);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(PriceB + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(PriceB - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(PriceB + 2.0));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
