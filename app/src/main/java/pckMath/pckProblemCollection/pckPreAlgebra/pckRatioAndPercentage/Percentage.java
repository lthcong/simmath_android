package pckMath.pckProblemCollection.pckPreAlgebra.pckRatioAndPercentage;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/3/2017.
 */

public class Percentage extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public Percentage(){
        createProblem();
        swapAnswer();
    }

    public Percentage(String Question,
                               String RightAnswer,
                               String AnswerA,
                               String AnswerB,
                               String AnswerC,
                               String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 5;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            default:
                createProblem_01();
                break;
        }

        return new Percentage(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int percent = MathUtility.getRandomPositiveNumber_1Digit();
        int number = MathUtility.getRandomPositiveNumber_2Digit();

        double result = (double) number / 100.0 * (double) percent;
        result = MathUtility.round_2_Decimal(result);

        QUESTION = String.valueOf(percent) + "% of " + String.valueOf(number) + " is what number?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(result + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(result - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(result + 2.0));
    }

    private void createProblem_02(){
        int percent = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int number = MathUtility.getRandomPositiveNumber_2Digit();

        double result = (double) number / 100.0 * (double) percent;
        result = MathUtility.round_2_Decimal(result);

        QUESTION = String.valueOf(percent) + "% of " + String.valueOf(number) + " is what number?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(result + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(result - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(result + 2.0));
    }

    private void createProblem_03(){
        int percent = MathUtility.getRandomPositiveNumber_2Digit() + 10;
        int number = MathUtility.getRandomPositiveNumber_3Digit();

        double result = (double) number / 100.0 * (double) percent;
        result = MathUtility.round_2_Decimal(result);

        QUESTION = String.valueOf(percent) + "% of " + String.valueOf(number) + " is what number?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(result + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(result - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(result + 2.0));
    }

    private void createProblem_04(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        double result = (double) a / (double) b * 100.0;
        result = MathUtility.round_2_Decimal(result);

        QUESTION = String.valueOf(a) + " is what percent of " + String.valueOf(b) + "?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(result - 0.1));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(result + 0.2));
    }

    private void createProblem_05(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int percent = MathUtility.getRandomPositiveNumber_1Digit() + 10;

        double result = (double) a * 100.0 / (double) percent;
        result = MathUtility.round_2_Decimal(result);

        QUESTION = String.valueOf(a) + " is " + String.valueOf(percent) + "% of what number?";
        RIGHT_ANSWER = String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(result + 0.1));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(result - 0.1));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(result + 0.2));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }


}
