package pckMath.pckProblemCollection.pckPreAlgebra.pckRatioAndPercentage;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 6/4/2017.
 */

public class IncreaseAndDecreasePercentage extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public IncreaseAndDecreasePercentage(){
        createProblem();
        swapAnswer();
    }

    public IncreaseAndDecreasePercentage(String Question,
                       String RightAnswer,
                       String AnswerA,
                       String AnswerB,
                       String AnswerC,
                       String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 10;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            default:
                createProblem_01();
                break;
        }

        return new IncreaseAndDecreasePercentage(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  INCREASE
    private void createProblem_01(){
        int OldPrice = (MathUtility.getRandomPositiveNumber_2Digit() + 100) * 1000;
        int NewPrice = OldPrice + (MathUtility.getRandomPositiveNumber_4Digit() + 10000);

        double percent = (double) (NewPrice - OldPrice) / (double) OldPrice;
        percent = percent * 100.0;
        percent = MathUtility.round_2_Decimal(percent);

        QUESTION = "The house price goes from $" + String.valueOf(OldPrice) + " to $" + String.valueOf(NewPrice)
                + ". Find the increase percent.";
        RIGHT_ANSWER = String.valueOf(percent);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(percent + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(percent - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(percent + 20.0));
    }

    //  INCREASE
    private void createProblem_02(){
        int OldPrice = MathUtility.getRandomPositiveNumber_4Digit() + 10000;
        int NewPrice = OldPrice + MathUtility.getRandomPositiveNumber_4Digit();

        double percent = (double) (NewPrice - OldPrice) / (double) OldPrice;
        percent = percent * 100.0;
        percent = MathUtility.round_2_Decimal(percent);

        QUESTION = "The car price goes from $" + String.valueOf(OldPrice) + " to $" + String.valueOf(NewPrice)
                + ". Find the increase percent.";
        RIGHT_ANSWER = String.valueOf(percent);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(percent + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(percent - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(percent + 20.0));
    }

    //  INCREASE
    private void createProblem_03(){
        int OldPrice = MathUtility.getRandomPositiveNumber_3Digit() + 1000;
        int NewPrice = OldPrice + (MathUtility.getRandomPositiveNumber_2Digit() + 100);

        double percent = (double) (NewPrice - OldPrice) / (double) OldPrice;
        percent = percent * 100.0;
        percent = MathUtility.round_2_Decimal(percent);

        QUESTION = "The airplane ticket price goes from $" + String.valueOf(OldPrice) + " to $" + String.valueOf(NewPrice)
                + ". Find the increase percent.";
        RIGHT_ANSWER = String.valueOf(percent);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(percent + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(percent - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(percent + 20.0));
    }

    //  INCREASE
    private void createProblem_04(){
        int OldScore = MathUtility.getRandomPositiveNumber_2Digit() + 100;
        int NewScore = OldScore + MathUtility.getRandomPositiveNumber_2Digit();

        double percent = (double) (NewScore - OldScore) / (double) OldScore;
        percent = percent * 100.0;
        percent = MathUtility.round_2_Decimal(percent);

        QUESTION = "Jame test score goes from " + String.valueOf(OldScore) + " to " + String.valueOf(NewScore)
                + ". Find the increase percent.";
        RIGHT_ANSWER = String.valueOf(percent);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(percent + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(percent - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(percent + 20.0));
    }

    //  INCREASE
    private void createProblem_05(){
        int OldPopulation = MathUtility.getRandomPositiveNumber_3Digit() * 1000;
        int NewPopulation = OldPopulation + MathUtility.getRandomPositiveNumber_2Digit() * 1000;

        double percent = (double) (NewPopulation - OldPopulation) / (double) OldPopulation;
        percent = percent * 100.0;
        percent = MathUtility.round_2_Decimal(percent);

        QUESTION = "The city population increases from " + String.valueOf(OldPopulation) + " to " + String.valueOf(NewPopulation)
                + ". Find the increase percent.";
        RIGHT_ANSWER = String.valueOf(percent);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(percent + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(percent - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(percent + 20.0));
    }

    //  DECREASE
    private void createProblem_06(){
        int OldPrice = (MathUtility.getRandomPositiveNumber_2Digit() + 100) * 1000;
        int NewPrice = OldPrice - (MathUtility.getRandomPositiveNumber_1Digit() * 1000);

        double percent = (double) (OldPrice - NewPrice) / (double) OldPrice;
        percent = percent * 100.0;
        percent = MathUtility.round_2_Decimal(percent);

        QUESTION = "The house price drops from $" + String.valueOf(OldPrice) + " to $" + String.valueOf(NewPrice)
                + ". Find the decrease percent.";
        RIGHT_ANSWER = String.valueOf(percent);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(percent + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(percent - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(percent + 20.0));
    }

    //  DECREASE
    private void createProblem_07(){
        int OldPrice = MathUtility.getRandomPositiveNumber_4Digit() + 10000;
        int NewPrice = OldPrice - MathUtility.getRandomPositiveNumber_4Digit();

        double percent = (double) (OldPrice - NewPrice) / (double) OldPrice;
        percent = percent * 100.0;
        percent = MathUtility.round_2_Decimal(percent);

        QUESTION = "The car price drops from $" + String.valueOf(OldPrice) + " to $" + String.valueOf(NewPrice)
                + ". Find the decrease percent.";
        RIGHT_ANSWER = String.valueOf(percent);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(percent + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(percent - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(percent + 20.0));
    }

    //  DECREASE
    private void createProblem_08(){
        int OldPrice = MathUtility.getRandomPositiveNumber_3Digit() + 1000;
        int NewPrice = OldPrice - MathUtility.getRandomPositiveNumber_3Digit();

        double percent = (double) (OldPrice - NewPrice) / (double) OldPrice;
        percent = percent * 100.0;
        percent = MathUtility.round_2_Decimal(percent);

        QUESTION = "The airplane ticket price drops from $" + String.valueOf(OldPrice) + " to $" + String.valueOf(NewPrice)
                + ". Find the decrease percent.";
        RIGHT_ANSWER = String.valueOf(percent);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(percent + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(percent - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(percent + 20.0));
    }

    //  DECREASE
    private void createProblem_09(){
        int OldScore = MathUtility.getRandomPositiveNumber_2Digit() + 110;
        int NewScore = OldScore - (MathUtility.getRandomPositiveNumber_1Digit() + 10);

        double percent = (double) (OldScore - NewScore) / (double) OldScore;
        percent = percent * 100.0;
        percent = MathUtility.round_2_Decimal(percent);

        QUESTION = "Tom's test score drops from " + String.valueOf(OldScore) + " to " + String.valueOf(NewScore)
                + ". Find the decrease percent.";
        RIGHT_ANSWER = String.valueOf(percent);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(percent + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(percent - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(percent + 20.0));
    }

    //  DECREASE
    private void createProblem_10(){
        int OldPopulation = MathUtility.getRandomPositiveNumber_2Digit() + 100;
        int NewPopulation = OldPopulation - MathUtility.getRandomPositiveNumber_1Digit() + 15;

        double percent = (double) (OldPopulation - NewPopulation) / (double) OldPopulation;
        percent = percent * 100.0;
        percent = MathUtility.round_2_Decimal(percent);

        QUESTION = "The pigeon population decrease from " + String.valueOf(OldPopulation) + " to " + String.valueOf(NewPopulation)
                + ". Find the decrease percent.";
        RIGHT_ANSWER = String.valueOf(percent);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(percent + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(percent - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(percent + 20.0));
    }
    
    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
