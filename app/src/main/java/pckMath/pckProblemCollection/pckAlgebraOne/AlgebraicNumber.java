package pckMath.pckProblemCollection.pckAlgebraOne;

import java.util.ArrayList;

import pckMath.MathUtility;

/**
 * Created by Cong on 6/26/2017.
 */

public class AlgebraicNumber {

    private int COEFFICIENT;
    private ArrayList<Variable> VARIABLE_LIST;

    public AlgebraicNumber(){
        COEFFICIENT = 0;
        VARIABLE_LIST = new ArrayList<>();
    }

    public void setCoefficient(int Coefficient){
        COEFFICIENT = Coefficient;
    }

    public int getCoefficient(){
        return COEFFICIENT;
    }

    public void addVariable(Variable _Variable){
        VARIABLE_LIST.add(_Variable);
        arrangeVariable();
        combineVariables();
    }

    public void addVariable(ArrayList<Variable> _Variable){
        VARIABLE_LIST.addAll(MathUtility.convertToArrayList(MathUtility.convertToArray(_Variable)));
        arrangeVariable();
        combineVariables();
    }

    private void combineVariables(){
        int size = VARIABLE_LIST.size();
        for (int i = 0; i < VARIABLE_LIST.size() - 1; i++){
            for (int j = i + 1; j < VARIABLE_LIST.size(); j++){
                Variable Temp_1 = VARIABLE_LIST.get(i);
                Variable Temp_2 = VARIABLE_LIST.get(j);
                if (Temp_1.isTheSameVariable(Temp_2)){
                    Variable Temp = new Variable(Temp_1.getName(), Temp_1.getExponent() + Temp_2.getExponent());
                    VARIABLE_LIST.set(i, Temp);
                    VARIABLE_LIST.remove(j);
                }
            }
        }
    }

    public void setVariable(int Index, Variable _Variable){
        VARIABLE_LIST.set(Index, _Variable);
    }

    public void arrangeVariable(){
        int size = VARIABLE_LIST.size();
        for (int i = 0; i < size; i++){
            for (int j = size - 1; j > i; j--){
                if (VARIABLE_LIST.get(j).getCharName() < VARIABLE_LIST.get(j - 1).getCharName()){
                    Variable Temp_1 = VARIABLE_LIST.get(j - 1);
                    Variable Temp_2 = VARIABLE_LIST.get(j);

                    VARIABLE_LIST.set(j - 1, Temp_2);
                    VARIABLE_LIST.set(j, Temp_1);
                }
            }
        }
    }

    public int getNoVariable(){
        return VARIABLE_LIST.size();
    }

    public ArrayList<Variable> getVariableList(){
        return VARIABLE_LIST;
    }

    public Variable getVariable(int Index){
        return VARIABLE_LIST.get(Index);
    }

    public double evaluate(){
        double NumberValue = COEFFICIENT;
        int size = VARIABLE_LIST.size();
        for (int i = 0; i < size; i++){
            NumberValue = NumberValue * VARIABLE_LIST.get(i).evaluate();
        }

        return NumberValue;
    }

    public boolean isLikeTerms(AlgebraicNumber SecondNumber){
        boolean isLike = true;

        if (getNoVariable() != SecondNumber.getNoVariable()){
            isLike = false;
        }
        else {
            arrangeVariable();
            SecondNumber.arrangeVariable();
            int size = VARIABLE_LIST.size();
            for (int i = 0; i < size; i++){
                if (!VARIABLE_LIST.get(i).isLikeTerms(SecondNumber.getVariable(i))){
                    isLike = false;
                }
            }
        }

        return isLike;
    }

    public boolean isNull(){
        boolean isNullNumber = false;
        if (COEFFICIENT == 0){
            int size = VARIABLE_LIST.size();
            for (int i = 0; i < size; i++){
                if (!VARIABLE_LIST.get(i).isNull()){
                    isNullNumber = true;
                }
            }
        }

        return isNullNumber;
    }

    public String toString(){
        String NumberString = "";

        //  COEFFICIENT
        if (COEFFICIENT == 0){
            NumberString = "0";
        }
        else {
            //  COEFFICIENT
            NumberString = String.valueOf(COEFFICIENT);

            //  VARIABLE LIST
            int size = VARIABLE_LIST.size();
            for (int i = 0; i < size; i++){
                if (!VARIABLE_LIST.get(i).toString().trim().equalsIgnoreCase("1")){
                    NumberString += VARIABLE_LIST.get(i).toString();
                }
            }
        }

        return NumberString;
    }

}
