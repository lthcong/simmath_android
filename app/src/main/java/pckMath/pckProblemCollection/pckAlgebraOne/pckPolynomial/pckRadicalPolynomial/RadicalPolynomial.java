package pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRadicalPolynomial;

import pckInfo.InfoCollector;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 8/9/2017.
 */

public class RadicalPolynomial {

    //  (ax + b) * sqrt(ax + b)
    private Polynomial COEFFICIENT, RADICAL_EXPRESSION;
    private int ROOT_NUMBER;

    public RadicalPolynomial(){
        COEFFICIENT = new Polynomial();
        ROOT_NUMBER = 0;
        RADICAL_EXPRESSION = new Polynomial();
    }

    public void setCoefficient(Polynomial Coefficient){
        COEFFICIENT = Coefficient;
    }

    public Polynomial getCoefficient(){
        return COEFFICIENT;
    }

    public void setRootNumber(int RootNumber){
        ROOT_NUMBER = RootNumber;
    }

    public int getRootNumber(){
        return ROOT_NUMBER;
    }

    public void setExpression(Polynomial RadicalExpression){
        RADICAL_EXPRESSION = RadicalExpression;
    }

    public Polynomial getExpression(){
        return RADICAL_EXPRESSION;
    }

    public String toString(){
        String RadicalString = "";
        if (!COEFFICIENT.isNull()){
            RadicalString = COEFFICIENT.toString();
        }

        if (!RADICAL_EXPRESSION.isNull()) {
            switch (ROOT_NUMBER) {
                case 0:
                    RadicalString = "0";
                    break;
                case 1:
                    RadicalString += "(" + RADICAL_EXPRESSION.toString() + ")";
                    break;
                case 2:
                    RadicalString += InfoCollector.getSquareRootSign() + "(" + RADICAL_EXPRESSION.toString() + ")";
                    break;
                default:
                    RadicalString += InfoCollector.putExponent(ROOT_NUMBER) + InfoCollector.getSquareRootSign() + "(" + RADICAL_EXPRESSION.toString() + ")";
                    break;
            }
        }


        if (RadicalString.length() == 0){
            RadicalString = "0";
        }

        return RadicalString;
    }
}
