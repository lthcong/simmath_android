package pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRationalPolynomial;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/6/2017.
 */

public class AddingRationalPolynomial extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public AddingRationalPolynomial(){
        createProblem();
        swapAnswer();
    }

    public AddingRationalPolynomial(String Question,
                                      String RightAnswer,
                                      String AnswerA,
                                      String AnswerB,
                                      String AnswerC,
                                      String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 2;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            default:
                createProblem_01();
                break;
        }

        return new AddingRationalPolynomial(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        String VarName = MathUtility.getVariable();
        RationalPolynomial FirstPolynomial = AlgebraMath.getRandomRationalPolynomial(VarName);
        RationalPolynomial SecondPolynomial = AlgebraMath.getRandomRationalPolynomial(VarName);

        RationalPolynomial Result = AlgebraMath.add(FirstPolynomial, SecondPolynomial);

        QUESTION = FirstPolynomial.toString() + " + " + SecondPolynomial.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
    }

    private void createProblem_02(){
        String VarName = MathUtility.getVariable();
        Polynomial CommonDenominator = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial NumeratorOne = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial NumeratorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        RationalPolynomial FirstPolynomial = new RationalPolynomial();
        FirstPolynomial.setTopPolynomial(NumeratorOne);
        FirstPolynomial.setBottomPolynomial(CommonDenominator);

        RationalPolynomial SecondPolynomial = new RationalPolynomial();
        SecondPolynomial.setTopPolynomial(NumeratorTwo);
        SecondPolynomial.setBottomPolynomial(CommonDenominator);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(AlgebraMath.add(NumeratorOne, NumeratorTwo));
        Result.setBottomPolynomial(CommonDenominator);

        QUESTION = FirstPolynomial.toString() + " + " + SecondPolynomial.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
    }

    @Override
    public String getQuestion() {
        QUESTION = "<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
