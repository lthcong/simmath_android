package pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRadicalPolynomial;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/7/2017.
 */

public class SimplifyRadicalExpressionWithoutVariable extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SimplifyRadicalExpressionWithoutVariable(){
        createProblem();
        swapAnswer();
    }

    public SimplifyRadicalExpressionWithoutVariable(String Question,
                                    String RightAnswer,
                                    String AnswerA,
                                    String AnswerB,
                                    String AnswerC,
                                    String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 11;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SimplifyRadicalExpressionWithoutVariable(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  sqrt(a/b)
    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        Fraction _Fraction = new Fraction(a * a, b * b);

        QUESTION = InfoCollector.getSquareRootSign() + "(" + _Fraction.toString() + ") = ?";
        if (a % b == 0){
            RIGHT_ANSWER = String.valueOf((a / b));
            ANSWER_B = String.valueOf((a / b + 1));
            ANSWER_C = String.valueOf((a / b - 1));
            ANSWER_D = String.valueOf((a / b + 2));
        }
        else {
            RIGHT_ANSWER = (new Fraction(a, b)).toString();
            ANSWER_B = FractionMath.getRandomImproperFraction().toString();
            ANSWER_C = FractionMath.getRandomMixedNumber().toString();
            ANSWER_D = FractionMath.getRandomProperFraction().toString();
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  a / (sqrt(b) + c) = a * (sqrt(b) - c) / (b - c^2)
    private void createProblem_02(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int c = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        QUESTION = InfoCollector.getStartNumerator() + String.valueOf(a) + InfoCollector.getEndNumerator() + "/"
                + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") + " + String.valueOf(c) + ")";
        if (a % (b - c * c) == 0){
            RIGHT_ANSWER = String.valueOf(a / (b - c * c))
                    + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") - " + String.valueOf(c) + ")";

            ANSWER_B = String.valueOf(a / (b - c * c))
                    + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") + " + String.valueOf(c) + ")";

            ANSWER_C = String.valueOf(b - c * c)
                    + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") + " + String.valueOf(c) + ")";

            ANSWER_D = String.valueOf(a)
                    + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") + " + String.valueOf(c) + ")";
        }
        else {
            Fraction Temp = new Fraction(a, b - c * c);
            RIGHT_ANSWER = Temp.toString()
                    + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") - " + String.valueOf(c) + ")";

            ANSWER_B = Temp.toString()
                    + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") + " + String.valueOf(c) + ")";

            ANSWER_C = Temp.toString()
                    + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") * " + String.valueOf(c) + ")";

            ANSWER_D = Temp.toString()
                    + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(c) + ") - " + String.valueOf(b) + ")";
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  a / (sqrt(b) - c) = a * (sqrt(b) + c) / (b - c^2)
    private void createProblem_03(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int c = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        QUESTION = InfoCollector.getStartNumerator() + String.valueOf(a) + InfoCollector.getEndNumerator() + "/"
                + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") - " + String.valueOf(c) + ")";
        if (a % (b - c * c) == 0){
            RIGHT_ANSWER = String.valueOf(a / (b - c * c))
                    + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") + " + String.valueOf(c) + ")";

            ANSWER_B = String.valueOf(a / (b - c * c))
                    + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") - " + String.valueOf(c) + ")";

            ANSWER_C = String.valueOf(b - c * c)
                    + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") + " + String.valueOf(c) + ")";

            ANSWER_D = String.valueOf(a)
                    + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") + " + String.valueOf(c) + ")";
        }
        else {
            Fraction Temp = new Fraction(a, b - c * c);
            RIGHT_ANSWER = Temp.toString()
                    + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") + " + String.valueOf(c) + ")";

            ANSWER_B = Temp.toString()
                    + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") - " + String.valueOf(c) + ")";

            ANSWER_C = Temp.toString()
                    + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") * " + String.valueOf(c) + ")";

            ANSWER_D = Temp.toString()
                    + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(c) + ") - " + String.valueOf(b) + ")";
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  (sqrt(a) + b)^2 = a + b^2 + 2 * b * sqrt(a)
    private void createProblem_04(){
        int a = MathUtility.getRandomPrimeNumber();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ") + " + String.valueOf(b) + ")"
                + InfoCollector.getExponentSign("2") + " = ?";
        RIGHT_ANSWER = String.valueOf((a + b * b)) + " + " + (2 * b) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ")";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((a + b * b + 1)) + " + " + (2 * b - 2) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ")";
        ANSWER_C = String.valueOf((a + b - 1)) + " + " + (2 * b + 2) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ")";
        ANSWER_D = String.valueOf((a + b * b + 2)) + " + " + (2 * b - 4) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ")";
    }

    //  a + b^2 + 2 * b * sqrt(a) = (sqrt(a) + b)^2
    private void createProblem_05(){
        int a = MathUtility.getRandomPrimeNumber();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = String.valueOf((a + b * b)) + " + " + (2 * b) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ") = ?";
        RIGHT_ANSWER = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ") + " + String.valueOf(b) + ")" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ") + " + String.valueOf(b + 1) + ")" + InfoCollector.getExponentSign("2");
        ANSWER_C = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ") + " + String.valueOf(b - 1) + ")" + InfoCollector.getExponentSign("2");
        ANSWER_D = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ") + " + String.valueOf(b + 2) + ")" + InfoCollector.getExponentSign("2");
    }

    //  (sqrt(a) - b)^2 = a + b - 2 * b * sqrt(a)
    private void createProblem_06(){
        int a = MathUtility.getRandomPrimeNumber();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ") - " + String.valueOf(b) + ")"
                + InfoCollector.getExponentSign("2") + " = ?";
        RIGHT_ANSWER = String.valueOf((a + b)) + " - " + (2 * b) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ")";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((a + b + 1)) + " - " + (2 * b - 2) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ")";
        ANSWER_C = String.valueOf((a + b - 1)) + " - " + (2 * b + 2) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ")";
        ANSWER_D = String.valueOf((a + b + 2)) + " + " + (2 * b - 4) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ")";
    }

    //  a + b - 2 * b * sqrt(a) = (sqrt(a) - b)^2
    private void createProblem_07(){
        int a = MathUtility.getRandomPrimeNumber();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = String.valueOf((a + b)) + " - " + (2 * b) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ") = ?";
        RIGHT_ANSWER = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ") - " + String.valueOf(b) + ")" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ") - " + String.valueOf(b + 1) + ")" + InfoCollector.getExponentSign("2");
        ANSWER_C = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ") - " + String.valueOf(b - 1) + ")" + InfoCollector.getExponentSign("2");
        ANSWER_D = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ") + " + String.valueOf(b + 2) + ")" + InfoCollector.getExponentSign("2");
    }

    //  (sqrt(a^2 + 1) + a) / (sqrt(a^2 + 1) - (a))
    private void createProblem_08(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = a * a + 1;

        QUESTION = InfoCollector.getStartNumerator()
                + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") + " + String.valueOf(a) + ")"
                + InfoCollector.getEndNumerator()
                + InfoCollector.getStartDenominator() +
                "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") - " + String.valueOf(a) + ")"
                + InfoCollector.getEndDenominator();
        RIGHT_ANSWER  = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") + " + String.valueOf(a) + ")" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") - " + String.valueOf(a) + ")" + InfoCollector.getExponentSign("2");
        ANSWER_C = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ") - " + String.valueOf(b) + ")" + InfoCollector.getExponentSign("2");
        ANSWER_D = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ") + " + String.valueOf(b) + ")" + InfoCollector.getExponentSign("2");
    }

    //  (sqrt(a^2 + 1) + a) / (sqrt(a^2 + 1) - (a))
    private void createProblem_09(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = a * a + 1;

        QUESTION = InfoCollector.getStartNumerator()
                + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") + " + String.valueOf(a) + ")"
                + InfoCollector.getEndNumerator()
                + InfoCollector.getStartDenominator() +
                "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") - " + String.valueOf(a) + ")"
                + InfoCollector.getEndDenominator();
        RIGHT_ANSWER  = String.valueOf((a * a + b)) + " + " + (2 * a) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ")";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((a * a + b)) + " - " + (2 * a) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ")";
        ANSWER_C = String.valueOf((a * a)) + " - " + (2 * a) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ")";
        ANSWER_D = String.valueOf((a + b)) + " + " + (2 * a) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ")";
    }

    //  (sqrt(a^2 + 1) - a) / (sqrt(a^2 + 1) + (a))
    private void createProblem_10(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = a * a + 1;

        QUESTION = InfoCollector.getStartNumerator()
                + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") - " + String.valueOf(a) + ")"
                + InfoCollector.getEndNumerator()
                + InfoCollector.getStartDenominator() +
                "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") + " + String.valueOf(a) + ")"
                + InfoCollector.getEndDenominator();
        RIGHT_ANSWER  = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") - " + String.valueOf(a) + ")" + InfoCollector.getExponentSign("2");

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") - " + String.valueOf(a) + ")" + InfoCollector.getExponentSign("2");
        ANSWER_C = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ") - " + String.valueOf(b) + ")" + InfoCollector.getExponentSign("2");
        ANSWER_D = "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(a) + ") + " + String.valueOf(b) + ")" + InfoCollector.getExponentSign("2");
    }

    //  (sqrt(a^2 + 1) - a) / (sqrt(a^2 + 1) + (a))
    private void createProblem_11(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = a * a + 1;

        QUESTION = InfoCollector.getStartNumerator()
                + "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") - " + String.valueOf(a) + ")"
                + InfoCollector.getEndNumerator()
                + InfoCollector.getStartDenominator() +
                "(" + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ") + " + String.valueOf(a) + ")"
                + InfoCollector.getEndDenominator();
        RIGHT_ANSWER  = String.valueOf((a * a + b)) + " - " + (2 * a) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ")";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((a * a + b)) + " - " + (2 * a) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ")";
        ANSWER_C = String.valueOf((a * a)) + " - " + (2 * a) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ")";
        ANSWER_D = String.valueOf((a + b)) + " + " + (2 * a) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(b) + ")";
    }

    @Override
    public String getQuestion() {
        QUESTION = "<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
