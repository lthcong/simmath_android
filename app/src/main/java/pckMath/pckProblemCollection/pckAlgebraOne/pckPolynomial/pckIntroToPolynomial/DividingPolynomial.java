package pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckIntroToPolynomial;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/4/2017.
 */

public class DividingPolynomial extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public DividingPolynomial(){
        createProblem();
        swapAnswer();
    }

    public DividingPolynomial(String Question,
                                 String RightAnswer,
                                 String AnswerA,
                                 String AnswerB,
                                 String AnswerC,
                                 String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 7;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            default:
                createProblem_01();
                break;
        }

        return new DividingPolynomial(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  NO REMAINDER
    private void createProblem_01(){
        String VarName = MathUtility.getVariable();
        Polynomial Dividend, Divisor, Quotient;

        while (true){
            Quotient = AlgebraMath.getRandomPolynomial(VarName, 2);
            Divisor = AlgebraMath.getRandomPolynomial(VarName, 1);

            if (!Quotient.toString().equalsIgnoreCase("0")
                    && !Divisor.toString().equalsIgnoreCase("0")){
                break;
            }
        }

        Dividend = AlgebraMath.mul(Quotient, Divisor);

        QUESTION = "(" + Dividend.toString() + ")" + InfoCollector.getDivisionSign() + "(" + Divisor.toString() + ") = ?";
        RIGHT_ANSWER = Quotient.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = Divisor.toString();
        ANSWER_C = AlgebraMath.getRandomPolynomial(VarName).toString();
        ANSWER_D = AlgebraMath.getRandomPolynomial(VarName).toString();
    }

    //  NO REMAINDER
    private void createProblem_02(){
        String VarName = MathUtility.getVariable();
        Polynomial Dividend, Divisor, Quotient;

        while (true){
            Quotient = AlgebraMath.getRandomPolynomial(VarName, 3);
            Divisor = AlgebraMath.getRandomPolynomial(VarName, 1);

            if (!Quotient.toString().equalsIgnoreCase("0")
                    && !Divisor.toString().equalsIgnoreCase("0")){
                break;
            }
        }

        Dividend = AlgebraMath.mul(Quotient, Divisor);

        QUESTION = "(" + Dividend.toString() + ")" + InfoCollector.getDivisionSign() + "(" + Divisor.toString() + ") = ?";
        RIGHT_ANSWER = Quotient.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = Divisor.toString();
        ANSWER_C = AlgebraMath.getRandomPolynomial(VarName).toString();
        ANSWER_D = AlgebraMath.getRandomPolynomial(VarName).toString();
    }

    //  NO REMAINDER
    private void createProblem_03(){
        String VarName = MathUtility.getVariable();
        Polynomial Dividend, Divisor, Quotient;

        while (true){
            Quotient = AlgebraMath.getRandomPolynomial(VarName, 2);
            Divisor = AlgebraMath.getRandomPolynomial(VarName, 2);

            if (!Quotient.toString().equalsIgnoreCase("0")
                    && !Divisor.toString().equalsIgnoreCase("0")){
                break;
            }
        }

        Dividend = AlgebraMath.mul(Quotient, Divisor);

        QUESTION = "(" + Dividend.toString() + ")" + InfoCollector.getDivisionSign() + "(" + Divisor.toString() + ") = ?";
        RIGHT_ANSWER = Quotient.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = Divisor.toString();
        ANSWER_C = AlgebraMath.getRandomPolynomial(VarName).toString();
        ANSWER_D = AlgebraMath.getRandomPolynomial(VarName).toString();
    }

    //  NO REMAINDER
    private void createProblem_04(){
        String VarName = MathUtility.getVariable();
        Polynomial Dividend, Divisor, Quotient;

        while (true){
            Quotient = AlgebraMath.getRandomPolynomial(VarName, 3);
            Divisor = AlgebraMath.getRandomPolynomial(VarName, 2);

            if (!Quotient.toString().equalsIgnoreCase("0")
                    && !Divisor.toString().equalsIgnoreCase("0")){
                break;
            }
        }

        Dividend = AlgebraMath.mul(Quotient, Divisor);

        QUESTION = "(" + Dividend.toString() + ")" + InfoCollector.getDivisionSign() + "(" + Divisor.toString() + ") = ?";
        RIGHT_ANSWER = Quotient.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = Divisor.toString();
        ANSWER_C = AlgebraMath.getRandomPolynomial(VarName).toString();
        ANSWER_D = AlgebraMath.getRandomPolynomial(VarName).toString();
    }

    //  NO REMAINDER
    private void createProblem_05(){
        String VarName = MathUtility.getVariable();
        Polynomial Dividend, Divisor, Quotient;

        while (true){
            Quotient = AlgebraMath.getRandomPolynomial(VarName);
            Divisor = AlgebraMath.getRandomPolynomial(VarName);

            if (!Quotient.toString().equalsIgnoreCase("0")
                    && !Divisor.toString().equalsIgnoreCase("0")){
                break;
            }
        }

        Dividend = AlgebraMath.mul(Quotient, Divisor);

        QUESTION = "(" + Dividend.toString() + ")" + InfoCollector.getDivisionSign() + "(" + Divisor.toString() + ") = ?";
        RIGHT_ANSWER = Quotient.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = Divisor.toString();
        ANSWER_C = AlgebraMath.getRandomPolynomial(VarName).toString();
        ANSWER_D = AlgebraMath.getRandomPolynomial(VarName).toString();
    }

    //  WITH REMAINDER
    private void createProblem_06(){
        String VarName = MathUtility.getVariable();
        int HighestExponent = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        Polynomial Dividend, Divisor, Quotient, Remainder;

        while (true){
            Quotient = AlgebraMath.getRandomPolynomial(VarName, 1);
            Divisor = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, HighestExponent);
            Remainder = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, HighestExponent - 1);


            if (!Quotient.toString().equalsIgnoreCase("0")
                    && !Divisor.toString().equalsIgnoreCase("0")
                    && !Remainder.toString().equalsIgnoreCase("0")){
                break;
            }
        }

        Dividend = AlgebraMath.mul(Quotient, Divisor);
        Dividend = AlgebraMath.add(Dividend, Remainder);

        QUESTION = "(" + Dividend.toString() + ")" + InfoCollector.getDivisionSign() + "(" + Divisor.toString() + ") = ?";
        RIGHT_ANSWER = "(" +  Quotient.toString() + ") R (" + Remainder.toString() + ")";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "(" + Divisor.toString() + ") R (" + Remainder.toString() + ")";
        ANSWER_C = "(" + AlgebraMath.getRandomPolynomial(VarName).toString() + ") R (" + Remainder.toString() + ")";
        ANSWER_D = AlgebraMath.getRandomPolynomial(VarName).toString();
    }

    //  WITH REMAINDER
    private void createProblem_07(){
        String VarName = MathUtility.getVariable();
        int HighestExponent = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        Polynomial Dividend, Divisor, Quotient, Remainder;

        while (true){
            Quotient = AlgebraMath.getRandomPolynomial(VarName, 2);
            Divisor = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, HighestExponent);
            Remainder = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, HighestExponent - 1);


            if (!Quotient.toString().equalsIgnoreCase("0")
                    && !Divisor.toString().equalsIgnoreCase("0")
                    && !Remainder.toString().equalsIgnoreCase("0")){
                break;
            }
        }

        Dividend = AlgebraMath.mul(Quotient, Divisor);
        Dividend = AlgebraMath.add(Dividend, Remainder);

        QUESTION = "(" + Dividend.toString() + ")" + InfoCollector.getDivisionSign() + "(" + Divisor.toString() + ") = ?";
        RIGHT_ANSWER = "(" +  Quotient.toString() + ") R (" + Remainder.toString() + ")";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "(" + Divisor.toString() + ") R (" + Remainder.toString() + ")";
        ANSWER_C = "(" + AlgebraMath.getRandomPolynomial(VarName).toString() + ") R (" + Remainder.toString() + ")";
        ANSWER_D = AlgebraMath.getRandomPolynomial(VarName).toString();
    }

    @Override
    public String getQuestion() {
        QUESTION = "<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
