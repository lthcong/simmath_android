package pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial;

import pckMath.pckProblemCollection.pckAlgebraOne.*;

/**
 * Created by Cong on 7/1/2017.
 */

public class Polynomial {

    private AlgebraicExpression EXPRESSION;

    public Polynomial(){
        EXPRESSION = new AlgebraicExpression();
    }

    public void setExpression(AlgebraicExpression _Expression){
        EXPRESSION.addNumber(_Expression.getNumberList());
    }

    public AlgebraicExpression getExpression(){
        return EXPRESSION;
    }

    public String toString(){
        return EXPRESSION.toString();
    }

    public boolean isNull(){
        return EXPRESSION.isNull();
    }
}
