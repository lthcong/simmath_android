package pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRationalPolynomial;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/6/2017.
 */

public class SimplifyRationalPolynomial extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SimplifyRationalPolynomial(){
        createProblem();
        swapAnswer();
    }

    public SimplifyRationalPolynomial(String Question,
                                 String RightAnswer,
                                 String AnswerA,
                                 String AnswerB,
                                 String AnswerC,
                                 String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 16;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            case 14:
                createProblem_15();
                break;
            case 15:
                createProblem_16();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SimplifyRationalPolynomial(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        String VarName = MathUtility.getVariable();
        Polynomial CommonFactor = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        Polynomial TopPolynomial = AlgebraMath.mul(FactorOne, CommonFactor);
        Polynomial BottomPolynomial = AlgebraMath.mul(FactorTwo, CommonFactor);

        RationalPolynomial _Polynomial = new RationalPolynomial();
        _Polynomial.setTopPolynomial(TopPolynomial);
        _Polynomial.setBottomPolynomial(BottomPolynomial);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorTwo);

        QUESTION = "Simplify: " + _Polynomial.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial().toString();
    }

    private void createProblem_02(){
        String VarName = MathUtility.getVariable();
        Polynomial CommonFactor = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 2);
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        Polynomial TopPolynomial = AlgebraMath.mul(FactorOne, CommonFactor);
        Polynomial BottomPolynomial = AlgebraMath.mul(FactorTwo, CommonFactor);

        RationalPolynomial _Polynomial = new RationalPolynomial();
        _Polynomial.setTopPolynomial(TopPolynomial);
        _Polynomial.setBottomPolynomial(BottomPolynomial);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorTwo);

        QUESTION = "Simplify: " + _Polynomial.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial().toString();
    }

    private void createProblem_03(){
        String VarName = MathUtility.getVariable();
        Polynomial CommonFactor = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 2);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        Polynomial TopPolynomial = AlgebraMath.mul(FactorOne, CommonFactor);
        Polynomial BottomPolynomial = AlgebraMath.mul(FactorTwo, CommonFactor);

        RationalPolynomial _Polynomial = new RationalPolynomial();
        _Polynomial.setTopPolynomial(TopPolynomial);
        _Polynomial.setBottomPolynomial(BottomPolynomial);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorTwo);

        QUESTION = "Simplify: " + _Polynomial.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial().toString();
    }

    private void createProblem_04(){
        String VarName = MathUtility.getVariable();
        Polynomial CommonFactor = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 2);

        Polynomial TopPolynomial = AlgebraMath.mul(FactorOne, CommonFactor);
        Polynomial BottomPolynomial = AlgebraMath.mul(FactorTwo, CommonFactor);

        RationalPolynomial _Polynomial = new RationalPolynomial();
        _Polynomial.setTopPolynomial(TopPolynomial);
        _Polynomial.setBottomPolynomial(BottomPolynomial);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorTwo);

        QUESTION = "Simplify: " + _Polynomial.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial().toString();
    }

    private void createProblem_05(){
        Polynomial CommonFactor = AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1);
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        Polynomial TopPolynomial = AlgebraMath.mul(FactorOne, CommonFactor);
        Polynomial BottomPolynomial = AlgebraMath.mul(FactorTwo, CommonFactor);

        RationalPolynomial _Polynomial = new RationalPolynomial();
        _Polynomial.setTopPolynomial(TopPolynomial);
        _Polynomial.setBottomPolynomial(BottomPolynomial);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorTwo);

        QUESTION = "Simplify: " + _Polynomial.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial().toString();
    }

    private void createProblem_06(){
        Polynomial CommonFactor = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        Polynomial TopPolynomial = AlgebraMath.mul(FactorOne, CommonFactor);
        Polynomial BottomPolynomial = AlgebraMath.mul(FactorTwo, CommonFactor);

        RationalPolynomial _Polynomial = new RationalPolynomial();
        _Polynomial.setTopPolynomial(TopPolynomial);
        _Polynomial.setBottomPolynomial(BottomPolynomial);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorTwo);

        QUESTION = "Simplify: " + _Polynomial.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial().toString();
    }

    private void createProblem_07(){
        Polynomial CommonFactor = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1);

        Polynomial TopPolynomial = AlgebraMath.mul(FactorOne, CommonFactor);
        Polynomial BottomPolynomial = AlgebraMath.mul(FactorTwo, CommonFactor);

        RationalPolynomial _Polynomial = new RationalPolynomial();
        _Polynomial.setTopPolynomial(TopPolynomial);
        _Polynomial.setBottomPolynomial(BottomPolynomial);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorTwo);

        QUESTION = "Simplify: " + _Polynomial.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial().toString();
    }

    private void createProblem_08(){
        Polynomial CommonFactor = AlgebraMath.getRandomPolynomialWithHighestExponent("y", 2);
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        Polynomial TopPolynomial = AlgebraMath.mul(FactorOne, CommonFactor);
        Polynomial BottomPolynomial = AlgebraMath.mul(FactorTwo, CommonFactor);

        RationalPolynomial _Polynomial = new RationalPolynomial();
        _Polynomial.setTopPolynomial(TopPolynomial);
        _Polynomial.setBottomPolynomial(BottomPolynomial);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorTwo);

        QUESTION = "Simplify: " + _Polynomial.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial().toString();
    }

    private void createProblem_09(){
        Polynomial CommonFactor = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 2);
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        Polynomial TopPolynomial = AlgebraMath.mul(FactorOne, CommonFactor);
        Polynomial BottomPolynomial = AlgebraMath.mul(FactorTwo, CommonFactor);

        RationalPolynomial _Polynomial = new RationalPolynomial();
        _Polynomial.setTopPolynomial(TopPolynomial);
        _Polynomial.setBottomPolynomial(BottomPolynomial);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorTwo);

        QUESTION = "Simplify: " + _Polynomial.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial().toString();
    }

    private void createProblem_10(){
        Polynomial CommonFactor = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 2);
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1);

        Polynomial TopPolynomial = AlgebraMath.mul(FactorOne, CommonFactor);
        Polynomial BottomPolynomial = AlgebraMath.mul(FactorTwo, CommonFactor);

        RationalPolynomial _Polynomial = new RationalPolynomial();
        _Polynomial.setTopPolynomial(TopPolynomial);
        _Polynomial.setBottomPolynomial(BottomPolynomial);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorTwo);

        QUESTION = "Simplify: " + _Polynomial.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial().toString();
    }

    private void createProblem_11(){
        Polynomial CommonFactor = AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1);
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 2);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        Polynomial TopPolynomial = AlgebraMath.mul(FactorOne, CommonFactor);
        Polynomial BottomPolynomial = AlgebraMath.mul(FactorTwo, CommonFactor);

        RationalPolynomial _Polynomial = new RationalPolynomial();
        _Polynomial.setTopPolynomial(TopPolynomial);
        _Polynomial.setBottomPolynomial(BottomPolynomial);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorTwo);

        QUESTION = "Simplify: " + _Polynomial.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial().toString();
    }

    private void createProblem_12(){
        Polynomial CommonFactor = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent("y", 2);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        Polynomial TopPolynomial = AlgebraMath.mul(FactorOne, CommonFactor);
        Polynomial BottomPolynomial = AlgebraMath.mul(FactorTwo, CommonFactor);

        RationalPolynomial _Polynomial = new RationalPolynomial();
        _Polynomial.setTopPolynomial(TopPolynomial);
        _Polynomial.setBottomPolynomial(BottomPolynomial);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorTwo);

        QUESTION = "Simplify: " + _Polynomial.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial().toString();
    }

    private void createProblem_13(){
        Polynomial CommonFactor = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 2);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1);

        Polynomial TopPolynomial = AlgebraMath.mul(FactorOne, CommonFactor);
        Polynomial BottomPolynomial = AlgebraMath.mul(FactorTwo, CommonFactor);

        RationalPolynomial _Polynomial = new RationalPolynomial();
        _Polynomial.setTopPolynomial(TopPolynomial);
        _Polynomial.setBottomPolynomial(BottomPolynomial);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorTwo);

        QUESTION = "Simplify: " + _Polynomial.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial().toString();
    }

    private void createProblem_14(){
        Polynomial CommonFactor = AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1);
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 2);

        Polynomial TopPolynomial = AlgebraMath.mul(FactorOne, CommonFactor);
        Polynomial BottomPolynomial = AlgebraMath.mul(FactorTwo, CommonFactor);

        RationalPolynomial _Polynomial = new RationalPolynomial();
        _Polynomial.setTopPolynomial(TopPolynomial);
        _Polynomial.setBottomPolynomial(BottomPolynomial);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorTwo);

        QUESTION = "Simplify: " + _Polynomial.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial().toString();
    }

    private void createProblem_15(){
        Polynomial CommonFactor = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 2);

        Polynomial TopPolynomial = AlgebraMath.mul(FactorOne, CommonFactor);
        Polynomial BottomPolynomial = AlgebraMath.mul(FactorTwo, CommonFactor);

        RationalPolynomial _Polynomial = new RationalPolynomial();
        _Polynomial.setTopPolynomial(TopPolynomial);
        _Polynomial.setBottomPolynomial(BottomPolynomial);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorTwo);

        QUESTION = "Simplify: " + _Polynomial.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial().toString();
    }

    private void createProblem_16(){
        Polynomial CommonFactor = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent("y", 2);

        Polynomial TopPolynomial = AlgebraMath.mul(FactorOne, CommonFactor);
        Polynomial BottomPolynomial = AlgebraMath.mul(FactorTwo, CommonFactor);

        RationalPolynomial _Polynomial = new RationalPolynomial();
        _Polynomial.setTopPolynomial(TopPolynomial);
        _Polynomial.setBottomPolynomial(BottomPolynomial);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorTwo);

        QUESTION = "Simplify: " + _Polynomial.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial().toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial().toString();
    }

    @Override
    public String getQuestion() {
        QUESTION = "<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
