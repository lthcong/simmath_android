package pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckIntroToPolynomial;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/5/2017.
 */

public class FactoringPolynomial extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FactoringPolynomial(){
        createProblem();
        swapAnswer();
    }

    public FactoringPolynomial(String Question,
                                 String RightAnswer,
                                 String AnswerA,
                                 String AnswerB,
                                 String AnswerC,
                                 String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 3;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FactoringPolynomial(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        String VarName = MathUtility.getVariable();
        Polynomial Factor_1 = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial Factor_2 = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        Polynomial _Polynomial = AlgebraMath.mul(Factor_1, Factor_2);

        QUESTION = "Find the factor of: <br>" + _Polynomial.toString();
        RIGHT_ANSWER = "(" + Factor_1.toString() + ")(" + Factor_2.toString() + ")";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "(" + AlgebraMath.getRandomPolynomial(VarName).toString() + ")(" + AlgebraMath.getRandomPolynomial(VarName) + ")";
        ANSWER_C = "(" + Factor_1.toString() + ")(" + Factor_1.toString() + ")";
        ANSWER_D = "(" + Factor_2.toString() + ")(" + Factor_2.toString() + ")";
    }

    private void createProblem_02(){
        String VarOne, VarTwo;
        while (true){
            VarOne = MathUtility.getVariable();
            VarTwo = MathUtility.getVariable();

            if (!VarOne.equalsIgnoreCase(VarTwo)){
                break;
            }
        }

        Polynomial Factor_1 = AlgebraMath.getRandomPolynomialWithHighestExponent(VarOne, 1);
        Polynomial Factor_2 = AlgebraMath.getRandomPolynomialWithHighestExponent(VarTwo, 1);

        Polynomial _Polynomial = AlgebraMath.mul(Factor_1, Factor_2);

        QUESTION = "Find the factor of: <br>" + _Polynomial.toString();
        RIGHT_ANSWER = "(" + Factor_1.toString() + ")(" + Factor_2.toString() + ")";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "(" + AlgebraMath.getRandomPolynomial(VarOne).toString() + ")(" + AlgebraMath.getRandomPolynomial(VarTwo) + ")";
        ANSWER_C = "(" + Factor_1.toString() + ")(" + Factor_1.toString() + ")";
        ANSWER_D = "(" + Factor_2.toString() + ")(" + Factor_2.toString() + ")";
    }

    private void createProblem_03(){
        String VarName = MathUtility.getVariable();
        Polynomial Factor_1 = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial Factor_2 = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial Factor_3 = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        Polynomial _Polynomial = AlgebraMath.mul(AlgebraMath.mul(Factor_1, Factor_2), Factor_3);

        QUESTION = "Find the factor of: <br>" + _Polynomial.toString();
        RIGHT_ANSWER = "(" + Factor_1.toString() + ")(" + Factor_2.toString() + ")(" + Factor_3.toString() + ")";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "(" + AlgebraMath.getRandomPolynomial(VarName).toString() + ")(" + AlgebraMath.getRandomPolynomial(VarName) + ")(" + AlgebraMath.getRandomPolynomial(VarName) + ")";
        ANSWER_C = "(" + AlgebraMath.getRandomPolynomial(VarName).toString() + ")(" + AlgebraMath.getRandomPolynomial(VarName) + ")(" + AlgebraMath.getRandomPolynomial(VarName) + ")";
        ANSWER_D = "(" + AlgebraMath.getRandomPolynomial(VarName).toString() + ")(" + AlgebraMath.getRandomPolynomial(VarName) + ")(" + AlgebraMath.getRandomPolynomial(VarName) + ")";
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
