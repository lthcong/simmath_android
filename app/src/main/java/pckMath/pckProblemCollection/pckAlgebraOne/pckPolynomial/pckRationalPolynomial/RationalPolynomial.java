package pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRationalPolynomial;

import pckInfo.InfoCollector;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/6/2017.
 */

public class RationalPolynomial {

    private Polynomial TOP_POLYNOMIAL, BOTTOM_POLYNOMIAL;

    public RationalPolynomial(){
        TOP_POLYNOMIAL = new Polynomial();
        BOTTOM_POLYNOMIAL = new Polynomial();
    }

    public RationalPolynomial(Polynomial TopPolynomial, Polynomial BottomPolynomial){
        TOP_POLYNOMIAL = TopPolynomial;
        BOTTOM_POLYNOMIAL = BottomPolynomial;
    }

    public void setTopPolynomial(Polynomial _Polynomial){
        TOP_POLYNOMIAL = _Polynomial;
    }

    public Polynomial getTopPolynomial(){
        return TOP_POLYNOMIAL;
    }

    public void setBottomPolynomial(Polynomial _Polynomial){
        BOTTOM_POLYNOMIAL = _Polynomial;
    }

    public Polynomial getBottomPolynomial(){
        return BOTTOM_POLYNOMIAL;
    }

    public String toString(){
        return InfoCollector.getStartNumerator() + "(" + TOP_POLYNOMIAL.toString() + ")" + InfoCollector.getEndNumerator() + "/" +
                InfoCollector.getStartDenominator() + "(" + BOTTOM_POLYNOMIAL.toString() + ")" + InfoCollector.getEndDenominator();
    }
}
