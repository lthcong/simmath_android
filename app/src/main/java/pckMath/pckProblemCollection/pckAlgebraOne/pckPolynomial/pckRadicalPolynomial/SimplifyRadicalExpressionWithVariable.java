package pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRadicalPolynomial;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRationalPolynomial.RationalPolynomial;

/**
 * Created by Cong on 7/8/2017.
 */

public class SimplifyRadicalExpressionWithVariable extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SimplifyRadicalExpressionWithVariable(){
        createProblem();
        swapAnswer();
    }

    public SimplifyRadicalExpressionWithVariable(String Question,
                                                    String RightAnswer,
                                                    String AnswerA,
                                                    String AnswerB,
                                                    String AnswerC,
                                                    String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 12;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SimplifyRadicalExpressionWithoutVariable(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  sqrt((ax)^2 + 2abx + b^2) = ax + b
    private void createProblem_01(){
        String VarName = MathUtility.getVariable();
        Polynomial _Polynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial Problem = AlgebraMath.mul(_Polynomial, _Polynomial);

        QUESTION = InfoCollector.getSquareRootSign() + "(" + Problem.toString() + ") = ?";
        RIGHT_ANSWER = _Polynomial.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
        ANSWER_C = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
        ANSWER_D = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
    }

    //  cube_root((ax + b)^3) = (ax + b)* sqrt(ax * b)
    private void createProblem_02(){
        String VarName = MathUtility.getVariable();
        Polynomial _Polynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial Problem = AlgebraMath.mul(AlgebraMath.mul(_Polynomial, _Polynomial), _Polynomial);

        QUESTION = InfoCollector.getExponentSign("3") + InfoCollector.getSquareRootSign() + "(" + Problem.toString() + ") = ?";
        RIGHT_ANSWER = _Polynomial.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
        ANSWER_C = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
        ANSWER_D = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
    }

    //  sqrt((ax)^2 + 2abx + b^2) + sqrt((ax)^2 + 2abx + b^2) = ?
    private void createProblem_03(){
        String VarName = MathUtility.getVariable();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial Result = AlgebraMath.add(FirstPolynomial, SecondPolynomial);

        QUESTION = InfoCollector.getSquareRootSign() + "(" + AlgebraMath.mul(FirstPolynomial, FirstPolynomial).toString() + ") + "
                + InfoCollector.getSquareRootSign() + "(" + AlgebraMath.mul(SecondPolynomial, SecondPolynomial).toString() + ") = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
        ANSWER_C = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
        ANSWER_D = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
    }

    //  sqrt((ax)^2 + 2abx + b^2) + cx + d = ?
    private void createProblem_04(){
        String VarName = MathUtility.getVariable();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        Polynomial Result = AlgebraMath.add(FirstPolynomial, SecondPolynomial);

        QUESTION = InfoCollector.getSquareRootSign() + "(" + AlgebraMath.mul(FirstPolynomial, FirstPolynomial).toString() + ") + "
                + "(" + SecondPolynomial.toString() + ") = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
        ANSWER_C = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
        ANSWER_D = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
    }

    //  sqrt((ax)^2 + 2abx + b^2) - sqrt((ax)^2 + 2abx + b^2) = ?
    private void createProblem_05(){
        String VarName = MathUtility.getVariable();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial Result = AlgebraMath.sub(FirstPolynomial, SecondPolynomial);

        QUESTION = InfoCollector.getSquareRootSign() + "(" + AlgebraMath.mul(FirstPolynomial, FirstPolynomial).toString() + ") - "
                + InfoCollector.getSquareRootSign() + "(" + AlgebraMath.mul(SecondPolynomial, SecondPolynomial).toString() + ") = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
        ANSWER_C = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
        ANSWER_D = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
    }

    //  sqrt((ax)^2 + 2abx + b^2) - (cx + d) = ?
    private void createProblem_06(){
        String VarName = MathUtility.getVariable();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        Polynomial Result = AlgebraMath.sub(FirstPolynomial, SecondPolynomial);

        QUESTION = InfoCollector.getSquareRootSign() + "(" + AlgebraMath.mul(FirstPolynomial, FirstPolynomial).toString() + ") - "
                + "(" + SecondPolynomial.toString() + ") = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
        ANSWER_C = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
        ANSWER_D = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
    }

    //  (ax + b) * sqrt(ax^2 + 2ab + b^2) = ?
    private void createProblem_07(){
        String VarName = MathUtility.getVariable();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        Polynomial Result = AlgebraMath.mul(FirstPolynomial, SecondPolynomial);

        QUESTION = FirstPolynomial.toString() + " x "
                + InfoCollector.getSquareRootSign() + "(" + AlgebraMath.mul(SecondPolynomial, SecondPolynomial).toString() + ") = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.mul(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1), AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1)).toString();
        ANSWER_C = AlgebraMath.mul(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1), AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1)).toString();
        ANSWER_D = AlgebraMath.mul(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1), AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1)).toString();
    }

    //  a * sqrt(ax + b) + a * sqrt(ax^2 + 2ab + b^2) = ?
    private void createProblem_08(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();

        String VarName = MathUtility.getVariable();
        Polynomial _Polynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        QUESTION = String.valueOf(a) + InfoCollector.getSquareRootSign() + "(" + _Polynomial.toString() + ") + "
                + String.valueOf(b) + InfoCollector.getSquareRootSign() + "(" + AlgebraMath.mul(_Polynomial, _Polynomial).toString() + ")";
        RIGHT_ANSWER = String.valueOf((a + b)) + InfoCollector.getSquareRootSign() + "(" + _Polynomial.toString() + ")";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((a + b)) + InfoCollector.getSquareRootSign() + "(" + _Polynomial.toString() + ")";
        ANSWER_C = InfoCollector.getSquareRootSign() + "(" + AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString() + ")";
        ANSWER_D = String.valueOf((a - b)) + InfoCollector.getSquareRootSign() + "(" + _Polynomial.toString() + ")";
    }

    //  sqrt(A * B) / sqrt(B)
    private void createProblem_09(){
        String VarName = MathUtility.getVariable();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        QUESTION = InfoCollector.getSquareRootSign() + "(" + AlgebraMath.mul(FirstPolynomial, SecondPolynomial).toString() + ")"
                + InfoCollector.getDivisionSign() + InfoCollector.getSquareRootSign() + "(" + SecondPolynomial.toString() + ") = ?";
        RIGHT_ANSWER = FirstPolynomial.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
        ANSWER_C = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
        ANSWER_D = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString();
    }

    //  sqrt(A) x sqrt(AB)
    private void createProblem_10(){
        String VarName = MathUtility.getVariable();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        QUESTION = InfoCollector.getSquareRootSign() + "(" + FirstPolynomial.toString() + ") * "
                +  InfoCollector.getSquareRootSign() + "(" + AlgebraMath.mul(FirstPolynomial, SecondPolynomial).toString() + ") = ?";
        RIGHT_ANSWER = "(" + FirstPolynomial.toString() + ")" + InfoCollector.getSquareRootSign() + "(" + SecondPolynomial.toString() + ")";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "(" + SecondPolynomial.toString() + ")" + InfoCollector.getSquareRootSign() + "(" + FirstPolynomial.toString() + ")";
        ANSWER_C = "(" + AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString() + ")" + InfoCollector.getSquareRootSign() + "(" + FirstPolynomial.toString() + ")";
        ANSWER_D = "(" + SecondPolynomial.toString() + ")" + InfoCollector.getSquareRootSign() + "(" + AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString() + ")";
    }

    //  sqrt(A * B) / sqrt(B * c)
    private void createProblem_11(){
        String VarName = MathUtility.getVariable();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial CommonPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FirstPolynomial);
        Result.setBottomPolynomial(SecondPolynomial);

        QUESTION = InfoCollector.getSquareRootSign() + "(" + AlgebraMath.mul(FirstPolynomial, CommonPolynomial).toString() + ")" + InfoCollector.getDivisionSign()
                + InfoCollector.getSquareRootSign() + "(" + AlgebraMath.mul(SecondPolynomial, CommonPolynomial).toString() + ") = ?";
        RIGHT_ANSWER = InfoCollector.getStartNumerator() + InfoCollector.getSquareRootSign() + "(" + FirstPolynomial.toString() + ")" + InfoCollector.getEndNumerator() + "/"
                + InfoCollector.getStartDenominator() + InfoCollector.getSquareRootSign() + "(" + SecondPolynomial.toString() + ")" + InfoCollector.getEndDenominator();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = InfoCollector.getStartNumerator() + InfoCollector.getSquareRootSign() + "(" + SecondPolynomial.toString() + ")" + InfoCollector.getEndNumerator() + "/"
                + InfoCollector.getStartDenominator() + InfoCollector.getSquareRootSign() + "(" + FirstPolynomial.toString() + ")" + InfoCollector.getEndDenominator();
        ANSWER_C = InfoCollector.getStartNumerator() + InfoCollector.getSquareRootSign() + "(" + AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString() + ")" + InfoCollector.getEndNumerator() + "/"
                + InfoCollector.getStartDenominator() + InfoCollector.getSquareRootSign() + "(" + SecondPolynomial.toString() + ")" + InfoCollector.getEndDenominator();
        ANSWER_D = InfoCollector.getStartNumerator() + InfoCollector.getSquareRootSign() + "(" + FirstPolynomial.toString() + ")" + InfoCollector.getEndNumerator() + "/"
                + InfoCollector.getStartDenominator() + InfoCollector.getSquareRootSign() + "(" + AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString() + ")" + InfoCollector.getEndDenominator();
    }

    //  sqrt(A * B) = sqrt(A) x sqrt(B)
    private void createProblem_12(){
        String VarName = MathUtility.getVariable();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        QUESTION = InfoCollector.getSquareRootSign() + "(" + AlgebraMath.mul(FirstPolynomial, SecondPolynomial).toString() + ") = ?";
        RIGHT_ANSWER = InfoCollector.getSquareRootSign() + "(" + FirstPolynomial.toString() + ") * " + InfoCollector.getSquareRootSign() + "(" + SecondPolynomial.toString() + ")";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = InfoCollector.getSquareRootSign() + "(" + AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString() + ") * " + InfoCollector.getSquareRootSign() + "(" + AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString() + ")";
        ANSWER_C = InfoCollector.getSquareRootSign() + "(" + AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString() + ") * " + InfoCollector.getSquareRootSign() + "(" + SecondPolynomial.toString() + ")";
        ANSWER_D = InfoCollector.getSquareRootSign() + "(" + FirstPolynomial.toString() + ") * " + InfoCollector.getSquareRootSign() + "(" + AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1).toString() + ")";
    }

    @Override
    public String getQuestion() {
        QUESTION = "<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
