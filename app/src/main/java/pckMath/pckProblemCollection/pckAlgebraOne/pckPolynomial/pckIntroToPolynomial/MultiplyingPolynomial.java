package pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckIntroToPolynomial;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/4/2017.
 */

public class MultiplyingPolynomial extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public MultiplyingPolynomial(){
        createProblem();
        swapAnswer();
    }

    public MultiplyingPolynomial(String Question,
                                       String RightAnswer,
                                       String AnswerA,
                                       String AnswerB,
                                       String AnswerC,
                                       String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 3;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            default:
                createProblem_01();
                break;
        }

        return new MultiplyingPolynomial(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        String VarName = MathUtility.getVariable();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomial(VarName);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomial(VarName);

        Polynomial Result = AlgebraMath.mul(FirstPolynomial, SecondPolynomial);

        QUESTION = "(" + FirstPolynomial.toString() + ") x (" + SecondPolynomial.toString() + ") = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.sub(FirstPolynomial, SecondPolynomial).toString();
        ANSWER_C = AlgebraMath.add(FirstPolynomial, SecondPolynomial).toString();
        ANSWER_D = AlgebraMath.getRandomPolynomial(VarName).toString();
    }

    private void createProblem_02(){
        String VarName = MathUtility.getVariable();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomial(VarName);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomial(VarName);

        Polynomial Result = AlgebraMath.mul(FirstPolynomial, SecondPolynomial);

        QUESTION = "( ? ) x (" + SecondPolynomial.toString() + ") = " + Result.toString();
        RIGHT_ANSWER = FirstPolynomial.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.sub(FirstPolynomial, SecondPolynomial).toString();
        ANSWER_C = AlgebraMath.add(FirstPolynomial, SecondPolynomial).toString();
        ANSWER_D = AlgebraMath.getRandomPolynomial(VarName).toString();
    }

    private void createProblem_03(){
        String VarName = MathUtility.getVariable();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomial(VarName);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomial(VarName);

        Polynomial Result = AlgebraMath.mul(FirstPolynomial, SecondPolynomial);

        QUESTION = "(" + FirstPolynomial.toString() + ") x ( ? ) = " + Result.toString();
        RIGHT_ANSWER = SecondPolynomial.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.sub(FirstPolynomial, SecondPolynomial).toString();
        ANSWER_C = AlgebraMath.add(FirstPolynomial, SecondPolynomial).toString();
        ANSWER_D = AlgebraMath.getRandomPolynomial(VarName).toString();
    }

    @Override
    public String getQuestion() {
        QUESTION = "<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
