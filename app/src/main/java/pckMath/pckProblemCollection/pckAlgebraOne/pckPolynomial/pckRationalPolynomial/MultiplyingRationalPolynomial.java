package pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRationalPolynomial;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/6/2017.
 */

public class MultiplyingRationalPolynomial extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public MultiplyingRationalPolynomial(){
        createProblem();
        swapAnswer();
    }

    public MultiplyingRationalPolynomial(String Question,
                                    String RightAnswer,
                                    String AnswerA,
                                    String AnswerB,
                                    String AnswerC,
                                    String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 5;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            default:
                createProblem_01();
                break;
        }

        return new MultiplyingRationalPolynomial(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  A/B x C/D
    private void createProblem_01(){
        String VarName = MathUtility.getVariable();
        RationalPolynomial FirstPolynomial = AlgebraMath.getRandomRationalPolynomial(VarName);
        RationalPolynomial SecondPolynomial = AlgebraMath.getRandomRationalPolynomial(VarName);

        RationalPolynomial Result = AlgebraMath.mul(FirstPolynomial, SecondPolynomial);

        QUESTION = FirstPolynomial.toString() + " x " + SecondPolynomial.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
    }

    //  A/B x B/C
    private void createProblem_02(){
        String VarName = MathUtility.getVariable();
        Polynomial CommonFactor = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        RationalPolynomial FirstPolynomial = new RationalPolynomial();
        FirstPolynomial.setTopPolynomial(CommonFactor);
        FirstPolynomial.setBottomPolynomial(FactorOne);

        RationalPolynomial SecondPolynomial = new RationalPolynomial();
        SecondPolynomial.setTopPolynomial(FactorTwo);
        SecondPolynomial.setBottomPolynomial(CommonFactor);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorTwo);
        Result.setBottomPolynomial(FactorOne);

        QUESTION = FirstPolynomial.toString() + " x " + SecondPolynomial.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
    }

    //  AB/CD x C/B
    private void createProblem_03(){
        String VarName = MathUtility.getVariable();
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorThree = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorFour = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        RationalPolynomial FirstPolynomial = new RationalPolynomial();
        FirstPolynomial.setTopPolynomial(AlgebraMath.mul(FactorOne, FactorTwo));
        FirstPolynomial.setBottomPolynomial(AlgebraMath.mul(FactorThree, FactorFour));

        RationalPolynomial SecondPolynomial = new RationalPolynomial();
        SecondPolynomial.setTopPolynomial(FactorFour);
        SecondPolynomial.setBottomPolynomial(FactorTwo);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorThree);

        QUESTION = FirstPolynomial.toString() + " x " + SecondPolynomial.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
    }

    //  C/B x AB/CD
    private void createProblem_04(){
        String VarName = MathUtility.getVariable();
        Polynomial FactorOne = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorTwo = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorThree = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorFour = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        RationalPolynomial FirstPolynomial = new RationalPolynomial();
        FirstPolynomial.setTopPolynomial(AlgebraMath.mul(FactorOne, FactorTwo));
        FirstPolynomial.setBottomPolynomial(AlgebraMath.mul(FactorThree, FactorFour));

        RationalPolynomial SecondPolynomial = new RationalPolynomial();
        SecondPolynomial.setTopPolynomial(FactorFour);
        SecondPolynomial.setBottomPolynomial(FactorTwo);

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(FactorOne);
        Result.setBottomPolynomial(FactorThree);

        QUESTION = SecondPolynomial.toString() + " x " + FirstPolynomial.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
        ANSWER_C = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
    }

    //  AB / CD x EC / BF = AE / DF
    private void createProblem_05(){
        String VarName = MathUtility.getVariable();
        Polynomial FactorA = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorB = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorC = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorD = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorE = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial FactorF = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        RationalPolynomial FirstPolynomial = new RationalPolynomial();
        FirstPolynomial.setTopPolynomial(AlgebraMath.mul(FactorA, FactorB));
        FirstPolynomial.setBottomPolynomial(AlgebraMath.mul(FactorC, FactorD));

        RationalPolynomial SecondPolynomial = new RationalPolynomial();
        SecondPolynomial.setTopPolynomial(AlgebraMath.mul(FactorE, FactorC));
        SecondPolynomial.setBottomPolynomial(AlgebraMath.mul(FactorB, FactorF));

        RationalPolynomial Result = new RationalPolynomial();
        Result.setTopPolynomial(AlgebraMath.mul(FactorA, FactorE));
        Result.setBottomPolynomial(AlgebraMath.mul(FactorD, FactorF));

        QUESTION = SecondPolynomial.toString() + " x " + FirstPolynomial.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FirstPolynomial.toString();
        ANSWER_C = SecondPolynomial.toString();
        ANSWER_D = AlgebraMath.getRandomRationalPolynomial(VarName).toString();
    }

    @Override
    public String getQuestion() {
        QUESTION = "<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
