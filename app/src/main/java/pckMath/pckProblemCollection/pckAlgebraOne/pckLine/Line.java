package pckMath.pckProblemCollection.pckAlgebraOne.pckLine;

import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;

/**
 * Created by Cong on 7/15/2017.
 */

public class Line {

    private int A_COEFFICIENT, B_COEFFICIENT, C_COEFFICIENT;
    private Point RANDOM_POINT;
    private Fraction SLOPE ,Y_INTERCEPT;

    public Line(){
        A_COEFFICIENT = 0;
        B_COEFFICIENT = 0;
        C_COEFFICIENT = 0;

        RANDOM_POINT = new Point();
        SLOPE = new Fraction();
        Y_INTERCEPT = new Fraction();
    }

    public Line(int ACoefficient, int BCoefficient, int CCoefficient){
        A_COEFFICIENT = ACoefficient;
        B_COEFFICIENT = BCoefficient;
        C_COEFFICIENT = CCoefficient;

        RANDOM_POINT = new Point();
        SLOPE = new Fraction();
        Y_INTERCEPT = new Fraction();
    }

    public Line(Point RandomPoint, Fraction Slope){
        A_COEFFICIENT = 0;
        B_COEFFICIENT = 0;
        C_COEFFICIENT = 0;

        RANDOM_POINT = RandomPoint;
        SLOPE = Slope;
        Y_INTERCEPT = new Fraction();
    }

    public Line(Fraction Slope, Fraction YIntercept){
        A_COEFFICIENT = 0;
        B_COEFFICIENT = 0;
        C_COEFFICIENT = 0;

        RANDOM_POINT = new Point();
        SLOPE = Slope;
        Y_INTERCEPT = YIntercept;
    }
    
    public void setACoefficient(int Coefficient){
        A_COEFFICIENT = Coefficient;
    }
    
    public int getACoefficient(){
        return A_COEFFICIENT;
    }

    public void setBCoefficient(int Coefficient){
        B_COEFFICIENT = Coefficient;
    }

    public int getBCoefficient(){
        return B_COEFFICIENT;
    }

    public void setCCoefficient(int Coefficient){
        C_COEFFICIENT = Coefficient;
    }

    public int getCCoefficient(){
        return C_COEFFICIENT;
    }

    public Point getPoint(){
        return RANDOM_POINT;
    }

    public void setSlope(Fraction Slope){
        SLOPE = Slope;
    }

    public Fraction getSlope(){
        return SLOPE;
    }

    public void setYIntercept(Fraction YIntercept){
        Y_INTERCEPT = YIntercept;
    }

    public Fraction getYIntercept(){
        return Y_INTERCEPT;
    }

    //  AX + BY + C = 0
    public String getStandardForm(){
        return String.valueOf(A_COEFFICIENT) + "x + " + String.valueOf(B_COEFFICIENT) + "y + " + String.valueOf(C_COEFFICIENT) + " = 0";
    }

    //  (Y - y) = K * (X - x)
    public String getPointSlopeForm(){
        return "(y - " + RANDOM_POINT.getYCoordinate() + ") = " + String.valueOf(SLOPE) + "(x - " + String.valueOf(RANDOM_POINT.getXCoordinate()) + ")";
    }

    //  Y = KX + y
    public String getSlopeInterceptForm(){
        return "y = " + String.valueOf(SLOPE) + "x + " + String.valueOf(Y_INTERCEPT);
    }

    public String toString(){
        String StringLine;

        if (A_COEFFICIENT == 0 && B_COEFFICIENT == 0 && C_COEFFICIENT == 0){
            if (Y_INTERCEPT.getValue() != 0){
                StringLine = getSlopeInterceptForm();
            }
            else {
                StringLine = getPointSlopeForm();
            }
        }
        else {
            StringLine = getStandardForm();
        }

        return StringLine;
    }

}
