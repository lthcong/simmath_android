package pckMath.pckProblemCollection.pckAlgebraOne.pckLine.pckBasicLine;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Line;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.LineMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;

/**
 * Created by Cong on 7/16/2017.
 */

public class FindingDistanceAndMidpoint extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingDistanceAndMidpoint(){
        createProblem();
        swapAnswer();
    }

    public FindingDistanceAndMidpoint(String Question,
                                          String RightAnswer,
                                          String AnswerA,
                                          String AnswerB,
                                          String AnswerC,
                                          String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FindingDistanceAndMidpoint(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  DISTANCE
    private void createProblem_01(){
        Point PointA = LineMath.getRandomPoint();
        Point PointB = LineMath.getRandomPoint();

        QUESTION = "Find the distance between two points: " + PointA.toString() + ", " + PointB.toString() + ".";
        RIGHT_ANSWER = LineMath.findDistanceBetween(PointA, PointB);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.findDistanceBetween(PointA, LineMath.getRandomPoint());
        ANSWER_C = LineMath.findDistanceBetween(PointB, LineMath.getRandomPoint());
        ANSWER_D = LineMath.findDistanceBetween(LineMath.getRandomPoint(), LineMath.getRandomPoint());
    }

    //  MIDPOINT
    private void createProblem_02(){
        Point PointA = LineMath.getRandomPoint();
        Point PointB = LineMath.getRandomPoint();

        QUESTION = "Find the midpoint of the two points: " + PointA.toString() + ", " + PointB.toString() + ".";
        RIGHT_ANSWER = LineMath.findMidPoint(PointA, PointB).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.findMidPoint(PointA, LineMath.getRandomPoint()).toString();
        ANSWER_C = LineMath.findMidPoint(PointB, LineMath.getRandomPoint()).toString();
        ANSWER_D = LineMath.findMidPoint(LineMath.getRandomPoint(), LineMath.getRandomPoint()).toString();
    }

    //  MIDPOINT
    private void createProblem_03(){
        Point PointA = LineMath.getRandomPoint();
        Point PointB = LineMath.getRandomPoint();
        Point MidPoint = LineMath.findMidPoint(PointA, PointB);

        QUESTION = MidPoint.toString() + " is the midpoint of " + PointA.toString() + " and what number?";
        RIGHT_ANSWER = PointB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = PointA.toString();
        ANSWER_C = LineMath.getRandomPoint().toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  STANDARD FORM: POINT ON LINE
    private void createProblem_04(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        QUESTION = "Find the point on the line " + _Line.toString() + ".";
        RIGHT_ANSWER = LineMath.findPointOnTheLine(_Line).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Point(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit()).toString();
        ANSWER_C = new Point(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit()).toString();
        ANSWER_D = new Point(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit()).toString();
    }

    //  POINT SLOPE FORM: POINT ON LINE
    private void createProblem_05(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        QUESTION = "Find the point on the line " + LineMath.getStandardToPointSlope(_Line).toString() + ".";
        RIGHT_ANSWER = LineMath.findPointOnTheLine(_Line).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Point(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit()).toString();
        ANSWER_C = new Point(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit()).toString();
        ANSWER_D = new Point(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit()).toString();
    }

    //  SLOPE INTERCEPT FORM: POINT ON LINE
    private void createProblem_06(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        QUESTION = "Find the point on the line " + LineMath.getStandardToSlopeIntercept(_Line).toString() + ".";
        RIGHT_ANSWER = LineMath.findPointOnTheLine(_Line).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Point(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit()).toString();
        ANSWER_C = new Point(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit()).toString();
        ANSWER_D = new Point(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit()).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
