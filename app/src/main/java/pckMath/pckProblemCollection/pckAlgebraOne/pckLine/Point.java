package pckMath.pckProblemCollection.pckAlgebraOne.pckLine;

import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;

/**
 * Created by Cong on 7/15/2017.
 */

public class Point {

    private Fraction X_COORDINATE, Y_COORDINATE;
    
    public Point(){
        X_COORDINATE = new Fraction(0, 1);
        Y_COORDINATE = new Fraction(0, 1);
    }
    
    public Point(int XCoordinate, int YCoordinate){
        X_COORDINATE = new Fraction(XCoordinate, 1);
        Y_COORDINATE = new Fraction(YCoordinate, 1);
    }
    
    public Point(Fraction XCoordinate, Fraction YCoordinate){
        X_COORDINATE = XCoordinate;
        Y_COORDINATE = YCoordinate;
    }
    
    public void setXCoordinate(int XCoordinate){
        X_COORDINATE = new Fraction(XCoordinate, 1);
    }

    public void setXCoordinate(Fraction XCoordinate){
        X_COORDINATE = XCoordinate;
    }
    
    public Fraction getXCoordinate(){
        return X_COORDINATE;
    }

    public void setYCoordinate(int YCoordinate){
        Y_COORDINATE = new Fraction(YCoordinate, 1);
    }

    public void setYCoordinate(Fraction YCoordinate){
        Y_COORDINATE = YCoordinate;
    }

    public Fraction getYCoordinate(){
        return Y_COORDINATE;
    }

    public boolean isTheSame(Point _Point){
        boolean result = false;
        if (X_COORDINATE.isEquivalent(_Point.getXCoordinate())
                && Y_COORDINATE.isEquivalent(_Point.getYCoordinate())){
            result = true;
        }

        return result;
    }

    public boolean isTheOrigin(Point _Point){
        boolean result = false;
        if (X_COORDINATE.getValue() == 0.0
                && Y_COORDINATE.getValue() == 0.0){
            result = true;
        }

        return result;
    }

    public String toString(){
        return "(" + X_COORDINATE.toString() + ", " + Y_COORDINATE + ")";
    }

}
