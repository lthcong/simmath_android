package pckMath.pckProblemCollection.pckAlgebraOne.pckLine.pckFindingEquation;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Line;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.LineMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/17/2017.
 */

public class FindingPerpendicularAndParallelLine extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingPerpendicularAndParallelLine(){
        createProblem();
        swapAnswer();
    }

    public FindingPerpendicularAndParallelLine(String Question,
                                String RightAnswer,
                                String AnswerA,
                                String AnswerB,
                                String AnswerC,
                                String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 18;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            case 14:
                createProblem_15();
                break;
            case 15:
                createProblem_16();
                break;
            case 16:
                createProblem_17();
                break;
            case 17:
                createProblem_18();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FindingPerpendicularAndParallelLine(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  PERPENDICULAR - STANDARD FORM - STANDARD FORM
    private void createProblem_01(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = FractionMath.divideFraction(new Fraction(-1, 1), LineMath.getSlope(LineA));
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and perpendicular to " + LineA.toString() + ".";
        RIGHT_ANSWER = LineMath.getPointSlopeToStandardForm(LineB).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  PERPENDICULAR - STANDARD FORM - POINT SLOPE FORM
    private void createProblem_02(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = FractionMath.divideFraction(new Fraction(-1, 1), LineMath.getSlope(LineA));
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and perpendicular to " + LineA.toString() + ".";
        RIGHT_ANSWER = LineB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  PERPENDICULAR - STANDARD FORM - SLOPE INTERCEPT FORM
    private void createProblem_03(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = FractionMath.divideFraction(new Fraction(-1, 1), LineMath.getSlope(LineA));
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and perpendicular to " + LineA.toString() + ".";
        RIGHT_ANSWER = LineMath.getStandardToSlopeIntercept(LineMath.getPointSlopeToStandardForm(LineB)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  PERPENDICULAR - POINT SLOPE FORM - STANDARD FORM
    private void createProblem_04(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = FractionMath.divideFraction(new Fraction(-1, 1), LineMath.getSlope(LineA));
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and perpendicular to " + LineMath.getStandardToPointSlope(LineA).toString() + ".";
        RIGHT_ANSWER = LineMath.getPointSlopeToStandardForm(LineB).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  PERPENDICULAR - POINT SLOPE FORM - SLOPE INTERCEPT FORM
    private void createProblem_05(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = FractionMath.divideFraction(new Fraction(-1, 1), LineMath.getSlope(LineA));
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and perpendicular to " + LineMath.getStandardToPointSlope(LineA).toString() + ".";
        RIGHT_ANSWER = LineMath.getStandardToSlopeIntercept(LineMath.getPointSlopeToStandardForm(LineB)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  PERPENDICULAR - POINT SLOPE FORM - POINT SLOPE FORM
    private void createProblem_06(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = FractionMath.divideFraction(new Fraction(-1, 1), LineMath.getSlope(LineA));
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and perpendicular to " + LineMath.getStandardToPointSlope(LineA).toString() + ".";
        RIGHT_ANSWER = LineB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  PERPENDICULAR - SLOPE INTERCEPT FORM - STANDARD FORM
    private void createProblem_07(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = FractionMath.divideFraction(new Fraction(-1, 1), LineMath.getSlope(LineA));
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and perpendicular to " + LineMath.getStandardToSlopeIntercept(LineA).toString() + ".";
        RIGHT_ANSWER = LineMath.getPointSlopeToStandardForm(LineB).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  PERPENDICULAR - SLOPE INTERCEPT FORM - SLOPE INTERCEPT FORM
    private void createProblem_08(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = FractionMath.divideFraction(new Fraction(-1, 1), LineMath.getSlope(LineA));
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and perpendicular to " + LineMath.getStandardToSlopeIntercept(LineA).toString() + ".";
        RIGHT_ANSWER = LineMath.getStandardToSlopeIntercept(LineMath.getPointSlopeToStandardForm(LineB)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  PERPENDICULAR - SLOPE INTERCEPT FORM - POINT SLOPE FORM
    private void createProblem_09(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = FractionMath.divideFraction(new Fraction(-1, 1), LineMath.getSlope(LineA));
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and perpendicular to " + LineMath.getStandardToSlopeIntercept(LineA).toString() + ".";
        RIGHT_ANSWER = LineB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  PARALLEL - STANDARD FORM - STANDARD FORM
    private void createProblem_10(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = LineMath.getSlope(LineA);
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and parallel to " + LineA.toString() + ".";
        RIGHT_ANSWER = LineMath.getPointSlopeToStandardForm(LineB).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  PARALLEL - STANDARD FORM - POINT SLOPE FORM
    private void createProblem_11(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = LineMath.getSlope(LineA);
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and parallel to " + LineA.toString() + ".";
        RIGHT_ANSWER = LineB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  PARALLEL - STANDARD FORM - SLOPE INTERCEPT FORM
    private void createProblem_12(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = LineMath.getSlope(LineA);
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and parallel to " + LineA.toString() + ".";
        RIGHT_ANSWER = LineMath.getStandardToSlopeIntercept(LineMath.getPointSlopeToStandardForm(LineB)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  PARALLEL - POINT SLOPE FORM - STANDARD FORM
    private void createProblem_13(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = LineMath.getSlope(LineA);
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and parallel to " + LineMath.getStandardToPointSlope(LineA).toString() + ".";
        RIGHT_ANSWER = LineMath.getPointSlopeToStandardForm(LineB).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  PARALLEL - POINT SLOPE FORM - SLOPE INTERCEPT FORM
    private void createProblem_14(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = LineMath.getSlope(LineA);
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and parallel to " + LineMath.getStandardToPointSlope(LineA).toString() + ".";
        RIGHT_ANSWER = LineMath.getStandardToSlopeIntercept(LineMath.getPointSlopeToStandardForm(LineB)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  PARALLEL - POINT SLOPE FORM - POINT SLOPE FORM
    private void createProblem_15(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = LineMath.getSlope(LineA);
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and parallel to " + LineMath.getStandardToPointSlope(LineA).toString() + ".";
        RIGHT_ANSWER = LineB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  PARALLEL - SLOPE INTERCEPT FORM - STANDARD FORM
    private void createProblem_16(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = LineMath.getSlope(LineA);
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and parallel to " + LineMath.getStandardToSlopeIntercept(LineA).toString() + ".";
        RIGHT_ANSWER = LineMath.getPointSlopeToStandardForm(LineB).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  PARALLEL - SLOPE INTERCEPT FORM - SLOPE INTERCEPT FORM
    private void createProblem_17(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = LineMath.getSlope(LineA);
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and parallel to " + LineMath.getStandardToSlopeIntercept(LineA).toString() + ".";
        RIGHT_ANSWER = LineMath.getStandardToSlopeIntercept(LineMath.getPointSlopeToStandardForm(LineB)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  PARALLEL - SLOPE INTERCEPT FORM - POINT SLOPE FORM
    private void createProblem_18(){
        Line LineA = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        Point _Point = LineMath.getRandomPoint();
        Fraction Slope = LineMath.getSlope(LineA);
        Line LineB = new Line(_Point, Slope);

        QUESTION = "Find the line pass through the point " + _Point.toString() + " and parallel to " + LineMath.getStandardToSlopeIntercept(LineA).toString() + ".";
        RIGHT_ANSWER = LineB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
