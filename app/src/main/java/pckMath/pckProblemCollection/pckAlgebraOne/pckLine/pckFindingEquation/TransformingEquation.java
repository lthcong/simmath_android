package pckMath.pckProblemCollection.pckAlgebraOne.pckLine.pckFindingEquation;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Line;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.LineMath;

/**
 * Created by Cong on 7/17/2017.
 */

public class TransformingEquation extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public TransformingEquation(){
        createProblem();
        swapAnswer();
    }

    public TransformingEquation(String Question,
                                        String RightAnswer,
                                        String AnswerA,
                                        String AnswerB,
                                        String AnswerC,
                                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 9;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            default:
                createProblem_01();
                break;
        }

        return new TransformingEquation(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  STANDARD FORM - POINT SLOPE FORM
    private void createProblem_01(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        QUESTION = "Transforming this equation to point slope form: " + _Line.toString() + ".";
        RIGHT_ANSWER = LineMath.getStandardToPointSlope(_Line).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  STANDARD FORM - SLOPE INTERCEPT FORM
    private void createProblem_02(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        QUESTION = "Transforming this equation to slope intercept form: " + _Line.toString() + ".";
        RIGHT_ANSWER = LineMath.getStandardToSlopeIntercept(_Line).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  POINT SLOPE FORM - STANDARD FORM
    private void createProblem_03(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        QUESTION = "Transforming this equation to standard form: " + LineMath.getStandardToPointSlope(_Line).toString() + ".";
        RIGHT_ANSWER = _Line.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  POINT SLOPE FORM - SLOPE INTERCEPT FORM
    private void createProblem_04(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        QUESTION = "Transforming this equation to slope intercept form: " + LineMath.getStandardToPointSlope(_Line).toString() + ".";
        RIGHT_ANSWER = LineMath.getStandardToSlopeIntercept(_Line).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  SLOPE INTERCEPT FORM - STANDARD FORM
    private void createProblem_05(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        QUESTION = "Transforming this equation to standard form: " + LineMath.getStandardToSlopeIntercept(_Line).toString() + ".";
        RIGHT_ANSWER = _Line.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  SLOPE INTERCEPT FORM - POINT SLOPE FORM
    private void createProblem_06(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        QUESTION = "Transforming this equation to point slope form: " + LineMath.getStandardToSlopeIntercept(_Line).toString() + ".";
        RIGHT_ANSWER = LineMath.getStandardToPointSlope(_Line).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  POINT SLOPE - STANDARD FORM
    private void createProblem_07(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
        Line ConvertedLine = LineMath.getStandardToPointSlope(_Line);

        QUESTION = "Find the equation of the line pass through the point " + ConvertedLine.getPoint().toString() + ", and the slope is " + ConvertedLine.getSlope().toString() + ".";
        RIGHT_ANSWER = _Line.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = (new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  POINT SLOPE - SLOPE INTERCEPT FORM
    private void createProblem_08(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
        Line ConvertedLine = LineMath.getStandardToPointSlope(_Line);

        QUESTION = "Find the equation of the line pass through the point " + ConvertedLine.getPoint().toString() + ", and the slope is " + ConvertedLine.getSlope().toString() + ".";
        RIGHT_ANSWER = LineMath.getStandardToSlopeIntercept(_Line).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = LineMath.getStandardToSlopeIntercept(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    //  POINT SLOPE - POINT SLOPE FORM
    private void createProblem_09(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
        Line ConvertedLine = LineMath.getStandardToPointSlope(_Line);

        QUESTION = "Find the equation of the line pass through the point " + ConvertedLine.getPoint().toString() + ", and the slope is " + ConvertedLine.getSlope().toString() + ".";
        RIGHT_ANSWER = LineMath.getStandardToPointSlope(_Line).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_D = LineMath.getStandardToPointSlope(new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit())).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
