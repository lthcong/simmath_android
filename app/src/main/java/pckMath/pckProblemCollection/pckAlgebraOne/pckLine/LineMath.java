package pckMath.pckProblemCollection.pckAlgebraOne.pckLine;

import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/15/2017.
 */

public class LineMath {

    public static Point getRandomPoint(){
        return new Point(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
    }

    public static String findDistanceBetween(Point PointA, Point PointB){
        String DistanceString;
        Fraction Distance;

        Fraction XDifferent = FractionMath.subtractFraction(PointA.getXCoordinate(), PointB.getXCoordinate());
        XDifferent = FractionMath.multiplyFraction(XDifferent, XDifferent);

        Fraction YDifferent = FractionMath.subtractFraction(PointA.getYCoordinate(), PointB.getYCoordinate());
        YDifferent = FractionMath.multiplyFraction(YDifferent, YDifferent);

        Distance = FractionMath.addFraction(XDifferent, YDifferent);

        if (!MathUtility.isSquaredNumber(Distance.getNumerator()) && !MathUtility.isSquaredNumber(Distance.getDenominator())){
            DistanceString = InfoCollector.getSquareRootSign() + "(" + Distance.toString() + ")";
        }
        else {
            String StNumerator, StDenominator;
            if (MathUtility.isSquaredNumber(Distance.getNumerator())){
                StNumerator = String.valueOf((int) Math.sqrt(Distance.getNumerator()));
            }
            else {
                StNumerator = InfoCollector.getSquareRootSign() + "(" + String.valueOf(Distance.getNumerator()) + ")";
            }

            if (MathUtility.isSquaredNumber(Distance.getDenominator())){
                StDenominator = String.valueOf((int) Math.sqrt(Distance.getDenominator()));
            }
            else {
                StDenominator = InfoCollector.getSquareRootSign() + "(" + String.valueOf(Distance.getDenominator()) + ")";
            }

            DistanceString = InfoCollector.getStartNumerator() + StNumerator + InfoCollector.getEndNumerator() + "/"
                    + InfoCollector.getStartDenominator() + StDenominator + InfoCollector.getEndDenominator();
        }

        return DistanceString;
    }

    public static double findDecimalDistanceBetween(Point PointA, Point PointB){
        double Distance;

        Fraction XDifferent = FractionMath.subtractFraction(PointA.getXCoordinate(), PointB.getXCoordinate());
        XDifferent = FractionMath.multiplyFraction(XDifferent, XDifferent);

        Fraction YDifferent = FractionMath.subtractFraction(PointA.getYCoordinate(), PointB.getYCoordinate());
        YDifferent = FractionMath.multiplyFraction(YDifferent, YDifferent);

        Distance = FractionMath.addFraction(XDifferent, YDifferent).getValue();
        Distance = Math.sqrt(Distance);

        return Distance;
    }

    public static Point findMidPoint(Point PointA, Point PointB){
        Fraction XCoordinate = FractionMath.addFraction(PointA.getXCoordinate(), PointB.getXCoordinate());
        XCoordinate = FractionMath.multiplyFraction(XCoordinate, new Fraction(1, 2));

        Fraction YCoordinate = FractionMath.addFraction(PointA.getYCoordinate(), PointB.getYCoordinate());
        YCoordinate = FractionMath.multiplyFraction(YCoordinate, new Fraction(1, 2));

        return new Point(XCoordinate, YCoordinate);
    }

    public static Point findPointOnTheLine(Line LineInStandardForm){
        int XCoordinate = MathUtility.getRandomNumber_1Digit();
        Fraction YCoordinate = new Fraction(LineInStandardForm.getACoefficient() * XCoordinate + LineInStandardForm.getCCoefficient(),
                0 - LineInStandardForm.getBCoefficient());
        YCoordinate = FractionMath.getSimplifiedFraction(YCoordinate);

        return new Point(new Fraction(XCoordinate), YCoordinate);
    }

    public static Point findPointOnTheLineWithGivenXCoordinate(Line LineInStandardForm, int XCoordinate){
        Fraction YCoordinate = new Fraction(LineInStandardForm.getACoefficient() * XCoordinate + LineInStandardForm.getCCoefficient(),
                0 - LineInStandardForm.getBCoefficient());
        YCoordinate = FractionMath.getSimplifiedFraction(YCoordinate);

        return new Point(new Fraction(XCoordinate), YCoordinate);
    }

    public static Point findPointOnTheLineWithGivenYCoordinate(Line LineInStandardForm, int YCoordinate){
        Fraction XCoordinate = new Fraction(LineInStandardForm.getBCoefficient() * YCoordinate + LineInStandardForm.getCCoefficient(),
                0 - LineInStandardForm.getACoefficient());
        XCoordinate = FractionMath.getSimplifiedFraction(XCoordinate);

        return new Point(XCoordinate, new Fraction(YCoordinate));
    }

    public static Point findXIntercept(Line LineInStandardForm){
        return findPointOnTheLineWithGivenYCoordinate(LineInStandardForm, 0);
    }

    public static Point findYIntercept(Line LineInStandardForm){
        return findPointOnTheLineWithGivenXCoordinate(LineInStandardForm, 0);
    }

    public static Line getRandomLine(){
        return new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
    }

    public static Fraction getSlope(Point PointA, Point PointB){
        return FractionMath.divideFraction(FractionMath.subtractFraction(PointA.getYCoordinate(), PointB.getYCoordinate()),
                FractionMath.subtractFraction(PointA.getXCoordinate(), PointB.getXCoordinate()));
    }

    public static Fraction getSlope(Line LineInStandardForm){
        return new Fraction(0 - LineInStandardForm.getACoefficient(), LineInStandardForm.getBCoefficient());
    }

    public static Line getStandardToSlopeIntercept(Line _Line){
        //  SLOPE
        Fraction Slope = new Fraction(0 - _Line.getACoefficient(), _Line.getBCoefficient());

        //  INTERCEPT
        Fraction Intercept = new Fraction(0 - _Line.getCCoefficient(), _Line.getBCoefficient());

        return new Line(Slope, Intercept);
    }

    public static Line getStandardToPointSlope(Line _Line){
        //  SLOPE
        Fraction Slope = new Fraction(0 - _Line.getACoefficient(), _Line.getBCoefficient());

        //  POINT
        Fraction Intercept = new Fraction(0 - _Line.getCCoefficient(), _Line.getBCoefficient());
        Fraction XCoordinate = FractionMath.getRandomUnitFraction();
        Fraction YCoordinate = FractionMath.subtractFraction(Intercept, XCoordinate);
        Point RandomPoint = new Point(XCoordinate, YCoordinate);

        return new Line(RandomPoint, Slope);
    }

    public static Line getSlopeInterceptToStandardForm(Line _Line){
        int ACoefficient = _Line.getSlope().getNumerator() * _Line.getYIntercept().getDenominator();
        int BCoefficient = _Line.getSlope().getDenominator() * _Line.getYIntercept().getDenominator() * -1;
        int CCoefficient = _Line.getYIntercept().getNumerator() * _Line.getSlope().getDenominator();

        return new Line(ACoefficient, BCoefficient, CCoefficient);
    }

    public static Line getPointSlopeToStandardForm(Line _Line){
        int ACoefficient = _Line.getSlope().getNumerator()
                * _Line.getPoint().getXCoordinate().getDenominator()
                *_Line.getPoint().getYCoordinate().getDenominator();
        int BCoefficient = _Line.getSlope().getDenominator()
                * _Line.getPoint().getXCoordinate().getDenominator()
                *_Line.getPoint().getYCoordinate().getDenominator() * -1;
        int CCoefficient = _Line.getPoint().getYCoordinate().getDenominator() * -1
                * _Line.getSlope().getNumerator()
                * _Line.getPoint().getXCoordinate().getNumerator()
                + _Line.getSlope().getDenominator()
                * _Line.getPoint().getXCoordinate().getDenominator()
                * _Line.getPoint().getYCoordinate().getNumerator();

        return new Line(ACoefficient, BCoefficient, CCoefficient);
    }

}
