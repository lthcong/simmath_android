package pckMath.pckProblemCollection.pckAlgebraOne.pckLine.pckBasicLine;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Line;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.LineMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/16/2017.
 */

public class FindingSlopeAndIntercepts extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingSlopeAndIntercepts(){
        createProblem();
        swapAnswer();
    }

    public FindingSlopeAndIntercepts(String Question,
                                      String RightAnswer,
                                      String AnswerA,
                                      String AnswerB,
                                      String AnswerC,
                                      String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 9;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FindingSlopeAndIntercepts(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  STANDARD FORM: SLOPE
    private void createProblem_01(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
        Fraction Slope = LineMath.getSlope(_Line);

        QUESTION = "Find slope of the line: " + _Line.toString();
        RIGHT_ANSWER = Slope.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Fraction(Slope.getNumerator() + 1, Slope.getDenominator())).toString();
        ANSWER_C = (new Fraction(Slope.getNumerator() + 1, Slope.getDenominator() + 1)).toString();
        ANSWER_D = (new Fraction(Slope.getNumerator() - 1, Slope.getDenominator() - 1)).toString();
    }

    //  SLOPE INTERCEPT FORM: SLOPE
    private void createProblem_02(){
        Fraction Slope = FractionMath.getRandomFraction();
        Fraction Intercept = FractionMath.getRandomFraction();

        Line _Line = new Line(Slope, Intercept);

        QUESTION = "Find slope of the line: " + _Line.toString();
        RIGHT_ANSWER = Slope.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Fraction(Slope.getNumerator() + 1, Slope.getDenominator())).toString();
        ANSWER_C = (new Fraction(Slope.getNumerator() + 1, Slope.getDenominator() + 1)).toString();
        ANSWER_D = (new Fraction(Slope.getNumerator() - 1, Slope.getDenominator() - 1)).toString();
    }

    //  POINT SLOPE FORM: SLOPE
    private void createProblem_03(){
        Fraction Slope = FractionMath.getRandomFraction();
        Point RandomPoint = LineMath.getRandomPoint();

        Line _Line = new Line(RandomPoint, Slope);

        QUESTION = "Find slope of the line: " + _Line.toString();
        RIGHT_ANSWER = Slope.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Fraction(Slope.getNumerator() + 1, Slope.getDenominator())).toString();
        ANSWER_C = (new Fraction(Slope.getNumerator() + 1, Slope.getDenominator() + 1)).toString();
        ANSWER_D = (new Fraction(Slope.getNumerator() - 1, Slope.getDenominator() - 1)).toString();
    }

    //  STANDARD FORM: FINDING X INTERCEPT
    private void createProblem_04(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        QUESTION = "Find the X intercept of the line " + _Line.toString() + ".";
        RIGHT_ANSWER = LineMath.findXIntercept(_Line).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Point(MathUtility.getRandomNumber_1Digit(), 0).toString();
        ANSWER_C = new Point(0, MathUtility.getRandomNumber_1Digit()).toString();
        ANSWER_D = new Point(MathUtility.getRandomNumber_1Digit(), 0).toString();
    }

    //  STANDARD FORM: FINDING Y INTERCEPT
    private void createProblem_05(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        QUESTION = "Find the Y intercept of the line " + _Line.toString() + ".";
        RIGHT_ANSWER = LineMath.findYIntercept(_Line).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.findXIntercept(_Line).toString();
        ANSWER_C = new Point(0, MathUtility.getRandomNumber_1Digit()).toString();
        ANSWER_D = new Point(0, MathUtility.getRandomNumber_1Digit()).toString();
    }

    //  SLOPE INTERCEPT FORM: FINDING X INTERCEPT
    private void createProblem_06(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        QUESTION = "Find the X intercept of the line " + LineMath.getStandardToSlopeIntercept(_Line).toString() + ".";
        RIGHT_ANSWER = LineMath.findXIntercept(_Line).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Point(MathUtility.getRandomNumber_1Digit(), 0).toString();
        ANSWER_C = new Point(0, MathUtility.getRandomNumber_1Digit()).toString();
        ANSWER_D = new Point(MathUtility.getRandomNumber_1Digit(), 0).toString();
    }

    //  SLOPE INTERCEPT FORM: FINDING Y INTERCEPT
    private void createProblem_07(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        QUESTION = "Find the Y intercept of the line " + LineMath.getStandardToSlopeIntercept(_Line).toString() + ".";
        RIGHT_ANSWER = LineMath.findYIntercept(_Line).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.findXIntercept(_Line).toString();
        ANSWER_C = new Point(0, MathUtility.getRandomNumber_1Digit()).toString();
        ANSWER_D = new Point(0, MathUtility.getRandomNumber_1Digit()).toString();
    }

    //  POINT SLOPE FORM: FINDING X INTERCEPT
    private void createProblem_08(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        QUESTION = "Find the X intercept of the line " + LineMath.getStandardToPointSlope(_Line).toString() + ".";
        RIGHT_ANSWER = LineMath.findXIntercept(_Line).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Point(MathUtility.getRandomNumber_1Digit(), 0).toString();
        ANSWER_C = new Point(0, MathUtility.getRandomNumber_1Digit()).toString();
        ANSWER_D = new Point(MathUtility.getRandomNumber_1Digit(), 0).toString();
    }

    //  POINT SLOPE FORM: FINDING Y INTERCEPT
    private void createProblem_09(){
        Line _Line = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        QUESTION = "Find the Y intercept of the line " + LineMath.getStandardToPointSlope(_Line).toString() + ".";
        RIGHT_ANSWER = LineMath.findYIntercept(_Line).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.findXIntercept(_Line).toString();
        ANSWER_C = new Point(0, MathUtility.getRandomNumber_1Digit()).toString();
        ANSWER_D = new Point(0, MathUtility.getRandomNumber_1Digit()).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
