package pckMath.pckProblemCollection.pckAlgebraOne.pckLine.pckFindingIntercept;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Line;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.LineMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.SystemOfLinearEquations;

/**
 * Created by Cong on 7/16/2017.
 */

public class FindingInterceptOfTwoLines extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingInterceptOfTwoLines(){
        createProblem();
        swapAnswer();
    }

    public FindingInterceptOfTwoLines(String Question,
                                      String RightAnswer,
                                      String AnswerA,
                                      String AnswerB,
                                      String AnswerC,
                                      String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 9;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FindingInterceptOfTwoLines(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  STANDARD FORM vs STANDARD FORM
    private void createProblem_01(){
        Line FirstLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
        Line SecondLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations("x", "y", FirstLine.getACoefficient(), FirstLine.getBCoefficient(), FirstLine.getCCoefficient(),
                SecondLine.getACoefficient(), SecondLine.getBCoefficient(), SecondLine.getCCoefficient());

        QUESTION = "Find the intersection point of the two lines: <br>" + FirstLine.toString() + " and " + SecondLine.toString() + ".";
        if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getNoSolution())){
            RIGHT_ANSWER = "The lines are parallel.";

            ANSWER_B = "These lines are the same.";
            ANSWER_C = LineMath.getRandomPoint().toString();
            ANSWER_D = LineMath.getRandomPoint().toString();
        }
        else {
            if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getInfiniteManySolution())){
                RIGHT_ANSWER = "These lines are the same.";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = LineMath.getRandomPoint().toString();
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
            else {
                SOLE.getSolution();
                RIGHT_ANSWER = "(" + SOLE.getXSolution() + ", " + SOLE.getYSolution() + ")";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = "These lines are the same.";
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  STANDARD FORM vs POINT SLOPE FORM
    private void createProblem_02(){
        Line FirstLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
        Line SecondLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations("x", "y", FirstLine.getACoefficient(), FirstLine.getBCoefficient(), FirstLine.getCCoefficient(),
                SecondLine.getACoefficient(), SecondLine.getBCoefficient(), SecondLine.getCCoefficient());

        QUESTION = "Find the intersection point of the two lines: <br>" + FirstLine.toString() + " and " + LineMath.getStandardToPointSlope(SecondLine).toString() + ".";
        if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getNoSolution())){
            RIGHT_ANSWER = "The lines are parallel.";

            ANSWER_B = "These lines are the same.";
            ANSWER_C = LineMath.getRandomPoint().toString();
            ANSWER_D = LineMath.getRandomPoint().toString();
        }
        else {
            if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getInfiniteManySolution())){
                RIGHT_ANSWER = "These lines are the same.";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = LineMath.getRandomPoint().toString();
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
            else {
                SOLE.getSolution();
                RIGHT_ANSWER = "(" + SOLE.getXSolution() + ", " + SOLE.getYSolution() + ")";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = "These lines are the same.";
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  POINT SLOPE FORM vs STANDARD FORM
    private void createProblem_03(){
        Line FirstLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
        Line SecondLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations("x", "y", FirstLine.getACoefficient(), FirstLine.getBCoefficient(), FirstLine.getCCoefficient(),
                SecondLine.getACoefficient(), SecondLine.getBCoefficient(), SecondLine.getCCoefficient());

        QUESTION = "Find the intersection point of the two lines: <br>" + LineMath.getStandardToPointSlope(FirstLine).toString() + " and " + SecondLine.toString() + ".";
        if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getNoSolution())){
            RIGHT_ANSWER = "The lines are parallel.";

            ANSWER_B = "These lines are the same.";
            ANSWER_C = LineMath.getRandomPoint().toString();
            ANSWER_D = LineMath.getRandomPoint().toString();
        }
        else {
            if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getInfiniteManySolution())){
                RIGHT_ANSWER = "These lines are the same.";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = LineMath.getRandomPoint().toString();
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
            else {
                SOLE.getSolution();
                RIGHT_ANSWER = "(" + SOLE.getXSolution() + ", " + SOLE.getYSolution() + ")";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = "These lines are the same.";
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  POINT SLOPE FORM vs POINT SLOPE FORM
    private void createProblem_04(){
        Line FirstLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
        Line SecondLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations("x", "y", FirstLine.getACoefficient(), FirstLine.getBCoefficient(), FirstLine.getCCoefficient(),
                SecondLine.getACoefficient(), SecondLine.getBCoefficient(), SecondLine.getCCoefficient());

        QUESTION = "Find the intersection point of the two lines: <br>" + LineMath.getStandardToPointSlope(FirstLine).toString() + " and " + LineMath.getStandardToPointSlope(SecondLine).toString() + ".";
        if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getNoSolution())){
            RIGHT_ANSWER = "The lines are parallel.";

            ANSWER_B = "These lines are the same.";
            ANSWER_C = LineMath.getRandomPoint().toString();
            ANSWER_D = LineMath.getRandomPoint().toString();
        }
        else {
            if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getInfiniteManySolution())){
                RIGHT_ANSWER = "These lines are the same.";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = LineMath.getRandomPoint().toString();
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
            else {
                SOLE.getSolution();
                RIGHT_ANSWER = "(" + SOLE.getXSolution() + ", " + SOLE.getYSolution() + ")";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = "These lines are the same.";
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  STANDARD FORM vs SLOPE INTERCEPT FORM
    private void createProblem_05(){
        Line FirstLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
        Line SecondLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations("x", "y", FirstLine.getACoefficient(), FirstLine.getBCoefficient(), FirstLine.getCCoefficient(),
                SecondLine.getACoefficient(), SecondLine.getBCoefficient(), SecondLine.getCCoefficient());

        QUESTION = "Find the intersection point of the two lines: <br>" + FirstLine.toString() + " and " + LineMath.getStandardToSlopeIntercept(SecondLine).toString() + ".";
        if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getNoSolution())){
            RIGHT_ANSWER = "The lines are parallel.";

            ANSWER_B = "These lines are the same.";
            ANSWER_C = LineMath.getRandomPoint().toString();
            ANSWER_D = LineMath.getRandomPoint().toString();
        }
        else {
            if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getInfiniteManySolution())){
                RIGHT_ANSWER = "These lines are the same.";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = LineMath.getRandomPoint().toString();
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
            else {
                SOLE.getSolution();
                RIGHT_ANSWER = "(" + SOLE.getXSolution() + ", " + SOLE.getYSolution() + ")";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = "These lines are the same.";
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  POINT SLOPE FORM vs STANDARD FORM
    private void createProblem_06(){
        Line FirstLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
        Line SecondLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations("x", "y", FirstLine.getACoefficient(), FirstLine.getBCoefficient(), FirstLine.getCCoefficient(),
                SecondLine.getACoefficient(), SecondLine.getBCoefficient(), SecondLine.getCCoefficient());

        QUESTION = "Find the intersection point of the two lines: <br>" + LineMath.getStandardToPointSlope(FirstLine).toString() + " and " + SecondLine.toString() + ".";
        if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getNoSolution())){
            RIGHT_ANSWER = "The lines are parallel.";

            ANSWER_B = "These lines are the same.";
            ANSWER_C = LineMath.getRandomPoint().toString();
            ANSWER_D = LineMath.getRandomPoint().toString();
        }
        else {
            if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getInfiniteManySolution())){
                RIGHT_ANSWER = "These lines are the same.";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = LineMath.getRandomPoint().toString();
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
            else {
                SOLE.getSolution();
                RIGHT_ANSWER = "(" + SOLE.getXSolution() + ", " + SOLE.getYSolution() + ")";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = "These lines are the same.";
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  POINT SLOPE FORM vs SLOPE INTERCEPT FORM
    private void createProblem_07(){
        Line FirstLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
        Line SecondLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations("x", "y", FirstLine.getACoefficient(), FirstLine.getBCoefficient(), FirstLine.getCCoefficient(),
                SecondLine.getACoefficient(), SecondLine.getBCoefficient(), SecondLine.getCCoefficient());

        QUESTION = "Find the intersection point of the two lines: <br>" + LineMath.getStandardToPointSlope(FirstLine).toString() + " and " + LineMath.getStandardToSlopeIntercept(SecondLine).toString() + ".";
        if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getNoSolution())){
            RIGHT_ANSWER = "The lines are parallel.";

            ANSWER_B = "These lines are the same.";
            ANSWER_C = LineMath.getRandomPoint().toString();
            ANSWER_D = LineMath.getRandomPoint().toString();
        }
        else {
            if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getInfiniteManySolution())){
                RIGHT_ANSWER = "These lines are the same.";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = LineMath.getRandomPoint().toString();
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
            else {
                SOLE.getSolution();
                RIGHT_ANSWER = "(" + SOLE.getXSolution() + ", " + SOLE.getYSolution() + ")";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = "These lines are the same.";
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  SLOPE INTERCEPT FORM vs POINT SLOPE FORM
    private void createProblem_08(){
        Line FirstLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
        Line SecondLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations("x", "y", FirstLine.getACoefficient(), FirstLine.getBCoefficient(), FirstLine.getCCoefficient(),
                SecondLine.getACoefficient(), SecondLine.getBCoefficient(), SecondLine.getCCoefficient());

        QUESTION = "Find the intersection point of the two lines: <br>" + LineMath.getStandardToSlopeIntercept(FirstLine).toString() + " and " + LineMath.getStandardToPointSlope(SecondLine).toString() + ".";
        if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getNoSolution())){
            RIGHT_ANSWER = "The lines are parallel.";

            ANSWER_B = "These lines are the same.";
            ANSWER_C = LineMath.getRandomPoint().toString();
            ANSWER_D = LineMath.getRandomPoint().toString();
        }
        else {
            if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getInfiniteManySolution())){
                RIGHT_ANSWER = "These lines are the same.";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = LineMath.getRandomPoint().toString();
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
            else {
                SOLE.getSolution();
                RIGHT_ANSWER = "(" + SOLE.getXSolution() + ", " + SOLE.getYSolution() + ")";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = "These lines are the same.";
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  SLOPE INTERCEPT FORM vs SLOPE INTERCEPT FORM
    private void createProblem_09(){
        Line FirstLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
        Line SecondLine = new Line(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations("x", "y", FirstLine.getACoefficient(), FirstLine.getBCoefficient(), FirstLine.getCCoefficient(),
                SecondLine.getACoefficient(), SecondLine.getBCoefficient(), SecondLine.getCCoefficient());

        QUESTION = "Find the intersection point of the two lines: <br>" + LineMath.getStandardToSlopeIntercept(FirstLine).toString() + " and " + LineMath.getStandardToSlopeIntercept(SecondLine).toString() + ".";
        if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getNoSolution())){
            RIGHT_ANSWER = "The lines are parallel.";

            ANSWER_B = "These lines are the same.";
            ANSWER_C = LineMath.getRandomPoint().toString();
            ANSWER_D = LineMath.getRandomPoint().toString();
        }
        else {
            if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getInfiniteManySolution())){
                RIGHT_ANSWER = "These lines are the same.";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = LineMath.getRandomPoint().toString();
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
            else {
                SOLE.getSolution();
                RIGHT_ANSWER = "(" + SOLE.getXSolution() + ", " + SOLE.getYSolution() + ")";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = "These lines are the same.";
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
