package pckMath.pckProblemCollection.pckAlgebraOne.pckAlgebraicExpression;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.Variable;

/**
 * Created by Cong on 7/6/2017.
 */

public class DividingWithVariable extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public DividingWithVariable(){
        createProblem();
        swapAnswer();
    }

    public DividingWithVariable(String Question,
                                             String RightAnswer,
                                             String AnswerA,
                                             String AnswerB,
                                             String AnswerC,
                                             String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }

        return new DividingWithVariable(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  NUMBER / VARIABLE = NUMBER
    private void createProblem_01(){
        Variable _Variable = AlgebraMath.getRandomVariable();
        AlgebraicNumber _Number = AlgebraMath.getRandomAlgebraicNumber();

        AlgebraicNumber Result = AlgebraMath.mul(_Variable, _Number);

        QUESTION = Result.toString() + InfoCollector.getDivisionSign() + _Variable + " = ?";
        RIGHT_ANSWER = _Number.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = _Variable.toString();
        ANSWER_C = AlgebraMath.mul(_Number, _Number).toString();
        ANSWER_D = AlgebraMath.mul(AlgebraMath.getRandomVariable(), AlgebraMath.getRandomVariable()).toString();
    }

    //  NUMBER / NUMBER = VARIABLE
    private void createProblem_02(){
        Variable _Variable = AlgebraMath.getRandomVariable();
        AlgebraicNumber _Number = AlgebraMath.getRandomAlgebraicNumber();

        AlgebraicNumber Result = AlgebraMath.mul(_Variable, _Number);

        QUESTION = Result.toString() + InfoCollector.getDivisionSign() + _Number + " = ?";
        RIGHT_ANSWER = _Variable.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = _Number.toString();
        ANSWER_C = AlgebraMath.mul(_Number, _Number).toString();
        ANSWER_D = AlgebraMath.mul(AlgebraMath.getRandomVariable(), AlgebraMath.getRandomVariable()).toString();
    }

    //  NUMBER / NUMBER = NUMBER
    private void createProblem_03(){
        AlgebraicNumber Number_1 = AlgebraMath.getRandomAlgebraicNumberTwoVariable(MathUtility.getVariable(), MathUtility.getVariable());
        AlgebraicNumber Number_2 = AlgebraMath.getRandomAlgebraicNumberTwoVariable(MathUtility.getVariable(), MathUtility.getVariable());

        AlgebraicNumber Result = AlgebraMath.mul(Number_1, Number_2);

        QUESTION = Result.toString() + InfoCollector.getDivisionSign() + Number_1 + " = ?";
        RIGHT_ANSWER = Number_2.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = Number_1.toString();
        ANSWER_C = AlgebraMath.mul(Number_1, Number_2).toString();
        ANSWER_D = AlgebraMath.mul(AlgebraMath.getRandomVariable(), AlgebraMath.getRandomVariable()).toString();
    }

    //  NUMBER / NUMBER = NUMBER
    private void createProblem_04(){
        AlgebraicNumber Number_1 = AlgebraMath.getRandomAlgebraicNumberTwoVariable("x", "y");
        AlgebraicNumber Number_2 = AlgebraMath.getRandomAlgebraicNumberTwoVariable("x", "y");

        AlgebraicNumber Result = AlgebraMath.mul(Number_1, Number_2);

        QUESTION = Result.toString() + InfoCollector.getDivisionSign() + Number_1 + " = ?";
        RIGHT_ANSWER = Number_2.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = Number_1.toString();
        ANSWER_C = AlgebraMath.mul(Number_1, Number_2).toString();
        ANSWER_D = AlgebraMath.mul(AlgebraMath.getRandomVariable(), AlgebraMath.getRandomVariable()).toString();
    }

    //  NUMBER / NUMBER = NUMBER
    private void createProblem_05(){
        AlgebraicNumber Number_1 = AlgebraMath.getRandomAlgebraicNumberTwoVariable(MathUtility.getVariable(), MathUtility.getVariable());
        AlgebraicNumber Number_2 = AlgebraMath.getRandomAlgebraicNumber(MathUtility.getVariable());

        AlgebraicNumber Result = AlgebraMath.mul(Number_1, Number_2);

        QUESTION = Result.toString() + InfoCollector.getDivisionSign() + Number_1 + " = ?";
        RIGHT_ANSWER = Number_2.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = Number_1.toString();
        ANSWER_C = AlgebraMath.mul(Number_1, Number_2).toString();
        ANSWER_D = AlgebraMath.mul(AlgebraMath.getRandomVariable(), AlgebraMath.getRandomVariable()).toString();
    }

    //  NUMBER / NUMBER = NUMBER
    private void createProblem_06(){
        AlgebraicNumber Number_1 = AlgebraMath.getRandomAlgebraicNumber(MathUtility.getVariable());
        AlgebraicNumber Number_2 = AlgebraMath.getRandomAlgebraicNumberTwoVariable(MathUtility.getVariable(), MathUtility.getVariable());

        AlgebraicNumber Result = AlgebraMath.mul(Number_1, Number_2);

        QUESTION = Result.toString() + InfoCollector.getDivisionSign() + Number_1 + " = ?";
        RIGHT_ANSWER = Number_2.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = Number_1.toString();
        ANSWER_C = AlgebraMath.mul(Number_1, Number_2).toString();
        ANSWER_D = AlgebraMath.mul(AlgebraMath.getRandomVariable(), AlgebraMath.getRandomVariable()).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
