package pckMath.pckProblemCollection.pckAlgebraOne.pckAlgebraicExpression;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicExpression;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;

/**
 * Created by Cong on 6/25/2017.
 */

public class EvaluateAlgebraicExpression extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public EvaluateAlgebraicExpression(){
        createProblem();
        swapAnswer();
    }

    public EvaluateAlgebraicExpression(String Question,
                                            String RightAnswer,
                                            String AnswerA,
                                            String AnswerB,
                                            String AnswerC,
                                            String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 4;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            default:
                createProblem_01();
                break;
        }
        return new EvaluateAlgebraicExpression(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        AlgebraicExpression Expression = AlgebraMath.getRandomAlgebraicExpressionTwoNumbers("x", "y");
        double Result = MathUtility.round_0_Decimal(Expression.evaluate());

        QUESTION = "Evaluate expression: " + Expression.toString() + "<br>"
                + Expression.getNumber(0).getVariable(0).getName() + " = " + Expression.getNumber(0).getVariable(0).getValue() + "<br>"
                + Expression.getNumber(1).getVariable(0).getName() + " = " + Expression.getNumber(1).getVariable(0).getValue() + ".";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_0_Decimal(Result + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_0_Decimal(Result - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_0_Decimal(Result + 15.0));
    }

    private void createProblem_02(){
        AlgebraicExpression Expression = AlgebraMath.getRandomAlgebraicExpressionThreeNumbers("x", "y", "z");
        double Result = MathUtility.round_0_Decimal(Expression.evaluate());

        QUESTION = "Evaluate expression: " + Expression.toString() + "<br>"
                + Expression.getNumber(0).getVariable(0).getName() + " = " + Expression.getNumber(0).getVariable(0).getValue() + "<br>"
                + Expression.getNumber(1).getVariable(0).getName() + " = " + Expression.getNumber(1).getVariable(0).getValue() + "<br>"
                + Expression.getNumber(2).getVariable(0).getName() + " = " + Expression.getNumber(2).getVariable(0).getValue() + ".";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_0_Decimal(Result + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_0_Decimal(Result - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_0_Decimal(Result + 15.0));
    }

    private void createProblem_03(){
        AlgebraicNumber Number = AlgebraMath.getRandomAlgebraicNumber();
        double Result = MathUtility.round_0_Decimal(Number.evaluate());

        QUESTION = "Find the value of: " + Number.toString() + "<br>"
                + Number.getVariable(0).getName() + " = " + Number.getVariable(0).getValue() + ".";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_0_Decimal(Result + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_0_Decimal(Result - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_0_Decimal(Result + 15.0));

    }

    private void createProblem_04(){
        AlgebraicNumber Number = AlgebraMath.getRandomAlgebraicNumberTwoVariable("x", "y");
        double Result = MathUtility.round_0_Decimal(Number.evaluate());

        QUESTION = "Find the value of: " + Number.toString() + "<br>"
                + Number.getVariable(0).getName() + " = " + Number.getVariable(0).getValue() + "<br>"
                + Number.getVariable(1).getName() + " = " + Number.getVariable(1).getValue() + ".";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_0_Decimal(Result + 10.0));
        ANSWER_C = String.valueOf(MathUtility.round_0_Decimal(Result - 10.0));
        ANSWER_D = String.valueOf(MathUtility.round_0_Decimal(Result + 15.0));

    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }


}
