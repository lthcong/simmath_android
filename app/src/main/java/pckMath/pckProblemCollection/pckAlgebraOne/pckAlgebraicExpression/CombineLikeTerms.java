package pckMath.pckProblemCollection.pckAlgebraOne.pckAlgebraicExpression;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicExpression;

/**
 * Created by Cong on 6/28/2017.
 */

public class CombineLikeTerms extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public CombineLikeTerms(){
        createProblem();
        swapAnswer();
    }

    public CombineLikeTerms(String Question,
                                       String RightAnswer,
                                       String AnswerA,
                                       String AnswerB,
                                       String AnswerC,
                                       String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 7;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            default:
                createProblem_01();
                break;
        }
        return new CombineLikeTerms(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        AlgebraicExpression Expression = AlgebraMath.getRandomAlgebraicExpressionTwoVariables("x", "y");
        AlgebraicExpression Result = AlgebraMath.combineLikeTerms(Expression);

        QUESTION = "Combine like terms: <br>" + Expression.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomAlgebraicExpressionThreeNumbers("x", "x", "y").toString();
        ANSWER_C = AlgebraMath.getRandomAlgebraicExpressionTwoNumbers("x", "y").toString();
        ANSWER_D = AlgebraMath.getRandomAlgebraicExpressionThreeNumbers("x", "x", "y").toString();
    }

    private void createProblem_02(){
        AlgebraicExpression Expression = AlgebraMath.getRandomAlgebraicExpressionThreeNumbers("x", "x", "x");
        AlgebraicExpression Result = AlgebraMath.combineLikeTerms(Expression);

        QUESTION = "Combine like terms: <br>" + Expression.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomAlgebraicExpressionThreeNumbers("x", "x", "y").toString();
        ANSWER_C = AlgebraMath.getRandomAlgebraicExpressionTwoNumbers("x", "y").toString();
        ANSWER_D = AlgebraMath.getRandomAlgebraicExpressionThreeNumbers("x", "x", "y").toString();
    }

    private void createProblem_03(){
        AlgebraicExpression Expression = AlgebraMath.getRandomAlgebraicExpressionFourNumbers("x", "y", "x", "x");
        AlgebraicExpression Result = AlgebraMath.combineLikeTerms(Expression);

        QUESTION = "Combine like terms: <br>" + Expression.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomAlgebraicExpressionThreeNumbers("x", "x", "y").toString();
        ANSWER_C = AlgebraMath.getRandomAlgebraicExpressionTwoNumbers("x", "y").toString();
        ANSWER_D = AlgebraMath.getRandomAlgebraicExpressionThreeNumbers("x", "x", "y").toString();
    }

    private void createProblem_04(){
        AlgebraicExpression Expression = AlgebraMath.getRandomAlgebraicExpressionFourNumbers("x", "x", "y", "x");
        AlgebraicExpression Result = AlgebraMath.combineLikeTerms(Expression);

        QUESTION = "Combine like terms: <br>" + Expression.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomAlgebraicExpressionThreeNumbers("x", "x", "y").toString();
        ANSWER_C = AlgebraMath.getRandomAlgebraicExpressionTwoNumbers("x", "y").toString();
        ANSWER_D = AlgebraMath.getRandomAlgebraicExpressionThreeNumbers("x", "x", "y").toString();
    }

    private void createProblem_05(){
        AlgebraicExpression Expression = AlgebraMath.getRandomAlgebraicExpressionFourNumbers("x", "x", "y", "x");
        AlgebraicExpression Result = AlgebraMath.combineLikeTerms(Expression);

        QUESTION = "Combine like terms: <br>" + Expression.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomAlgebraicExpressionThreeNumbers("x", "x", "y").toString();
        ANSWER_C = AlgebraMath.getRandomAlgebraicExpressionTwoNumbers("x", "y").toString();
        ANSWER_D = AlgebraMath.getRandomAlgebraicExpressionThreeNumbers("x", "x", "y").toString();
    }

    private void createProblem_06(){
        AlgebraicExpression Expression = AlgebraMath.getRandomAlgebraicExpressionFourNumbers("x", "y", "y", "x");
        AlgebraicExpression Result = AlgebraMath.combineLikeTerms(Expression);

        QUESTION = "Combine like terms: <br>" + Expression.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomAlgebraicExpressionThreeNumbers("x", "x", "y").toString();
        ANSWER_C = AlgebraMath.getRandomAlgebraicExpressionTwoNumbers("x", "y").toString();
        ANSWER_D = AlgebraMath.getRandomAlgebraicExpressionThreeNumbers("x", "x", "y").toString();
    }

    private void createProblem_07(){
        AlgebraicExpression Expression = AlgebraMath.getRandomAlgebraicExpressionFourNumbers("x", "x", "x", "x");
        AlgebraicExpression Result = AlgebraMath.combineLikeTerms(Expression);

        QUESTION = "Combine like terms: <br>" + Expression.toString();
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = AlgebraMath.getRandomAlgebraicExpressionThreeNumbers("x", "x", "y").toString();
        ANSWER_C = AlgebraMath.getRandomAlgebraicExpressionTwoNumbers("x", "y").toString();
        ANSWER_D = AlgebraMath.getRandomAlgebraicExpressionThreeNumbers("x", "x", "y").toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
