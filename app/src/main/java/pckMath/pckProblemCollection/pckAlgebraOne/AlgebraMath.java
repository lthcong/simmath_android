package pckMath.pckProblemCollection.pckAlgebraOne;

import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRationalPolynomial.RationalPolynomial;

/**
 * Created by Cong on 6/26/2017.
 */

public class AlgebraMath {

    public static Variable getRandomVariable(){
        int Exponent = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 1;
        return new Variable(MathUtility.getVariable(), Exponent, MathUtility.getRandomNumber_1Digit());
    }

    public static Variable getRandomVariable(String Name){
        int Exponent = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 1;
        return new Variable(Name, Exponent, MathUtility.getRandomNumber_1Digit());
    }

    public static Variable getRandomVariable(String Name, double ValueOfVariable){
        int Exponent = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 1;
        return new Variable(Name, Exponent, ValueOfVariable);
    }

    public static AlgebraicNumber getRandomAlgebraicNumber(){
        AlgebraicNumber RandomAlgebraicNumber = new AlgebraicNumber();
        RandomAlgebraicNumber.setCoefficient(MathUtility.getRandomNumber_1Digit());
        RandomAlgebraicNumber.addVariable(getRandomVariable());

        return RandomAlgebraicNumber;
    }

    public static AlgebraicNumber getRandomAlgebraicNumber(String VariableName){
        AlgebraicNumber RandomAlgebraicNumber = new AlgebraicNumber();
        RandomAlgebraicNumber.setCoefficient(MathUtility.getRandomNumber_1Digit());
        RandomAlgebraicNumber.addVariable(getRandomVariable(VariableName));

        return RandomAlgebraicNumber;
    }

    public static AlgebraicNumber getRandomAlgebraicNumberTwoVariable(String FirstVariable, String SecondVariable){
        AlgebraicNumber RandomAlgebraicNumber = new AlgebraicNumber();
        RandomAlgebraicNumber.setCoefficient(MathUtility.getRandomNumber_1Digit());
        RandomAlgebraicNumber.addVariable(getRandomVariable(FirstVariable));
        RandomAlgebraicNumber.addVariable(getRandomVariable(SecondVariable));

        return RandomAlgebraicNumber;
    }

    public static AlgebraicNumber getRandomAlgebraicNumber(String FirstVariable, String SecondVariable){
        AlgebraicNumber RandomAlgebraicNumber = new AlgebraicNumber();
        RandomAlgebraicNumber.setCoefficient(MathUtility.getRandomNumber_1Digit());
        RandomAlgebraicNumber.addVariable(getRandomVariable(FirstVariable));
        RandomAlgebraicNumber.addVariable(getRandomVariable(SecondVariable));

        return RandomAlgebraicNumber;
    }

    public static AlgebraicExpression getRandomAlgebraicExpressionTwoVariables(String FirstVariable, String SecondVariable){
        AlgebraicExpression RandomExpression = new AlgebraicExpression();

        RandomExpression.addNumber(getRandomAlgebraicNumberTwoVariable(FirstVariable, SecondVariable));
        RandomExpression.addNumber(getRandomAlgebraicNumberTwoVariable(FirstVariable, SecondVariable));
        RandomExpression.addNumber(getRandomAlgebraicNumberTwoVariable(FirstVariable, SecondVariable));
        RandomExpression.addNumber(getRandomAlgebraicNumberTwoVariable(FirstVariable, SecondVariable));

        return RandomExpression;
    }

    public static AlgebraicExpression getRandomAlgebraicExpression(){
        AlgebraicExpression RandomExpression = new AlgebraicExpression();
        RandomExpression.addNumber(getRandomAlgebraicNumber());
        RandomExpression.addNumber(getRandomAlgebraicNumber());

        return RandomExpression;
    }

    public static AlgebraicExpression getRandomAlgebraicExpression(String VariableName){
        AlgebraicExpression RandomExpression = new AlgebraicExpression();
        RandomExpression.addNumber(getRandomAlgebraicNumber(VariableName));
        RandomExpression.addNumber(getRandomAlgebraicNumber(VariableName));

        return RandomExpression;
    }

    public static AlgebraicExpression getRandomAlgebraicExpressionTwoNumbers(){
        AlgebraicExpression RandomExpression = new AlgebraicExpression();
        RandomExpression.addNumber(getRandomAlgebraicNumber());
        RandomExpression.addNumber(getRandomAlgebraicNumber());

        return RandomExpression;
    }

    public static AlgebraicExpression getRandomAlgebraicExpressionTwoNumbers(String FirstVariable, String SecondVariable){
        AlgebraicExpression RandomExpression = new AlgebraicExpression();
        RandomExpression.addNumber(getRandomAlgebraicNumber(FirstVariable));
        RandomExpression.addNumber(getRandomAlgebraicNumber(SecondVariable));

        return RandomExpression;
    }

    public static AlgebraicExpression getRandomAlgebraicExpressionThreeNumbers(){
        AlgebraicExpression RandomExpression = new AlgebraicExpression();
        RandomExpression.addNumber(getRandomAlgebraicNumber());
        RandomExpression.addNumber(getRandomAlgebraicNumber());
        RandomExpression.addNumber(getRandomAlgebraicNumber());

        return RandomExpression;
    }

    public static AlgebraicExpression getRandomAlgebraicExpressionThreeNumbers(String FirstVariable, String SecondVariable, String ThirdVariable){
        AlgebraicExpression RandomExpression = new AlgebraicExpression();
        RandomExpression.addNumber(getRandomAlgebraicNumber(FirstVariable));
        RandomExpression.addNumber(getRandomAlgebraicNumber(SecondVariable));
        RandomExpression.addNumber(getRandomAlgebraicNumber(ThirdVariable));

        return RandomExpression;
    }

    public static AlgebraicExpression getRandomAlgebraicExpressionFourNumbers(String FirstVariable, String SecondVariable, String ThirdVariable, String ForthVariable){
        AlgebraicExpression RandomExpression = new AlgebraicExpression();
        RandomExpression.addNumber(getRandomAlgebraicNumber(FirstVariable));
        RandomExpression.addNumber(getRandomAlgebraicNumber(SecondVariable));
        RandomExpression.addNumber(getRandomAlgebraicNumber(ThirdVariable));
        RandomExpression.addNumber(getRandomAlgebraicNumber(ForthVariable));

        return RandomExpression;
    }

    public static AlgebraicExpression combineLikeTerms(AlgebraicExpression _Expression){
        AlgebraicExpression CombinedExpression = new AlgebraicExpression();
        CombinedExpression.addNumber(_Expression.getNumberList());

        for (int i = 0; i < CombinedExpression.getNumberList().size() - 1; i++){
            for (int j = i + 1; j < CombinedExpression.getNumberList().size(); j++){
                AlgebraicNumber Temp_1 = CombinedExpression.getNumber(i);
                AlgebraicNumber Temp_2 = CombinedExpression.getNumber(j);

                if (Temp_1.isLikeTerms(Temp_2)){
                    AlgebraicNumber Temp = new AlgebraicNumber();
                    Temp.setCoefficient(Temp_1.getCoefficient() + Temp_2.getCoefficient());
                    Temp.addVariable(Temp_1.getVariableList());

                    CombinedExpression.addNumber(Temp);
                    CombinedExpression.getNumberList().remove(j);
                    CombinedExpression.getNumberList().remove(i);

                    i = -1;
                    break;
                }
            }
        }

        return CombinedExpression;
    }

    public static Polynomial getRandomPolynomial(String VariableName){
        Polynomial RandomPolynomial = new Polynomial();

        Variable _Var_1 = new Variable(VariableName, (MathUtility.getRandomPositiveNumber_1Digit() % 3 + 1));
        AlgebraicNumber _Number_1 = new AlgebraicNumber();
        _Number_1.setCoefficient(MathUtility.getRandomNumber_1Digit());
        _Number_1.addVariable(_Var_1);

        Variable _Var_2 = new Variable(VariableName, (MathUtility.getRandomPositiveNumber_1Digit() % 3 + 1));
        AlgebraicNumber _Number_2 = new AlgebraicNumber();
        _Number_2.setCoefficient(MathUtility.getRandomNumber_1Digit());
        _Number_2.addVariable(_Var_2);

        Variable _Var_3 = new Variable(VariableName, (MathUtility.getRandomPositiveNumber_1Digit() % 3 + 1));
        AlgebraicNumber _Number_3 = new AlgebraicNumber();
        _Number_3.setCoefficient(MathUtility.getRandomNumber_1Digit());
        _Number_3.addVariable(_Var_3);

        Variable _Var_4 = new Variable(VariableName, (MathUtility.getRandomPositiveNumber_1Digit() % 3 + 1));
        AlgebraicNumber _Number_4 = new AlgebraicNumber();
        _Number_4.setCoefficient(MathUtility.getRandomNumber_1Digit());
        _Number_4.addVariable(_Var_4);

        AlgebraicExpression RandomExpression = new AlgebraicExpression();
        RandomExpression.addNumber(_Number_1);
        RandomExpression.addNumber(_Number_2);
        RandomExpression.addNumber(_Number_3);
        RandomExpression.addNumber(_Number_4);

        RandomExpression = combineLikeTerms(RandomExpression);

        RandomPolynomial.setExpression(RandomExpression);

        return RandomPolynomial;
    }

    public static Polynomial getRandomPolynomial(String VariableName, int NoNumber){
        Polynomial RandomPolynomial = new Polynomial();
        AlgebraicExpression RandomExpression = new AlgebraicExpression();

        for (int i = 0; i < NoNumber; i++){
            Variable _Var = new Variable(VariableName, (MathUtility.getRandomPositiveNumber_1Digit() % 3 + 1));
            AlgebraicNumber _Number = new AlgebraicNumber();
            _Number.setCoefficient(MathUtility.getRandomNumber_1Digit());
            _Number.addVariable(_Var);

            RandomExpression.addNumber(_Number);
        }

        RandomExpression = combineLikeTerms(RandomExpression);

        RandomPolynomial.setExpression(RandomExpression);

        return RandomPolynomial;
    }

    public static Polynomial getRandomPolynomialWithHighestExponent(String VariableName, int HighestExponent){
        Polynomial RandomPolynomial = new Polynomial();

        Variable _Var_1 = new Variable(VariableName, HighestExponent);
        AlgebraicNumber _Number_1 = new AlgebraicNumber();
        _Number_1.setCoefficient(MathUtility.getRandomNumber_1Digit());
        _Number_1.addVariable(_Var_1);

        Variable _Var_2 = new Variable(VariableName, HighestExponent - 1);
        AlgebraicNumber _Number_2 = new AlgebraicNumber();
        _Number_2.setCoefficient(MathUtility.getRandomNumber_1Digit());
        _Number_2.addVariable(_Var_2);

        AlgebraicExpression RandomExpression = new AlgebraicExpression();
        RandomExpression.addNumber(_Number_1);
        RandomExpression.addNumber(_Number_2);

        RandomExpression = combineLikeTerms(RandomExpression);

        RandomPolynomial.setExpression(RandomExpression);

        return RandomPolynomial;
    }

    public static RationalPolynomial getRandomRationalPolynomial(){
        String VarName = MathUtility.getVariable();

        RationalPolynomial RandomPolynomial = new RationalPolynomial();
        RandomPolynomial.setTopPolynomial(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1));
        RandomPolynomial.setBottomPolynomial(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1));

        return RandomPolynomial;
    }

    public static RationalPolynomial getRandomRationalPolynomial(String VarName){
        RationalPolynomial RandomPolynomial = new RationalPolynomial();
        RandomPolynomial.setTopPolynomial(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1));
        RandomPolynomial.setBottomPolynomial(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1));

        return RandomPolynomial;
    }

    public static RationalPolynomial reverseRationalPolynomial(RationalPolynomial _Polynomial){
        RationalPolynomial ReversedPolynomial = new RationalPolynomial();
        ReversedPolynomial.setTopPolynomial(_Polynomial.getBottomPolynomial());
        ReversedPolynomial.setBottomPolynomial(_Polynomial.getTopPolynomial());

        return ReversedPolynomial;
    }

    public static AlgebraicExpression add(AlgebraicNumber FirstNumber, AlgebraicNumber SecondNumber){
        AlgebraicExpression Sum = new AlgebraicExpression();

        if (FirstNumber.isLikeTerms(SecondNumber)){
            AlgebraicNumber Temp = new AlgebraicNumber();
            Temp.setCoefficient(FirstNumber.getCoefficient() + SecondNumber.getCoefficient());
            Temp.addVariable(FirstNumber.getVariableList());

            Sum.addNumber(Temp);
        }
        else {
            Sum.addNumber(FirstNumber);
            Sum.addNumber(SecondNumber);
        }

        return Sum;
    }

    public static Polynomial add(Polynomial FirstPolynomial, int Number){
        Polynomial Sum = new Polynomial();

        AlgebraicExpression SumExpression = new AlgebraicExpression();
        SumExpression.addNumber(FirstPolynomial.getExpression().getNumberList());

        AlgebraicNumber Temp = new AlgebraicNumber();
        Temp.setCoefficient(Number);
        Temp.addVariable(new Variable("x", 0));

        AlgebraicExpression TempExp = new AlgebraicExpression();
        TempExp.addNumber(Temp);

        Polynomial SecondPolynomial = new Polynomial();
        SecondPolynomial.setExpression(TempExp);

        SumExpression.addNumber(SecondPolynomial.getExpression().getNumberList());
        SumExpression = combineLikeTerms(SumExpression);

        Sum.setExpression(SumExpression);

        return Sum;
    }

    public static Polynomial add(Polynomial FirstPolynomial, Polynomial SecondPolynomial){
        Polynomial Sum = new Polynomial();

        AlgebraicExpression SumExpression = new AlgebraicExpression();
        SumExpression.addNumber(FirstPolynomial.getExpression().getNumberList());
        SumExpression.addNumber(SecondPolynomial.getExpression().getNumberList());
        SumExpression = combineLikeTerms(SumExpression);

        Sum.setExpression(SumExpression);

        return Sum;
    }

    public static RationalPolynomial add(RationalPolynomial FirstPolynomial, RationalPolynomial SecondPolynomial){
        RationalPolynomial Sum = new RationalPolynomial();

        Polynomial TopPolynomial = AlgebraMath.add(
                AlgebraMath.mul(FirstPolynomial.getTopPolynomial(), SecondPolynomial.getBottomPolynomial()),
                AlgebraMath.mul(SecondPolynomial.getTopPolynomial(), FirstPolynomial.getBottomPolynomial()));
        Polynomial BottomPolynomial = AlgebraMath.mul(FirstPolynomial.getBottomPolynomial(), SecondPolynomial.getBottomPolynomial());

        Sum.setTopPolynomial(TopPolynomial);
        Sum.setBottomPolynomial(BottomPolynomial);

        return Sum;
    }

    public static AlgebraicExpression sub(AlgebraicNumber FirstNumber, AlgebraicNumber SecondNumber){
        AlgebraicExpression Sum = new AlgebraicExpression();

        if (FirstNumber.isLikeTerms(SecondNumber)){
            AlgebraicNumber Temp = new AlgebraicNumber();
            Temp.setCoefficient(FirstNumber.getCoefficient() - SecondNumber.getCoefficient());
            Temp.addVariable(FirstNumber.getVariableList());

            Sum.addNumber(Temp);
        }
        else {
            Sum.addNumber(FirstNumber);

            SecondNumber.setCoefficient(0 - SecondNumber.getCoefficient());
            Sum.addNumber(SecondNumber);
        }

        return Sum;
    }

    public static Polynomial sub(Polynomial FirstPolynomial, Polynomial SecondPolynomial){
        Polynomial Diff = new Polynomial();

        AlgebraicExpression DiffExpression = new AlgebraicExpression();

        //  1ST POL
        DiffExpression.addNumber(FirstPolynomial.getExpression().getNumberList());

        //  2ND POL
        AlgebraicExpression SecondExpression = new AlgebraicExpression();
        int size = SecondPolynomial.getExpression().getNumberList().size();
        for (int i = 0; i < size; i++){
            AlgebraicNumber Temp = new AlgebraicNumber();
            Temp.setCoefficient(0 - SecondPolynomial.getExpression().getNumberList().get(i).getCoefficient());
            Temp.addVariable(SecondPolynomial.getExpression().getNumberList().get(i).getVariableList());
            SecondExpression.addNumber(Temp);
        }
        DiffExpression.addNumber(SecondExpression.getNumberList());
        
        DiffExpression = combineLikeTerms(DiffExpression);

        Diff.setExpression(DiffExpression);

        return Diff;
    }

    public static RationalPolynomial sub(RationalPolynomial FirstPolynomial, RationalPolynomial SecondPolynomial){
        RationalPolynomial Sum = new RationalPolynomial();

        Polynomial TopPolynomial = AlgebraMath.sub(
                AlgebraMath.mul(FirstPolynomial.getTopPolynomial(), SecondPolynomial.getBottomPolynomial()),
                AlgebraMath.mul(SecondPolynomial.getTopPolynomial(), FirstPolynomial.getBottomPolynomial()));
        Polynomial BottomPolynomial = AlgebraMath.mul(FirstPolynomial.getBottomPolynomial(), SecondPolynomial.getBottomPolynomial());

        Sum.setTopPolynomial(TopPolynomial);
        Sum.setBottomPolynomial(BottomPolynomial);

        return Sum;
    }

    public static AlgebraicNumber mul(Variable FirstVariable, Variable SecondVariable){
        AlgebraicNumber Result = new AlgebraicNumber();

        //  COEFFICIENT
        Result.setCoefficient(1);

        //  VARIABLE
        Result.addVariable(FirstVariable);
        Result.addVariable(SecondVariable);

        return Result;
    }

    public static AlgebraicNumber mul(Variable _Variable, int _Number){
        AlgebraicNumber Product = new AlgebraicNumber();
        Product.setCoefficient(_Number);
        Product.addVariable(_Variable);

        return Product;
    }

    public static AlgebraicNumber mul(Variable _Variable, AlgebraicNumber _AlgebraicNumber){
        AlgebraicNumber Result = new AlgebraicNumber();

        //  COEFFICIENT
        Result.setCoefficient(_AlgebraicNumber.getCoefficient());

        //  VARIABLE
        Result.addVariable(_AlgebraicNumber.getVariableList());
        Result.addVariable(_Variable);

        return Result;
    }

    public static AlgebraicNumber mul(AlgebraicNumber _AlgebraicNumber, Variable _Variable){
        return mul(_Variable, _AlgebraicNumber);
    }

    public static AlgebraicNumber mul(AlgebraicNumber _AlgebraicNumber, int _Number){
        AlgebraicNumber Product = new AlgebraicNumber();
        Product.setCoefficient(_Number * _AlgebraicNumber.getCoefficient());
        Product.addVariable(_AlgebraicNumber.getVariableList());

        return Product;
    }

    public static AlgebraicNumber mul(AlgebraicNumber FirstNumber, AlgebraicNumber SecondNumber){
        AlgebraicNumber Result = new AlgebraicNumber();

        Result.setCoefficient(FirstNumber.getCoefficient() * SecondNumber.getCoefficient());
        Result.addVariable(FirstNumber.getVariableList());
        Result.addVariable(SecondNumber.getVariableList());

        return Result;
    }

    public static RationalPolynomial mul(RationalPolynomial FirstPolynomial, RationalPolynomial SecondPolynomial){
        RationalPolynomial Sum = new RationalPolynomial();

        Polynomial TopPolynomial = AlgebraMath.mul(FirstPolynomial.getTopPolynomial(), SecondPolynomial.getBottomPolynomial());
        Polynomial BottomPolynomial = AlgebraMath.mul(FirstPolynomial.getBottomPolynomial(), SecondPolynomial.getBottomPolynomial());

        Sum.setTopPolynomial(TopPolynomial);
        Sum.setBottomPolynomial(BottomPolynomial);

        return Sum;
    }

    public static Polynomial mul(Polynomial _Polynomial, int _Number){
        Polynomial Product = new Polynomial();
        int size = _Polynomial.getExpression().getNumberList().size();
        for (int i = 0; i < size; i++){
            Product.getExpression().addNumber(mul(_Polynomial.getExpression().getNumber(i), _Number));
        }

        return Product;
    }

    public static Polynomial mul(Polynomial FirstPolynomial, Polynomial SecondPolynomial){
        Polynomial Product = new Polynomial();

        AlgebraicExpression ProductExpression = new AlgebraicExpression();

        int Fsize = FirstPolynomial.getExpression().getNumberList().size();
        for (int i = 0; i < Fsize; i++){
            int Ssize = SecondPolynomial.getExpression().getNumberList().size();
            for (int j = 0; j < Ssize; j++){
                AlgebraicNumber Temp = mul(FirstPolynomial.getExpression().getNumberList().get(i),
                        SecondPolynomial.getExpression().getNumberList().get(j));
                ProductExpression.addNumber(Temp);
            }
        }
        ProductExpression = combineLikeTerms(ProductExpression);

        Product.setExpression(ProductExpression);

        return Product;
    }
}
