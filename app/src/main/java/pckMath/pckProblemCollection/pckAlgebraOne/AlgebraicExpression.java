package pckMath.pckProblemCollection.pckAlgebraOne;

import java.util.ArrayList;

import pckMath.MathUtility;

/**
 * Created by Cong on 6/27/2017.
 */

public class AlgebraicExpression {

    private ArrayList<AlgebraicNumber> NUMBER_LIST;

    public AlgebraicExpression(){
        NUMBER_LIST = new ArrayList<>();
    }

    public void addNumber(AlgebraicNumber _Number){
        NUMBER_LIST.add(_Number);
    }

    public void addNumber(ArrayList<AlgebraicNumber> _Number){
        NUMBER_LIST.addAll(MathUtility.convertToAlgebraicNumberArrayList(MathUtility.convertToAlgebraicNumberArray(_Number)));
    }

    public void setNumber(int Index, AlgebraicNumber _Number){
        NUMBER_LIST.set(Index, _Number);
    }

    public ArrayList<AlgebraicNumber> getNumberList(){
        return NUMBER_LIST;
    }

    public AlgebraicNumber getNumber(int Index){
        return NUMBER_LIST.get(Index);
    }

    public double evaluate(){
        double ExpressionValue = 0.0;
        int size = NUMBER_LIST.size();
        for (int i = 0; i < size; i++){
            ExpressionValue = ExpressionValue + NUMBER_LIST.get(i).evaluate();
        }

        return ExpressionValue;
    }

    public String toString(){
        String ExpressionString = "";
        int size = NUMBER_LIST.size();
        for (int i = 0; i < size; i++){
            if (!NUMBER_LIST.get(i).isNull()){
                ExpressionString += NUMBER_LIST.get(i).toString() + " + ";
            }
        }

        if (ExpressionString.trim().length() > 0){
            ExpressionString = ExpressionString.trim().substring(0, ExpressionString.length() - 2).trim();
        }
        else {
            ExpressionString = "0";
        }

        return ExpressionString;
    }

    public boolean isNull(){
        boolean NullExpression = false;
        if (NUMBER_LIST.size() == 0){
            NullExpression = true;
        }
        else {
            int size = NUMBER_LIST.size();
            for (int i = 0; i < size; i++){
                if (!NUMBER_LIST.get(i).isNull()){
                    NullExpression = false;
                    break;
                }
            }
        }

        return NullExpression;
    }

}
