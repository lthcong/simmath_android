package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckCubic.pckEquation;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckCubic.pckFunction.CubicFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckLinearFunction.LinearFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckFunction.QuadraticFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.LinearEquation;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 8/2/2017.
 */

public class FindingTheIntersectionsOfCubicFunctionByEquation extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingTheIntersectionsOfCubicFunctionByEquation(){
        createProblem();
        swapAnswer();
    }

    public FindingTheIntersectionsOfCubicFunctionByEquation(String Question,
                                String RightAnswer,
                                String AnswerA,
                                String AnswerB,
                                String AnswerC,
                                String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 9;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FindingTheIntersectionsOfCubicFunctionByEquation(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  CUBIC vs LINEAR: 3 intersections
    private void createProblem_01(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolC = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        Polynomial Pol = AlgebraMath.mul(AlgebraMath.mul(PolA, PolB), PolC);
        CubicEquation Equation = new CubicEquation(Pol);

        String Result = (new LinearEquation(PolA)).getSolution() + ", "
                + (new LinearEquation(PolB)).getSolution() + ", "
                + (new LinearEquation(PolC)).getSolution();

        int A = Equation.getACoefficient();
        int B = Equation.getBCoefficient();
        int C = Equation.getCCoefficient();
        int D = Equation.getDCoefficient();

        int C1 = MathUtility.getRandomNumber_1Digit();
        int D1 = MathUtility.getRandomNumber_1Digit();

        int C2 = C1 - C;
        int D2 = D1 - D;

        CubicFunction FunctionA = new CubicFunction(A, B, C1, D1);
        LinearFunction FunctionB = new LinearFunction(C2, D2);

        QUESTION = FunctionA.toString() + " and <br> " + FunctionB.toString();
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "x = " + FractionMath.getRandomFraction().toString()
                + ", x = " + FractionMath.getRandomUnitFraction().toString()
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  CUBIC vs LINEAR: 2 intersections
    private void createProblem_02(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        Polynomial Pol = AlgebraMath.mul(AlgebraMath.mul(PolA, PolB), PolA);
        CubicEquation Equation = new CubicEquation(Pol);

        String Result = (new LinearEquation(PolA)).getSolution() + ", "
                + (new LinearEquation(PolB)).getSolution();

        int A = Equation.getACoefficient();
        int B = Equation.getBCoefficient();
        int C = Equation.getCCoefficient();
        int D = Equation.getDCoefficient();

        int C1 = MathUtility.getRandomNumber_1Digit();
        int D1 = MathUtility.getRandomNumber_1Digit();

        int C2 = C1 - C;
        int D2 = D1 - D;

        CubicFunction FunctionA = new CubicFunction(A, B, C1, D1);
        LinearFunction FunctionB = new LinearFunction(C2, D2);

        QUESTION = FunctionA.toString() + " and <br> " + FunctionB.toString();
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  CUBIC vs LINEAR: 1 intersections
    private void createProblem_03(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        Polynomial Pol = AlgebraMath.mul(AlgebraMath.mul(PolA, PolA), PolA);
        CubicEquation Equation = new CubicEquation(Pol);

        String Result = (new LinearEquation(PolA)).getSolution();

        int A = Equation.getACoefficient();
        int B = Equation.getBCoefficient();
        int C = Equation.getCCoefficient();
        int D = Equation.getDCoefficient();

        int C1 = MathUtility.getRandomNumber_1Digit();
        int D1 = MathUtility.getRandomNumber_1Digit();

        int C2 = C1 - C;
        int D2 = D1 - D;

        CubicFunction FunctionA = new CubicFunction(A, B, C1, D1);
        LinearFunction FunctionB = new LinearFunction(C2, D2);

        QUESTION = FunctionA.toString() + " and <br> " + FunctionB.toString();
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  CUBIC vs QUADRATIC: 3 intersections
    private void createProblem_04(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolC = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        Polynomial Pol = AlgebraMath.mul(AlgebraMath.mul(PolA, PolB), PolC);
        CubicEquation Equation = new CubicEquation(Pol);

        String Result = (new LinearEquation(PolA)).getSolution() + ", "
                + (new LinearEquation(PolB)).getSolution() + ", "
                + (new LinearEquation(PolC)).getSolution();

        int A = Equation.getACoefficient();
        int B = Equation.getBCoefficient();
        int C = Equation.getCCoefficient();
        int D = Equation.getDCoefficient();

        int B1 = MathUtility.getRandomNumber_1Digit();
        int C1 = MathUtility.getRandomNumber_1Digit();
        int D1 = MathUtility.getRandomNumber_1Digit();

        int B2 = B1 - B;
        int C2 = C1 - C;
        int D2 = D1 - D;

        CubicFunction FunctionA = new CubicFunction(A, B1, C1, D1);
        QuadraticFunction FunctionB = new QuadraticFunction(B2, C2, D2);

        QUESTION = FunctionA.toString() + " and <br> " + FunctionB.toString();
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "x = " + FractionMath.getRandomFraction().toString()
                + ", x = " + FractionMath.getRandomUnitFraction().toString()
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  CUBIC vs QUADRATIC: 2 intersections
    private void createProblem_05(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        Polynomial Pol = AlgebraMath.mul(AlgebraMath.mul(PolA, PolB), PolB);
        CubicEquation Equation = new CubicEquation(Pol);

        String Result = (new LinearEquation(PolA)).getSolution() + ", "
                + (new LinearEquation(PolB)).getSolution();

        int A = Equation.getACoefficient();
        int B = Equation.getBCoefficient();
        int C = Equation.getCCoefficient();
        int D = Equation.getDCoefficient();

        int B1 = MathUtility.getRandomNumber_1Digit();
        int C1 = MathUtility.getRandomNumber_1Digit();
        int D1 = MathUtility.getRandomNumber_1Digit();

        int B2 = B1 - B;
        int C2 = C1 - C;
        int D2 = D1 - D;

        CubicFunction FunctionA = new CubicFunction(A, B1, C1, D1);
        QuadraticFunction FunctionB = new QuadraticFunction(B2, C2, D2);

        QUESTION = FunctionA.toString() + " and <br> " + FunctionB.toString();
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "x = " + FractionMath.getRandomFraction().toString()
                + ", x = " + FractionMath.getRandomUnitFraction().toString()
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  CUBIC vs QUADRATIC: 1 intersections
    private void createProblem_06(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        Polynomial Pol = AlgebraMath.mul(AlgebraMath.mul(PolA, PolA), PolA);
        CubicEquation Equation = new CubicEquation(Pol);

        String Result = (new LinearEquation(PolA)).getSolution();

        int A = Equation.getACoefficient();
        int B = Equation.getBCoefficient();
        int C = Equation.getCCoefficient();
        int D = Equation.getDCoefficient();

        int B1 = MathUtility.getRandomNumber_1Digit();
        int C1 = MathUtility.getRandomNumber_1Digit();
        int D1 = MathUtility.getRandomNumber_1Digit();

        int B2 = B1 - B;
        int C2 = C1 - C;
        int D2 = D1 - D;

        CubicFunction FunctionA = new CubicFunction(A, B1, C1, D1);
        QuadraticFunction FunctionB = new QuadraticFunction(B2, C2, D2);

        QUESTION = FunctionA.toString() + " and <br> " + FunctionB.toString();
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "x = " + FractionMath.getRandomFraction().toString()
                + ", x = " + FractionMath.getRandomUnitFraction().toString();
    }

    //  CUBIC vs CUBIC: 3 intersections
    private void createProblem_07(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolC = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        Polynomial Pol = AlgebraMath.mul(AlgebraMath.mul(PolA, PolB), PolC);
        CubicEquation Equation = new CubicEquation(Pol);

        String Result = (new LinearEquation(PolA)).getSolution() + ", "
                + (new LinearEquation(PolB)).getSolution() + ", "
                + (new LinearEquation(PolC)).getSolution();

        int A = Equation.getACoefficient();
        int B = Equation.getBCoefficient();
        int C = Equation.getCCoefficient();
        int D = Equation.getDCoefficient();

        int A1 = MathUtility.getRandomNumber_1Digit();
        int B1 = MathUtility.getRandomNumber_1Digit();
        int C1 = MathUtility.getRandomNumber_1Digit();
        int D1 = MathUtility.getRandomNumber_1Digit();

        int A2 = A1 - A;
        int B2 = B1 - B;
        int C2 = C1 - C;
        int D2 = D1 - D;

        CubicFunction FunctionA = new CubicFunction(A1, B1, C1, D1);
        CubicFunction FunctionB = new CubicFunction(A2, B2, C2, D2);

        QUESTION = FunctionA.toString() + " and <br> " + FunctionB.toString();
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "x = " + FractionMath.getRandomFraction().toString()
                + ", x = " + FractionMath.getRandomUnitFraction().toString()
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  CUBIC vs CUBIC: 2 intersections
    private void createProblem_08(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        Polynomial Pol = AlgebraMath.mul(AlgebraMath.mul(PolA, PolB), PolB);
        CubicEquation Equation = new CubicEquation(Pol);

        String Result = (new LinearEquation(PolA)).getSolution() + ", "
                + (new LinearEquation(PolB)).getSolution();

        int A = Equation.getACoefficient();
        int B = Equation.getBCoefficient();
        int C = Equation.getCCoefficient();
        int D = Equation.getDCoefficient();

        int A1 = MathUtility.getRandomNumber_1Digit();
        int B1 = MathUtility.getRandomNumber_1Digit();
        int C1 = MathUtility.getRandomNumber_1Digit();
        int D1 = MathUtility.getRandomNumber_1Digit();

        int A2 = A1 - A;
        int B2 = B1 - B;
        int C2 = C1 - C;
        int D2 = D1 - D;

        CubicFunction FunctionA = new CubicFunction(A1, B1, C1, D1);
        CubicFunction FunctionB = new CubicFunction(A2, B2, C2, D2);

        QUESTION = FunctionA.toString() + " and <br> " + FunctionB.toString();
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "x = " + FractionMath.getRandomFraction().toString()
                + ", x = " + FractionMath.getRandomUnitFraction().toString()
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  CUBIC vs CUBIC: 1 intersections
    private void createProblem_09(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        Polynomial Pol = AlgebraMath.mul(AlgebraMath.mul(PolA, PolA), PolA);
        CubicEquation Equation = new CubicEquation(Pol);

        String Result = (new LinearEquation(PolA)).getSolution();

        int A = Equation.getACoefficient();
        int B = Equation.getBCoefficient();
        int C = Equation.getCCoefficient();
        int D = Equation.getDCoefficient();

        int A1 = MathUtility.getRandomNumber_1Digit();
        int B1 = MathUtility.getRandomNumber_1Digit();
        int C1 = MathUtility.getRandomNumber_1Digit();
        int D1 = MathUtility.getRandomNumber_1Digit();

        int A2 = A1 - A;
        int B2 = B1 - B;
        int C2 = C1 - C;
        int D2 = D1 - D;

        CubicFunction FunctionA = new CubicFunction(A1, B1, C1, D1);
        CubicFunction FunctionB = new CubicFunction(A2, B2, C2, D2);

        QUESTION = FunctionA.toString() + " and <br> " + FunctionB.toString();
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "x = " + FractionMath.getRandomFraction().toString();
    }

    @Override
    public String getQuestion() {
        QUESTION = "Find the intersections of the graphs:<br> " + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
