package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckCubic.pckEquation;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.LinearEquation;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/26/2017.
 */

public class SolvingCubicEquation extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SolvingCubicEquation(){
        createProblem();
        swapAnswer();
    }

    public SolvingCubicEquation(String Question,
                                        String RightAnswer,
                                        String AnswerA,
                                        String AnswerB,
                                        String AnswerC,
                                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 3;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SolvingCubicEquation(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  (ax + b)(cx + d)(ex + f)
    private void createProblem_01(){
        String VarName = "x";
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial PolC = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        Polynomial Pol = AlgebraMath.mul(AlgebraMath.mul(PolA, PolB), PolC);

        LinearEquation EquationA = new LinearEquation(PolA);
        LinearEquation EquationB = new LinearEquation(PolB);
        LinearEquation EquationC = new LinearEquation(PolC);
        CubicEquation Equation = new CubicEquation(Pol);

        QUESTION = Equation.toString();
        RIGHT_ANSWER = EquationA.getSolution()+ ", " + EquationB.getSolution() + ", "
                + EquationC.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "x = " + FractionMath.getRandomFraction().toString()
                + ", x = " + FractionMath.getRandomUnitFraction().toString()
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  (ax + b)(ax + b)(ex + f)
    private void createProblem_02(){
        String VarName = "x";
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        Polynomial Pol = AlgebraMath.mul(AlgebraMath.mul(PolA, PolA), PolB);

        LinearEquation EquationA = new LinearEquation(PolA);
        LinearEquation EquationB = new LinearEquation(PolB);
        CubicEquation Equation = new CubicEquation(Pol);

        QUESTION = Equation.toString();
        RIGHT_ANSWER = EquationA.getSolution()+ ", " + EquationB.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString()
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit())
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "x = " + FractionMath.getRandomFraction().toString()
                + ", x = " + FractionMath.getRandomUnitFraction().toString()
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  (ax + b)(ax + b)(ax + b)
    private void createProblem_03(){
        String VarName = "x";
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        Polynomial Pol = AlgebraMath.mul(AlgebraMath.mul(PolA, PolA), PolA);

        LinearEquation EquationA = new LinearEquation(PolA);
        CubicEquation Equation = new CubicEquation(Pol);

        QUESTION = Equation.toString();
        RIGHT_ANSWER = EquationA.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString()
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "x = " + FractionMath.getRandomFraction().toString()
                + ", x = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    @Override
    public String getQuestion() {
        QUESTION = "Solve this equation:<br> " + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
