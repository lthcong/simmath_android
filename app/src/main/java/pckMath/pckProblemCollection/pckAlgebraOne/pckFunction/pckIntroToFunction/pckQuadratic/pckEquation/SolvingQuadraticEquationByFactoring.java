package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/25/2017.
 */

public class SolvingQuadraticEquationByFactoring extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SolvingQuadraticEquationByFactoring(){
        createProblem();
        swapAnswer();
    }

    public SolvingQuadraticEquationByFactoring(String Question,
                                               String RightAnswer,
                                               String AnswerA,
                                               String AnswerB,
                                               String AnswerC,
                                               String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SolvingQuadraticEquationByFactoring(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  a + b + c = 0
    private void createProblem_01(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = 0 - (a + b);

        QuadraticEquation Equation = new QuadraticEquation(a, b, c);
        String SolutionString = QuadraticMath.getSolution(Equation).toString();
        SolutionString = SolutionString.substring(1, SolutionString.length() - 1);

        QUESTION = Equation.toString();
        RIGHT_ANSWER = SolutionString;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + FractionMath.getRandomFraction().toString();
    }

    //  a - b + c = 0
    private void createProblem_02(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = 0 - (a - b);

        QuadraticEquation Equation = new QuadraticEquation(a, b, c);
        String SolutionString = QuadraticMath.getSolution(Equation).toString();
        SolutionString = SolutionString.substring(1, SolutionString.length() - 1);

        QUESTION = Equation.toString();
        RIGHT_ANSWER = SolutionString;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + FractionMath.getRandomFraction().toString();
    }

    //  (ax + b)(cx + d) = 0
    private void createProblem_03(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial Pol = AlgebraMath.mul(PolA, PolB);

        QuadraticEquation Equation = new QuadraticEquation(Pol);
        String SolutionString = QuadraticMath.getSolution(Equation).toString();
        SolutionString = SolutionString.substring(1, SolutionString.length() - 1);

        QUESTION = Equation.toString();
        RIGHT_ANSWER = SolutionString;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + FractionMath.getRandomFraction().toString();
    }

    //  (ax + b)^2 = 0
    private void createProblem_04(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial Pol = AlgebraMath.mul(PolA, PolA);

        QuadraticEquation Equation = new QuadraticEquation(Pol);
        String SolutionString = QuadraticMath.getSolution(Equation).toString();
        SolutionString = SolutionString.substring(1, SolutionString.length() - 1);

        QUESTION = Equation.toString();
        RIGHT_ANSWER = SolutionString;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + FractionMath.getRandomFraction().toString();
    }

    //  ax^2 + bx = 0
    private void createProblem_05(){
        QuadraticEquation Equation = new QuadraticEquation(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(), 0);
        String SolutionString = QuadraticMath.getSolution(Equation).toString();
        SolutionString = SolutionString.substring(1, SolutionString.length() - 1);

        QUESTION = Equation.toString();
        RIGHT_ANSWER = SolutionString;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + FractionMath.getRandomFraction().toString();
    }

    //  ax^2 + c = 0
    private void createProblem_06(){
        QuadraticEquation Equation = new QuadraticEquation(MathUtility.getRandomNumber_1Digit(), 0,
                MathUtility.getRandomNumber_1Digit());
        String SolutionString = QuadraticMath.getSolution(Equation).toString();
        SolutionString = SolutionString.substring(1, SolutionString.length() - 1);

        QUESTION = Equation.toString();
        RIGHT_ANSWER = SolutionString;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + FractionMath.getRandomFraction().toString();
    }

    @Override
    public String getQuestion() {
        QUESTION = "Solve this equation:<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
