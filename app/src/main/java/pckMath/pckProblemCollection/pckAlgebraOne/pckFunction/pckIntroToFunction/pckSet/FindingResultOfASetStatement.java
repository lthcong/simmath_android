package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckSet;

import java.util.ArrayList;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.OrderedPairs;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.Set;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.SetMath;

/**
 * Created by Cong on 7/20/2017.
 */

public class FindingResultOfASetStatement extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingResultOfASetStatement(){
        createProblem();
        swapAnswer();
    }

    public FindingResultOfASetStatement(String Question,
                                      String RightAnswer,
                                      String AnswerA,
                                      String AnswerB,
                                      String AnswerC,
                                      String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 14;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FindingResultOfASetStatement(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  ELEMENT
    private void createProblem_01(){
        Set RandomSet = SetMath.getRandomSet();
        int RandomElement = RandomSet.get(MathUtility.getRandomPositiveNumber_1Digit() % RandomSet.getSize());

        QUESTION = "x " + InfoCollector.getElementOf() + " A = " + RandomSet.toString() + "<br>. Find the x value.";
        RIGHT_ANSWER = String.valueOf(RandomElement);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = InfoCollector.getEmptySet();
        ANSWER_C = String.valueOf(MathUtility.getRandomPositiveNumber_1Digit() + 10);
        ANSWER_D = (new OrderedPairs(RandomSet.get(0), RandomSet.get(1))).toString();
    }

    //  SUB SET
    private void createProblem_02(){
        Set RandomSet = SetMath.getRandomSet();
        Set SubSet = SetMath.findSubSet(RandomSet);

        QUESTION = "A " + InfoCollector.getSubSet() + " B = " + RandomSet.toString() + ".<br>Find the set A.";
        RIGHT_ANSWER = "A = " + SubSet.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "A = " + InfoCollector.getEmptySet();
        ANSWER_C = "A = " + SetMath.getRandomSet().toString();
        ANSWER_D = "A = " + SetMath.getRandomSet().toString();
    }

    //  SUB SET
    private void createProblem_03(){
        Set RandomSet = SetMath.getRandomSet();
        Set SubSet = SetMath.findSubSet(RandomSet);

        QUESTION = "A = " + SubSet.toString() + ", B = " + RandomSet.toString() + ".<br>Find the true statement.";
        RIGHT_ANSWER = "A " + InfoCollector.getSubSet() + " B";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "A " + InfoCollector.getNotSubSet() + " B";
        ANSWER_C = "A " + InfoCollector.getIntersectionSet() + " B";
        ANSWER_D = "A " + InfoCollector.getUnionSet() + " B";
    }

    //  SUB SET
    private void createProblem_04(){
        Set RandomSet = SetMath.getRandomSet();
        Set SubSet = SetMath.findSubSet(RandomSet);

        QUESTION = "Given A " + InfoCollector.getSubSet() + " B.<br>A = " + SubSet.toString() + ". Find the set B." ;
        RIGHT_ANSWER = "B = " + RandomSet.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "B = " + SetMath.findSubSet(SubSet);
        ANSWER_C = "B = " + SetMath.getRandomSet();
        ANSWER_D = "B = " + SetMath.getRandomSet();
    }

    //  INTERSECTION
    private void createProblem_05(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set IntersectionSet = SetMath.findTheIntersection(SetA, SetB);

        QUESTION = "A = " + SetA.toString() + ", B = " + SetB.toString() + ".<br>Find A " + InfoCollector.getIntersectionSet() + " B = ?";
        RIGHT_ANSWER = IntersectionSet.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SetMath.findSubSet(SetA).toString();
        ANSWER_C = SetMath.findSubSet(SetB).toString();
        ANSWER_D = SetMath.findTheUnion(SetA, SetB).toString();
    }

    //  INTERSECTION
    private void createProblem_06(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set IntersectionSet = SetMath.findTheIntersection(SetA, SetB);

        QUESTION = "A = " + SetA.toString() + "<br> B = " + SetB.toString()
                + "<br>C = " + IntersectionSet.toString() + ". Find the true statement";
        RIGHT_ANSWER = "C = A" + InfoCollector.getIntersectionSet() + " B";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "C = A" + InfoCollector.getUnionSet() + " B";
        ANSWER_C = "C = A" + InfoCollector.getDifferentOfSet() + " B";
        ANSWER_D = "C = A" + InfoCollector.getNotSubSet() + " B";
    }

    //  INTERSECTION
    private void createProblem_07(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set IntersectionSet = SetMath.findTheIntersection(SetA, SetB);

        QUESTION = "C = A " + InfoCollector.getIntersectionSet() + " B.<br>"
                + "C = " + IntersectionSet.toString() + "<br>A = " + SetA.toString() + ". Find the set B.";
        RIGHT_ANSWER = "B = " + SetB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "B = " + SetA.toString();
        ANSWER_C = "B = " + SetMath.findTheUnion(SetA, SetB).toString();
        ANSWER_D = "B = " + SetMath.findTheDifferent(SetA, SetB).toString();
    }

    //  UNION
    private void createProblem_08(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set IntersectionSet = SetMath.findTheIntersection(SetA, SetB);

        QUESTION = "A = " + SetA.toString() + ", B = " + SetB.toString() + ".<br>Find A " + InfoCollector.getUnionSet() + " B = ?";
        RIGHT_ANSWER = IntersectionSet.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SetMath.findSubSet(SetA).toString();
        ANSWER_C = SetMath.findSubSet(SetB).toString();
        ANSWER_D = SetMath.findTheIntersection(SetA, SetB).toString();
    }

    //  UNION
    private void createProblem_09(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set UnionSet = SetMath.findTheUnion(SetA, SetB);

        QUESTION = "C = A " + InfoCollector.getUnionSet() + " B.<br>"
                + "C = " + UnionSet.toString() + "<br>A = " + SetA.toString() + ". Find the set B.";
        RIGHT_ANSWER = "B = " + SetB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "B = " + SetA.toString();
        ANSWER_C = "B = " + SetMath.findTheIntersection(SetA, SetB).toString();
        ANSWER_D = "B = " + SetMath.findTheDifferent(SetA, SetB).toString();
    }

    //  CARTESIAN PRODUCT
    private void createProblem_10(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        ArrayList<OrderedPairs> CartesianProduct = SetMath.findCartesianProduct(SetA, SetB);

        QUESTION = "A = " + SetA.toString() + ", B = " + SetB.toString() + ".<br>Find A x B";
        RIGHT_ANSWER = CartesianProduct.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SetMath.findCartesianProduct(SetA, SetA).toString();
        ANSWER_C = SetMath.findCartesianProduct(SetMath.findSubSet(SetA), SetMath.findSubSet(SetB)).toString();
        ANSWER_D = SetMath.findCartesianProduct(SetB, SetB).toString();
    }

    //  DIFFERENT
    private void createProblem_11(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set DifferentSet = SetMath.findTheDifferent(SetA, SetB);

        QUESTION = "A = " + SetA.toString() + ", B = " + SetB.toString() + ".<br>Find A " + InfoCollector.getDifferentOfSet() + " B = ?";
        RIGHT_ANSWER = DifferentSet.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SetMath.findSubSet(SetA).toString();
        ANSWER_C = SetMath.findSubSet(SetB).toString();
        ANSWER_D = SetMath.findTheIntersection(SetA, SetB).toString();
    }

    //  DIFFERENT
    private void createProblem_12(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set DifferentSet = SetMath.findTheDifferent(SetA, SetB);

        QUESTION = "A = " + SetA.toString()
                + "<br>B = " + SetB.toString()
                + "<br>C = " + DifferentSet.toString() + ". Find the true statement.";
        RIGHT_ANSWER = "C = A" + InfoCollector.getDifferentOfSet() + " B";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "C = A" + InfoCollector.getUnionSet() + " B";
        ANSWER_C = "C = A" + InfoCollector.getIntersectionSet() + " B";
        ANSWER_D = "C = A" + InfoCollector.getDivisionSign() + " B";
    }

    //  LIST ELEMENT
    private void createProblem_13(){
        int StartingElement = MathUtility.getRandomNumber_1Digit();
        Set SetA = new Set();
        SetA.add(StartingElement);
        SetA.add(StartingElement + 1);
        SetA.add(StartingElement + 2);
        SetA.add(StartingElement + 3);
        SetA.add(StartingElement + 4);

        QUESTION = "Given A = {x: "
                + String.valueOf(StartingElement) + InfoCollector.getLessThanOrEqualSign() + "x" + InfoCollector.getGreaterThanOrEqualSign() + String.valueOf(StartingElement + 4) + "}."
                + "<br>List all the element of set A.";
        RIGHT_ANSWER = SetA.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SetMath.getRandomSet().toString();
        ANSWER_C = SetMath.getRandomSet().toString();
        ANSWER_D = SetMath.getRandomSet().toString();
    }

    //  LIST ELEMENT
    private void createProblem_14(){
        int StartingElement = MathUtility.getRandomNumber_1Digit();
        Set SetA = new Set();
        SetA.add(StartingElement);
        SetA.add(StartingElement + 1);
        SetA.add(StartingElement + 2);
        SetA.add(StartingElement + 3);
        SetA.add(StartingElement + 4);

        QUESTION = "Given A = " + SetA.toString() + ".<br>Find the true statement.";
        RIGHT_ANSWER = "A = {x: "
                + String.valueOf(StartingElement) + InfoCollector.getLessThanOrEqualSign() + "x"
                + InfoCollector.getLessThanOrEqualSign() + String.valueOf(StartingElement + 4) + "}";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "A = {x: x" + InfoCollector.getGreaterThanOrEqualSign() + String.valueOf(StartingElement + 4) + "}";
        ANSWER_C = "A = {x: " + String.valueOf(StartingElement) + "< x < " + String.valueOf(StartingElement + 4) + "}";
        ANSWER_D = "A = {x: " + String.valueOf(StartingElement) + "< x}";
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
