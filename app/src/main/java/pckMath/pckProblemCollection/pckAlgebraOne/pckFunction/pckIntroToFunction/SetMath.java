package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction;

import java.util.ArrayList;

import pckMath.MathUtility;

/**
 * Created by Cong on 7/19/2017.
 */

public class SetMath {

    private static int NUMBER_OF_ELEMENT = 5;

    public static Set getRandomSet(){
        Set RandomSet = new Set();
        for (int i = 0; i < NUMBER_OF_ELEMENT; i++){
            RandomSet.add(MathUtility.getRandomPositiveNumber_1Digit());
        }

        return RandomSet;
    }

    public static Set getRandomSet(int NoElement){
        Set RandomSet = new Set();
        for (int i = 0; i < NoElement; i++){
            RandomSet.add(MathUtility.getRandomPositiveNumber_1Digit());
        }

        return RandomSet;
    }

    public static Set findSubSet(Set GivenSet){
        Set SubSet = new Set();
        int size = GivenSet.getSize();
        int SubSetSize = MathUtility.getRandomPositiveNumber_1Digit() % size + 1;
        for (int i = 0; i < SubSetSize; i++){
            SubSet.add(GivenSet.get(i));
        }

        return SubSet;
    }

    public static Set findComplement(Set GivenSet){
        Set ComplementSet = new Set();
        for (int i = 0; i < NUMBER_OF_ELEMENT; i++){
            while(true){
                int Temp = MathUtility.getRandomNumber_1Digit();
                if (!GivenSet.isContains(Temp)){
                    ComplementSet.add(Temp);
                    break;
                }
            }
        }

        return ComplementSet;
    }

    public static Set findComplement(Set SuperSet, Set SubSet){
        Set ComplementSet = new Set();
        for (int i = 0; i < SuperSet.getSize(); i++){
            int Temp = SuperSet.get(i);
            if (!SubSet.isContains(Temp)){
                ComplementSet.add(Temp);
            }
        }

        return ComplementSet;
    }

    public static Set findTheIntersection(Set SetA, Set SetB){
        Set IntersectionSet = new Set();
        IntersectionSet.add(SetA.getAll());
        IntersectionSet.add(SetB.getAll());

        return IntersectionSet;
    }

    public static Set findTheUnion(Set SetA, Set SetB){
        Set UnionSet = findTheIntersection(SetA, SetB);
        for (int i = 0; i < UnionSet.getSize(); i++){
            int Temp = UnionSet.get(i);
            if (SetA.isContains(Temp) && SetB.isContains(Temp)){
                UnionSet.remove(i);
            }
        }

        if (UnionSet.getSize() == 0){
            UnionSet.add(Integer.MIN_VALUE);
        }

        return UnionSet;
    }

    public static Set findTheDifferent(Set SetA, Set SetB){
        Set DifferentSet = new Set();
        int size = SetA.getSize();
        for (int i = 0; i < size; i++){
            int Temp = SetA.get(i);
            if (!SetB.isContains(Temp)){
                DifferentSet.add(Temp);
            }
        }

        if (DifferentSet.getSize() == 0){
            DifferentSet.add(Integer.MIN_VALUE);
        }

        return DifferentSet;
    }

    public static ArrayList<OrderedPairs> findCartesianProduct(Set SetA, Set SetB){
        ArrayList<OrderedPairs> CartesianProduct = new ArrayList<>();
        int sizeA = SetA.getSize();
        int sizeB = SetB.getSize();

        for (int i = 0; i < sizeA; i++){
            for (int j = 0; j < sizeB; j++){
                CartesianProduct.add(new OrderedPairs(SetA.get(i), SetB.get(j)));
            }
        }

        return CartesianProduct;
    }

    public static Interval convertInequalityToInterval(Inequality ConvertingInequality){
        return new Interval(ConvertingInequality.getEqualToLeftBound(), ConvertingInequality.getLeftBound(),
                ConvertingInequality.getRightBound(), ConvertingInequality.getEqualToRightBound());
    }

    public static Inequality convertIntervalToInequality(Interval ConvertingInterval){
        return new Inequality("x", ConvertingInterval.getSlowerBound(), ConvertingInterval.getEqualToSlowerBound(),
                ConvertingInterval.getEqualToUpperBound(), ConvertingInterval.getUpperBound());
    }
}
