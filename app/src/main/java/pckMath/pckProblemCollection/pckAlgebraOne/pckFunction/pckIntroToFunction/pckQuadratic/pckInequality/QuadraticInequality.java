package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckInequality;

import pckInfo.InfoCollector;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/26/2017.
 */

public class QuadraticInequality {

    /*  ax^2 + bx + c > 0*/

    private int A_COEFFICIENT, B_COEFFICIENT, C_COEFFICIENT;
    private String INEQUALITY_SIGN;

    public QuadraticInequality(){
        A_COEFFICIENT = 0;
        B_COEFFICIENT = 0;
        C_COEFFICIENT = 0;

        INEQUALITY_SIGN = ">";
    }

    public QuadraticInequality(int ACoefficient, int BCoefficient, int CCoefficient, String InequalitySign){
        A_COEFFICIENT = ACoefficient;
        B_COEFFICIENT = BCoefficient;
        C_COEFFICIENT = CCoefficient;

        INEQUALITY_SIGN = InequalitySign;
    }

    public QuadraticInequality(Polynomial _Polynomial, String InequalitySign){
        for (int i = 0; i < 3; i++) {
            AlgebraicNumber Temp = _Polynomial.getExpression().getNumber(i);
            switch (Temp.getVariable(0).getExponent()){
                case 0:
                    C_COEFFICIENT = Temp.getCoefficient();
                    break;
                case 1:
                    B_COEFFICIENT = Temp.getCoefficient();
                    break;
                case 2:
                    A_COEFFICIENT = Temp.getCoefficient();
                    break;
            }
        }

        INEQUALITY_SIGN = InequalitySign;
    }

    public void setACoefficient(int Coefficient){
        A_COEFFICIENT = Coefficient;
    }

    public int getACoefficient(){
        return A_COEFFICIENT;
    }

    public void setBCoefficient(int Coefficient){
        B_COEFFICIENT = Coefficient;
    }

    public int getBCoefficient(){
        return B_COEFFICIENT;
    }

    public void setCCoefficient(int Coefficient){
        C_COEFFICIENT = Coefficient;
    }

    public int getCCoefficient(){
        return C_COEFFICIENT;
    }

    public void setInequalitySign(String InequalitySign){
        INEQUALITY_SIGN = InequalitySign;
    }

    public String getInequalitySign(){
        return INEQUALITY_SIGN;
    }

    public String toString(){
        String EquationString;

        if (A_COEFFICIENT != 1){
            EquationString = String.valueOf(A_COEFFICIENT) + "x" + InfoCollector.putExponent(2);
        }
        else {
            EquationString = "x" + InfoCollector.putExponent(2);
        }

        if (B_COEFFICIENT != 0){
            if (B_COEFFICIENT != 1){
                EquationString += " + " + String.valueOf(B_COEFFICIENT) + "x";
            }
            else {
                EquationString += " + x";
            }
        }

        if (C_COEFFICIENT != 0){
            EquationString += " + " + String.valueOf(C_COEFFICIENT);
        }

        EquationString += INEQUALITY_SIGN + "0";

        return EquationString;
    }
}
