package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckInequality;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/26/2017.
 */

public class SolvingQuadraticInequality extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SolvingQuadraticInequality(){
        createProblem();
        swapAnswer();
    }

    public SolvingQuadraticInequality(String Question,
                                    String RightAnswer,
                                    String AnswerA,
                                    String AnswerB,
                                    String AnswerC,
                                    String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 8;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SolvingQuadraticInequality(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  (ax + b)(cx + d) > 0
    private void createProblem_01(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial Pol = AlgebraMath.mul(PolA, PolB);

        QuadraticEquation Equation = new QuadraticEquation(Pol);

        int a = Equation.getACoefficient();
        int b = Equation.getBCoefficient();
        int c = Equation.getCCoefficient();

        QuadraticInequality Inequality = new QuadraticInequality(a, b, c, " > ");

        String SolutionA = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(0));
        String SolutionB = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(1));

        QUESTION = Inequality.toString();
        if (a > 0) {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = "x " + InfoCollector.getElementOf() + "(" + InfoCollector.getInfinity() + ", " + SolutionA + ") "
                        + InfoCollector.getUnionSet()
                        + "(" + SolutionA + ", -" + InfoCollector.getInfinity() + ")";
            }
            else {
                RIGHT_ANSWER = "x < " + SolutionA + " or x > " + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionA + " < x < " + SolutionB;
            ANSWER_C = SolutionB + " < x < " + SolutionA;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
        else {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = InfoCollector.getNoSolution();
            }
            else {
                RIGHT_ANSWER = SolutionA + " < x < " + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionB + " < x < " + SolutionA;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
    }

    //  (ax + b)(cx + d) < 0
    private void createProblem_02(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial Pol = AlgebraMath.mul(PolA, PolB);

        QuadraticEquation Equation = new QuadraticEquation(Pol);

        int a = Equation.getACoefficient();
        int b = Equation.getBCoefficient();
        int c = Equation.getCCoefficient();

        QuadraticInequality Inequality = new QuadraticInequality(a, b, c, " < ");

        String SolutionA = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(0));
        String SolutionB = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(1));

        QUESTION = Inequality.toString();
        if (a < 0) {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = "x " + InfoCollector.getElementOf() + "(" + InfoCollector.getInfinity() + ", " + SolutionA + ") "
                        + InfoCollector.getUnionSet()
                        + "(" + SolutionA + ", -" + InfoCollector.getInfinity() + ")";
            }
            else {
                RIGHT_ANSWER = "x < " + SolutionA + " or x > " + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionA + " < x < " + SolutionB;
            ANSWER_C = SolutionB + " < x < " + SolutionA;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
        else {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = InfoCollector.getNoSolution();
            }
            else {
                RIGHT_ANSWER = SolutionA + " < x < " + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionB + " < x < " + SolutionA;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
    }

    //  (ax + b)(cx + d) >= 0
    private void createProblem_03(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial Pol = AlgebraMath.mul(PolA, PolB);

        QuadraticEquation Equation = new QuadraticEquation(Pol);

        int a = Equation.getACoefficient();
        int b = Equation.getBCoefficient();
        int c = Equation.getCCoefficient();

        QuadraticInequality Inequality = new QuadraticInequality(a, b, c, InfoCollector.getGreaterThanOrEqualSign());

        String SolutionA = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(0));
        String SolutionB = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(1));

        QUESTION = Inequality.toString();
        if (a > 0) {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = "x " + InfoCollector.getElementOf() + " (-" + InfoCollector.getInfinity() + ", " + InfoCollector.getInfinity() + ")";
            }
            else {
                RIGHT_ANSWER = "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA
                        + " or x" + InfoCollector.getGreaterThanOrEqualSign() + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA
                    + " or x" + InfoCollector.getLessThanOrEqualSign() + SolutionB;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
        else {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = InfoCollector.getNoSolution();
            }
            else {
                RIGHT_ANSWER = SolutionA + InfoCollector.getLessThanOrEqualSign() + "x" + InfoCollector.getLessThanOrEqualSign() + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionB + InfoCollector.getLessThanOrEqualSign() + "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
    }

    //  (ax + b)(cx + d) <= 0
    private void createProblem_04(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial Pol = AlgebraMath.mul(PolA, PolB);

        QuadraticEquation Equation = new QuadraticEquation(Pol);

        int a = Equation.getACoefficient();
        int b = Equation.getBCoefficient();
        int c = Equation.getCCoefficient();

        QuadraticInequality Inequality = new QuadraticInequality(a, b, c, InfoCollector.getLessThanOrEqualSign());

        String SolutionA = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(0));
        String SolutionB = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(1));

        QUESTION = Inequality.toString();
        if (a < 0) {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = InfoCollector.getNoSolution();
            }
            else {
                RIGHT_ANSWER = "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA
                        + " or x" + InfoCollector.getGreaterThanOrEqualSign() + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA
                    + " or x" + InfoCollector.getLessThanOrEqualSign() + SolutionB;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
        else {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = "x " + InfoCollector.getElementOf() + " (-" + InfoCollector.getInfinity() + ", " + InfoCollector.getInfinity() + ")";
            }
            else {
                RIGHT_ANSWER = SolutionA + InfoCollector.getLessThanOrEqualSign() + "x" + InfoCollector.getLessThanOrEqualSign() + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionB + InfoCollector.getLessThanOrEqualSign() + "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
    }

    //  QUADRATIC > LINEAR
    private void createProblem_05(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        if (PolA.toString().equalsIgnoreCase(PolB.toString())){
            createProblem_05();
        }

        Polynomial Pol = AlgebraMath.mul(PolA, PolB);

        QuadraticEquation Equation = new QuadraticEquation(Pol);

        int A = Equation.getACoefficient();
        int B = Equation.getBCoefficient();
        int C = Equation.getCCoefficient();

        int A1 = MathUtility.getRandomNumber_1Digit();
        int B1 = MathUtility.getRandomNumber_1Digit();
        int C1 = MathUtility.getRandomNumber_1Digit();

        int B2 = B1 - B;
        int C2 = C1 - C;

        String SolutionA = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(0));
        String SolutionB = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(1));

        QUESTION = String.valueOf(A1) + "x" + InfoCollector.putExponent(2) + " + " + String.valueOf(B1) + "x" + " + " + String.valueOf(C1) + " > "
                + String.valueOf(B2) + "x" + " + " + String.valueOf(C2);
        if (A > 0) {
            RIGHT_ANSWER = "x < " + SolutionA + " or x > " + SolutionB;

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionA + " < x < " + SolutionB;
            ANSWER_C = SolutionB + " < x < " + SolutionA;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
        else {
            RIGHT_ANSWER = SolutionA + " < x < " + SolutionB;

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionB + " < x < " + SolutionA;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
    }

    //  QUADRATIC < LINEAR
    private void createProblem_06(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        if (PolA.toString().equalsIgnoreCase(PolB.toString())){
            createProblem_06();
        }

        Polynomial Pol = AlgebraMath.mul(PolA, PolB);

        QuadraticEquation Equation = new QuadraticEquation(Pol);

        int A = Equation.getACoefficient();
        int B = Equation.getBCoefficient();
        int C = Equation.getCCoefficient();

        int A1 = MathUtility.getRandomNumber_1Digit();
        int B1 = MathUtility.getRandomNumber_1Digit();
        int C1 = MathUtility.getRandomNumber_1Digit();

        int B2 = B1 - B;
        int C2 = C1 - C;

        String SolutionA = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(0));
        String SolutionB = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(1));

        QUESTION = String.valueOf(A1) + "x" + InfoCollector.putExponent(2) + " + " + String.valueOf(B1) + "x" + " + " + String.valueOf(C1) + " < "
                + String.valueOf(B2) + "x" + " + " + String.valueOf(C2);
        if (A < 0) {
            RIGHT_ANSWER = "x < " + SolutionA + " or x > " + SolutionB;

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionA + " < x < " + SolutionB;
            ANSWER_C = SolutionB + " < x < " + SolutionA;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
        else {
            RIGHT_ANSWER = SolutionA + " < x < " + SolutionB;

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionB + " < x < " + SolutionA;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
    }

    //  QUADRATIC <= QUADRATIC
    private void createProblem_07(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        if (PolA.toString().equalsIgnoreCase(PolB.toString())){
            createProblem_07();
        }

        Polynomial Pol = AlgebraMath.mul(PolA, PolB);

        QuadraticEquation Equation = new QuadraticEquation(Pol);

        int A = Equation.getACoefficient();
        int B = Equation.getBCoefficient();
        int C = Equation.getCCoefficient();

        int A1 = MathUtility.getRandomNumber_1Digit();
        int B1 = MathUtility.getRandomNumber_1Digit();
        int C1 = MathUtility.getRandomNumber_1Digit();

        int A2 = A1 - A;
        int B2 = B1 - B;
        int C2 = C1 - C;

        String SolutionA = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(0));
        String SolutionB = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(1));

        QUESTION = String.valueOf(A1) + "x" + InfoCollector.putExponent(2) + " + " + String.valueOf(B1) + "x" + " + " + String.valueOf(C1) + InfoCollector.getLessThanOrEqualSign()
                + String.valueOf(A2) + "x" + InfoCollector.putExponent(2) + " + " + String.valueOf(B2) + "x" + " + " + String.valueOf(C2);
        if (A < 0) {
            RIGHT_ANSWER = "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA
                    + " or x" + InfoCollector.getGreaterThanOrEqualSign() + SolutionB;

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA
                    + " or x" + InfoCollector.getLessThanOrEqualSign() + SolutionB;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
        else {
            RIGHT_ANSWER = SolutionA + InfoCollector.getLessThanOrEqualSign() + "x" + InfoCollector.getLessThanOrEqualSign() + SolutionB;

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionB + InfoCollector.getLessThanOrEqualSign() + "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
    }

    //  QUADRATIC >= QUADRATIC
    private void createProblem_08(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        if (PolA.toString().equalsIgnoreCase(PolB.toString())){
            createProblem_07();
        }

        Polynomial Pol = AlgebraMath.mul(PolA, PolB);

        QuadraticEquation Equation = new QuadraticEquation(Pol);

        int A = Equation.getACoefficient();
        int B = Equation.getBCoefficient();
        int C = Equation.getCCoefficient();

        int A1 = MathUtility.getRandomNumber_1Digit();
        int B1 = MathUtility.getRandomNumber_1Digit();
        int C1 = MathUtility.getRandomNumber_1Digit();

        int A2 = A1 - A;
        int B2 = B1 - B;
        int C2 = C1 - C;

        String SolutionA = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(0));
        String SolutionB = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(1));

        QUESTION = String.valueOf(A1) + "x" + InfoCollector.putExponent(2) + " + " + String.valueOf(B1) + "x" + " + " + String.valueOf(C1) + InfoCollector.getGreaterThanOrEqualSign()
                + String.valueOf(A2) + "x" + InfoCollector.putExponent(2) + " + " + String.valueOf(B2) + "x" + " + " + String.valueOf(C2);
        if (A > 0) {
            RIGHT_ANSWER = "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA
                    + " or x" + InfoCollector.getGreaterThanOrEqualSign() + SolutionB;

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA
                    + " or x" + InfoCollector.getLessThanOrEqualSign() + SolutionB;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
        else {
            RIGHT_ANSWER = SolutionA + InfoCollector.getLessThanOrEqualSign() + "x" + InfoCollector.getLessThanOrEqualSign() + SolutionB;

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionB + InfoCollector.getLessThanOrEqualSign() + "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
    }

    @Override
    public String getQuestion() {
        QUESTION = "Solve this inequality:<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
