package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckFunction;

import pckInfo.InfoCollector;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicExpression;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.Variable;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.MathFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/23/2017.
 */

public class QuadraticFunction extends MathFunction{

    private int A_COEFFICIENT, B_COEFFICIENT, C_COEFFICIENT;

    public QuadraticFunction(){
        A_COEFFICIENT = 0;
        B_COEFFICIENT = 0;
        C_COEFFICIENT = 0;
    }
    
    public QuadraticFunction(int ACoefficient, int BCoefficient, int CCoefficient){
        A_COEFFICIENT = ACoefficient;
        B_COEFFICIENT = BCoefficient;
        C_COEFFICIENT = CCoefficient;
    }

    public QuadraticFunction(Polynomial _Polynomial){
        for (int i = 0; i < 3; i++) {
            AlgebraicNumber Temp = _Polynomial.getExpression().getNumber(i);
            switch (Temp.getVariable(0).getExponent()){
                case 0:
                    C_COEFFICIENT = Temp.getCoefficient();
                    break;
                case 1:
                    B_COEFFICIENT = Temp.getCoefficient();
                    break;
                case 2:
                    A_COEFFICIENT = Temp.getCoefficient();
                    break;
            }
        }
    }

    public QuadraticFunction(QuadraticEquation Equation){
        A_COEFFICIENT = Equation.getACoefficient();
        B_COEFFICIENT = Equation.getBCoefficient();
        C_COEFFICIENT = Equation.getCCoefficient();
    }

    public void setACoefficient(int Coefficient){
        A_COEFFICIENT = Coefficient;
    }
    
    public int getACoefficient(){
        return A_COEFFICIENT;
    }

    public void setBCoefficient(int Coefficient){
        B_COEFFICIENT = Coefficient;
    }

    public int getBCoefficient(){
        return B_COEFFICIENT;
    }

    public void setCCoefficient(int Coefficient){
        C_COEFFICIENT = Coefficient;
    }

    public int getCCoefficient(){
        return C_COEFFICIENT;
    }

    @Override
    public String toString(){
        String QuadraticString = "y = " + String.valueOf(A_COEFFICIENT) + "x" + InfoCollector.putExponent(2);

        if (B_COEFFICIENT != 0){
            QuadraticString += " + " + String.valueOf(B_COEFFICIENT) + "x";
        }

        if (C_COEFFICIENT != 0){
            QuadraticString += " + " + String.valueOf(C_COEFFICIENT);
        }

        return QuadraticString;
    }

    @Override
    public Polynomial getPolynomial(){
        AlgebraicNumber FirstNumber = new AlgebraicNumber();
        FirstNumber.setCoefficient(A_COEFFICIENT);
        FirstNumber.addVariable(new Variable("x", 2));

        AlgebraicNumber SecondNumber = new AlgebraicNumber();
        SecondNumber.setCoefficient(B_COEFFICIENT);
        SecondNumber.addVariable(new Variable("x", 1));

        AlgebraicNumber ThirdNumber = new AlgebraicNumber();
        ThirdNumber.setCoefficient(C_COEFFICIENT);
        ThirdNumber.addVariable(new Variable("x", 0));

        AlgebraicExpression Expression = new AlgebraicExpression();
        Expression.addNumber(FirstNumber);
        Expression.addNumber(SecondNumber);
        Expression.addNumber(ThirdNumber);

        Polynomial QuadraticPolynomial = new Polynomial();
        QuadraticPolynomial.setExpression(Expression);

        return QuadraticPolynomial;
    }
}
