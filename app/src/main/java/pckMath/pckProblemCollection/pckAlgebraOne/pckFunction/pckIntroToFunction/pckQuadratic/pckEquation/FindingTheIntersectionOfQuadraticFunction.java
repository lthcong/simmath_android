package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckLinearFunction.LinearFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckFunction.QuadraticFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/26/2017.
 */

public class FindingTheIntersectionOfQuadraticFunction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingTheIntersectionOfQuadraticFunction(){
        createProblem();
        swapAnswer();
    }

    public FindingTheIntersectionOfQuadraticFunction(String Question,
                                                     String RightAnswer,
                                                     String AnswerA,
                                                     String AnswerB,
                                                     String AnswerC,
                                                     String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 8;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FindingTheIntersectionOfQuadraticFunction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  QUADRATIC vs QUADRATIC
    private void createProblem_01(){
        int A = MathUtility.getRandomNumber_1Digit();
        int B = MathUtility.getRandomNumber_1Digit();
        int C = 0 - (A + B);
        
        int A1 = MathUtility.getRandomNumber_1Digit();
        int B1 = MathUtility.getRandomNumber_1Digit();
        int C1 = MathUtility.getRandomNumber_1Digit();
        
        int A2 = A + A1;
        int B2 = B + B1;
        int C2 = C + C1;

        QuadraticEquation EquationA = new QuadraticEquation(A1, B1, C1);
        QuadraticEquation EquationB = new QuadraticEquation(A2, B2, C2);
        QuadraticEquation Equation = new QuadraticEquation(A, B, C);

        String SolutionString = QuadraticMath.getSolution(Equation).toString();
        SolutionString = SolutionString.substring(1, SolutionString.length() - 1);

        QUESTION = "Find the intersection of the 2 functions:<br>"
                + (new QuadraticFunction(EquationA)).toString() + "<br>and "
                + (new QuadraticFunction(EquationB)).toString() + ".";
        RIGHT_ANSWER = SolutionString;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "They have no intersections.";
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + FractionMath.getRandomFraction().toString();
    }

    //  QUADRATIC vs QUADRATIC
    private void createProblem_02(){
        int A = MathUtility.getRandomNumber_1Digit();
        int B = MathUtility.getRandomNumber_1Digit();
        int C = 0 - (A - B);

        int A1 = MathUtility.getRandomNumber_1Digit();
        int B1 = MathUtility.getRandomNumber_1Digit();
        int C1 = MathUtility.getRandomNumber_1Digit();

        int A2 = A + A1;
        int B2 = B + B1;
        int C2 = C + C1;

        QuadraticEquation EquationA = new QuadraticEquation(A1, B1, C1);
        QuadraticEquation EquationB = new QuadraticEquation(A2, B2, C2);
        QuadraticEquation Equation = new QuadraticEquation(A, B, C);

        String SolutionString = QuadraticMath.getSolution(Equation).toString();
        SolutionString = SolutionString.substring(1, SolutionString.length() - 1);

        QUESTION = "Find the intersection of the 2 functions:<br>"
                + (new QuadraticFunction(EquationA)).toString() + "<br>and "
                + (new QuadraticFunction(EquationB)).toString() + ".";
        RIGHT_ANSWER = SolutionString;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "They have no intersections.";
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + FractionMath.getRandomFraction().toString();
    }

    //  QUADRATIC vs QUADRATIC
    private void createProblem_03(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial Pol = AlgebraMath.mul(PolA, PolB);

        QuadraticEquation Equation = new QuadraticEquation(Pol);

        int A = Equation.getACoefficient();
        int B = Equation.getBCoefficient();
        int C = Equation.getCCoefficient();

        int A1 = MathUtility.getRandomNumber_1Digit();
        int B1 = MathUtility.getRandomNumber_1Digit();
        int C1 = MathUtility.getRandomNumber_1Digit();

        int A2 = A + A1;
        int B2 = B + B1;
        int C2 = C + C1;

        QuadraticEquation EquationA = new QuadraticEquation(A1, B1, C1);
        QuadraticEquation EquationB = new QuadraticEquation(A2, B2, C2);

        String SolutionString = QuadraticMath.getSolution(Equation).toString();
        SolutionString = SolutionString.substring(1, SolutionString.length() - 1);

        QUESTION = "Find the intersection of the 2 functions:<br>"
                + (new QuadraticFunction(EquationA)).toString() + "<br>and "
                + (new QuadraticFunction(EquationB)).toString() + ".";
        RIGHT_ANSWER = SolutionString;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "They have no intersections.";
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + FractionMath.getRandomFraction().toString();
    }

    //  QUADRATIC vs LINEAR
    private void createProblem_04(){
        int A = MathUtility.getRandomNumber_1Digit();
        int B = MathUtility.getRandomNumber_1Digit();
        int C = 0 - (A + B);
        
        int B1 = MathUtility.getRandomNumber_1Digit();
        int C1 = MathUtility.getRandomNumber_1Digit();
        
        int B2 = B + B1;
        int C2 = C + C1;

        QuadraticEquation QEquation = new QuadraticEquation(A, B1, C1);
        LinearFunction LEquation = new LinearFunction(B2, C2);
        QuadraticEquation Equation = new QuadraticEquation(A, B, C);

        String SolutionString = QuadraticMath.getSolution(Equation).toString();
        SolutionString = SolutionString.substring(1, SolutionString.length() - 1);

        QUESTION = "Find the intersection of the 2 functions:<br>"
                + (new QuadraticFunction(QEquation)).toString() + "<br>and "
                + LEquation.toString() + ".";
        RIGHT_ANSWER = SolutionString;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "They have no intersections.";
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + FractionMath.getRandomFraction().toString();
    }

    //  QUADRATIC vs LINEAR
    private void createProblem_05(){
        int A = MathUtility.getRandomNumber_1Digit();
        int B = MathUtility.getRandomNumber_1Digit();
        int C = 0 - (A - B);

        int B1 = MathUtility.getRandomNumber_1Digit();
        int C1 = MathUtility.getRandomNumber_1Digit();

        int B2 = B + B1;
        int C2 = C + C1;

        QuadraticEquation QEquation = new QuadraticEquation(A, B1, C1);
        LinearFunction LEquation = new LinearFunction(B2, C2);
        QuadraticEquation Equation = new QuadraticEquation(A, B, C);

        String SolutionString = QuadraticMath.getSolution(Equation).toString();
        SolutionString = SolutionString.substring(1, SolutionString.length() - 1);

        QUESTION = "Find the intersection of the 2 functions:<br>"
                + (new QuadraticFunction(QEquation)).toString() + "<br>and "
                + LEquation.toString() + ".";
        RIGHT_ANSWER = SolutionString;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "They have no intersections.";
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + FractionMath.getRandomFraction().toString();
    }

    //  QUADRATIC vs LINEAR
    private void createProblem_06(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial Pol = AlgebraMath.mul(PolA, PolB);

        QuadraticEquation Equation = new QuadraticEquation(Pol);

        int A = Equation.getACoefficient();
        int B = Equation.getBCoefficient();
        int C = Equation.getCCoefficient();

        int A1 = MathUtility.getRandomNumber_1Digit();
        int B1 = MathUtility.getRandomNumber_1Digit();
        int C1 = MathUtility.getRandomNumber_1Digit();

        int B2 = B + B1;
        int C2 = C + C1;

        QuadraticEquation QEquation = new QuadraticEquation(A1, B1, C1);
        LinearFunction LEquation = new LinearFunction(B2, C2);

        String SolutionString = QuadraticMath.getSolution(Equation).toString();
        SolutionString = SolutionString.substring(1, SolutionString.length() - 1);

        QUESTION = "Find the intersection of the 2 functions:<br>"
                + (new QuadraticFunction(QEquation)).toString() + "<br>and "
                + LEquation.toString() + ".";
        RIGHT_ANSWER = SolutionString;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "They have no intersections.";
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + MathUtility.getRandomNumber_1Digit();
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + FractionMath.getRandomFraction().toString();
    }

    //  QUADRATIC vs QUADRATIC
    private void createProblem_07(){
        QuadraticEquation EquationA = new QuadraticEquation(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
        QuadraticEquation EquationB = new QuadraticEquation(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
        QuadraticEquation Equation = new QuadraticEquation(
                EquationA.getACoefficient() - EquationB.getACoefficient(),
                EquationA.getBCoefficient() - EquationB.getBCoefficient(),
                EquationA.getCCoefficient() - EquationB.getCCoefficient());

        QUESTION = "Find the intersection of the 2 functions:<br>"
                + (new QuadraticFunction(EquationA)).toString() + "<br>and "
                + (new QuadraticFunction(EquationB)).toString() + ".";
        if (!QuadraticMath.getSolution(Equation).toString().contains("i")){
            RIGHT_ANSWER = "They have no intersections.";

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + MathUtility.getRandomNumber_1Digit();
            ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + MathUtility.getRandomNumber_1Digit();
            ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + FractionMath.getRandomFraction().toString();
        }
        else {
            String SolutionString = QuadraticMath.getDecimalSolution(Equation).toString();
            SolutionString = SolutionString.substring(1, SolutionString.length() - 1);

            RIGHT_ANSWER = SolutionString;

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = "They have no intersections.";
            ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + MathUtility.getRandomNumber_1Digit();
            ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + FractionMath.getRandomFraction().toString();
        }

    }

    //  QUADRATIC vs LINEAR
    private void createProblem_08(){
        QuadraticEquation EquationA = new QuadraticEquation(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
        LinearFunction EquationB = new LinearFunction(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
        QuadraticEquation Equation = new QuadraticEquation(
                EquationA.getACoefficient(),
                EquationA.getBCoefficient() - EquationB.getSlope(),
                EquationA.getCCoefficient() - EquationB.getYIntercept());

        QUESTION = "Find the intersection of the 2 functions:<br>"
                + (new QuadraticFunction(EquationA)).toString() + "<br>and "
                + EquationB.toString() + ".";
        if (!QuadraticMath.getSolution(Equation).toString().contains("i")){
            RIGHT_ANSWER = "They have no intersections.";

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + MathUtility.getRandomNumber_1Digit();
            ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + MathUtility.getRandomNumber_1Digit();
            ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + FractionMath.getRandomFraction().toString();
        }
        else {
            String SolutionString = QuadraticMath.getDecimalSolution(Equation).toString();
            SolutionString = SolutionString.substring(1, SolutionString.length() - 1);

            RIGHT_ANSWER = SolutionString;

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = "They have no intersections.";
            ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", x = " + MathUtility.getRandomNumber_1Digit();
            ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", x = " + FractionMath.getRandomFraction().toString();
        }

    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
