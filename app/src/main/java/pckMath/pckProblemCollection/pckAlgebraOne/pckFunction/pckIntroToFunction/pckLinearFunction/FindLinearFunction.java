package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckLinearFunction;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.FunctionMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;

/**
 * Created by Cong on 7/23/2017.
 */

public class FindLinearFunction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindLinearFunction(){
        createProblem();
        swapAnswer();
    }

    public FindLinearFunction(String Question,
                                  String RightAnswer,
                                  String AnswerA,
                                  String AnswerB,
                                  String AnswerC,
                                  String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 5;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FindLinearFunction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  2 POINTS
    private void createProblem_01(){
        LinearFunction RandomFunction = FunctionMath.getRandomLinearFunction();

        int XCoordinateA = MathUtility.getRandomNumber_1Digit();
        Point PointA = FunctionMath.getRandomPoint(RandomFunction, XCoordinateA);

        int XCoordinateB = MathUtility.getRandomNumber_1Digit();
        Point PointB = FunctionMath.getRandomPoint(RandomFunction, XCoordinateB);

        QUESTION = "Find the function that has the graph pass through 2 points " + PointA.toString() + ", " + PointB.toString() + ".";
        RIGHT_ANSWER = RandomFunction.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  SLOPE + POINT
    private void createProblem_02(){
        LinearFunction RandomFunction = FunctionMath.getRandomLinearFunction();

        int XCoordinateA = MathUtility.getRandomNumber_1Digit();
        Point PointA = FunctionMath.getRandomPoint(RandomFunction, XCoordinateA);

        QUESTION = "Find the function that has the graph pass through the point " + PointA.toString()
                + ", and the slope is " + String.valueOf(RandomFunction.getSlope()) + ".";
        RIGHT_ANSWER = RandomFunction.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  SLOPE + INTERCEPT
    private void createProblem_03(){
        LinearFunction RandomFunction = FunctionMath.getRandomLinearFunction();
        Point Intercept = new Point(0, RandomFunction.getYIntercept());

        QUESTION = "Find the function given the Y-Intercept is " + Intercept.toString()
                + ", and the slope is " + String.valueOf(RandomFunction.getSlope()) + ".";
        RIGHT_ANSWER = RandomFunction.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  PARALLEL
    private void createProblem_04(){
        LinearFunction FunctionA = FunctionMath.getRandomLinearFunction();

        LinearFunction FunctionB = new LinearFunction(FunctionA.getSlope(), MathUtility.getRandomNumber_1Digit());
        Point PointB = FunctionMath.getRandomPoint(FunctionB, MathUtility.getRandomNumber_1Digit());

        QUESTION = "Find the function that has the graph parallel to the line " + FunctionA.toString()
                + ", and pass through the point " + PointB.toString() + ".";
        RIGHT_ANSWER = FunctionB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new LinearFunction(FunctionA.getSlope(), MathUtility.getRandomNumber_1Digit()).toString();
        ANSWER_C = new LinearFunction(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit()).toString();
        ANSWER_D = new LinearFunction(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit()).toString();
    }

    //  PERPENDICULAR
    private void createProblem_05(){
        LinearFunction FunctionA = new LinearFunction(1, MathUtility.getRandomNumber_1Digit());

        LinearFunction FunctionB = new LinearFunction(-1, MathUtility.getRandomNumber_1Digit());
        Point PointB = FunctionMath.getRandomPoint(FunctionB, MathUtility.getRandomNumber_1Digit());

        QUESTION = "Find the function that has the graph perpendicular to the line " + FunctionA.toString()
                + ", and pass through the point " + PointB.toString() + ".";
        RIGHT_ANSWER = FunctionB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new LinearFunction(FunctionA.getSlope(), MathUtility.getRandomNumber_1Digit()).toString();
        ANSWER_C = new LinearFunction(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit()).toString();
        ANSWER_D = new LinearFunction(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit()).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
