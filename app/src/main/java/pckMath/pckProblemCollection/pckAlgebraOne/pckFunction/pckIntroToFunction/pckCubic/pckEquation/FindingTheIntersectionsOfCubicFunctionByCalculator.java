package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckCubic.pckEquation;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.FunctionMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckCubic.pckFunction.CubicFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckLinearFunction.LinearFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckFunction.QuadraticFunction;

/**
 * Created by Cong on 8/2/2017.
 */

public class FindingTheIntersectionsOfCubicFunctionByCalculator extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingTheIntersectionsOfCubicFunctionByCalculator(){
        createProblem();
        swapAnswer();
    }

    public FindingTheIntersectionsOfCubicFunctionByCalculator(String Question,
                                                            String RightAnswer,
                                                            String AnswerA,
                                                            String AnswerB,
                                                            String AnswerC,
                                                            String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 3;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FindingTheIntersectionsOfCubicFunctionByCalculator(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  CUBIC vs LINEAR
    private void createProblem_01(){
        CubicFunction FunctionA = FunctionMath.getRandomCubicFunction();
        LinearFunction FunctionB = FunctionMath.getRandomLinearFunction();

        CubicFunction Function = new CubicFunction(FunctionA.getACoefficient(), FunctionA.getBCoefficient(),
                FunctionA.getCCoefficient() - FunctionB.getSlope(),
                FunctionA.getDCoefficient() - FunctionB.getYIntercept());

        QUESTION = FunctionA.toString() + " and<br> " + FunctionB.toString();
        RIGHT_ANSWER = "x = " + FunctionMath.findTheZero(Function);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomDecimalNumber_4Digit());
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomDecimalNumber_4Digit());
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomDecimalNumber_4Digit());
    }

    //  CUBIC vs QUADRATIC
    private void createProblem_02(){
        CubicFunction FunctionA = FunctionMath.getRandomCubicFunction();
        QuadraticFunction FunctionB = FunctionMath.getRandomQuadraticFunction();

        CubicFunction Function = new CubicFunction(FunctionA.getACoefficient(),
                FunctionA.getBCoefficient() - FunctionB.getACoefficient(),
                FunctionA.getCCoefficient() - FunctionB.getBCoefficient(),
                FunctionA.getDCoefficient() - FunctionB.getCCoefficient());

        QUESTION = FunctionA.toString() + " and<br> " + FunctionB.toString();
        RIGHT_ANSWER = "x = " + FunctionMath.findTheZero(Function);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomDecimalNumber_4Digit());
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomDecimalNumber_4Digit());
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomDecimalNumber_4Digit());
    }

    //  CUBIC vs CUBIC
    private void createProblem_03(){
        CubicFunction FunctionA = FunctionMath.getRandomCubicFunction();
        CubicFunction FunctionB = FunctionMath.getRandomCubicFunction();

        CubicFunction Function = new CubicFunction(FunctionA.getACoefficient() - FunctionB.getACoefficient(),
                FunctionA.getBCoefficient() - FunctionB.getBCoefficient(),
                FunctionA.getCCoefficient() - FunctionB.getCCoefficient(),
                FunctionA.getDCoefficient() - FunctionB.getDCoefficient());

        QUESTION = FunctionA.toString() + " and<br> " + FunctionB.toString();
        RIGHT_ANSWER = "x = " + FunctionMath.findTheZero(Function);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + String.valueOf(MathUtility.getRandomDecimalNumber_4Digit());
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomDecimalNumber_4Digit());
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomDecimalNumber_4Digit());
    }

    @Override
    public String getQuestion() {
        QUESTION = "Use the graphing calculator to find the intersections:<br> " + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
