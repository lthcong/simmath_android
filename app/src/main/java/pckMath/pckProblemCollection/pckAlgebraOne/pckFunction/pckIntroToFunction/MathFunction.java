package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction;

import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/31/2017.
 */

public abstract class MathFunction {

    public abstract String toString();
    public abstract Polynomial getPolynomial();
}
