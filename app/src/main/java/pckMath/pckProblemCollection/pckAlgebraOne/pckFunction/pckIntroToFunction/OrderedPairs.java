package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction;

/**
 * Created by Cong on 7/19/2017.
 */

public class OrderedPairs {

    private int FIRST_COORDINATE, SECOND_COORDINATE;

    public OrderedPairs(){
        FIRST_COORDINATE = 0;
        SECOND_COORDINATE = 0;
    }

    public OrderedPairs(int FirstCoordinate, int SecondCoordinate){
        FIRST_COORDINATE = FirstCoordinate;
        SECOND_COORDINATE = SecondCoordinate;
    }

    public void setFirstCoordinate(int FirstCoordinate){
        FIRST_COORDINATE = FirstCoordinate;
    }

    public int getFirstCoordinate(){
        return FIRST_COORDINATE;
    }

    public void setSecondCoordinate(int SecondCoordinate){
        SECOND_COORDINATE = SecondCoordinate;
    }

    public int getSecondCoordinate(){
        return SECOND_COORDINATE;
    }

    public String toString(){
        return "(" + String.valueOf(FIRST_COORDINATE) + ", " + String.valueOf(SECOND_COORDINATE) + ")";
    }
}
