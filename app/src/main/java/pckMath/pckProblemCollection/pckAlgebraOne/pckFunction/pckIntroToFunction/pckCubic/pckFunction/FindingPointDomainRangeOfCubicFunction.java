package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckCubic.pckFunction;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.FunctionMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.Interval;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.Set;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.LineMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/26/2017.
 */

public class FindingPointDomainRangeOfCubicFunction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingPointDomainRangeOfCubicFunction(){
        createProblem();
        swapAnswer();
    }

    public FindingPointDomainRangeOfCubicFunction(String Question,
                                                  String RightAnswer,
                                                  String AnswerA,
                                                  String AnswerB,
                                                  String AnswerC,
                                                  String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 5;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FindingPointDomainRangeOfCubicFunction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  POINTS
    private void createProblem_01(){
        CubicFunction Function = FunctionMath.getRandomCubicFunction();
        int XCoordinate = MathUtility.getRandomNumber_1Digit();
        Point RandomPoint = FunctionMath.getRandomPoint(Function, XCoordinate);

        QUESTION = "Find the point of the function " + Function.toString() + " with the given x coordinate is " + String.valueOf(XCoordinate) + ".";
        RIGHT_ANSWER = RandomPoint.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString();
        ANSWER_C = (new Point(FractionMath.getRandomFraction(), FractionMath.getRandomFraction())).toString();
        ANSWER_D = (new Point(FractionMath.getRandomFraction(), new Fraction(MathUtility.getRandomNumber_1Digit()))).toString();
    }

    //  POINTS
    private void createProblem_02(){
        CubicFunction Function = FunctionMath.getRandomCubicFunction();
        int XCoordinate = MathUtility.getRandomNumber_1Digit();
        Point PointA = FunctionMath.getRandomPoint(Function, XCoordinate - 2);
        Point PointB = FunctionMath.getRandomPoint(Function, XCoordinate - 1);
        Point PointC = FunctionMath.getRandomPoint(Function, XCoordinate);
        Point PointD = FunctionMath.getRandomPoint(Function, XCoordinate + 1);
        Point PointE = FunctionMath.getRandomPoint(Function, XCoordinate + 2);

        Set XCoordinateSet = new Set();
        XCoordinateSet.add(XCoordinate - 2);
        XCoordinateSet.add(XCoordinate - 1);
        XCoordinateSet.add(XCoordinate);
        XCoordinateSet.add(XCoordinate + 1);
        XCoordinateSet.add(XCoordinate + 2);

        QUESTION = "Find the point of the function " + Function.toString() + " with the given x coordinates are x = " + XCoordinateSet.toString() + ".";
        RIGHT_ANSWER = "{" + PointA.toString() + ", " + PointB.toString() + ", "
                + PointC.toString() + ", " + PointD.toString() + ", "
                + PointE.toString() + "}";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "{" + PointA.toString() + ", " + LineMath.getRandomPoint().toString() + ", "
                + PointC.toString() + ", " + LineMath.getRandomPoint().toString() + ", "
                + PointE.toString() + "}";
        ANSWER_C = "{" + PointA.toString() + ", " + PointB.toString() + ", "
                + PointC.toString() + ", " + PointD.toString() + ", "
                + LineMath.getRandomPoint().toString() + "}";
        ANSWER_D = "{" + PointA.toString() + ", " + PointB.toString() + ", "
                + LineMath.getRandomPoint().toString() + ", " + LineMath.getRandomPoint().toString() + ", "
                + PointE.toString() + "}";
    }

    //  POINTS
    private void createProblem_03(){
        CubicFunction Function = FunctionMath.getRandomCubicFunction();
        int XCoordinate = MathUtility.getRandomNumber_1Digit();
        Point PointA = FunctionMath.getRandomPoint(Function, XCoordinate - 1);
        Point PointB = FunctionMath.getRandomPoint(Function, XCoordinate);
        Point PointC = FunctionMath.getRandomPoint(Function, XCoordinate + 1);

        Set YCoordinateSet = new Set();
        YCoordinateSet.add((int) PointA.getYCoordinate().getValue());
        YCoordinateSet.add((int) PointB.getYCoordinate().getValue());
        YCoordinateSet.add((int) PointC.getYCoordinate().getValue());

        QUESTION = "Find the point of the function " + Function.toString() + " with the given y coordinates are y = " + YCoordinateSet.toString() + ".";
        RIGHT_ANSWER = "{" + PointA.toString() + ", " + PointB.toString() + ", " + PointC.toString() + "}";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "{" + PointA.toString() + ", " + LineMath.getRandomPoint().toString() + ", "
                + PointC.toString() + ", " + LineMath.getRandomPoint().toString() + ", "
                + "}";
        ANSWER_C = "{" + PointA.toString() + ", " + PointB.toString() + ", "
                + PointC.toString() + ", " + LineMath.getRandomPoint().toString() + "}";
        ANSWER_D = "{" + PointA.toString() + ", " + PointB.toString() + ", "
                + LineMath.getRandomPoint().toString() + ", " + LineMath.getRandomPoint().toString()
                + "}";
    }

    //  DOMAIN
    private void createProblem_04(){
        CubicFunction Function = FunctionMath.getRandomCubicFunction();

        QUESTION = "Find the domain of this function: " + Function.toString() + ".";
        RIGHT_ANSWER = (new Interval()).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, MathUtility.getRandomNegativeNumber(), MathUtility.getRandomPositiveNumber(), false)).toString();
        ANSWER_C = (new Interval(true, MathUtility.getRandomNegativeNumber(), MathUtility.getRandomPositiveNumber(), false)).toString();
        ANSWER_D = (new Interval(false, MathUtility.getRandomNegativeNumber(), MathUtility.getRandomPositiveNumber(), true)).toString();
    }

    //  RANGE
    private void createProblem_05(){
        CubicFunction Function = FunctionMath.getRandomCubicFunction();

        QUESTION = "Find the range of this function: " + Function.toString() + ".";
        RIGHT_ANSWER = (new Interval()).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, MathUtility.getRandomNegativeNumber(), MathUtility.getRandomPositiveNumber(), false)).toString();
        ANSWER_C = (new Interval(true, MathUtility.getRandomNegativeNumber(), MathUtility.getRandomPositiveNumber(), false)).toString();
        ANSWER_D = (new Interval(false, MathUtility.getRandomNegativeNumber(), MathUtility.getRandomPositiveNumber(), true)).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
