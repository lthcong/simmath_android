package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction;

import java.util.ArrayList;

import pckInfo.InfoCollector;
import pckMath.MathUtility;

/**
 * Created by Cong on 7/19/2017.
 */

public class Set {

    private ArrayList<Integer> ELEMENT_LIST;

    public Set(){
        ELEMENT_LIST = new ArrayList<>();
    }

    public void add(Integer IntElement){
        ELEMENT_LIST.add(IntElement);
    }

    public void add(ArrayList<Integer> SetElement){
        ELEMENT_LIST.addAll(MathUtility.convertToIntegerArrayList(MathUtility.convertToIntegerArray(SetElement)));
    }

    public void remove(int Index){
        ELEMENT_LIST.remove(Index);
    }

    public Integer get(int ElementIndex){
        return ELEMENT_LIST.get(ElementIndex);
    }

    public ArrayList<Integer> getAll(){
        return ELEMENT_LIST;
    }

    public int getSize(){
        return ELEMENT_LIST.size();
    }

    public boolean isContains(Integer Element){
        return ELEMENT_LIST.contains(Element);
    }

    public boolean isEqualTo(Set SecondSet){
        boolean isEqual = true;
        int size = getSize();
        if (size != SecondSet.getSize()){
            isEqual = false;
        }
        else {
            for (int i = 0; i < size; i++){
                if (!ELEMENT_LIST.get(i).equals(SecondSet.get(i))){
                    isEqual = false;
                }
            }
        }

        return isEqual;
    }

    public String toString(){
        String StringList = "";
        int size = ELEMENT_LIST.size();
        for (int i = 0; i < size; i++){
            int Temp = ELEMENT_LIST.get(i);
            if (Temp != Integer.MIN_VALUE) {
                StringList += String.valueOf(ELEMENT_LIST.get(i)) + ", ";
            }
            else {
                StringList += InfoCollector.getEmptySet() + ", ";
            }
        }
        StringList = "{" + StringList.trim().substring(0, StringList.length() - 1) + "}";

        return StringList;
    }
}
