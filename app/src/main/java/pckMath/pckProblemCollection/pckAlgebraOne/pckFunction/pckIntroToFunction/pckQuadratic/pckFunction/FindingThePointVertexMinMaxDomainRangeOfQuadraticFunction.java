package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckFunction;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.FunctionMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.Interval;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.LineMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/23/2017.
 */

public class FindingThePointVertexMinMaxDomainRangeOfQuadraticFunction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingThePointVertexMinMaxDomainRangeOfQuadraticFunction(){
        createProblem();
        swapAnswer();
    }

    public FindingThePointVertexMinMaxDomainRangeOfQuadraticFunction(String Question,
                                                                     String RightAnswer,
                                                                     String AnswerA,
                                                                     String AnswerB,
                                                                     String AnswerC,
                                                                     String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 13;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FindingThePointVertexMinMaxDomainRangeOfQuadraticFunction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  POINT
    private void createProblem_01(){
        QuadraticFunction QF = FunctionMath.getRandomQuadraticFunction();

        int XCoordinate = MathUtility.getRandomNumber_1Digit();
        Point RandomPoint = FunctionMath.getRandomPoint(QF, XCoordinate);

        QUESTION = "Find the point of the graph of the quadratic function: " + QF.toString() + ".";
        RIGHT_ANSWER = RandomPoint.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Point(FractionMath.getRandomFraction(), FractionMath.getRandomFraction())).toString();
        ANSWER_C = (new Point(FractionMath.getRandomFraction(), FractionMath.getRandomFraction())).toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  POINT
    private void createProblem_02(){
        QuadraticFunction QF = new QuadraticFunction(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), 0);

        int XCoordinate = MathUtility.getRandomNumber_1Digit();
        Point RandomPoint = FunctionMath.getRandomPoint(QF, XCoordinate);

        QUESTION = "Find the point of the graph of the quadratic function: " + QF.toString() + ".";
        RIGHT_ANSWER = RandomPoint.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Point(FractionMath.getRandomFraction(), FractionMath.getRandomFraction())).toString();
        ANSWER_C = (new Point(FractionMath.getRandomFraction(), FractionMath.getRandomFraction())).toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  POINT
    private void createProblem_03(){
        QuadraticFunction QF = new QuadraticFunction(MathUtility.getRandomNumber_1Digit(), 0, MathUtility.getRandomNumber_1Digit());

        int XCoordinate = MathUtility.getRandomNumber_1Digit();
        Point RandomPoint = FunctionMath.getRandomPoint(QF, XCoordinate);

        QUESTION = "Find the point of the graph of the quadratic function: " + QF.toString() + ".";
        RIGHT_ANSWER = RandomPoint.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Point(FractionMath.getRandomFraction(), FractionMath.getRandomFraction())).toString();
        ANSWER_C = (new Point(FractionMath.getRandomFraction(), FractionMath.getRandomFraction())).toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  VERTEX
    private void createProblem_04(){
        QuadraticFunction QF = FunctionMath.getRandomQuadraticFunction();
        Point Vertex = FunctionMath.getQuadraticVertex(QF);

        QUESTION = "Find the vertex of the graph of the quadratic function: " + QF.toString() + ".";
        RIGHT_ANSWER = Vertex.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Point(FractionMath.getRandomFraction(), FractionMath.getRandomFraction())).toString();
        ANSWER_C = (new Point(FractionMath.getRandomFraction(), FractionMath.getRandomFraction())).toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  VERTEX
    private void createProblem_05(){
        QuadraticFunction QF = new QuadraticFunction(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), 0);
        Point Vertex = FunctionMath.getQuadraticVertex(QF);

        QUESTION = "Find the vertex of the graph of the quadratic function: " + QF.toString() + ".";
        RIGHT_ANSWER = Vertex.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Point(FractionMath.getRandomFraction(), FractionMath.getRandomFraction())).toString();
        ANSWER_C = (new Point(FractionMath.getRandomFraction(), FractionMath.getRandomFraction())).toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  VERTEX
    private void createProblem_06(){
        QuadraticFunction QF = new QuadraticFunction(MathUtility.getRandomNumber_1Digit(), 0, MathUtility.getRandomNumber_1Digit());
        Point Vertex = FunctionMath.getQuadraticVertex(QF);

        QUESTION = "Find the vertex of the graph of the quadratic function: " + QF.toString() + ".";
        RIGHT_ANSWER = Vertex.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Point(FractionMath.getRandomFraction(), FractionMath.getRandomFraction())).toString();
        ANSWER_C = (new Point(FractionMath.getRandomFraction(), FractionMath.getRandomFraction())).toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  POINTS
    private void createProblem_07(){
        QuadraticFunction QF = FunctionMath.getRandomQuadraticFunction();

        int XCoordinate = MathUtility.getRandomNumber_1Digit();
        Point PointA = FunctionMath.getRandomPoint(QF, XCoordinate);
        Point PointB = FunctionMath.getRandomPoint(QF, XCoordinate + 2);
        Point PointC = FunctionMath.getRandomPoint(QF, XCoordinate - 2);

        QUESTION = "Find the points of the graph of the quadratic function: " + QF.toString()
                + ", with the given x coordinate x = {" + String.valueOf(XCoordinate - 2) + ", " + String.valueOf(XCoordinate) + ", " + String.valueOf(XCoordinate + 2) + "}.";
        RIGHT_ANSWER = PointA.toString() + ", " + PointB.toString() + ", " + PointC.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString() + ", " + PointB.toString() + ", " + LineMath.getRandomPoint().toString();
        ANSWER_C = LineMath.getRandomPoint().toString() + ", " + LineMath.getRandomPoint().toString() + ", " + PointC.toString();
        ANSWER_D = PointA.toString() + ", " + LineMath.getRandomPoint().toString() + ", " + LineMath.getRandomPoint().toString();
    }

    //  POINTS
    private void createProblem_08(){
        QuadraticFunction QF = new QuadraticFunction(MathUtility.getRandomNumber_1Digit(), 0, MathUtility.getRandomNumber_1Digit());

        int XCoordinate = MathUtility.getRandomNumber_1Digit();
        Point PointA = FunctionMath.getRandomPoint(QF, XCoordinate);
        Point PointB = FunctionMath.getRandomPoint(QF, XCoordinate + 2);
        Point PointC = FunctionMath.getRandomPoint(QF, XCoordinate - 2);

        QUESTION = "Find the points of the graph of the quadratic function: " + QF.toString()
                + ", with the given x coordinate x = {" + String.valueOf(XCoordinate - 2) + ", " + String.valueOf(XCoordinate) + ", " + String.valueOf(XCoordinate + 2) + "}.";
        RIGHT_ANSWER = PointA.toString() + ", " + PointB.toString() + ", " + PointC.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString() + ", " + PointB.toString() + ", " + LineMath.getRandomPoint().toString();
        ANSWER_C = LineMath.getRandomPoint().toString() + ", " + LineMath.getRandomPoint().toString() + ", " + PointC.toString();
        ANSWER_D = PointA.toString() + ", " + LineMath.getRandomPoint().toString() + ", " + LineMath.getRandomPoint().toString();
    }

    //  POINTS
    private void createProblem_09(){
        QuadraticFunction QF = new QuadraticFunction(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), 0);

        int XCoordinate = MathUtility.getRandomNumber_1Digit();
        Point PointA = FunctionMath.getRandomPoint(QF, XCoordinate);
        Point PointB = FunctionMath.getRandomPoint(QF, XCoordinate + 2);
        Point PointC = FunctionMath.getRandomPoint(QF, XCoordinate - 2);

        QUESTION = "Find the points of the graph of the quadratic function: " + QF.toString()
                + ", with the given x coordinate x = {" + String.valueOf(XCoordinate - 2) + ", " + String.valueOf(XCoordinate) + ", " + String.valueOf(XCoordinate + 2) + "}.";
        RIGHT_ANSWER = PointA.toString() + ", " + PointB.toString() + ", " + PointC.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString() + ", " + PointB.toString() + ", " + LineMath.getRandomPoint().toString();
        ANSWER_C = LineMath.getRandomPoint().toString() + ", " + LineMath.getRandomPoint().toString() + ", " + PointC.toString();
        ANSWER_D = PointA.toString() + ", " + LineMath.getRandomPoint().toString() + ", " + LineMath.getRandomPoint().toString();
    }

    //  MAXIMUM VALUE
    private void createProblem_10(){
        QuadraticFunction QF = new QuadraticFunction(MathUtility.getRandomNegativeNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit());
        Point Vertex = FunctionMath.getQuadraticVertex(QF);

        QUESTION = "Find the maximum value of the function: " + QF.toString() + ".";
        RIGHT_ANSWER = Vertex.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Point(FractionMath.getRandomFraction(), FractionMath.getRandomFraction())).toString();
        ANSWER_C = (new Point(FractionMath.getRandomFraction(), FractionMath.getRandomFraction())).toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  MINIMUM VALUE
    private void createProblem_11(){
        QuadraticFunction QF = new QuadraticFunction(MathUtility.getRandomPositiveNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNegativeNumber_1Digit());
        Point Vertex = FunctionMath.getQuadraticVertex(QF);

        QUESTION = "Find the minimum value of the function: " + QF.toString() + ".";
        RIGHT_ANSWER = Vertex.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Point(FractionMath.getRandomFraction(), FractionMath.getRandomFraction())).toString();
        ANSWER_C = (new Point(FractionMath.getRandomFraction(), FractionMath.getRandomFraction())).toString();
        ANSWER_D = LineMath.getRandomPoint().toString();
    }

    //  RANGE
    private void createProblem_12(){
        QuadraticFunction Function = FunctionMath.getRandomQuadraticFunction();

        QUESTION = "Find the range of this function: " + Function.toString() + ".";
        if (Function.getACoefficient() > 0){
            RIGHT_ANSWER = (new Interval(true, FunctionMath.getQuadraticVertex(Function).getYCoordinate().toString(), Integer.MAX_VALUE, false)).toString();
        }
        else {
            RIGHT_ANSWER = (new Interval(false, Integer.MIN_VALUE, FunctionMath.getQuadraticVertex(Function).getYCoordinate().toString(), true)).toString();
        }

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, MathUtility.getRandomNegativeNumber(), MathUtility.getRandomPositiveNumber(), false)).toString();
        ANSWER_C = (new Interval(true, MathUtility.getRandomNegativeNumber(), MathUtility.getRandomPositiveNumber(), false)).toString();
        ANSWER_D = (new Interval(false, MathUtility.getRandomNegativeNumber(), MathUtility.getRandomPositiveNumber(), true)).toString();
    }

    //  DOMAIN
    private void createProblem_13(){
        QuadraticFunction Function = FunctionMath.getRandomQuadraticFunction();

        QUESTION = "Find the domain of this function: " + Function.toString() + ".";
        RIGHT_ANSWER = (new Interval()).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, MathUtility.getRandomNegativeNumber(), MathUtility.getRandomPositiveNumber(), false)).toString();
        ANSWER_C = (new Interval(true, MathUtility.getRandomNegativeNumber(), Integer.MAX_VALUE, false)).toString();
        ANSWER_D = (new Interval(false, Integer.MIN_VALUE, MathUtility.getRandomPositiveNumber(), true)).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
