package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction;

import pckInfo.InfoCollector;

/**
 * Created by Cong on 7/20/2017.
 */

public class Interval {

    private String SLOWER_SIGN, SLOWER_BOUND, UPPER_BOUND, UPPER_SIGN;
    private boolean EQUAL_TO_SLOWER_BOUND, EQUAL_TO_UPPER_BOUND;

    public Interval(){
        EQUAL_TO_SLOWER_BOUND = false;
        EQUAL_TO_UPPER_BOUND = false;

        SLOWER_SIGN = "(";
        SLOWER_BOUND = "-" + InfoCollector.getInfinity();
        UPPER_BOUND = InfoCollector.getInfinity();
        UPPER_SIGN = ")";
    }

    public Interval(int SlowerBound, int UpperBound){
        EQUAL_TO_SLOWER_BOUND = false;
        EQUAL_TO_UPPER_BOUND = false;

        SLOWER_SIGN = "(";
        if (SlowerBound == Integer.MIN_VALUE) {
            SLOWER_BOUND = "-" + InfoCollector.getInfinity();
        }
        else {
            SLOWER_BOUND = String.valueOf(SlowerBound);
        }

        if (UpperBound == Integer.MAX_VALUE) {
            UPPER_BOUND = InfoCollector.getInfinity();
        }
        else {
            UPPER_BOUND = String.valueOf(UpperBound);
        }
        UPPER_SIGN = ")";
    }

    public Interval(boolean isEqualToSlowerBound, int SlowerBound, int UpperBound, boolean isEqualToUpperBound){
        EQUAL_TO_SLOWER_BOUND = isEqualToSlowerBound;
        EQUAL_TO_UPPER_BOUND = isEqualToUpperBound;

        if (isEqualToSlowerBound) {
            SLOWER_SIGN = "[";
        }
        else {
            SLOWER_SIGN = "(";
        }

        if (SlowerBound == Integer.MIN_VALUE) {
            SLOWER_BOUND = "-" + InfoCollector.getInfinity();
        }
        else {
            SLOWER_BOUND = String.valueOf(SlowerBound);
        }

        if (UpperBound == Integer.MAX_VALUE) {
            UPPER_BOUND = InfoCollector.getInfinity();
        }
        else {
            UPPER_BOUND = String.valueOf(UpperBound);
        }

        if (isEqualToUpperBound) {
            UPPER_SIGN = "]";
        }
        else {
            UPPER_SIGN = ")";
        }
    }

    public Interval(boolean isEqualToSlowerBound, String SlowerBound, int UpperBound, boolean isEqualToUpperBound){
        EQUAL_TO_SLOWER_BOUND = isEqualToSlowerBound;
        EQUAL_TO_UPPER_BOUND = isEqualToUpperBound;

        if (isEqualToSlowerBound) {
            SLOWER_SIGN = "[";
        }
        else {
            SLOWER_SIGN = "(";
        }

        SLOWER_BOUND = String.valueOf(SlowerBound);

        if (UpperBound == Integer.MAX_VALUE) {
            UPPER_BOUND = InfoCollector.getInfinity();
        }
        else {
            UPPER_BOUND = String.valueOf(UpperBound);
        }

        if (isEqualToUpperBound) {
            UPPER_SIGN = "]";
        }
        else {
            UPPER_SIGN = ")";
        }
    }

    public Interval(boolean isEqualToSlowerBound, int SlowerBound, String UpperBound, boolean isEqualToUpperBound){
        EQUAL_TO_SLOWER_BOUND = isEqualToSlowerBound;
        EQUAL_TO_UPPER_BOUND = isEqualToUpperBound;

        if (isEqualToSlowerBound) {
            SLOWER_SIGN = "[";
        }
        else {
            SLOWER_SIGN = "(";
        }

        if (SlowerBound == Integer.MIN_VALUE) {
            SLOWER_BOUND = "-" + InfoCollector.getInfinity();
        }
        else {
            SLOWER_BOUND = String.valueOf(SlowerBound);
        }

        UPPER_BOUND = String.valueOf(UpperBound);

        if (isEqualToUpperBound) {
            UPPER_SIGN = "]";
        }
        else {
            UPPER_SIGN = ")";
        }
    }

    public Interval(boolean isEqualToSlowerBound, String SlowerBound, String UpperBound, boolean isEqualToUpperBound){
        EQUAL_TO_SLOWER_BOUND = isEqualToSlowerBound;
        EQUAL_TO_UPPER_BOUND = isEqualToUpperBound;

        if (isEqualToSlowerBound) {
            SLOWER_SIGN = "[";
        }
        else {
            SLOWER_SIGN = "(";
        }

        SLOWER_BOUND = String.valueOf(SlowerBound);

        UPPER_BOUND = String.valueOf(UpperBound);

        if (isEqualToUpperBound) {
            UPPER_SIGN = "]";
        }
        else {
            UPPER_SIGN = ")";
        }
    }

    public void setSlowerBound(String SlowerBound){
        SLOWER_BOUND = SlowerBound;
    }

    public String getSlowerBound(){
        return SLOWER_BOUND;
    }

    public void setUpperBound(String UpperBound){
        UPPER_BOUND = UpperBound;
    }

    public String getUpperBound(){
        return UPPER_BOUND;
    }

    public boolean getEqualToSlowerBound(){
        return EQUAL_TO_SLOWER_BOUND;
    }

    public boolean getEqualToUpperBound(){
        return EQUAL_TO_UPPER_BOUND;
    }

    public String toString(){
        return SLOWER_SIGN + SLOWER_BOUND + ", " + UPPER_BOUND + UPPER_SIGN;
    }
}
