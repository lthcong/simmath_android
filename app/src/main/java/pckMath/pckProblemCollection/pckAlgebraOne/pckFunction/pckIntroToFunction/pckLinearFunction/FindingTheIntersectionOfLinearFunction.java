package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckLinearFunction;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.FunctionMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.LineMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.SystemOfLinearEquations;

/**
 * Created by Cong on 7/23/2017.
 */

public class FindingTheIntersectionOfLinearFunction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingTheIntersectionOfLinearFunction(){
        createProblem();
        swapAnswer();
    }

    public FindingTheIntersectionOfLinearFunction(String Question,
                                                  String RightAnswer,
                                                  String AnswerA,
                                                  String AnswerB,
                                                  String AnswerC,
                                                  String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 2;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FindingTheIntersectionOfLinearFunction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        LinearFunction FunctionA = FunctionMath.getRandomLinearFunction();
        LinearFunction FunctionB = FunctionMath.getRandomLinearFunction();

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations("x", "y",
                FunctionA.getSlope(), -1, 0 - FunctionA.getYIntercept(),
                FunctionB.getSlope(), -1, 0 - FunctionB.getYIntercept());

        QUESTION = "Find the intersection of the graphs: <br>" + FunctionA.toString() + " and " + FunctionB.toString() + ".";
        if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getNoSolution())){
            RIGHT_ANSWER = "The lines are parallel.";

            ANSWER_B = "These lines are the same.";
            ANSWER_C = LineMath.getRandomPoint().toString();
            ANSWER_D = LineMath.getRandomPoint().toString();
        }
        else {
            if (SOLE.getSolution().equalsIgnoreCase(InfoCollector.getInfiniteManySolution())){
                RIGHT_ANSWER = "These lines are the same.";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = LineMath.getRandomPoint().toString();
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
            else {
                SOLE.getSolution();
                RIGHT_ANSWER = "(" + SOLE.getXSolution() + ", " + SOLE.getYSolution() + ")";

                ANSWER_B = "The lines are parallel.";
                ANSWER_C = "These lines are the same.";
                ANSWER_D = LineMath.getRandomPoint().toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }


}
