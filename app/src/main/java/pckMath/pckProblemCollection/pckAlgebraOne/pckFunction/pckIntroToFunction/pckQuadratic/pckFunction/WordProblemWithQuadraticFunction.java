package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckFunction;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.FunctionMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;

/**
 * Created by Cong on 10/17/2017.
 */

public class WordProblemWithQuadraticFunction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public WordProblemWithQuadraticFunction(){
        createProblem();
        swapAnswer();
    }

    public WordProblemWithQuadraticFunction(String Question,
                                         String RightAnswer,
                                         String AnswerA,
                                         String AnswerB,
                                         String AnswerC,
                                         String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 7;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            default:
                createProblem_01();
                break;
        }

        return new WordProblemWithQuadraticFunction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  LINE BEST FIT
    private void createProblem_01(){
        QuadraticFunction Function = FunctionMath.getRandomQuadraticFunction();

        Point PointA = FunctionMath.getRandomPoint(Function);
        Point PointB = FunctionMath.getRandomPoint(Function);
        Point PointC = FunctionMath.getRandomPoint(Function);
        Point PointD = FunctionMath.getRandomPoint(Function);
        Point PointE = FunctionMath.getRandomPoint(Function);

        QUESTION = "Given the data list (x, y): <br>"
                + "(" + String.valueOf(PointA.getXCoordinate().getNumerator() + 1) + ", " + String.valueOf(PointA.getYCoordinate().getNumerator() + 1) + "), "
                + "(" + String.valueOf(PointB.getXCoordinate().getNumerator() - 1) + ", " + String.valueOf(PointB.getYCoordinate().getNumerator() + 1) + "), "
                + "(" + String.valueOf(PointC.getXCoordinate().getNumerator() - 1) + ", " + String.valueOf(PointC.getYCoordinate().getNumerator() - 2) + "), "
                + "(" + String.valueOf(PointD.getXCoordinate().getNumerator() - 1) + ", " + String.valueOf(PointD.getYCoordinate().getNumerator() + 2) + "), "
                + "(" + String.valueOf(PointE.getXCoordinate().getNumerator() - 2) + ", " + String.valueOf(PointE.getYCoordinate().getNumerator() + 1) + ")."
                + "Find the line of best fit for the data";
        RIGHT_ANSWER = Function.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  LINE OF BEST FIT
    private void createProblem_02(){
        QuadraticFunction Function = new QuadraticFunction(MathUtility.getRandomPositiveNumber_1Digit(),
                MathUtility.getRandomPositiveNumber_1Digit(),
                MathUtility.getRandomPositiveNumber_1Digit());

        Point Month_1 = FunctionMath.getRandomPoint(Function, 1);
        Point Month_2 = FunctionMath.getRandomPoint(Function, 2);
        Point Month_3 = FunctionMath.getRandomPoint(Function, 3);
        Point Month_4 = FunctionMath.getRandomPoint(Function, 4);
        Point Month_5 = FunctionMath.getRandomPoint(Function, 5);

        QUESTION = "Jame's company record shows that the profit of the first 4 months (in million USD) are:<br>"
                + String.valueOf(Month_1.getYCoordinate().getNumerator() + 2) + ", "
                + String.valueOf(Month_2.getYCoordinate().getNumerator() + 3) + ", "
                + String.valueOf(Month_3.getYCoordinate().getNumerator() - 4) + ", "
                + String.valueOf(Month_4.getYCoordinate().getNumerator() - 1) + "."
                + "Use the line of best fit to predict the profit of Jame's company in the next month.";
        RIGHT_ANSWER = Month_5.getYCoordinate().toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.getRandomPositiveNumber_2Digit());
        ANSWER_C = String.valueOf(MathUtility.getRandomPositiveNumber_2Digit());
        ANSWER_D = String.valueOf(MathUtility.getRandomPositiveNumber_2Digit());
    }

    //  LINE OF BEST FIT
    private void createProblem_03(){
        QuadraticFunction Function = new QuadraticFunction(MathUtility.getRandomPositiveNumber_1Digit(),
                MathUtility.getRandomPositiveNumber_1Digit(),
                MathUtility.getRandomPositiveNumber_1Digit());

        Point Month_1 = FunctionMath.getRandomPoint(Function, 1);
        Point Month_2 = FunctionMath.getRandomPoint(Function, 2);
        Point Month_3 = FunctionMath.getRandomPoint(Function, 3);
        Point Month_4 = FunctionMath.getRandomPoint(Function, 4);
        Point Result = FunctionMath.getRandomPoint(Function, MathUtility.getRandomPositiveNumber_1Digit() + 10);

        QUESTION = "Jame's company record shows that the profit of the first 4 months (in million USD) are:<br>"
                + String.valueOf(Month_1.getYCoordinate().getNumerator() + 2) + ", "
                + String.valueOf(Month_2.getYCoordinate().getNumerator() + 3) + ", "
                + String.valueOf(Month_3.getYCoordinate().getNumerator() - 4) + ", "
                + String.valueOf(Month_4.getYCoordinate().getNumerator() - 1) + "."
                + "How long does it take his company to get $" + String.valueOf(Result.getYCoordinate().toString()) + " in profit?";
        RIGHT_ANSWER = Result.getXCoordinate().toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.getRandomPositiveNumber_2Digit());
        ANSWER_C = String.valueOf(MathUtility.getRandomPositiveNumber_2Digit());
        ANSWER_D = String.valueOf(MathUtility.getRandomPositiveNumber_2Digit());
    }

    //  COMPANY VALUE
    private void createProblem_04(){
        int Principal = MathUtility.getRandomPositiveNumber_1Digit();

        QuadraticFunction Function = new QuadraticFunction(MathUtility.getRandomPositiveNumber_1Digit(),
                MathUtility.getRandomPositiveNumber_1Digit(),
                Principal);

        Point PointA = FunctionMath.getRandomPoint(Function, MathUtility.getRandomPositiveNumber_1Digit());
        Point PointB = FunctionMath.getRandomPoint(Function, PointA.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());
        Point PointC = FunctionMath.getRandomPoint(Function, PointB.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());
        Point PointD = FunctionMath.getRandomPoint(Function, PointC.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());
        Point PointE = FunctionMath.getRandomPoint(Function, PointD.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());

        QUESTION = "A company's value is showed below (Month, Value):<br>"
                + PointA.toString() + ", " + PointB.toString() + ", " + PointC.toString() + ", "
                + PointD.toString() + ", " + PointE.toString() + ". Find the quadratic function the can used to describe the company's value.";
        RIGHT_ANSWER = Function.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  COMPANY VALUE
    private void createProblem_05(){
        int Principal = MathUtility.getRandomPositiveNumber_1Digit();

        QuadraticFunction Function = new QuadraticFunction(MathUtility.getRandomPositiveNumber_1Digit(),
                MathUtility.getRandomPositiveNumber_1Digit(),
                Principal);

        Point PointA = FunctionMath.getRandomPoint(Function, MathUtility.getRandomPositiveNumber_1Digit());
        Point PointB = FunctionMath.getRandomPoint(Function, PointA.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());
        Point PointC = FunctionMath.getRandomPoint(Function, PointB.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());
        Point PointD = FunctionMath.getRandomPoint(Function, PointC.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());
        Point PointE = FunctionMath.getRandomPoint(Function, PointD.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());
        Point PointF = FunctionMath.getRandomPoint(Function, PointE.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());

        QUESTION = "A company's value is showed below (Month, Value):<br>"
                + PointA.toString() + ", " + PointB.toString() + ", " + PointC.toString() + ", "
                + PointD.toString() + ", " + PointE.toString() + ". Predict the company value in " + PointF.getXCoordinate().toString() + " months.";
        RIGHT_ANSWER = PointF.getYCoordinate().toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomPoint(Function).getYCoordinate().toString();
        ANSWER_C = FunctionMath.getRandomPoint(Function).getYCoordinate().toString();
        ANSWER_D = FunctionMath.getRandomPoint(Function).getYCoordinate().toString();
    }

    //  FARM PRODUCT
    private void createProblem_06(){
        int Principal = MathUtility.getRandomPositiveNumber_1Digit();

        QuadraticFunction Function = new QuadraticFunction(MathUtility.getRandomPositiveNumber_1Digit(),
                MathUtility.getRandomPositiveNumber_1Digit(),
                Principal);

        Point PointA = FunctionMath.getRandomPoint(Function, MathUtility.getRandomPositiveNumber_1Digit());
        Point PointB = FunctionMath.getRandomPoint(Function, PointA.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());
        Point PointC = FunctionMath.getRandomPoint(Function, PointB.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());
        Point PointD = FunctionMath.getRandomPoint(Function, PointC.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());
        Point PointE = FunctionMath.getRandomPoint(Function, PointD.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());
        Point PointF = FunctionMath.getRandomPoint(Function, PointE.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());

        QUESTION = "The farm product relationship (Apple tree, Pounds of apple) is showed below:<br>"
                + PointA.toString() + ", " + PointB.toString() + ", " + PointC.toString() + ", "
                + PointD.toString() + ", " + PointE.toString() + ". How many apples does the farmer have if he has " + PointF.getXCoordinate().toString() + " apple trees?";
        RIGHT_ANSWER = PointF.getYCoordinate().toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomPoint(Function).getYCoordinate().toString();
        ANSWER_C = FunctionMath.getRandomPoint(Function).getYCoordinate().toString();
        ANSWER_D = FunctionMath.getRandomPoint(Function).getYCoordinate().toString();
    }

    //  FARM PRODUCT
    private void createProblem_07(){
        int Principal = MathUtility.getRandomPositiveNumber_1Digit();

        QuadraticFunction Function = new QuadraticFunction(MathUtility.getRandomPositiveNumber_1Digit(),
                MathUtility.getRandomPositiveNumber_1Digit(),
                Principal);

        Point PointA = FunctionMath.getRandomPoint(Function, MathUtility.getRandomPositiveNumber_1Digit());
        Point PointB = FunctionMath.getRandomPoint(Function, PointA.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());
        Point PointC = FunctionMath.getRandomPoint(Function, PointB.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());
        Point PointD = FunctionMath.getRandomPoint(Function, PointC.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());
        Point PointE = FunctionMath.getRandomPoint(Function, PointD.getXCoordinate().getNumerator() + MathUtility.getRandomPositiveNumber_1Digit());

        QUESTION = "The farm product relationship (Apple tree, Pounds of apple) is showed below:<br>"
                + PointA.toString() + ", " + PointB.toString() + ", " + PointC.toString() + ", "
                + PointD.toString() + ", " + PointE.toString()
                + ". Find the quadratic function that can be used to described the relationship?";
        RIGHT_ANSWER = Function.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
