package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckFunction;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicExpression;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.Variable;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 8/3/2017.
 */

public class RewriteQuadraticFunctionInVertexForm extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public RewriteQuadraticFunctionInVertexForm(){
        createProblem();
        swapAnswer();
    }

    public RewriteQuadraticFunctionInVertexForm(String Question,
                                      String RightAnswer,
                                      String AnswerA,
                                      String AnswerB,
                                      String AnswerC,
                                      String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 1;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            default:
                createProblem_01();
                break;
        }
        return new RewriteQuadraticFunctionInVertexForm(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomNumber_1Digit();
        int h = MathUtility.getRandomNumber_1Digit();
        int k = MathUtility.getRandomNumber_1Digit();

        AlgebraicNumber NumberA = new AlgebraicNumber();
        NumberA.setCoefficient(1);
        NumberA.addVariable(new Variable("x", 1));

        AlgebraicExpression ExpressionA = new AlgebraicExpression();
        ExpressionA.addNumber(NumberA);

        Polynomial PolA = new Polynomial();
        PolA.setExpression(ExpressionA);

        AlgebraicNumber NumberB = new AlgebraicNumber();
        NumberB.setCoefficient(h);
        NumberB.addVariable(new Variable("x", 0));

        AlgebraicExpression ExpressionB = new AlgebraicExpression();
        ExpressionB.addNumber(NumberB);

        Polynomial PolB = new Polynomial();
        PolB.setExpression(ExpressionB);

        AlgebraicNumber NumberC = new AlgebraicNumber();
        NumberC.setCoefficient(k);
        NumberC.addVariable(new Variable("x", 0));

        AlgebraicExpression ExpressionC = new AlgebraicExpression();
        ExpressionC.addNumber(NumberC);

        Polynomial PolC = new Polynomial();
        PolC.setExpression(ExpressionC);

        Polynomial Pol = AlgebraMath.sub(PolA, PolB);
        Pol = AlgebraMath.mul(Pol, Pol);
        Pol = AlgebraMath.mul(Pol, a);
        Pol = AlgebraMath.add(Pol, PolC);

        QUESTION = "Write this equation in vertex form: " + Pol.toString();
        RIGHT_ANSWER = String.valueOf(a) + "(" + PolA.toString() + " - " + PolB.toString()  + ")" + InfoCollector.getExponentSign("2") + " + " + PolC.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.getRandomNumber_1Digit())
                + "(x - " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ")" + InfoCollector.getExponentSign("2")
                + " + " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = String.valueOf(MathUtility.getRandomNumber_1Digit())
                + "(x - " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ")" + InfoCollector.getExponentSign("2")
                + " + " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = String.valueOf(MathUtility.getRandomNumber_1Digit())
                + "(x - " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ")" + InfoCollector.getExponentSign("2")
                + " + " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
