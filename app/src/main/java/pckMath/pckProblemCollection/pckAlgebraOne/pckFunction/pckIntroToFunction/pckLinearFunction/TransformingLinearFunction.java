package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckLinearFunction;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.FunctionMath;

/**
 * Created by Cong on 7/27/2017.
 */

public class TransformingLinearFunction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public TransformingLinearFunction(){
        createProblem();
        swapAnswer();
    }

    public TransformingLinearFunction(String Question,
                              String RightAnswer,
                              String AnswerA,
                              String AnswerB,
                              String AnswerC,
                              String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 12;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            default:
                createProblem_01();
                break;
        }

        return new TransformingLinearFunction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  SHIFT LEFT
    private void createProblem_01(){
        int Slope = MathUtility.getRandomNumber_1Digit();
        int Intercept = MathUtility.getRandomNumber_1Digit();

        int ShiftingUnit = MathUtility.getRandomPositiveNumber_1Digit();
        LinearFunction Function = new LinearFunction(Slope, Intercept);
        LinearFunction ShiftedFunction = new LinearFunction(FunctionMath.add(Function, Slope * ShiftingUnit));

        QUESTION = "Shift the function: " + Function.toString() + " to the left " + String.valueOf(ShiftingUnit) + " units.";
        RIGHT_ANSWER = ShiftedFunction.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  SHIFT RIGHT
    private void createProblem_02(){
        int Slope = MathUtility.getRandomNumber_1Digit();
        int Intercept = MathUtility.getRandomNumber_1Digit();

        int ShiftingUnit = MathUtility.getRandomPositiveNumber_1Digit();
        LinearFunction Function = new LinearFunction(Slope, Intercept);
        LinearFunction ShiftedFunction = new LinearFunction(FunctionMath.sub(Function, Slope * ShiftingUnit));

        QUESTION = "Shift the function: " + Function.toString() + " to the right " + String.valueOf(ShiftingUnit) + " units.";
        RIGHT_ANSWER = ShiftedFunction.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  SHIFT UP
    private void createProblem_03(){
        int Slope = MathUtility.getRandomNumber_1Digit();
        int Intercept = MathUtility.getRandomNumber_1Digit();

        int ShiftingUnit = MathUtility.getRandomPositiveNumber_1Digit();
        LinearFunction Function = new LinearFunction(Slope, Intercept);
        LinearFunction ShiftedFunction = new LinearFunction(FunctionMath.add(Function, ShiftingUnit));

        QUESTION = "Shift the function: " + Function.toString() + " up " + String.valueOf(ShiftingUnit) + " units.";
        RIGHT_ANSWER = ShiftedFunction.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  SHIFT DOWN
    private void createProblem_04(){
        int Slope = MathUtility.getRandomNumber_1Digit();
        int Intercept = MathUtility.getRandomNumber_1Digit();

        int ShiftingUnit = MathUtility.getRandomPositiveNumber_1Digit();
        LinearFunction Function = new LinearFunction(Slope, Intercept);
        LinearFunction ShiftedFunction = new LinearFunction(FunctionMath.sub(Function, ShiftingUnit));

        QUESTION = "Shift the function: " + Function.toString() + " down " + String.valueOf(ShiftingUnit) + " units.";
        RIGHT_ANSWER = ShiftedFunction.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  REFLECT X-AXIS
    private void createProblem_05(){
        int Slope = MathUtility.getRandomNumber_1Digit();
        int Intercept = MathUtility.getRandomNumber_1Digit();

        LinearFunction Function = new LinearFunction(Slope, Intercept);
        LinearFunction ReflectedFunction = new LinearFunction(0 - Slope, 0 - Intercept);

        QUESTION = "Reflect the function: " + Function.toString() + " over the x axis.";
        RIGHT_ANSWER = ReflectedFunction.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  REFLECT Y-AXIS
    private void createProblem_06(){
        int Slope = MathUtility.getRandomNumber_1Digit();
        int Intercept = MathUtility.getRandomNumber_1Digit();

        LinearFunction Function = new LinearFunction(Slope, Intercept);
        LinearFunction ReflectedFunction = new LinearFunction(0 - Slope, Intercept);

        QUESTION = "Reflect the function: " + Function.toString() + " over the y axis.";
        RIGHT_ANSWER = ReflectedFunction.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  SHIFT UP + LEFT
    private void createProblem_07(){
        int Slope = MathUtility.getRandomNumber_1Digit();
        int Intercept = MathUtility.getRandomNumber_1Digit();

        int ShiftingUpUnit = MathUtility.getRandomPositiveNumber_1Digit();
        int ShiftingLeftUnit = MathUtility.getRandomPositiveNumber_1Digit();
        LinearFunction Function = new LinearFunction(Slope, Intercept);
        LinearFunction ShiftedFunction = new LinearFunction(FunctionMath.add(new LinearFunction(FunctionMath.add(Function, ShiftingUpUnit)),
                Slope * ShiftingLeftUnit));

        QUESTION = "Shift the function: " + Function.toString() + " up " + String.valueOf(ShiftingUpUnit) + " units, and left " + String.valueOf(ShiftingLeftUnit) + " units.";
        RIGHT_ANSWER = ShiftedFunction.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  SHIFT DOWN + RIGHT
    private void createProblem_08(){
        int Slope = MathUtility.getRandomNumber_1Digit();
        int Intercept = MathUtility.getRandomNumber_1Digit();

        int ShiftingDownUnit = MathUtility.getRandomPositiveNumber_1Digit();
        int ShiftingRightUnit = MathUtility.getRandomPositiveNumber_1Digit();
        LinearFunction Function = new LinearFunction(Slope, Intercept);
        LinearFunction ShiftedFunction = new LinearFunction(FunctionMath.sub(new LinearFunction(FunctionMath.sub(Function, ShiftingDownUnit)),
                Slope * ShiftingRightUnit));

        QUESTION = "Shift the function: " + Function.toString() + " down " + String.valueOf(ShiftingDownUnit) + " units, and right " + String.valueOf(ShiftingRightUnit) + " units.";
        RIGHT_ANSWER = ShiftedFunction.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  SHIFT DOWN + LEFT + REFLECT X-AXIS
    private void createProblem_09(){
        int Slope = MathUtility.getRandomNumber_1Digit();
        int Intercept = MathUtility.getRandomNumber_1Digit();

        int ShiftingDownUnit = MathUtility.getRandomPositiveNumber_1Digit();
        int ShiftingLeftUnit = MathUtility.getRandomPositiveNumber_1Digit();

        LinearFunction Function = new LinearFunction(Slope, Intercept);
        LinearFunction Result = new LinearFunction(
                FunctionMath.mul(
                        new LinearFunction(
                                FunctionMath.add(
                                        new LinearFunction(
                                                FunctionMath.sub(Function, ShiftingDownUnit)),
                                        ShiftingLeftUnit * Function.getSlope())),
                        -1)
        );

        QUESTION = "Shift the function " + Function.toString() + " down " + String.valueOf(ShiftingDownUnit)
                + " units, shift to the left " + String.valueOf(ShiftingLeftUnit) + " units, and reflect it over the x-axis.";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  SHIFT UP + RIGHT + REFLECT X-AXIS
    private void createProblem_10(){
        int Slope = MathUtility.getRandomNumber_1Digit();
        int Intercept = MathUtility.getRandomNumber_1Digit();

        int ShiftingUpUnit = MathUtility.getRandomPositiveNumber_1Digit();
        int ShiftingRightUnit = MathUtility.getRandomPositiveNumber_1Digit();

        LinearFunction Function = new LinearFunction(Slope, Intercept);
        LinearFunction Result = new LinearFunction(
                FunctionMath.mul(
                        new LinearFunction(
                                FunctionMath.sub(
                                        new LinearFunction(
                                                FunctionMath.add(Function, ShiftingUpUnit)), Function.getSlope() * ShiftingRightUnit)), -1));

        QUESTION = "Shift the function " + Function.toString() + " up " + String.valueOf(ShiftingUpUnit)
                + " units, shift to the right " + String.valueOf(ShiftingRightUnit) + " units, and reflect it over the x-axis.";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  SHIFT UP + LEFT + REFLECT Y-AXIS
    private void createProblem_11(){
        int Slope = MathUtility.getRandomNumber_1Digit();
        int Intercept = MathUtility.getRandomNumber_1Digit();

        int ShiftingUpUnit = MathUtility.getRandomPositiveNumber_1Digit();
        int ShiftingLeftUnit = MathUtility.getRandomPositiveNumber_1Digit();

        LinearFunction Function = new LinearFunction(Slope, Intercept);
        LinearFunction Result = new LinearFunction(
                FunctionMath.add(
                        new LinearFunction(FunctionMath.add(Function, ShiftingUpUnit)), Function.getSlope() * ShiftingLeftUnit));
        Result.setSlope(Result.getSlope() * -1);

        QUESTION = "Shift the function " + Function.toString() + " up " + String.valueOf(ShiftingUpUnit)
                + " units, shift to the left " + String.valueOf(ShiftingLeftUnit) + " units, and reflect it over the y-axis.";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  SHIFT DOWN + RIGHT + REFLECT Y-AXIS
    private void createProblem_12(){
        int Slope = MathUtility.getRandomNumber_1Digit();
        int Intercept = MathUtility.getRandomNumber_1Digit();

        int ShiftingDownUnit = MathUtility.getRandomPositiveNumber_1Digit();
        int ShiftingRightUnit = MathUtility.getRandomPositiveNumber_1Digit();

        LinearFunction Function = new LinearFunction(Slope, Intercept);
        LinearFunction Result = new LinearFunction(
                FunctionMath.sub(new LinearFunction(FunctionMath.sub(Function, ShiftingDownUnit)), Function.getSlope() * ShiftingRightUnit));
        Result.setSlope(Result.getSlope() * -1);

        QUESTION = "Shift the function " + Function.toString() + " down " + String.valueOf(ShiftingDownUnit)
                + " units, shift to the right " + String.valueOf(ShiftingRightUnit) + " units, and reflect it over the y-axis.";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }
    
    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
