package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction;

import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicExpression;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.Variable;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckCubic.pckFunction.CubicFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckLinearFunction.LinearFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckFunction.QuadraticFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuartic.pckFunction.QuarticFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/22/2017.
 */

public class FunctionMath {

    private static final double LEFT_BOUND = -10000.0;
    private static final double RIGHT_BOUND = 10000.0;
    private static final double ERROR_MARGIN = Math.pow(10, -6);

    public static LinearFunction getRandomLinearFunction(){
        return new LinearFunction(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
    }

    public static QuadraticFunction getRandomQuadraticFunction(){
        return new QuadraticFunction(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit());
    }

    public static CubicFunction getRandomCubicFunction(){
        return new CubicFunction(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit());
    }

    public static QuarticFunction getRandomQuarticFunction(){
        return new QuarticFunction(MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit(),
                MathUtility.getRandomNumber_1Digit());
    }

    public static double getYValue(CubicFunction Function, double XValue){
        return Function.getACoefficient() * Math.pow(XValue, 3)
                + Function.getBCoefficient() * Math.pow(XValue, 2)
                + Function.getCCoefficient() * XValue
                + Function.getDCoefficient();
    }

    public static Point getRandomPoint(LinearFunction LF){
        int XCoordinate = MathUtility.getRandomNumber_1Digit();
        int YCoordinate = LF.getSlope() * XCoordinate + LF.getYIntercept();

        return new Point(XCoordinate, YCoordinate);
    }

    public static Point getRandomPoint(QuadraticFunction QF){
        int XCoordinate = MathUtility.getRandomNumber_1Digit();
        int YCoordinate = QF.getACoefficient() * XCoordinate * XCoordinate
                + QF.getBCoefficient() * XCoordinate + QF.getCCoefficient();

        return new Point(XCoordinate, YCoordinate);
    }

    public static Point getRandomPoint(CubicFunction CF){
        int XCoordinate = MathUtility.getRandomNumber_1Digit();
        int YCoordinate = CF.getACoefficient() * XCoordinate * XCoordinate * XCoordinate
                + CF.getBCoefficient() * XCoordinate * XCoordinate
                + CF.getCCoefficient() * XCoordinate
                + CF.getDCoefficient();

        return new Point(XCoordinate, YCoordinate);
    }

    public static Point getRandomPoint(LinearFunction LF, int XCoordinate){
        int YCoordinate = LF.getSlope() * XCoordinate + LF.getYIntercept();

        return new Point(XCoordinate, YCoordinate);
    }

    public static Point getRandomPoint(QuadraticFunction QF, int XCoordinate){
        int YCoordinate = QF.getACoefficient() * XCoordinate * XCoordinate
                + QF.getBCoefficient() * XCoordinate + QF.getCCoefficient();

        return new Point(XCoordinate, YCoordinate);
    }

    public static Point getRandomPoint(CubicFunction CF, int XCoordinate){
        int YCoordinate = CF.getACoefficient() * XCoordinate * XCoordinate * XCoordinate
                + CF.getBCoefficient() * XCoordinate * XCoordinate
                + CF.getCCoefficient() * XCoordinate
                + CF.getDCoefficient();

        return new Point(XCoordinate, YCoordinate);
    }

    public static Point getQuadraticVertex(QuadraticFunction QF){
        Fraction XCoordinate = new Fraction(0 - QF.getBCoefficient(), 2 * QF.getACoefficient());
        XCoordinate = FractionMath.getSimplifiedFraction(XCoordinate);

        Fraction YCoordinate =
                FractionMath.addFraction(
                FractionMath.multiplyFraction(new Fraction(QF.getACoefficient()),
                        FractionMath.multiplyFraction(XCoordinate, XCoordinate)),
                        FractionMath.addFraction(FractionMath.multiplyFraction(new Fraction(QF.getBCoefficient()), XCoordinate),
                                new Fraction(QF.getCCoefficient())));
        YCoordinate = FractionMath.getSimplifiedFraction(YCoordinate);

        return new Point(XCoordinate, YCoordinate);
    }

    //  ADDING
    public static Polynomial add(MathFunction Function, int Number){
        return AlgebraMath.add(Function.getPolynomial(), Number);
    }

    public static Polynomial add(MathFunction FunctionA, MathFunction FunctionB){
        return AlgebraMath.add(FunctionA.getPolynomial(), FunctionB.getPolynomial());
    }

    //  SUBTRACTING
    public static Polynomial sub(MathFunction Function, int Number){
        return AlgebraMath.add(Function.getPolynomial(), 0 - Number);
    }

    public static Polynomial sub(MathFunction FunctionA, MathFunction FunctionB){
        return AlgebraMath.sub(FunctionA.getPolynomial(), FunctionB.getPolynomial());
    }

    //  MULTIPLYING
    public static Polynomial mul(MathFunction Function, int Number){
        return AlgebraMath.mul(Function.getPolynomial(), Number);
    }

    public static Polynomial mul(MathFunction FunctionA, MathFunction FunctionB){
        return AlgebraMath.mul(FunctionA.getPolynomial(), FunctionB.getPolynomial());
    }

    //  COMPOSING
    public static LinearFunction composite(LinearFunction FunctionA, LinearFunction FunctionB){
        return new LinearFunction(AlgebraMath.add(AlgebraMath.mul(FunctionB.getPolynomial(), FunctionA.getSlope()), FunctionA.getYIntercept()));
    }

    public static QuadraticFunction composite(QuadraticFunction FunctionA, LinearFunction FunctionB){
        Polynomial FirstPol = AlgebraMath.mul(AlgebraMath.mul(FunctionB.getPolynomial(), FunctionB.getPolynomial()),
                FunctionA.getACoefficient());

        Polynomial SecondPol = AlgebraMath.mul(FunctionB.getPolynomial(), FunctionA.getBCoefficient());

        Polynomial ThirdPol = new Polynomial();
        AlgebraicNumber TempNumber = new AlgebraicNumber();
        TempNumber.setCoefficient(FunctionA.getCCoefficient());
        TempNumber.addVariable(new Variable("x", 0));
        AlgebraicExpression TempExpression = new AlgebraicExpression();
        TempExpression.addNumber(TempNumber);
        ThirdPol.setExpression(TempExpression);

        AlgebraicExpression Expression = new AlgebraicExpression();
        Expression.addNumber(FirstPol.getExpression().getNumberList());
        Expression.addNumber(SecondPol.getExpression().getNumberList());
        Expression.addNumber(ThirdPol.getExpression().getNumberList());
        Expression = AlgebraMath.combineLikeTerms(Expression);

        Polynomial QuadraticPol = new Polynomial();
        QuadraticPol.setExpression(Expression);

        return new QuadraticFunction(QuadraticPol);
    }

    public static QuadraticFunction composite(LinearFunction FunctionA, QuadraticFunction FunctionB){
        Polynomial FirstPol = AlgebraMath.mul(FunctionB.getPolynomial(), FunctionA.getSlope());

        Polynomial SecondPol = new Polynomial();
        AlgebraicNumber TempNumber = new AlgebraicNumber();
        TempNumber.setCoefficient(FunctionA.getYIntercept());
        TempNumber.addVariable(new Variable("x", 0));
        AlgebraicExpression TempExpression = new AlgebraicExpression();
        TempExpression.addNumber(TempNumber);
        SecondPol.setExpression(TempExpression);

        AlgebraicExpression Expression = new AlgebraicExpression();
        Expression.addNumber(FirstPol.getExpression().getNumberList());
        Expression.addNumber(SecondPol.getExpression().getNumberList());
        Expression = AlgebraMath.combineLikeTerms(Expression);

        Polynomial QuadraticPol = new Polynomial();
        QuadraticPol.setExpression(Expression);

        return new QuadraticFunction(QuadraticPol);
    }

    public static CubicFunction composite(CubicFunction FunctionA, LinearFunction FunctionB){
        Polynomial FirstPol = AlgebraMath.mul(AlgebraMath.mul(
                AlgebraMath.mul(FunctionB.getPolynomial(), FunctionB.getPolynomial()), FunctionB.getPolynomial()),
                FunctionA.getACoefficient());

        Polynomial SecondPol = AlgebraMath.mul(AlgebraMath.mul(FunctionB.getPolynomial(), FunctionB.getPolynomial()),
                FunctionA.getBCoefficient());

        Polynomial ThirdPol = AlgebraMath.mul(FunctionB.getPolynomial(), FunctionA.getCCoefficient());

        Polynomial ForthPol = new Polynomial();
        AlgebraicNumber TempNumber = new AlgebraicNumber();
        TempNumber.setCoefficient(FunctionA.getDCoefficient());
        TempNumber.addVariable(new Variable("x", 0));
        AlgebraicExpression TempExpression = new AlgebraicExpression();
        TempExpression.addNumber(TempNumber);
        ForthPol.setExpression(TempExpression);

        AlgebraicExpression Expression = new AlgebraicExpression();
        Expression.addNumber(FirstPol.getExpression().getNumberList());
        Expression.addNumber(SecondPol.getExpression().getNumberList());
        Expression.addNumber(ThirdPol.getExpression().getNumberList());
        Expression.addNumber(ForthPol.getExpression().getNumberList());
        Expression = AlgebraMath.combineLikeTerms(Expression);

        Polynomial QuadraticPol = new Polynomial();
        QuadraticPol.setExpression(Expression);

        return new CubicFunction(QuadraticPol);
    }

    public static CubicFunction composite(LinearFunction FunctionA, CubicFunction FunctionB){
        Polynomial FirstPol = AlgebraMath.mul(FunctionB.getPolynomial(), FunctionA.getSlope());

        Polynomial SecondPol = new Polynomial();
        AlgebraicNumber TempNumber = new AlgebraicNumber();
        TempNumber.setCoefficient(FunctionA.getYIntercept());
        TempNumber.addVariable(new Variable("x", 0));
        AlgebraicExpression TempExpression = new AlgebraicExpression();
        TempExpression.addNumber(TempNumber);
        SecondPol.setExpression(TempExpression);

        AlgebraicExpression Expression = new AlgebraicExpression();
        Expression.addNumber(FirstPol.getExpression().getNumberList());
        Expression.addNumber(SecondPol.getExpression().getNumberList());
        Expression = AlgebraMath.combineLikeTerms(Expression);

        Polynomial CubicPol = new Polynomial();
        CubicPol.setExpression(Expression);

        return new CubicFunction(CubicPol);
    }

    public static Polynomial composite(CubicFunction FunctionA, QuadraticFunction FunctionB){
        Polynomial FirstPol = AlgebraMath.mul(AlgebraMath.mul(
                AlgebraMath.mul(FunctionB.getPolynomial(), FunctionB.getPolynomial()), FunctionB.getPolynomial()),
                FunctionA.getACoefficient());

        Polynomial SecondPol = AlgebraMath.mul(AlgebraMath.mul(FunctionB.getPolynomial(), FunctionB.getPolynomial()),
                FunctionA.getBCoefficient());

        Polynomial ThirdPol = AlgebraMath.mul(FunctionB.getPolynomial(), FunctionA.getCCoefficient());

        Polynomial ForthPol = new Polynomial();
        AlgebraicNumber TempNumber = new AlgebraicNumber();
        TempNumber.setCoefficient(FunctionA.getDCoefficient());
        TempNumber.addVariable(new Variable("x", 0));
        AlgebraicExpression TempExpression = new AlgebraicExpression();
        TempExpression.addNumber(TempNumber);
        ForthPol.setExpression(TempExpression);

        AlgebraicExpression Expression = new AlgebraicExpression();
        Expression.addNumber(FirstPol.getExpression().getNumberList());
        Expression.addNumber(SecondPol.getExpression().getNumberList());
        Expression.addNumber(ThirdPol.getExpression().getNumberList());
        Expression.addNumber(ForthPol.getExpression().getNumberList());
        Expression = AlgebraMath.combineLikeTerms(Expression);

        Polynomial CubicPol = new Polynomial();
        CubicPol.setExpression(Expression);

        return CubicPol;
    }

    public static double findTheZero(CubicFunction Function){
        double XValue = 0.0, left_bound = LEFT_BOUND, right_bound = RIGHT_BOUND;

        while(true){
            double MID_POINT = (left_bound + right_bound) / 2.0;
            if (getYValue(Function, left_bound) * getYValue(Function, MID_POINT) < 0){
                right_bound = MID_POINT;
            }
            else {
                left_bound = MID_POINT;
            }

            if (right_bound - left_bound <= ERROR_MARGIN){
                XValue = left_bound;
                break;
            }
        }

        XValue = MathUtility.round_4_Decimal(XValue);
        return XValue;
    }

}
