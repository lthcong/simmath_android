package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation;

import java.util.ArrayList;

import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/23/2017.
 */

public class QuadraticMath {

    public static ArrayList<String> getSolution(QuadraticEquation Equation){
        ArrayList<String> SolutionSet = new ArrayList<>();
        String SolutionA, SolutionB;

        int a = Equation.getACoefficient();
        int b = Equation.getBCoefficient();
        int c = Equation.getCCoefficient();

        //  delta = b^2 - 4ac
        int Delta = b * b - 4 * a * c;

        //  x = [-b +/- sqrt(delta)] / 2a
        if (MathUtility.isSquaredNumber(Math.abs(Delta))){
            int SqrtDelta = (int) Math.sqrt(Math.abs(Delta));

            if (Delta >= 0) {
                SolutionA = FractionMath.getSimplifiedFraction(new Fraction(0 - b + SqrtDelta, 2 * a)).toString();
                SolutionB = FractionMath.getSimplifiedFraction(new Fraction(0 - b - SqrtDelta, 2 * a)).toString();
            }
            else {
                SolutionA = InfoCollector.getStartNumerator() + "(" + String.valueOf(0 - b) + " + i * " + String.valueOf(SqrtDelta) + ")" + InfoCollector.getEndNumerator()
                        + "/"
                        + InfoCollector.getStartDenominator() + String.valueOf(2 * a) + InfoCollector.getEndDenominator();

                SolutionB = InfoCollector.getStartNumerator() + "(" + String.valueOf(0 - b) + " - i * " + String.valueOf(SqrtDelta) + ")" + InfoCollector.getEndNumerator()
                        + "/"
                        + InfoCollector.getStartDenominator() + String.valueOf(2 * a) + InfoCollector.getEndDenominator();
            }
        }
        else {

            if (Delta > 0) {
                SolutionA = InfoCollector.getStartNumerator()
                        + "(" + String.valueOf(0 - b) + " + " + InfoCollector.getSquareRootSign() + "(" + String.valueOf(Delta) + "))"
                        + InfoCollector.getEndNumerator()
                        + "/"
                        + InfoCollector.getStartDenominator() + String.valueOf(2 * a) + InfoCollector.getEndDenominator();

                SolutionB = InfoCollector.getStartNumerator()
                        + "(" + String.valueOf(0 - b) + " - " + InfoCollector.getSquareRootSign() + "(" + String.valueOf(Delta) + "))"
                        + InfoCollector.getEndNumerator()
                        + "/"
                        + InfoCollector.getStartDenominator() + String.valueOf(2 * a) + InfoCollector.getEndDenominator();
            }
            else {
                SolutionA = InfoCollector.getStartNumerator()
                        + "(" + String.valueOf(0 - b) + " + i * " + InfoCollector.getSquareRootSign() + "(" + String.valueOf(Delta) + "))"
                        + InfoCollector.getEndNumerator()
                        + "/"
                        + InfoCollector.getStartDenominator() + String.valueOf(2 * a) + InfoCollector.getEndDenominator();

                SolutionB = InfoCollector.getStartNumerator()
                        + "(" + String.valueOf(0 - b) + " - i * " + InfoCollector.getSquareRootSign() + "(" + String.valueOf(Delta) + "))"
                        + InfoCollector.getEndNumerator()
                        + "/"
                        + InfoCollector.getStartDenominator() + String.valueOf(2 * a) + InfoCollector.getEndDenominator();
            }
        }

        SolutionSet.add("x = " + SolutionA);

        if (!SolutionA.equalsIgnoreCase(SolutionB)){
            SolutionSet.add("x = " + SolutionB);
        }

        return SolutionSet;
    }

    public static ArrayList<Double> getDecimalSolution(QuadraticEquation Equation){
        ArrayList<Double> SolutionSet = new ArrayList<>();
        double SolutionA, SolutionB;

        int a = Equation.getACoefficient();
        int b = Equation.getBCoefficient();
        int c = Equation.getCCoefficient();

        //  delta = b^2 - 4ac
        int Delta = b * b - 4 * a * c;

        //  x = [-b +/- sqrt(delta)] / 2a
        if (Delta >= 0){
            SolutionA = ((double) (0 - b) + Math.sqrt(Delta)) / (double) (2 * a);
            SolutionB = ((double) (0 - b) - Math.sqrt(Delta)) / (double) (2 * a);

            double Temp = SolutionA;
            if (SolutionA > SolutionB){
                SolutionA = SolutionB;
                SolutionB = Temp;
            }
        }
        else {
            SolutionA = (double) Integer.MIN_VALUE;
            SolutionB = (double) Integer.MAX_VALUE;
        }

        SolutionSet.add(MathUtility.round_2_Decimal(SolutionA));
        SolutionSet.add(MathUtility.round_2_Decimal(SolutionB));

        return SolutionSet;
    }

}
