package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckFunction;

import pckInfo.InfoCollector;
import pckMath.*;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.FunctionMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckLinearFunction.LinearFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckRationalPolynomial.RationalPolynomial;

/**
 * Created by Cong on 7/29/2017.
 */

public class ManipulatingQuadraticFunction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public ManipulatingQuadraticFunction(){
        createProblem();
        swapAnswer();
    }

    public ManipulatingQuadraticFunction(String Question,
                                         String RightAnswer,
                                         String AnswerA,
                                         String AnswerB,
                                         String AnswerC,
                                         String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 10;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            default:
                createProblem_01();
                break;
        }

        return new ManipulatingQuadraticFunction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  ADD: QUAR + LIN
    private void createProblem_01(){
        QuadraticFunction FunctionA = FunctionMath.getRandomQuadraticFunction();
        LinearFunction FunctionB = FunctionMath.getRandomLinearFunction();

        QuadraticFunction Result = new QuadraticFunction(FunctionMath.add(FunctionA, FunctionB));

        QUESTION = "Given: f(x) = " + FunctionA.toString() + "<br>g(x) = " + FunctionB.toString()
                + "<br>Find the result of (f + g)(x).";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  ADD: QUAR + QUAR
    private void createProblem_02(){
        QuadraticFunction FunctionA = FunctionMath.getRandomQuadraticFunction();
        QuadraticFunction FunctionB = FunctionMath.getRandomQuadraticFunction();

        QuadraticFunction Result = new QuadraticFunction(FunctionMath.add(FunctionA, FunctionB));

        QUESTION = "Given: f(x) = " + FunctionA.toString() + "<br>g(x) = " + FunctionB.toString()
                + "<br>Find the result of (f + g)(x).";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  SUB: QUAR - LIN
    private void createProblem_03(){
        QuadraticFunction FunctionA = FunctionMath.getRandomQuadraticFunction();
        LinearFunction FunctionB = FunctionMath.getRandomLinearFunction();

        QuadraticFunction Result = new QuadraticFunction(FunctionMath.sub(FunctionA, FunctionB));

        QUESTION = "Given: f(x) = " + FunctionA.toString() + "<br>g(x) = " + FunctionB.toString()
                + "<br>Find the result of (f - g)(x).";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  SUB: QUAR - QUAR
    private void createProblem_04(){
        QuadraticFunction FunctionA = FunctionMath.getRandomQuadraticFunction();
        QuadraticFunction FunctionB = FunctionMath.getRandomQuadraticFunction();

        QuadraticFunction Result = new QuadraticFunction(FunctionMath.sub(FunctionA, FunctionB));

        QUESTION = "Given: f(x) = " + FunctionA.toString() + "<br>g(x) = " + FunctionB.toString()
                + "<br>Find the result of (f - g)(x).";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  MUL: QUAR x LIN
    private void createProblem_05(){
        QuadraticFunction FunctionA = FunctionMath.getRandomQuadraticFunction();
        LinearFunction FunctionB = FunctionMath.getRandomLinearFunction();

        Polynomial Result = FunctionMath.mul(FunctionA, FunctionB);

        QUESTION = "Given: f(x) = " + FunctionA.toString() + "<br>g(x) = " + FunctionB.toString()
                + "<br>Find the result of (f * g)(x).";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  MUL: QUAR x QUAR
    private void createProblem_06(){
        QuadraticFunction FunctionA = FunctionMath.getRandomQuadraticFunction();
        QuadraticFunction FunctionB = FunctionMath.getRandomQuadraticFunction();

        Polynomial Result = FunctionMath.mul(FunctionA, FunctionB);

        QUESTION = "Given: f(x) = " + FunctionA.toString() + "<br>g(x) = " + FunctionB.toString()
                + "<br>Find the result of (f * g)(x).";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  DIV: QUAR / LIN
    private void createProblem_07(){
        LinearFunction FunctionA = FunctionMath.getRandomLinearFunction();
        LinearFunction Temp = FunctionMath.getRandomLinearFunction();

        QuadraticFunction FunctionB = new QuadraticFunction(FunctionMath.mul(FunctionA, Temp));

        QUESTION = "Given: f(x) = " + FunctionB.toString() + "<br>g(x) = " + FunctionA.toString()
                + "<br>Find the result of (f " + InfoCollector.getDivisionSign() + " g)(x).";
        RIGHT_ANSWER = Temp.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  DIV: QUAR / QUAR
    private void createProblem_08(){
        LinearFunction ResultTop = FunctionMath.getRandomLinearFunction();
        LinearFunction ResultBottom = FunctionMath.getRandomLinearFunction();
        LinearFunction Common = FunctionMath.getRandomLinearFunction();

        QuadraticFunction FunctionA = new QuadraticFunction(FunctionMath.mul(ResultTop, Common));
        QuadraticFunction FunctionB = new QuadraticFunction(FunctionMath.mul(ResultBottom, Common));

        QUESTION = "Given: f(x) = " + FunctionB.toString() + "<br>g(x) = " + FunctionA.toString()
                + "<br>Find the result of (f " + InfoCollector.getDivisionSign() + " g)(x).";
        RIGHT_ANSWER = (new RationalPolynomial(ResultTop.getPolynomial(), ResultBottom.getPolynomial())).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new RationalPolynomial(AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1), AlgebraMath.getRandomPolynomialWithHighestExponent("x" , 1))).toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  COM: QUAR vs LIN
    private void createProblem_09(){
        QuadraticFunction FunctionA = FunctionMath.getRandomQuadraticFunction();
        LinearFunction FunctionB = FunctionMath.getRandomLinearFunction();

        QuadraticFunction Result = FunctionMath.composite(FunctionA, FunctionB);

        QUESTION = "Given: f(x) = " + FunctionA.toString() + "<br>g(x) = " + FunctionB.toString()
                + "<br>Find the result of (f o g)(x).";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  COM: LIN vs QUAR
    private void createProblem_10(){
        QuadraticFunction FunctionA = FunctionMath.getRandomQuadraticFunction();
        LinearFunction FunctionB = FunctionMath.getRandomLinearFunction();

        QuadraticFunction Result = FunctionMath.composite(FunctionB, FunctionA);

        QUESTION = "Given: f(x) = " + FunctionA.toString() + "<br>g(x) = " + FunctionB.toString()
                + "<br>Find the result of (g o f)(x).";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
