package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckSet;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.Set;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.SetMath;

/**
 * Created by Cong on 7/20/2017.
 */

public class FindingTheResultOfASetOperation extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingTheResultOfASetOperation(){
        createProblem();
        swapAnswer();
    }

    public FindingTheResultOfASetOperation(String Question,
                                        String RightAnswer,
                                        String AnswerA,
                                        String AnswerB,
                                        String AnswerC,
                                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 13;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FindingTheResultOfASetOperation(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  FIND THE SUB SET OF A INTERSECT B
    private void createProblem_01(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set Result = SetMath.findSubSet(SetMath.findTheIntersection(SetA, SetB));

        QUESTION = "A = " + SetA.toString() + ".<br>"
                + "B = " + SetB.toString() + ".<br>"
                + "C " + InfoCollector.getSubSet() + " (A " + InfoCollector.getIntersectionSet() + " B). C = ?";

        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SetMath.findSubSet(SetMath.getRandomSet()).toString();
        ANSWER_C = SetB.toString();
        ANSWER_D = SetMath.findTheIntersection(SetA, SetB).toString();
    }

    //  FIND THE SUB SET OF A UNION B
    private void createProblem_02(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set Result = SetMath.findSubSet(SetMath.findTheUnion(SetA, SetB));

        QUESTION = "A = " + SetA.toString() + ".<br>"
                + "B = " + SetB.toString() + ".<br>"
                + "C " + InfoCollector.getSubSet() + " (A " + InfoCollector.getUnionSet() + " B). C = ?";

        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SetMath.findSubSet(SetMath.getRandomSet()).toString();
        ANSWER_C = SetB.toString();
        ANSWER_D = SetMath.findTheIntersection(SetA, SetB).toString();
    }

    //  FIND THE SUB SET OF A DIFFERENT B
    private void createProblem_03(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set Result = SetMath.findSubSet(SetMath.findTheDifferent(SetA, SetB));

        QUESTION = "A = " + SetA.toString() + ".<br>"
                + "B = " + SetB.toString() + ".<br>"
                + "C " + InfoCollector.getSubSet() + " (A " + InfoCollector.getDifferentOfSet() + " B). C = ?";

        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SetMath.findSubSet(SetMath.getRandomSet()).toString();
        ANSWER_C = SetB.toString();
        ANSWER_D = SetMath.findTheIntersection(SetA, SetB).toString();
    }

    //  (A INTERSECT B) UNION (A INTERSECT C)
    private void createProblem_04(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set SetC = SetMath.getRandomSet();

        Set Result = SetMath.findTheUnion(SetMath.findTheIntersection(SetA, SetB), SetMath.findTheIntersection(SetA, SetC));

        QUESTION = "A = " + SetA.toString() + ".<br>"
                + "B = " + SetB.toString() + ".<br>"
                + "C = " + SetC.toString() + ".<br>"
                + "(A " + InfoCollector.getIntersectionSet() + " B) " + InfoCollector.getUnionSet() 
                +  " (A " + InfoCollector.getIntersectionSet() + " C) = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SetMath.findSubSet(SetA).toString();
        ANSWER_C = SetMath.findSubSet(SetB).toString();
        ANSWER_D = SetMath.findTheUnion(SetB, SetC).toString();
    }

    //  (A INTERSECT B) INTERSECT (A INTERSECT C)
    private void createProblem_05(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set SetC = SetMath.getRandomSet();

        Set Result = SetMath.findTheIntersection(SetMath.findTheIntersection(SetA, SetB), SetMath.findTheIntersection(SetA, SetC));

        QUESTION = "A = " + SetA.toString() + ".<br>"
                + "B = " + SetB.toString() + ".<br>"
                + "C = " + SetC.toString() + ".<br>"
                + "(A " + InfoCollector.getIntersectionSet() + " B) " + InfoCollector.getIntersectionSet()
                +  " (A " + InfoCollector.getIntersectionSet() + " C) = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SetMath.findSubSet(SetA).toString();
        ANSWER_C = SetMath.findSubSet(SetB).toString();
        ANSWER_D = SetMath.findTheUnion(SetB, SetC).toString();
    }

    //  (A INTERSECT B) DIFFERENT (A INTERSECT C)
    private void createProblem_06(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set SetC = SetMath.getRandomSet();

        Set Result = SetMath.findTheDifferent(SetMath.findTheIntersection(SetA, SetB), SetMath.findTheIntersection(SetA, SetC));

        QUESTION = "A = " + SetA.toString() + ".<br>"
                + "B = " + SetB.toString() + ".<br>"
                + "C = " + SetC.toString() + ".<br>"
                + "(A " + InfoCollector.getIntersectionSet() + " B) " + InfoCollector.getDifferentOfSet()
                +  " (A " + InfoCollector.getIntersectionSet() + " C) = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SetMath.findSubSet(SetA).toString();
        ANSWER_C = SetMath.findSubSet(SetB).toString();
        ANSWER_D = SetMath.findTheUnion(SetB, SetC).toString();
    }

    //  (A DIFFERENT B) UNION (A DIFFERENT C)
    private void createProblem_07(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set SetC = SetMath.getRandomSet();

        Set Result = SetMath.findTheUnion(SetMath.findTheDifferent(SetA, SetB), SetMath.findTheDifferent(SetA, SetC));

        QUESTION = "A = " + SetA.toString() + ".<br>"
                + "B = " + SetB.toString() + ".<br>"
                + "C = " + SetC.toString() + ".<br>"
                + "(A " + InfoCollector.getDifferentOfSet() + " B) " + InfoCollector.getUnionSet()
                +  " (A " + InfoCollector.getDifferentOfSet() + " C) = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SetMath.findSubSet(SetA).toString();
        ANSWER_C = SetMath.findSubSet(SetB).toString();
        ANSWER_D = SetMath.findTheUnion(SetB, SetC).toString();
    }

    //  (A UNION B) UNION C
    private void createProblem_08(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set SetC = SetMath.getRandomSet();

        Set Result = SetMath.findTheUnion(SetMath.findTheUnion(SetA, SetB), SetC);

        QUESTION = "A = " + SetA.toString() + ".<br>"
                + "B = " + SetB.toString() + ".<br>"
                + "C = " + SetC.toString() + ".<br>"
                + "(A " + InfoCollector.getUnionSet() + " B) " + InfoCollector.getUnionSet() +  " C) = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SetMath.findSubSet(SetA).toString();
        ANSWER_C = SetMath.findSubSet(SetB).toString();
        ANSWER_D = SetMath.findTheUnion(SetB, SetC).toString();
    }

    //  (A INTERSECT B) INTERSECT C
    private void createProblem_09(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set SetC = SetMath.getRandomSet();

        Set Result = SetMath.findTheIntersection(SetMath.findTheIntersection(SetA, SetB), SetC);

        QUESTION = "A = " + SetA.toString() + ".<br>"
                + "B = " + SetB.toString() + ".<br>"
                + "C = " + SetC.toString() + ".<br>"
                + "(A " + InfoCollector.getIntersectionSet() + " B) " + InfoCollector.getIntersectionSet() +  " C) = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SetMath.findSubSet(SetA).toString();
        ANSWER_C = SetMath.findSubSet(SetB).toString();
        ANSWER_D = SetMath.findTheUnion(SetB, SetC).toString();
    }

    //  (A UNION B) INTERSECT C
    private void createProblem_10(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set SetC = SetMath.getRandomSet();

        Set Result = SetMath.findTheIntersection(SetMath.findTheUnion(SetA, SetB), SetC);

        QUESTION = "A = " + SetA.toString() + ".<br>"
                + "B = " + SetB.toString() + ".<br>"
                + "C = " + SetC.toString() + ".<br>"
                + "(A " + InfoCollector.getUnionSet() + " B) " + InfoCollector.getIntersectionSet() +  " C) = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SetMath.findSubSet(SetA).toString();
        ANSWER_C = SetMath.findSubSet(SetB).toString();
        ANSWER_D = SetMath.findTheUnion(SetB, SetC).toString();
    }

    //  C DIFFERENT (A UNION B)
    private void createProblem_11(){
        Set SetA = SetMath.getRandomSet();
        Set SetB = SetMath.getRandomSet();
        Set SetC = SetMath.getRandomSet();

        Set Result = SetMath.findTheDifferent(SetC, SetMath.findTheUnion(SetA, SetB));

        QUESTION = "A = " + SetA.toString() + ".<br>"
                + "B = " + SetB.toString() + ".<br>"
                + "C = " + SetC.toString() + ".<br>"
                + "C " + InfoCollector.getDifferentOfSet() + " (A " + InfoCollector.getUnionSet() +  " B) = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SetMath.findSubSet(SetA).toString();
        ANSWER_C = SetMath.findSubSet(SetB).toString();
        ANSWER_D = SetMath.findTheUnion(SetB, SetC).toString();
    }

    //  COMPLEMENT OF (SUB OF A)
    private void createProblem_12(){
        Set SuperSet = SetMath.getRandomSet();
        Set SubSet = SetMath.findSubSet(SuperSet);
        Set Result = SetMath.findComplement(SuperSet, SubSet);

        QUESTION = "A = " + SuperSet.toString() + ".> B " + InfoCollector.getSubSet() + " A.<br>B = " + SubSet.toString()
                + ". Find B" + InfoCollector.getExponentSign("C") + ".";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SuperSet.toString();
        ANSWER_C = SubSet.toString();
        ANSWER_D = SetMath.getRandomSet().toString();
    }

    //  COMPLEMENT OF (SUB OF A) UNION COMPLEMENT OF (SUB OF B)
    private void createProblem_13(){
        Set SuperA = SetMath.getRandomSet();
        Set SubA = SetMath.findSubSet(SuperA);

        Set SuperB = SetMath.getRandomSet();
        Set SubB = SetMath.findSubSet(SuperB);

        Set Result = SetMath.findTheUnion(SetMath.findComplement(SuperA, SubA), SetMath.findComplement(SuperB, SubB));

        QUESTION = "Given A = " + SuperA.toString() + ".<br>"
                + "SubA " + InfoCollector.getSubSet() + " A. SubA = " + SubA.toString() + ".<br>"
                + "B = " + SuperB.toString() + ".<br>"
                + "SubB " + InfoCollector.getSubSet() + " B. SubB = " + SubB.toString() + ".<br>"
                + "Find SubA" + InfoCollector.getExponentSign("C") + " "
                + InfoCollector.getUnionSet() + " SubB" + InfoCollector.getExponentSign("C") + ".";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = SubA.toString();
        ANSWER_C = SubB.toString();
        ANSWER_D = SuperA.toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
