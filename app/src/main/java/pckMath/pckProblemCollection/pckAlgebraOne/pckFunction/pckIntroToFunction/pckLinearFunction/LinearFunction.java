package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckLinearFunction;

import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicExpression;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.Variable;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.MathFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/22/2017.
 */

public class LinearFunction extends MathFunction{

    /*  y = ax + b  */

    private int SLOPE, Y_INTERCEPT;

    public LinearFunction(){
        SLOPE = 0;
        Y_INTERCEPT = 0;
    }

    public LinearFunction(int Slope, int YIntercept){
        SLOPE = Slope;
        Y_INTERCEPT = YIntercept;
    }

    public LinearFunction(Polynomial _Polynomial){

        if (_Polynomial.getExpression().getNumber(0).getVariable(0).isNull()){

            SLOPE = _Polynomial.getExpression().getNumber(1).getCoefficient();
            Y_INTERCEPT = _Polynomial.getExpression().getNumber(0).getCoefficient();
        }
        else {

            SLOPE = _Polynomial.getExpression().getNumber(0).getCoefficient();
            Y_INTERCEPT = _Polynomial.getExpression().getNumber(1).getCoefficient();
        }

    }

    public void setSlope(int Slope){
        SLOPE = Slope;
    }

    public int getSlope(){
        return SLOPE;
    }

    public void setYIntercept(int YIntercept){
        Y_INTERCEPT = YIntercept;
    }

    public int getYIntercept(){
        return Y_INTERCEPT;
    }

    @Override
    public String toString(){
        return "y = " + String.valueOf(SLOPE) + "x + " + String.valueOf(Y_INTERCEPT);
    }

    @Override
    public Polynomial getPolynomial(){
        AlgebraicNumber FirstNumber = new AlgebraicNumber();
        FirstNumber.setCoefficient(SLOPE);
        FirstNumber.addVariable(new Variable("x"));

        AlgebraicNumber SecondNumber = new AlgebraicNumber();
        SecondNumber.setCoefficient(Y_INTERCEPT);
        SecondNumber.addVariable(new Variable("x", 0));

        AlgebraicExpression Expression = new AlgebraicExpression();
        Expression.addNumber(FirstNumber);
        Expression.addNumber(SecondNumber);

        Polynomial LinearPolynomial = new Polynomial();
        LinearPolynomial.setExpression(Expression);

        return LinearPolynomial;
    }
}
