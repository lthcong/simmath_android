package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction;

import pckInfo.InfoCollector;

/**
 * Created by Cong on 8/11/2017.
 */

public class Inequality {

    //  a < x < b
    private String LEFT_BOUND, FIRST_SIGN, VARIABLE_NAME, SECOND_SIGN, RIGHT_BOUND;
    private boolean EQUAL_TO_LEFT_BOUND, EQUAL_TO_RIGHT_BOUND;

    public Inequality(){
        LEFT_BOUND = InfoCollector.getNegativeInfinity();
        FIRST_SIGN = "<";
        SECOND_SIGN = "<";
        RIGHT_BOUND = InfoCollector.getPositiveInfinity();

        EQUAL_TO_LEFT_BOUND = false;
        EQUAL_TO_RIGHT_BOUND = false;

        VARIABLE_NAME = "x";
    }

    public Inequality(String LeftBound, String RightBound){
        LEFT_BOUND = LeftBound;
        RIGHT_BOUND = RightBound;
        FIRST_SIGN = "<";
        SECOND_SIGN = "<";

        EQUAL_TO_LEFT_BOUND = false;
        EQUAL_TO_RIGHT_BOUND = false;

        VARIABLE_NAME = "x";
    }

    public Inequality(String LeftBound, boolean isEqualToLeftBound, boolean isEqualToRightBound, String RightBound){
        LEFT_BOUND = LeftBound;
        if (isEqualToLeftBound){
            FIRST_SIGN = InfoCollector.getLessThanOrEqualSign();
            EQUAL_TO_LEFT_BOUND = true;
        }
        else {
            FIRST_SIGN = " < ";
            EQUAL_TO_LEFT_BOUND = false;
        }

        RIGHT_BOUND = RightBound;
        if (isEqualToRightBound){
            SECOND_SIGN = InfoCollector.getLessThanOrEqualSign();
            EQUAL_TO_RIGHT_BOUND = true;
        }
        else {
            SECOND_SIGN = " < ";
            EQUAL_TO_RIGHT_BOUND = false;
        }

        VARIABLE_NAME = "x";
    }

    public Inequality(String VariableName, String LeftBound, boolean isEqualToLeftBound, boolean isEqualToRightBound, String RightBound){
        LEFT_BOUND = LeftBound;
        if (isEqualToLeftBound){
            FIRST_SIGN = InfoCollector.getLessThanOrEqualSign();
            EQUAL_TO_LEFT_BOUND = true;
        }
        else {
            FIRST_SIGN = " < ";
            EQUAL_TO_LEFT_BOUND = false;
        }

        RIGHT_BOUND = RightBound;
        if (isEqualToRightBound){
            SECOND_SIGN = InfoCollector.getLessThanOrEqualSign();
            EQUAL_TO_RIGHT_BOUND = true;
        }
        else {
            SECOND_SIGN = " < ";
            EQUAL_TO_RIGHT_BOUND = false;
        }

        VARIABLE_NAME = VariableName;
    }

    public void setLeftBound(String LeftBound){
        LEFT_BOUND = LeftBound;
    }

    public String getLeftBound(){
        return LEFT_BOUND;
    }

    public void setRightBound(String RightBound){
        RIGHT_BOUND = RightBound;
    }

    public String getRightBound(){
        return RIGHT_BOUND;
    }

    public void setVariableName(String VarName){
        VARIABLE_NAME = VarName;
    }

    public String getVariableName(){
        return VARIABLE_NAME;
    }

    public boolean getEqualToLeftBound(){
        return EQUAL_TO_LEFT_BOUND;
    }

    public boolean getEqualToRightBound(){
        return EQUAL_TO_RIGHT_BOUND;
    }

    public String toString(){
        String InequalityString;
        if (LEFT_BOUND.trim().equalsIgnoreCase(InfoCollector.getNegativeInfinity()) && RIGHT_BOUND.trim().equalsIgnoreCase(InfoCollector.getPositiveInfinity())){
            InequalityString = VARIABLE_NAME + InfoCollector.getElementOf() + "(" + InfoCollector.getNegativeInfinity() + ", " + InfoCollector.getPositiveInfinity() + ")";
        }
        else {
            if (LEFT_BOUND.trim().equalsIgnoreCase(InfoCollector.getNegativeInfinity())){
                InequalityString = VARIABLE_NAME + SECOND_SIGN + RIGHT_BOUND;
            }
            else {
                if (RIGHT_BOUND.trim().equalsIgnoreCase(InfoCollector.getPositiveInfinity())){
                    InequalityString = LEFT_BOUND + FIRST_SIGN + VARIABLE_NAME;
                }
                else {
                    InequalityString = LEFT_BOUND + FIRST_SIGN + VARIABLE_NAME + SECOND_SIGN + RIGHT_BOUND;
                }
            }
        }

        return InequalityString;
    }

}
