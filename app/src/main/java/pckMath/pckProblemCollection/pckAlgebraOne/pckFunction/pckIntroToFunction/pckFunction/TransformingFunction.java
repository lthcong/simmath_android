package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckFunction;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/22/2017.
 */

public class TransformingFunction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;


    public TransformingFunction(){
        createProblem();
        swapAnswer();
    }

    public TransformingFunction(String Question,
                             String RightAnswer,
                             String AnswerA,
                             String AnswerB,
                             String AnswerC,
                             String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 22;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            case 14:
                createProblem_15();
                break;
            case 15:
                createProblem_16();
                break;
            case 16:
                createProblem_17();
                break;
            case 17:
                createProblem_18();
                break;
            case 18:
                createProblem_19();
                break;
            case 19:
                createProblem_20();
                break;
            case 20:
                createProblem_21();
                break;
            case 21:
                createProblem_22();
                break;
            default:
                createProblem_01();
                break;
        }

        return new TransformingFunction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  SHIFT LEFT
    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Shift the graph of the function y = f(x) to the left " + String.valueOf(a) + " units.";
        RIGHT_ANSWER = "y = f(x + " + String.valueOf(a) + ").";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = f(x) + " + String.valueOf(a) + ".";
        ANSWER_C = "y = f(x - " + String.valueOf(a) + ").";
        ANSWER_D = "y = f(x) - " + String.valueOf(a) + ".";
    }

    //  SHIFT RIGHT
    private void createProblem_02(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Shift the graph of the function y = f(x) to the right " + String.valueOf(a) + " units.";
        RIGHT_ANSWER = "y = f(x - " + String.valueOf(a) + ").";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = f(x) + " + String.valueOf(a) + ".";
        ANSWER_C = "y = f(x + " + String.valueOf(a) + ").";
        ANSWER_D = "y = f(x) - " + String.valueOf(a) + ".";
    }

    //  SHIFT UP
    private void createProblem_03(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Shift the graph of the function y = f(x) to the up " + String.valueOf(a) + " units.";
        RIGHT_ANSWER = "y = f(x) + " + String.valueOf(a) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = f(x - " + String.valueOf(a) + ").";
        ANSWER_C = "y = f(x + " + String.valueOf(a) + ").";
        ANSWER_D = "y = f(x) - " + String.valueOf(a) + ".";
    }

    //  SHIFT DOWN
    private void createProblem_04(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Shift the graph of the function y = f(x) to the down " + String.valueOf(a) + " units.";
        RIGHT_ANSWER = "y = f(x) - " + String.valueOf(a) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = f(x - " + String.valueOf(a) + ").";
        ANSWER_C = "y = f(x + " + String.valueOf(a) + ").";
        ANSWER_D = "y = f(x) + " + String.valueOf(a) + ".";
    }

    //  REFLECT X-AXIS
    private void createProblem_05(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Reflect the graph of the function y = f(x) over the x-axis.";
        RIGHT_ANSWER = "y = -f(x).";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = f(-x).";
        ANSWER_C = "y = f(x + " + String.valueOf(a) + ").";
        ANSWER_D = "y = f(x) + " + String.valueOf(a) + ".";
    }

    //  REFLECT X-AXIS
    private void createProblem_06(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Reflect the graph of the function y = f(x) over the y-axis.";
        RIGHT_ANSWER = "y = f(-x).";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = -f(x).";
        ANSWER_C = "y = f(x + " + String.valueOf(a) + ").";
        ANSWER_D = "y = f(x) + " + String.valueOf(a) + ".";
    }

    //  COMPRESS HORIZONTALLY
    private void createProblem_07(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Compress the graph of the function y = f(x) horizontally by the factor of " + String.valueOf(a) + ".";
        RIGHT_ANSWER = "y = f(" + String.valueOf(a) + "x).";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = " + String.valueOf(a) + "f(x).";
        ANSWER_C = "y = f(x + " + String.valueOf(a) + ").";
        ANSWER_D = "y = f(x) + " + String.valueOf(a) + ".";
    }

    //  STRETCH HORIZONTALLY
    private void createProblem_08(){
        Fraction Factor = FractionMath.getRandomProperFraction();

        QUESTION = "Stretch the graph of the function y = f(x) horizontally by the factor of " + Factor.toString() + ".";
        RIGHT_ANSWER = "y = f(" + Factor.toString() + "x).";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = f(" + Factor.toString() + " + x).";
        ANSWER_C = "y = f(x + " + Factor.toString() + ").";
        ANSWER_D = "y = f(x) + " + Factor.toString() + ".";
    }

    //  COMPRESS VERTICALLY
    private void createProblem_09(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Compress the graph of the function y = f(x) vertically by the factor of " + String.valueOf(a) + ".";
        RIGHT_ANSWER = "y = " + String.valueOf(a) + "f(x).";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = f(" + String.valueOf(a) + "x).";
        ANSWER_C = "y = f(x + " + String.valueOf(a) + ").";
        ANSWER_D = "y = f(x) + " + String.valueOf(a) + ".";
    }

    //  STRETCH VERTICALLY
    private void createProblem_10(){
        Fraction Factor = FractionMath.getRandomProperFraction();

        QUESTION = "Stretch the graph of the function y = f(x) horizontally by the factor of " + Factor.toString() + ".";
        RIGHT_ANSWER = "y = " + Factor.toString() + "f(x).";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = f(" + Factor.toString() + "x).";
        ANSWER_C = "y = f(x + " + Factor.toString() + ").";
        ANSWER_D = "y = f(x) + " + Factor.toString() + ".";
    }

    //  SHIFT LEFT + UP
    private void createProblem_11(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Shift the graph of the function y = f(x) to the left "
                + String.valueOf(a) + " units, and up " + String.valueOf(b) + " units.";
        RIGHT_ANSWER = "y = f(x + " + String.valueOf(a) + ") + " + String.valueOf(b) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = f(x - " + String.valueOf(a) + ") + " + String.valueOf(b) + ".";
        ANSWER_C = "y = f(x + " + String.valueOf(a) + ") - " + String.valueOf(b) + ".";
        ANSWER_D = "y = f(x - " + String.valueOf(a) + ") - " + String.valueOf(b) + ".";
    }

    //  SHIFT LEFT + DOWN
    private void createProblem_12(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Shift the graph of the function y = f(x) to the left "
                + String.valueOf(a) + " units, and down " + String.valueOf(b) + " units.";
        RIGHT_ANSWER = "y = f(x + " + String.valueOf(a) + ") - " + String.valueOf(b) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = f(x - " + String.valueOf(a) + ") + " + String.valueOf(b) + ".";
        ANSWER_C = "y = f(x + " + String.valueOf(a) + ") + " + String.valueOf(b) + ".";
        ANSWER_D = "y = f(x - " + String.valueOf(a) + ") - " + String.valueOf(b) + ".";
    }

    //  SHIFT RIGHT + UP
    private void createProblem_13(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Shift the graph of the function y = f(x) to the right "
                + String.valueOf(a) + " units, and up " + String.valueOf(b) + " units.";
        RIGHT_ANSWER = "y = f(x - " + String.valueOf(a) + ") + " + String.valueOf(b) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = f(x + " + String.valueOf(a) + ") + " + String.valueOf(b) + ".";
        ANSWER_C = "y = f(x + " + String.valueOf(a) + ") - " + String.valueOf(b) + ".";
        ANSWER_D = "y = f(x - " + String.valueOf(a) + ") - " + String.valueOf(b) + ".";
    }

    //  SHIFT RIGHT + DOWN
    private void createProblem_14(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Shift the graph of the function y = f(x) to the right "
                + String.valueOf(a) + " units, and down " + String.valueOf(b) + " units.";
        RIGHT_ANSWER = "y = f(x - " + String.valueOf(a) + ") - " + String.valueOf(b) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = f(x - " + String.valueOf(a) + ") + " + String.valueOf(b) + ".";
        ANSWER_C = "y = f(x + " + String.valueOf(a) + ") + " + String.valueOf(b) + ".";
        ANSWER_D = "y = f(x + " + String.valueOf(a) + ") - " + String.valueOf(b) + ".";
    }

    //  SHIFT LEFT + REFLECT X-AXIS
    private void createProblem_15(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Shift the graph of the function y = f(x) to the left "
                + String.valueOf(a) + " units, and reflect over the x-axis.";
        RIGHT_ANSWER = "y = -f(x + " + String.valueOf(a) + ").";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = f(-x - " + String.valueOf(a) + ").";
        ANSWER_C = "y = f(x + " + String.valueOf(a) + ").";
        ANSWER_D = "y = -f(x - " + String.valueOf(a) + ").";
    }

    //  SHIFT LEFT + REFLECT Y-AXIS
    private void createProblem_16(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Shift the graph of the function y = f(x) to the left "
                + String.valueOf(a) + " units, and reflect over the x-axis.";
        RIGHT_ANSWER = "y = f(-x + " + String.valueOf(a) + ").";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = f(-x - " + String.valueOf(a) + ").";
        ANSWER_C = "y = -f(x + " + String.valueOf(a) + ").";
        ANSWER_D = "y = -f(x - " + String.valueOf(a) + ").";
    }

    //  SHIFT RIGHT + REFLECT X-AXIS
    private void createProblem_17(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Shift the graph of the function y = f(x) to the left "
                + String.valueOf(a) + " units, and reflect over the x-axis.";
        RIGHT_ANSWER = "y = -f(x - " + String.valueOf(a) + ").";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = f(-x - " + String.valueOf(a) + ").";
        ANSWER_C = "y = f(x + " + String.valueOf(a) + ").";
        ANSWER_D = "y = -f(x - " + String.valueOf(a) + ").";
    }

    //  SHIFT RIGHT + REFLECT Y-AXIS
    private void createProblem_18(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Shift the graph of the function y = f(x) to the left "
                + String.valueOf(a) + " units, and reflect over the x-axis.";
        RIGHT_ANSWER = "y = f(-x - " + String.valueOf(a) + ").";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = f(-x - " + String.valueOf(a) + ").";
        ANSWER_C = "y = -f(x + " + String.valueOf(a) + ").";
        ANSWER_D = "y = -f(x - " + String.valueOf(a) + ").";
    }

    //  SHIFT LEFT + SHIFT DOWN + COMPRESS HORIZONTALLY
    private void createProblem_19(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Compress the graph of the function y = f(x) horizontally by the factor of " + String.valueOf(b)
                + ", shift left " + String.valueOf(a) + "units, and shift down " + String.valueOf(c) + " units.";
        RIGHT_ANSWER = "y = f(" + String.valueOf(b) + "x + " + String.valueOf(a) + ") - " + String.valueOf(c) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = -f(" + String.valueOf(b) + "x + " + String.valueOf(a) + ") - " + String.valueOf(c) + ".";
        ANSWER_C = "y = f(" + String.valueOf(b) + "x - " + String.valueOf(a) + ") - " + String.valueOf(c) + ".";
        ANSWER_D = "y = f(" + String.valueOf(b) + "x + " + String.valueOf(a) + ") + " + String.valueOf(c) + ".";
    }

    //  SHIFT RIGHT + SHIFT UP + STRETCH VERTICALLY
    private void createProblem_20(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Stretch the graph of the function y = f(x) vertically by the factor of " + String.valueOf(b)
                + ", shift right " + String.valueOf(a) + "units, and shift up " + String.valueOf(c) + " units.";
        RIGHT_ANSWER = "y = f(" + String.valueOf(b) + "x - " + String.valueOf(a) + ") + " + String.valueOf(c) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = f(" + String.valueOf(b) + "x - " + String.valueOf(a) + ") + " + String.valueOf(c) + ".";
        ANSWER_C = "y = f(" + String.valueOf(b) + "x - " + String.valueOf(a) + ") - " + String.valueOf(c) + ".";
        ANSWER_D = "y = -f(" + String.valueOf(b) + "x + " + String.valueOf(a) + ") - " + String.valueOf(c) + ".";
    }

    //  REFLECT X-AXIS + STRETCH HORIZONTALLY + COMPRESS VERTICALLY
    private void createProblem_21(){
        Fraction HFactor = FractionMath.getRandomProperFraction();
        Fraction VFactor = FractionMath.getRandomProperFraction();

        QUESTION = "Reflect the graph of the function y = f(x) over the x-axis, stretch horizontally by the factor of "
                + HFactor.toString() + ", and compress vertically by the factor of " + VFactor.toString() + ".";
        RIGHT_ANSWER = "y = -" + VFactor.toString() + "f(" + HFactor.toString() + "x).";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = -" + VFactor.toString() + "f(-" + HFactor.toString() + "x).";
        ANSWER_C = "y = " + VFactor.toString() + "f(-" + HFactor.toString() + "x).";
        ANSWER_D = "y = -" + VFactor.toString() + "f(" + HFactor.toString() + "x).";
    }
    
    //  REFLECT Y-AXIS + STRETCH VERTICALLY + SHIFT DOWN
    private void createProblem_22(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Reflect the graph of the function y = f(x) over the y-axis, stretch vertically by the factor of "
                + String.valueOf(a) + ", and shift down " + String.valueOf(b) + " units.";
        RIGHT_ANSWER = "y = " + String.valueOf(a) + "f(-x) - " + String.valueOf(b) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "y = " + String.valueOf(a) + "-f(x) - " + String.valueOf(b) + ".";
        ANSWER_C = "y = " + String.valueOf(a) + "f(x) + " + String.valueOf(b) + ".";
        ANSWER_D = "y = " + String.valueOf(a) + "f(-x) + " + String.valueOf(b) + ".";
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
