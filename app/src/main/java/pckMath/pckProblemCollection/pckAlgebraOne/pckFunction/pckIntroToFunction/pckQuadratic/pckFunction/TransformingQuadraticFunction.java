package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckFunction;

import pckMath.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.FunctionMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckLinearFunction.LinearFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/29/2017.
 */

public class TransformingQuadraticFunction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public TransformingQuadraticFunction(){
        createProblem();
        swapAnswer();
    }

    public TransformingQuadraticFunction(String Question,
                                      String RightAnswer,
                                      String AnswerA,
                                      String AnswerB,
                                      String AnswerC,
                                      String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 12;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            default:
                createProblem_01();
                break;
        }

        return new TransformingQuadraticFunction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  SHIFT LEFT
    private void createProblem_01(){
        QuadraticFunction Function = FunctionMath.getRandomQuadraticFunction();

        int ShiftedUnit = MathUtility.getRandomPositiveNumber_1Digit();
        LinearFunction ShiftedFunction = new LinearFunction(1, ShiftedUnit);

        Polynomial Result = FunctionMath.composite(Function, ShiftedFunction).getPolynomial();

        QUESTION = "Shift the function: " + Function.toString() + " to the left " + String.valueOf(ShiftedUnit) + " units.";
        RIGHT_ANSWER = new QuadraticFunction(Result).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  SHIFT RIGHT
    private void createProblem_02(){
        QuadraticFunction Function = FunctionMath.getRandomQuadraticFunction();

        int ShiftedUnit = MathUtility.getRandomPositiveNumber_1Digit();
        LinearFunction ShiftedFunction = new LinearFunction(1, 0 - ShiftedUnit);

        Polynomial Result = FunctionMath.composite(Function, ShiftedFunction).getPolynomial();

        QUESTION = "Shift the function: " + Function.toString() + " to the right " + String.valueOf(ShiftedUnit) + " units.";
        RIGHT_ANSWER = new QuadraticFunction(Result).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  SHIFT UP
    private void createProblem_03(){
        QuadraticFunction Function = FunctionMath.getRandomQuadraticFunction();

        int ShiftedUnit = MathUtility.getRandomPositiveNumber_1Digit();
        Polynomial Result = FunctionMath.add(Function, ShiftedUnit);

        QUESTION = "Shift the function: " + Function.toString() + " up " + String.valueOf(ShiftedUnit) + " units.";
        RIGHT_ANSWER = new QuadraticFunction(Result).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  SHIFT DOWN
    private void createProblem_04(){
        QuadraticFunction Function = FunctionMath.getRandomQuadraticFunction();

        int ShiftedUnit = MathUtility.getRandomPositiveNumber_1Digit();
        Polynomial Result = FunctionMath.sub(Function, ShiftedUnit);

        QUESTION = "Shift the function: " + Function.toString() + " down " + String.valueOf(ShiftedUnit) + " units.";
        RIGHT_ANSWER = new QuadraticFunction(Result).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  REFLECT X-AXIS
    private void createProblem_05(){
        QuadraticFunction Function = FunctionMath.getRandomQuadraticFunction();
        Polynomial Result = FunctionMath.mul(Function, -1);

        QUESTION = "Reflect the function: " + Function.toString() + " over the x-axis.";
        RIGHT_ANSWER = new QuadraticFunction(Result).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  REFLECT Y-AXIS
    private void createProblem_06(){
        QuadraticFunction Function = FunctionMath.getRandomQuadraticFunction();
        Polynomial Result = new QuadraticFunction(Function.getACoefficient(), 0 - Function.getBCoefficient(), Function.getCCoefficient()).getPolynomial();

        QUESTION = "Reflect the function: " + Function.toString() + " over the y-axis.";
        RIGHT_ANSWER = new QuadraticFunction(Result).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  SHIFT UP + LEFT
    private void createProblem_07(){
        QuadraticFunction Function = FunctionMath.getRandomQuadraticFunction();

        int ShiftedUpUnit = MathUtility.getRandomPositiveNumber_1Digit();
        QuadraticFunction Result = new QuadraticFunction(FunctionMath.add(Function, ShiftedUpUnit));

        int ShiftedLeftUnit = MathUtility.getRandomPositiveNumber_1Digit();
        LinearFunction ShiftedFunction = new LinearFunction(1, ShiftedLeftUnit);
        Result = FunctionMath.composite(Result, ShiftedFunction);

        QUESTION = "Shift the function: " + Function.toString() + " up " + String.valueOf(ShiftedUpUnit) + " units, "
                + "and " + String.valueOf(ShiftedLeftUnit) + " units to the left.";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  SHIFT DOWN + RIGHT
    private void createProblem_08(){
        QuadraticFunction Function = FunctionMath.getRandomQuadraticFunction();

        int ShiftedDownUnit = MathUtility.getRandomPositiveNumber_1Digit();
        QuadraticFunction Result = new QuadraticFunction(FunctionMath.sub(Function, ShiftedDownUnit));

        int ShiftedRightUnit = MathUtility.getRandomPositiveNumber_1Digit();
        LinearFunction ShiftedFunction = new LinearFunction(1, 0 - ShiftedRightUnit);
        Result = FunctionMath.composite(Result, ShiftedFunction);

        QUESTION = "Shift the function: " + Function.toString() + " down " + String.valueOf(ShiftedDownUnit) + " units, "
                + "and " + String.valueOf(ShiftedRightUnit) + " units to the right.";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  SHIFT DOWN + LEFT + REFLECT X-AXIS
    private void createProblem_09(){
        QuadraticFunction Function = FunctionMath.getRandomQuadraticFunction();

        int ShiftedDownUnit = MathUtility.getRandomPositiveNumber_1Digit();
        QuadraticFunction Result = new QuadraticFunction(FunctionMath.sub(Function, ShiftedDownUnit));

        int ShiftedLeftUnit = MathUtility.getRandomPositiveNumber_1Digit();
        LinearFunction ShiftedFunction = new LinearFunction(1, ShiftedLeftUnit);
        Result = FunctionMath.composite(Result, ShiftedFunction);
        
        Result = new QuadraticFunction(FunctionMath.mul(Result, -1));

        QUESTION = "Shift the function: " + Function.toString() + " down " + String.valueOf(ShiftedDownUnit) + " units, "
                + String.valueOf(ShiftedLeftUnit) + " units to the left, and reflect over the x-axis.";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  SHIFT UP + RIGHT + REFLECT X-AXIS
    private void createProblem_10(){
        QuadraticFunction Function = FunctionMath.getRandomQuadraticFunction();

        int ShiftedUpUnit = MathUtility.getRandomPositiveNumber_1Digit();
        QuadraticFunction Result = new QuadraticFunction(FunctionMath.add(Function, ShiftedUpUnit));

        int ShiftedRightUnit = MathUtility.getRandomPositiveNumber_1Digit();
        LinearFunction ShiftedFunction = new LinearFunction(1, 0 - ShiftedRightUnit);
        Result = FunctionMath.composite(Result, ShiftedFunction);

        Result = new QuadraticFunction(FunctionMath.mul(Result, -1));

        QUESTION = "Shift the function: " + Function.toString() + " up " + String.valueOf(ShiftedUpUnit) + " units, "
                + String.valueOf(ShiftedRightUnit) + " units to the right, and reflect over the x-axis";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  SHIFT UP + LEFT + REFLECT Y-AXIS
    private void createProblem_11(){
        QuadraticFunction Function = FunctionMath.getRandomQuadraticFunction();

        int ShiftedUpUnit = MathUtility.getRandomPositiveNumber_1Digit();
        QuadraticFunction Result = new QuadraticFunction(FunctionMath.add(Function, ShiftedUpUnit));

        int ShiftedLeftUnit = MathUtility.getRandomPositiveNumber_1Digit();
        LinearFunction ShiftedFunction = new LinearFunction(1, ShiftedLeftUnit);
        Result = FunctionMath.composite(Result, ShiftedFunction);

        Result = new QuadraticFunction(Result.getACoefficient(), 0 - Result.getBCoefficient(), Result.getCCoefficient());

        QUESTION = "Shift the function: " + Function.toString() + " up " + String.valueOf(ShiftedUpUnit) + " units, "
                + String.valueOf(ShiftedLeftUnit) + " units to the left, and reflect over the y-axis.";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    //  SHIFT DOWN + RIGHT + REFLECT Y-AXIS
    private void createProblem_12(){
        QuadraticFunction Function = FunctionMath.getRandomQuadraticFunction();

        int ShiftedDownUnit = MathUtility.getRandomPositiveNumber_1Digit();
        QuadraticFunction Result = new QuadraticFunction(FunctionMath.sub(Function, ShiftedDownUnit));

        int ShiftedRightUnit = MathUtility.getRandomPositiveNumber_1Digit();
        LinearFunction ShiftedFunction = new LinearFunction(1, 0 - ShiftedRightUnit);
        Result = FunctionMath.composite(Result, ShiftedFunction);

        Result = new QuadraticFunction(Result.getACoefficient(), 0 - Result.getBCoefficient(), Result.getCCoefficient());

        QUESTION = "Shift the function: " + Function.toString() + " down " + String.valueOf(ShiftedDownUnit) + " units, "
                + String.valueOf(ShiftedRightUnit) + " units to the right, and reflect over the y-axis.";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_C = FunctionMath.getRandomQuadraticFunction().toString();
        ANSWER_D = FunctionMath.getRandomQuadraticFunction().toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
