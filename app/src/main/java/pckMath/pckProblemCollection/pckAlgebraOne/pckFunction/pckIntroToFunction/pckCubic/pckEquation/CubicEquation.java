package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckCubic.pckEquation;

import pckInfo.InfoCollector;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/26/2017.
 */

public class CubicEquation {

    /*  ax^3 + bx^2 + cx + d = 0*/

    private int A_COEFFICIENT, B_COEFFICIENT, C_COEFFICIENT, D_COEFFICIENT;

    public CubicEquation(){
        A_COEFFICIENT = 0;
        B_COEFFICIENT = 0;
        C_COEFFICIENT = 0;
        D_COEFFICIENT = 0;
    }

    public CubicEquation(int ACoefficient, int BCoefficient, int CCoefficient, int DCoefficient){
        A_COEFFICIENT = ACoefficient;
        B_COEFFICIENT = BCoefficient;
        C_COEFFICIENT = CCoefficient;
        D_COEFFICIENT = DCoefficient;
    }

    public CubicEquation(Polynomial _Polynomial){
        for (int i = 0; i < 4; i++) {
            AlgebraicNumber Temp = _Polynomial.getExpression().getNumber(i);
            switch (Temp.getVariable(0).getExponent()){
                case 0:
                    D_COEFFICIENT = Temp.getCoefficient();
                    break;
                case 1:
                    C_COEFFICIENT = Temp.getCoefficient();
                    break;
                case 2:
                    B_COEFFICIENT = Temp.getCoefficient();
                    break;
                case 3:
                    A_COEFFICIENT = Temp.getCoefficient();
                    break;
            }
        }
    }

    public void setACoefficient(int Coefficient){
        A_COEFFICIENT = Coefficient;
    }

    public int getACoefficient(){
        return A_COEFFICIENT;
    }

    public void setBCoefficient(int Coefficient){
        B_COEFFICIENT = Coefficient;
    }

    public int getBCoefficient(){
        return B_COEFFICIENT;
    }

    public void setCCoefficient(int Coefficient){
        C_COEFFICIENT = Coefficient;
    }

    public int getCCoefficient(){
        return C_COEFFICIENT;
    }

    public void setDCoefficient(int Coefficient){
        D_COEFFICIENT = Coefficient;
    }

    public int getDCoefficient(){
        return D_COEFFICIENT;
    }

    public String toString(){
        String CubicString = String.valueOf(A_COEFFICIENT) + "x" + InfoCollector.putExponent(3);

        if (B_COEFFICIENT != 0){
            CubicString += " + " + String.valueOf(B_COEFFICIENT) + "x" + InfoCollector.putExponent(2);
        }

        if (C_COEFFICIENT != 0){
            CubicString += " + " + String.valueOf(C_COEFFICIENT) + "x";
        }

        if (D_COEFFICIENT != 0){
            CubicString += " + " + String.valueOf(D_COEFFICIENT);
        }

        CubicString += " = 0";

        return CubicString;
    }

}
