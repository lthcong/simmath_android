package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckLinearFunction;

import pckInfo.InfoCollector;
import pckMath.*;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.FunctionMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/29/2017.
 */

public class ManipulatingLinearFunction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public ManipulatingLinearFunction(){
        createProblem();
        swapAnswer();
    }

    public ManipulatingLinearFunction(String Question,
                              String RightAnswer,
                              String AnswerA,
                              String AnswerB,
                              String AnswerC,
                              String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 8;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }

        return new ManipulatingLinearFunction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  ADD
    private void createProblem_01(){
        LinearFunction FunctionA = FunctionMath.getRandomLinearFunction();
        LinearFunction FunctionB = FunctionMath.getRandomLinearFunction();
        Polynomial Result = FunctionMath.add(FunctionA, FunctionB);

        QUESTION = "Given f(x): " + FunctionA.toString() + ", g(x): " + FunctionB.toString() + ".<br>"
                + "Find the result of (f + g)(x).";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  SUB
    private void createProblem_02(){
        LinearFunction FunctionA = FunctionMath.getRandomLinearFunction();
        LinearFunction FunctionB = FunctionMath.getRandomLinearFunction();
        Polynomial Result = FunctionMath.sub(FunctionA, FunctionB);

        QUESTION = "Given f(x): " + FunctionA.toString() + ", g(x): " + FunctionB.toString() + ".<br>"
                + "Find the result of (f - g)(x).";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  MUL
    private void createProblem_03(){
        LinearFunction FunctionA = FunctionMath.getRandomLinearFunction();
        LinearFunction FunctionB = FunctionMath.getRandomLinearFunction();
        Polynomial Result = FunctionMath.mul(FunctionA, FunctionB);

        QUESTION = "Given f(x): " + FunctionA.toString() + ", g(x): " + FunctionB.toString() + ".<br>"
                + "Find the result of (f * g)(x).";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  DIV
    private void createProblem_04(){
        LinearFunction FunctionA = FunctionMath.getRandomLinearFunction();
        int Number = MathUtility.getRandomNumber_1Digit();

        LinearFunction FunctionB = new LinearFunction(FunctionMath.mul(FunctionA, Number));

        QUESTION = "Given f(x): " + FunctionA.toString() + ", g(x): " + FunctionB.toString() + ".<br>"
                + "Find the result of (f " + InfoCollector.getDivisionSign() + " g)(x).";
        RIGHT_ANSWER = String.valueOf(Number);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Number * 2);
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  COMPOSING
    private void createProblem_05(){
        LinearFunction FunctionA = FunctionMath.getRandomLinearFunction();
        LinearFunction FunctionB = FunctionMath.getRandomLinearFunction();
        Polynomial Result = FunctionMath.composite(FunctionA, FunctionB).getPolynomial();

        QUESTION = "Given f(x): " + FunctionA.toString() + ", g(x): " + FunctionB.toString() + ".<br>"
                + "Find the result of (f o g)(x).";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.composite(FunctionB, FunctionA).toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  COMPOSING
    private void createProblem_06(){
        LinearFunction FunctionA = FunctionMath.getRandomLinearFunction();
        LinearFunction FunctionB = FunctionMath.getRandomLinearFunction();
        Polynomial Result = FunctionMath.composite(FunctionB, FunctionA).getPolynomial();

        QUESTION = "Given f(x): " + FunctionA.toString() + ", g(x): " + FunctionB.toString() + ".<br>"
                + "Find the result of (g o f)(x).";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  COMPOSING
    private void createProblem_07(){
        LinearFunction FunctionA = FunctionMath.getRandomLinearFunction();
        LinearFunction FunctionB = FunctionMath.getRandomLinearFunction();
        Polynomial Result = FunctionMath.composite(FunctionA, FunctionA).getPolynomial();

        QUESTION = "Given f(x): " + FunctionA.toString() + ", g(x): " + FunctionB.toString() + ".<br>"
                + "Find the result of (f o f)(x).";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    //  COMPOSING
    private void createProblem_08(){
        LinearFunction FunctionA = FunctionMath.getRandomLinearFunction();
        LinearFunction FunctionB = FunctionMath.getRandomLinearFunction();
        Polynomial Result = FunctionMath.composite(FunctionB, FunctionB).getPolynomial();

        QUESTION = "Given f(x): " + FunctionA.toString() + ", g(x): " + FunctionB.toString() + ".<br>"
                + "Find the result of (g o g)(x).";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_C = FunctionMath.getRandomLinearFunction().toString();
        ANSWER_D = FunctionMath.getRandomLinearFunction().toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
