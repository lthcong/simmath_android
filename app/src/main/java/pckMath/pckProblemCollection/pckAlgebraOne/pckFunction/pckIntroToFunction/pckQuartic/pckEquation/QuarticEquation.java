package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuartic.pckEquation;

import pckInfo.InfoCollector;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/27/2017.
 */

public class QuarticEquation {

    /*  ax^4 + bx^3 + cx^2 + dx + e = 0*/

    private int A_COEFFICIENT, B_COEFFICIENT, C_COEFFICIENT, D_COEFFICIENT, E_COEFFICIENT;

    public QuarticEquation(){
        A_COEFFICIENT = 0;
        B_COEFFICIENT = 0;
        C_COEFFICIENT = 0;
        D_COEFFICIENT = 0;
        E_COEFFICIENT = 0;
    }

    public QuarticEquation(int ACoefficient, int BCoefficient, int CCoefficient, int DCoefficient, int ECoefficient){
        A_COEFFICIENT = ACoefficient;
        B_COEFFICIENT = BCoefficient;
        C_COEFFICIENT = CCoefficient;
        D_COEFFICIENT = DCoefficient;
        E_COEFFICIENT = ECoefficient;
    }

    public QuarticEquation(Polynomial _Polynomial){
        for (int i = 0; i < 5; i++) {
            AlgebraicNumber Temp = _Polynomial.getExpression().getNumber(i);
            switch (Temp.getVariable(0).getExponent()){
                case 0:
                    E_COEFFICIENT = Temp.getCoefficient();
                    break;
                case 1:
                    D_COEFFICIENT = Temp.getCoefficient();
                    break;
                case 2:
                    C_COEFFICIENT = Temp.getCoefficient();
                    break;
                case 3:
                    B_COEFFICIENT = Temp.getCoefficient();
                    break;
                case 4:
                    A_COEFFICIENT = Temp.getCoefficient();
                    break;
            }
        }
    }

    public void setACoefficient(int Coefficient){
        A_COEFFICIENT = Coefficient;
    }

    public int getACoefficient(){
        return A_COEFFICIENT;
    }

    public void setBCoefficient(int Coefficient){
        B_COEFFICIENT = Coefficient;
    }

    public int getBCoefficient(){
        return B_COEFFICIENT;
    }

    public void setCCoefficient(int Coefficient){
        C_COEFFICIENT = Coefficient;
    }

    public int getCCoefficient(){
        return C_COEFFICIENT;
    }

    public void setDCoefficient(int Coefficient){
        D_COEFFICIENT = Coefficient;
    }

    public int getDCoefficient(){
        return D_COEFFICIENT;
    }

    public void setECoefficient(int Coefficient){
        E_COEFFICIENT = Coefficient;
    }

    public int getECoefficient(){
        return E_COEFFICIENT;
    }

    public String toString(){
        String QuarticString = String.valueOf(A_COEFFICIENT) + "x" + InfoCollector.putExponent(4);

        if (B_COEFFICIENT != 0){
            QuarticString += " + " + String.valueOf(B_COEFFICIENT) + "x" + InfoCollector.putExponent(3);
        }

        if (C_COEFFICIENT != 0){
            QuarticString += " + " + String.valueOf(C_COEFFICIENT) + "x" + InfoCollector.putExponent(2);
        }

        if (D_COEFFICIENT != 0){
            QuarticString += " + " + String.valueOf(D_COEFFICIENT) + "x";
        }

        if (E_COEFFICIENT != 0){
            QuarticString += " + " + String.valueOf(E_COEFFICIENT);
        }

        QuarticString += " = 0";

        return QuarticString;
    }
}
