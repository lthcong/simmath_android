package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckFunction;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.OrderedPairs;

/**
 * Created by Cong on 7/21/2017.
 */

public class BeginningFunction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;
    private String FUNCTION = "Yes, It is a function.",
            NOT_A_FUNCTION = "No, It is not a function.",
            EITHER = "Either.",
            NEITHER = "Neither.",
            NONE = "None of them.",
            BOTH = "Both of them.";

    public BeginningFunction(){
        createProblem();
        swapAnswer();
    }

    public BeginningFunction(String Question,
                                        String RightAnswer,
                                        String AnswerA,
                                        String AnswerB,
                                        String AnswerC,
                                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }

        return new BeginningFunction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  FUNCTION
    private void createProblem_01(){
        int XNumber = MathUtility.getRandomNumber_1Digit();
        int YNumber = MathUtility.getRandomNumber_1Digit();

        OrderedPairs PairsA = new OrderedPairs(XNumber, YNumber);
        OrderedPairs PairsB = new OrderedPairs(XNumber + 1, YNumber + 1);
        OrderedPairs PairsC = new OrderedPairs(XNumber + 2, YNumber + 2);
        OrderedPairs PairsD = new OrderedPairs(XNumber + 3, YNumber + 3);
        OrderedPairs PairsE = new OrderedPairs(XNumber + 4, YNumber + 4);

        QUESTION = "Is this relation a function: <br>{"
                + PairsA.toString() + ", " + PairsB.toString() + ", " + PairsC.toString() + ", " + PairsD.toString() + ", " + PairsE.toString()
                + "}";
        RIGHT_ANSWER = FUNCTION;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = NOT_A_FUNCTION;
        ANSWER_C = EITHER;
        ANSWER_D = NEITHER;
    }

    //  FUNCTION
    private void createProblem_02(){
        int XNumber = MathUtility.getRandomNumber_1Digit();
        int YNumber = MathUtility.getRandomNumber_1Digit();

        OrderedPairs PairsA = new OrderedPairs(XNumber, YNumber);
        OrderedPairs PairsB = new OrderedPairs(XNumber + 1, YNumber + 1);
        OrderedPairs PairsC = new OrderedPairs(XNumber + 2, YNumber);
        OrderedPairs PairsD = new OrderedPairs(XNumber + 3, YNumber + 3);
        OrderedPairs PairsE = new OrderedPairs(XNumber + 4, YNumber + 4);

        QUESTION = "Is this relation a function: <br>{"
                + PairsA.toString() + ", " + PairsB.toString() + ", " + PairsC.toString() + ", " + PairsD.toString() + ", " + PairsE.toString()
                + "}";
        RIGHT_ANSWER = FUNCTION;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = NOT_A_FUNCTION;
        ANSWER_C = EITHER;
        ANSWER_D = NEITHER;
    }

    //  NOT A FUNCTION
    private void createProblem_03(){
        int XNumber = MathUtility.getRandomNumber_1Digit();
        int YNumber = MathUtility.getRandomNumber_1Digit();

        OrderedPairs PairsA = new OrderedPairs(XNumber, YNumber);
        OrderedPairs PairsB = new OrderedPairs(XNumber + 1, YNumber + 1);
        OrderedPairs PairsC = new OrderedPairs(XNumber, YNumber + 2);
        OrderedPairs PairsD = new OrderedPairs(XNumber + 3, YNumber + 3);
        OrderedPairs PairsE = new OrderedPairs(XNumber + 4, YNumber + 4);

        QUESTION = "Is this relation a function: <br>{"
                + PairsA.toString() + ", " + PairsB.toString() + ", " + PairsC.toString() + ", " + PairsD.toString() + ", " + PairsE.toString()
                + "}";
        RIGHT_ANSWER = NOT_A_FUNCTION;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FUNCTION;
        ANSWER_C = EITHER;
        ANSWER_D = NEITHER;
    }

    //  NOT A FUNCTION
    private void createProblem_04(){
        int XNumber = MathUtility.getRandomNumber_1Digit();
        int YNumber = MathUtility.getRandomNumber_1Digit();

        OrderedPairs PairsA = new OrderedPairs(XNumber, YNumber);
        OrderedPairs PairsB = new OrderedPairs(XNumber + 1, YNumber + 1);
        OrderedPairs PairsC = new OrderedPairs(XNumber + 2, YNumber + 2);
        OrderedPairs PairsD = new OrderedPairs(XNumber + 2, YNumber + 3);
        OrderedPairs PairsE = new OrderedPairs(XNumber + 4, YNumber + 4);

        QUESTION = "Is this relation a function: <br>{"
                + PairsA.toString() + ", " + PairsB.toString() + ", " + PairsC.toString() + ", " + PairsD.toString() + ", " + PairsE.toString()
                + "}";
        RIGHT_ANSWER = NOT_A_FUNCTION;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FUNCTION;
        ANSWER_C = EITHER;
        ANSWER_D = NEITHER;
    }

    //  FIND FUNCTION
    private void createProblem_05(){
        int XNumber = MathUtility.getRandomNumber_1Digit();
        int YNumber = MathUtility.getRandomNumber_1Digit();

        QUESTION = "Find the function from these relations:";
        RIGHT_ANSWER = "{"
                + new OrderedPairs(XNumber, YNumber) + ", " + new OrderedPairs(XNumber + 1, YNumber + 1) + ", " + new OrderedPairs(XNumber + 2, YNumber + 2)
                + "}";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "{"
                + new OrderedPairs(XNumber + 1, YNumber) + ", " + new OrderedPairs(XNumber + 1, YNumber + 1) + ", " + new OrderedPairs(XNumber + 2, YNumber + 2)
                + "}";
        ANSWER_C = "{"
                + new OrderedPairs(XNumber, YNumber) + ", " + new OrderedPairs(XNumber + 2, YNumber + 1) + ", " + new OrderedPairs(XNumber + 2, YNumber + 2)
                + "}";
        ANSWER_D = "{"
                + new OrderedPairs(XNumber, YNumber) + ", " + new OrderedPairs(XNumber + 1, YNumber + 1) + ", " + new OrderedPairs(XNumber, YNumber + 2)
                + "}";
    }

    //  FIND FUNCTION
    private void createProblem_06(){
        int XNumber = MathUtility.getRandomNumber_1Digit();
        int YNumber = MathUtility.getRandomNumber_1Digit();

        QUESTION = "Find the function from these relations:";
        RIGHT_ANSWER = "{"
                + new OrderedPairs(XNumber, YNumber) + ", " + new OrderedPairs(XNumber + 1, YNumber + 1) + ", " + new OrderedPairs(XNumber + 2, YNumber + 2)
                + "}";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "{"
                + new OrderedPairs(XNumber + 1, YNumber) + ", " + new OrderedPairs(XNumber + 1, YNumber + 1) + ", " + new OrderedPairs(XNumber + 2, YNumber + 2)
                + "}";
        ANSWER_C = NONE;
        ANSWER_D = BOTH;
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
