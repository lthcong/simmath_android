package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuartic.pckFunction;

import pckInfo.InfoCollector;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicExpression;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.Variable;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.MathFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/28/2017.
 */

public class QuarticFunction extends MathFunction{

    //  y = ax^4 + bx^3 + cx^2 + dx + e

    private int A_COEFFICIENT, B_COEFFICIENT, C_COEFFICIENT, D_COEFFICIENT, E_COEFFICIENT;

    public QuarticFunction(){
        A_COEFFICIENT = 0;
        B_COEFFICIENT = 0;
        C_COEFFICIENT = 0;
        D_COEFFICIENT = 0;
        E_COEFFICIENT = 0;
    }

    public QuarticFunction(int ACoefficient, int BCoefficient, int CCoefficient, int DCoefficient, int ECoefficient){
        A_COEFFICIENT = ACoefficient;
        B_COEFFICIENT = BCoefficient;
        C_COEFFICIENT = CCoefficient;
        D_COEFFICIENT = DCoefficient;
        E_COEFFICIENT = ECoefficient;
    }

    public void setACoefficient(int Coefficient){
        A_COEFFICIENT = Coefficient;
    }

    public int getACoefficient(){
        return A_COEFFICIENT;
    }

    public void setBCoefficient(int Coefficient){
        B_COEFFICIENT = Coefficient;
    }

    public int getBCoefficient(){
        return B_COEFFICIENT;
    }

    public void setCCoefficient(int Coefficient){
        C_COEFFICIENT = Coefficient;
    }

    public int getCCoefficient(){
        return C_COEFFICIENT;
    }

    public void setDCoefficient(int Coefficient){
        D_COEFFICIENT = Coefficient;
    }

    public int getDCoefficient(){
        return D_COEFFICIENT;
    }

    public void setECoefficient(int Coefficient){
        E_COEFFICIENT = Coefficient;
    }

    public int getECoefficient(){
        return E_COEFFICIENT;
    }

    @Override
    public String toString(){
        String QuarticString = "y = " + String.valueOf(A_COEFFICIENT) + "x" + InfoCollector.putExponent(4);

        if (B_COEFFICIENT != 0){
            QuarticString += " + " + String.valueOf(B_COEFFICIENT) + "x" + InfoCollector.putExponent(3);
        }

        if (C_COEFFICIENT != 0){
            QuarticString += " + " + String.valueOf(C_COEFFICIENT) + "x" + InfoCollector.putExponent(2);
        }

        if (D_COEFFICIENT != 0){
            QuarticString += " + " + String.valueOf(D_COEFFICIENT) + "x";
        }

        if (E_COEFFICIENT != 0){
            QuarticString += " + " + String.valueOf(E_COEFFICIENT);
        }

        return QuarticString;
    }

    @Override
    public Polynomial getPolynomial(){
        AlgebraicNumber FirstNumber = new AlgebraicNumber();
        FirstNumber.setCoefficient(A_COEFFICIENT);
        FirstNumber.addVariable(new Variable("x", 4));

        AlgebraicNumber SecondNumber = new AlgebraicNumber();
        SecondNumber.setCoefficient(B_COEFFICIENT);
        SecondNumber.addVariable(new Variable("x", 3));

        AlgebraicNumber ThirdNumber = new AlgebraicNumber();
        ThirdNumber.setCoefficient(C_COEFFICIENT);
        ThirdNumber.addVariable(new Variable("x", 2));

        AlgebraicNumber ForthNumber = new AlgebraicNumber();
        ForthNumber.setCoefficient(D_COEFFICIENT);
        ForthNumber.addVariable(new Variable("x", 1));

        AlgebraicNumber FifthNumber = new AlgebraicNumber();
        FifthNumber.setCoefficient(E_COEFFICIENT);
        FifthNumber.addVariable(new Variable("x", 0));

        AlgebraicExpression Expression = new AlgebraicExpression();
        Expression.addNumber(FirstNumber);
        Expression.addNumber(SecondNumber);
        Expression.addNumber(ThirdNumber);
        Expression.addNumber(ForthNumber);
        Expression.addNumber(FifthNumber);

        Polynomial QuadraticPolynomial = new Polynomial();
        QuadraticPolynomial.setExpression(Expression);

        return QuadraticPolynomial;
    }

}
