package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.FunctionMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckFunction.QuadraticFunction;

/**
 * Created by Cong on 7/25/2017.
 */

public class WordProblemWithQuadraticEquation extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public WordProblemWithQuadraticEquation(){
        createProblem();
        swapAnswer();
    }

    public WordProblemWithQuadraticEquation(String Question,
                                               String RightAnswer,
                                               String AnswerA,
                                               String AnswerB,
                                               String AnswerC,
                                               String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 11;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            default:
                createProblem_01();
                break;
        }

        return new WordProblemWithQuadraticEquation(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomNumber_1Digit();
        int diff = MathUtility.getRandomPositiveNumber_1Digit();
        int b = a + diff;
        int product = a * b;

        QUESTION = "Find the two numbers. Given that the different between them is " + String.valueOf(diff)
                + ", and their product is " + String.valueOf(product) + ".";
        RIGHT_ANSWER = String.valueOf(a) + ", " + String.valueOf(b);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a - 1) + ", " + String.valueOf(b);
        ANSWER_C = String.valueOf(a + 1) + ", " + String.valueOf(b - 1);
        ANSWER_D = String.valueOf(a) + ", " + String.valueOf(b + 1);
    }

    private void createProblem_02(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int product = a * b;

        QUESTION = "Find the two numbers. Given that the sum is " + String.valueOf(a + b)
                + ", and their product is " + String.valueOf(product) + ".";
        RIGHT_ANSWER = String.valueOf(a) + ", " + String.valueOf(b);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a - 1) + ", " + String.valueOf(b);
        ANSWER_C = String.valueOf(a + 1) + ", " + String.valueOf(b - 1);
        ANSWER_D = String.valueOf(a) + ", " + String.valueOf(b + 1);
    }

    private void createProblem_03(){
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int diff = MathUtility.getRandomPositiveNumber_1Digit() + 5;
        int width = length + diff;
        int area = length * width;

        QUESTION = "Find the length and the width of a rectangle garden. Given the area is " + String.valueOf(area)
                + " yards square, and the width is " + String.valueOf(diff) + " yards longer than the length.";
        RIGHT_ANSWER = "Length = " + String.valueOf(length) + " yards, width = " + String.valueOf(width) + " yards.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "Length = " + String.valueOf(width) + " yards, width = " + String.valueOf(length) + " yards.";
        ANSWER_C = "Length = " + String.valueOf(length + 1) + " yards, width = " + String.valueOf(width - 1) + " yards.";
        ANSWER_D = "Length = " + String.valueOf(length - 1) + " yards, width = " + String.valueOf(width + 1) + " yards.";
    }

    private void createProblem_04(){
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int perimeter = 2 * (length + width);
        int area = length * width;

        QUESTION = "Find the length and the width of a rectangle garden. Given the area is " + String.valueOf(area)
                + " yards square, and the perimeter is " + String.valueOf(perimeter) + " yards.";
        RIGHT_ANSWER = "Length = " + String.valueOf(length) + " yards, width = " + String.valueOf(width) + " yards.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "Length = " + String.valueOf(width + 1) + " yards, width = " + String.valueOf(length + 1) + " yards.";
        ANSWER_C = "Length = " + String.valueOf(length + 1) + " yards, width = " + String.valueOf(width - 1) + " yards.";
        ANSWER_D = "Length = " + String.valueOf(length - 1) + " yards, width = " + String.valueOf(width + 1) + " yards.";
    }

    private void createProblem_05(){
        QuadraticEquation Equation = new QuadraticEquation(MathUtility.getRandomNegativeNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit() + 10, 0);

        QUESTION = "The height of a flying object is described by the function of the time (x in second): " + (new QuadraticFunction(Equation)).toString()
                + ". How long does it take the object to go back on the ground?";
        if (QuadraticMath.getDecimalSolution(Equation).get(0) == 0.0){
            RIGHT_ANSWER = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(1)) + " seconds.";

            ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(QuadraticMath.getDecimalSolution(Equation).get(1) + 1.0)) + " seconds.";
            ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(QuadraticMath.getDecimalSolution(Equation).get(1) - 1.0)) + " seconds.";
            ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(QuadraticMath.getDecimalSolution(Equation).get(1) + 1.5)) + " seconds.";
        }
        else {
            RIGHT_ANSWER = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(0)) + " seconds.";

            ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(QuadraticMath.getDecimalSolution(Equation).get(0) + 1.0)) + " seconds.";
            ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(QuadraticMath.getDecimalSolution(Equation).get(0) - 1.0)) + " seconds.";
            ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(QuadraticMath.getDecimalSolution(Equation).get(0) + 1.5)) + " seconds.";
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    private void createProblem_06(){
        QuadraticEquation Equation = new QuadraticEquation(MathUtility.getRandomNegativeNumber_1Digit(), 0, MathUtility.getRandomPositiveNumber_1Digit() + 10);

        QUESTION = "The height of a flying object is described by the function of the time (x in second): " + (new QuadraticFunction(Equation)).toString()
                + ". How long does it take the object to go back on the ground?";
        if (QuadraticMath.getDecimalSolution(Equation).get(0) == 0.0){
            RIGHT_ANSWER = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(1)) + " seconds.";

            ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(QuadraticMath.getDecimalSolution(Equation).get(1) + 1.0)) + " seconds.";
            ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(QuadraticMath.getDecimalSolution(Equation).get(1) - 1.0)) + " seconds.";
            ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(QuadraticMath.getDecimalSolution(Equation).get(1) + 1.5)) + " seconds.";
        }
        else {
            RIGHT_ANSWER = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(0)) + " seconds.";

            ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(QuadraticMath.getDecimalSolution(Equation).get(0) + 1.0)) + " seconds.";
            ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(QuadraticMath.getDecimalSolution(Equation).get(0) - 1.0)) + " seconds.";
            ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(QuadraticMath.getDecimalSolution(Equation).get(0) + 1.5)) + " seconds.";
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    private void createProblem_07(){
        QuadraticEquation Equation = new QuadraticEquation(MathUtility.getRandomNegativeNumber_1Digit(), MathUtility.getRandomPositiveNumber_1Digit() + 10, MathUtility.getRandomPositiveNumber_1Digit() + 10);
        double highestPoint = FunctionMath.getQuadraticVertex(new QuadraticFunction(Equation)).getYCoordinate().getValue();
        highestPoint = MathUtility.round_2_Decimal(highestPoint);

        QUESTION = "The height of a flying object is described by the function of the time (x in second): " + (new QuadraticFunction(Equation)).toString()
                + ". Find the highest point it can reach?";
        RIGHT_ANSWER = String.valueOf(highestPoint);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_2_Decimal(highestPoint + 1.0));
        ANSWER_C = String.valueOf(MathUtility.round_2_Decimal(highestPoint - 1.0));
        ANSWER_D = String.valueOf(MathUtility.round_2_Decimal(highestPoint + 1.5));
    }

    private void createProblem_08(){
        int a = (MathUtility.getRandomPositiveNumber_1Digit() + 1) * -1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int c = MathUtility.getRandomPositiveNumber_4Digit();
        
        QuadraticEquation Equation = new QuadraticEquation(a, b, c);
        double Result = MathUtility.round_1_Decimal((double) (0 - b) / (double) (2 * a));

        QUESTION = "John business profit is showed by the function: " + (new  QuadraticFunction(Equation)).toString()
                + ", x is the time in month. How long does it take him to get the maximum profit? ";
        RIGHT_ANSWER = String.valueOf(Result) + " months.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Result + 1.0)) + " months.";
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Result - 1.0)) + " months.";
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Result + 1.5)) + " months.";
    }

    private void createProblem_09(){
        int a = (MathUtility.getRandomPositiveNumber_1Digit() + 1) * -1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int c = MathUtility.getRandomPositiveNumber_4Digit();

        QuadraticEquation Equation = new QuadraticEquation(a, b, c);
        QuadraticFunction Function = new QuadraticFunction(Equation);
        double Result = MathUtility.round_2_Decimal(FunctionMath.getQuadraticVertex(Function).getYCoordinate().getValue());

        QUESTION = "John business profit is showed by the function: " + Function.toString()
                + ", x is the time in month. Find the maximum profit he can get.";
        RIGHT_ANSWER = "$" + String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "$" + String.valueOf(MathUtility.round_1_Decimal(Result + 1.0));
        ANSWER_C = "$" + String.valueOf(MathUtility.round_1_Decimal(Result - 1.0));
        ANSWER_D = "$" + String.valueOf(MathUtility.round_1_Decimal(Result + 1.5));
    }

    private void createProblem_10(){
        int a = MathUtility.getRandomNegativeNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_4Digit();

        QuadraticEquation Equation = new QuadraticEquation(a, b, c);
        QuadraticFunction Function = new QuadraticFunction(Equation);
        double Result = MathUtility.round_3_Decimal(FunctionMath.getQuadraticVertex(Function).getYCoordinate().getValue());

        QUESTION = "The path of an air plane is described as the function: " + Function.toString()
                + ", x is the time in hour. Find the maximum height it can get.";
        RIGHT_ANSWER = String.valueOf(Result) + " miles.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_3_Decimal(Result + 100.0)) + " miles.";
        ANSWER_C = String.valueOf(MathUtility.round_3_Decimal(Result - 100.0)) + " miles.";
        ANSWER_D = String.valueOf(MathUtility.round_3_Decimal(Result + 1000.0)) + " miles.";
    }

    private void createProblem_11(){
        int a = MathUtility.getRandomNegativeNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_4Digit();

        QuadraticEquation Equation = new QuadraticEquation(a, b, c);
        QuadraticFunction Function = new QuadraticFunction(Equation);
        double Result = MathUtility.round_1_Decimal(FunctionMath.getQuadraticVertex(Function).getXCoordinate().getValue());

        QUESTION = "The path of an air plane is described as the function: " + Function.toString()
                + ", x is the time in hour. How long does it take the air plane to reach the highest level?";
        RIGHT_ANSWER = String.valueOf(Result) + " hours.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.round_1_Decimal(Result + 100.0)) + " hours.";
        ANSWER_C = String.valueOf(MathUtility.round_1_Decimal(Result - 100.0)) + " hours.";
        ANSWER_D = String.valueOf(MathUtility.round_1_Decimal(Result + 1000.0)) + " hours.";
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
