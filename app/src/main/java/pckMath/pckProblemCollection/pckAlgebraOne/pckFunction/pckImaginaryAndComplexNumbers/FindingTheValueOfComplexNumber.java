package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckImaginaryAndComplexNumbers;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 7/18/2017.
 */

public class FindingTheValueOfComplexNumber extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindingTheValueOfComplexNumber(){
        createProblem();
        swapAnswer();
    }

    public FindingTheValueOfComplexNumber(String Question,
                                      String RightAnswer,
                                      String AnswerA,
                                      String AnswerB,
                                      String AnswerC,
                                      String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 1;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FindingTheValueOfComplexNumber(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int result = a * a + b * b;

        QUESTION = "|" + String.valueOf(a) + " + " + String.valueOf(b) + "i| = ?";
        if (MathUtility.isSquaredNumber(result)){
            int Temp = (int) Math.pow(result, 0.5);

            RIGHT_ANSWER = String.valueOf(Temp);

            ANSWER_B = String.valueOf(a);
            ANSWER_C = String.valueOf(Temp + 1);
            ANSWER_D = String.valueOf(Temp - 1);
        }
        else {
            RIGHT_ANSWER = InfoCollector.getSquareRootSign() + "(" + String.valueOf(result) + ")";

            ANSWER_B = InfoCollector.getSquareRootSign() + "(" + String.valueOf(result - 1) + ")";
            ANSWER_C = InfoCollector.getSquareRootSign() + "(" + String.valueOf(result + 1) + ")";
            ANSWER_D = InfoCollector.getSquareRootSign() + "(" + String.valueOf(result - 2) + ")";
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
