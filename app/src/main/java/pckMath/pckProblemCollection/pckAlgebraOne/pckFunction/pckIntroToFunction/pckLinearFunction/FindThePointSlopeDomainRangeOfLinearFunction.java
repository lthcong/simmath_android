package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckLinearFunction;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.FunctionMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.Interval;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.LineMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLine.Point;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/22/2017.
 */

public class FindThePointSlopeDomainRangeOfLinearFunction extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindThePointSlopeDomainRangeOfLinearFunction(){
        createProblem();
        swapAnswer();
    }

    public FindThePointSlopeDomainRangeOfLinearFunction(String Question,
                                                        String RightAnswer,
                                                        String AnswerA,
                                                        String AnswerB,
                                                        String AnswerC,
                                                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }

        return new FindThePointSlopeDomainRangeOfLinearFunction(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  1 POINTS - GIVEN X COORDINATE
    private void createProblem_01(){
        LinearFunction RandomFunction = FunctionMath.getRandomLinearFunction();
        int XCoordinate = MathUtility.getRandomNumber_1Digit();
        Point RandomPoint = FunctionMath.getRandomPoint(RandomFunction, XCoordinate);

        QUESTION = "Find the point of the graph of the function "
                + RandomFunction.toString() + ". Given the X coordinate is " + String.valueOf(XCoordinate) + ".";
        RIGHT_ANSWER = RandomPoint.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Point(XCoordinate, MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = (new Point(FractionMath.getRandomProperFraction(), FractionMath.getRandomImproperFraction())).toString();
        ANSWER_D = (new Point(FractionMath.getRandomImproperFraction(), FractionMath.getRandomUnitFraction())).toString();
    }

    //  1 POINTS - GIVEN Y COORDINATE
    private void createProblem_02(){
        LinearFunction RandomFunction = FunctionMath.getRandomLinearFunction();
        int XCoordinate = MathUtility.getRandomNumber_1Digit();
        Point RandomPoint = FunctionMath.getRandomPoint(RandomFunction, XCoordinate);

        QUESTION = "Find the point of the graph of the function " + RandomFunction.toString()
                + ". Given the Y coordinate is " + String.valueOf(RandomPoint.getYCoordinate().toString()) + ".";
        RIGHT_ANSWER = RandomPoint.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Point(XCoordinate, MathUtility.getRandomNumber_1Digit())).toString();
        ANSWER_C = (new Point(FractionMath.getRandomProperFraction(), FractionMath.getRandomImproperFraction())).toString();
        ANSWER_D = (new Point(FractionMath.getRandomImproperFraction(), FractionMath.getRandomUnitFraction())).toString();
    }

    //  2 POINTS
    private void createProblem_03(){
        LinearFunction RandomFunction = FunctionMath.getRandomLinearFunction();

        int XCoordinateA = MathUtility.getRandomNumber_1Digit();
        int XCoordinateB = MathUtility.getRandomNumber_1Digit() + MathUtility.getRandomPositiveNumber_1Digit();

        Point PointA = FunctionMath.getRandomPoint(RandomFunction, XCoordinateA);
        Point PointB = FunctionMath.getRandomPoint(RandomFunction, XCoordinateB);

        QUESTION = "Find two points on the graph of the function: " + RandomFunction.toString() + ".";
        RIGHT_ANSWER = PointA.toString() + ", " + PointB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = LineMath.getRandomPoint().toString() + ", " + LineMath.getRandomPoint().toString();
        ANSWER_C = LineMath.getRandomPoint().toString() + ", " + LineMath.getRandomPoint().toString();
        ANSWER_D = LineMath.getRandomPoint().toString() + ", " + LineMath.getRandomPoint().toString();
    }

    //  SLOPE
    private void createProblem_04(){
        LinearFunction RandomFunction = FunctionMath.getRandomLinearFunction();

        int XCoordinateA = MathUtility.getRandomNumber_1Digit();
        int XCoordinateB = MathUtility.getRandomNumber_1Digit() + MathUtility.getRandomPositiveNumber_1Digit();

        Point PointA = FunctionMath.getRandomPoint(RandomFunction, XCoordinateA);
        Point PointB = FunctionMath.getRandomPoint(RandomFunction, XCoordinateB);

        QUESTION = "Find the slope of the line pass through two points " + PointA.toString() + ", " + PointB.toString() + ".";
        RIGHT_ANSWER = String.valueOf(RandomFunction.getSlope());

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_C = String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = FractionMath.getRandomFraction().toString();
    }

    //  DOMAIN
    private void createProblem_05(){
        LinearFunction Function = FunctionMath.getRandomLinearFunction();

        QUESTION = "Find the domain of this function: " + Function.toString() + ".";
        RIGHT_ANSWER = (new Interval()).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, MathUtility.getRandomNegativeNumber(), MathUtility.getRandomPositiveNumber(), false)).toString();
        ANSWER_C = (new Interval(true, MathUtility.getRandomNegativeNumber(), MathUtility.getRandomPositiveNumber(), false)).toString();
        ANSWER_D = (new Interval(false, MathUtility.getRandomNegativeNumber(), MathUtility.getRandomPositiveNumber(), true)).toString();
    }

    //  RANGE
    private void createProblem_06(){
        LinearFunction Function = FunctionMath.getRandomLinearFunction();

        QUESTION = "Find the range of this function: " + Function.toString() + ".";
        RIGHT_ANSWER = (new Interval()).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, MathUtility.getRandomNegativeNumber(), MathUtility.getRandomPositiveNumber(), false)).toString();
        ANSWER_C = (new Interval(true, MathUtility.getRandomNegativeNumber(), MathUtility.getRandomPositiveNumber(), false)).toString();
        ANSWER_D = (new Interval(false, MathUtility.getRandomNegativeNumber(), MathUtility.getRandomPositiveNumber(), true)).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
