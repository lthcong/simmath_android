package pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckSet;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.Interval;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.OrderedPairs;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.Set;

/**
 * Created by Cong on 7/21/2017.
 */

public class WriteSolutionInSetNotation extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public WriteSolutionInSetNotation(){
        createProblem();
        swapAnswer();
    }

    public WriteSolutionInSetNotation(String Question,
                                               String RightAnswer,
                                               String AnswerA,
                                               String AnswerB,
                                               String AnswerC,
                                               String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 15;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            case 14:
                createProblem_15();
                break;
            default:
                createProblem_01();
                break;
        }

        return new WriteSolutionInSetNotation(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  x < a
    private void createProblem_01(){
        int a = MathUtility.getRandomNumber_1Digit();
        Interval Result = new Interval(Integer.MIN_VALUE, a);

        QUESTION = "Rewrite this inequality into interval notation: x < " + String.valueOf(a);
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(0, a)).toString();
        ANSWER_C = (new Interval()).toString();
        ANSWER_D = (new Interval(a, Integer.MAX_VALUE)).toString();
    }

    //  x > a
    private void createProblem_02(){
        int a = MathUtility.getRandomNumber_1Digit();
        Interval Result = new Interval(a, Integer.MAX_VALUE);

        QUESTION = "Rewrite this inequality into interval notation: x > " + String.valueOf(a);
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(0, a)).toString();
        ANSWER_C = (new Interval()).toString();
        ANSWER_D = (new Interval(Integer.MIN_VALUE, a)).toString();
    }

    //  x <= a
    private void createProblem_03(){
        int a = MathUtility.getRandomNumber_1Digit();
        Interval Result = new Interval(false, Integer.MIN_VALUE, a, true);

        QUESTION = "Rewrite this inequality into interval notation: x " + InfoCollector.getLessThanOrEqualSign() + String.valueOf(a);
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(0, a)).toString();
        ANSWER_C = (new Interval()).toString();
        ANSWER_D = (new Interval(Integer.MIN_VALUE, a)).toString();
    }

    //  x => a
    private void createProblem_04(){
        int a = MathUtility.getRandomNumber_1Digit();
        Interval Result = new Interval(true, a, Integer.MAX_VALUE, false);

        QUESTION = "Rewrite this inequality into interval notation: x " + InfoCollector.getGreaterThanOrEqualSign() + String.valueOf(a);
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(0, a)).toString();
        ANSWER_C = (new Interval()).toString();
        ANSWER_D = (new Interval(a, Integer.MAX_VALUE)).toString();
    }

    //  x is all REAL
    private void createProblem_05(){
        int a = MathUtility.getRandomNumber_1Digit();
        Interval Result = new Interval();

        QUESTION = "Rewrite this inequality into interval notation: x " + InfoCollector.getElementOf() + " R";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(0, a)).toString();
        ANSWER_C = (new Interval(a + 1, a + 5)).toString();
        ANSWER_D = (new Interval(a, Integer.MAX_VALUE)).toString();
    }

    //  a < x < b
    private void createProblem_06(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = a + MathUtility.getRandomPositiveNumber_1Digit();
        Interval Result = new Interval(false, a, b, false);

        QUESTION = "Rewrite this inequality into interval notation: " + String.valueOf(a) + " < x < " + String.valueOf(b) + ".";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(0, a)).toString();
        ANSWER_C = (new Interval(b, 0)).toString();
        ANSWER_D = (new Interval(a, Integer.MAX_VALUE)).toString();

    }

    //  a <= x < b
    private void createProblem_07(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = a + MathUtility.getRandomPositiveNumber_1Digit();
        Interval Result = new Interval(true, a, b, false);

        QUESTION = "Rewrite this inequality into interval notation: " + String.valueOf(a) + InfoCollector.getLessThanOrEqualSign() + " x < " + String.valueOf(b) + ".";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, a, b, true)).toString();
        ANSWER_C = (new Interval(a, b)).toString();
        ANSWER_D = (new Interval(a, Integer.MAX_VALUE)).toString();

    }

    //  a < x <= b
    private void createProblem_08(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = a + MathUtility.getRandomPositiveNumber_1Digit();
        Interval Result = new Interval(false, a, b, true);

        QUESTION = "Rewrite this inequality into interval notation: " + String.valueOf(a) + " < x " + InfoCollector.getLessThanOrEqualSign() + String.valueOf(b) + ".";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(true, a, b, false)).toString();
        ANSWER_C = (new Interval(a, b)).toString();
        ANSWER_D = (new Interval(a, Integer.MAX_VALUE)).toString();

    }

    //  a <= x <= b
    private void createProblem_09(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = a + MathUtility.getRandomPositiveNumber_1Digit();
        Interval Result = new Interval(true, a, b, true);

        QUESTION = "Rewrite this inequality into interval notation: " + String.valueOf(a) + InfoCollector.getLessThanOrEqualSign() + " x " + InfoCollector.getLessThanOrEqualSign() + String.valueOf(b) + ".";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(true, a, b, false)).toString();
        ANSWER_C = (new Interval(false, a, b, true)).toString();
        ANSWER_D = (new Interval(a, Integer.MAX_VALUE)).toString();

    }

    //  x < a OR  x > b
    private void createProblem_10(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = a + MathUtility.getRandomPositiveNumber_1Digit();
        Interval ResultA = new Interval(false, Integer.MIN_VALUE, a, false);
        Interval ResultB = new Interval(false, b, Integer.MAX_VALUE, false);

        QUESTION = "Rewrite this inequality into interval notation: x < " + String.valueOf(a)
                + "OR  x > " + String.valueOf(b) + ".";
        RIGHT_ANSWER = ResultA.toString() + InfoCollector.getUnionSet() + ResultB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(true, a, b, true)).toString();
        ANSWER_C = (new Interval(false, a, b, false)).toString();
        ANSWER_D = (new Interval()).toString();

    }

    //  x <= a OR  x > b
    private void createProblem_11(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = a + MathUtility.getRandomPositiveNumber_1Digit();
        Interval ResultA = new Interval(false, Integer.MIN_VALUE, a, true);
        Interval ResultB = new Interval(false, b, Integer.MAX_VALUE, false);

        QUESTION = "Rewrite this inequality into interval notation: x " + InfoCollector.getLessThanOrEqualSign() + String.valueOf(a)
                + "OR  x > " + String.valueOf(b) + ".";
        RIGHT_ANSWER = ResultA.toString() + InfoCollector.getUnionSet() + ResultB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(true, a, b, true)).toString();
        ANSWER_C = (new Interval(false, a, b, false)).toString();
        ANSWER_D = (new Interval()).toString();

    }

    //  x < a OR  x >= b
    private void createProblem_12(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = a + MathUtility.getRandomPositiveNumber_1Digit();
        Interval ResultA = new Interval(false, Integer.MIN_VALUE, a, false);
        Interval ResultB = new Interval(false, b, Integer.MAX_VALUE, true);

        QUESTION = "Rewrite this inequality into interval notation: x < " + String.valueOf(a)
                + "OR  x" + InfoCollector.getGreaterThanOrEqualSign() + String.valueOf(b) + ".";
        RIGHT_ANSWER = ResultA.toString() + InfoCollector.getUnionSet() + ResultB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(true, a, b, true)).toString();
        ANSWER_C = (new Interval(false, a, b, false)).toString();
        ANSWER_D = (new Interval()).toString();

    }

    //  x <= a OR  x >= b
    private void createProblem_13(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = a + MathUtility.getRandomPositiveNumber_1Digit();
        Interval ResultA = new Interval(false, Integer.MIN_VALUE, a, true);
        Interval ResultB = new Interval(false, b, Integer.MAX_VALUE, true);

        QUESTION = "Rewrite this inequality into interval notation: x " + InfoCollector.getLessThanOrEqualSign() + String.valueOf(a)
                + "OR  x" + InfoCollector.getGreaterThanOrEqualSign() + String.valueOf(b) + ".";
        RIGHT_ANSWER = ResultA.toString() + InfoCollector.getUnionSet() + ResultB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(true, a, b, true)).toString();
        ANSWER_C = (new Interval(false, a, b, false)).toString();
        ANSWER_D = (new Interval()).toString();

    }

    //  x = a
    private void createProblem_14(){
        int a = MathUtility.getRandomNumber_1Digit();
        Set Result = new Set();
        Result.add(a);

        QUESTION = "Rewrite this solution into set notation: x = " + String.valueOf(a);
        RIGHT_ANSWER = "x = " + Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval()).toString();
        ANSWER_C = (new Interval(true, a, Integer.MAX_VALUE, false)).toString();
        ANSWER_D = (new Interval(false, Integer.MIN_VALUE, a, true)).toString();
    }

    //  x = a, y = b
    private void createProblem_15(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        OrderedPairs Result = new OrderedPairs(a, b);

        QUESTION = "Rewrite this solution into ordered pairs notation: x = " + String.valueOf(a) + ", y = " + String.valueOf(b) + ".";
        RIGHT_ANSWER = "(x, y) = " + Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Interval(false, a, b, false)).toString();
        ANSWER_C = "(x, y) = " + new OrderedPairs(MathUtility.getRandomNumber_1Digit(), MathUtility.getRandomNumber_1Digit()).toString();
        ANSWER_D = (new Interval(true, a, b, true)).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
