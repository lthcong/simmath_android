package pckMath.pckProblemCollection.pckAlgebraOne;

import pckInfo.InfoCollector;

/**
 * Created by Cong on 6/26/2017.
 */

public class Variable {

    private String NAME;
    private int EXPONENT;
    private double VALUE;

    public Variable(){
        NAME = "";
        EXPONENT = 1;
        VALUE = 0;
    }

    public Variable(String Name){
        NAME = Name;
        EXPONENT = 1;
        VALUE = 0;
    }

    public Variable(String Name, int Exponent){
        NAME = Name;
        EXPONENT = Exponent;
        VALUE = 0;
    }

    public Variable(String Name, int Exponent, double Value){
        NAME = Name;
        EXPONENT = Exponent;
        VALUE = Value;
    }

    public void setName(String Name){
        NAME = Name;
    }

    public String getName(){
        return NAME;
    }

    public char getCharName(){
        return NAME.charAt(0);
    }

    public void setExponent(int Exponent){
        EXPONENT = Exponent;
    }

    public int getExponent(){
        return EXPONENT;
    }

    public void setValue(double Value){
        VALUE = Value;
    }

    public double getValue(){
        return VALUE;
    }

    public double evaluate(){
        return Math.pow(VALUE, EXPONENT);
    }

    public boolean isNull(){
        boolean isNullVariable = false;
        if (NAME.trim().length() == 0 || EXPONENT == 0){
            isNullVariable = true;
        }

        return isNullVariable;
    }

    public boolean isTheSameVariable(Variable SecondVariable){
        boolean isTheSame = false;
        if (NAME.equalsIgnoreCase(SecondVariable.getName())){
            isTheSame = true;
        }

        return isTheSame;
    }

    public boolean isLikeTerms(Variable SecondVariable){
        boolean isLike = false;

        if (EXPONENT == 0 && SecondVariable.getExponent() == 0){
            isLike = true;
        }
        else {
            if (NAME.equalsIgnoreCase(SecondVariable.getName())
                    && EXPONENT == SecondVariable.getExponent()){
                isLike = true;
            }
        }

        return isLike;
    }

    public String toString(){
        String VarString;
        switch (EXPONENT){
            case 0:
                VarString = "1";
                break;
            case 1:
                VarString = NAME;
                break;
            default:
                VarString = NAME + InfoCollector.putExponent(EXPONENT);
                break;
        }

        return VarString;
    }

}
