package pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.pckSystemOfLinearEquation;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.SystemOfLinearEquations;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/10/2017.
 */

public class SolvingSystemOfLinearEquations extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SolvingSystemOfLinearEquations(){
        createProblem();
        swapAnswer();
    }

    public SolvingSystemOfLinearEquations(String Question,
                                 String RightAnswer,
                                 String AnswerA,
                                 String AnswerB,
                                 String AnswerC,
                                 String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 8;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SolvingSystemOfLinearEquations(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    /*
    *   ax + by = c
    *   dx + ey = f
    * */
    private void createProblem_01(){
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1);
        Polynomial FirstEquation = AlgebraMath.add(FirstPolynomial, SecondPolynomial);

        FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1);
        Polynomial SecondEquation = AlgebraMath.add(FirstPolynomial, SecondPolynomial);

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations(FirstEquation, SecondEquation);

        QUESTION = SOLE.toString();
        RIGHT_ANSWER = SOLE.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + FractionMath.getRandomFraction().toString() + ", y = " + MathUtility.getRandomNumber_1Digit() + ".";
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", y = " + FractionMath.getRandomFraction().toString() + ".";
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", y = " + MathUtility.getRandomNumber_1Digit() + ".";
    }

    /*
    *   (ax + by + c) + (ax + by + c) = 0
    *   (dx + ey + f) = (dx + ey + f)
    * */
    private void createProblem_02(){
        Polynomial FirstPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial SecondPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial ThirdPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial ForthPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations(AlgebraMath.add(FirstPolynomial, SecondPolynomial),
                                                                    AlgebraMath.sub(ThirdPolynomial, ForthPolynomial));

        QUESTION = FirstPolynomial.toString() + " + " + SecondPolynomial.toString() + " = 0<br>";
        QUESTION += ThirdPolynomial.toString() + " = " + ForthPolynomial.toString();
        RIGHT_ANSWER = SOLE.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + FractionMath.getRandomFraction().toString() + ", y = " + MathUtility.getRandomNumber_1Digit() + ".";
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", y = " + FractionMath.getRandomFraction().toString() + ".";
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", y = " + MathUtility.getRandomNumber_1Digit() + ".";
     }

    /*
   *   (ax + by + c) + A(ax + by + c) = A(ax + by + c)
   *   (dx + ey + f) + A(dx + ey + f) = (dx + ey + f)
   * */
    private void createProblem_03(){
        Polynomial FirstPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial SecondPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial ThirdPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial ForthPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial FifthPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial SixthPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));

        int NumberA = MathUtility.getRandomNumber_1Digit();
        int NumberB = MathUtility.getRandomNumber_1Digit();
        int NumberC = MathUtility.getRandomNumber_1Digit();

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations(
                AlgebraMath.sub(AlgebraMath.add(FirstPolynomial, AlgebraMath.mul(SecondPolynomial, NumberA)), AlgebraMath.mul(ThirdPolynomial, NumberB)),
                AlgebraMath.sub(AlgebraMath.add(ForthPolynomial, AlgebraMath.mul(FifthPolynomial, NumberC)), SixthPolynomial));

        QUESTION = FirstPolynomial.toString() + " + " + String.valueOf(NumberA) + "(" + SecondPolynomial.toString() + ") = " + String.valueOf(NumberB) + "(" + ThirdPolynomial.toString() + ")<br>";
        QUESTION += ThirdPolynomial.toString() + " + " + String.valueOf(NumberC) + "(" + ForthPolynomial.toString() + ") = " + SixthPolynomial.toString();
        RIGHT_ANSWER = SOLE.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + FractionMath.getRandomFraction().toString() + ", y = " + MathUtility.getRandomNumber_1Digit() + ".";
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", y = " + FractionMath.getRandomFraction().toString() + ".";
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", y = " + MathUtility.getRandomNumber_1Digit() + ".";
    }

    /*
   *   (ax + by + c) + (ax + by + c) = (ax + by + c) + (ax + by + c)
   *   (dx + ey + f) + (dx + ey + f) = (dx + ey + f) + (dx + ey + f)
   * */
    private void createProblem_04(){
        Polynomial FirstPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial SecondPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial ThirdPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial ForthPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial FifthPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial SixthPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial SeventhPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial EighthPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations(
                AlgebraMath.sub(AlgebraMath.add(FirstPolynomial, SecondPolynomial), AlgebraMath.add(ThirdPolynomial, ForthPolynomial)),
                        AlgebraMath.sub(AlgebraMath.add(FifthPolynomial, SixthPolynomial), AlgebraMath.add(SeventhPolynomial, EighthPolynomial)));

        QUESTION = FirstPolynomial.toString() + " + " + SecondPolynomial.toString() + " = " + ThirdPolynomial.toString() + " + " + ForthPolynomial.toString() + "<br>";
        QUESTION += FifthPolynomial.toString() + " + " + SixthPolynomial.toString() + " = " + SeventhPolynomial.toString() + " + " + EighthPolynomial.toString();
        RIGHT_ANSWER = SOLE.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + FractionMath.getRandomFraction().toString() + ", y = " + MathUtility.getRandomNumber_1Digit() + ".";
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", y = " + FractionMath.getRandomFraction().toString() + ".";
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", y = " + MathUtility.getRandomNumber_1Digit() + ".";
    }

    /*
   *   (ax + by + c) + (ax + by + c) = (ax + by + c)
   *   (dx + ey + f) = (dx + ey + f) + (dx + ey + f)
   * */
    private void createProblem_05(){
        Polynomial FirstPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial SecondPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial ThirdPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial ForthPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial FifthPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial SixthPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations(
                AlgebraMath.sub(AlgebraMath.add(FirstPolynomial, SecondPolynomial), ThirdPolynomial),
                AlgebraMath.sub(ForthPolynomial, AlgebraMath.add(FifthPolynomial, SixthPolynomial)));

        QUESTION = FirstPolynomial.toString() + " + " + SecondPolynomial.toString() + " = " + ThirdPolynomial.toString() + "<br>";
        QUESTION += ForthPolynomial.toString() + " = " + FifthPolynomial.toString() + " + " + SixthPolynomial.toString();
        RIGHT_ANSWER = SOLE.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + FractionMath.getRandomFraction().toString() + ", y = " + MathUtility.getRandomNumber_1Digit() + ".";
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", y = " + FractionMath.getRandomFraction().toString() + ".";
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", y = " + MathUtility.getRandomNumber_1Digit() + ".";
    }

    /*
   *   A(ax + by + c) = A(ax + by + c)
   *   (dx + ey + f) = (dx + ey + f)
   * */
    private void createProblem_06(){
        Polynomial FirstPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial SecondPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial ThirdPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial ForthPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));

        int NumberA = MathUtility.getRandomNumber_1Digit();
        int NumberB = MathUtility.getRandomNumber_1Digit();

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations(
                AlgebraMath.sub(AlgebraMath.mul(FirstPolynomial, NumberA), AlgebraMath.mul(SecondPolynomial, NumberB)),
                AlgebraMath.sub(ThirdPolynomial, ForthPolynomial));

        QUESTION = String.valueOf(NumberA) + "(" + FirstPolynomial.toString() + ") = " + String.valueOf(NumberB) + "(" + SecondPolynomial.toString() + ")<br>";
        QUESTION += ThirdPolynomial.toString() + " = " + ForthPolynomial.toString();

        RIGHT_ANSWER = SOLE.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + FractionMath.getRandomFraction().toString() + ", y = " + MathUtility.getRandomNumber_1Digit() + ".";
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", y = " + FractionMath.getRandomFraction().toString() + ".";
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", y = " + MathUtility.getRandomNumber_1Digit() + ".";
    }

    /*
    *   (ax + by + c) + (ax + by + c) = 0
    *   (dx + ey + f) = (dx + ey + f)
    * */
    private void createProblem_07(){
        Polynomial FirstPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        Polynomial SecondPolynomial = AlgebraMath.add(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1), AlgebraMath.getRandomPolynomialWithHighestExponent("y", 1));
        int Number = MathUtility.getRandomNegativeNumber_1Digit();
        Polynomial ThirdPolynomial = AlgebraMath.mul(FirstPolynomial, Number);
        Polynomial ForthPolynomial = AlgebraMath.mul(SecondPolynomial, (0 - Number));

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations(AlgebraMath.add(FirstPolynomial, SecondPolynomial),
                AlgebraMath.sub(ThirdPolynomial, ForthPolynomial));

        QUESTION = FirstPolynomial.toString() + " + " + SecondPolynomial.toString() + " = 0<br>";
        QUESTION += ThirdPolynomial.toString() + " = " + ForthPolynomial.toString();
        RIGHT_ANSWER = SOLE.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + FractionMath.getRandomFraction().toString() + ", y = " + MathUtility.getRandomNumber_1Digit() + ".";
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", y = " + FractionMath.getRandomFraction().toString() + ".";
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", y = " + MathUtility.getRandomNumber_1Digit() + ".";
    }

    /*
    *   ax + by = c
    *   dx + ey = f
    * */
    private void createProblem_08(){
        String FirstVariable = MathUtility.getVariable(), SecondVariable = MathUtility.getVariable();
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int Temp = MathUtility.getRandomNumber_1Digit();

        int d = a * Temp;
        int e = b * Temp;
        int f = MathUtility.getRandomNumber_1Digit() * Temp;

        SystemOfLinearEquations SOLE = new SystemOfLinearEquations(FirstVariable, SecondVariable, a, b, c, d, e, f);

        QUESTION = SOLE.toString();
        RIGHT_ANSWER = SOLE.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = " + FractionMath.getRandomFraction().toString() + ", y = " + MathUtility.getRandomNumber_1Digit() + ".";
        ANSWER_C = "x = " + FractionMath.getRandomFraction().toString() + ", y = " + FractionMath.getRandomFraction().toString() + ".";
        ANSWER_D = "x = " + MathUtility.getRandomNumber_1Digit() + ", y = " + MathUtility.getRandomNumber_1Digit() + ".";
    }

    @Override
    public String getQuestion() {
        QUESTION = "Solve this system of linear equations: <br>"
                + QUESTION
                + "<br>x = ?, y = ?";
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
