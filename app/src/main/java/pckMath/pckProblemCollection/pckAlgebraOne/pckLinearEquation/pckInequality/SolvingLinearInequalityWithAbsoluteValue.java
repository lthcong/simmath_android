package pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.pckInequality;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/13/2017.
 */

public class SolvingLinearInequalityWithAbsoluteValue extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SolvingLinearInequalityWithAbsoluteValue(){
        createProblem();
        swapAnswer();
    }

    public SolvingLinearInequalityWithAbsoluteValue(String Question,
                                                  String RightAnswer,
                                                  String AnswerA,
                                                  String AnswerB,
                                                  String AnswerC,
                                                  String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 8;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SolvingLinearInequalityWithAbsoluteValue(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    /*  |ax + b| > c    
     *  ax + b < -c     OR      ax + b > c
     *  x < (-c - b)/a          x > (c - b)/a
     */
    private void createProblem_01(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();
        
        Fraction ResultA = new Fraction((b - c), a);
        Fraction ResultB = new Fraction(0 - c - b, a);

        QUESTION = "|" + String.valueOf(a) + VarName + " + " + String.valueOf(b) + "| > " + String.valueOf(c);

        if (ResultA.getValue() < ResultB.getValue()){
            RIGHT_ANSWER = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();

            ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
            ANSWER_C = VarName + " > " + ResultA.toString() + " OR " + VarName + " < " + ResultB.toString();
            ANSWER_D = VarName + " < " + ResultB.toString() + " OR " + VarName + " > " + ResultA.toString();
        }
        else {
            if (ResultA.getValue() == ResultB.getValue()){
                RIGHT_ANSWER = VarName + " = " + ResultA.toString();

                ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
                ANSWER_C = VarName + " > " + ResultA.toString() + " OR " + VarName + " < " + ResultB.toString();
                ANSWER_D = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
            }
            else {
                RIGHT_ANSWER = VarName + " > " + ResultA.toString() + " OR " + VarName + " < " + ResultB.toString();

                ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
                ANSWER_C = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
                ANSWER_D = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;        
    }

    /*  |ax + b| > c    
     *  ax + b < -c     OR      ax + b > c
     *  x > (-c - b)/a          x < (c - b)/a
     */
    private void createProblem_02(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomNegativeNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        Fraction ResultA = new Fraction((b - c), a);
        Fraction ResultB = new Fraction(0 - c - b, a);

        QUESTION = "|" + String.valueOf(a) + VarName + " + " + String.valueOf(b) + "| > " + String.valueOf(c);

        if (ResultA.getValue() < ResultB.getValue()){
            RIGHT_ANSWER = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();

            ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
            ANSWER_C = VarName + " > " + ResultA.toString() + " OR " + VarName + " < " + ResultB.toString();
            ANSWER_D = VarName + " < " + ResultB.toString() + " OR " + VarName + " > " + ResultA.toString();
        }
        else {
            if (ResultA.getValue() == ResultB.getValue()){
                RIGHT_ANSWER = VarName + " = " + ResultA.toString();

                ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
                ANSWER_C = VarName + " > " + ResultA.toString() + " OR " + VarName + " < " + ResultB.toString();
                ANSWER_D = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
            }
            else {
                RIGHT_ANSWER = VarName + " > " + ResultA.toString() + " OR " + VarName + " < " + ResultB.toString();

                ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
                ANSWER_C = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
                ANSWER_D = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }
    
    /*  |ax + b| < c    
     *  -c < ax + b < c
     *  (-c - b)/a < x < (c - b)/a
     */
    private void createProblem_03(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        Fraction ResultA = new Fraction(0 - c - b, a);
        Fraction ResultB = new Fraction((b - c), a);

        QUESTION = "|" + String.valueOf(a) + VarName + " + " + String.valueOf(b) + "| < " + String.valueOf(c);
        RIGHT_ANSWER = ResultA.toString() + " < " + VarName + " < " + ResultB.toString();
        
        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = ResultB.toString() + " < " + VarName + " < " + ResultA.toString();
        ANSWER_C = FractionMath.getReciprocal(ResultA).toString() + " < " + VarName + " < " + FractionMath.getReciprocal(ResultA).toString();
        ANSWER_D = ResultA.toString() + " > " + VarName + " > " + ResultB.toString();
    }

    /*  |ax + b| < c    
     *  -c < ax + b < c
     *  (-c - b)/a > x > (c - b)/a
     */
    private void createProblem_04(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomNegativeNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        Fraction ResultA = new Fraction(0 - c - b, a);
        Fraction ResultB = new Fraction((b - c), a);

        QUESTION = "|" + String.valueOf(a) + VarName + " + " + String.valueOf(b) + "| < " + String.valueOf(c);
        RIGHT_ANSWER = ResultA.toString() + " > " + VarName + " > " + ResultB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = ResultB.toString() + " < " + VarName + " < " + ResultA.toString();
        ANSWER_C = FractionMath.getReciprocal(ResultA).toString() + " < " + VarName + " < " + FractionMath.getReciprocal(ResultA).toString();
        ANSWER_D = ResultA.toString() + " < " + VarName + " < " + ResultB.toString();
    }

    /*  |ax + b| >= c    
     *  ax + b <= -c     OR      ax + b >= c
     *  x <= (-c - b)/a          x >= (c - b)/a
     */
    private void createProblem_05(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        Fraction ResultA = new Fraction((b - c), a);
        Fraction ResultB = new Fraction(0 - c - b, a);

        QUESTION = "|" + String.valueOf(a) + VarName + " + " + String.valueOf(b) + "| " + InfoCollector.getGreaterThanOrEqualSign() + String.valueOf(c);

        if (ResultA.getValue() < ResultB.getValue()){
            RIGHT_ANSWER = VarName + InfoCollector.getLessThanOrEqualSign() + ResultA.toString() + " OR " + VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultB.toString();

            ANSWER_B = VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultB.toString() + " OR " + VarName + InfoCollector.getLessThanOrEqualSign() + ResultA.toString();
            ANSWER_C = VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultA.toString() + " OR " + VarName + InfoCollector.getLessThanOrEqualSign() + ResultB.toString();
            ANSWER_D = VarName + InfoCollector.getLessThanOrEqualSign() + ResultB.toString() + " OR " + VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultA.toString();
        }
        else {
            if (ResultA.getValue() == ResultB.getValue()){
                RIGHT_ANSWER = VarName + " = " + ResultA.toString();

                ANSWER_B = VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultB.toString() + " OR " + VarName + InfoCollector.getLessThanOrEqualSign() + ResultA.toString();
                ANSWER_C = VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultA.toString() + " OR " + VarName + InfoCollector.getLessThanOrEqualSign() + ResultB.toString();
                ANSWER_D = VarName + InfoCollector.getLessThanOrEqualSign() + ResultA.toString() + " OR " + VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultB.toString();
            }
            else {
                RIGHT_ANSWER = VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultA.toString() + " OR " + VarName + InfoCollector.getLessThanOrEqualSign() + ResultB.toString();

                ANSWER_B = VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultB.toString() + " OR " + VarName + InfoCollector.getLessThanOrEqualSign() + ResultA.toString();
                ANSWER_C = VarName + InfoCollector.getLessThanOrEqualSign() + ResultA.toString() + " OR " + VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultB.toString();
                ANSWER_D = VarName + InfoCollector.getLessThanOrEqualSign() + ResultA.toString() + " OR " + VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultB.toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    /*  |ax + b| >= c
     *  ax + b <= -c     OR      ax + b >= c
     *  x >= (-c - b)/a          x <= (c - b)/a
     */
    private void createProblem_06(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomNegativeNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        Fraction ResultA = new Fraction((b - c), a);
        Fraction ResultB = new Fraction(0 - c - b, a);

        QUESTION = "|" + String.valueOf(a) + VarName + " + " + String.valueOf(b) + "| " + InfoCollector.getGreaterThanOrEqualSign() + String.valueOf(c);

        if (ResultA.getValue() < ResultB.getValue()){
            RIGHT_ANSWER = VarName + InfoCollector.getLessThanOrEqualSign() + ResultA.toString() + " OR " + VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultB.toString();

            ANSWER_B = VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultB.toString() + " OR " + VarName + InfoCollector.getLessThanOrEqualSign() + ResultA.toString();
            ANSWER_C = VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultA.toString() + " OR " + VarName + InfoCollector.getLessThanOrEqualSign() + ResultB.toString();
            ANSWER_D = VarName + InfoCollector.getLessThanOrEqualSign() + ResultB.toString() + " OR " + VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultA.toString();
        }
        else {
            if (ResultA.getValue() == ResultB.getValue()){
                RIGHT_ANSWER = VarName + " = " + ResultA.toString();

                ANSWER_B = VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultB.toString() + " OR " + VarName + InfoCollector.getLessThanOrEqualSign() + ResultA.toString();
                ANSWER_C = VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultA.toString() + " OR " + VarName + InfoCollector.getLessThanOrEqualSign() + ResultB.toString();
                ANSWER_D = VarName + InfoCollector.getLessThanOrEqualSign() + ResultA.toString() + " OR " + VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultB.toString();
            }
            else {
                RIGHT_ANSWER = VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultA.toString() + " OR " + VarName + InfoCollector.getLessThanOrEqualSign() + ResultB.toString();

                ANSWER_B = VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultB.toString() + " OR " + VarName + InfoCollector.getLessThanOrEqualSign() + ResultA.toString();
                ANSWER_C = VarName + InfoCollector.getLessThanOrEqualSign() + ResultA.toString() + " OR " + VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultB.toString();
                ANSWER_D = VarName + InfoCollector.getLessThanOrEqualSign() + ResultA.toString() + " OR " + VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultB.toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    /*  |ax + b| <= c
     *  -c <= ax + b <= c
     *  (-c - b)/a <= x <= (c - b)/a
     */
    private void createProblem_07(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        Fraction ResultA = new Fraction(0 - c - b, a);
        Fraction ResultB = new Fraction((b - c), a);

        QUESTION = "|" + String.valueOf(a) + VarName + " + " + String.valueOf(b) + "| " + InfoCollector.getLessThanOrEqualSign() + String.valueOf(c);
        RIGHT_ANSWER = ResultA.toString() + InfoCollector.getLessThanOrEqualSign() + VarName + InfoCollector.getLessThanOrEqualSign() + ResultB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = ResultB.toString() + InfoCollector.getLessThanOrEqualSign() + VarName + InfoCollector.getLessThanOrEqualSign() + ResultA.toString();
        ANSWER_C = FractionMath.getReciprocal(ResultA).toString() + InfoCollector.getLessThanOrEqualSign() + VarName + InfoCollector.getLessThanOrEqualSign() + FractionMath.getReciprocal(ResultA).toString();
        ANSWER_D = ResultA.toString() + InfoCollector.getGreaterThanOrEqualSign() + VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultB.toString();
    }

    /*  |ax + b| <= c
     *  -c <= ax + b <= c
     *  (-c - b)/a >= x >= (c - b)/a
     */
    private void createProblem_08(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomNegativeNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        Fraction ResultA = new Fraction(0 - c - b, a);
        Fraction ResultB = new Fraction((b - c), a);

        QUESTION = "|" + String.valueOf(a) + VarName + " + " + String.valueOf(b) + "| " + InfoCollector.getLessThanOrEqualSign() + String.valueOf(c);
        RIGHT_ANSWER = ResultA.toString() + InfoCollector.getGreaterThanOrEqualSign() + VarName + InfoCollector.getGreaterThanOrEqualSign() + ResultB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = ResultB.toString() + InfoCollector.getLessThanOrEqualSign() + VarName + InfoCollector.getLessThanOrEqualSign() + ResultA.toString();
        ANSWER_C = FractionMath.getReciprocal(ResultA).toString() + InfoCollector.getLessThanOrEqualSign() + VarName + InfoCollector.getLessThanOrEqualSign() + FractionMath.getReciprocal(ResultA).toString();
        ANSWER_D = ResultA.toString() + InfoCollector.getLessThanOrEqualSign() + VarName + InfoCollector.getLessThanOrEqualSign() + ResultB.toString();
    }

    @Override
    public String getQuestion() {
        QUESTION = "Solve this inequality: <br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
