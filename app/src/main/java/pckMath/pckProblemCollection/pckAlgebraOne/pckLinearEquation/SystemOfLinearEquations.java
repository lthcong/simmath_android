package pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation;

import pckInfo.InfoCollector;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/10/2017.
 */

public class SystemOfLinearEquations {

    /*
    *   ax + by = c
    *   dx + ey = f
    * */

    private String FIRST_VARIABLE, SECOND_VARIABLE, SOLUTION;
    private int A_COEFFICIENT, B_COEFFICIENT, C_COEFFICIENT,
                D_COEFFICIENT, E_COEFFICIENT, F_COEFFICIENT;
    private Fraction X_SOLUTION, Y_SOLUTION;
    
    public SystemOfLinearEquations(){
        FIRST_VARIABLE = "x";
        SECOND_VARIABLE = "y";

        A_COEFFICIENT = 0;
        B_COEFFICIENT = 0;
        C_COEFFICIENT = 0;
        D_COEFFICIENT = 0;
        E_COEFFICIENT = 0;
        F_COEFFICIENT = 0;
    }

    public SystemOfLinearEquations(String FirstVariable, String SecondVariable,
                                   int ACoefficient, int BCoefficient, int CCoefficient,
                                   int DCoefficient, int ECoefficient, int FCoefficient){
        FIRST_VARIABLE = FirstVariable;
        SECOND_VARIABLE = SecondVariable;

        A_COEFFICIENT = ACoefficient;
        B_COEFFICIENT = BCoefficient;
        C_COEFFICIENT = CCoefficient;
        D_COEFFICIENT = DCoefficient;
        E_COEFFICIENT = ECoefficient;
        F_COEFFICIENT = FCoefficient;
    }
    
    public SystemOfLinearEquations(Polynomial FirstEquation, Polynomial SecondEquation){
        FIRST_VARIABLE = "";
        SECOND_VARIABLE = "";

        // 1ST EQUATION
        int size = FirstEquation.getExpression().getNumberList().size();
        for (int i = 0; i < size; i++){
            AlgebraicNumber Temp = FirstEquation.getExpression().getNumber(i);
            if (Temp.getVariable(0).isNull()){
                C_COEFFICIENT = 0 - Temp.getCoefficient();
            }
            else {
                if (FIRST_VARIABLE.trim().length() == 0){
                    A_COEFFICIENT = Temp.getCoefficient();
                    FIRST_VARIABLE = Temp.getVariable(0).getName();
                }
                else {
                    B_COEFFICIENT = Temp.getCoefficient();
                    SECOND_VARIABLE = Temp.getVariable(0).getName();
                }
            }
        }

        // 2ND EQUATION
        size = SecondEquation.getExpression().getNumberList().size();
        for (int i = 0; i < size; i++){
            AlgebraicNumber Temp = SecondEquation.getExpression().getNumber(i);
            if (Temp.getVariable(0).isNull()){
                F_COEFFICIENT = 0 - Temp.getCoefficient();
            }
            else {
                if (Temp.getVariable(0).getName().equalsIgnoreCase(FIRST_VARIABLE)){
                    D_COEFFICIENT = Temp.getCoefficient();
                    FIRST_VARIABLE = Temp.getVariable(0).getName();
                }
                else {
                    E_COEFFICIENT = Temp.getCoefficient();
                    SECOND_VARIABLE = Temp.getVariable(0).getName();
                }
            }
        }

    }

    public void setFirstVariableName(String VarName){
        FIRST_VARIABLE = VarName;
    }

    public String getFirstVariableName(){
        return FIRST_VARIABLE;
    }

    public void setSecondVariableName(String VarName){
        SECOND_VARIABLE = VarName;
    }

    public String getSecondVariableName(){
        return SECOND_VARIABLE;
    }

    public void setACoefficient(int ACoefficient){
        A_COEFFICIENT = ACoefficient;
    }

    public int getACoefficient(){
        return A_COEFFICIENT;
    }

    public void setBCoefficient(int BCoefficient){
        B_COEFFICIENT = BCoefficient;
    }

    public int getBCoefficient(){
        return B_COEFFICIENT;
    }

    public void setCCoefficient(int CCoefficient){
        C_COEFFICIENT = CCoefficient;
    }

    public int getCCoefficient(){
        return C_COEFFICIENT;
    }

    public void setDCoefficient(int DCoefficient){
        D_COEFFICIENT = DCoefficient;
    }

    public int getDCoefficient(){
        return D_COEFFICIENT;
    }

    public void setECoefficient(int ECoefficient){
        E_COEFFICIENT = ECoefficient;
    }

    public int getECoefficient(){
        return E_COEFFICIENT;
    }

    public void setFCoefficient(int FCoefficient){
        F_COEFFICIENT = FCoefficient;
    }

    public int getFCoefficient(){
        return F_COEFFICIENT;
    }

    public String toString(){
        String EquationString;
        EquationString = String.valueOf(A_COEFFICIENT) + FIRST_VARIABLE + " + " + String.valueOf(B_COEFFICIENT) + SECOND_VARIABLE + " = " + String.valueOf(C_COEFFICIENT);
        EquationString += "<br>";
        EquationString += String.valueOf(D_COEFFICIENT) + FIRST_VARIABLE + " + " + String.valueOf(E_COEFFICIENT) + SECOND_VARIABLE + " = " + String.valueOf(F_COEFFICIENT);
        return EquationString;
    }

    public String getSolution(){
        int detD = A_COEFFICIENT * E_COEFFICIENT - B_COEFFICIENT * D_COEFFICIENT;
        int detDx = C_COEFFICIENT * E_COEFFICIENT - B_COEFFICIENT * D_COEFFICIENT;
        int detDy = A_COEFFICIENT * F_COEFFICIENT - C_COEFFICIENT * D_COEFFICIENT;

        if (detD == 0){
            if (detDx != 0 && detDy != 0){
                SOLUTION = InfoCollector.getNoSolution();
                X_SOLUTION = new Fraction(-10000);
                Y_SOLUTION = new Fraction(-10000);
            }
            else {
                SOLUTION = InfoCollector.getInfiniteManySolution();
                X_SOLUTION = new Fraction(10000);
                Y_SOLUTION = new Fraction(10000);
            }
        }
        else {
            //  X SOLUTION
            if (detDx % detD == 0){
                SOLUTION = "x = " + String.valueOf((detDx / detD));
                X_SOLUTION = new Fraction((detDx / detD));
            }
            else {
                SOLUTION = "x = " + FractionMath.getSimplifiedFraction(new Fraction(detDx, detD)).toString();
                X_SOLUTION = FractionMath.getSimplifiedFraction(new Fraction(detDx, detD));
            }

            //  Y SOLUTION
            if (detDy % detD == 0){
                SOLUTION += ", y = " + String.valueOf((detDy / detD));
                Y_SOLUTION = new Fraction((detDy / detD));
            }
            else {
                SOLUTION += ", y = " + FractionMath.getSimplifiedFraction(new Fraction(detDy, detD)).toString();
                Y_SOLUTION = FractionMath.getSimplifiedFraction(new Fraction(detDy, detD));
            }
        }

        return SOLUTION;
    }
    
    public Fraction getXSolution(){
        return X_SOLUTION;
    }
    
    public Fraction getYSolution(){
        return Y_SOLUTION;
    }
    
}
