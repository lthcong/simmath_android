package pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.pckEquation;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.LinearEquation;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 7/8/2017.
 */

public class SolvingLinearEquation extends MathProblem{

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SolvingLinearEquation(){
        createProblem();
        swapAnswer();
    }

    public SolvingLinearEquation(String Question,
                                            String RightAnswer,
                                            String AnswerA,
                                            String AnswerB,
                                            String AnswerC,
                                            String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 11;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SolvingLinearEquation(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  ax + b = c
    private void createProblem_01(){
        String VarName = MathUtility.getVariable();
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        LinearEquation _Equation = new LinearEquation(VarName, a, b, c);

        QUESTION = _Equation.toString();
        RIGHT_ANSWER = _Equation.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1)).getSolution();
        ANSWER_C = new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1)).getSolution();
        ANSWER_D = new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1)).getSolution();
    }

    //  ax + b = cx + d
    private void createProblem_02(){
        String VarName = MathUtility.getVariable();
        Polynomial LPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial RPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        LinearEquation _Equation = new LinearEquation(AlgebraMath.sub(LPolynomial, RPolynomial));

        QUESTION = LPolynomial.toString() + " = " + RPolynomial.toString();
        RIGHT_ANSWER = _Equation.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new LinearEquation(LPolynomial).getSolution();
        ANSWER_C = new LinearEquation(RPolynomial).getSolution();
        ANSWER_D = new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1)).getSolution();
    }

    //  A(ax + b) = cx + d
    private void createProblem_03(){
        String VarName = MathUtility.getVariable();
        int _Number = MathUtility.getRandomNumber_1Digit();
        Polynomial LPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial RPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        LinearEquation _Equation = new LinearEquation(AlgebraMath.sub(AlgebraMath.mul(LPolynomial, _Number), RPolynomial));

        QUESTION = String.valueOf(_Number) + "(" + LPolynomial.toString() + ") = " + RPolynomial.toString();
        RIGHT_ANSWER = _Equation.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new LinearEquation(LPolynomial).getSolution();
        ANSWER_C = new LinearEquation(RPolynomial).getSolution();
        ANSWER_D = new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1)).getSolution();
    }

    //  ax + b + cx + d = ex + f
    private void createProblem_04(){
        String VarName = MathUtility.getVariable();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial ThirdPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        LinearEquation _Equation = new LinearEquation(AlgebraMath.sub(AlgebraMath.add(FirstPolynomial, SecondPolynomial), ThirdPolynomial));

        QUESTION = FirstPolynomial.toString() + " + " + SecondPolynomial.toString() + " = " + ThirdPolynomial.toString();
        RIGHT_ANSWER = _Equation.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new LinearEquation(FirstPolynomial).getSolution();
        ANSWER_C = new LinearEquation(SecondPolynomial).getSolution();
        ANSWER_D = new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1)).getSolution();
    }

    //  A(ax + b) + cx + d = ex + f
    private void createProblem_05(){
        String VarName = MathUtility.getVariable();
        int _Number = MathUtility.getRandomNumber_1Digit();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial ThirdPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        LinearEquation _Equation = new LinearEquation(AlgebraMath.sub(AlgebraMath.add(AlgebraMath.mul(FirstPolynomial, _Number), SecondPolynomial), ThirdPolynomial));

        QUESTION = String.valueOf(_Number) + "(" + FirstPolynomial.toString() + ") + " + SecondPolynomial.toString() + " = " + ThirdPolynomial.toString();
        RIGHT_ANSWER = _Equation.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new LinearEquation(FirstPolynomial).getSolution();
        ANSWER_C = new LinearEquation(SecondPolynomial).getSolution();
        ANSWER_D = new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1)).getSolution();
    }

    //  ax + b + cx + d = ex + f + gx + h
    private void createProblem_06(){
        String VarName = MathUtility.getVariable();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial ThirdPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial ForthPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        LinearEquation _Equation = new LinearEquation(AlgebraMath.sub(AlgebraMath.add(FirstPolynomial, SecondPolynomial), AlgebraMath.add(ThirdPolynomial, ForthPolynomial)));

        QUESTION = FirstPolynomial.toString() + " + " + SecondPolynomial.toString() + " = " + ThirdPolynomial.toString() + " + " + ForthPolynomial.toString();
        RIGHT_ANSWER = _Equation.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new LinearEquation(FirstPolynomial).getSolution();
        ANSWER_C = new LinearEquation(SecondPolynomial).getSolution();
        ANSWER_D = new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1)).getSolution();
    }

    //  A(ax + b) + cx + d = ex + f + B(gx + h)
    private void createProblem_07(){
        String VarName = MathUtility.getVariable();
        int FirstNumber = MathUtility.getRandomNumber_1Digit();
        int SecondNumber = MathUtility.getRandomNumber_1Digit();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial ThirdPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial ForthPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        LinearEquation _Equation = new LinearEquation(AlgebraMath.sub(AlgebraMath.add(AlgebraMath.mul(FirstPolynomial, FirstNumber), SecondPolynomial), AlgebraMath.add(AlgebraMath.mul(ForthPolynomial, SecondPolynomial), ThirdPolynomial)));

        QUESTION = String.valueOf(FirstNumber) + "(" + FirstPolynomial.toString() + ") + " + SecondPolynomial.toString() + " = " + ThirdPolynomial.toString() + " + " + String.valueOf(SecondNumber) + "(" + ForthPolynomial.toString() + ")";
        RIGHT_ANSWER = _Equation.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new LinearEquation(FirstPolynomial).getSolution();
        ANSWER_C = new LinearEquation(SecondPolynomial).getSolution();
        ANSWER_D = new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1)).getSolution();
    }

    //  ax + b + cx + d = ex + f
    private void createProblem_08(){
        String VarName = MathUtility.getVariable();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial ThirdPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.sub(ThirdPolynomial, FirstPolynomial);

        LinearEquation _Equation = new LinearEquation(AlgebraMath.sub(AlgebraMath.add(FirstPolynomial, SecondPolynomial), ThirdPolynomial));

        QUESTION = FirstPolynomial.toString() + " + " + SecondPolynomial.toString() + " = " + ThirdPolynomial.toString();
        RIGHT_ANSWER = _Equation.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new LinearEquation(FirstPolynomial).getSolution();
        ANSWER_C = new LinearEquation(SecondPolynomial).getSolution();
        ANSWER_D = new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1)).getSolution();
    }

    //  A(ax + b) = cx + d
    private void createProblem_09(){
        String VarName = MathUtility.getVariable();
        int _Number = MathUtility.getRandomNumber_1Digit();
        Polynomial LPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial RPolynomial = AlgebraMath.mul(LPolynomial, MathUtility.getRandomNumber_1Digit());

        LinearEquation _Equation = new LinearEquation(AlgebraMath.sub(AlgebraMath.mul(LPolynomial, _Number), RPolynomial));

        QUESTION = String.valueOf(_Number) + "(" + LPolynomial.toString() + ") = " + RPolynomial.toString();
        RIGHT_ANSWER = _Equation.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new LinearEquation(LPolynomial).getSolution();
        ANSWER_C = new LinearEquation(RPolynomial).getSolution();
        ANSWER_D = new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1)).getSolution();
    }

    //  A / (ax + b) = B / (cx + d)
    private void createProblem_10(){
        int A = MathUtility.getRandomNumber_1Digit();
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        String FirstExclude = (new LinearEquation(PolA)).getSolution();

        int B = MathUtility.getRandomNumber_1Digit();
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        String SecondExclude = (new LinearEquation(PolB)).getSolution();

        LinearEquation Equation = new LinearEquation(AlgebraMath.sub(AlgebraMath.mul(PolB, A), AlgebraMath.mul(PolA, B)));

        QUESTION = InfoCollector.getSup(String.valueOf(A)) + "/" + InfoCollector.getSub(PolA.toString())
                + " = "
                + InfoCollector.getSup(String.valueOf(B)) + "/" + InfoCollector.getSub(PolB.toString());
        if (FirstExclude.trim().equalsIgnoreCase(Equation.getSolution().trim())
                || SecondExclude.trim().equalsIgnoreCase(Equation.getSolution().trim())){
            RIGHT_ANSWER = InfoCollector.getNoSolution();

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = InfoCollector.getInfiniteManySolution();
            ANSWER_C = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1))).getSolution();
            ANSWER_D = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1))).getSolution();
        }
        else {
            RIGHT_ANSWER = Equation.getSolution();

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1))).getSolution();
            ANSWER_C = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1))).getSolution();
            ANSWER_D = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1))).getSolution();
        }
    }

    // (ax + b) / A = (cx + d) / B
    private void createProblem_11(){
        int A = MathUtility.getRandomNumber_1Digit();
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        int B = MathUtility.getRandomNumber_1Digit();
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);

        LinearEquation Equation = new LinearEquation(AlgebraMath.sub(AlgebraMath.mul(PolB, A), AlgebraMath.mul(PolA, B)));

        QUESTION = InfoCollector.getSup(PolA.toString()) + "/" + InfoCollector.getSub(String.valueOf(A))
                + " = "
                + InfoCollector.getSup(PolB.toString()) + "/" + InfoCollector.getSub(String.valueOf(B));
        RIGHT_ANSWER = Equation.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1))).getSolution();
        ANSWER_C = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1))).getSolution();
        ANSWER_D = (new LinearEquation(AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1))).getSolution();
    }

    @Override
    public String getQuestion() {
        QUESTION = "Solve this equation: <br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
