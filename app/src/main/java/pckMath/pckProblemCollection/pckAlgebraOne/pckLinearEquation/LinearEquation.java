package pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation;

import pckInfo.InfoCollector;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/8/2017.
 */

public class LinearEquation {
    
    private int A_COEFFICIENT, B_COEFFICIENT, C_COEFFICIENT;
    private String VARIABLE, SOLUTION;
    
    public LinearEquation(){
        VARIABLE = "x";

        A_COEFFICIENT = 0;
        B_COEFFICIENT = 0;
        C_COEFFICIENT = 0;
    }

    public LinearEquation(String VarName, int ACoefficient, int BCoefficient, int CCoefficient){
        VARIABLE = VarName;

        A_COEFFICIENT = ACoefficient;
        B_COEFFICIENT = BCoefficient;
        C_COEFFICIENT = CCoefficient;
    }

    public LinearEquation(Polynomial _Polynomial){

        if (_Polynomial.getExpression().getNumber(0).getVariable(0).isNull()){
            VARIABLE = _Polynomial.getExpression().getNumber(1).getVariable(0).getName();

            A_COEFFICIENT = _Polynomial.getExpression().getNumber(1).getCoefficient();
            B_COEFFICIENT = _Polynomial.getExpression().getNumber(0).getCoefficient();
            C_COEFFICIENT = 0;
        }
        else {
            VARIABLE = _Polynomial.getExpression().getNumber(0).getVariable(0).getName();

            A_COEFFICIENT = _Polynomial.getExpression().getNumber(0).getCoefficient();
            B_COEFFICIENT = _Polynomial.getExpression().getNumber(1).getCoefficient();
            C_COEFFICIENT = 0;
        }

    }

    public void setVariableName(String VarName){
        VARIABLE = VarName;
    }

    public String getVariableName(){
        return VARIABLE;
    }

    public void setACoefficient(int ACoefficient){
        A_COEFFICIENT = ACoefficient;
    }
    
    public int getACoefficient(){
        return A_COEFFICIENT;
    }

    public void setBCoefficient(int BCoefficient){
        B_COEFFICIENT = BCoefficient;
    }

    public int getBCoefficient(){
        return B_COEFFICIENT;
    }

    public void setCCoefficient(int CCoefficient){
        C_COEFFICIENT = CCoefficient;
    }

    public int getCCoefficient(){
        return C_COEFFICIENT;
    }

    public String toString(){
        return String.valueOf(A_COEFFICIENT) + VARIABLE + " + " + String.valueOf(B_COEFFICIENT) + " = " + String.valueOf(C_COEFFICIENT);
    }

    public String getSolution(){
        if (A_COEFFICIENT != 0){
            if ((C_COEFFICIENT - B_COEFFICIENT) % A_COEFFICIENT == 0){
                SOLUTION = VARIABLE + " = " + String.valueOf((C_COEFFICIENT - B_COEFFICIENT) / A_COEFFICIENT);
            }
            else {
                SOLUTION = VARIABLE + " = " + FractionMath.getSimplifiedFraction(new Fraction((C_COEFFICIENT - B_COEFFICIENT), A_COEFFICIENT)).toString();
            }
        }
        else {
            if (C_COEFFICIENT - B_COEFFICIENT == 0){
                SOLUTION = InfoCollector.getInfiniteManySolution();
            }
            else {
                SOLUTION = InfoCollector.getNoSolution();
            }
        }

        return SOLUTION;
    }

    public String getSolutionNumber(){
        String SolutionNumber;
        if (A_COEFFICIENT != 0){
            if ((C_COEFFICIENT - B_COEFFICIENT) % A_COEFFICIENT == 0){
                SolutionNumber = String.valueOf((C_COEFFICIENT - B_COEFFICIENT) / A_COEFFICIENT);
            }
            else {
                SolutionNumber = FractionMath.getSimplifiedFraction(new Fraction((C_COEFFICIENT - B_COEFFICIENT), A_COEFFICIENT)).toString();
            }
        }
        else {
            if (C_COEFFICIENT - B_COEFFICIENT == 0){
                SolutionNumber = InfoCollector.getInfiniteManySolution();
            }
            else {
                SolutionNumber = InfoCollector.getNoSolution();
            }
        }

        return SolutionNumber;
    }
}
