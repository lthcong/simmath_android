package pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.pckInequality;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.LinearInequality;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/12/2017.
 */

public class SolvingLinearInequality extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;
    private static final String GREATER_THAN_OR_EQUAL_SIGN = "\u2265";
    private static final String LESS_THAN_OR_EQUAL_SIGN = "\u2264";

    public SolvingLinearInequality(){
        createProblem();
        swapAnswer();
    }

    public SolvingLinearInequality(String Question,
                                 String RightAnswer,
                                 String AnswerA,
                                 String AnswerB,
                                 String AnswerC,
                                 String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 7;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SolvingLinearInequality(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private String getRandomInequalitySign(){
        String InequalitySign = ">";
        switch (MathUtility.getRandomPositiveNumber_1Digit() % 4){
            case 0:
                InequalitySign = ">";
                break;
            case 1:
                InequalitySign = "<";
                break;
            case 2:
                InequalitySign = GREATER_THAN_OR_EQUAL_SIGN;
                break;
            case 3:
                InequalitySign = LESS_THAN_OR_EQUAL_SIGN;
                break;
        }

        return " " + InequalitySign + " ";
    }

    private String switchInequalitySign(String OriginalSign){
        String SwitchedSign;
        switch (OriginalSign){
            case "<":
                SwitchedSign = ">";
                break;
            case ">":
                SwitchedSign = "<";
                break;
            case GREATER_THAN_OR_EQUAL_SIGN:
                SwitchedSign = InfoCollector.getLessThanOrEqualSign();
                break;
            case LESS_THAN_OR_EQUAL_SIGN:
                SwitchedSign = InfoCollector.getGreaterThanOrEqualSign();
                break;
            default:
                SwitchedSign = ">";
                break;
        }

        return SwitchedSign;
    }

    //  ax + b >
    private void createProblem_01(){
        String VarName = MathUtility.getVariable();
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        LinearInequality Inequality = new LinearInequality(VarName, getRandomInequalitySign(), a, b, c);

        QUESTION = Inequality.toString();
        RIGHT_ANSWER = Inequality.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = VarName + getRandomInequalitySign() + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_C = VarName + getRandomInequalitySign() + FractionMath.getRandomFraction().toString();
        ANSWER_D = VarName + getRandomInequalitySign() + MathUtility.getRandomNegativeNumber_1Digit();
    }

    //  ax + b > cx + d
    private void createProblem_02(){
        String VarName = MathUtility.getVariable();
        String InequalitySign = getRandomInequalitySign();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        LinearInequality Inequality = new LinearInequality(InequalitySign,
                AlgebraMath.sub(FirstPolynomial, SecondPolynomial));

        QUESTION = FirstPolynomial.toString() + InequalitySign + SecondPolynomial.toString();
        RIGHT_ANSWER = Inequality.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = VarName + getRandomInequalitySign() + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_C = VarName + getRandomInequalitySign() + FractionMath.getRandomFraction().toString();
        ANSWER_D = VarName + getRandomInequalitySign() + MathUtility.getRandomNegativeNumber_1Digit();
    }

    //  A(ax + b) > cx + d
    private void createProblem_03(){
        String VarName = MathUtility.getVariable();
        String InequalitySign = getRandomInequalitySign();
        int Number = MathUtility.getRandomNumber_1Digit();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        LinearInequality Inequality = new LinearInequality(InequalitySign,
                AlgebraMath.sub(AlgebraMath.mul(FirstPolynomial, Number), SecondPolynomial));

        QUESTION = String.valueOf(Number) + "(" + FirstPolynomial.toString() + ")" + InequalitySign + SecondPolynomial.toString();
        RIGHT_ANSWER = Inequality.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = VarName + getRandomInequalitySign() + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_C = VarName + getRandomInequalitySign() + FractionMath.getRandomFraction().toString();
        ANSWER_D = VarName + getRandomInequalitySign() + MathUtility.getRandomNegativeNumber_1Digit();
    }

    //  ax + b + cx + d > ex + f
    private void createProblem_04(){
        String VarName = MathUtility.getVariable();
        String InequalitySign = getRandomInequalitySign();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial ThirdPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        LinearInequality Inequality = new LinearInequality(InequalitySign,
                AlgebraMath.sub(AlgebraMath.add(FirstPolynomial, SecondPolynomial), ThirdPolynomial));

        QUESTION = FirstPolynomial.toString() + " + " + SecondPolynomial + InequalitySign + ThirdPolynomial.toString();
        RIGHT_ANSWER = Inequality.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = VarName + getRandomInequalitySign() + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_C = VarName + getRandomInequalitySign() + FractionMath.getRandomFraction().toString();
        ANSWER_D = VarName + getRandomInequalitySign() + MathUtility.getRandomNegativeNumber_1Digit();
    }

    //  A(ax + b) + cx + d > ex + f
    private void createProblem_05(){
        String VarName = MathUtility.getVariable();
        String InequalitySign = getRandomInequalitySign();
        int Number = MathUtility.getRandomNumber_1Digit();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial ThirdPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        LinearInequality Inequality = new LinearInequality(InequalitySign,
                AlgebraMath.sub(AlgebraMath.add(AlgebraMath.mul(FirstPolynomial, Number), SecondPolynomial), ThirdPolynomial));

        QUESTION = String.valueOf(Number) + "(" + FirstPolynomial.toString() + ") + " + SecondPolynomial.toString() + InequalitySign + ThirdPolynomial.toString();
        RIGHT_ANSWER = Inequality.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = VarName + getRandomInequalitySign() + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_C = VarName + getRandomInequalitySign() + FractionMath.getRandomFraction().toString();
        ANSWER_D = VarName + getRandomInequalitySign() + MathUtility.getRandomNegativeNumber_1Digit();
    }

    //  ax + b + cx + d < ex + f + gx + h
    private void createProblem_06(){
        String VarName = MathUtility.getVariable();
        String InequalitySign = getRandomInequalitySign();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial ThirdPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial ForthPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        LinearInequality Inequality = new LinearInequality(InequalitySign,
                AlgebraMath.sub(AlgebraMath.add(FirstPolynomial, SecondPolynomial), AlgebraMath.add(ThirdPolynomial, ForthPolynomial)));

        QUESTION = FirstPolynomial.toString() + " + " + SecondPolynomial + InequalitySign + ThirdPolynomial.toString() + " + " + ForthPolynomial.toString();
        RIGHT_ANSWER = Inequality.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = VarName + getRandomInequalitySign() + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_C = VarName + getRandomInequalitySign() + FractionMath.getRandomFraction().toString();
        ANSWER_D = VarName + getRandomInequalitySign() + MathUtility.getRandomNegativeNumber_1Digit();
    }

    //  A(ax + b) + cx + d > ex + f + B(gx + h)
    private void createProblem_07(){
        String VarName = MathUtility.getVariable();
        String InequalitySign = getRandomInequalitySign();
        int NumberA = MathUtility.getRandomNumber_1Digit();
        int NumberB = MathUtility.getRandomNumber_1Digit();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial ThirdPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial ForthPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        LinearInequality Inequality = new LinearInequality(InequalitySign,
                AlgebraMath.sub(AlgebraMath.add(AlgebraMath.mul(FirstPolynomial, NumberA), SecondPolynomial),
                        AlgebraMath.add(ThirdPolynomial, AlgebraMath.mul(ForthPolynomial, NumberB))));

        QUESTION = String.valueOf(NumberA) + "(" + FirstPolynomial.toString() + ") + " + SecondPolynomial.toString()
                + InequalitySign
                + ThirdPolynomial.toString() + " + " + String.valueOf(NumberB) + "(" + ForthPolynomial.toString() + ")";
        RIGHT_ANSWER = Inequality.getSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = VarName + getRandomInequalitySign() + MathUtility.getRandomPositiveNumber_1Digit();
        ANSWER_C = VarName + getRandomInequalitySign() + FractionMath.getRandomFraction().toString();
        ANSWER_D = VarName + getRandomInequalitySign() + MathUtility.getRandomNegativeNumber_1Digit();
    }

    @Override
    public String getQuestion() {
        QUESTION = "Solve this inequality: <br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
