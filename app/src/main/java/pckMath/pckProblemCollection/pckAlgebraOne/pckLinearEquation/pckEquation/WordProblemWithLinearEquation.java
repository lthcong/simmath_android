package pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.pckEquation;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 7/10/2017.
 */

public class WordProblemWithLinearEquation extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public WordProblemWithLinearEquation(){
        createProblem();
        swapAnswer();
    }

    public WordProblemWithLinearEquation(String Question,
                                 String RightAnswer,
                                 String AnswerA,
                                 String AnswerB,
                                 String AnswerC,
                                 String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 10;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            default:
                createProblem_01();
                break;
        }

        return new WordProblemWithLinearEquation(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = a + 1;

        QUESTION = "Given the sum of the two consecutive numbers are " + String.valueOf((a + b)) + ". Find these numbers.";
        RIGHT_ANSWER = String.valueOf(a) + ", " + String.valueOf(b);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a - 1) + ", " + String.valueOf(b);
        ANSWER_C = String.valueOf(a) + ", " + String.valueOf(b + 1);
        ANSWER_D = String.valueOf(a - 1) + ", " + String.valueOf(b + 1);
    }

    private void createProblem_02(){
        int PhonePrice = MathUtility.getRandomPositiveNumber_2Digit() + 100;
        int TabletPrice = MathUtility.getRandomPositiveNumber_2Digit() + 100;

        QUESTION = "Given the price of the phone and the tablet is $" + String.valueOf((PhonePrice + TabletPrice))
                + ". The different between the two prices is " + String.valueOf(Math.abs(PhonePrice - TabletPrice)) + ". Find the two prices.";
        RIGHT_ANSWER = "$" + String.valueOf(PhonePrice) + ", $" + String.valueOf(TabletPrice);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "$" + String.valueOf(PhonePrice - 10) + ", $" + String.valueOf(TabletPrice);
        ANSWER_C = "$" + String.valueOf(PhonePrice) + ", $" + String.valueOf(TabletPrice + 10);
        ANSWER_D = "$" + String.valueOf(PhonePrice + 10) + ", $" + String.valueOf(TabletPrice - 10);
    }

    private void createProblem_03(){
        int length = MathUtility.getRandomPositiveNumber_2Digit();
        int width = length + MathUtility.getRandomPositiveNumber_2Digit();

        int Perimeter = 2 * (length + width);

        QUESTION = "The width of the rectangle is " + String.valueOf(width - length) + "cm longer than the length, and the perimeter is "
                + String.valueOf(Perimeter) + "cm. Find the dimension of the rectangle.";
        RIGHT_ANSWER = String.valueOf(length) + "cm, " + String.valueOf(width) + "cm.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(length - 10) + "cm, " + String.valueOf(width) + "cm.";
        ANSWER_C = String.valueOf(length) + "cm, " + String.valueOf(width + 10) + "cm.";
        ANSWER_D = String.valueOf(length + 10) + "cm, " + String.valueOf(width - 10) + "cm.";
    }

    private void createProblem_04(){
        int length = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int area = length * width;

        QUESTION = "Find the width of the rectangle. Given the length is " + String.valueOf(length) + "cm, and the area is " + String.valueOf(area) + "cm" + InfoCollector.getExponentSign("2") + ".";
        RIGHT_ANSWER = String.valueOf(width) + "cm.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(width + 1) + "cm.";
        ANSWER_C = String.valueOf(width - 1) + "cm.";
        ANSWER_D = String.valueOf(width + 2) + "cm.";
    }

    private void createProblem_05(){
        int Older = MathUtility.getRandomPositiveNumber_1Digit() + 20;
        int Younger = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        
        QUESTION = "Given the sum of the age of the  two brothers is " + String.valueOf(Older + Younger) 
                + ". The older is " + String.valueOf(Older - Younger) + " years older than the younger one. Find the age of each one.";
        RIGHT_ANSWER = String.valueOf(Older) + ", " + String.valueOf(Younger) + ".";
        
        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Older + 5) + ", " + String.valueOf(Younger) + ".";
        ANSWER_C = String.valueOf(Older) + ", " + String.valueOf(Younger + 5) + ".";
        ANSWER_D = String.valueOf(Older - 5) + ", " + String.valueOf(Younger - 5) + ".";
    }

    private void createProblem_06(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() * 2;
        int b = a + 2;
        int c = b + 2;

        QUESTION = "Given the sum of the 3 consecutive even numbers are " + String.valueOf(a + b + c) + ". Find these numbers.";
        RIGHT_ANSWER = String.valueOf(a) + ", " + String.valueOf(b) + ", " + String.valueOf(c);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a - 2) + ", " + String.valueOf(b - 2) + ", " + String.valueOf(c - 2);
        ANSWER_C = String.valueOf(a) + ", " + String.valueOf(b) + ", " + String.valueOf(c + 1);
        ANSWER_D = String.valueOf(a) + ", " + String.valueOf(b + 2) + ", " + String.valueOf(c + 2);
    }

    private void createProblem_07(){
        int PopulationA = MathUtility.getRandomPositiveNumber_4Digit() + 1000;
        int PopulationB = PopulationA + MathUtility.getRandomPositiveNumber_3Digit();

        QUESTION = "Given the population of the two cities is " + String.valueOf(PopulationA + PopulationB)
                + ". One of them has " + String.valueOf(PopulationB - PopulationA) + " people more than the other. Find the population of each of them.";
        RIGHT_ANSWER = String.valueOf(PopulationA) + ", " + String.valueOf(PopulationB);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B  = String.valueOf(PopulationA) + ", " + String.valueOf(PopulationB + 1000);
        ANSWER_C  = String.valueOf(PopulationA + 1000) + ", " + String.valueOf(PopulationB);
        ANSWER_D  = String.valueOf(PopulationA) + ", " + String.valueOf(PopulationB - 1000);
    }

    private void createProblem_08(){
        int ShorterBase = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int LongerBase = ShorterBase + MathUtility.getRandomPositiveNumber_1Digit();
        int Height = MathUtility.getRandomPositiveNumber_1Digit() + 5;

        double area = MathUtility.round_1_Decimal((double) ((ShorterBase + LongerBase) * Height) / 2.0);

        QUESTION = "Given the area of a trapezoid is " + String.valueOf(area) + "cm" + InfoCollector.getExponentSign("2")
                + ", one of the base is " + String.valueOf(LongerBase) + "cm, the height is " + String.valueOf(Height) + "cm. Find the length of the other base.";
        RIGHT_ANSWER = String.valueOf(ShorterBase) + "cm.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(ShorterBase + 1) + "cm.";
        ANSWER_C = String.valueOf(ShorterBase - 1) + "cm.";
        ANSWER_D = String.valueOf(ShorterBase + 2) + "cm.";
    }

    private void createProblem_09(){
        int Jame = MathUtility.getRandomPositiveNumber_2Digit();
        int Tom = Jame + MathUtility.getRandomPositiveNumber_1Digit() + 5;

        QUESTION = "Tom and Jame earned $" + String.valueOf(Jame + Tom) + " during the weekend. "
                + "Tom earned $" + String.valueOf(Tom - Jame) + " than Jame. How much did they earn?";
        RIGHT_ANSWER = "Jame: $" + String.valueOf(Jame) + ", Tom: $" + String.valueOf(Tom) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "Jame: $" + String.valueOf(Jame + 10) + ", Tom: $" + String.valueOf(Tom) + ".";
        ANSWER_C = "Jame: $" + String.valueOf(Jame) + ", Tom: $" + String.valueOf(Tom + 10) + ".";
        ANSWER_D = "Jame: $" + String.valueOf(Jame - 5) + ", Tom: $" + String.valueOf(Tom - 5) + ".";
    }

    private void createProblem_10(){
        int Yesterday = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int Today = Yesterday + MathUtility.getRandomPositiveNumber_1Digit() + 5;

        QUESTION = "Jack biked " + String.valueOf(Yesterday + Today) + " miles in two days. Yesterday, he biked "
                + String.valueOf(Today - Yesterday) + " miles less than today. How many miles did he bike in each day.";
        RIGHT_ANSWER = String.valueOf(Yesterday) + " miles, " + String.valueOf(Today) + " miles.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Yesterday - 10) + " miles, " + String.valueOf(Today) + " miles.";
        ANSWER_C = String.valueOf(Yesterday) + " miles, " + String.valueOf(Today + 10) + " miles.";
        ANSWER_D = String.valueOf(Yesterday - 5) + " miles, " + String.valueOf(Today - 5) + " miles.";
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
