package pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.pckEquation;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.LinearEquation;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/13/2017.
 */

public class SolvingLinearEquationWithAbsoluteValue extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SolvingLinearEquationWithAbsoluteValue(){
        createProblem();
        swapAnswer();
    }

    public SolvingLinearEquationWithAbsoluteValue(String Question,
                                 String RightAnswer,
                                 String AnswerA,
                                 String AnswerB,
                                 String AnswerC,
                                 String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 4;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SolvingLinearEquationWithAbsoluteValue(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  |ax + b| = c
    private void createProblem_01(){
        String VarName = MathUtility.getVariable();
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        LinearEquation FirstEquation = new LinearEquation(VarName, a, b, c);
        LinearEquation SecondEquation = new LinearEquation(VarName, a, b, (0 - c));

        String FirstSolution = FirstEquation.getSolution();
        String SecondSolution = SecondEquation.getSolution();

        QUESTION = "|" + String.valueOf(a) + VarName + " + " + String.valueOf(b) + "| = " + String.valueOf(c);
        if (FirstSolution.equalsIgnoreCase(SecondSolution)){
            RIGHT_ANSWER = FirstEquation.getSolution();
        }
        else {
            if ((FirstSolution.equalsIgnoreCase(InfoCollector.getNoSolution()))){
                RIGHT_ANSWER = SecondSolution;
            }
            else {
                if ((SecondSolution.equalsIgnoreCase(InfoCollector.getNoSolution()))){
                    RIGHT_ANSWER = FirstSolution;
                }
                else {
                    RIGHT_ANSWER = FirstSolution + ", " + SecondSolution;
                }
            }
        }

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = VarName + " = " + FractionMath.getRandomFraction().toString() + ", " + VarName + " = " + FractionMath.getRandomFraction().toString();
        ANSWER_C = VarName + " = " + FractionMath.getRandomFraction().toString() + ", " + VarName + " = " + FractionMath.getRandomFraction().toString();
        ANSWER_D = VarName + " = " + String.valueOf(MathUtility.getRandomDecimalNumber_1Digit()) + ", " + VarName + " = " + String.valueOf(MathUtility.getRandomDecimalNumber_1Digit());
    }

    //  |ax + b| = ax + b
    private void createProblem_02(){
        String VarName = MathUtility.getVariable();
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        LinearEquation FirstEquation = new LinearEquation(AlgebraMath.sub(FirstPolynomial, SecondPolynomial));
        LinearEquation SecondEquation = new LinearEquation(AlgebraMath.add(FirstPolynomial, SecondPolynomial));

        String FirstSolution = FirstEquation.getSolution();
        String SecondSolution = SecondEquation.getSolution();

        QUESTION = "|" + FirstPolynomial.toString() + "| = " + SecondPolynomial.toString();
        if (FirstSolution.equalsIgnoreCase(SecondSolution)){
            RIGHT_ANSWER = FirstEquation.getSolution();
        }
        else {
            if ((FirstSolution.equalsIgnoreCase(InfoCollector.getNoSolution()))){
                RIGHT_ANSWER = SecondSolution;
            }
            else {
                if ((SecondSolution.equalsIgnoreCase(InfoCollector.getNoSolution()))){
                    RIGHT_ANSWER = FirstSolution;
                }
                else {
                    RIGHT_ANSWER = FirstSolution + ", " + SecondSolution;
                }
            }
        }

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = VarName + " = " + FractionMath.getRandomFraction().toString() + ", " + VarName + " = " + FractionMath.getRandomFraction().toString();
        ANSWER_C = VarName + " = " + FractionMath.getRandomFraction().toString() + ", " + VarName + " = " + FractionMath.getRandomFraction().toString();
        ANSWER_D = VarName + " = " + String.valueOf(MathUtility.getRandomDecimalNumber_1Digit()) + ", " + VarName + " = " + String.valueOf(MathUtility.getRandomDecimalNumber_1Digit());
    }

    //  |ax + b| = A(ax + b)
    private void createProblem_03(){
        String VarName = MathUtility.getVariable();
        int Number = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        Polynomial FirstPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);
        Polynomial SecondPolynomial = AlgebraMath.getRandomPolynomialWithHighestExponent(VarName, 1);

        LinearEquation FirstEquation = new LinearEquation(AlgebraMath.sub(FirstPolynomial, AlgebraMath.mul(SecondPolynomial, Number)));
        LinearEquation SecondEquation = new LinearEquation(AlgebraMath.add(FirstPolynomial, AlgebraMath.mul(SecondPolynomial, Number)));

        String FirstSolution = FirstEquation.getSolution();
        String SecondSolution = SecondEquation.getSolution();

        QUESTION = "|" + FirstPolynomial.toString() + "| = " + String.valueOf(Number) + "(" + SecondPolynomial.toString() + ")";
        if (FirstSolution.equalsIgnoreCase(SecondSolution)){
            RIGHT_ANSWER = FirstEquation.getSolution();
        }
        else {
            if ((FirstSolution.equalsIgnoreCase(InfoCollector.getNoSolution()))){
                RIGHT_ANSWER = SecondSolution;
            }
            else {
                if ((SecondSolution.equalsIgnoreCase(InfoCollector.getNoSolution()))){
                    RIGHT_ANSWER = FirstSolution;
                }
                else {
                    RIGHT_ANSWER = FirstSolution + ", " + SecondSolution;
                }
            }
        }

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = VarName + " = " + FractionMath.getRandomFraction().toString() + ", " + VarName + " = " + FractionMath.getRandomFraction().toString();
        ANSWER_C = VarName + " = " + FractionMath.getRandomFraction().toString() + ", " + VarName + " = " + FractionMath.getRandomFraction().toString();
        ANSWER_D = VarName + " = " + String.valueOf(MathUtility.getRandomDecimalNumber_1Digit()) + ", " + VarName + " = " + String.valueOf(MathUtility.getRandomDecimalNumber_1Digit());
    }

    //  |ax + b| = -c
    private void createProblem_04(){
        String VarName = MathUtility.getVariable();
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNegativeNumber_1Digit();

        LinearEquation FirstEquation = new LinearEquation(VarName, a, b, c);
        LinearEquation SecondEquation = new LinearEquation(VarName, a, b, (0 - c));

        String FirstSolution = FirstEquation.getSolution();
        String SecondSolution = SecondEquation.getSolution();

        QUESTION = "|" + String.valueOf(a) + VarName + " + " + String.valueOf(b) + "| = " + String.valueOf(c);
        RIGHT_ANSWER = InfoCollector.getNoSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = VarName + " = " + FirstSolution + ", " + VarName + " = " + SecondSolution;
        ANSWER_C = VarName + " = " + FractionMath.getRandomFraction().toString() + ", " + VarName + " = " + FractionMath.getRandomFraction().toString();
        ANSWER_D = VarName + " = " + String.valueOf(MathUtility.getRandomDecimalNumber_1Digit()) + ", " + VarName + " = " + String.valueOf(MathUtility.getRandomDecimalNumber_1Digit());
    }

    @Override
    public String getQuestion() {
        QUESTION = "Solve this equation: <br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
