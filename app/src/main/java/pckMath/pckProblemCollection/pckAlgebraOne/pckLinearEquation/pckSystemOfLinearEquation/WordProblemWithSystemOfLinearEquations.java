package pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.pckSystemOfLinearEquation;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/11/2017.
 */

public class WordProblemWithSystemOfLinearEquations extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public WordProblemWithSystemOfLinearEquations(){
        createProblem();
        swapAnswer();
    }

    public WordProblemWithSystemOfLinearEquations(String Question,
                                         String RightAnswer,
                                         String AnswerA,
                                         String AnswerB,
                                         String AnswerC,
                                         String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 10;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            default:
                createProblem_01();
                break;
        }

        return new WordProblemWithSystemOfLinearEquations(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_2Digit();

        QUESTION = "The sum of 2 numbers is " + String.valueOf(a + b)
                + ", and the different between these numbers is " + String.valueOf(Math.abs(a - b)) + ". Find the 2 numbers.";
        RIGHT_ANSWER = String.valueOf(a) + ", " + String.valueOf(b) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a) + ", " + String.valueOf(b + 1) + ".";
        ANSWER_C = String.valueOf(a + 1) + ", " + String.valueOf(b + 1) + ".";
        ANSWER_D = String.valueOf(a - 1) + ", " + String.valueOf(b - 1) + ".";
    }

    private void createProblem_02(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();

        QUESTION = "The sum of 2 numbers is " + String.valueOf(a + b)
                + ", and the ratio between these numbers is " + FractionMath.getSimplifiedFraction(new Fraction(a, b)).toString() + ". Find the 2 numbers.";
        RIGHT_ANSWER = String.valueOf(a) + ", " + String.valueOf(b) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a) + ", " + String.valueOf(b + 1) + ".";
        ANSWER_C = String.valueOf(a + 1) + ", " + String.valueOf(b + 1) + ".";
        ANSWER_D = String.valueOf(a - 1) + ", " + String.valueOf(b - 1) + ".";
    }

    private void createProblem_03(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();

        int temp_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int temp_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int temp_3 = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = "Find the 2 numbers. Given " + String.valueOf(temp_1) + " times the first number plus the second number is " + String.valueOf((temp_1 * a + b));
        QUESTION += ", " + String.valueOf(temp_2) + " times the first number minus " + String.valueOf(temp_3) + " times the second one is " + String.valueOf((temp_2 * a - temp_3 * b)) + ".";
        RIGHT_ANSWER = String.valueOf(a) + ", " + String.valueOf(b) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a) + ", " + String.valueOf(b + 1) + ".";
        ANSWER_C = String.valueOf(a + 1) + ", " + String.valueOf(b + 1) + ".";
        ANSWER_D = String.valueOf(a - 1) + ", " + String.valueOf(b - 1) + ".";
    }

    private void createProblem_04(){
        int width = MathUtility.getRandomPositiveNumber_1Digit() + 10;
        int length = width + MathUtility.getRandomPositiveNumber_1Digit();

        int perimeter = 2 * (length + width);
        int different = length - width;

        QUESTION = "The length of a rectangle is " + String.valueOf(different) + "cm longer than the width. The perimeter is "
                + String.valueOf(perimeter) + "cm. Find the length and the width of the rectangle.";
        RIGHT_ANSWER = "Length: " + String.valueOf(length) + "cm, width: " + String.valueOf(width) + "cm.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "Length: " + String.valueOf(length) + "cm, width: " + String.valueOf(width + 5) + "cm.";
        ANSWER_C = "Length: " + String.valueOf(width) + "cm, width: " + String.valueOf(length) + "cm.";
        ANSWER_D = "Length: " + String.valueOf(length + 5) + "cm, width: " + String.valueOf(width) + "cm.";
    }

    private void createProblem_05(){
        int Jack = MathUtility.getRandomPositiveNumber_2Digit() + 100;
        int Jame = Jack + MathUtility.getRandomPositiveNumber_2Digit();

        QUESTION = "Jame and Jack earned $" + String.valueOf(Jack + Jame)
                + " in a week. Jame earned $" + String.valueOf(Jame - Jack) + " more than Jack. How much did each of them earn?";
        RIGHT_ANSWER = "Jack: $" + String.valueOf(Jack) + ", Jame: $" + String.valueOf(Jame) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "Jack: $" + String.valueOf(Jame) + ", Jame: $" + String.valueOf(Jack) + ".";
        ANSWER_C = "Jack: $" + String.valueOf(Jack + 10) + ", Jame: $" + String.valueOf(Jame) + ".";
        ANSWER_D = "Jack: $" + String.valueOf(Jack) + ", Jame: $" + String.valueOf(Jame - 10) + ".";
    }

    private void createProblem_06(){
        int PhonePrice = MathUtility.getRandomPositiveNumber_3Digit();
        int TabletPrice = PhonePrice + MathUtility.getRandomPositiveNumber_2Digit();

        QUESTION = "The phone and the tablet cost the total of  $" + String.valueOf(PhonePrice + TabletPrice)
                + ". The tablet is $" + String.valueOf(TabletPrice - PhonePrice) + " more expensive than the phone. Find the price of the phone and the tablet?";
        RIGHT_ANSWER = "Phone: $" + String.valueOf(PhonePrice) + ", tablet: $" + String.valueOf(TabletPrice) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "Phone: $" + String.valueOf(TabletPrice) + ", tablet: $" + String.valueOf(PhonePrice) + ".";
        ANSWER_C = "Phone: $" + String.valueOf(PhonePrice) + ", tablet: $" + String.valueOf(TabletPrice - 50) + ".";
        ANSWER_D = "Phone: $" + String.valueOf(PhonePrice - 50) + ", tablet: $" + String.valueOf(TabletPrice) + ".";
    }

    private void createProblem_07(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();

        int temp_1 = MathUtility.getRandomNumber_1Digit();
        int temp_2 = MathUtility.getRandomNumber_1Digit();
        int temp_3 = MathUtility.getRandomNumber_1Digit();

        QUESTION = "Find the 2 numbers. Given the first number minus " + String.valueOf(temp_1) + " times " + String.valueOf(temp_2) + " plus the second number is "
                + String.valueOf((a - temp_1) * temp_2 + b) + ". ";
        QUESTION += "The second number minus " + String.valueOf(temp_3) + " times " + String.valueOf(temp_1) + " plus the first number times " + String.valueOf(temp_2) + " is "
                + String.valueOf((b - temp_3) * temp_1 + a * temp_2) + ".";
        RIGHT_ANSWER = String.valueOf(a) + ", " + String.valueOf(b) + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a) + ", " + String.valueOf(b + 1) + ".";
        ANSWER_C = String.valueOf(a + 1) + ", " + String.valueOf(b + 1) + ".";
        ANSWER_D = String.valueOf(a - 1) + ", " + String.valueOf(b - 1) + ".";
    }

    private void createProblem_08(){
        int JameGarden = MathUtility.getRandomPositiveNumber_4Digit();
        int TomGarden = JameGarden + MathUtility.getRandomPositiveNumber_3Digit();

        QUESTION = "The total area of Jame and Tom garden is " + String.valueOf(JameGarden + TomGarden) + "ft" + InfoCollector.getExponentSign("2")
                + ". Tom's garden is " + String.valueOf(TomGarden - JameGarden) + "ft" + InfoCollector.getExponentSign("2") + " wider than Jame's one. "
                + "Find the area of each garden.";
        RIGHT_ANSWER = "Jame: " + String.valueOf(JameGarden) + "ft" + InfoCollector.getExponentSign("2")
                + ", Tom: " + String.valueOf(TomGarden) + "ft" + InfoCollector.getExponentSign("2") + ".";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "Jame: " + String.valueOf(TomGarden) + "ft" + InfoCollector.getExponentSign("2")
                + ", Tom: " + String.valueOf(JameGarden) + "ft" + InfoCollector.getExponentSign("2") + ".";
        ANSWER_C = "Jame: " + String.valueOf(JameGarden + 100) + "ft" + InfoCollector.getExponentSign("2")
                + ", Tom: " + String.valueOf(TomGarden) + "ft" + InfoCollector.getExponentSign("2") + ".";
        ANSWER_D = "Jame: " + String.valueOf(JameGarden) + "ft" + InfoCollector.getExponentSign("2")
                + ", Tom: " + String.valueOf(TomGarden + 100) + "ft" + InfoCollector.getExponentSign("2") + ".";
    }

    private void createProblem_09(){
        int TripOne = MathUtility.getRandomPositiveNumber_3Digit();
        int TripTwo = TripOne + MathUtility.getRandomPositiveNumber_2Digit();

        QUESTION = "Jack is planning to go on two trips with the total of " + String.valueOf(TripOne + TripTwo)
                + " miles. The second one is " + String.valueOf(TripTwo - TripOne) + " miles longer than the first one. "
                + "How many miles is he planning to go on each trip?";
        RIGHT_ANSWER = "First Trip: " + String.valueOf(TripOne) + " miles. Second Trip : " + String.valueOf(TripTwo) + " miles.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "First Trip: " + String.valueOf(TripTwo) + " miles. Second Trip : " + String.valueOf(TripOne) + " miles.";
        ANSWER_C = "First Trip: " + String.valueOf(TripOne) + " miles. Second Trip : " + String.valueOf(TripTwo + 100) + " miles.";
        ANSWER_D = "First Trip: " + String.valueOf(TripOne + 100) + " miles. Second Trip : " + String.valueOf(TripTwo) + " miles.";
    }

    private void createProblem_10(){
        int ShelfA = MathUtility.getRandomPositiveNumber_3Digit();
        int ShelfB = ShelfA + MathUtility.getRandomPositiveNumber_2Digit();

        QUESTION = "The school library has " + String.valueOf(ShelfA + ShelfB) + " books. Librarians want to put them in two shelves. "
                + "The second one can hold " + String.valueOf(ShelfB - ShelfA) + " books more than the first one. "
                + "Find the number of the books each shelf can hold.";
        RIGHT_ANSWER = "First shelf: " + String.valueOf(ShelfA) + " books. Second shelf: " + String.valueOf(ShelfB) + " books.";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "First shelf: " + String.valueOf(ShelfB) + " books. Second shelf: " + String.valueOf(ShelfA) + " books.";
        ANSWER_C = "First shelf: " + String.valueOf(ShelfA) + " books. Second shelf: " + String.valueOf(ShelfB + 100) + " books.";
        ANSWER_D = "First shelf: " + String.valueOf(ShelfA + 100) + " books. Second shelf: " + String.valueOf(ShelfB) + " books.";
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
