package pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.pckInequality;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.LinearInequality;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;

/**
 * Created by Cong on 7/12/2017.
 */

public class SolvingCompoundInequalities extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;
    private static final String GREATER_THAN_OR_EQUAL_SIGN = "\u2265";
    private static final String LESS_THAN_OR_EQUAL_SIGN = "\u2264";

    public SolvingCompoundInequalities(){
        createProblem();
        swapAnswer();
    }

    public SolvingCompoundInequalities(String Question,
                                       String RightAnswer,
                                       String AnswerA,
                                       String AnswerB,
                                       String AnswerC,
                                       String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 15;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            case 13:
                createProblem_14();
                break;
            case 14:
                createProblem_15();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SolvingCompoundInequalities(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  ax + b > c AND dx + e > f   -   a, d POSITIVE
    private void createProblem_01(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int d = MathUtility.getRandomPositiveNumber_1Digit();
        int e = MathUtility.getRandomNumber_1Digit();
        int f = MathUtility.getRandomNumber_1Digit();

        LinearInequality InequalityA = new LinearInequality(VarName, ">", a, b, c);
        Fraction ResultA = new Fraction((b - c), a);

        LinearInequality InequalityB = new LinearInequality(VarName, ">", d, e, f);
        Fraction ResultB = new Fraction((f - e), d);

        QUESTION = InequalityA.toString() + " AND " + InequalityB.toString();

        if (ResultA.getValue() >= ResultB.getValue()){
            RIGHT_ANSWER = VarName + " > " + ResultA.toString();

            ANSWER_B = VarName + " > " + ResultB.toString();
            ANSWER_C = VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultA.toString();
            ANSWER_D = VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
        }
        else {
            RIGHT_ANSWER = VarName + " > " + ResultB.toString();

            ANSWER_B = VarName + " > " + ResultA.toString();
            ANSWER_C = VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultB.toString();
            ANSWER_D = VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  ax + b > c AND dx + e > f   -   a NEGATIVE
    /*
    *   ax > (c - b)        dx > f - e
    *   x < (c - b) / a     x > (f - e) / d
    * */
    private void createProblem_02(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomNegativeNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int d = MathUtility.getRandomPositiveNumber_1Digit();
        int e = MathUtility.getRandomNumber_1Digit();
        int f = MathUtility.getRandomNumber_1Digit();

        LinearInequality InequalityA = new LinearInequality(VarName, ">", a, b, c);
        Fraction ResultA = new Fraction((b - c), a);

        LinearInequality InequalityB = new LinearInequality(VarName, ">", d, e, f);
        Fraction ResultB = new Fraction((f - e), d);

        QUESTION = InequalityA.toString() + " AND " + InequalityB.toString();

        if (ResultA.getValue() > ResultB.getValue()){
            RIGHT_ANSWER = ResultB.toString() + " < " + VarName + " < " + ResultA.toString();

            ANSWER_B = ResultA.toString() + " < " + VarName + " < " + ResultB.toString();
            ANSWER_C = ResultB.toString() + LESS_THAN_OR_EQUAL_SIGN + VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
            ANSWER_D = ResultB.toString() + GREATER_THAN_OR_EQUAL_SIGN + VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultA.toString();
        }
        else {
            if (ResultA.getValue() == ResultB.getValue()){
                RIGHT_ANSWER = VarName + " = " + ResultA.toString();

                ANSWER_B = ResultA.toString() + " < " + VarName + " < " + ResultB.toString();
                ANSWER_C = ResultB.toString() + LESS_THAN_OR_EQUAL_SIGN + VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
                ANSWER_D = ResultB.toString() + GREATER_THAN_OR_EQUAL_SIGN + VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultA.toString();
            }
            else {
                RIGHT_ANSWER = InfoCollector.getNoSolution();

                ANSWER_B = ResultA.toString() + " < " + VarName + " < " + ResultB.toString();
                ANSWER_C = ResultB.toString() + LESS_THAN_OR_EQUAL_SIGN + VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
                ANSWER_D = ResultB.toString() + GREATER_THAN_OR_EQUAL_SIGN + VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultA.toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  ax + b > c AND dx + e > f   -   a, d NEGATIVE
    /*
    *   ax > (c - b)        dx > f - e
    *   x < (c - b) / a     x < (f - e) / d
    * */
    private void createProblem_03(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomNegativeNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int d = MathUtility.getRandomNegativeNumber_1Digit();
        int e = MathUtility.getRandomNumber_1Digit();
        int f = MathUtility.getRandomNumber_1Digit();

        LinearInequality InequalityA = new LinearInequality(VarName, ">", a, b, c);
        Fraction ResultA = new Fraction((b - c), a);

        LinearInequality InequalityB = new LinearInequality(VarName, ">", d, e, f);
        Fraction ResultB = new Fraction((f - e), d);

        QUESTION = InequalityA.toString() + " AND " + InequalityB.toString();

        if (ResultA.getValue() <= ResultB.getValue()){
            RIGHT_ANSWER = VarName + " < " + ResultA.toString();

            ANSWER_B = VarName + " > " + ResultB.toString();
            ANSWER_C = VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultA.toString();
            ANSWER_D = VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
        }
        else {
            RIGHT_ANSWER = VarName + " < " + ResultB.toString();

            ANSWER_B = VarName + " > " + ResultA.toString();
            ANSWER_C = VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultB.toString();
            ANSWER_D = VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  ax + b >= c AND dx + e > f   -   a, d POSITIVE
    private void createProblem_04(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int d = MathUtility.getRandomPositiveNumber_1Digit();
        int e = MathUtility.getRandomNumber_1Digit();
        int f = MathUtility.getRandomNumber_1Digit();

        LinearInequality InequalityA = new LinearInequality(VarName, GREATER_THAN_OR_EQUAL_SIGN, a, b, c);
        Fraction ResultA = new Fraction((b - c), a);

        LinearInequality InequalityB = new LinearInequality(VarName, ">", d, e, f);
        Fraction ResultB = new Fraction((f - e), d);

        QUESTION = InequalityA.toString() + " AND " + InequalityB.toString();

        if (ResultA.getValue() >= ResultB.getValue()){
            RIGHT_ANSWER = VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultA.toString();

            ANSWER_B = VarName + " > " + ResultB.toString();
            ANSWER_C = VarName + " < " + ResultA.toString();
            ANSWER_D = VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
        }
        else {
            RIGHT_ANSWER = VarName + " > " + ResultB.toString();

            ANSWER_B = VarName + " > " + ResultA.toString();
            ANSWER_C = VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultB.toString();
            ANSWER_D = VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  ax + b >= c AND dx + e > f   -   a NEGATIVE
    /*
    *   ax >= (c - b)        dx > f - e
    *   x <= (c - b) / a     x > (f - e) / d
    * */
    private void createProblem_05(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomNegativeNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int d = MathUtility.getRandomPositiveNumber_1Digit();
        int e = MathUtility.getRandomNumber_1Digit();
        int f = MathUtility.getRandomNumber_1Digit();

        LinearInequality InequalityA = new LinearInequality(VarName, GREATER_THAN_OR_EQUAL_SIGN, a, b, c);
        Fraction ResultA = new Fraction((b - c), a);

        LinearInequality InequalityB = new LinearInequality(VarName, ">", d, e, f);
        Fraction ResultB = new Fraction((f - e), d);

        QUESTION = InequalityA.toString() + " AND " + InequalityB.toString();

        if (ResultA.getValue() > ResultB.getValue()){
            RIGHT_ANSWER = ResultB.toString() + " < " + VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();

            ANSWER_B = ResultA.toString() + " < " + VarName + LESS_THAN_OR_EQUAL_SIGN + ResultB.toString();
            ANSWER_C = ResultB.toString() + LESS_THAN_OR_EQUAL_SIGN + VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
            ANSWER_D = ResultB.toString() + GREATER_THAN_OR_EQUAL_SIGN + VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultA.toString();
        }
        else {
            if (ResultA.getValue() == ResultB.getValue()){
                RIGHT_ANSWER = VarName + " = " + ResultA.toString();

                ANSWER_B = ResultA.toString() + " < " + VarName + " < " + ResultB.toString();
                ANSWER_C = ResultB.toString() + LESS_THAN_OR_EQUAL_SIGN + VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
                ANSWER_D = ResultB.toString() + GREATER_THAN_OR_EQUAL_SIGN + VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultA.toString();
            }
            else {
                RIGHT_ANSWER = InfoCollector.getNoSolution();
                ANSWER_B = ResultA.toString() + " < " + VarName + " < " + ResultB.toString();
                ANSWER_C = ResultB.toString() + LESS_THAN_OR_EQUAL_SIGN + VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
                ANSWER_D = ResultB.toString() + GREATER_THAN_OR_EQUAL_SIGN + VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultA.toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  ax + b >= c AND dx + e > f   -   a, d NEGATIVE
    /*
    *   ax >= (c - b)        dx > f - e
    *   x <= (c - b) / a     x < (f - e) / d
    * */
    private void createProblem_06(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomNegativeNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int d = MathUtility.getRandomNegativeNumber_1Digit();
        int e = MathUtility.getRandomNumber_1Digit();
        int f = MathUtility.getRandomNumber_1Digit();

        LinearInequality InequalityA = new LinearInequality(VarName, GREATER_THAN_OR_EQUAL_SIGN, a, b, c);
        Fraction ResultA = new Fraction((b - c), a);

        LinearInequality InequalityB = new LinearInequality(VarName, ">", d, e, f);
        Fraction ResultB = new Fraction((f - e), d);

        QUESTION = InequalityA.toString() + " AND " + InequalityB.toString();

        if (ResultA.getValue() <= ResultB.getValue()){
            RIGHT_ANSWER = VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();

            ANSWER_B = VarName + " > " + ResultB.toString();
            ANSWER_C = VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultA.toString();
            ANSWER_D = VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
        }
        else {
            RIGHT_ANSWER = VarName + " < " + ResultB.toString();

            ANSWER_B = VarName + " > " + ResultA.toString();
            ANSWER_C = VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultB.toString();
            ANSWER_D = VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  ax + b >= c AND dx + e >= f   -   a, d POSITIVE
    private void createProblem_07(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int d = MathUtility.getRandomPositiveNumber_1Digit();
        int e = MathUtility.getRandomNumber_1Digit();
        int f = MathUtility.getRandomNumber_1Digit();

        LinearInequality InequalityA = new LinearInequality(VarName, GREATER_THAN_OR_EQUAL_SIGN, a, b, c);
        Fraction ResultA = new Fraction((b - c), a);

        LinearInequality InequalityB = new LinearInequality(VarName, GREATER_THAN_OR_EQUAL_SIGN, d, e, f);
        Fraction ResultB = new Fraction((f - e), d);

        QUESTION = InequalityA.toString() + " AND " + InequalityB.toString();

        if (ResultA.getValue() >= ResultB.getValue()){
            RIGHT_ANSWER = VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultA.toString();

            ANSWER_B = VarName + " > " + ResultB.toString();
            ANSWER_C = VarName + " < " + ResultA.toString();
            ANSWER_D = VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
        }
        else {
            RIGHT_ANSWER = VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultB.toString();

            ANSWER_B = VarName + " > " + ResultA.toString();
            ANSWER_C = VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultB.toString();
            ANSWER_D = VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  ax + b >= c AND dx + e >= f   -   a NEGATIVE
    /*
    *   ax >= (c - b)        dx >= f - e
    *   x <= (c - b) / a     x >= (f - e) / d
    * */
    private void createProblem_08(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomNegativeNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int d = MathUtility.getRandomPositiveNumber_1Digit();
        int e = MathUtility.getRandomNumber_1Digit();
        int f = MathUtility.getRandomNumber_1Digit();

        LinearInequality InequalityA = new LinearInequality(VarName, GREATER_THAN_OR_EQUAL_SIGN, a, b, c);
        Fraction ResultA = new Fraction((b - c), a);

        LinearInequality InequalityB = new LinearInequality(VarName, GREATER_THAN_OR_EQUAL_SIGN, d, e, f);
        Fraction ResultB = new Fraction((f - e), d);

        QUESTION = InequalityA.toString() + " AND " + InequalityB.toString();

        if (ResultA.getValue() > ResultB.getValue()){
            RIGHT_ANSWER = ResultB.toString() + LESS_THAN_OR_EQUAL_SIGN + VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();

            ANSWER_B = ResultA.toString() + " < " + VarName + LESS_THAN_OR_EQUAL_SIGN + ResultB.toString();
            ANSWER_C = ResultB.toString() + LESS_THAN_OR_EQUAL_SIGN + VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultA.toString();
            ANSWER_D = ResultB.toString() + GREATER_THAN_OR_EQUAL_SIGN + VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultA.toString();
        }
        else {
            if (ResultA.getValue() == ResultB.getValue()){
                RIGHT_ANSWER = VarName + " = " + ResultA.toString();

                ANSWER_B = ResultA.toString() + " < " + VarName + " < " + ResultB.toString();
                ANSWER_C = ResultB.toString() + LESS_THAN_OR_EQUAL_SIGN + VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
                ANSWER_D = ResultB.toString() + GREATER_THAN_OR_EQUAL_SIGN + VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultA.toString();
            }
            else {
                RIGHT_ANSWER = InfoCollector.getNoSolution();
                ANSWER_B = ResultA.toString() + " < " + VarName + " < " + ResultB.toString();
                ANSWER_C = ResultB.toString() + LESS_THAN_OR_EQUAL_SIGN + VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
                ANSWER_D = ResultB.toString() + GREATER_THAN_OR_EQUAL_SIGN + VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultA.toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  ax + b >= c AND dx + e >= f   -   a, d NEGATIVE
    /*
    *   ax >= (c - b)        dx >= f - e
    *   x <= (c - b) / a     x <= (f - e) / d
    * */
    private void createProblem_09(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomNegativeNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int d = MathUtility.getRandomNegativeNumber_1Digit();
        int e = MathUtility.getRandomNumber_1Digit();
        int f = MathUtility.getRandomNumber_1Digit();

        LinearInequality InequalityA = new LinearInequality(VarName, GREATER_THAN_OR_EQUAL_SIGN, a, b, c);
        Fraction ResultA = new Fraction((b - c), a);

        LinearInequality InequalityB = new LinearInequality(VarName, GREATER_THAN_OR_EQUAL_SIGN, d, e, f);
        Fraction ResultB = new Fraction((f - e), d);

        QUESTION = InequalityA.toString() + " AND " + InequalityB.toString();

        if (ResultA.getValue() <= ResultB.getValue()){
            RIGHT_ANSWER = VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();

            ANSWER_B = VarName + " > " + ResultB.toString();
            ANSWER_C = VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultA.toString();
            ANSWER_D = VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
        }
        else {
            RIGHT_ANSWER = VarName + LESS_THAN_OR_EQUAL_SIGN + ResultB.toString();

            ANSWER_B = VarName + " > " + ResultA.toString();
            ANSWER_C = VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultB.toString();
            ANSWER_D = VarName + LESS_THAN_OR_EQUAL_SIGN + ResultA.toString();
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    /*  ax + b > c  OR  dx + e < f          - a, d POSITIVE
    *   x > (c - b)/a   x < (f - e)/d
    */
    private void createProblem_10(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int d = MathUtility.getRandomPositiveNumber_1Digit();
        int e = MathUtility.getRandomNumber_1Digit();
        int f = MathUtility.getRandomNumber_1Digit();

        LinearInequality InequalityA = new LinearInequality(VarName, ">", a, b, c);
        Fraction ResultA = new Fraction((b - c), a);

        LinearInequality InequalityB = new LinearInequality(VarName, "<", d, e, f);
        Fraction ResultB = new Fraction((f - e), d);

        QUESTION = InequalityA.toString() + " OR " + InequalityB.toString();

        if (ResultA.getValue() > ResultB.getValue()){
            RIGHT_ANSWER = VarName + " < " + ResultB.toString() + " OR " + VarName + " > " + ResultA.toString();

            ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
            ANSWER_C = VarName + " > " + ResultA.toString() + " OR " + VarName + " < " + ResultB.toString();
            ANSWER_D = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
        }
        else {
            if (ResultA.getValue() == ResultB.getValue()){
                RIGHT_ANSWER = VarName + " = " + ResultA.toString();

                ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
                ANSWER_C = VarName + " > " + ResultA.toString() + " OR " + VarName + " < " + ResultB.toString();
                ANSWER_D = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
            }
            else {
                RIGHT_ANSWER = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();

                ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
                ANSWER_C = VarName + " > " + ResultA.toString() + " OR " + VarName + " < " + ResultB.toString();
                ANSWER_D = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    /*  ax + b > c  OR  dx + e < f          - a NEGATIVE
    *   x < (c - b)/a   x < (f - e)/d
    */
    private void createProblem_11(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomNegativeNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int d = MathUtility.getRandomPositiveNumber_1Digit();
        int e = MathUtility.getRandomNumber_1Digit();
        int f = MathUtility.getRandomNumber_1Digit();

        LinearInequality InequalityA = new LinearInequality(VarName, "<", a, b, c);
        Fraction ResultA = new Fraction((b - c), a);

        LinearInequality InequalityB = new LinearInequality(VarName, "<", d, e, f);
        Fraction ResultB = new Fraction((f - e), d);

        QUESTION = InequalityA.toString() + " OR " + InequalityB.toString();

        if (ResultA.getValue() > ResultB.getValue()){
            RIGHT_ANSWER = ResultB.toString() + " < " + VarName + " < " + ResultA.toString();

            ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
            ANSWER_C = VarName + " > " + ResultA.toString() + " OR " + VarName + " < " + ResultB.toString();
            ANSWER_D = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
        }
        else {
            if (ResultA.getValue() == ResultB.getValue()){
                RIGHT_ANSWER = VarName + " = " + ResultA.toString();

                ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
                ANSWER_C = VarName + " > " + ResultA.toString() + " OR " + VarName + " < " + ResultB.toString();
                ANSWER_D = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
            }
            else {
                RIGHT_ANSWER = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();

                ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
                ANSWER_C = VarName + " > " + ResultA.toString() + " OR " + VarName + " < " + ResultB.toString();
                ANSWER_D = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    /*  ax + b > c  OR  dx + e < f          - a, d NEGATIVE
    *   x < (c - b)/a   x > (f - e)/d
    */
    private void createProblem_12(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomNegativeNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int d = MathUtility.getRandomNegativeNumber_1Digit();
        int e = MathUtility.getRandomNumber_1Digit();
        int f = MathUtility.getRandomNumber_1Digit();

        LinearInequality InequalityA = new LinearInequality(VarName, ">", a, b, c);
        Fraction ResultA = new Fraction((b - c), a);

        LinearInequality InequalityB = new LinearInequality(VarName, "<", d, e, f);
        Fraction ResultB = new Fraction((f - e), d);

        QUESTION = InequalityA.toString() + " OR " + InequalityB.toString();

        if (ResultA.getValue() < ResultB.getValue()){
            RIGHT_ANSWER = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();

            ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
            ANSWER_C = VarName + " > " + ResultA.toString() + " OR " + VarName + " < " + ResultB.toString();
            ANSWER_D = VarName + " = " + ResultA.toString();
        }
        else {
            if (ResultA.getValue() == ResultB.getValue()){
                RIGHT_ANSWER = VarName + " = " + ResultA.toString();

                ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
                ANSWER_C = VarName + " > " + ResultA.toString() + " OR " + VarName + " < " + ResultB.toString();
                ANSWER_D = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
            }
            else {
                RIGHT_ANSWER = VarName + " < " + ResultB.toString() + " OR " + VarName + " > " + ResultA.toString();

                ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
                ANSWER_C = VarName + " > " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
                ANSWER_D = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    /*  ax + b > c  OR  dx + e <= f          - a, d POSITIVE
    *   x > (c - b)/a   x <= (f - e)/d
    */
    private void createProblem_13(){
        String VarName = MathUtility.getVariable();

        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();

        int d = MathUtility.getRandomPositiveNumber_1Digit();
        int e = MathUtility.getRandomNumber_1Digit();
        int f = MathUtility.getRandomNumber_1Digit();

        LinearInequality InequalityA = new LinearInequality(VarName, ">", a, b, c);
        Fraction ResultA = new Fraction((b - c), a);

        LinearInequality InequalityB = new LinearInequality(VarName, LESS_THAN_OR_EQUAL_SIGN, d, e, f);
        Fraction ResultB = new Fraction((f - e), d);

        QUESTION = InequalityA.toString() + " OR " + InequalityB.toString();

        if (ResultA.getValue() > ResultB.getValue()){
            RIGHT_ANSWER = VarName + LESS_THAN_OR_EQUAL_SIGN + ResultB.toString() + " OR " + VarName + " > " + ResultA.toString();

            ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
            ANSWER_C = VarName + " > " + ResultA.toString() + " OR " + VarName + LESS_THAN_OR_EQUAL_SIGN + ResultB.toString();
            ANSWER_D = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
        }
        else {
            if (ResultA.getValue() == ResultB.getValue()){
                RIGHT_ANSWER = VarName + " = " + ResultA.toString();

                ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
                ANSWER_C = VarName + " > " + ResultA.toString() + " OR " + VarName + " < " + ResultB.toString();
                ANSWER_D = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
            }
            else {
                RIGHT_ANSWER = VarName + " < " + ResultA.toString() + " OR " + VarName + GREATER_THAN_OR_EQUAL_SIGN + ResultB.toString();

                ANSWER_B = VarName + " > " + ResultB.toString() + " OR " + VarName + " < " + ResultA.toString();
                ANSWER_C = VarName + " > " + ResultA.toString() + " OR " + VarName + " < " + ResultB.toString();
                ANSWER_D = VarName + " < " + ResultA.toString() + " OR " + VarName + " > " + ResultB.toString();
            }
        }

        ANSWER_A = RIGHT_ANSWER;
    }

    //  a < bx + c <= d
    private void createProblem_14(){
        String VarName = MathUtility.getVariable();
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        LinearInequality Inequality = new LinearInequality(VarName, LESS_THAN_OR_EQUAL_SIGN, b, c, d);
        Fraction ResultA = new Fraction((a - c), b);
        Fraction ResultB = new Fraction((d - c), b);

        QUESTION = String.valueOf(a) + " < " + Inequality.toString();
        RIGHT_ANSWER = ResultA.toString() + " < " + VarName + LESS_THAN_OR_EQUAL_SIGN + ResultB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = VarName + LESS_THAN_OR_EQUAL_SIGN + ResultB.toString();
        ANSWER_C = ResultA.toString() + " < " + VarName;
        ANSWER_D = InfoCollector.getNoSolution();
    }

    //  a < bx + c < d
    private void createProblem_15(){
        String VarName = MathUtility.getVariable();
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        LinearInequality Inequality = new LinearInequality(VarName, "<", b, c, d);
        Fraction ResultA = new Fraction((a - c), b);
        Fraction ResultB = new Fraction((d - c), b);

        QUESTION = String.valueOf(a) + " < " + Inequality.toString();
        RIGHT_ANSWER = ResultA.toString() + " < " + VarName + " < " + ResultB.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = VarName + " < " + ResultB.toString();
        ANSWER_C = ResultA.toString() + " < " + VarName;
        ANSWER_D = InfoCollector.getNoSolution();
    }

    @Override
    public String getQuestion() {
        QUESTION = "Solve this inequality: " + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }
}
