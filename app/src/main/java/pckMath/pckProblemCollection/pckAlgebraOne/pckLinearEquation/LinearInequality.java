package pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation;

import pckInfo.InfoCollector;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.Inequality;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/12/2017.
 */

public class LinearInequality {

    private int A_COEFFICIENT, B_COEFFICIENT, C_COEFFICIENT;
    private String VARIABLE, INEQUALITY_SIGN, SOLUTION, SOLUTION_NUMBER;
    private Inequality INEQUALITY_SOLUTION;
    private static final String GREATER_THAN_OR_EQUAL_SIGN = "\u2265";
    private static final String LESS_THAN_OR_EQUAL_SIGN = "\u2264";

    public LinearInequality(){
        VARIABLE = "x";
        INEQUALITY_SIGN = ">";

        A_COEFFICIENT = 0;
        B_COEFFICIENT = 0;
        C_COEFFICIENT = 0;
    }

    public LinearInequality(String VarName, String InequalitySign, int ACoefficient, int BCoefficient, int CCoefficient){
        VARIABLE = VarName;
        INEQUALITY_SIGN = " " + InequalitySign + " ";

        A_COEFFICIENT = ACoefficient;
        B_COEFFICIENT = BCoefficient;
        C_COEFFICIENT = CCoefficient;
    }

    public LinearInequality(String InequalitySign, Polynomial _Polynomial){
        INEQUALITY_SIGN = " " + InequalitySign + " ";

        if (_Polynomial.getExpression().getNumber(0).getVariable(0).isNull()){
            VARIABLE = _Polynomial.getExpression().getNumber(1).getVariable(0).getName();

            A_COEFFICIENT = _Polynomial.getExpression().getNumber(1).getCoefficient();
            B_COEFFICIENT = _Polynomial.getExpression().getNumber(0).getCoefficient();
            C_COEFFICIENT = 0;
        }
        else {
            VARIABLE = _Polynomial.getExpression().getNumber(0).getVariable(0).getName();

            A_COEFFICIENT = _Polynomial.getExpression().getNumber(0).getCoefficient();
            B_COEFFICIENT = _Polynomial.getExpression().getNumber(1).getCoefficient();
            C_COEFFICIENT = 0;
        }

    }

    public void setVariableName(String VarName){
        VARIABLE = VarName;
    }

    public String getVariableName(){
        return VARIABLE;
    }

    public void setInequalitySign(String InequalitySign){
        INEQUALITY_SIGN = " " + InequalitySign + " ";
    }

    public String getInequalitySign(){
        return INEQUALITY_SIGN;
    }

    public void setACoefficient(int ACoefficient){
        A_COEFFICIENT = ACoefficient;
    }

    public int getACoefficient(){
        return A_COEFFICIENT;
    }

    public void setBCoefficient(int BCoefficient){
        B_COEFFICIENT = BCoefficient;
    }

    public int getBCoefficient(){
        return B_COEFFICIENT;
    }

    public void setCCoefficient(int CCoefficient){
        C_COEFFICIENT = CCoefficient;
    }

    public int getCCoefficient(){
        return C_COEFFICIENT;
    }

    public String toString(){
        return String.valueOf(A_COEFFICIENT) + VARIABLE + " + " + String.valueOf(B_COEFFICIENT) + INEQUALITY_SIGN + String.valueOf(C_COEFFICIENT);
    }

    private void switchInequalitySign(){
        switch (INEQUALITY_SIGN.trim()){
            case ">":
                INEQUALITY_SIGN = "<";
                break;
            case "<":
                INEQUALITY_SIGN = ">";
                break;
            case GREATER_THAN_OR_EQUAL_SIGN:
                INEQUALITY_SIGN = LESS_THAN_OR_EQUAL_SIGN;
                break;
            case LESS_THAN_OR_EQUAL_SIGN:
                INEQUALITY_SIGN = GREATER_THAN_OR_EQUAL_SIGN;
                break;
        }
    }

    public String getSolution(){
        if (A_COEFFICIENT != 0){
            //  SWITCH INEQUALITY SIGN
            if (A_COEFFICIENT < 0){
                switchInequalitySign();
            }

            //  SOLUTION
            if ((C_COEFFICIENT - B_COEFFICIENT) % A_COEFFICIENT == 0){
                SOLUTION_NUMBER = String.valueOf((C_COEFFICIENT - B_COEFFICIENT) / A_COEFFICIENT);

                SOLUTION = VARIABLE + INEQUALITY_SIGN + SOLUTION_NUMBER;
            }
            else {
                SOLUTION_NUMBER = FractionMath.getSimplifiedFraction(new Fraction((C_COEFFICIENT - B_COEFFICIENT), A_COEFFICIENT)).toString();
                SOLUTION = VARIABLE + INEQUALITY_SIGN + SOLUTION_NUMBER;
            }
        }
        else {
            if (C_COEFFICIENT - B_COEFFICIENT == 0){
                SOLUTION = InfoCollector.getInfiniteManySolution();
            }
            else {
                SOLUTION = InfoCollector.getNoSolution();
            }
        }

        return SOLUTION;
    }

    public Inequality getInequalitySolution(){
        getSolution();
        INEQUALITY_SOLUTION = new Inequality();

        switch (INEQUALITY_SIGN.trim()){
            case ">":
                INEQUALITY_SOLUTION = new Inequality(SOLUTION_NUMBER, false, false, InfoCollector.getPositiveInfinity());
                break;
            case "<":
                INEQUALITY_SOLUTION = new Inequality(InfoCollector.getNegativeInfinity(), false, false, SOLUTION_NUMBER);
                break;
            case GREATER_THAN_OR_EQUAL_SIGN:
                INEQUALITY_SOLUTION = new Inequality(SOLUTION_NUMBER, true, false, InfoCollector.getPositiveInfinity());
                break;
            case LESS_THAN_OR_EQUAL_SIGN:
                INEQUALITY_SOLUTION = new Inequality(InfoCollector.getNegativeInfinity(), false, true, SOLUTION_NUMBER);
                break;
        }

        return INEQUALITY_SOLUTION;
    }

}
