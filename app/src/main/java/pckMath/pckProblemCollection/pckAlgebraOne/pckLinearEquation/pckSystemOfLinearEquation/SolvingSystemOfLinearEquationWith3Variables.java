package pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.pckSystemOfLinearEquation;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 9/6/2017.
 */

public class SolvingSystemOfLinearEquationWith3Variables extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SolvingSystemOfLinearEquationWith3Variables(){
        createProblem();
        swapAnswer();
    }

    public SolvingSystemOfLinearEquationWith3Variables(String Question,
                                             String RightAnswer,
                                             String AnswerA,
                                             String AnswerB,
                                             String AnswerC,
                                             String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 3;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SolvingSystemOfLinearEquationWith3Variables(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  3 SOLUTIONS
    private void createProblem_01(){
        int x = MathUtility.getRandomNumber_1Digit();
        int y = MathUtility.getRandomNumber_1Digit();
        int z = MathUtility.getRandomNumber_1Digit();

        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = a * x + b * y + c * z;

        int e = MathUtility.getRandomNumber_1Digit();
        int f = MathUtility.getRandomNumber_1Digit();
        int g = MathUtility.getRandomNumber_1Digit();
        int h = e * x + f * y + g * z;

        int i = MathUtility.getRandomNumber_1Digit();
        int j = MathUtility.getRandomNumber_1Digit();
        int k = MathUtility.getRandomNumber_1Digit();
        int l = i * x +  j* y + k * z;

        QUESTION = String.valueOf(a) + "x + " + String.valueOf(b) + "y + " + String.valueOf(c) + "z = " + String.valueOf(d) + "<br>"
                + String.valueOf(e) + "x + " + String.valueOf(f) + "y + " + String.valueOf(g) + "z = " + String.valueOf(h) + "<br>"
                + String.valueOf(i) + "x + " + String.valueOf(j) + "y + " + String.valueOf(k) + "z = " + String.valueOf(l);
        RIGHT_ANSWER = "x = " + String.valueOf(x) + ", y = " + String.valueOf(y) + ", z = " + String.valueOf(z);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = InfoCollector.getNoSolution();
        ANSWER_C = InfoCollector.getInfiniteManySolution();
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ", y = " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ", z = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  MANY SOLUTIONS
    private void createProblem_02(){
        int multiple = MathUtility.getRandomNumber_1Digit();

        int x = MathUtility.getRandomNumber_1Digit();
        int y = MathUtility.getRandomNumber_1Digit();
        int z = MathUtility.getRandomNumber_1Digit();

        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = a * x + b * y + c * z;

        int e = multiple * a;
        int f = multiple * b;
        int g = multiple * c;
        int h = multiple * d;

        int i = multiple * e;
        int j = multiple * f;
        int k = multiple * g;
        int l = multiple * h;

        QUESTION = String.valueOf(a) + "x + " + String.valueOf(b) + "y + " + String.valueOf(c) + "z = " + String.valueOf(d) + "<br>"
                + String.valueOf(e) + "x + " + String.valueOf(f) + "y + " + String.valueOf(g) + "z = " + String.valueOf(h) + "<br>"
                + String.valueOf(i) + "x + " + String.valueOf(j) + "y + " + String.valueOf(k) + "z = " + String.valueOf(l);
        RIGHT_ANSWER = InfoCollector.getInfiniteManySolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = InfoCollector.getNoSolution();
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ", y = " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ", z = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ", y = " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ", z = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    //  NO SOLUTIONS
    private void createProblem_03(){
        int multiple = MathUtility.getRandomNumber_1Digit();

        int x = MathUtility.getRandomNumber_1Digit();
        int y = MathUtility.getRandomNumber_1Digit();
        int z = MathUtility.getRandomNumber_1Digit();

        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = a * x + b * y + c * z;

        int e = multiple * a;
        int f = multiple * b;
        int g = multiple * c;
        int h = multiple * d + (MathUtility.getRandomPositiveNumber_1Digit() % (multiple - 1) + 1);

        int i = MathUtility.getRandomNumber_1Digit();
        int j = MathUtility.getRandomNumber_1Digit();
        int k = MathUtility.getRandomNumber_1Digit();
        int l = MathUtility.getRandomNumber_1Digit();

        QUESTION = String.valueOf(a) + "x + " + String.valueOf(b) + "y + " + String.valueOf(c) + "z = " + String.valueOf(d) + "<br>"
                + String.valueOf(e) + "x + " + String.valueOf(f) + "y + " + String.valueOf(g) + "z = " + String.valueOf(h) + "<br>"
                + String.valueOf(i) + "x + " + String.valueOf(j) + "y + " + String.valueOf(k) + "z = " + String.valueOf(l);
        RIGHT_ANSWER = InfoCollector.getNoSolution();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = InfoCollector.getInfiniteManySolution();
        ANSWER_C = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ", y = " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ", z = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
        ANSWER_D = "x = " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ", y = " + String.valueOf(MathUtility.getRandomNumber_1Digit()) + ", z = " + String.valueOf(MathUtility.getRandomNumber_1Digit());
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
