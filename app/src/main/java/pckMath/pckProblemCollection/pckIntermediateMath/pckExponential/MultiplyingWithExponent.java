package pckMath.pckProblemCollection.pckIntermediateMath.pckExponential;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 7/7/2017.
 */

public class MultiplyingWithExponent extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public MultiplyingWithExponent(){
        createProblem();
        swapAnswer();
    }

    public MultiplyingWithExponent(String Question,
                                 String RightAnswer,
                                 String AnswerA,
                                 String AnswerB,
                                 String AnswerC,
                                 String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }

        return new MultiplyingWithExponent(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  EXP x EXP = EXP     -POSITIVE BASE-
    private void createProblem_01(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit();
        int ExponentA = MathUtility.getRandomPositiveNumber_1Digit();
        int ExponentB = MathUtility.getRandomPositiveNumber_1Digit();

        Exponential ExpA = new Exponential(Base, ExponentA);
        Exponential ExpB = new Exponential(Base, ExponentB);
        Exponential Result = new Exponential(Base, ExponentA + ExponentB);

        QUESTION = ExpA.toString() + " x " + ExpB.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Exponential(2 * Base, ExponentA + ExponentB)).toString();
        ANSWER_C = (new Exponential(Base * Base, ExponentA * ExponentB)).toString();
        ANSWER_D = (new Exponential(Base, ExponentA * ExponentB)).toString();
    }

    //  EXP x EXP = EXP     -NEGATIVE BASE-
    private void createProblem_02(){
        int Base = MathUtility.getRandomNegativeNumber_1Digit();
        int ExponentA = MathUtility.getRandomNumber_1Digit();
        int ExponentB = MathUtility.getRandomNumber_1Digit();

        Exponential ExpA = new Exponential(Base, ExponentA);
        Exponential ExpB = new Exponential(Base, ExponentB);
        Exponential Result = new Exponential(Base, ExponentA + ExponentB);

        QUESTION = ExpA.toString(true) + " x " + ExpB.toString(true) + " = ?";
        RIGHT_ANSWER = Result.toString(true);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Exponential(2 * Base, ExponentA + ExponentB)).toString(true);
        ANSWER_C = (new Exponential(Base * Base, ExponentA * ExponentB)).toString(true);
        ANSWER_D = (new Exponential(Base, ExponentA * ExponentB)).toString(true);
    }

    //  EXP x EXP = EXP x EXP     -POSITIVE BASE-
    private void createProblem_03(){
        int BaseA = MathUtility.getRandomPositiveNumber_1Digit();
        int ExponentA = MathUtility.getRandomPositiveNumber_1Digit();

        int temp = MathUtility.getRandomPositiveNumber_1Digit();
        int BaseB = BaseA * temp;
        int ExponentB = MathUtility.getRandomPositiveNumber_1Digit();

        Exponential ExpA = new Exponential(BaseA, ExponentA);
        Exponential ExpB = new Exponential(BaseB, ExponentB);
        String Result = (new Exponential(BaseA, ExponentA + ExponentB)).toString()
                + (new Exponential(temp, ExponentB)).toString();

        QUESTION = ExpA.toString() + " x " + ExpB.toString() + " = ?";
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Exponential(BaseA * BaseB, ExponentA + ExponentB)).toString();
        ANSWER_C = (new Exponential(BaseA * BaseB, ExponentA * ExponentB)).toString();
        ANSWER_D = (new Exponential(BaseA + BaseB, ExponentA + ExponentB)).toString();
    }

    //  EXP x EXP = EXP x EXP     -NEGATIVE BASE-
    private void createProblem_04(){
        int BaseA = MathUtility.getRandomPositiveNumber_1Digit();
        int ExponentA = MathUtility.getRandomPositiveNumber_1Digit();

        int temp = MathUtility.getRandomNegativeNumber_1Digit();
        int BaseB = BaseA * temp;
        int ExponentB = MathUtility.getRandomPositiveNumber_1Digit();

        Exponential ExpA = new Exponential(BaseA, ExponentA);
        Exponential ExpB = new Exponential(BaseB, ExponentB);
        String Result = (new Exponential(BaseA, ExponentA + ExponentB)).toString()
                + (new Exponential(temp, ExponentB)).toString(true);

        QUESTION = ExpA.toString() + " x " + ExpB.toString(true) + " = ?";
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Exponential(BaseA * BaseB, ExponentA + ExponentB)).toString(true);
        ANSWER_C = (new Exponential(BaseA * BaseB, ExponentA * ExponentB)).toString();
        ANSWER_D = (new Exponential(BaseA + BaseB, ExponentA + ExponentB)).toString();
    }

    //  EXP x EXP = EXP x EXP     -NEGATIVE BASE-
    private void createProblem_05(){
        int BaseA = MathUtility.getRandomNegativeNumber_1Digit();
        int ExponentA = MathUtility.getRandomPositiveNumber_1Digit();

        int temp = MathUtility.getRandomNegativeNumber_1Digit();
        int BaseB = BaseA * temp;
        int ExponentB = MathUtility.getRandomPositiveNumber_1Digit();

        Exponential ExpA = new Exponential(BaseA, ExponentA);
        Exponential ExpB = new Exponential(BaseB, ExponentB);
        String Result = (new Exponential(BaseA, ExponentA + ExponentB)).toString(true)
                + (new Exponential(temp, ExponentB)).toString(true);

        QUESTION = ExpA.toString(true) + " x " + ExpB.toString() + " = ?";
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Exponential(BaseA * BaseB, ExponentA + ExponentB)).toString(true);
        ANSWER_C = (new Exponential(BaseA * BaseB, ExponentA * ExponentB)).toString();
        ANSWER_D = (new Exponential(BaseA + BaseB, ExponentA + ExponentB)).toString();
    }

    //  FRACTION x FRACTION
    private void createProblem_06(){
        Fraction _Fraction = FractionMath.getRandomFraction();
        int ExponentA = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int ExponentB = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;

        QUESTION = "(" + _Fraction.toString() + ")" + InfoCollector.getExponentSign(String.valueOf(ExponentA)) + " x "
                + "(" + _Fraction.toString() + ")" + InfoCollector.getExponentSign(String.valueOf(ExponentB));
        RIGHT_ANSWER = "(" + _Fraction.toString() + ")" + InfoCollector.getExponentSign(String.valueOf(ExponentA + ExponentB));

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "(" + _Fraction.toString() + ")" + InfoCollector.getExponentSign(String.valueOf(ExponentA));
        ANSWER_C = "(" + _Fraction.toString() + ")" + InfoCollector.getExponentSign(String.valueOf(ExponentB));
        ANSWER_D = "(" + _Fraction.toString() + ")" + InfoCollector.getExponentSign(String.valueOf(ExponentA - ExponentB));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
