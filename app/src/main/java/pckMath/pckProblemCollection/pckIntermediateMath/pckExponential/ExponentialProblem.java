package pckMath.pckProblemCollection.pckIntermediateMath.pckExponential;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 5/19/2017.
 */

public class ExponentialProblem extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public ExponentialProblem(){
        createProblem();
        swapAnswer();
    }

    public ExponentialProblem(String Question,
                               String RightAnswer,
                               String AnswerA,
                               String AnswerB,
                               String AnswerC,
                               String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 8;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            default:
                createProblem_01();
                break;
        }

        return new ExponentialProblem(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit();
        int Exponent = MathUtility.getRandomPositiveNumber_1Digit() % 4;

        Exponential Problem = new Exponential(Base, Exponent);
        int Result = Problem.getValue();

        QUESTION = Problem.toString() + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((Result + 1));
        ANSWER_C = String.valueOf((Result + 2));
        ANSWER_D = String.valueOf((Result - 1));
    }

    private void createProblem_02(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() % 4;
        int Exponent = MathUtility.getRandomPositiveNumber_1Digit();

        Exponential Problem = new Exponential(Base, Exponent);
        int Result = Problem.getValue();

        QUESTION = Problem.toString() + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((Result + 1));
        ANSWER_C = String.valueOf((Result + 2));
        ANSWER_D = String.valueOf((Result - 1));
    }

    private void createProblem_03(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Exponent = MathUtility.getRandomPositiveNumber_1Digit() % 4;

        Exponential Problem = new Exponential(Base, Exponent);
        int Result = Problem.getValue();

        QUESTION = "?" + InfoCollector.getStartExponential() + String.valueOf(Exponent) + InfoCollector.getEndExponential()
                + " = " + String.valueOf(Result);
        RIGHT_ANSWER = String.valueOf(Base);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Base + 1);
        ANSWER_C = String.valueOf(Base * -1);
        ANSWER_D = String.valueOf(Base - 1);
    }

    private void createProblem_04(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit();
        int Exponent = MathUtility.getRandomPositiveNumber_1Digit() % 4;

        Exponential Problem = new Exponential(Base, Exponent);
        int Result = Problem.getValue();

        QUESTION = String.valueOf(Base) + InfoCollector.getStartExponential() + "?" + InfoCollector.getEndExponential()
                + " = " + String.valueOf(Result);
        RIGHT_ANSWER = String.valueOf(Exponent);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Base + 1);
        ANSWER_C = String.valueOf(Base + 2);
        ANSWER_D = String.valueOf(Base - 1);
    }

    private void createProblem_05(){
        int Base = MathUtility.getRandomNegativeNumber_1Digit();
        int Exponent = MathUtility.getRandomPositiveNumber_2Digit() % 4;

        Exponential Problem = new Exponential(Base, Exponent);
        int Result = Problem.getValue(false);

        QUESTION = Problem.toString() + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result - 1);
        ANSWER_D = String.valueOf(Math.abs(Result));
    }

    private void createProblem_06(){
        int Base = MathUtility.getRandomNegativeNumber_1Digit();
        int Exponent = MathUtility.getRandomPositiveNumber_1Digit() % 4;

        Exponential Problem = new Exponential(Base, Exponent);
        int Result = Problem.getValue(true);

        QUESTION = Problem.toString(true) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result - 1);
        ANSWER_D = String.valueOf(Result * -1);
    }

    private void createProblem_07(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Exponent = 0 - MathUtility.getRandomPositiveNumber_2Digit() % 4;

        QUESTION = String.valueOf(Base) + InfoCollector.getStartExponential() + String.valueOf(Exponent) + InfoCollector.getEndExponential() + " = ?";
        RIGHT_ANSWER = (new Fraction(1, (new Exponential(Base, Math.abs(Exponent))).getValue())).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(new Exponential(Base, Math.abs(Exponent)).getValue());
        ANSWER_C = String.valueOf(new Exponential(0 - Base, Math.abs(Exponent)).getValue());
        ANSWER_D = (new Fraction(1, (new Exponential(0 - Base, Math.abs(Exponent))).getValue(false))).toString();
    }

    private void createProblem_08(){
        Fraction _Fraction = FractionMath.getRandomFraction();
        int Exponent = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;

        Fraction Result = new Fraction(new Exponential(_Fraction.getNumerator(), Exponent).getValue(),
                new Exponential(_Fraction.getDenominator(), Exponent).getValue());

        QUESTION = "(" + _Fraction.toString() + ")" + InfoCollector.getExponentSign(String.valueOf(Exponent)) + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionMath.getReciprocal(Result).toString();
        ANSWER_C = FractionMath.findEquivalentFraction(_Fraction, Exponent).toString();
        ANSWER_D = FractionMath.getRandomProperFraction().toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
