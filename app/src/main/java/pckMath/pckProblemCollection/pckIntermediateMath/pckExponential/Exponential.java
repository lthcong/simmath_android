package pckMath.pckProblemCollection.pckIntermediateMath.pckExponential;

import pckInfo.InfoCollector;
import pckMath.MathUtility;

/**
 * Created by Cong on 5/19/2017.
 */

public class Exponential {

    private int BASE, EXPONENT;

    public Exponential(){
        BASE = 1;
        EXPONENT = 0;
    }

    public Exponential(int Base, int Exponent){
        BASE = Base;
        EXPONENT = Exponent;
    }

    public void setBase(int Base){
        BASE = Base;
    }

    public int getBase(){
        return BASE;
    }

    public void setExponent(int Exponent){
        EXPONENT = Exponent;
    }

    public int getExponent(){
        return EXPONENT;
    }

    public int getValue(){
        return (int) Math.pow(BASE, EXPONENT);
    }

    public int getValue(boolean WithParentheses){
        int ExpValue = (int) Math.pow(Math.abs(BASE), EXPONENT);
        if (BASE < 0){
            if (WithParentheses){
                if (EXPONENT % 2 != 0){
                    ExpValue = ExpValue * -1;
                }
            }
            else {
                ExpValue = ExpValue * -1;
            }
        }
        return ExpValue;
    }

    public String toString(){
        return String.valueOf(BASE) + InfoCollector.getStartExponential() + String.valueOf(EXPONENT) + InfoCollector.getEndExponential();
    }

    public String toString(boolean WithParentheses){
        String ExpString = toString();
        if (WithParentheses){
            ExpString = "(" + String.valueOf(BASE) + ")" + InfoCollector.getStartExponential() + String.valueOf(EXPONENT) + InfoCollector.getEndExponential();
        }
        return ExpString;
    }

}
