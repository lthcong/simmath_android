package pckMath.pckProblemCollection.pckIntermediateMath.pckExponential;


import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 5/21/2017.
 */

public class OrderOfOperationsWithExponent extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public OrderOfOperationsWithExponent(){
        createProblem();
        swapAnswer();
    }

    public OrderOfOperationsWithExponent(String Question,
                               String RightAnswer,
                               String AnswerA,
                               String AnswerB,
                               String AnswerC,
                               String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 13;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            case 7:
                createProblem_08();
                break;
            case 8:
                createProblem_09();
                break;
            case 9:
                createProblem_10();
                break;
            case 10:
                createProblem_11();
                break;
            case 11:
                createProblem_12();
                break;
            case 12:
                createProblem_13();
                break;
            default:
                createProblem_01();
                break;
        }

        return new OrderOfOperationsWithExponent(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  a + b^c + d
    private void createProblem_01(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int d = MathUtility.getRandomNumber_1Digit();

        Exponential Exp = new Exponential(b, c);

        int Result = a + Exp.getValue() + d;

        QUESTION = String.valueOf(a) + " + " + Exp.toString() + " + "+ String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);
        ANSWER_D = String.valueOf(Result - 1);
    }

    //  a + b^c + d
    private void createProblem_02(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNegativeNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int d = MathUtility.getRandomNumber_1Digit();

        Exponential Exp = new Exponential(b, c);

        int Result = a + Exp.getValue(true) + d;

        QUESTION = String.valueOf(a)+ " + " + Exp.toString(true) + " + "+ String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);
        ANSWER_D = String.valueOf(Result - 1);
    }

    //  a + b^c + d
    private void createProblem_03(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNegativeNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int d = MathUtility.getRandomNumber_1Digit();

        Exponential Exp = new Exponential(b, c);

        int Result = a + Exp.getValue(false) + d;

        QUESTION = String.valueOf(a) + " + "+ Exp.toString(false) + " + "+ String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);
        ANSWER_D = String.valueOf(Result - 1);
    }

    //  a^ b + c x d
    private void createProblem_04(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        Exponential Exp = new Exponential(a, b);

        int Result = Exp.getValue() + c * d;

        QUESTION = Exp.toString() + " + " + String.valueOf(c) + " x " + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);
        ANSWER_D = String.valueOf(Result - 1);
    }

    //  a^ b - c x d
    private void createProblem_05(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        Exponential Exp = new Exponential(a, b);

        int Result = Exp.getValue() - c * d;

        QUESTION = Exp.toString() + " - " + String.valueOf(c) + " x " + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);
        ANSWER_D = String.valueOf(Result - 1);
    }

    //  a^ b - c x d
    private void createProblem_06(){
        int a = MathUtility.getRandomNegativeNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();

        Exponential Exp = new Exponential(a, b);

        int Result = Exp.getValue(true) - c * d;

        QUESTION = Exp.toString(true) + " - " + String.valueOf(c) + " x " + String.valueOf(d) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);
        ANSWER_D = String.valueOf(Result - 1);
    }

    //  a^ b - c^d + e^f
    private void createProblem_07(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int c = MathUtility.getRandomPositiveNumber_1Digit();
        int d = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int e = MathUtility.getRandomPositiveNumber_1Digit();
        int f = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;

        Exponential ExpA = new Exponential(a, b);
        Exponential ExpB = new Exponential(c, d);
        Exponential ExpC = new Exponential(e, f);

        int Result = ExpA.getValue() - ExpB.getValue() + ExpC.getValue();

        QUESTION = ExpA.toString() + " - " + ExpB.toString() + " + " + ExpC.toString();
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);
        ANSWER_D = String.valueOf(Result - 1);
    }

    //  a^ b - c^d + e^f
    private void createProblem_08(){
        int a = MathUtility.getRandomNegativeNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int c = MathUtility.getRandomNegativeNumber_1Digit();
        int d = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int e = MathUtility.getRandomNegativeNumber_1Digit();
        int f = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;

        Exponential ExpA = new Exponential(a, b);
        Exponential ExpB = new Exponential(c, d);
        Exponential ExpC = new Exponential(e, f);

        int Result = ExpA.getValue(true) - ExpB.getValue(true) + ExpC.getValue(true);

        QUESTION = ExpA.toString(true) + " - " + ExpB.toString(true) + " + " + ExpC.toString(true);
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);
        ANSWER_D = String.valueOf(Result - 1);
    }

    //  a^ b - c^d + e^f
    private void createProblem_09(){
        int a = MathUtility.getRandomNegativeNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int c = MathUtility.getRandomPositiveNumber_1Digit();
        int d = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int e = MathUtility.getRandomNegativeNumber_1Digit();
        int f = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;

        Exponential ExpA = new Exponential(a, b);
        Exponential ExpB = new Exponential(c, d);
        Exponential ExpC = new Exponential(e, f);

        int Result = ExpA.getValue(true) - ExpB.getValue(true) + ExpC.getValue(true);

        QUESTION = ExpA.toString(true) + " - " + ExpB.toString() + " + " + ExpC.toString(true);
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);
        ANSWER_D = String.valueOf(Result - 1);
    }

    //  a^b + c x d - e^f
    private void createProblem_10(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        int e = MathUtility.getRandomNegativeNumber_1Digit();
        int f = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;

        Exponential ExpA = new Exponential(a, b);
        Exponential ExpB = new Exponential(e, f);

        int Result = ExpA.getValue() + c * d - ExpB.getValue(true);

        QUESTION = ExpA.toString(true) + " + " + String.valueOf(c) + " x " + String.valueOf(d) + " - " + ExpB.toString(true);
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);
        ANSWER_D = String.valueOf(Result - 1);
    }

    //  a + b^c - d^e + f
    private void createProblem_11(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int d = MathUtility.getRandomPositiveNumber_1Digit();
        int e = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int f = MathUtility.getRandomNumber_1Digit();

        Exponential ExpA = new Exponential(b, c);
        Exponential ExpB = new Exponential(d, e);

        int Result = a + ExpA.getValue() - ExpB.getValue() + f;

        QUESTION = String.valueOf(a) + " + " + ExpA.toString() + " - " + ExpB.toString() + " + " + String.valueOf(f);
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);
        ANSWER_D = String.valueOf(Result - 1);
    }

    //  a + b^c - d^e + f
    private void createProblem_12(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNegativeNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int d = MathUtility.getRandomPositiveNumber_1Digit();
        int e = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int f = MathUtility.getRandomNumber_1Digit();

        Exponential ExpA = new Exponential(b, c);
        Exponential ExpB = new Exponential(d, e);

        int Result = a + ExpA.getValue(true) - ExpB.getValue() + f;

        QUESTION = String.valueOf(a) + " + " + ExpA.toString(true) + " - " + ExpB.toString() + " + " + String.valueOf(f);
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);
        ANSWER_D = String.valueOf(Result - 1);
    }

    //  a + b^c - d^e + f
    private void createProblem_13(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNegativeNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int d = MathUtility.getRandomNegativeNumber_1Digit();
        int e = MathUtility.getRandomPositiveNumber_1Digit() % 2 + 2;
        int f = MathUtility.getRandomNumber_1Digit();

        Exponential ExpA = new Exponential(b, c);
        Exponential ExpB = new Exponential(d, e);

        int Result = a + ExpA.getValue(true) - ExpB.getValue(true) + f;

        QUESTION = String.valueOf(a) + " + " + ExpA.toString(true) + " - " + ExpB.toString(true) + " + " + String.valueOf(f);
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Result + 1);
        ANSWER_C = String.valueOf(Result + 2);
        ANSWER_D = String.valueOf(Result - 1);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }


    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }


}
