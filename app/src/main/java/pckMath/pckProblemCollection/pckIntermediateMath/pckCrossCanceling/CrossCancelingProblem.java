package pckMath.pckProblemCollection.pckIntermediateMath.pckCrossCanceling;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 6/2/2017.
 */

public class CrossCancelingProblem extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public CrossCancelingProblem(){
        createProblem();
        swapAnswer();
    }

    public CrossCancelingProblem(String Question,
                          String RightAnswer,
                          String AnswerA,
                          String AnswerB,
                          String AnswerC,
                          String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 4;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            default:
                createProblem_01();
                break;
        }

        return new CrossCancelingProblem(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.getReciprocal(FractionMath.findEquivalentFraction(FractionA));

        Fraction Result = FractionMath.multiplyFraction(FractionA, FractionB);

        QUESTION = FractionA.toString() + " x " + FractionB.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getReciprocal(FractionMath.findEquivalentFraction(FractionB)).toString();
    }

    private void createProblem_02(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.findEquivalentFraction(FractionA);

        Fraction Result = FractionMath.divideFraction(FractionA, FractionB);

        QUESTION = FractionA.toString() + InfoCollector.getDivisionSign() + FractionB.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getReciprocal(FractionMath.findEquivalentFraction(FractionB)).toString();
    }

    private void createProblem_03(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.getReciprocal(FractionMath.findEquivalentFraction(FractionA));
        Fraction FractionC = FractionMath.getReciprocal(FractionMath.findEquivalentFraction(FractionA));

        Fraction Result = FractionMath.multiplyFraction(FractionA, FractionB);
        Result = FractionMath.multiplyFraction(Result, FractionC);

        QUESTION = FractionA.toString() + " x " + FractionB.toString() + " x " + FractionC.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getReciprocal(FractionMath.findEquivalentFraction(FractionC)).toString();
    }

    private void createProblem_04(){
        Fraction FractionA = FractionMath.getRandomFraction();
        Fraction FractionB = FractionMath.getReciprocal(FractionMath.findEquivalentFraction(FractionA));
        Fraction FractionC = FractionMath.findEquivalentFraction(FractionA);

        Fraction Result = FractionMath.multiplyFraction(FractionA, FractionB);
        Result = FractionMath.divideFraction(Result, FractionC);

        QUESTION = FractionA.toString() + " x " + FractionB.toString() + InfoCollector.getDivisionSign() + FractionC.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = FractionA.toString();
        ANSWER_C = FractionB.toString();
        ANSWER_D = FractionMath.getReciprocal(FractionMath.findEquivalentFraction(FractionC)).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
