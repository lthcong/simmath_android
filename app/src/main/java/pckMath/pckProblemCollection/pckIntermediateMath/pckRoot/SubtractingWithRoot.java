package pckMath.pckProblemCollection.pckIntermediateMath.pckRoot;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 7/7/2017.
 */

public class SubtractingWithRoot extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public SubtractingWithRoot(){
        createProblem();
        swapAnswer();
    }

    public SubtractingWithRoot(String Question,
                          String RightAnswer,
                          String AnswerA,
                          String AnswerB,
                          String AnswerC,
                          String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 5;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            default:
                createProblem_01();
                break;
        }

        return new SubtractingWithRoot(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  sqrt(a x a x b) - sqrt(c x c x b)
    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int b = MathUtility.getRandomPrimeNumber();
        int c = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        int NumberA = a * a * b;
        int NumberB = c * c * b;

        Root RootA = new Root(2, NumberA);
        Root RootB = new Root(2, NumberB);

        String Result = String.valueOf((a - c)) + (new Root(2, b)).toString();

        QUESTION = RootA.toString() + " - " + RootB.toString() + " = ?";
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((a)) + (new Root(2, b)).toString();
        ANSWER_C = String.valueOf((c)) + (new Root(2, b)).toString();
        ANSWER_D = String.valueOf((a)) + (new Root(2, c)).toString();
    }

    //  a x sqrt(b) - sqrt(c x c x b)
    private void createProblem_02(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomPrimeNumber();
        int c = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        QUESTION = String.valueOf(a) + (new Root(2, b)).toString() + " - " +
                (new Root(2, c * c * b)).toString() + " = ?";
        RIGHT_ANSWER = String.valueOf((a - c)) + (new Root(2, b)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(b) + (new Root(2, (a + c))).toString();
        ANSWER_C = String.valueOf(a) + (new Root(2, b)).toString();
        ANSWER_D = String.valueOf(c) + (new Root(2, b)).toString();
    }

    //  sqrt(c x c x b) - a x sqrt(b)
    private void createProblem_03(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomPrimeNumber();
        int c = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        QUESTION =  (new Root(2, c * c * b)).toString() + " - " +
                String.valueOf(a) + (new Root(2, b)).toString() + " = ?";
        RIGHT_ANSWER = String.valueOf((a - c)) + (new Root(2, b)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(b) + (new Root(2, (a + c))).toString();
        ANSWER_C = String.valueOf(a) + (new Root(2, b)).toString();
        ANSWER_D = String.valueOf(c) + (new Root(2, b)).toString();
    }

    //  a x sqrt(b) - c x sqrt(b)
    private void createProblem_04(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomPrimeNumber();
        int c = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        QUESTION = String.valueOf(a) + (new Root(2, b)).toString() + " - " +
                String.valueOf(c) + (new Root(2, b)).toString() + " = ?";
        RIGHT_ANSWER = String.valueOf((a - c)) + (new Root(2, b)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(b) + (new Root(2, (a + c))).toString();
        ANSWER_C = String.valueOf(a) + (new Root(2, b)).toString();
        ANSWER_D = String.valueOf(c) + (new Root(2, b)).toString();
    }

    //  sqrt(a^3) - b x sqrt(a)
    private void createProblem_05(){
        int a = MathUtility.getRandomPrimeNumber();
        int b = MathUtility.getRandomNumber_1Digit();

        QUESTION = (new Root(3, a * a * a)).toString() + " - " +
                String.valueOf(b) + (new Root(2, a)).toString();
        RIGHT_ANSWER = String.valueOf((a - b)) + (new Root(2, a)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(b) + (new Root(2, a * a * a + a)).toString();
        ANSWER_C = String.valueOf(b + 1) + (new Root(2, a * a * a + a)).toString();
        ANSWER_D = String.valueOf((a + b)) + (new Root(3, a)).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
