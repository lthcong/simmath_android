package pckMath.pckProblemCollection.pckIntermediateMath.pckRoot;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckIntermediateMath.pckExponential.Exponential;

/**
 * Created by Cong on 7/7/2017.
 */

public class MultiplyingWithRoot extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public MultiplyingWithRoot(){
        createProblem();
        swapAnswer();
    }

    public MultiplyingWithRoot(String Question,
                          String RightAnswer,
                          String AnswerA,
                          String AnswerB,
                          String AnswerC,
                          String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }

        return new MultiplyingWithRoot(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  sqrt(a) x sqrt(b)
    private void createProblem_01(){
        int a = MathUtility.getRandomPrimeNumber();
        int b = MathUtility.getRandomPrimeNumber();

        Root RootA = new Root(2, a);
        Root RootB = new Root(2, b);
        Root Result = new Root(2, a * b);

        QUESTION = RootA.toString() + " x " + RootB.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = new Root(2, a + b).toString();
        ANSWER_C = new Root(2, b).toString();
        ANSWER_D = new Root(2, a).toString();
    }

    //  sqrt(a) x sqrt(a x b)
    private void createProblem_02(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int b = MathUtility.getRandomPrimeNumber();

        Root RootA = new Root(2, a);
        Root RootB = new Root(2, (a * b));

        String Result = String.valueOf(a) + (new Root(2, b)).toString();

        QUESTION = RootA.toString() + " x " + RootB.toString() + " = ?";
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(b) + (new Root(2, a)).toString();
        ANSWER_C = RootB.toString();
        ANSWER_D = String.valueOf(a) + (new Root(3, b)).toString();
    }

    //  sqrt(a x b) x sqrt(a x c)
    private void createProblem_03(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int b = MathUtility.getRandomPrimeNumber();
        int c = MathUtility.getRandomPrimeNumber();

        while(true) {
            if (b != c){
                break;
            }
            else {
                b = MathUtility.getRandomPrimeNumber();
                c = MathUtility.getRandomPrimeNumber();
            }
        }

        Root RootA = new Root(2, (a * b));
        Root RootB = new Root(2, (a * c));

        String Result = String.valueOf(a) + (new Root(2, (b * c))).toString();

        QUESTION = RootA.toString() + " x " + RootB.toString() + " = ?";
        RIGHT_ANSWER = Result;

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Root(2, (b * c))).toString();
        ANSWER_C = RootA.toString();
        ANSWER_D = RootB.toString();
    }

    //  a x sqrt(b) x c x sqrt(d)
    private void createProblem_04(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomPrimeNumber();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomPrimeNumber();

        while (true) {
            if (b != d){
                break;
            }
            else {
                b = MathUtility.getRandomPrimeNumber();
                d = MathUtility.getRandomPrimeNumber();
            }
        }

        QUESTION = String.valueOf(a) + (new Root(2, b)).toString() + " x "
                + String.valueOf(c) + (new Root(2, d)).toString() + " = ?";
        RIGHT_ANSWER = String.valueOf((a * c)) + (new Root(2, (b * d))).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((a)) + (new Root(2, (b * d))).toString();
        ANSWER_C = String.valueOf((c)) + (new Root(2, (b * d))).toString();
        ANSWER_D = String.valueOf((a * c)) + (new Root(2, d)).toString();
    }

    //  a x cube_root(b^2) x c x cube_root(b)
    private void createProblem_05(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomPrimeNumber();
        int c = MathUtility.getRandomNumber_1Digit();

        QUESTION = String.valueOf(a) + (new Root(3, b * b)).toString() + " x "
                + String.valueOf(c) + (new Root(3, b)).toString() + " = ?";
        RIGHT_ANSWER = String.valueOf((a * b * c));

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a * c) + (new Root(3, b * b)).toString();
        ANSWER_C = String.valueOf((a * c + b));
        ANSWER_D = String.valueOf(a * c) + (new Root(3, 3 * b)).toString();
    }

    //  sqrt(a^3)
    private void createProblem_06(){
        int a = MathUtility.getRandomPrimeNumber();
        int Temp = (new Exponential(a, 3)).getValue();

        QUESTION = (new Root(2, Temp)).toString() + " = ?";
        RIGHT_ANSWER = String.valueOf(a) + (new Root(2, a)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a) + (new Root(3, a)).toString();
        ANSWER_C = String.valueOf(a);
        ANSWER_D = String.valueOf(a) + (new Root(2, a * a)).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
