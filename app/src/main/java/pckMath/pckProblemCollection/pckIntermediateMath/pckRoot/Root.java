package pckMath.pckProblemCollection.pckIntermediateMath.pckRoot;

import pckInfo.InfoCollector;

/**
 * Created by Cong on 5/31/2017.
 */

public class Root {

    private int N_ROOT, NUMBER;

    public Root(){
        N_ROOT = 1;
        NUMBER = 1;
    }

    public Root(int n_root, int number){
        N_ROOT = n_root;
        NUMBER = number;
    }

    public void setNRoot(int NRoot){
        N_ROOT = NRoot;
    }

    public int getNRoot(){
        return N_ROOT;
    }

    public void setNumber(int number){
        NUMBER = number;
    }

    public int getNumber(){
        return NUMBER;
    }

    public double getValue(){
        return Math.pow(NUMBER, (1.0 / N_ROOT));
    }

    public String toString(){
        String RootString;

        switch (N_ROOT){
            case 1:
                RootString = String.valueOf(NUMBER);
                break;
            case 2:
                RootString = InfoCollector.getSquareRootSign() + "(" + String.valueOf(NUMBER) + ")";
                break;
            default:
                RootString = InfoCollector.getStartExponential() + String.valueOf(N_ROOT) + InfoCollector.getEndExponential() +
                        InfoCollector.getSquareRootSign() + "(" + String.valueOf(NUMBER) + ")";
                break;
        }

        return RootString;
    }

}
