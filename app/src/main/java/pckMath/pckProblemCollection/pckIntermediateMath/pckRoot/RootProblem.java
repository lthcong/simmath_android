package pckMath.pckProblemCollection.pckIntermediateMath.pckRoot;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 5/31/2017.
 */

public class RootProblem extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public RootProblem(){
        createProblem();
        swapAnswer();
    }

    public RootProblem(String Question,
                               String RightAnswer,
                               String AnswerA,
                               String AnswerB,
                               String AnswerC,
                               String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }

        return new RootProblem(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  sqrt(a) = b
    private void createProblem_01(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit();
        int Temp = Base * Base;

        QUESTION = (new Root(2, Temp)).toString() + " = ?";
        RIGHT_ANSWER = String.valueOf(Base);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((Base + 1));
        ANSWER_C = String.valueOf((Base - 1));
        ANSWER_D = String.valueOf((Base + 2));
    }

    //  sqrt(a) = b
    private void createProblem_02(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit();
        int Temp = Base * Base * Base;

        QUESTION = (new Root(3, Temp)).toString() + " = ?";
        RIGHT_ANSWER = String.valueOf(Base);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((Base + 1));
        ANSWER_C = String.valueOf((Base - 1));
        ANSWER_D = String.valueOf((Base + 2));
    }

    //  sqrt(a) = b * sqrt(c)
    private void createProblem_03(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit();
        int Temp = MathUtility.getRandomPrimeNumber();

        int NUMBER = Base * Base * Temp;

        QUESTION = (new Root(2, NUMBER)).toString() + " = ?";
        RIGHT_ANSWER = String.valueOf(Base) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(Temp) + ")";

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Temp) + InfoCollector.getSquareRootSign() + "(" + String.valueOf(Base) + ")";
        ANSWER_C = String.valueOf(Temp);
        ANSWER_D = InfoCollector.getSquareRootSign() + "(" + String.valueOf(Base) + ")";
    }

    //  sqrt(a) = b * sqrt(c)
    private void createProblem_04(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit();
        int Temp = MathUtility.getRandomPrimeNumber();

        int NUMBER = Base * Base * Temp;

        QUESTION = "? = " + String.valueOf(Base) + (new Root(2, Temp)).toString();;
        RIGHT_ANSWER = (new Root(2, NUMBER)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = (new Root(2, Base)).toString();
        ANSWER_C = (new Root(2, Temp)).toString();
        ANSWER_D = (new Root(3, NUMBER)).toString();
    }

    //  sqrt(a) = b * sqrt(c)
    private void createProblem_05(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit();
        int Temp = MathUtility.getRandomPrimeNumber();

        int NUMBER = Base * Base * Base * Temp;

        QUESTION = (new Root(3, NUMBER)).toString() + " = ?";
        RIGHT_ANSWER = String.valueOf(Base) + (new Root(3, Temp)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Base) + (new Root(2, Temp)).toString();
        ANSWER_C = String.valueOf(Base);
        ANSWER_D = (new Root(3, Temp)).toString();
    }

    //  sqrt(a) = b * sqrt(c)
    private void createProblem_06(){
        int Base = MathUtility.getRandomPositiveNumber_1Digit();
        int Temp = MathUtility.getRandomPrimeNumber();

        int NUMBER = Base * Base * Base * Temp;

        QUESTION = "? = " + String.valueOf(Base) + (new Root(3, Temp)).toString();
        RIGHT_ANSWER = (new Root(3, NUMBER)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(Base) + (new Root(2, Temp)).toString();
        ANSWER_C = String.valueOf(Base);
        ANSWER_D = (new Root(3, Temp)).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
