package pckMath.pckProblemCollection.pckIntermediateMath.pckRoot;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckIntermediateMath.pckExponential.Exponential;

/**
 * Created by Cong on 7/7/2017.
 */

public class DividingWithRoot extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public DividingWithRoot(){
        createProblem();
        swapAnswer();
    }

    public DividingWithRoot(String Question,
                              String RightAnswer,
                              String AnswerA,
                              String AnswerB,
                              String AnswerC,
                              String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 5;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            default:
                createProblem_01();
                break;
        }

        return new DividingWithRoot(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  sqrt(a x b) / sqrt(b)
    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        Root RootA = new Root(2, (a * b));
        Root RootB = new Root(2, (b));
        Root Result = new Root(2, a);

        QUESTION = RootA.toString() + InfoCollector.getDivisionSign() + RootB.toString() + " = ?";
        RIGHT_ANSWER = Result.toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = RootA.toString();
        ANSWER_C = RootB.toString();
        ANSWER_D = (new Root(2, (a + b))).toString();
    }

    //  a x sqrt(b x c) / sqrt(b)
    private void createProblem_02(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomPrimeNumber();
        int c = MathUtility.getRandomPrimeNumber();

        QUESTION = String.valueOf(a) + (new Root(2, b * c)).toString() + InfoCollector.getDivisionSign() +
                (new Root(2, b)).toString() + " = ?";
        RIGHT_ANSWER = String.valueOf(a) + (new Root(2, c)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a) + (new Root(2, b)).toString();
        ANSWER_C = (new Root(2, (b * c))).toString();
        ANSWER_D = (new Root(2, a)).toString();
    }

    //  sqrt(a x a x b) / sqrt(b)
    private void createProblem_03(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        Root RootA = new Root(2, a * a * b);
        Root RootB = new Root(2, b);

        QUESTION = RootA.toString() + InfoCollector.getDivisionSign() + RootB.toString() + " = ?";
        RIGHT_ANSWER = String.valueOf(a);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = RootB.toString();
        ANSWER_C = RootA.toString();
        ANSWER_D = String.valueOf((a * a));
    }

    //  a x sqrt(b x c) / d x sqrt(b)
    private void createProblem_04(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomPrimeNumber();
        int c = MathUtility.getRandomPrimeNumber();
        int d = MathUtility.getRandomNumber_1Digit();

        QUESTION = String.valueOf(a) + (new Root(2, (b * c))).toString() + InfoCollector.getDivisionSign()
                + String.valueOf(d) + (new Root(2, b)).toString() + " = ?";

        if (a % d == 0) {
            RIGHT_ANSWER = String.valueOf((a / d)) + (new Root(2, c)).toString();
        }
        else {
            Fraction Temp = new Fraction(a, d);
            RIGHT_ANSWER = Temp.toString() + (new Root(2, c)).toString();
        }

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a) + (new Root(2, (b * c))).toString();
        ANSWER_C = String.valueOf(a) + (new Root(2, c)).toString();
        ANSWER_D = String.valueOf(d) + (new Root(2, c)).toString();
    }

    //  a x sqrt(b^3) / sqrt(b)
    private void createProblem_05(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 1;

        QUESTION = String.valueOf(a) + (new Root(2, b * b * b)).toString() + InfoCollector.getDivisionSign() +
                (new Root(2, b)).toString() + " = ?";
        RIGHT_ANSWER = String.valueOf((a * b));

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(b);
        ANSWER_C = String.valueOf(a);
        ANSWER_D = (new Root(2, b * b * b)).toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
