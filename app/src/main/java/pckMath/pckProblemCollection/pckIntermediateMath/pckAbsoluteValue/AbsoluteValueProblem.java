package pckMath.pckProblemCollection.pckIntermediateMath.pckAbsoluteValue;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 5/21/2017.
 */

public class AbsoluteValueProblem extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public AbsoluteValueProblem(){
        createProblem();
        swapAnswer();
    }

    public AbsoluteValueProblem(String Question,
                               String RightAnswer,
                               String AnswerA,
                               String AnswerB,
                               String AnswerC,
                               String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 7;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            case 6:
                createProblem_07();
                break;
            default:
                createProblem_01();
                break;
        }

        return new AbsoluteValueProblem(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  |A|
    private void createProblem_01(){
        int a = MathUtility.getRandomNumber_1Digit();
        int Result = Math.abs(a);

        QUESTION = "|" + String.valueOf(a) + "| = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((Result + 2));
        ANSWER_C = String.valueOf((Result + 1));
        ANSWER_D = String.valueOf((Result - 1));
    }

    //  |A + B| + C
    private void createProblem_02(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int Result = Math.abs((a + b)) + c;

        QUESTION = "|" + String.valueOf(a) + " + " + String.valueOf(b) + "| + " + String.valueOf(c) + " = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((Result + 2));
        ANSWER_C = String.valueOf((Result + 1));
        ANSWER_D = String.valueOf((Result - 1));
    }

    //  |A + B x C| - |D + E|
    private void createProblem_03(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        int e = MathUtility.getRandomNumber_1Digit();
        int Result = Math.abs((a + b * c)) - Math.abs(d + e);

        QUESTION = "|" + String.valueOf(a) + " + " + String.valueOf(b) + " x " + String.valueOf(c) + "| - |"
                + String.valueOf(d) + " + " + String.valueOf(e) + "| = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((Result + 2));
        ANSWER_C = String.valueOf((Result + 1));
        ANSWER_D = String.valueOf((Result - 1));
    }

    //  A + ||B - C| - |D + E||
    private void createProblem_04(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        int e = MathUtility.getRandomNumber_1Digit();
        int Result = a + Math.abs(Math.abs(b - c) - Math.abs(d + e));

        QUESTION = String.valueOf(a) + " + ||" + String.valueOf(b) + " - " + String.valueOf(c) + "|"
                + " - |" + String.valueOf(d) + " + " + String.valueOf(e) + "|| = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((Result + 2));
        ANSWER_C = String.valueOf((Result + 1));
        ANSWER_D = String.valueOf((Result - 1));
    }

    //  |A + |B + C|| - |D - E|
    private void createProblem_05(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        int e = MathUtility.getRandomNumber_1Digit();
        int Result = Math.abs(a + Math.abs(b + c)) - Math.abs(d - e);

        QUESTION = "|" + String.valueOf(a) + " + |" + String.valueOf(b) + " + " + String.valueOf(c) + "||"
                + " - |" + String.valueOf(d) + " - " + String.valueOf(e) + "| = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((Result + 2));
        ANSWER_C = String.valueOf((Result + 1));
        ANSWER_D = String.valueOf((Result - 1));
    }

    //  ||A + B| - |C + D|| - |E|
    private void createProblem_06(){
        int a = MathUtility.getRandomNumber_1Digit();
        int b = MathUtility.getRandomNumber_1Digit();
        int c = MathUtility.getRandomNumber_1Digit();
        int d = MathUtility.getRandomNumber_1Digit();
        int e = MathUtility.getRandomNumber_1Digit();

        int Result = Math.abs(Math.abs(a + b) - Math.abs(c + d)) - Math.abs(e);

        QUESTION = "||" + String.valueOf(a) + " + " + String.valueOf(b) + "| - |" + String.valueOf(c) + " + " + String.valueOf(d) + "||"
                + " - |" + String.valueOf(e) + "| = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((Result + 2));
        ANSWER_C = String.valueOf((Result + 1));
        ANSWER_D = String.valueOf((Result - 1));
    }

    //  -|A|
    private void createProblem_07(){
        int a = MathUtility.getRandomNumber_1Digit();
        int Result = Math.abs(a) * -1;

        QUESTION = "- |" + String.valueOf(a) + "| = ?";
        RIGHT_ANSWER = String.valueOf(Result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf((Result + 2));
        ANSWER_C = String.valueOf((Result + 1));
        ANSWER_D = String.valueOf((Result - 1));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
