package pckMath;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.Variable;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;

/**
 * Created by Cong on 4/26/2017.
 */

public class MathUtility {

    private static int NUMBER_RANGE = 100000;
    private static String CHARACTER_LIST = "abcdefghijklmnopqrstuvwxyz";
    private static String VARIABLE_LIST = "abcdyzt";

    public static String convertToWord(int Number){
        String NumberInWord = "";

        switch (Number){
            case 0:
                NumberInWord = "zero";
                break;
            case 1:
                NumberInWord = "one";
                break;
            case 2:
                NumberInWord = "two";
                break;
            case 3:
                NumberInWord = "three";
                break;
            case 4:
                NumberInWord = "four";
                break;
            case 5:
                NumberInWord = "five";
                break;
            case 6:
                NumberInWord = "six";
                break;
            case 7:
                NumberInWord = "seven";
                break;
            case 8:
                NumberInWord = "eight";
                break;
            case 9:
                NumberInWord = "nine";
                break;
            case 10:
                NumberInWord = "ten";
                break;
            case 11:
                NumberInWord = "eleven";
                break;
            case 12:
                NumberInWord = "twelve";
                break;
            case 13:
                NumberInWord = "thirteen";
                break;
            case 14:
                NumberInWord = "fourteen";
                break;
            case 15:
                NumberInWord = "fifteen";
                break;
            case 16:
                NumberInWord = "sixteen";
                break;
            case 17:
                NumberInWord = "seventeen";
                break;
            case 18:
                NumberInWord = "eighteen";
                break;
            case 19:
                NumberInWord = "nineteen";
                break;
            case 20:
                NumberInWord = "twenty";
                break;
        }

        return NumberInWord.toUpperCase();
    }

    public static String getID(){
        DateFormat CurrentDateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
        return CurrentDateFormat.format(new Date());
    }

    public static boolean getRandomBooleanValue(){
        boolean RandomValue;
        if (getRandomPositiveNumber_1Digit() % 2 == 0){
            RandomValue = true;
        }
        else {
            RandomValue = false;
        }

        return RandomValue;
    }

    public static boolean isSquaredNumber(int Number){
        boolean result = false;
        for (int i = 0; i <= Number; i++){
            if (i * i == Number){
                result = true;
                break;
            }
            else {
                if (i * i > Number){
                    result = false;
                    break;
                }
            }
        }

        return result;
    }

    public static int getRandomPrimeNumber(){
        int PrimeNumber;
        
        switch (MathUtility.getRandomPositiveNumber_1Digit() % 5){
            case 0:
                PrimeNumber = 2;
                break;
            case 1:
                PrimeNumber = 3;
                break;
            case 2:
                PrimeNumber = 5;
                break;
            case 3:
                PrimeNumber = 7;
                break;
            case 4:
                PrimeNumber = 11;
                break;
            default:
                PrimeNumber = 2;
                break;
        }
        
        return PrimeNumber;
    }
    
    public static int getRandomPositiveNumber_1Digit(){
        return (new Random()).nextInt(NUMBER_RANGE) % 10 + 1;
    }

    public static int getRandomPositiveNumber_2Digit(){
        return (new Random()).nextInt(NUMBER_RANGE) % 90 + 10;
    }

    public static int getRandomPositiveNumber_3Digit(){
        return (new Random()).nextInt(NUMBER_RANGE) % 900 + 100;
    }

    public static int getRandomPositiveNumber_4Digit(){
        return (new Random()).nextInt(NUMBER_RANGE) % 9000 + 1000;
    }

    public static int getRandomPositiveNumber(){
        int random_number = getRandomPositiveNumber_1Digit();

        if (getRandomPositiveNumber_1Digit() % 2 == 0){
            random_number = getRandomPositiveNumber_2Digit();
        }
        else {
            if (getRandomPositiveNumber_1Digit() % 2 == 0){
                random_number = getRandomPositiveNumber_3Digit();
            }
            else {
                if (getRandomPositiveNumber_1Digit() % 2 == 0){
                    random_number = getRandomPositiveNumber_4Digit();
                }
            }
        }

        return random_number;
    }

    public static int getRandomNegativeNumber_1Digit(){
        return ((new Random()).nextInt(NUMBER_RANGE) % 10 + 1) * -1;
    }

    public static int getRandomNegativeNumber_2Digit(){
        return ((new Random()).nextInt(NUMBER_RANGE) % 90 + 10) * -1;
    }

    public static int getRandomNegativeNumber_3Digit(){
        return ((new Random()).nextInt(NUMBER_RANGE) % 900 + 100) * -1;
    }

    public static int getRandomNegativeNumber_4Digit(){
        return ((new Random()).nextInt(NUMBER_RANGE) % 9000 + 1000) * -1;
    }

    public static int getRandomNegativeNumber(){
        int random_number = getRandomNegativeNumber_1Digit();

        if (getRandomNegativeNumber_1Digit() % 2 == 0){
            random_number = getRandomNegativeNumber_2Digit();
        }
        else {
            if (getRandomNegativeNumber_1Digit() % 2 == 0){
                random_number = getRandomNegativeNumber_3Digit();
            }
            else {
                if (getRandomNegativeNumber_1Digit() % 2 == 0){
                    random_number = getRandomNegativeNumber_4Digit();
                }
            }
        }

        return random_number;
    }

    public static int getRandomNumber_1Digit(){
        int random_number = getRandomPositiveNumber_1Digit();

        if (getRandomPositiveNumber_1Digit() % 2 == 0){
            random_number = random_number * -1;
        }

        return random_number;
    }

    public static int getRandomNumber_2Digit(){
        int random_number = getRandomPositiveNumber_2Digit();

        if (getRandomPositiveNumber_1Digit() % 2 == 0){
            random_number = random_number * -1;
        }

        return random_number;
    }

    public static int getRandomNumber_3Digit(){
        int random_number = getRandomPositiveNumber_3Digit();

        if (getRandomPositiveNumber_1Digit() % 2 == 0){
            random_number = random_number * -1;
        }

        return random_number;
    }

    public static int getRandomNumber_4Digit(){
        int random_number = getRandomPositiveNumber_4Digit();

        if (getRandomPositiveNumber_1Digit() % 2 == 0){
            random_number = random_number * -1;
        }

        return random_number;
    }

    public static String getRandomCharacter(){
        return String.valueOf(CHARACTER_LIST.charAt(getRandomPositiveNumber_2Digit() % CHARACTER_LIST.length()));
    }

    public static String getVariable(){
        return String.valueOf(VARIABLE_LIST.charAt(getRandomPositiveNumber_2Digit() % VARIABLE_LIST.length()));
    }

    public static double round_0_Decimal(double NUMBER){
        return Math.round(NUMBER * 1.0) / 1.0;
    }

    public static double round_1_Decimal(double NUMBER){
        return Math.round(NUMBER * 10.0) / 10.0;
    }

    public static double round_2_Decimal(double NUMBER){
        return Math.round(NUMBER * 100.0) / 100.0;
    }

    public static double round_3_Decimal(double NUMBER){
        return Math.round(NUMBER * 1000.0) / 1000.0;
    }

    public static double round_4_Decimal(double NUMBER){
        return Math.round(NUMBER * 10000.0) / 10000.0;
    }

    public static double round_5_Decimal(double NUMBER){
        return Math.round(NUMBER * 100000.0) / 100000.0;
    }

    public static double round_Decimal(double NUMBER){
        double rounded_number = 0.0;

        if (getRandomPositiveNumber() % 2 == 0) {
            rounded_number = round_1_Decimal(NUMBER);
        }
        else {
            if (getRandomPositiveNumber() % 2 == 0) {
                rounded_number = round_2_Decimal(NUMBER);
            } else {
                if (getRandomPositiveNumber() % 2 == 0) {
                    rounded_number = round_3_Decimal(NUMBER);
                } else {
                    rounded_number = round_4_Decimal(NUMBER);
                }
            }
        }

        return rounded_number;
    }

    public static double getDecimalNumber_1Digit(int Number){
        double random_decimal = (double) Number;
        random_decimal = random_decimal / 10.0;
        random_decimal = Math.round(random_decimal * 10.0) / 10.0;
        return random_decimal;
    }

    public static double getDecimalNumber_2Digit(int Number){
        double random_decimal = (double) Number;
        random_decimal = random_decimal / 100.0;
        random_decimal = Math.round(random_decimal * 100.0) / 100.0;
        return random_decimal;
    }

    public static double getDecimalNumber_3Digit(int Number){
        double random_decimal = (double) Number;
        random_decimal = random_decimal / 1000.0;
        random_decimal = Math.round(random_decimal * 1000.0) / 1000.0;
        return random_decimal;
    }

    public static double getDecimalNumber_4Digit(int Number){
        double random_decimal = (double) Number;
        random_decimal = random_decimal / 10000.0;
        random_decimal = Math.round(random_decimal * 1000.0) / 1000.0;
        return random_decimal;
    }

    public static double getRandomDecimalNumber_1Digit(){
        double random_decimal = (double) getRandomPositiveNumber();
        random_decimal = random_decimal / 10.0;
        random_decimal = Math.round(random_decimal * 10.0) / 10.0;
        return random_decimal;
    }

    public static double getRandomDecimalNumber_2Digit(){
        double random_decimal = (double) getRandomPositiveNumber();
        random_decimal = random_decimal / 100.0;
        random_decimal = Math.round(random_decimal * 100.0) / 100.0;
        return random_decimal;
    }

    public static double getRandomDecimalNumber_3Digit(){
        double random_decimal = (double) getRandomPositiveNumber();
        random_decimal = random_decimal / 1000.0;
        random_decimal = Math.round(random_decimal * 1000.0) / 1000.0;
        return random_decimal;
    }

    public static double getRandomDecimalNumber_4Digit(){
        double random_decimal = (double) getRandomPositiveNumber();
        random_decimal = random_decimal / 10000.0;
        random_decimal = Math.round(random_decimal * 10000.0) / 10000.0;
        return random_decimal;
    }

    public static double getRandomDecimalNumber(){
        double random_decimal;

        switch (getRandomPositiveNumber_4Digit() % 4){
            case 0:
                random_decimal = getRandomDecimalNumber_1Digit();
                break;
            case 1:
                random_decimal = getRandomDecimalNumber_2Digit();
                break;
            case 2:
                random_decimal = getRandomDecimalNumber_3Digit();
                break;
            case 3:
                random_decimal = getRandomDecimalNumber_4Digit();
                break;
            default:
                random_decimal = getRandomDecimalNumber_1Digit();
                break;
        }

        return random_decimal;
    }

    public static double getSmallerNumber(double a, double b){
        double BiggerNumber;
        if (a < b){
            BiggerNumber = a;
        }
        else {
            BiggerNumber = b;
        }

        return BiggerNumber;
    }

    public static int getSmallestNumber(ArrayList<Integer> NumberList){
        int SmallestNumber = NumberList.get(0);
        int size = NumberList.size();
        for (int i = 1; i < size; i++){
            if (NumberList.get(i) < SmallestNumber){
                SmallestNumber = NumberList.get(i);
            }
        }

        return SmallestNumber;
    }

    public static Fraction getSmallerFraction(Fraction a, Fraction b){
        Fraction BiggerNumber;
        if (a.getValue() < b.getValue()){
            BiggerNumber = a;
        }
        else {
            BiggerNumber = b;
        }

        return BiggerNumber;
    }

    public static double getBiggerNumber(double a, double b){
        double BiggerNumber;
        if (a > b){
            BiggerNumber = a;
        }
        else {
            BiggerNumber = b;
        }

        return BiggerNumber;
    }

    public static int getBiggestNumber(ArrayList<Integer> NumberList){
        int BiggestNumber = NumberList.get(0);
        int size = NumberList.size();
        for (int i = 1; i < size; i++){
            if (NumberList.get(i) > BiggestNumber){
                BiggestNumber = NumberList.get(i);
            }
        }

        return BiggestNumber;
    }

    public static Fraction getBiggerFraction(Fraction a, Fraction b){
        Fraction BiggerNumber;
        if (a.getValue() > b.getValue()){
            BiggerNumber = a;
        }
        else {
            BiggerNumber = b;
        }

        return BiggerNumber;
    }

    public static int findGCF(int a, int b){
        int TempA = Math.abs(a), TempB = Math.abs(b);
        int GCF = 1;

        int smaller_number = TempA;
        if (TempA > TempB){
            smaller_number = TempB;
        }

        for (int i = smaller_number; i >= 2; i--){
            if (TempA % i == 0 && TempB % i == 0){
                GCF = i;
                break;
            }
        }

        return GCF;
    }

    public static int findLCM(int a, int b){
        int LCM = Math.abs(a * b);
        int TempA = Math.abs(a);
        int TempB = Math.abs(b);

        int smaller_number, bigger_number;
        if (TempA > TempB){
            smaller_number = TempB;
            bigger_number = TempA;
        }
        else {
            smaller_number = TempA;
            bigger_number = TempB;
        }

        for (int i = 1; i <= smaller_number; i++){
            if (bigger_number * i % smaller_number == 0){
                LCM = bigger_number * i;
                break;
            }
        }

        return LCM;
    }

    public static ArrayList<Integer> findAllFactor(int NUMBER){
        ArrayList<Integer> FactorList = new ArrayList<>();

        for (int i = 1; i <= NUMBER; i++){
            if (NUMBER % i == 0){
                FactorList.add(i);
            }
        }

        return FactorList;
    }

    public static ArrayList<Integer> findAllPrimeFactor(int NUMBER){
        ArrayList<Integer> FactorList = new ArrayList<>();
        int temp = NUMBER;

        for (int i = 2; i <= temp; i++){
            if (temp % i == 0){
                FactorList.add(i);
                temp = temp / i;
                i = 1;
            }
        }

        return FactorList;
    }

    public static int getRandomAngleLessThan90Degree(){
        return getRandomPositiveNumber_4Digit() % 75 + 10;
    }

    public static int getRandomAngleGreaterThan90Degree(){
        return getRandomPositiveNumber_4Digit() % 70 + 100;
    }

    public static int getRandomAngle(){
        int Angle = getRandomAngleLessThan90Degree();
        if (getRandomPositiveNumber_1Digit() % 2 == 0){
            Angle = getRandomAngleGreaterThan90Degree();
        }

        return Angle;
    }

    private static ArrayList<Integer> getDataList_1Digit(){
        ArrayList<Integer> DataList = new ArrayList<>();

        int NoData = 6;
        for (int i = 0; i < NoData; i++){
            DataList.add(getRandomPositiveNumber_1Digit());
        }

        return DataList;
    }

    private static ArrayList<Integer> getDataList_2Digit(){
        ArrayList<Integer> DataList = new ArrayList<>();

        int NoData = 5;
        for (int i = 0; i < NoData; i++){
            DataList.add(getRandomPositiveNumber_2Digit());
        }

        return DataList;
    }

    public static ArrayList<Integer> getDataList_10Element(){
        ArrayList<Integer> DataList = new ArrayList<>();

        int NoData = 10;
        for (int i = 0; i < NoData; i++){
            if (getRandomPositiveNumber_1Digit() % 2 == 0) {
                DataList.add(getRandomPositiveNumber_2Digit());
            }
            else {
                DataList.add(getRandomPositiveNumber_1Digit());
            }
        }

        return DataList;
    }

    public static ArrayList<Integer> getDataList(){
        ArrayList<Integer> DataList = getDataList_1Digit();
        if (getRandomPositiveNumber_1Digit() % 2 == 0){
            DataList = getDataList_2Digit();
        }

        return DataList;
    }

    public static ArrayList<Integer> getDataListWithMode(){
        ArrayList<Integer> DataList = new ArrayList<>();

        int Mode = getRandomPositiveNumber_1Digit();
        DataList.add(Mode);
        DataList.add(Mode);
        DataList.add(Mode);
        DataList.add(Mode);

        int NoData = 7;
        for (int i = 0; i < NoData - 4; i++){
            DataList.add(getRandomPositiveNumber_1Digit());
        }

        return swapDataList(DataList);
    }

    public static ArrayList<Integer> swapDataList(ArrayList<Integer> DataList){
        int size = DataList.size(), swap_time = getRandomPositiveNumber_2Digit();
        for (int i = 0; i < swap_time; i++){
            int swap_index = getRandomPositiveNumber_2Digit() % size;
            int temp = DataList.get(swap_index);
            DataList.remove(swap_index);
            DataList.add(0, temp);
        }

        return DataList;
    }

    public static int getSumOfData(ArrayList<Integer> DataList){
        int SUM = 0;
        int size = DataList.size();
        for (int i = 0; i < size; i++){
            SUM += DataList.get(i);
        }

        return SUM;
    }

    public static int findGreatestNumber(ArrayList<Integer> ListOfNumber){
        int result = ListOfNumber.get(0);

        int size = ListOfNumber.size();
        for (int i = 0; i < size; i++){
            if (ListOfNumber.get(i) > result){
                result = ListOfNumber.get(i);
            }
        }

        return result;
    }

    public static int findSmallestNumber(ArrayList<Integer> ListOfNumber){
        int result = ListOfNumber.get(0);

        int size = ListOfNumber.size();
        for (int i = 0; i < size; i++){
            if (ListOfNumber.get(i) < result){
                result = ListOfNumber.get(i);
            }
        }

        return result;
    }

    public static ArrayList<Integer> getOrderedList(ArrayList<Integer> ListOfNumber){
        ArrayList<Integer> temp = ListOfNumber;

        int size = ListOfNumber.size();
        for (int i = 0; i < size; i++){
            for (int j = size - 1; j > i; j--){
                if (temp.get(j) < temp.get(j - 1)){
                    int sample_1 = temp.get(j - 1);
                    int sample_2 = temp.get(j);

                    temp.remove(j);
                    temp.remove(j - 1);

                    temp.add(j - 1, sample_2);
                    temp.add(j, sample_1);
                }
            }
        }

        return temp;
    }

    public static int getFactorial(int Number){
        int result = 1;

        for (int i = 1; i <= Number; i++){
            result = result * i;
        }

        return result;
    }

    public static Variable[] convertToArray(ArrayList<Variable> VariableList){
        int size = VariableList.size();
        Variable[] ArrayOfVariable = new Variable[size];
        for (int i = 0; i < size; i++){
            ArrayOfVariable[i] = VariableList.get(i);
        }

        return ArrayOfVariable;
    }

    public static ArrayList<Variable> convertToArrayList(Variable[] ArrayOfVariable){
        int size = ArrayOfVariable.length;
        ArrayList<Variable> ArrayListOfVariable = new ArrayList<>();
        for (int i = 0; i < size; i++){
            ArrayListOfVariable.add(ArrayOfVariable[i]);
        }

        return ArrayListOfVariable;
    }

    public static AlgebraicNumber[] convertToAlgebraicNumberArray(ArrayList<AlgebraicNumber> NumberList){
        int size = NumberList.size();
        AlgebraicNumber[] ArrayOfNumber = new AlgebraicNumber[size];
        for (int i = 0; i < size; i++){
            ArrayOfNumber[i] = NumberList.get(i);
        }

        return ArrayOfNumber;
    }

    public static ArrayList<AlgebraicNumber> convertToAlgebraicNumberArrayList(AlgebraicNumber[] ArrayOfNumber){
        int size = ArrayOfNumber.length;
        ArrayList<AlgebraicNumber> ArrayListOfAlgebraicNumber = new ArrayList<>();
        for (int i = 0; i < size; i++){
            ArrayListOfAlgebraicNumber.add(ArrayOfNumber[i]);
        }

        return ArrayListOfAlgebraicNumber;
    }

    public static int[] convertToIntegerArray(ArrayList<Integer> IntegerList){
        int size = IntegerList.size();
        int[] ArrayOfInteger = new int[size];
        for (int i = 0; i < size; i++){
            ArrayOfInteger[i] = IntegerList.get(i);
        }

        return ArrayOfInteger;
    }

    public static ArrayList<Integer> convertToIntegerArrayList(int[] ArrayOfInteger){
        int size = ArrayOfInteger.length;
        ArrayList<Integer> ArrayListOfInteger = new ArrayList<>();
        for (int i = 0; i < size; i++){
            ArrayListOfInteger.add(ArrayOfInteger[i]);
        }

        return ArrayListOfInteger;
    }

    public static double toRadian(double Degree){
        return Degree * Math.PI / 180.0;
    }

    public static double toDegree(double Radian){
        return Radian * 180.0 / Math.PI;
    }

}
