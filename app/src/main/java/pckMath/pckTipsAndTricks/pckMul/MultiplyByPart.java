package pckMath.pckTipsAndTricks.pckMul;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 9/29/2017.
 */

public class MultiplyByPart extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public MultiplyByPart(){
        createProblem();
        swapAnswer();
    }

    public MultiplyByPart(String Question,
                            String RightAnswer,
                            String AnswerA,
                            String AnswerB,
                            String AnswerC,
                            String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 1;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            default:
                createProblem_01();
                break;
        }
        return new MultiplyByPart(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() % 9 + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 9 + 1;
        int c = MathUtility.getRandomPositiveNumber_1Digit() % 9 + 1;
        int d = MathUtility.getRandomPositiveNumber_1Digit() % 9 + 1;
        
        int FirstNumber = a * 10 + b;
        int SecondNumber = c * 10 + d;
        
        QUESTION = String.valueOf(FirstNumber) + " x " + String.valueOf(SecondNumber) + " = ?";
        RIGHT_ANSWER = String.valueOf(FirstNumber) + " x (" + String.valueOf(c * 10) + " + " + String.valueOf(d) + ") <br>= " 
                + String.valueOf(FirstNumber) + " x " + String.valueOf(c * 10) + " + " + String.valueOf(FirstNumber) + " x " + String.valueOf(d) + " <br>= "
                + String.valueOf(FirstNumber) + " x " + String.valueOf(c * 10) + " + (" + String.valueOf(a * 10) + " + " + String.valueOf(b) + ") x " + String.valueOf(d) + " <br>= "
                + String.valueOf(FirstNumber) + " x " + String.valueOf(c * 10) + " + " + String.valueOf(a * 10) + " x " + String.valueOf(d) + " + " + String.valueOf(b) + " x " + String.valueOf(d) + " <br>= " 
                + String.valueOf(FirstNumber * c + 10) + " + " + String.valueOf(a * 10 * d) + " + " + String.valueOf(b * d) + " <br>= "
                + String.valueOf(FirstNumber * SecondNumber) + "<br><br>";
        
        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(FirstNumber) + " x (" + String.valueOf(c * 10 - 1) + " + " + String.valueOf(d + 1) + ") <br>= "
                + String.valueOf(FirstNumber) + " x " + String.valueOf(c * 10) + " + " + String.valueOf(FirstNumber) + " x " + String.valueOf(d) + " <br>= "
                + String.valueOf(FirstNumber) + " x " + String.valueOf(c * 10) + " + (" + String.valueOf(a * 10) + " + " + String.valueOf(b) + ") x " + String.valueOf(d) + " <br>= "
                + String.valueOf(FirstNumber) + " x " + String.valueOf(c * 10) + " + " + String.valueOf(a * 10) + " x " + String.valueOf(d) + " + " + String.valueOf(b) + " x " + String.valueOf(d) + " <br>= "
                + String.valueOf(FirstNumber * c * 10) + " + " + String.valueOf(a * 10 * d) + " + " + String.valueOf(b * d) + " <br>= "
                + String.valueOf(FirstNumber * SecondNumber) + "<br><br>";
        ANSWER_C = String.valueOf(FirstNumber) + " x (" + String.valueOf(c * 10) + " + " + String.valueOf(d) + ") <br>= "
                + String.valueOf(FirstNumber) + " x " + String.valueOf(c * 10) + " + " + String.valueOf(FirstNumber) + " x " + String.valueOf(d) + " <br>= "
                + String.valueOf(FirstNumber) + " x " + String.valueOf(c * 10) + " + (" + String.valueOf(a * 10 - 1) + " + " + String.valueOf(b + 1) + ") x " + String.valueOf(d) + " <br>= "
                + String.valueOf(FirstNumber) + " x " + String.valueOf(c * 10) + " + " + String.valueOf(a * 10) + " x " + String.valueOf(d) + " + " + String.valueOf(b) + " x " + String.valueOf(d) + " <br>= "
                + String.valueOf(FirstNumber * c * 10) + " + " + String.valueOf(a * 10 * d) + " + " + String.valueOf(b * d) + " <br>= "
                + String.valueOf(FirstNumber * SecondNumber) + "<br><br>";
        ANSWER_D = String.valueOf(FirstNumber) + " x (" + String.valueOf(c * 10 + 2) + " + " + String.valueOf(d - 2) + ") <br>= "
                + String.valueOf(FirstNumber) + " x " + String.valueOf(c * 10) + " + " + String.valueOf(FirstNumber) + " x " + String.valueOf(d) + " <br>= "
                + String.valueOf(FirstNumber) + " x " + String.valueOf(c * 10) + " + (" + String.valueOf(a * 10) + " + " + String.valueOf(b) + ") x " + String.valueOf(d) + " <br>= "
                + String.valueOf(FirstNumber) + " x " + String.valueOf(c * 10) + " + " + String.valueOf(a * 10) + " x " + String.valueOf(d) + " + " + String.valueOf(b) + " x " + String.valueOf(d) + " <br>= "
                + String.valueOf(FirstNumber * c * 10) + " + " + String.valueOf(a * 10 * d) + " + " + String.valueOf(b * d) + " <br>= "
                + String.valueOf(FirstNumber * SecondNumber) + "<br><br>";
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
