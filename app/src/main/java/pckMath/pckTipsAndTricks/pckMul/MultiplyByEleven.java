package pckMath.pckTipsAndTricks.pckMul;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 9/28/2017.
 */

public class MultiplyByEleven extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public MultiplyByEleven(){
        createProblem();
        swapAnswer();
    }

    public MultiplyByEleven(String Question,
                         String RightAnswer,
                         String AnswerA,
                         String AnswerB,
                         String AnswerC,
                         String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 3;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            default:
                createProblem_01();
                break;
        }
        return new MultiplyByEleven(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() % 9 + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 9 + 1;

        int FirstNumber = a * 10 + b;
        int result = FirstNumber * 11;

        QUESTION = String.valueOf(FirstNumber) + " x 11 = ?";
        RIGHT_ANSWER = String.valueOf(a) + "<i> (" + String.valueOf(a) + " + " + String.valueOf(b) + ") </i>" + String.valueOf(b) + " = " + String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a) + "<i> (" + String.valueOf(a + 1) + " + " + String.valueOf(b - 1) + ") </i>" + String.valueOf(b) + " = " + String.valueOf(result);
        ANSWER_C = String.valueOf(a) + "<i> (" + String.valueOf(a - 1) + " + " + String.valueOf(b + 1) + ") </i>" + String.valueOf(b) + " = " + String.valueOf(result);
        ANSWER_D = String.valueOf(a) + "<i> (" + String.valueOf(a - 2) + " + " + String.valueOf(b + 2) + ") </i>" + String.valueOf(b) + " = " + String.valueOf(result);
    }

    private void createProblem_02(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() % 9 + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 9 + 1;
        int c = MathUtility.getRandomPositiveNumber_1Digit() % 9 + 1;

        int FirstNumber = a * 100 + b * 10 + c;
        int result = FirstNumber * 11;

        QUESTION = String.valueOf(FirstNumber) + " x 11 = ?";
        RIGHT_ANSWER = String.valueOf(a)
                + "<i> (" + String.valueOf(a) + " + " + String.valueOf(b) + ")"
                + "(" + String.valueOf(b) + " + " + String.valueOf(c) + ") </i>"
                + String.valueOf(b) + " = " + String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a)
                + "<i> (" + String.valueOf(a - 1) + " + " + String.valueOf(b + 1) + ")"
                + "(" + String.valueOf(b + 2) + " + " + String.valueOf(c - 2) + ") </i>"
                + String.valueOf(b) + " = " + String.valueOf(result);
        ANSWER_C = String.valueOf(a)
                + "<i> (" + String.valueOf(a - 2) + " + " + String.valueOf(b + 2) + ")"
                + "(" + String.valueOf(b - 1) + " + " + String.valueOf(c + 1) + ") </i>"
                + String.valueOf(b) + " = " + String.valueOf(result);
        ANSWER_D = String.valueOf(a)
                + "<i> (" + String.valueOf(a - 2) + " + " + String.valueOf(b + 1) + ")"
                + "(" + String.valueOf(b - 1) + " + " + String.valueOf(c + 2) + ") </i>"
                + String.valueOf(b) + " = " + String.valueOf(result);
    }

    private void createProblem_03(){
        int a = MathUtility.getRandomPositiveNumber_1Digit() % 9 + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() % 9 + 1;
        int c = MathUtility.getRandomPositiveNumber_1Digit() % 9 + 1;
        int d = MathUtility.getRandomPositiveNumber_1Digit() % 9 + 1;

        int FirstNumber = a * 1000 + b * 100 + c * 10 + d;
        int result = FirstNumber * 11;

        QUESTION = String.valueOf(FirstNumber) + " x 11 = ?";
        RIGHT_ANSWER = String.valueOf(a)
                + "<i> (" + String.valueOf(a) + " + " + String.valueOf(b) + ")"
                + "(" + String.valueOf(b) + " + " + String.valueOf(c) + ")"
                + "(" + String.valueOf(c) + " + " + String.valueOf(d) + ") </i>"
                + String.valueOf(b) + " = " + String.valueOf(result);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a)
                + "<i> (" + String.valueOf(a + 1) + " + " + String.valueOf(b - 1) + ")"
                + "(" + String.valueOf(b - 1) + " + " + String.valueOf(c - 2) + ")"
                + "(" + String.valueOf(c + 2) + " + " + String.valueOf(d) + ") </i>"
                + String.valueOf(b) + " = " + String.valueOf(result);
        ANSWER_C = String.valueOf(a)
                + "<i> (" + String.valueOf(a) + " + " + String.valueOf(b) + ")"
                + "(" + String.valueOf(b - 1) + " + " + String.valueOf(c + 1) + ")"
                + "(" + String.valueOf(c) + " + " + String.valueOf(d) + ") </i>"
                + String.valueOf(b) + " = " + String.valueOf(result);
        ANSWER_D = String.valueOf(a)
                + "<i> (" + String.valueOf(a) + " + " + String.valueOf(b) + ")"
                + "(" + String.valueOf(b) + " + " + String.valueOf(c) + ")"
                + "(" + String.valueOf(c - 2) + " + " + String.valueOf(d + 2) + ") </i>"
                + String.valueOf(b) + " = " + String.valueOf(result);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
