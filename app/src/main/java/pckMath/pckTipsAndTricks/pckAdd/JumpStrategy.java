package pckMath.pckTipsAndTricks.pckAdd;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 9/28/2017.
 */

public class JumpStrategy extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public JumpStrategy(){
        createProblem();
        swapAnswer();
    }

    public JumpStrategy(String Question,
                                            String RightAnswer,
                                            String AnswerA,
                                            String AnswerB,
                                            String AnswerC,
                                            String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 2;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            default:
                createProblem_01();
                break;
        }
        return new JumpStrategy(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = String.valueOf((a * 10 + b)) + " + " + String.valueOf(c) + " = ?";
        RIGHT_ANSWER = String.valueOf(a * 10)
                + " + " + String.valueOf(b)
                + " + " + String.valueOf(c)
                + " = " + String.valueOf((a * 10)) + " + " + String.valueOf((b + c)) + " = " + String.valueOf((a * 10 + b + c));

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a * 10 - 1)
                + " + " + String.valueOf(b + 2)
                + " + " + String.valueOf(c - 1)
                + " = " + String.valueOf((a * 10 - 1)) + " + " + String.valueOf((b + c + 1)) + " = " + String.valueOf((a * 10 + b + c));
        ANSWER_C = String.valueOf(a * 10)
                + " + " + String.valueOf(b - 1)
                + " + " + String.valueOf(c + 1)
                + " = " + String.valueOf((a * 10 + 1)) + " + " + String.valueOf((b + c - 1)) + " = " + String.valueOf((a * 10 + b + c));
        ANSWER_D = String.valueOf(a * 10)
                + " + " + String.valueOf(b + 1)
                + " + " + String.valueOf(c - 1)
                + " = " + String.valueOf((a * 10)) + " + " + String.valueOf((b + c)) + " = " + String.valueOf((a * 10 + b + c));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
