package pckMath.pckTipsAndTricks.pckAdd;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 9/28/2017.
 */

public class AimToTens extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public AimToTens(){
        createProblem();
        swapAnswer();
    }

    public AimToTens(String Question,
                    String RightAnswer,
                    String AnswerA,
                    String AnswerB,
                    String AnswerC,
                    String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 2;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            default:
                createProblem_01();
                break;
        }
        return new AimToTens(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_2Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = String.valueOf(a) + " + " + String.valueOf((b * 10 + c)) + " = ?";
        if (c > 5){
            RIGHT_ANSWER = String.valueOf(a) + " + (" + String.valueOf((b + 1) * 10) + " - " + String.valueOf(10 - c) + ") = "
                    + "(" + String.valueOf(a) + " + " + String.valueOf((b + 1) * 10) + ") - " + String.valueOf(10 - c) + " <br>= "
                    + String.valueOf(a + (b + 1) * 10) + " - " + String.valueOf(10 - c) + " = "
                    + String.valueOf(a + b * 10 + c) + "<br><br>";

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(a) + " + (" + String.valueOf((b + 1) * 10) + " - " + String.valueOf(10 - c) + ") = "
                    + "(" + String.valueOf(a - 1) + " + " + String.valueOf((b + 1) * 10 + 1) + ") - " + String.valueOf(10 - c) + " <br>= "
                    + String.valueOf(a + (b + 1) * 10) + " - " + String.valueOf(10 - c) + " = "
                    + String.valueOf(a + b * 10 + c) + "<br><br>";
            ANSWER_C = String.valueOf(a) + " + (" + String.valueOf((b + 1) * 10) + " - " + String.valueOf(10 - c) + ") = "
                    + "(" + String.valueOf(a + 1) + " + " + String.valueOf((b + 1) * 10 - 1) + ") - " + String.valueOf(10 - c) + " <br>= "
                    + String.valueOf(a + (b + 1) * 10) + " - " + String.valueOf(10 - c) + " = "
                    + String.valueOf(a + b * 10 + c) + "<br><br>";
            ANSWER_D = String.valueOf(a) + " + (" + String.valueOf((b + 1) * 10) + " - " + String.valueOf(10 - c) + ") = "
                    + "(" + String.valueOf(a) + " + " + String.valueOf((b + 1) * 10) + ") - " + String.valueOf(10 - c) + " <br>= "
                    + String.valueOf(a + (b + 1) * 10 - 1) + " - " + String.valueOf(10 - c + 1) + " = "
                    + String.valueOf(a + b * 10 + c) + "<br><br>";
        }
        else {
            RIGHT_ANSWER = String.valueOf(a) + " + (" + String.valueOf(b * 10) + " + " + String.valueOf(c) + ") <br>= "
                    + "(" + String.valueOf(a) + " + " + String.valueOf(b * 10) + ") + " + String.valueOf(c) + " <br>= "
                    + String.valueOf(a + b * 10) + " + " + String.valueOf(c) + " <br>= "
                    + String.valueOf(a + b * 10 + c) + "<br><br>";

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = String.valueOf(a) + " + (" + String.valueOf(b * 10) + " + " + String.valueOf(c) + ") <br>= "
                    + "(" + String.valueOf(a - 1) + " + " + String.valueOf(b * 10 + 1) + ") + " + String.valueOf(c) + " <br>= "
                    + String.valueOf(a + b * 10) + " + " + String.valueOf(c) + " <br>= "
                    + String.valueOf(a + b * 10 + c) + "<br><br>";
            ANSWER_C = String.valueOf(a) + " + (" + String.valueOf(b * 10) + " + " + String.valueOf(c) + ") <br>= "
                    + "(" + String.valueOf(a) + " + " + String.valueOf(b * 10) + ") + " + String.valueOf(c) + " <br>= "
                    + String.valueOf(a + b * 10 - 2) + " + " + String.valueOf(c + 2) + " <br>= "
                    + String.valueOf(a + b * 10 + c) + "<br><br>";
            ANSWER_D = String.valueOf(a) + " + (" + String.valueOf(b * 10) + " + " + String.valueOf(c) + ") <br>= "
                    + "(" + String.valueOf(a - 1) + " + " + String.valueOf(b * 10 + 2) + ") + " + String.valueOf(c - 1) + " <br>= "
                    + String.valueOf(a + b * 10) + " + " + String.valueOf(c) + " <br>= "
                    + String.valueOf(a + b * 10 + c) + "<br><br>";
        }
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
