package pckMath.pckTipsAndTricks.pckAdd;

import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 9/28/2017.
 */

public class AddUpToTen extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public AddUpToTen(){
        createProblem();
        swapAnswer();
    }

    public AddUpToTen(String Question,
                        String RightAnswer,
                        String AnswerA,
                        String AnswerB,
                        String AnswerC,
                        String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 2;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            default:
                createProblem_01();
                break;
        }
        return new AddUpToTen(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = 10 - (MathUtility.getRandomPositiveNumber_1Digit() % 8 + 1);
        int b = 10 - a;
        int c = 10 - (MathUtility.getRandomPositiveNumber_1Digit() % 8 + 1);
        int d = 10 - c;
        int e = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = String.valueOf(a) + " + " + String.valueOf(c)
                + " + " + String.valueOf(e) + " + " + String.valueOf(b) + " + " + String.valueOf(d) + " = ?" ;
        RIGHT_ANSWER = "(" + String.valueOf(a) + " + " + String.valueOf(b) + ") + "
                + "(" + String.valueOf(c) + " + " + String.valueOf(d) + ") + " + String.valueOf(e)
                + " = " + String.valueOf((a + b + c + d + e));

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "(" + String.valueOf(d) + " + " + String.valueOf(b) + ") + "
                + "(" + String.valueOf(e) + " + " + String.valueOf(c) + ") + " + String.valueOf(a)
                + " = " + String.valueOf((a + b + c + d + e));
        ANSWER_C = "(" + String.valueOf(a) + " + " + String.valueOf(c) + ") + "
                + "(" + String.valueOf(e) + " + " + String.valueOf(d) + ") + " + String.valueOf(b)
                + " = " + String.valueOf((a + b + c + d + e));
        ANSWER_D = "(" + String.valueOf(a) + " + " + String.valueOf(e) + ") + "
                + "(" + String.valueOf(c) + " + " + String.valueOf(b) + ") + " + String.valueOf(d)
                + " = " + String.valueOf((a + b + c + d + e));
    }

    private void createProblem_02(){
        int Temp = MathUtility.getRandomPositiveNumber_1Digit() * 10;
        int a = Temp - MathUtility.getRandomPositiveNumber_3Digit() % Temp;
        int b = Temp - a;

        Temp = MathUtility.getRandomPositiveNumber_1Digit() * 10;
        int c = Temp - MathUtility.getRandomPositiveNumber_3Digit() % Temp;
        int d = Temp - c;
        int e = MathUtility.getRandomPositiveNumber_1Digit();

        QUESTION = String.valueOf(a) + " + " + String.valueOf(c)
                + " + " + String.valueOf(e) + " + " + String.valueOf(b) + " + " + String.valueOf(d) + " = ?" ;
        RIGHT_ANSWER = "(" + String.valueOf(a) + " + " + String.valueOf(b) + ") + "
                + "(" + String.valueOf(c) + " + " + String.valueOf(d) + ") + " + String.valueOf(e)
                + " = " + String.valueOf((a + b + c + d + e));

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "(" + String.valueOf(d) + " + " + String.valueOf(b) + ") + "
                + "(" + String.valueOf(e) + " + " + String.valueOf(c) + ") + " + String.valueOf(a)
                + " = " + String.valueOf((a + b + c + d + e));
        ANSWER_C = "(" + String.valueOf(a) + " + " + String.valueOf(c) + ") + "
                + "(" + String.valueOf(e) + " + " + String.valueOf(d) + ") + " + String.valueOf(b)
                + " = " + String.valueOf((a + b + c + d + e));
        ANSWER_D = "(" + String.valueOf(a) + " + " + String.valueOf(e) + ") + "
                + "(" + String.valueOf(c) + " + " + String.valueOf(b) + ") + " + String.valueOf(d)
                + " = " + String.valueOf((a + b + c + d + e));
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
