package pckMath.pckTipsAndTricks.pckAlgebra;

import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticMath;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 10/1/2017.
 */

public class QuadraticEquation extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public QuadraticEquation(){
        createProblem();
        swapAnswer();
    }

    public QuadraticEquation(String Question,
                          String RightAnswer,
                          String AnswerA,
                          String AnswerB,
                          String AnswerC,
                          String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 2;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            default:
                createProblem_01();
                break;
        }
        return new QuadraticEquation(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_01(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = 0 - (a + b);

        pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation
                Equation = new pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation(a, b, c);

        QUESTION = "Solve this equation: " + Equation.toString();
        RIGHT_ANSWER = "x = 1 or x = " + FractionMath.getSimplifiedFraction(new Fraction(c, a)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = -1 or x = " + FractionMath.getSimplifiedFraction(new Fraction((0 - c), a)).toString();
        ANSWER_C = "x = 1 or x = " + FractionMath.getRandomFraction().toString();
        ANSWER_D = "x = -1 or x = " + FractionMath.getRandomFraction().toString();
    }

    private void createProblem_02(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = 0 - (a - b);

        pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation
                Equation = new pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation(a, b, c);

        QUESTION = "Solve this equation: " + Equation.toString();
        RIGHT_ANSWER = "x = -1 or x = " + FractionMath.getSimplifiedFraction(new Fraction((0 - c), a)).toString();

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = "x = 1 or x = " + FractionMath.getSimplifiedFraction(new Fraction(c, a)).toString();
        ANSWER_C = "x = 1 or x = " + FractionMath.getRandomFraction().toString();
        ANSWER_D = "x = -1 or x = " + FractionMath.getRandomFraction().toString();
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
