package pckMath.pckTipsAndTricks.pckAlgebra;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraMath;
import pckMath.pckProblemCollection.pckAlgebraOne.AlgebraicNumber;
import pckMath.pckProblemCollection.pckAlgebraOne.Variable;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.*;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckInequality.SolvingQuadraticInequality;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.Polynomial;

/**
 * Created by Cong on 10/1/2017.
 */

public class QuadraticInequality extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public QuadraticInequality(){
        createProblem();
        swapAnswer();
    }

    public QuadraticInequality(String Question,
                                      String RightAnswer,
                                      String AnswerA,
                                      String AnswerB,
                                      String AnswerC,
                                      String AnswerD){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        int NoProblem = 6;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            case 2:
                createProblem_03();
                break;
            case 3:
                createProblem_04();
                break;
            case 4:
                createProblem_05();
                break;
            case 5:
                createProblem_06();
                break;
            default:
                createProblem_01();
                break;
        }

        return new QuadraticInequality(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    //  (ax + b)(cx + d) > 0
    private void createProblem_01(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial Pol = AlgebraMath.mul(PolA, PolB);

        pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation Equation = new pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation(Pol);

        int a = Equation.getACoefficient();
        int b = Equation.getBCoefficient();
        int c = Equation.getCCoefficient();

        pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckInequality.QuadraticInequality Inequality = new pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckInequality.QuadraticInequality(a, b, c, " > ");

        String SolutionA = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(0));
        String SolutionB = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(1));

        QUESTION = Inequality.toString();
        if (a > 0) {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = "x " + InfoCollector.getElementOf() + "(" + InfoCollector.getInfinity() + ", " + SolutionA + ") "
                        + InfoCollector.getUnionSet()
                        + "(" + SolutionA + ", -" + InfoCollector.getInfinity() + ")";
            }
            else {
                RIGHT_ANSWER = "x < " + SolutionA + " or x > " + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionA + " < x < " + SolutionB;
            ANSWER_C = SolutionB + " < x < " + SolutionA;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
        else {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = InfoCollector.getNoSolution();
            }
            else {
                RIGHT_ANSWER = SolutionA + " < x < " + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionB + " < x < " + SolutionA;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
    }

    //  (ax + b)(cx + d) < 0
    private void createProblem_02(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial Pol = AlgebraMath.mul(PolA, PolB);

        pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation Equation = new pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation(Pol);

        int a = Equation.getACoefficient();
        int b = Equation.getBCoefficient();
        int c = Equation.getCCoefficient();

        pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckInequality.QuadraticInequality Inequality = new pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckInequality.QuadraticInequality(a, b, c, " < ");

        String SolutionA = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(0));
        String SolutionB = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(1));

        QUESTION = Inequality.toString();
        if (a < 0) {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = "x " + InfoCollector.getElementOf() + "(" + InfoCollector.getInfinity() + ", " + SolutionA + ") "
                        + InfoCollector.getUnionSet()
                        + "(" + SolutionA + ", -" + InfoCollector.getInfinity() + ")";
            }
            else {
                RIGHT_ANSWER = "x < " + SolutionA + " or x > " + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionA + " < x < " + SolutionB;
            ANSWER_C = SolutionB + " < x < " + SolutionA;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
        else {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = InfoCollector.getNoSolution();
            }
            else {
                RIGHT_ANSWER = SolutionA + " < x < " + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionB + " < x < " + SolutionA;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
    }

    //  (ax + b)(cx + d) >= 0
    private void createProblem_03(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial Pol = AlgebraMath.mul(PolA, PolB);

        pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation Equation = new pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation(Pol);

        int a = Equation.getACoefficient();
        int b = Equation.getBCoefficient();
        int c = Equation.getCCoefficient();

        pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckInequality.QuadraticInequality Inequality = new pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckInequality.QuadraticInequality(a, b, c, InfoCollector.getGreaterThanOrEqualSign());

        String SolutionA = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(0));
        String SolutionB = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(1));

        QUESTION = Inequality.toString();
        if (a > 0) {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = "x " + InfoCollector.getElementOf() + " (-" + InfoCollector.getInfinity() + ", " + InfoCollector.getInfinity() + ")";
            }
            else {
                RIGHT_ANSWER = "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA
                        + " or x" + InfoCollector.getGreaterThanOrEqualSign() + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA
                    + " or x" + InfoCollector.getLessThanOrEqualSign() + SolutionB;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
        else {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = InfoCollector.getNoSolution();
            }
            else {
                RIGHT_ANSWER = SolutionA + InfoCollector.getLessThanOrEqualSign() + "x" + InfoCollector.getLessThanOrEqualSign() + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionB + InfoCollector.getLessThanOrEqualSign() + "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
    }

    //  (ax + b)(cx + d) <= 0
    private void createProblem_04(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial PolB = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial Pol = AlgebraMath.mul(PolA, PolB);

        pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation Equation = new pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation(Pol);

        int a = Equation.getACoefficient();
        int b = Equation.getBCoefficient();
        int c = Equation.getCCoefficient();

        pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckInequality.QuadraticInequality Inequality = new pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckInequality.QuadraticInequality(a, b, c, InfoCollector.getLessThanOrEqualSign());

        String SolutionA = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(0));
        String SolutionB = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(1));

        QUESTION = Inequality.toString();
        if (a < 0) {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = InfoCollector.getNoSolution();
            }
            else {
                RIGHT_ANSWER = "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA
                        + " or x" + InfoCollector.getGreaterThanOrEqualSign() + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA
                    + " or x" + InfoCollector.getLessThanOrEqualSign() + SolutionB;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
        else {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = "x " + InfoCollector.getElementOf() + " (-" + InfoCollector.getInfinity() + ", " + InfoCollector.getInfinity() + ")";
            }
            else {
                RIGHT_ANSWER = SolutionA + InfoCollector.getLessThanOrEqualSign() + "x" + InfoCollector.getLessThanOrEqualSign() + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionB + InfoCollector.getLessThanOrEqualSign() + "x" + InfoCollector.getLessThanOrEqualSign() + SolutionA;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
    }

    //  (ax + b)^2 > 0
    private void createProblem_05(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial Pol = AlgebraMath.mul(PolA, PolA);

        pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation Equation = new pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation(Pol);

        int a = Equation.getACoefficient();
        int b = Equation.getBCoefficient();
        int c = Equation.getCCoefficient();

        pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckInequality.QuadraticInequality Inequality = new pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckInequality.QuadraticInequality(a, b, c, " > ");

        String SolutionA = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(0));
        String SolutionB = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(1));

        QUESTION = Inequality.toString();
        if (a > 0) {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = "x " + InfoCollector.getElementOf() + "(" + InfoCollector.getInfinity() + ", " + SolutionA + ") "
                        + InfoCollector.getUnionSet()
                        + "(" + SolutionA + ", -" + InfoCollector.getInfinity() + ")";
            }
            else {
                RIGHT_ANSWER = "x < " + SolutionA + " or x > " + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionA + " < x < " + SolutionB;
            ANSWER_C = SolutionB + " < x < " + SolutionA;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
        else {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = InfoCollector.getNoSolution();
            }
            else {
                RIGHT_ANSWER = SolutionA + " < x < " + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionB + " < x < " + SolutionA;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
    }

    //  (ax + b)^2 < 0
    private void createProblem_06(){
        Polynomial PolA = AlgebraMath.getRandomPolynomialWithHighestExponent("x", 1);
        Polynomial Pol = AlgebraMath.mul(PolA, PolA);

        pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation Equation = new pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckEquation.QuadraticEquation(Pol);

        int a = Equation.getACoefficient();
        int b = Equation.getBCoefficient();
        int c = Equation.getCCoefficient();

        pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckInequality.QuadraticInequality Inequality = new pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckQuadratic.pckInequality.QuadraticInequality(a, b, c, " < ");

        String SolutionA = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(0));
        String SolutionB = String.valueOf(QuadraticMath.getDecimalSolution(Equation).get(1));

        QUESTION = Inequality.toString();
        if (a < 0) {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = "x " + InfoCollector.getElementOf() + "(" + InfoCollector.getInfinity() + ", " + SolutionA + ") "
                        + InfoCollector.getUnionSet()
                        + "(" + SolutionA + ", -" + InfoCollector.getInfinity() + ")";
            }
            else {
                RIGHT_ANSWER = "x < " + SolutionA + " or x > " + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionA + " < x < " + SolutionB;
            ANSWER_C = SolutionB + " < x < " + SolutionA;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
        else {
            if (SolutionA.equalsIgnoreCase(SolutionB)){
                RIGHT_ANSWER = InfoCollector.getNoSolution();
            }
            else {
                RIGHT_ANSWER = SolutionA + " < x < " + SolutionB;
            }

            ANSWER_A = RIGHT_ANSWER;
            ANSWER_B = SolutionB + " < x < " + SolutionA;
            ANSWER_C = "x < " + SolutionA + " or x < " + SolutionB;
            ANSWER_D = "x > " + SolutionA + " or x < " + SolutionB;
        }
    }

    @Override
    public String getQuestion() {
        QUESTION = "Solve this inequality:<br>" + QUESTION;
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}
