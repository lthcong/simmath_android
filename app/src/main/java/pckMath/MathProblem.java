package pckMath;

/**
 * Created by Cong on 4/26/2017.
 */

public abstract class MathProblem {

    public abstract MathProblem createProblem();
    public abstract String getQuestion();
    public abstract String getRightAnswer();
    public abstract void swapAnswer();
    public abstract String getAnswerA();
    public abstract String getAnswerB();
    public abstract String getAnswerC();
    public abstract String getAnswerD();

}
