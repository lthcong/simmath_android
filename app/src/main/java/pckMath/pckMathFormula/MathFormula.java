package pckMath.pckMathFormula;

/**
 * Created by Cong on 11/26/2017.
 */

public class MathFormula {

    private String TITLE, CONTENT;

    public MathFormula(){
        TITLE = "";
        CONTENT = "";
    }

    public MathFormula(String Title, String Content){
        TITLE = Title;
        CONTENT = Content;
    }

    public void setTitle(String Title){
        TITLE = Title;
    }

    public String getTitle(){
        return TITLE;
    }

    public void setContent(String Content){
        CONTENT = Content;
    }

    public String getContent(){
        return CONTENT;
    }

}
