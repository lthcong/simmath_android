package pckMath.pckMathFormula;

import java.util.ArrayList;

import pckInfo.InfoCollector;

/**
 * Created by Cong on 11/26/2017.
 */

public class FormulaCollection {

    private static ArrayList<MathFormula> FORMULA_LIST;

    public static void setFormulaList(){
        FORMULA_LIST = new ArrayList<>();

        //  ALGEBRA
        FORMULA_LIST.add(new MathFormula("Quadratic formula",
                "x<sub>1,2</sub> = <sup> -b "
                        + InfoCollector.getPlusAndMinusSign()
                        + InfoCollector.getSquareRootSign() + "(b" + InfoCollector.putExponent(2) + " - 4ac)</sup>"
                        + "/<sub>2a<sub>."));

        FORMULA_LIST.add(new MathFormula("Distance formula",
                "P<sub>1</sub>(x<sub>1</sub>, y<sub>1</sub>), P<sub>2</sub>(x<sub>2</sub>, y<sub>2</sub>)<br>"
                        + "d(P<sub>1</sub>, P<sub>2</sub>) = "
                        + InfoCollector.getSquareRootSign() + "("
                        + "(x<sub>1</sub> - x<sub>2</sub>)" + InfoCollector.putExponent(2)
                        + " + (y<sub>1</sub> - y<sub>2</sub>)" + InfoCollector.putExponent(2)
                        + ")."));

        //  TRIGONOMETRY
        FORMULA_LIST.add(new MathFormula("Degree to radian",
                "Radian = Degree * <sup>" + InfoCollector.getPiSign() + "</sup>/<sub>180</sub>"));
        FORMULA_LIST.add(new MathFormula("Radian to degree",
                "Degree = Radian * <sup>180</sup>/<sub>"+ InfoCollector.getPiSign() + "</sub>"));

        FORMULA_LIST.add(new MathFormula("Right triangle",
                "sin(x) = <sup>Opposite</sup>/<sub>Hypotenuse</sub>."));
        FORMULA_LIST.add(new MathFormula("Right triangle",
                "cos(x) = <sup>Adjacent</sup>/<sub>Hypotenuse</sub>."));
        FORMULA_LIST.add(new MathFormula("Right triangle",
                "tan(x) = <sup>Opposite</sup>/<sub>Adjacent</sub>."));
        FORMULA_LIST.add(new MathFormula("Right triangle",
                "cot(x) = <sup>Adjacent</sup>/<sub>Opposite</sub>."));
        FORMULA_LIST.add(new MathFormula("Right triangle",
                "sec(x) = <sup>Hypotenuse</sup>/<sub>Adjacent</sub>."));
        FORMULA_LIST.add(new MathFormula("Right triangle",
                "csc(x) = <sup>Hypotenuse</sup>/<sub>Opposite</sub>."));

        FORMULA_LIST.add(new MathFormula("Tan and Cot identities",
                "tan(x) = <sup>sin(x)</sup>/<sub>cos(x)</sub>."));
        FORMULA_LIST.add(new MathFormula("Tan and Cot identities",
                "cot(x) = <sup>cos(x)</sup>/<sub>sin(x)</sub>."));

        FORMULA_LIST.add(new MathFormula("Reciprocal identities",
                "csc(x) = <sup>1</sup>/<sub>sin(x)</sub>."));
        FORMULA_LIST.add(new MathFormula("Reciprocal identities",
                "sec(x) = <sup>1</sup>/<sub>cos(x)</sub>."));
        FORMULA_LIST.add(new MathFormula("Reciprocal identities",
                "sin(x) = <sup>1</sup>/<sub>csc(x)</sub>."));
        FORMULA_LIST.add(new MathFormula("Reciprocal identities",
                "cos(x) = <sup>1</sup>/<sub>sec(x)</sub>."));
        FORMULA_LIST.add(new MathFormula("Reciprocal identities",
                "tan(x) = <sup>1</sup>/<sub>cot(x)</sub>."));
        FORMULA_LIST.add(new MathFormula("Reciprocal identities",
                "cot(x) = <sup>1</sup>/<sub>tan(x)</sub>."));

        FORMULA_LIST.add(new MathFormula("Pythagorean identities",
                "sin" + InfoCollector.putExponent(2) + "(x) + cos" + InfoCollector.putExponent(2) + "(x) = 1."));
        FORMULA_LIST.add(new MathFormula("Pythagorean identities",
                "tan" + InfoCollector.putExponent(2) + "(x) + 1 = sec" + InfoCollector.putExponent(2) + "(x)."));
        FORMULA_LIST.add(new MathFormula("Pythagorean identities",
                "cot" + InfoCollector.putExponent(2) + "(x) + 1 = csc" + InfoCollector.putExponent(2) + "(x)."));

        FORMULA_LIST.add(new MathFormula("Even/Odd formula",
                "sin(-x) = -sin(x)."));
        FORMULA_LIST.add(new MathFormula("Even/Odd formula",
                "cos(-x) = cos(x)."));
        FORMULA_LIST.add(new MathFormula("Even/Odd formula",
                "tan(-x) = -tan(x)."));
        FORMULA_LIST.add(new MathFormula("Even/Odd formula",
                "cot(-x) = -cot(x)."));
        FORMULA_LIST.add(new MathFormula("Even/Odd formula",
                "csc(-x) = -csc(x)."));
        FORMULA_LIST.add(new MathFormula("Even/Odd formula",
                "sec(-x) = cos(x)."));

        FORMULA_LIST.add(new MathFormula("Periodic formula",
                "sin(x + 2" + InfoCollector.getPiSign() + "n) = sin(x)."));
        FORMULA_LIST.add(new MathFormula("Periodic formula",
                "cos(x + 2" + InfoCollector.getPiSign() + "n) = cos(x)."));
        FORMULA_LIST.add(new MathFormula("Periodic formula",
                "sec(x + 2" + InfoCollector.getPiSign() + "n) = sec(x)."));
        FORMULA_LIST.add(new MathFormula("Periodic formula",
                "csc(x + 2" + InfoCollector.getPiSign() + "n) = csc(x)."));
        FORMULA_LIST.add(new MathFormula("Periodic formula",
                "tan(x + " + InfoCollector.getPiSign() + "n) = tan(x)."));
        FORMULA_LIST.add(new MathFormula("Periodic formula",
                "cot(x + " + InfoCollector.getPiSign() + "n) = cot(x)."));

        FORMULA_LIST.add(new MathFormula("Double angle formula",
                "sin(2x) = 2sin(x)cos(x)."));
        FORMULA_LIST.add(new MathFormula("Double angle formula",
                "cos(2x) = cos" + InfoCollector.putExponent(2) + "(x) - sin" + InfoCollector.putExponent(2) + "<br>" +
                        "= 2cos" + InfoCollector.putExponent(2) + "(x) - 1<br>" +
                        "= 1 - 2sin" + InfoCollector.putExponent(2) + "(x)."));
        FORMULA_LIST.add(new MathFormula("Double angle formula",
                "tan(2x) = <sup>2tan(x)</sup>/<sub>(1 - tan" + InfoCollector.putExponent(2) + "(x))</sub>."));

        FORMULA_LIST.add(new MathFormula("Half angle formula",
                "sin(<sup>x</sup>/<sub>2</sub>) = " + InfoCollector.getPlusAndMinusSign()
                        + InfoCollector.getSquareRootSign() + "(<sup>(1 - cos(x))</sup>/<sub>2</sub>)"));
        FORMULA_LIST.add(new MathFormula("Half angle formula",
                "cos(<sup>x</sup>/<sub>2</sub>) = " + InfoCollector.getPlusAndMinusSign()
                        + InfoCollector.getSquareRootSign() + "(<sup>(1 + cos(x))</sup>/<sub>2</sub>)"));
        FORMULA_LIST.add(new MathFormula("Half angle formula",
                "tan(<sup>x</sup>/<sub>2</sub>) = " + InfoCollector.getPlusAndMinusSign()
                        + InfoCollector.getSquareRootSign() + "(<sup>(1 - cos(x))</sup>/<sub>(1 + cos(x))</sub>)"));

        FORMULA_LIST.add(new MathFormula("Sum and difference formula",
                "sin(x " + InfoCollector.getPlusAndMinusSign() + " y) = sin(x)cos(y) " + InfoCollector.getPlusAndMinusSign() + " cos(x)sin(y)"));
        FORMULA_LIST.add(new MathFormula("Sum and difference formula",
                "cos(x " + InfoCollector.getPlusAndMinusSign() + " y) = cos(x)cos(y) " + InfoCollector.getMinusAndPlusSign() + " sin(x)sin(y)"));
        FORMULA_LIST.add(new MathFormula("Sum and difference formula",
                "tan(x " + InfoCollector.getPlusAndMinusSign() + " y) = "
                        + "<sup>tan(x) " + InfoCollector.getPlusAndMinusSign() + " tan(y)</sup>/"
                        + "<sub>1 " + InfoCollector.getMinusAndPlusSign() + " tan(x)tan(y)</sub>"));

        FORMULA_LIST.add(new MathFormula("Product to sum formula",
                "sin(x)sin(y) = <sup>1</sup>/<sub>2</sub> * [cos(x - y) - cos(x + y)]"));
        FORMULA_LIST.add(new MathFormula("Product to sum formula",
                "cos(x)cos(y) = <sup>1</sup>/<sub>2</sub> * [cos(x - y) + cos(x + y)]"));
        FORMULA_LIST.add(new MathFormula("Product to sum formula",
                "sin(x)cos(y) = <sup>1</sup>/<sub>2</sub> * [sin(x + y) + sin(x - y)]"));
        FORMULA_LIST.add(new MathFormula("Product to sum formula",
                "cos(x)sin(y) = <sup>1</sup>/<sub>2</sub> * [sin(x + y) - sin(x - y)]"));

        FORMULA_LIST.add(new MathFormula("Sum to product formula",
                "sin(x) + sin(y) = 2 * sin(<sup>(x + y)</sup>/<sub>2</sub>)cos(<sup>(x - y)</sup>/<sub>2</sub>)"));
        FORMULA_LIST.add(new MathFormula("Sum to product formula",
                "sin(x) - sin(y) = 2 * cos(<sup>(x + y)</sup>/<sub>2</sub>)sin(<sup>(x - y)</sup>/<sub>2</sub>)"));
        FORMULA_LIST.add(new MathFormula("Sum to product formula",
                "cos(x) + cos(y) = 2 * cos(<sup>(x + y)</sup>/<sub>2</sub>)cos(<sup>(x - y)</sup>/<sub>2</sub>)"));
        FORMULA_LIST.add(new MathFormula("Sum to product formula",
                "cos(x) - cos(y) = -2 * sin(<sup>(x + y)</sup>/<sub>2</sub>)sin(<sup>(x - y)</sup>/<sub>2</sub>)"));

        FORMULA_LIST.add(new MathFormula("Sin law",
                "<sup>sin(A)</sup>/<sub>a</sub> = <sup>sin(B)</sup>/<sub>b</sub> = <sup>sin(C)</sup>/<sub>c</sub>"));

        FORMULA_LIST.add(new MathFormula("Cosine law",
                "a" + InfoCollector.putExponent(2) + " = b" + InfoCollector.putExponent(2) + " + c" + InfoCollector.putExponent(2) + " - 2bc * cos(A)"));
        FORMULA_LIST.add(new MathFormula("Cosine law",
                "b" + InfoCollector.putExponent(2) + " = a" + InfoCollector.putExponent(2) + " + c" + InfoCollector.putExponent(2) + " - 2ac * cos(B)"));
        FORMULA_LIST.add(new MathFormula("Cosine law",
                "c" + InfoCollector.putExponent(2) + " = a" + InfoCollector.putExponent(2) + " + b" + InfoCollector.putExponent(2) + " - 2ab * cos(C)"));

        //  GEOMETRY
        FORMULA_LIST.add(new MathFormula("Perimeter of a triangle",
                "P = a + b + c. <br>a, b, c are three sides of triangle."));
        FORMULA_LIST.add(new MathFormula("Area of a triangle",
                "S = <sup>1</sup>/<sub>2</sub> * base * height."));

        FORMULA_LIST.add(new MathFormula("Perimeter of a rectangle",
                "P = 2 * (length + width)."));
        FORMULA_LIST.add(new MathFormula("Area of a rectangle",
                "S = length * width."));

        FORMULA_LIST.add(new MathFormula("Perimeter of a square",
                "P = side * 4."));
        FORMULA_LIST.add(new MathFormula("Area of a square",
                "S = side * side."));

        FORMULA_LIST.add(new MathFormula("Perimeter of a trapezoid",
                "P = side + base<sub>1</sub> + side + base<sub>2</sub>."));
        FORMULA_LIST.add(new MathFormula("Area of a trapezoid",
                "S = <sup>1</sup>/<sub>2</sub> * (base<sub>1</sub> + base<sub>2</sub>) * height."));

        FORMULA_LIST.add(new MathFormula("Perimeter of a parallelogram",
                "P = 2 * (length + width)."));
        FORMULA_LIST.add(new MathFormula("Area of a parallelogram",
                "S = length * height."));

        FORMULA_LIST.add(new MathFormula("Perimeter of a rhombus",
                "P = 4 * side."));
        FORMULA_LIST.add(new MathFormula("Area of a rhombus",
                "S = <sup>1</sup>/<sub>2</sub> * diagonal<sub>1</sub> * diagonal<sub>2</sub>."));

        FORMULA_LIST.add(new MathFormula("Circumference of a circle",
                "P = 2 * " + InfoCollector.getPiSign() + " * radius <br> OR " + InfoCollector.getPiSign() + " * diameter."));
        FORMULA_LIST.add(new MathFormula("Area of a circle",
                "S = " + InfoCollector.getPiSign() + " * radius * radius."));

        FORMULA_LIST.add(new MathFormula("Volume of a rectangular solid",
                "V = length * width * height."));
        FORMULA_LIST.add(new MathFormula("Surface area of a rectangular solid",
                "SA = 2 * (length * width + length * height + width * height)."));

        FORMULA_LIST.add(new MathFormula("Volume of a cube",
                "V = s" + InfoCollector.putExponent(3) + ", s is the side."));
        FORMULA_LIST.add(new MathFormula("Surface area of a cube",
                "SA = 6 * s" + InfoCollector.putExponent(2) + "<br>s is the side."));

        FORMULA_LIST.add(new MathFormula("Volume of a cylinder",
                "V = " + InfoCollector.getPiSign() + " * r" + InfoCollector.putExponent(2) + " * h <br>r is the bottom radius, h is the height."));
        FORMULA_LIST.add(new MathFormula("Surface area of a cylinder",
                "SA = 2 * " + InfoCollector.getPiSign() + " * r * (h + r)<br>r is the bottom radius, h is the height."));

        FORMULA_LIST.add(new MathFormula("Volume of a sphere",
                "V = <sup>4</sup>/<sub>3</sub> * " + InfoCollector.getPiSign() + " * r" + InfoCollector.putExponent(3) + "<br> r is the radius."));
        FORMULA_LIST.add(new MathFormula("Surface area of a sphere",
                "SA = 4 * " + InfoCollector.getPiSign() + " * r" + InfoCollector.putExponent(2) + ",<br> r is the radius."));

        FORMULA_LIST.add(new MathFormula("Volume of a right circular cone",
                "V = <sup>1</sup>/<sub>3</sub> * " + InfoCollector.getPiSign() + " * r" + InfoCollector.putExponent(2) + " * h <br>r is the radius, h is the height."));
        FORMULA_LIST.add(new MathFormula("Surface area of a sphere",
                "SA = " + InfoCollector.getPiSign() + " * r * " + InfoCollector.getSquareRootSign() + "(r" + InfoCollector.putExponent(2) + ") + h" + InfoCollector.putExponent(2) + "<br>h is the height, r is the bottom radius."));

        FORMULA_LIST.add(new MathFormula("Volume of a square/ rectangular pyramid",
                "V = <sup>1</sup>/<sub>3</sub> * length * width * height."));

        FORMULA_LIST.add(new MathFormula("Volume of a frustum cone",
                "V = <sup>1</sup>/<sub>3</sub> * " + InfoCollector.getPiSign() + " * h * (r" + InfoCollector.putExponent(2) + " + r * R + R" + InfoCollector.putExponent(2) + ")<br>h is the height, R and r is the two radius."));
        FORMULA_LIST.add(new MathFormula("Surface area of a frustum cone",
                "SA = " + InfoCollector.getPiSign() + " * s * (R + r)<br>s is the slang height, R and r is the two radius."));

        FORMULA_LIST.add(new MathFormula("Volume of a frustum pyramid",
                "V = <sup>h</sup>/<sub>3</sub> * (B<sub>1</sub> + B<sub>2</sub> ) + " + InfoCollector.getSquareRootSign()
                        + "(B<sub>1</sub> * B<sub>2</sub>)<br>h is the height, B<sub>1</sub> and B<sub>2</sub> are the area of the two bases."
        ));

    }

    public static ArrayList<MathFormula> getFormulaList(){
        return FORMULA_LIST;
    }

    public static ArrayList<MathFormula> searchFormula(String TitleOrContent){
        ArrayList<MathFormula> Result = new ArrayList<>();
        int size = FORMULA_LIST.size();

        for(int i = 0; i < size; i++){
            MathFormula SearchingFormula = FORMULA_LIST.get(i);
            if (SearchingFormula.getTitle().toLowerCase().contains(TitleOrContent.toLowerCase().trim())
                    || SearchingFormula.getContent().toLowerCase().contains(TitleOrContent.toLowerCase().trim())){
                Result.add(SearchingFormula);
            }
        }

        return Result;
    }

}
