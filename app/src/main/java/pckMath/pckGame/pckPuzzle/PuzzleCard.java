package pckMath.pckGame.pckPuzzle;

import java.util.ArrayList;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckMathTopics.*;

/**
 * Created by Cong on 5/26/2017.
 */

public class PuzzleCard {

    private ArrayList<MathCard> CardList;
    private static String PROBLEM_CARD = "PROBLEM", ANSWER_CARD = "ANSWER";
    private int NoCard;

    public PuzzleCard(){
        CardList = new ArrayList<>();
        NoCard = InfoCollector.getNoPuzzleGameCard();
        setCardList();
        swapCard();
    }

    private void setCardList(){
        for (int i = 0; i < NoCard / 2; i++){
            MathProblem Problem = TopicCollection.getTopic(InfoCollector.getPuzzleGameCurrentLevel() - 1).getProblem();
            CardList.add(new MathCard(Problem.getQuestion(), Problem.getRightAnswer(), Problem.getQuestion(), PROBLEM_CARD));
            CardList.add(new MathCard(Problem.getRightAnswer(), Problem.getRightAnswer(), Problem.getQuestion(), ANSWER_CARD));
        }
    }

    private void swapCard(){
        int SwapTime = MathUtility.getRandomPositiveNumber_2Digit();
        for (int i = 0; i < SwapTime; i++){
            int SwapIndex = MathUtility.getRandomPositiveNumber_4Digit() % NoCard;
            MathCard Temp = CardList.get(SwapIndex);
            CardList.remove(SwapIndex);
            CardList.add(0, Temp);
        }

    }

    public ArrayList<MathCard> getCardList(){
        return CardList;
    }

    public static String getProblemCard(){
        return PROBLEM_CARD;
    }

    public static String getAnswerCard(){
        return ANSWER_CARD;
    }

}
