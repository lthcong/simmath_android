package pckMath.pckGame.pckPuzzle;

/**
 * Created by Cong on 5/26/2017.
 */

public class MathCard {

    private String FRONT_SIDE, BACK_SIDE, QUESTION_SIDE;
    private String CARD_TYPE;

    public MathCard(){
        FRONT_SIDE = "";
        BACK_SIDE = "";
        QUESTION_SIDE = "";
        CARD_TYPE = "";
    }

    public MathCard(String FrontSide, String BackSide, String QuestionSide, String CardType){
        FRONT_SIDE = FrontSide;
        BACK_SIDE = BackSide;
        QUESTION_SIDE = QuestionSide;
        CARD_TYPE = CardType;
    }

    public void setFrontSide(String FrontSide){
        FRONT_SIDE = FrontSide;
    }

    public String getFrontSide(){
        return FRONT_SIDE;
    }

    public void setBackSide(String BackSide){
        BACK_SIDE = BackSide;
    }

    public String getBackSide(){
        return BACK_SIDE;
    }

    public void setQuestionSide(String QuestionSide){
        QUESTION_SIDE = QuestionSide;
    }

    public String getQuestionSide(){
        return QUESTION_SIDE;
    }
    
    public void setCardType(String CardType){
        CARD_TYPE = CardType;
    }

    public String getCardType(){
        return CARD_TYPE;
    }

}
