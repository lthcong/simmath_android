package pckMath.pckGame.pckComparison.pckNumber;

import pckMath.MathUtility;

/**
 * Created by Cong on 9/16/2017.
 */

public class ComparingNumbers extends pckMath.pckGame.pckComparison.ComparingNumbers {

    private String QUESTION;
    private int FIRST_NUMBER, SECOND_NUMBER, THIRD_NUMBER, BIGGEST_NUMBER;

    public ComparingNumbers(){
        int NoProblem = 2;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                getLargestNumber();
                break;
            case 1:
                getSmallestNumber();
                break;
            default:
                getLargestNumber();
                break;
        }
    }

    private void getLargestNumber(){
        QUESTION = "Tag the <b>largest</b> number: ";

        FIRST_NUMBER = MathUtility.getRandomPositiveNumber_1Digit();
        SECOND_NUMBER = MathUtility.getRandomPositiveNumber_1Digit();
        THIRD_NUMBER = MathUtility.getRandomPositiveNumber_1Digit();

        BIGGEST_NUMBER = (int) MathUtility.getBiggerNumber(MathUtility.getBiggerNumber(FIRST_NUMBER, SECOND_NUMBER), THIRD_NUMBER);
    }

    private void getSmallestNumber(){
        QUESTION = "Tag the <b>smallest</b> number: ";

        FIRST_NUMBER = MathUtility.getRandomPositiveNumber_1Digit();
        SECOND_NUMBER = MathUtility.getRandomPositiveNumber_1Digit();
        THIRD_NUMBER = MathUtility.getRandomPositiveNumber_1Digit();

        BIGGEST_NUMBER = (int) MathUtility.getSmallerNumber(MathUtility.getSmallerNumber(FIRST_NUMBER, SECOND_NUMBER), THIRD_NUMBER);
    }

    @Override
    public String getInstruction() {
        return QUESTION;
    }

    @Override
    public String getFirstNumber() {
        return String.valueOf(FIRST_NUMBER);
    }

    @Override
    public String getSecondNumber() {
        return String.valueOf(SECOND_NUMBER);
    }

    @Override
    public String getThirdNumber() {
        return String.valueOf(THIRD_NUMBER);
    }

    @Override
    public String getRightAnswer() {
        return String.valueOf(BIGGEST_NUMBER);
    }
}
