package pckMath.pckGame.pckComparison;

import pckInfo.InfoCollector;
import pckMath.pckGame.pckComparison.pckFraction.ComparingFraction;

/**
 * Created by Cong on 9/16/2017.
 */

public class ComparisonGame {
    
    private ComparingNumbers ComparingGame;
    
    public ComparisonGame(){
        if (InfoCollector.getNumberGameSelected()){
            ComparingGame = new pckMath.pckGame.pckComparison.pckNumber.ComparingNumbers();
        }
        else {
            ComparingGame = new ComparingFraction();
        }
    }

    public String getInstruction(){
        return ComparingGame.getInstruction();
    }

    public String getFirstNumber(){
        return ComparingGame.getFirstNumber();
    }

    public String getSecondNumber(){
        return ComparingGame.getSecondNumber();
    }

    public String getThirdNumber(){
        return ComparingGame.getThirdNumber();
    }

    public String getRightAnswer(){
        return ComparingGame.getRightAnswer();
    }
}
