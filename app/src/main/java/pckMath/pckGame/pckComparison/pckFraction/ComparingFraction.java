package pckMath.pckGame.pckComparison.pckFraction;

import pckMath.MathUtility;
import pckMath.pckGame.pckComparison.ComparingNumbers;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.FractionMath;

/**
 * Created by Cong on 9/16/2017.
 */

public class ComparingFraction extends ComparingNumbers {

    private String QUESTION;
    private Fraction FIRST_FRACTION, SECOND_FRACTION, THIRD_FRACTION, BIGGEST_FRACTION;

    public ComparingFraction(){
        int NoProblem = 2;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                getLargestFraction();
                break;
            case 1:
                getSmallestFraction();
                break;
            default:
                getLargestFraction();
                break;
        }
    }

    //  LARGEST
    private void getLargestFraction(){
        QUESTION = "Tag the <b>largest</b> fraction: ";

        int NoProblem = 4;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createFraction_01();
                break;
            case 1:
                createFraction_02();
                break;
            case 2:
                createFraction_03();
                break;
            case 3:
                createFraction_04();
                break;
            default:
                createFraction_01();
                break;
        }
    }

    private void createFraction_01(){
        FIRST_FRACTION = FractionMath.getRandomUnitFraction();
        SECOND_FRACTION = FractionMath.getRandomUnitFraction();
        THIRD_FRACTION = FractionMath.getRandomUnitFraction();

        BIGGEST_FRACTION = MathUtility.getBiggerFraction(MathUtility.getBiggerFraction(FIRST_FRACTION, SECOND_FRACTION), THIRD_FRACTION);
    }

    private void createFraction_02(){
        FIRST_FRACTION = FractionMath.getRandomImproperFraction();
        SECOND_FRACTION = FractionMath.getRandomImproperFraction();
        THIRD_FRACTION = FractionMath.getRandomImproperFraction();

        BIGGEST_FRACTION = MathUtility.getBiggerFraction(MathUtility.getBiggerFraction(FIRST_FRACTION, SECOND_FRACTION), THIRD_FRACTION);
    }

    private void createFraction_03(){
        FIRST_FRACTION = FractionMath.getRandomProperFraction();
        SECOND_FRACTION = FractionMath.getRandomProperFraction();
        THIRD_FRACTION = FractionMath.getRandomProperFraction();

        BIGGEST_FRACTION = MathUtility.getBiggerFraction(MathUtility.getBiggerFraction(FIRST_FRACTION, SECOND_FRACTION), THIRD_FRACTION);
    }

    private void createFraction_04(){
        FIRST_FRACTION = FractionMath.getRandomFraction();
        SECOND_FRACTION = FractionMath.getRandomFraction();
        THIRD_FRACTION = FractionMath.getRandomFraction();

        BIGGEST_FRACTION = MathUtility.getBiggerFraction(MathUtility.getBiggerFraction(FIRST_FRACTION, SECOND_FRACTION), THIRD_FRACTION);
    }

    //  SMALLEST
    private void getSmallestFraction(){
        QUESTION = "Tag the <b>smallest</b> fraction: ";

        int NoProblem = 4;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createFraction_05();
                break;
            case 1:
                createFraction_06();
                break;
            case 2:
                createFraction_07();
                break;
            case 3:
                createFraction_08();
                break;
            default:
                createFraction_05();
                break;
        }
    }

    private void createFraction_05(){
        FIRST_FRACTION = FractionMath.getRandomUnitFraction();
        SECOND_FRACTION = FractionMath.getRandomUnitFraction();
        THIRD_FRACTION = FractionMath.getRandomUnitFraction();

        BIGGEST_FRACTION = MathUtility.getSmallerFraction(MathUtility.getSmallerFraction(FIRST_FRACTION, SECOND_FRACTION), THIRD_FRACTION);
    }

    private void createFraction_06(){
        FIRST_FRACTION = FractionMath.getRandomImproperFraction();
        SECOND_FRACTION = FractionMath.getRandomImproperFraction();
        THIRD_FRACTION = FractionMath.getRandomImproperFraction();

        BIGGEST_FRACTION = MathUtility.getSmallerFraction(MathUtility.getSmallerFraction(FIRST_FRACTION, SECOND_FRACTION), THIRD_FRACTION);
    }

    private void createFraction_07(){
        FIRST_FRACTION = FractionMath.getRandomProperFraction();
        SECOND_FRACTION = FractionMath.getRandomProperFraction();
        THIRD_FRACTION = FractionMath.getRandomProperFraction();

        BIGGEST_FRACTION = MathUtility.getSmallerFraction(MathUtility.getSmallerFraction(FIRST_FRACTION, SECOND_FRACTION), THIRD_FRACTION);
    }

    private void createFraction_08(){
        FIRST_FRACTION = FractionMath.getRandomFraction();
        SECOND_FRACTION = FractionMath.getRandomFraction();
        THIRD_FRACTION = FractionMath.getRandomFraction();

        BIGGEST_FRACTION = MathUtility.getSmallerFraction(MathUtility.getSmallerFraction(FIRST_FRACTION, SECOND_FRACTION), THIRD_FRACTION);
    }

    @Override
    public String getInstruction() {
        return QUESTION;
    }

    @Override
    public String getFirstNumber() {
        return FIRST_FRACTION.toString();
    }

    @Override
    public String getSecondNumber() {
        return SECOND_FRACTION.toString();
    }

    @Override
    public String getThirdNumber() {
        return THIRD_FRACTION.toString();
    }

    @Override
    public String getRightAnswer() {
        return BIGGEST_FRACTION.toString();
    }
}
