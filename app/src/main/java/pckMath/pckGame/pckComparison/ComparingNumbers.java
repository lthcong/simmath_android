package pckMath.pckGame.pckComparison;

/**
 * Created by Cong on 9/16/2017.
 */

public abstract class ComparingNumbers {

    public abstract String getInstruction();

    public abstract String getFirstNumber();
    public abstract String getSecondNumber();
    public abstract String getThirdNumber();

    public abstract String getRightAnswer();

}
