package pckMath.pckGame.pckRiddle;

/**
 * Created by Cong on 5/30/2017.
 */

public abstract class MultipleChoiceQuestion {

    public abstract void createProblem();

    public abstract String getFirstNumber();
    public abstract String getSecondNumber();
    public abstract String getThirdNumber();
    public abstract String getForthNumber();
    public abstract String getFifthNumber();

    public abstract void swapAnswer();
    public abstract String getAnswerA();
    public abstract String getAnswerB();
    public abstract String getAnswerC();
    public abstract String getAnswerD();

}
