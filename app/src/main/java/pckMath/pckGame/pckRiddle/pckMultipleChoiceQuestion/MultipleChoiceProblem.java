package pckMath.pckGame.pckRiddle.pckMultipleChoiceQuestion;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckGame.pckRiddle.MultipleChoiceQuestion;

/**
 * Created by Cong on 5/30/2017.
 */

public class MultipleChoiceProblem extends MultipleChoiceQuestion{

    private String FirstNumber, SecondNumber, ThirdNumber, ForthNumber, FifthNumber,
            AnswerA, AnswerB, AnswerC, AnswerD;

    public MultipleChoiceProblem(){
        createProblem();
        swapAnswer();
    }

    @Override
    public void createProblem() {
        switch (InfoCollector.getRiddleGameLevel()){
            case 1:
                createProblem_Level_1();
                break;
            case 2:
                createProblem_Level_2();
                break;
            case 3:
                createProblem_Level_3();
                break;
            case 4:
                createProblem_Level_4();
                break;
            case 5:
                createProblem_Level_5();
                break;
            case 6:
                createProblem_Level_6();
                break;
            case 7:
                createProblem_Level_7();
                break;
            case 8:
                createProblem_Level_8();
                break;
            case 9:
                createProblem_Level_9();
                break;
            case 10:
                createProblem_Level_10();
                break;
            case 11:
                createProblem_Level_11();
                break;
            case 12:
                createProblem_Level_12();
                break;
            case 13:
                createProblem_Level_13();
                break;
            case 14:
                createProblem_Level_14();
                break;
            case 15:
                createProblem_Level_15();
                break;
            case 19:
                createProblem_Level_19();
                break;
            case 26:
                createProblem_Level_26();
                break;
            case 36:
                createProblem_Level_36();
                break;
            case 37:
                createProblem_Level_37();
                break;
            case 46:
                createProblem_Level_46();
                break;
            case 47:
                createProblem_Level_47();
                break;
            case 59:
                createProblem_Level_59();
                break;
            case 60:
                createProblem_Level_60();
                break;
            case 61:
                createProblem_Level_61();
                break;
            case 62:
                createProblem_Level_62();
                break;
            case 63:
                createProblem_Level_63();
                break;
            default:
                createRandomProblem();
                break;
        }
    }

    private void createRandomProblem() {
        int NoProblemType = 47;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblemType + 1){
            case 1:
                createProblem_Level_1();
                break;
            case 2:
                createProblem_Level_2();
                break;
            case 3:
                createProblem_Level_3();
                break;
            case 4:
                createProblem_Level_4();
                break;
            case 5:
                createProblem_Level_5();
                break;
            case 6:
                createProblem_Level_6();
                break;
            case 7:
                createProblem_Level_7();
                break;
            case 8:
                createProblem_Level_8();
                break;
            case 9:
                createProblem_Level_9();
                break;
            case 10:
                createProblem_Level_10();
                break;
            case 11:
                createProblem_Level_11();
                break;
            case 12:
                createProblem_Level_12();
                break;
            case 13:
                createProblem_Level_13();
                break;
            case 14:
                createProblem_Level_14();
                break;
            case 15:
                createProblem_Level_15();
                break;
            case 19:
                createProblem_Level_19();
                break;
            case 26:
                createProblem_Level_26();
                break;
            case 36:
                createProblem_Level_36();
                break;
            case 37:
                createProblem_Level_37();
                break;
            case 46:
                createProblem_Level_46();
                break;
            case 47:
                createProblem_Level_47();
                break;
            default:
                createProblem_Level_1();
                break;
        }
    }

    private void createProblem_Level_1(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf(x);
        SecondNumber = String.valueOf((x + 1));
        ThirdNumber = String.valueOf((x + 2));
        ForthNumber = String.valueOf((x + 3));
        FifthNumber = String.valueOf((x + 4));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((x + 5));
        AnswerC = String.valueOf((x + 6));
        AnswerD = String.valueOf((x + 7));
    }

    private void createProblem_Level_2(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf(x);
        SecondNumber = String.valueOf((x * 2));
        ThirdNumber = String.valueOf((x * 3));
        ForthNumber = String.valueOf((x * 4));
        FifthNumber = String.valueOf((x * 5));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((x * 8));
        AnswerC = String.valueOf((x * 6));
        AnswerD = String.valueOf((x * 7));
    }

    private void createProblem_Level_3(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf(x);
        SecondNumber = String.valueOf((x + a));
        ThirdNumber = String.valueOf((x + 2 * a));
        ForthNumber = String.valueOf((x + 3 * a));
        FifthNumber = String.valueOf((x + 4 * a));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((x + 5 * a));
        AnswerC = String.valueOf((x + 6 * a));
        AnswerD = String.valueOf((x + 7 * a));
    }

    private void createProblem_Level_4(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf((x * a));
        SecondNumber = String.valueOf((x * (a + 1)));
        ThirdNumber = String.valueOf((x * (a + 2)));
        ForthNumber = String.valueOf((x * (a + 3)));
        FifthNumber = String.valueOf((x * (a + 4)));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((x * (a + 5)));
        AnswerC = String.valueOf((x * (a + 6)));
        AnswerD = String.valueOf((x * (a + 7)));
    }

    private void createProblem_Level_5(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf((x * 0 * a + 0));
        SecondNumber = String.valueOf((x * (1 * a + 1)));
        ThirdNumber = String.valueOf((x * (2 * a + 2)));
        ForthNumber = String.valueOf((x * (3 * a + 3)));
        FifthNumber = String.valueOf((x * (4 * a + 4)));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((x * (5 * a + 5)));
        AnswerC = String.valueOf((x * (6 * a + 6)));
        AnswerD = String.valueOf((x * (7 * a + 7)));
    }

    private void createProblem_Level_6(){
        int x = MathUtility.getRandomPositiveNumber_1Digit() % 4 + 1;

        FirstNumber = String.valueOf(1);
        SecondNumber = String.valueOf(x);
        ThirdNumber = String.valueOf((x * x));
        ForthNumber = String.valueOf((x * x * x));
        FifthNumber = String.valueOf((x * x * x * x));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((x * 5));
        AnswerC = String.valueOf((x * 3));
        AnswerD = String.valueOf((x * 4));
    }

    private void createProblem_Level_7(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf((x + (a - 1) * x));
        SecondNumber = String.valueOf((x + a * x));
        ThirdNumber = String.valueOf((x + (a + 1) * x));
        ForthNumber = String.valueOf((x + (a + 2) * x));
        FifthNumber = String.valueOf((x + (a + 3) * x));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((x + (a + 4) * x));
        AnswerC = String.valueOf((x + (a + 5) * x));
        AnswerD = String.valueOf((x + (a + 6) * x));
    }

    private void createProblem_Level_8(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf(0);
        SecondNumber = String.valueOf((a * x));
        ThirdNumber = String.valueOf((2 * a * x));
        ForthNumber = String.valueOf((3 * a * x));
        FifthNumber = String.valueOf((4 * a * x));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((5 * a * x));
        AnswerC = String.valueOf((6 * a * x));
        AnswerD = String.valueOf((7 * a * x));
    }

    private void createProblem_Level_9(){
        int x = MathUtility.getRandomPositiveNumber_1Digit() % 4;

        FirstNumber = String.valueOf((0 * x + 1));
        SecondNumber = String.valueOf(1 * x + 2);
        ThirdNumber = String.valueOf((2 * x + 3));
        ForthNumber = String.valueOf((3 * x + 4));
        FifthNumber = String.valueOf((4 * x + 5));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((6 * x + 5));
        AnswerC = String.valueOf((5 * x + 4));
        AnswerD = String.valueOf((7 * x + 6));
    }

    private void createProblem_Level_10(){
        int x = MathUtility.getRandomPositiveNumber_1Digit() % 3 + 1;

        FirstNumber = String.valueOf((x * x));
        SecondNumber = String.valueOf(((x + 1) * (x + 1)));
        ThirdNumber = String.valueOf(((x + 2) * (x + 2)));
        ForthNumber = String.valueOf(((x + 3) * (x + 3)));
        FifthNumber = String.valueOf(((x + 4) * (x + 4)));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf(((x + 5) * (x + 5)));
        AnswerC = String.valueOf(((x + 6) * (x + 6)));
        AnswerD = String.valueOf(((x + 7) * (x + 7)));
    }

    private void createProblem_Level_11(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf((x + (0 * a + -1) * x));
        SecondNumber = String.valueOf((x + (1 * a + 0) * x));
        ThirdNumber = String.valueOf((x + (2 * a + 1) * x));
        ForthNumber = String.valueOf((x + (3 * a + 2) * x));
        FifthNumber = String.valueOf((x + (4 * a + 3) * x));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((x + (5 * a + 4) * x));
        AnswerC = String.valueOf((x + (6 * a + 5) * x));
        AnswerD = String.valueOf((x + (7 * a + 6) * x));
    }

    private void createProblem_Level_12(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf(0);
        SecondNumber = String.valueOf(1 * (x + 1));
        ThirdNumber = String.valueOf((2 * (x + 2)));
        ForthNumber = String.valueOf((3 * (x + 3)));
        FifthNumber = String.valueOf((4 * (x + 4)));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((5 * (x + 5)));
        AnswerC = String.valueOf((6 * (x + 6)));
        AnswerD = String.valueOf((7 * (x + 7)));
    }

    private void createProblem_Level_13(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf(a);
        SecondNumber = String.valueOf((x + 2 * a));
        ThirdNumber = String.valueOf((2 * x + 3 * a));
        ForthNumber = String.valueOf((3 * x + 4 * a));
        FifthNumber = String.valueOf((4 * x + 5 * a));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((5 * x + 5 * a));
        AnswerC = String.valueOf((4 * x + 4 * a));
        AnswerD = String.valueOf((5 * x + 4 * a));
    }

    private void createProblem_Level_14(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf(x);
        SecondNumber = String.valueOf((x + 1 * (a + 0)));
        ThirdNumber = String.valueOf((x + 2 * (a + 1)));
        ForthNumber = String.valueOf((x + 3 * (a + 2)));
        FifthNumber = String.valueOf((x + 4 * (a + 3)));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((x + 5 * (a + 4)));
        AnswerC = String.valueOf((x + 6 * (a + 5)));
        AnswerD = String.valueOf((x + 7 * (a + 6)));
    }

    private void createProblem_Level_15(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();
        int a = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf(x);
        SecondNumber = String.valueOf((2 * x + a));
        ThirdNumber = String.valueOf((3 * x + 2 * a));
        ForthNumber = String.valueOf((4 * x + 3 * a));
        FifthNumber = String.valueOf((5 * x + 4 * a));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((6 * x + 6 * a));
        AnswerC = String.valueOf((7 * x + 7 * a));
        AnswerD = String.valueOf((6 * x + 7 * a));
    }

    private void createProblem_Level_19(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf((2 * x));
        SecondNumber = String.valueOf((3 * x + 1));
        ThirdNumber = String.valueOf((4 * x + 2));
        ForthNumber = String.valueOf((5 * x + 3));
        FifthNumber = String.valueOf((6 * x + 4));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((7 * x + 5));
        AnswerC = String.valueOf((5 * x + 7));
        AnswerD = String.valueOf((5 * x + 5));
    }

    private void createProblem_Level_26(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf(x);
        SecondNumber = String.valueOf((x + a));
        ThirdNumber = String.valueOf((x + a + b));
        ForthNumber = String.valueOf((x + a + b + a));
        FifthNumber = String.valueOf((x + a + b + a + b));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((x + a + b + a + 2 * b));
        AnswerC = String.valueOf((x + a + b + a + 3 * b));
        AnswerD = String.valueOf((x + a + b + a + 4 * b));
    }

    private void createProblem_Level_36(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf(x);
        SecondNumber = String.valueOf((x + 1));
        ThirdNumber = String.valueOf((2 * x + 1));
        ForthNumber = String.valueOf((3 * x + 2));
        FifthNumber = String.valueOf((5 * x + 3));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((5 * x + 8));
        AnswerC = String.valueOf((5 * x + 5));
        AnswerD = String.valueOf((8 * x + 5));
    }

    private void createProblem_Level_37(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf(x);
        SecondNumber = String.valueOf((2 * x + 5));
        ThirdNumber = String.valueOf((3 * x + 10));
        ForthNumber = String.valueOf((4 * x + 15));
        FifthNumber = String.valueOf((5 * x + 20));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((5 * x + 25));
        AnswerC = String.valueOf((6 * x + 30));
        AnswerD = String.valueOf((6 * x + 25));
    }

    private void createProblem_Level_46(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf(x);
        SecondNumber = String.valueOf((2 * x - 1));
        ThirdNumber = String.valueOf((3 * x - 2));
        ForthNumber = String.valueOf((4 * x - 3));
        FifthNumber = String.valueOf((5 * x - 4));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((6 * x - 5));
        AnswerC = String.valueOf((5 * x - 5));
        AnswerD = String.valueOf((5 * x - 6));
    }

    private void createProblem_Level_47(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf((2 * x));
        SecondNumber = String.valueOf((3 * x - 1));
        ThirdNumber = String.valueOf((4 * x - 2));
        ForthNumber = String.valueOf((5 * x - 3));
        FifthNumber = String.valueOf((6 * x - 4));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((7 * x - 5));
        AnswerC = String.valueOf((5 * x - 4));
        AnswerD = String.valueOf((7 * x - 6));
    }

    private void createProblem_Level_59(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf((x * (x + 1)));
        SecondNumber = String.valueOf(((x + 1) * (x + 2)));
        ThirdNumber = String.valueOf(((x + 2) * (x + 3)));
        ForthNumber = String.valueOf(((x + 3) * (x + 4)));
        FifthNumber = String.valueOf(((x + 4) * (x + 5)));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf(((x + 5) * (x + 6)));
        AnswerC = String.valueOf(((x + 5) * (x + 3)));
        AnswerD = String.valueOf(((x + 5) * (x + 2)));
    }

    private void createProblem_Level_60(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int n = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf((a + n));
        SecondNumber = String.valueOf((a * 2 * n));
        ThirdNumber = String.valueOf((a + 3 * n));
        ForthNumber = String.valueOf((a * 4 * n));
        FifthNumber = String.valueOf((a + 5 * n));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((a + 6 * n));
        AnswerC = String.valueOf((a * 5 * n));
        AnswerD = String.valueOf((a * 6 * n));
    }

    private void createProblem_Level_61(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int n = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf((a + n));
        SecondNumber = String.valueOf(((a + 1) * n));
        ThirdNumber = String.valueOf(((a + 2) + n));
        ForthNumber = String.valueOf(((a + 3) * n));
        FifthNumber = String.valueOf(((a + 4) + n));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf(((a + 4) * n));
        AnswerC = String.valueOf(((a + 3) + n));
        AnswerD = String.valueOf(((a + 1) + n));
    }

    private void createProblem_Level_62(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf((x * (x + 1)));
        SecondNumber = String.valueOf(((x + 1) + (x + 2)));
        ThirdNumber = String.valueOf(((x + 2) * (x + 3)));
        ForthNumber = String.valueOf(((x + 3) + (x + 4)));
        FifthNumber = String.valueOf(((x + 4) * (x + 5)));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf(((x + 5) + (x + 6)));
        AnswerC = String.valueOf(((x + 5) * (x + 6)));
        AnswerD = String.valueOf(((x + 5) + (x + 4)));
    }

    private void createProblem_Level_63(){
        int x = MathUtility.getRandomPositiveNumber_1Digit();
        int y = MathUtility.getRandomPositiveNumber_1Digit();

        FirstNumber = String.valueOf(x);
        SecondNumber = String.valueOf(y);
        ThirdNumber = String.valueOf((x + y));
        ForthNumber = String.valueOf((x + 2 * y));
        FifthNumber = String.valueOf((2 * x + 3 * y));

        AnswerA = FifthNumber;
        AnswerB = String.valueOf((x * y));
        AnswerC = String.valueOf((x + 3 * y));
        AnswerD = String.valueOf((2 * x + 2 * y));
    }

    @Override
    public String getFirstNumber() {
        return FirstNumber;
    }

    @Override
    public String getSecondNumber() {
        return SecondNumber;
    }

    @Override
    public String getThirdNumber() {
        return ThirdNumber;
    }

    @Override
    public String getForthNumber() {
        return ForthNumber;
    }

    @Override
    public String getFifthNumber() {
        return FifthNumber;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = AnswerA;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    AnswerA = AnswerB;
                    AnswerB = temp;
                    break;
                case 1:
                    AnswerA = AnswerC;
                    AnswerC = temp;
                    break;
                case 2:
                    AnswerA = AnswerD;
                    AnswerD = temp;
                    break;
                default:
                    AnswerA = AnswerB;
                    AnswerB = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return AnswerA;
    }

    @Override
    public String getAnswerB() {
        return AnswerB;
    }

    @Override
    public String getAnswerC() {
        return AnswerC;
    }

    @Override
    public String getAnswerD() {
        return AnswerD;
    }

}
