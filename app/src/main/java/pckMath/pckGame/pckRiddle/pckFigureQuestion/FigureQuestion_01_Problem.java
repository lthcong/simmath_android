package pckMath.pckGame.pckRiddle.pckFigureQuestion;

import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckGame.pckRiddle.FigureQuestion;

/**
 * Created by Cong on 5/30/2017.
 */

public class FigureQuestion_01_Problem extends FigureQuestion {

    private String  NUMBER_1_1, NUMBER_1_2, NUMBER_1_3,
            NUMBER_2_1, NUMBER_2_2, NUMBER_2_3,
            NUMBER_3_1, NUMBER_3_2, NUMBER_3_3;
    private int RightAnswer;
    private String  ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D, RIGHT_ANSWER;

    public FigureQuestion_01_Problem(){
        createProblem();
        createQuestion();
        createAnswer();
        swapAnswer();
    }

    @Override
    public void createProblem(){
        switch (InfoCollector.getRiddleGameLevel()){
            case 16:
                createProblem_Level_16();
                break;
            case 20:
                createProblem_Level_20();
                break;
            case 21:
                createProblem_Level_21();
                break;
            case 22:
                createProblem_Level_22();
                break;
            case 25:
                createProblem_Level_25();
                break;
            case 31:
                createProblem_Level_31();
                break;
            case 34:
                createProblem_Level_34();
                break;
            case 39:
                createProblem_Level_39();
                break;
            case 48:
                createProblem_Level_48();
                break;
            case 49:
                createProblem_Level_49();
                break;
            case 55:
                createProblem_Level_55();
                break;
            case 56:
                createProblem_Level_56();
                break;
            case 57:
                createProblem_Level_57();
                break;
            default:
                createRandomProblem();
                break;
        }
    }

    private void createRandomProblem(){
        int NoProblemType = 10;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblemType){
            case 0:
                createProblem_Level_16();
                break;
            case 1:
                createProblem_Level_20();
                break;
            case 2:
                createProblem_Level_21();
                break;
            case 3:
                createProblem_Level_22();
                break;
            case 4:
                createProblem_Level_25();
                break;
            case 5:
                createProblem_Level_31();
                break;
            case 6:
                createProblem_Level_34();
                break;
            case 7:
                createProblem_Level_39();
                break;
            case 8:
                createProblem_Level_48();
                break;
            case 9:
                createProblem_Level_49();
                break;
            default:
                createProblem_Level_16();
                break;
        }
    }

    private void createProblem_Level_16(){
        //  ROW 1
        int Number_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_1_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_1_2 = Number_1_1 + Number_1_3;

        NUMBER_1_1 = String.valueOf(Number_1_1);
        NUMBER_1_2 = String.valueOf(Number_1_2);
        NUMBER_1_3 = String.valueOf(Number_1_3);

        //  ROW 2
        int Number_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_2_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_2_2 = Number_2_1 + Number_2_3;

        NUMBER_2_1 = String.valueOf(Number_2_1);
        NUMBER_2_2 = String.valueOf(Number_2_2);
        NUMBER_2_3 = String.valueOf(Number_2_3);

        //  ROW 3
        int Number_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_2 = Number_3_1 + Number_3_3;

        NUMBER_3_1 = String.valueOf(Number_3_1);
        NUMBER_3_2 = String.valueOf(Number_3_2);
        NUMBER_3_3 = String.valueOf(Number_3_3);
    }

    private void createProblem_Level_20(){
        //  ROW 1
        int Number_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_1_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_1_2 = Number_1_1 + 2 * Number_1_3;

        NUMBER_1_1 = String.valueOf(Number_1_1);
        NUMBER_1_2 = String.valueOf(Number_1_2);
        NUMBER_1_3 = String.valueOf(Number_1_3);

        //  ROW 2
        int Number_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_2_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_2_2 = Number_2_1 + 2 * Number_2_3;

        NUMBER_2_1 = String.valueOf(Number_2_1);
        NUMBER_2_2 = String.valueOf(Number_2_2);
        NUMBER_2_3 = String.valueOf(Number_2_3);

        //  ROW 3
        int Number_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_2 = Number_3_1 + 2 * Number_3_3;

        NUMBER_3_1 = String.valueOf(Number_3_1);
        NUMBER_3_2 = String.valueOf(Number_3_2);
        NUMBER_3_3 = String.valueOf(Number_3_3);
    }

    private void createProblem_Level_21(){
        //  ROW 1
        int Number_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_1_2 = Number_1_1 + 1;
        int Number_1_3 = 2 * Number_1_2;

        NUMBER_1_1 = String.valueOf(Number_1_1);
        NUMBER_1_2 = String.valueOf(Number_1_2);
        NUMBER_1_3 = String.valueOf(Number_1_3);

        //  ROW 2
        int Number_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_2_2 = Number_2_1 + 1;
        int Number_2_3 = 2 * Number_2_2;

        NUMBER_2_1 = String.valueOf(Number_2_1);
        NUMBER_2_2 = String.valueOf(Number_2_2);
        NUMBER_2_3 = String.valueOf(Number_2_3);

        //  ROW 3
        int Number_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_2 = Number_3_1 + 1;
        int Number_3_3 = 2 * Number_3_2;

        NUMBER_3_1 = String.valueOf(Number_3_1);
        NUMBER_3_2 = String.valueOf(Number_3_2);
        NUMBER_3_3 = String.valueOf(Number_3_3);
    }

    private void createProblem_Level_22(){
        //  ROW 1
        int Number_1_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_1_1 = MathUtility.getRandomPositiveNumber_1Digit() + Number_1_3;
        int Number_1_2 = 2 * Number_1_1 - Number_1_3;

        NUMBER_1_1 = String.valueOf(Number_1_1);
        NUMBER_1_2 = String.valueOf(Number_1_2);
        NUMBER_1_3 = String.valueOf(Number_1_3);

        //  ROW 2
        int Number_2_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_2_1 = MathUtility.getRandomPositiveNumber_1Digit() + Number_2_3;
        int Number_2_2 = 2 * Number_2_1 - Number_2_3;

        NUMBER_2_1 = String.valueOf(Number_2_1);
        NUMBER_2_2 = String.valueOf(Number_2_2);
        NUMBER_2_3 = String.valueOf(Number_2_3);

        //  ROW 3
        int Number_3_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_1 = MathUtility.getRandomPositiveNumber_1Digit() + Number_3_3;
        int Number_3_2 = 2 * Number_3_1 - Number_3_3;

        NUMBER_3_1 = String.valueOf(Number_3_1);
        NUMBER_3_2 = String.valueOf(Number_3_2);
        NUMBER_3_3 = String.valueOf(Number_3_3);
    }

    private void createProblem_Level_25(){
        //  ROW 1
        int Number_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_1_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_1_2 = Number_1_1 + Number_1_3 * Number_1_3;

        NUMBER_1_1 = String.valueOf(Number_1_1);
        NUMBER_1_2 = String.valueOf(Number_1_2);
        NUMBER_1_3 = String.valueOf(Number_1_3);

        //  ROW 2
        int Number_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_2_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_2_2 = Number_2_1 + Number_2_3 * Number_2_3;

        NUMBER_2_1 = String.valueOf(Number_2_1);
        NUMBER_2_2 = String.valueOf(Number_2_2);
        NUMBER_2_3 = String.valueOf(Number_2_3);

        //  ROW 3
        int Number_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_2 = Number_3_1 + Number_3_3 * Number_3_3;

        NUMBER_3_1 = String.valueOf(Number_3_1);
        NUMBER_3_2 = String.valueOf(Number_3_2);
        NUMBER_3_3 = String.valueOf(Number_3_3);
    }

    private void createProblem_Level_31(){
        //  ROW 1
        int Number_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_1_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_1_2 = Math.abs(Number_1_1 * Number_1_1 - 2 * Number_1_3);

        NUMBER_1_1 = String.valueOf(Number_1_1);
        NUMBER_1_2 = String.valueOf(Number_1_2);
        NUMBER_1_3 = String.valueOf(Number_1_3);

        //  ROW 2
        int Number_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_2_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_2_2 = Math.abs(Number_2_1 * Number_2_1 - 2 * Number_2_3);

        NUMBER_2_1 = String.valueOf(Number_2_1);
        NUMBER_2_2 = String.valueOf(Number_2_2);
        NUMBER_2_3 = String.valueOf(Number_2_3);

        //  ROW 3
        int Number_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_2 = Math.abs(Number_3_1 * Number_3_1 - 2 * Number_3_3);

        NUMBER_3_1 = String.valueOf(Number_3_1);
        NUMBER_3_2 = String.valueOf(Number_3_2);
        NUMBER_3_3 = String.valueOf(Number_3_3);
    }

    private void createProblem_Level_34(){
        //  ROW 1
        int Number_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_1_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_1_2 = 2 * Number_1_1 * Number_1_3;

        Number_1_1 = Number_1_1 * Number_1_1;
        Number_1_3 = Number_1_3 * Number_1_3;

        NUMBER_1_1 = String.valueOf((Number_1_1));
        NUMBER_1_2 = String.valueOf(Number_1_2);
        NUMBER_1_3 = String.valueOf(Number_1_3);

        //  ROW 2
        int Number_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_2_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_2_2 = 2 * Number_2_1 * Number_2_3;

        Number_2_1 = Number_2_1 * Number_2_1;
        Number_2_3 = Number_2_3 * Number_2_3;

        NUMBER_2_1 = String.valueOf(Number_2_1);
        NUMBER_2_2 = String.valueOf(Number_2_2);
        NUMBER_2_3 = String.valueOf(Number_2_3);

        //  ROW 3
        int Number_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_2 = 2 * Number_2_1 * Number_2_3;

        Number_3_1 = Number_3_1 * Number_3_1;
        Number_3_3 = Number_3_3 * Number_3_3;

        NUMBER_3_1 = String.valueOf(Number_3_1);
        NUMBER_3_2 = String.valueOf(Number_3_2);
        NUMBER_3_3 = String.valueOf(Number_3_3);
    }

    private void createProblem_Level_39(){
        //  ROW 1
        int Number_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_1_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_1_2 = (Number_1_1 - Number_1_3) * (Number_1_1 - Number_1_3);

        NUMBER_1_1 = String.valueOf((Number_1_1));
        NUMBER_1_2 = String.valueOf(Number_1_2);
        NUMBER_1_3 = String.valueOf(Number_1_3);

        //  ROW 2
        int Number_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_2_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_2_2 = (Number_2_1 - Number_2_3) * (Number_2_1 - Number_2_3);

        NUMBER_2_1 = String.valueOf(Number_2_1);
        NUMBER_2_2 = String.valueOf(Number_2_2);
        NUMBER_2_3 = String.valueOf(Number_2_3);

        //  ROW 3
        int Number_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_3 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_2 = (Number_3_1 - Number_3_3) * (Number_3_1 - Number_3_3);

        NUMBER_3_1 = String.valueOf(Number_3_1);
        NUMBER_3_2 = String.valueOf(Number_3_2);
        NUMBER_3_3 = String.valueOf(Number_3_3);
    }

    private void createProblem_Level_48(){
        //  ROW 1
        int Number_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_1_2 = 2 * (Number_1_1 + 1);
        int Number_1_3 = 3 * (Number_1_1 + 2);

        NUMBER_1_1 = String.valueOf((Number_1_1));
        NUMBER_1_2 = String.valueOf(Number_1_2);
        NUMBER_1_3 = String.valueOf(Number_1_3);

        //  ROW 2
        int Number_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_2_2 = 2 * (Number_2_1 + 1);
        int Number_2_3 = 3 * (Number_2_1 + 2);

        NUMBER_2_1 = String.valueOf(Number_2_1);
        NUMBER_2_2 = String.valueOf(Number_2_2);
        NUMBER_2_3 = String.valueOf(Number_2_3);

        //  ROW 3
        int Number_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_2 = 2 * (Number_3_1 + 1);
        int Number_3_3 = 3 * (Number_3_2 + 2);

        NUMBER_3_1 = String.valueOf(Number_3_1);
        NUMBER_3_2 = String.valueOf(Number_3_2);
        NUMBER_3_3 = String.valueOf(Number_3_3);
    }

    private void createProblem_Level_49(){
        //  ROW 1
        int Number_1_1 = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Number_1_2 = 2 * Number_1_1;
        int Number_1_3 = 3 * (Number_1_1 + 1);

        NUMBER_1_1 = String.valueOf((Number_1_1 - 1));
        NUMBER_1_2 = String.valueOf(Number_1_2);
        NUMBER_1_3 = String.valueOf(Number_1_3);

        //  ROW 2
        int Number_2_1 = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Number_2_2 = 2 * Number_2_1;
        int Number_2_3 = 3 * (Number_2_1 + 1);

        NUMBER_2_1 = String.valueOf(Number_2_1 - 1);
        NUMBER_2_2 = String.valueOf(Number_2_2);
        NUMBER_2_3 = String.valueOf(Number_2_3);

        //  ROW 3
        int Number_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_2 = 2 * Number_3_1;
        int Number_3_3 = 3 * (Number_3_1 + 1);

        NUMBER_3_1 = String.valueOf(Number_3_1 - 1);
        NUMBER_3_2 = String.valueOf(Number_3_2);
        NUMBER_3_3 = String.valueOf(Number_3_3);
    }

    private void createProblem_Level_55(){
        //  ROW 1
        int Number_1_1 = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Number_1_2 = 2 * Number_1_1;
        int Number_1_3 = Number_1_1 * Number_1_1;

        NUMBER_1_1 = String.valueOf(Number_1_1);
        NUMBER_1_2 = String.valueOf(Number_1_2);
        NUMBER_1_3 = String.valueOf(Number_1_3);

        //  ROW 2
        int Number_2_1 = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Number_2_2 = 2 * Number_2_1;
        int Number_2_3 = Number_2_1 * Number_2_1;

        NUMBER_2_1 = String.valueOf(Number_2_1);
        NUMBER_2_2 = String.valueOf(Number_2_2);
        NUMBER_2_3 = String.valueOf(Number_2_3);

        //  ROW 3
        int Number_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_2 = 2 * Number_3_1;
        int Number_3_3 = Number_3_1 * Number_3_1;

        NUMBER_3_1 = String.valueOf(Number_3_1);
        NUMBER_3_2 = String.valueOf(Number_3_2);
        NUMBER_3_3 = String.valueOf(Number_3_3);
    }

    private void createProblem_Level_56(){
        //  ROW 1
        int Number_1_1 = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Number_1_2 = Number_1_1 + 1;
        int Number_1_3 = Number_1_1 * Number_1_1 + 1;

        NUMBER_1_1 = String.valueOf(Number_1_1);
        NUMBER_1_2 = String.valueOf(Number_1_2);
        NUMBER_1_3 = String.valueOf(Number_1_3);

        //  ROW 2
        int Number_2_1 = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Number_2_2 = Number_2_1 + 1;
        int Number_2_3 = Number_2_1 * Number_2_1 + 1;

        NUMBER_2_1 = String.valueOf(Number_2_1);
        NUMBER_2_2 = String.valueOf(Number_2_2);
        NUMBER_2_3 = String.valueOf(Number_2_3);

        //  ROW 3
        int Number_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int Number_3_2 = Number_3_1 + 1;
        int Number_3_3 = Number_3_1 * Number_3_1 + 1;

        NUMBER_3_1 = String.valueOf(Number_3_1);
        NUMBER_3_2 = String.valueOf(Number_3_2);
        NUMBER_3_3 = String.valueOf(Number_3_3);
    }

    private void createProblem_Level_57(){
        //  ROW 1
        int a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int b = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Number_1_1 = a * a;
        int Number_1_2 = Math.abs(a - b);
        int Number_1_3 = b * b;

        NUMBER_1_1 = String.valueOf((Number_1_1));
        NUMBER_1_2 = String.valueOf(Number_1_2);
        NUMBER_1_3 = String.valueOf(Number_1_3);

        //  ROW 2
        a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        b = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Number_2_1 = a * a;
        int Number_2_2 = Math.abs(a - b);
        int Number_2_3 = b * b;

        NUMBER_2_1 = String.valueOf(Number_2_1);
        NUMBER_2_2 = String.valueOf(Number_2_2);
        NUMBER_2_3 = String.valueOf(Number_2_3);

        //  ROW 3
        a = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        b = MathUtility.getRandomPositiveNumber_1Digit() + 1;
        int Number_3_1 = a * a;
        int Number_3_2 = Math.abs(a - b);
        int Number_3_3 = b * b;

        NUMBER_3_1 = String.valueOf(Number_3_1);
        NUMBER_3_2 = String.valueOf(Number_3_2);
        NUMBER_3_3 = String.valueOf(Number_3_3);
    }

    @Override
    public void createQuestion(){
        switch (MathUtility.getRandomPositiveNumber_4Digit() % 9){
            case 0:
                RIGHT_ANSWER = NUMBER_1_1;
                NUMBER_1_1 = "?";
                break;
            case 1:
                RIGHT_ANSWER = NUMBER_1_2;
                NUMBER_1_2 = "?";
                break;
            case 2:
                RIGHT_ANSWER = NUMBER_1_3;
                NUMBER_1_3 = "?";
                break;
            case 3:
                RIGHT_ANSWER = NUMBER_2_1;
                NUMBER_2_1 = "?";
                break;
            case 4:
                RIGHT_ANSWER = NUMBER_2_2;
                NUMBER_2_2 = "?";
                break;
            case 5:RIGHT_ANSWER = NUMBER_2_3;
                NUMBER_2_3 = "?";
                break;
            case 6:
                RIGHT_ANSWER = NUMBER_3_1;
                NUMBER_3_1 = "?";
                break;
            case 7:
                RIGHT_ANSWER = NUMBER_3_2;
                NUMBER_3_2 = "?";
                break;
            case 8:
                RIGHT_ANSWER = NUMBER_3_3;
                NUMBER_3_3 = "?";
                break;
            default:
                RIGHT_ANSWER = NUMBER_1_1;
                NUMBER_1_1 = "?";
                break;
        }

        RightAnswer = Integer.parseInt(RIGHT_ANSWER);
    }

    @Override
    public void createAnswer(){
        ANSWER_A = String.valueOf(RightAnswer);
        ANSWER_B = String.valueOf(RightAnswer + 1);
        ANSWER_C = String.valueOf(RightAnswer + 2);
        ANSWER_D = String.valueOf(RightAnswer - 1);
    }

    public String getNumber_1_1(){
        return NUMBER_1_1;
    }

    public String getNumber_1_2(){
        return NUMBER_1_2;
    }

    public String getNumber_1_3(){
        return NUMBER_1_3;
    }

    public String getNumber_2_1(){
        return NUMBER_2_1;
    }

    public String getNumber_2_2(){
        return NUMBER_2_2;
    }

    public String getNumber_2_3(){
        return NUMBER_2_3;
    }

    public String getNumber_3_1(){
        return NUMBER_3_1;
    }

    public String getNumber_3_2(){
        return NUMBER_3_2;
    }

    public String getNumber_3_3(){
        return NUMBER_3_3;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getRightAnswer(){
        return RIGHT_ANSWER;
    }

    @Override
    public String getAnswerA(){
        return ANSWER_A;
    }

    @Override
    public String getAnswerB(){
        return ANSWER_B;
    }

    @Override
    public String getAnswerC(){
        return ANSWER_C;
    }

    @Override
    public String getAnswerD(){
        return ANSWER_D;
    }

}
