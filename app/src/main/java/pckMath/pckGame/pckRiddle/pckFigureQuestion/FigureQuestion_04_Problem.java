package pckMath.pckGame.pckRiddle.pckFigureQuestion;

import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckGame.pckRiddle.FigureQuestion;

/**
 * Created by Cong on 5/30/2017.
 */

public class FigureQuestion_04_Problem extends FigureQuestion {

    private String Number_1_1, Number_1_2, Number_1_3, Number_1_4, Number_1_5, Number_1_6,
            Number_2_1, Number_2_2, Number_2_3, Number_2_4, Number_2_5, Number_2_6,
            Number_3_1, Number_3_2, Number_3_3, Number_3_4, Number_3_5, Number_3_6;
    private int RightAnswer;
    private String  ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D, RIGHT_ANSWER;

    public FigureQuestion_04_Problem(){
        createProblem();
        createQuestion();
        createAnswer();
        swapAnswer();
    }

    @Override
    public void createProblem() {
        switch (InfoCollector.getRiddleGameLevel()){
            case 24:
                createProblem_Level_24();
                break;
            case 27:
                createProblem_Level_27();
                break;
            case 43:
                createProblem_Level_43();
                break;
            case 44:
                createProblem_Level_44();
                break;
            case 45:
                createProblem_Level_45();
                break;
            case 51:
                createProblem_Level_51();
                break;
            default:
                createRandomProblem();
                break;
        }
    }

    private void createRandomProblem() {
        int NoProblemType = 5;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblemType){
            case 0:
                createProblem_Level_24();
                break;
            case 1:
                createProblem_Level_27();
                break;
            case 2:
                createProblem_Level_43();
                break;
            case 3:
                createProblem_Level_44();
                break;
            case 4:
                createProblem_Level_45();
                break;
            default:
                createProblem_Level_27();
                break;
        }
    }

    private void createProblem_Level_24() {
        //  ROW 1
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        Number_1_1 = String.valueOf(a);
        Number_1_2 = String.valueOf((a * (2 * b + a)));
        Number_1_3 = String.valueOf(b);
        Number_1_4 = String.valueOf((2 * a + b));
        Number_1_5 = String.valueOf((b * (2 * a + b)));
        Number_1_6 = String.valueOf((2 * b + a));

        //  ROW 2
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        Number_2_1 = String.valueOf(a);
        Number_2_2 = String.valueOf((a * (2 * b + a)));
        Number_2_3 = String.valueOf(b);
        Number_2_4 = String.valueOf((2 * a + b));
        Number_2_5 = String.valueOf((b * (2 * a + b)));
        Number_2_6 = String.valueOf((2 * b + a));

        //  ROW 3
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        Number_3_1 = String.valueOf(a);
        Number_3_2 = String.valueOf((a * (2 * b + a)));
        Number_3_3 = String.valueOf(b);
        Number_3_4 = String.valueOf((2 * a + b));
        Number_3_5 = String.valueOf((b * (2 * a + b)));
        Number_3_6 = String.valueOf((2 * b + a));
    }

    private void createProblem_Level_27() {
        //  ROW 1
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();
        int d = MathUtility.getRandomPositiveNumber_1Digit();
        Number_1_1 = String.valueOf(a);
        Number_1_2 = String.valueOf((2 * a + b));
        Number_1_3 = String.valueOf(b);
        Number_1_4 = String.valueOf(c);
        Number_1_5 = String.valueOf((2 * d + c));
        Number_1_6 = String.valueOf(d);

        //  ROW 2
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        d = MathUtility.getRandomPositiveNumber_1Digit();
        Number_2_1 = String.valueOf(a);
        Number_2_2 = String.valueOf((2 * a + b));
        Number_2_3 = String.valueOf(b);
        Number_2_4 = String.valueOf(c);
        Number_2_5 = String.valueOf((2 * d + c));
        Number_2_6 = String.valueOf(d);

        //  ROW 3
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        d = MathUtility.getRandomPositiveNumber_1Digit();
        Number_3_1 = String.valueOf(a);
        Number_3_2 = String.valueOf((2 * a + b));
        Number_3_3 = String.valueOf(b);
        Number_3_4 = String.valueOf(c);
        Number_3_5 = String.valueOf((2 * d + c));
        Number_3_6 = String.valueOf(d);
    }

    private void createProblem_Level_43() {
        //  ROW 1
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();
        Number_1_1 = String.valueOf(a);
        Number_1_2 = String.valueOf(b);
        Number_1_3 = String.valueOf(c);
        Number_1_4 = String.valueOf(Math.abs(a - b));
        Number_1_5 = String.valueOf(Math.abs(c - b));
        Number_1_6 = String.valueOf(Math.abs(a - c));

        //  ROW 2
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        Number_2_1 = String.valueOf(a);
        Number_2_2 = String.valueOf(b);
        Number_2_3 = String.valueOf(c);
        Number_2_4 = String.valueOf(Math.abs(a - b));
        Number_2_5 = String.valueOf(Math.abs(c - b));
        Number_2_6 = String.valueOf(Math.abs(a - c));

        //  ROW 3
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        Number_3_1 = String.valueOf(a);
        Number_3_2 = String.valueOf(b);
        Number_3_3 = String.valueOf(c);
        Number_3_4 = String.valueOf(Math.abs(a - b));
        Number_3_5 = String.valueOf(Math.abs(c - b));
        Number_3_6 = String.valueOf(Math.abs(a - c));
    }

    private void createProblem_Level_44() {
        //  ROW 1
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();
        Number_1_1 = String.valueOf(a);
        Number_1_2 = String.valueOf(b);
        Number_1_3 = String.valueOf(c);
        Number_1_4 = String.valueOf(Math.abs(a - b));
        Number_1_5 = String.valueOf(Math.abs(a + b + c));
        Number_1_6 = String.valueOf(Math.abs(b - c));

        //  ROW 2
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        Number_2_1 = String.valueOf(a);
        Number_2_2 = String.valueOf(b);
        Number_2_3 = String.valueOf(c);
        Number_2_4 = String.valueOf(Math.abs(a - b));
        Number_2_5 = String.valueOf(Math.abs(a + b + c));
        Number_2_6 = String.valueOf(Math.abs(b - c));

        //  ROW 3
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        Number_3_1 = String.valueOf(a);
        Number_3_2 = String.valueOf(b);
        Number_3_3 = String.valueOf(c);
        Number_3_4 = String.valueOf(Math.abs(a - b));
        Number_3_5 = String.valueOf(Math.abs(a + b + c));
        Number_3_6 = String.valueOf(Math.abs(b - c));
    }

    private void createProblem_Level_45() {
        //  ROW 1
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();
        Number_1_1 = String.valueOf(a);
        Number_1_2 = String.valueOf(b);
        Number_1_3 = String.valueOf(c);
        Number_1_4 = String.valueOf(Math.abs(a - b - c));
        Number_1_5 = String.valueOf(Math.abs(b - a - c));
        Number_1_6 = String.valueOf(Math.abs(c - a - b));

        //  ROW 2
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        Number_2_1 = String.valueOf(a);
        Number_2_2 = String.valueOf(b);
        Number_2_3 = String.valueOf(c);
        Number_2_4 = String.valueOf(Math.abs(a - b - c));
        Number_2_5 = String.valueOf(Math.abs(b - a - c));
        Number_2_6 = String.valueOf(Math.abs(c - a - b));

        //  ROW 3
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        Number_3_1 = String.valueOf(a);
        Number_3_2 = String.valueOf(b);
        Number_3_3 = String.valueOf(c);
        Number_3_4 = String.valueOf(Math.abs(a - b - c));
        Number_3_5 = String.valueOf(Math.abs(b - a - c));
        Number_3_6 = String.valueOf(Math.abs(c - a - b));
    }

    private void createProblem_Level_51() {
        //  ROW 1
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        Number_1_1 = String.valueOf(a);
        Number_1_2 = String.valueOf(a + 1);
        Number_1_3 = String.valueOf(a * 2);
        Number_1_4 = String.valueOf(a + 3);
        Number_1_5 = String.valueOf(a * 4);
        Number_1_6 = String.valueOf(a + 5);

        //  ROW 2
        a = MathUtility.getRandomPositiveNumber_1Digit();
        Number_2_1 = String.valueOf(a);
        Number_2_2 = String.valueOf(a + 1);
        Number_2_3 = String.valueOf(a * 2);
        Number_2_4 = String.valueOf(a + 3);
        Number_2_5 = String.valueOf(a * 4);
        Number_2_6 = String.valueOf(a + 5);

        //  ROW 3
        a = MathUtility.getRandomPositiveNumber_1Digit();
        Number_3_1 = String.valueOf(a);
        Number_3_2 = String.valueOf(a + 1);
        Number_3_3 = String.valueOf(a * 2);
        Number_3_4 = String.valueOf(a + 3);
        Number_3_5 = String.valueOf(a * 4);
        Number_3_6 = String.valueOf(a + 5);
    }

    @Override
    public void createQuestion() {
        switch (MathUtility.getRandomPositiveNumber_4Digit() % 18){
            case 0:
                RIGHT_ANSWER = Number_1_1;
                Number_1_1 = "?";
                break;
            case 1:
                RIGHT_ANSWER = Number_1_2;
                Number_1_2 = "?";
                break;
            case 2:
                RIGHT_ANSWER = Number_1_3;
                Number_1_3 = "?";
                break;
            case 3:
                RIGHT_ANSWER = Number_1_4;
                Number_1_4 = "?";
                break;
            case 4:
                RIGHT_ANSWER = Number_1_5;
                Number_1_5 = "?";
                break;
            case 5:
                RIGHT_ANSWER = Number_1_6;
                Number_1_6 = "?";
                break;
            case 6:
                RIGHT_ANSWER = Number_2_1;
                Number_2_1 = "?";
                break;
            case 7:
                RIGHT_ANSWER = Number_2_2;
                Number_2_2 = "?";
                break;
            case 8:
                RIGHT_ANSWER = Number_2_3;
                Number_2_3 = "?";
                break;
            case 9:
                RIGHT_ANSWER = Number_2_4;
                Number_2_4 = "?";
                break;
            case 10:
                RIGHT_ANSWER = Number_2_5;
                Number_2_5 = "?";
                break;
            case 11:
                RIGHT_ANSWER = Number_2_6;
                Number_2_6 = "?";
                break;
            case 12:
                RIGHT_ANSWER = Number_3_1;
                Number_3_1 = "?";
                break;
            case 13:
                RIGHT_ANSWER = Number_3_2;
                Number_3_2 = "?";
                break;
            case 14:
                RIGHT_ANSWER = Number_3_3;
                Number_3_3 = "?";
                break;
            case 15:
                RIGHT_ANSWER = Number_3_4;
                Number_3_4 = "?";
                break;
            case 16:
                RIGHT_ANSWER = Number_3_5;
                Number_3_5 = "?";
                break;
            case 17:
                RIGHT_ANSWER = Number_3_6;
                Number_3_6 = "?";
                break;
            default:
                RIGHT_ANSWER = Number_1_1;
                Number_1_1 = "?";
                break;
        }

        RightAnswer = Integer.parseInt(RIGHT_ANSWER);
    }

    @Override
    public void createAnswer(){
        ANSWER_A = String.valueOf(RightAnswer);
        ANSWER_B = String.valueOf(RightAnswer + 1);
        ANSWER_C = String.valueOf(RightAnswer + 2);
        ANSWER_D = String.valueOf(RightAnswer - 1);
    }

    public String getNumber_1_1(){
        return Number_1_1;
    }

    public String getNumber_1_2(){
        return Number_1_2;
    }

    public String getNumber_1_3(){
        return Number_1_3;
    }

    public String getNumber_1_4(){
        return Number_1_4;
    }

    public String getNumber_1_5(){
        return Number_1_5;
    }

    public String getNumber_1_6(){
        return Number_1_6;
    }

    public String getNumber_2_1(){
        return Number_2_1;
    }

    public String getNumber_2_2(){
        return Number_2_2;
    }

    public String getNumber_2_3(){
        return Number_2_3;
    }

    public String getNumber_2_4(){
        return Number_2_4;
    }

    public String getNumber_2_5(){
        return Number_2_5;
    }

    public String getNumber_2_6(){
        return Number_2_6;
    }

    public String getNumber_3_1(){
        return Number_3_1;
    }

    public String getNumber_3_2(){
        return Number_3_2;
    }

    public String getNumber_3_3(){
        return Number_3_3;
    }

    public String getNumber_3_4(){
        return Number_3_4;
    }

    public String getNumber_3_5(){
        return Number_3_5;
    }

    public String getNumber_3_6(){
        return Number_3_6;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getRightAnswer(){
        return RIGHT_ANSWER;
    }

    @Override
    public String getAnswerA(){
        return ANSWER_A;
    }

    @Override
    public String getAnswerB(){
        return ANSWER_B;
    }

    @Override
    public String getAnswerC(){
        return ANSWER_C;
    }

    @Override
    public String getAnswerD(){
        return ANSWER_D;
    }
}
