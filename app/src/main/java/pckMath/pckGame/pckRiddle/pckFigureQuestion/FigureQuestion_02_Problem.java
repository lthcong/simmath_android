package pckMath.pckGame.pckRiddle.pckFigureQuestion;

import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckGame.pckRiddle.FigureQuestion;

/**
 * Created by Cong on 5/30/2017.
 */

public class FigureQuestion_02_Problem extends FigureQuestion {

    private String  Number_1_1, Sum_1, Diff_1, Number_1_2,
            Number_2_1, Sum_2, Diff_2, Number_2_2,
            Number_3_1, Sum_3, Diff_3, Number_3_2;
    private int RightAnswer;
    private String  ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D, RIGHT_ANSWER;

    public FigureQuestion_02_Problem(){
        createProblem();
        createQuestion();
        createAnswer();
        swapAnswer();
    }

    @Override
    public void createProblem() {
        switch (InfoCollector.getRiddleGameLevel()){
            case 17:
                createProblem_Level_17();
                break;
            case 23:
                createProblem_Level_23();
                break;
            case 32:
                createProblem_Level_32();
                break;
            case 33:
                createProblem_Level_33();
                break;
            case 35:
                createProblem_Level_35();
                break;
            case 40:
                createProblem_Level_40();
                break;
            case 41:
                createProblem_Level_41();
                break;
            case 53:
                createProblem_Level_53();
                break;
            case 54:
                createProblem_Level_54();
                break;
            case 65:
                createProblem_Level_65();
                break;
            default:
                createRandomProblem();
                break;
        }
    }

    private void createRandomProblem() {
        int NoProblemType = 7;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblemType){
            case 0:
                createProblem_Level_17();
                break;
            case 1:
                createProblem_Level_23();
                break;
            case 2:
                createProblem_Level_32();
                break;
            case 3:
                createProblem_Level_33();
                break;
            case 4:
                createProblem_Level_35();
                break;
            case 5:
                createProblem_Level_40();
                break;
            case 6:
                createProblem_Level_41();
                break;
            default:
                createProblem_Level_17();
                break;
        }
    }

    private void createProblem_Level_17(){
        //  ROW 1
        int NUMBER_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_1_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_1 = NUMBER_1_1 + NUMBER_1_2;
        int DIFF_1 = Math.abs(NUMBER_1_1 - NUMBER_1_2);

        Number_1_1 = String.valueOf(NUMBER_1_1);
        Sum_1 = String.valueOf(SUM_1);
        Diff_1 = String.valueOf(DIFF_1);
        Number_1_2 = String.valueOf(NUMBER_1_2);

        //  ROW 2
        int NUMBER_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_2_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_2 = NUMBER_2_1 + NUMBER_2_2;
        int DIFF_2 = Math.abs(NUMBER_2_1 - NUMBER_2_2);

        Number_2_1 = String.valueOf(NUMBER_2_1);
        Sum_2 = String.valueOf(SUM_2);
        Diff_2 = String.valueOf(DIFF_2);
        Number_2_2 = String.valueOf(NUMBER_2_2);

        //  ROW 3
        int NUMBER_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_3_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_3 = NUMBER_3_1 + NUMBER_3_2;
        int DIFF_3 = Math.abs(NUMBER_3_1 - NUMBER_3_2);

        Number_3_1 = String.valueOf(NUMBER_3_1);
        Sum_3 = String.valueOf(SUM_3);
        Diff_3 = String.valueOf(DIFF_3);
        Number_3_2 = String.valueOf(NUMBER_3_2);
    }

    private void createProblem_Level_23(){
        //  ROW 1
        int NUMBER_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_1_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_1 = 2 * NUMBER_1_2 + NUMBER_1_1;
        int DIFF_1 = NUMBER_1_2 + 2 * NUMBER_1_1;

        Number_1_1 = String.valueOf(NUMBER_1_1);
        Sum_1 = String.valueOf(SUM_1);
        Diff_1 = String.valueOf(DIFF_1);
        Number_1_2 = String.valueOf(NUMBER_1_2);

        //  ROW 2
        int NUMBER_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_2_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_2 = 2 * NUMBER_2_2 + NUMBER_2_1;
        int DIFF_2 = NUMBER_2_2 + 2 * NUMBER_2_1;

        Number_2_1 = String.valueOf(NUMBER_2_1);
        Sum_2 = String.valueOf(SUM_2);
        Diff_2 = String.valueOf(DIFF_2);
        Number_2_2 = String.valueOf(NUMBER_2_2);

        //  ROW 3
        int NUMBER_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_3_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_3 = 2 * NUMBER_3_2 + NUMBER_3_1;
        int DIFF_3 = NUMBER_3_2 + 2 * NUMBER_3_1;

        Number_3_1 = String.valueOf(NUMBER_3_1);
        Sum_3 = String.valueOf(SUM_3);
        Diff_3 = String.valueOf(DIFF_3);
        Number_3_2 = String.valueOf(NUMBER_3_2);
    }

    private void createProblem_Level_32(){
        //  ROW 1
        int NUMBER_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_1_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_1 = 2 * NUMBER_1_1 + NUMBER_1_2;
        int DIFF_1 = 2 * NUMBER_1_2 - NUMBER_1_1;

        Number_1_1 = String.valueOf(SUM_1);
        Sum_1 = String.valueOf(NUMBER_1_1);
        Diff_1 = String.valueOf(NUMBER_1_2);
        Number_1_2 = String.valueOf(DIFF_1);

        //  ROW 2
        int NUMBER_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_2_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_2 = 2 * NUMBER_2_1 + NUMBER_2_2;
        int DIFF_2 = 2 * NUMBER_2_2 - NUMBER_2_1;

        Number_2_1 = String.valueOf(SUM_2);
        Sum_2 = String.valueOf(NUMBER_2_1);
        Diff_2 = String.valueOf(NUMBER_2_2);
        Number_2_2 = String.valueOf(DIFF_2);

        //  ROW 3
        int NUMBER_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_3_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_3 = 2 * NUMBER_3_1 + NUMBER_3_2;
        int DIFF_3 = 2 * NUMBER_3_2 - NUMBER_3_1;

        Number_3_1 = String.valueOf(SUM_3);
        Sum_3 = String.valueOf(NUMBER_3_1);
        Diff_3 = String.valueOf(NUMBER_3_2);
        Number_3_2 = String.valueOf(DIFF_3);
    }

    private void createProblem_Level_33(){
        //  ROW 1
        int NUMBER_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_1_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_1 = Math.abs(NUMBER_1_1 * NUMBER_1_1 - 2 * NUMBER_1_2);
        int DIFF_1 = Math.abs(NUMBER_1_2 * NUMBER_1_2 - 2 * NUMBER_1_1);

        Number_1_1 = String.valueOf(NUMBER_1_1);
        Sum_1 = String.valueOf(NUMBER_1_2);
        Diff_1 = String.valueOf(SUM_1);
        Number_1_2 = String.valueOf(DIFF_1);

        //  ROW 2
        int NUMBER_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_2_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_2 = Math.abs(NUMBER_2_1 * NUMBER_2_1 - 2 * NUMBER_2_2);
        int DIFF_2 = Math.abs(NUMBER_2_2 * NUMBER_2_2 - 2 * NUMBER_2_1);

        Number_2_1 = String.valueOf(NUMBER_2_1);
        Sum_2 = String.valueOf(NUMBER_2_2);
        Diff_2 = String.valueOf(SUM_2);
        Number_2_2 = String.valueOf(DIFF_2);

        //  ROW 3
        int NUMBER_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_3_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_3 = Math.abs(NUMBER_3_1 * NUMBER_3_1 - 2 * NUMBER_3_2);
        int DIFF_3 = Math.abs(NUMBER_3_2 * NUMBER_3_2 - 2 * NUMBER_3_1);

        Number_3_1 = String.valueOf(NUMBER_3_1);
        Sum_3 = String.valueOf(NUMBER_3_2);
        Diff_3 = String.valueOf(SUM_3);
        Number_3_2 = String.valueOf(DIFF_3);
    }

    private void createProblem_Level_35(){
        //  ROW 1
        int NUMBER_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_1_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_1 = Math.abs(NUMBER_1_1 + NUMBER_1_2);
        int DIFF_1 = Math.abs(NUMBER_1_1 - NUMBER_1_2);

        NUMBER_1_1 = NUMBER_1_1 * NUMBER_1_1;
        NUMBER_1_2 = NUMBER_1_2 * NUMBER_1_2;

        Number_1_1 = String.valueOf(NUMBER_1_1);
        Sum_1 = String.valueOf(NUMBER_1_2);
        Diff_1 = String.valueOf(SUM_1);
        Number_1_2 = String.valueOf(DIFF_1);

        //  ROW 2
        int NUMBER_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_2_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_2 = Math.abs(NUMBER_2_1 + NUMBER_2_2);
        int DIFF_2 = Math.abs(NUMBER_2_1 - NUMBER_2_2);

        NUMBER_2_1 = NUMBER_2_1 * NUMBER_2_1;
        NUMBER_2_2 = NUMBER_2_2 * NUMBER_2_2;

        Number_2_1 = String.valueOf(NUMBER_2_1);
        Sum_2 = String.valueOf(NUMBER_2_2);
        Diff_2 = String.valueOf(SUM_2);
        Number_2_2 = String.valueOf(DIFF_2);

        //  ROW 3
        int NUMBER_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_3_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_3 = Math.abs(NUMBER_3_1 + NUMBER_3_2);
        int DIFF_3 = Math.abs(NUMBER_3_1 + NUMBER_3_2);

        NUMBER_3_1 = NUMBER_3_1 * NUMBER_3_1;
        NUMBER_3_2 = NUMBER_3_2 * NUMBER_3_2;

        Number_3_1 = String.valueOf(NUMBER_3_1);
        Sum_3 = String.valueOf(NUMBER_3_2);
        Diff_3 = String.valueOf(SUM_3);
        Number_3_2 = String.valueOf(DIFF_3);
    }

    private void createProblem_Level_40(){
        //  ROW 1
        int NUMBER_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_1_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_1 = NUMBER_1_1 * NUMBER_1_1 - NUMBER_1_1;
        int DIFF_1 = NUMBER_1_2 * NUMBER_1_2 - NUMBER_1_2;

        Number_1_1 = String.valueOf(NUMBER_1_1);
        Sum_1 = String.valueOf(NUMBER_1_2);
        Diff_1 = String.valueOf(SUM_1);
        Number_1_2 = String.valueOf(DIFF_1);

        //  ROW 2
        int NUMBER_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_2_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_2 = NUMBER_2_1 * NUMBER_2_1 - NUMBER_2_1;
        int DIFF_2 = NUMBER_2_2 * NUMBER_2_2 - NUMBER_2_2;

        Number_2_1 = String.valueOf(NUMBER_2_1);
        Sum_2 = String.valueOf(NUMBER_2_2);
        Diff_2 = String.valueOf(SUM_2);
        Number_2_2 = String.valueOf(DIFF_2);

        //  ROW 3
        int NUMBER_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_3_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_3 = NUMBER_3_1 * NUMBER_3_1 - NUMBER_3_1;
        int DIFF_3 = NUMBER_3_2 * NUMBER_3_2 - NUMBER_3_2;

        Number_3_1 = String.valueOf(NUMBER_3_1);
        Sum_3 = String.valueOf(NUMBER_3_2);
        Diff_3 = String.valueOf(SUM_3);
        Number_3_2 = String.valueOf(DIFF_3);
    }

    private void createProblem_Level_41(){
        //  ROW 1
        int NUMBER_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_1_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_1 = Math.abs(NUMBER_1_1 * NUMBER_1_1 - NUMBER_1_2);
        int DIFF_1 = Math.abs(NUMBER_1_2 * NUMBER_1_2 - NUMBER_1_1);

        Number_1_1 = String.valueOf(NUMBER_1_1);
        Sum_1 = String.valueOf(NUMBER_1_2);
        Diff_1 = String.valueOf(SUM_1);
        Number_1_2 = String.valueOf(DIFF_1);

        //  ROW 2
        int NUMBER_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_2_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_2 = Math.abs(NUMBER_2_1 * NUMBER_2_1 - NUMBER_2_2);
        int DIFF_2 = Math.abs(NUMBER_2_2 * NUMBER_2_2 - NUMBER_2_1);

        Number_2_1 = String.valueOf(NUMBER_2_1);
        Sum_2 = String.valueOf(NUMBER_2_2);
        Diff_2 = String.valueOf(SUM_2);
        Number_2_2 = String.valueOf(DIFF_2);

        //  ROW 3
        int NUMBER_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_3_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_3 = Math.abs(NUMBER_3_1 * NUMBER_3_1 - NUMBER_3_2);
        int DIFF_3 = Math.abs(NUMBER_3_2 * NUMBER_3_2 - NUMBER_3_1);

        Number_3_1 = String.valueOf(NUMBER_3_1);
        Sum_3 = String.valueOf(NUMBER_3_2);
        Diff_3 = String.valueOf(SUM_3);
        Number_3_2 = String.valueOf(DIFF_3);
    }

    private void createProblem_Level_53(){
        //  ROW 1
        int NUMBER_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_1_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_1 = NUMBER_1_1 + 1;
        int DIFF_1 = NUMBER_1_2 + 2;

        Number_1_1 = String.valueOf(NUMBER_1_1);
        Sum_1 = String.valueOf(NUMBER_1_2);
        Diff_1 = String.valueOf(SUM_1);
        Number_1_2 = String.valueOf(DIFF_1);

        //  ROW 2
        int NUMBER_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_2_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_2 = NUMBER_2_1 + 1;
        int DIFF_2 = NUMBER_2_2 + 2;

        Number_2_1 = String.valueOf(NUMBER_2_1);
        Sum_2 = String.valueOf(NUMBER_2_2);
        Diff_2 = String.valueOf(SUM_2);
        Number_2_2 = String.valueOf(DIFF_2);

        //  ROW 3
        int NUMBER_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_3_2 = MathUtility.getRandomPositiveNumber_1Digit();
        int SUM_3 = NUMBER_3_1 + 1;
        int DIFF_3 = NUMBER_3_2 + 2;

        Number_3_1 = String.valueOf(NUMBER_3_1);
        Sum_3 = String.valueOf(NUMBER_3_2);
        Diff_3 = String.valueOf(SUM_3);
        Number_3_2 = String.valueOf(DIFF_3);
    }

    private void createProblem_Level_54(){
        //  ROW 1
        int NUMBER_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_1_2 = NUMBER_1_1 + 3;
        int SUM_1 = NUMBER_1_1 + 1;
        int DIFF_1 = NUMBER_1_2 * 2;

        Number_1_1 = String.valueOf(NUMBER_1_1);
        Sum_1 = String.valueOf(NUMBER_1_2);
        Diff_1 = String.valueOf(SUM_1);
        Number_1_2 = String.valueOf(DIFF_1);

        //  ROW 2
        int NUMBER_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_2_2 = NUMBER_2_1 + 3;
        int SUM_2 = NUMBER_2_1 + 1;
        int DIFF_2 = NUMBER_2_2 * 2;

        Number_2_1 = String.valueOf(NUMBER_2_1);
        Sum_2 = String.valueOf(NUMBER_2_2);
        Diff_2 = String.valueOf(SUM_2);
        Number_2_2 = String.valueOf(DIFF_2);

        //  ROW 3
        int NUMBER_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_3_2 = NUMBER_3_1 + 3;
        int SUM_3 = NUMBER_3_1 + 1;
        int DIFF_3 = NUMBER_3_2 * 2;

        Number_3_1 = String.valueOf(NUMBER_3_1);
        Sum_3 = String.valueOf(NUMBER_3_2);
        Diff_3 = String.valueOf(SUM_3);
        Number_3_2 = String.valueOf(DIFF_3);
    }

    private void createProblem_Level_65(){
        //  ROW 1
        int NUMBER_1_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_1_2 = 3 * NUMBER_1_1;
        int SUM_1 = NUMBER_1_1 + 1;
        int DIFF_1 = 2 * NUMBER_1_1 + 1;

        Number_1_1 = String.valueOf(NUMBER_1_1);
        Sum_1 = String.valueOf(NUMBER_1_2);
        Diff_1 = String.valueOf(SUM_1);
        Number_1_2 = String.valueOf(DIFF_1);

        //  ROW 2
        int NUMBER_2_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_2_2 = 3 * NUMBER_2_1;
        int SUM_2 = NUMBER_2_1 + 1;
        int DIFF_2 = NUMBER_2_1 * 2 + 1;

        Number_2_1 = String.valueOf(NUMBER_2_1);
        Sum_2 = String.valueOf(NUMBER_2_2);
        Diff_2 = String.valueOf(SUM_2);
        Number_2_2 = String.valueOf(DIFF_2);

        //  ROW 3
        int NUMBER_3_1 = MathUtility.getRandomPositiveNumber_1Digit();
        int NUMBER_3_2 = NUMBER_3_1 * 3;
        int SUM_3 = NUMBER_3_1 + 1;
        int DIFF_3 = NUMBER_3_2 * 2 + 1;

        Number_3_1 = String.valueOf(NUMBER_3_1);
        Sum_3 = String.valueOf(NUMBER_3_2);
        Diff_3 = String.valueOf(SUM_3);
        Number_3_2 = String.valueOf(DIFF_3);
    }

    @Override
    public void createQuestion() {
        switch (MathUtility.getRandomPositiveNumber_4Digit() % 12){
            case 0:
                RIGHT_ANSWER = Number_1_1;
                Number_1_1 = "?";
                break;
            case 1:
                RIGHT_ANSWER = Number_3_2;
                Number_3_2 = "?";
                break;
            case 2:
                RIGHT_ANSWER = Sum_1;
                Sum_1 = "?";
                break;
            case 3:
                RIGHT_ANSWER = Diff_1;
                Diff_1 = "?";
                break;
            case 4:
                RIGHT_ANSWER = Number_1_2;
                Number_1_2 = "?";
                break;
            case 5:
                RIGHT_ANSWER = Number_2_1;
                Number_2_1 = "?";
                break;
            case 6:
                RIGHT_ANSWER = Sum_2;
                Sum_2 = "?";
                break;
            case 7:
                RIGHT_ANSWER = Diff_2;
                Diff_2 = "?";
                break;
            case 8:
                RIGHT_ANSWER = Number_2_2;
                Number_2_2 = "?";
                break;
            case 9:
                RIGHT_ANSWER = Number_3_1;
                Number_3_1 = "?";
                break;
            case 10:
                RIGHT_ANSWER = Sum_3;
                Sum_3 = "?";
                break;
            case 11:
                RIGHT_ANSWER = Diff_3;
                Diff_3 = "?";
                break;
            default:
                RIGHT_ANSWER = Number_1_1;
                Number_1_1 = "?";
                break;
        }

        RightAnswer = Integer.parseInt(RIGHT_ANSWER);
    }

    @Override
    public void createAnswer(){
        ANSWER_A = String.valueOf(RightAnswer);
        ANSWER_B = String.valueOf(RightAnswer + 1);
        ANSWER_C = String.valueOf(RightAnswer + 2);
        ANSWER_D = String.valueOf(RightAnswer - 1);
    }

    public String getNumber_1_1(){
        return Number_1_1;
    }

    public String getSum_1(){
        return Sum_1;
    }

    public String getDiff_1(){
        return Diff_1;
    }

    public String getNumber_1_2(){
        return Number_1_2;
    }

    public String getNumber_2_1(){
        return Number_2_1;
    }

    public String getSum_2(){
        return Sum_2;
    }

    public String getDiff_2(){
        return Diff_2;
    }

    public String getNumber_2_2(){
        return Number_2_2;
    }

    public String getNumber_3_1(){
        return Number_3_1;
    }

    public String getSum_3(){
        return Sum_3;
    }

    public String getDiff_3(){
        return Diff_3;
    }

    public String getNumber_3_2(){
        return Number_3_2;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getRightAnswer(){
        return RIGHT_ANSWER;
    }

    @Override
    public String getAnswerA(){
        return ANSWER_A;
    }

    @Override
    public String getAnswerB(){
        return ANSWER_B;
    }

    @Override
    public String getAnswerC(){
        return ANSWER_C;
    }

    @Override
    public String getAnswerD(){
        return ANSWER_D;
    }
}
