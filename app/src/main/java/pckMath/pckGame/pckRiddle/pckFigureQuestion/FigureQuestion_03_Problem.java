package pckMath.pckGame.pckRiddle.pckFigureQuestion;

import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckGame.pckRiddle.FigureQuestion;

/**
 * Created by Cong on 5/30/2017.
 */

public class FigureQuestion_03_Problem extends FigureQuestion {

    private String Number_1_1, Number_1_2, Number_1_3, Number_1_4, Diff_1,
            Number_2_1, Number_2_2, Number_2_3, Number_2_4, Diff_2,
            Number_3_1, Number_3_2, Number_3_3, Number_3_4, Diff_3;
    private int RightAnswer;
    private String  ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D, RIGHT_ANSWER;

    public FigureQuestion_03_Problem(){
        createProblem();
        createQuestion();
        createAnswer();
        swapAnswer();
    }

    @Override
    public void createProblem() {
        switch(InfoCollector.getRiddleGameLevel()){
            case 18:
                createProblem_Level_18();
                break;
            case 28:
                createProblem_Level_28();
                break;
            case 29:
                createProblem_Level_29();
                break;
            case 30:
                createProblem_Level_30();
                break;
            case 38:
                createProblem_Level_38();
                break;
            case 42:
                createProblem_Level_42();
                break;
            case 50:
                createProblem_Level_50();
                break;
            case 52:
                createProblem_Level_52();
                break;
            case 58:
                createProblem_Level_58();
                break;
            case 64:
                createProblem_Level_64();
                break;
            default:
                createRandomProblem();
                break;
        }
    }

    private void createRandomProblem() {
        int NoProblemType = 7;
        switch(MathUtility.getRandomPositiveNumber_4Digit() % NoProblemType){
            case 0:
                createProblem_Level_50();
                break;
            case 1:
                createProblem_Level_18();
                break;
            case 2:
                createProblem_Level_28();
                break;
            case 3:
                createProblem_Level_29();
                break;
            case 4:
                createProblem_Level_30();
                break;
            case 5:
                createProblem_Level_38();
                break;
            case 6:
                createProblem_Level_42();
                break;
            default:
                createProblem_Level_18();
                break;
        }
    }

    private void createProblem_Level_18() {
        //  ROW 1
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();
        int d = MathUtility.getRandomPositiveNumber_1Digit();

        Number_1_1 = String.valueOf(a);
        Number_1_2 = String.valueOf(b);
        Number_1_3 = String.valueOf(c);
        Number_1_4 = String.valueOf(d);
        Diff_1 = String.valueOf(Math.abs((a * b - c * d)));

        //  ROW 2
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        d = MathUtility.getRandomPositiveNumber_1Digit();

        Number_2_1 = String.valueOf(a);
        Number_2_2 = String.valueOf(b);
        Number_2_3 = String.valueOf(c);
        Number_2_4 = String.valueOf(d);
        Diff_2 = String.valueOf(Math.abs((a * b - c * d)));

        //  ROW 3
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        d = MathUtility.getRandomPositiveNumber_1Digit();

        Number_3_1 = String.valueOf(a);
        Number_3_2 = String.valueOf(b);
        Number_3_3 = String.valueOf(c);
        Number_3_4 = String.valueOf(d);
        Diff_3 = String.valueOf(Math.abs((a * b - c * d)));
    }

    private void createProblem_Level_28() {
        //  ROW 1
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();
        int d = MathUtility.getRandomPositiveNumber_1Digit();

        Number_1_1 = String.valueOf(a);
        Number_1_2 = String.valueOf(b);
        Number_1_3 = String.valueOf(c);
        Number_1_4 = String.valueOf(d);
        Diff_1 = String.valueOf(Math.abs(((a + b) - (c + d))));

        //  ROW 2
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        d = MathUtility.getRandomPositiveNumber_1Digit();

        Number_2_1 = String.valueOf(a);
        Number_2_2 = String.valueOf(b);
        Number_2_3 = String.valueOf(c);
        Number_2_4 = String.valueOf(d);
        Diff_2 = String.valueOf(Math.abs(((a + b) - (c + d))));

        //  ROW 3
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        d = MathUtility.getRandomPositiveNumber_1Digit();

        Number_3_1 = String.valueOf(a);
        Number_3_2 = String.valueOf(b);
        Number_3_3 = String.valueOf(c);
        Number_3_4 = String.valueOf(d);
        Diff_3 = String.valueOf(Math.abs(((a + b) - (c + d))));
    }

    private void createProblem_Level_29() {
        //  ROW 1
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();
        int d = MathUtility.getRandomPositiveNumber_1Digit();

        Number_1_1 = String.valueOf(a);
        Number_1_2 = String.valueOf(b);
        Number_1_3 = String.valueOf(c);
        Number_1_4 = String.valueOf(d);
        Diff_1 = String.valueOf((Math.abs(a - b) + Math.abs(c - d)));

        //  ROW 2
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        d = MathUtility.getRandomPositiveNumber_1Digit();

        Number_2_1 = String.valueOf(a);
        Number_2_2 = String.valueOf(b);
        Number_2_3 = String.valueOf(c);
        Number_2_4 = String.valueOf(d);
        Diff_2 = String.valueOf((Math.abs(a - b) + Math.abs(c - d)));

        //  ROW 3
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        d = MathUtility.getRandomPositiveNumber_1Digit();

        Number_3_1 = String.valueOf(a);
        Number_3_2 = String.valueOf(b);
        Number_3_3 = String.valueOf(c);
        Number_3_4 = String.valueOf(d);
        Diff_3 = String.valueOf((Math.abs(a - b) + Math.abs(c - d)));
    }

    private void createProblem_Level_30() {
        //  ROW 1
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();
        int d = MathUtility.getRandomPositiveNumber_1Digit();

        Number_1_1 = String.valueOf(a);
        Number_1_2 = String.valueOf(b);
        Number_1_3 = String.valueOf(c);
        Number_1_4 = String.valueOf(d);
        Diff_1 = String.valueOf(Math.abs((2 * (a + b) - (c + d))));

        //  ROW 2
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        d = MathUtility.getRandomPositiveNumber_1Digit();

        Number_2_1 = String.valueOf(a);
        Number_2_2 = String.valueOf(b);
        Number_2_3 = String.valueOf(c);
        Number_2_4 = String.valueOf(d);
        Diff_2 = String.valueOf(Math.abs((2 * (a + b) - (c + d))));

        //  ROW 3
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        d = MathUtility.getRandomPositiveNumber_1Digit();

        Number_3_1 = String.valueOf(a);
        Number_3_2 = String.valueOf(b);
        Number_3_3 = String.valueOf(c);
        Number_3_4 = String.valueOf(d);
        Diff_3 = String.valueOf(Math.abs((2 * (a + b) - (c + d))));
    }

    private void createProblem_Level_38() {
        //  ROW 1
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = a * a;
        int d = b * b;

        Number_1_1 = String.valueOf(a);
        Number_1_2 = String.valueOf(d);
        Number_1_3 = String.valueOf(c);
        Number_1_4 = String.valueOf(b);
        Diff_1 = String.valueOf((a - b) * (a - b));

        //  ROW 2
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = a * a;
        d = b * b;

        Number_2_1 = String.valueOf(a);
        Number_2_2 = String.valueOf(d);
        Number_2_3 = String.valueOf(c);
        Number_2_4 = String.valueOf(b);
        Diff_2 = String.valueOf((a - b) * (a - b));

        //  ROW 3
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = a * a;
        d = b * b;

        Number_3_1 = String.valueOf(a);
        Number_3_2 = String.valueOf(d);
        Number_3_3 = String.valueOf(c);
        Number_3_4 = String.valueOf(b);
        Diff_3 = String.valueOf((a - b) * (a - b));
    }

    private void createProblem_Level_42() {
        //  ROW 1
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = a + b;
        int d = Math.abs(a - b);

        Number_1_1 = String.valueOf(a);
        Number_1_2 = String.valueOf(b);
        Number_1_3 = String.valueOf(c);
        Number_1_4 = String.valueOf(d);
        Diff_1 = String.valueOf(Math.abs(c * c - d * d));

        //  ROW 2
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = a + b;
        d = Math.abs(a - b);

        Number_2_1 = String.valueOf(a);
        Number_2_2 = String.valueOf(b);
        Number_2_3 = String.valueOf(c);
        Number_2_4 = String.valueOf(d);
        Diff_2 = String.valueOf(Math.abs(c * c - d * d));

        //  ROW 3
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = a + b;
        d = Math.abs(a - b);

        Number_3_1 = String.valueOf(a);
        Number_3_2 = String.valueOf(b);
        Number_3_3 = String.valueOf(c);
        Number_3_4 = String.valueOf(d);
        Diff_3 = String.valueOf(Math.abs(c * c - d * d));
    }

    private void createProblem_Level_50() {
        //  ROW 1
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = a * a;
        int d = b * b;

        Number_1_1 = String.valueOf(a);
        Number_1_2 = String.valueOf(b);
        Number_1_3 = String.valueOf(c);
        Number_1_4 = String.valueOf(d);
        Diff_1 = String.valueOf(Math.abs(2 * c - d));

        //  ROW 2
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = a * a;
        d = b * b;

        Number_2_1 = String.valueOf(a);
        Number_2_2 = String.valueOf(b);
        Number_2_3 = String.valueOf(c);
        Number_2_4 = String.valueOf(d);
        Diff_2 = String.valueOf(Math.abs(2 * c - d));

        //  ROW 3
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = a * a;
        d = b * b;

        Number_3_1 = String.valueOf(a);
        Number_3_2 = String.valueOf(b);
        Number_3_3 = String.valueOf(c);
        Number_3_4 = String.valueOf(d);
        Diff_3 = String.valueOf(Math.abs(2 * c - d));
    }

    private void createProblem_Level_52() {
        //  ROW 1
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = a + 1;
        int d = b + 1;

        Number_1_1 = String.valueOf(a);
        Number_1_2 = String.valueOf(b);
        Number_1_3 = String.valueOf(c);
        Number_1_4 = String.valueOf(d);
        Diff_1 = String.valueOf(Math.abs(2 * a - 2 * b));

        //  ROW 2
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = a + 1;
        d = b + 1;

        Number_2_1 = String.valueOf(a);
        Number_2_2 = String.valueOf(b);
        Number_2_3 = String.valueOf(c);
        Number_2_4 = String.valueOf(d);
        Diff_2 = String.valueOf(Math.abs(2 * a - 2 * b));

        //  ROW 3
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = a + 1;
        d = b + 1;

        Number_3_1 = String.valueOf(a);
        Number_3_2 = String.valueOf(b);
        Number_3_3 = String.valueOf(c);
        Number_3_4 = String.valueOf(d);
        Diff_3 = String.valueOf(Math.abs(2 * a - 2 * b));
    }

    private void createProblem_Level_58() {
        //  ROW 1
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = MathUtility.getRandomPositiveNumber_1Digit();
        int d = MathUtility.getRandomPositiveNumber_1Digit();

        Number_1_1 = String.valueOf(a);
        Number_1_2 = String.valueOf(b);
        Number_1_3 = String.valueOf(c);
        Number_1_4 = String.valueOf(d);
        Diff_1 = String.valueOf(Math.abs(a * c - b * d));

        //  ROW 2
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        d = MathUtility.getRandomPositiveNumber_1Digit();

        Number_2_1 = String.valueOf(a);
        Number_2_2 = String.valueOf(b);
        Number_2_3 = String.valueOf(c);
        Number_2_4 = String.valueOf(d);
        Diff_2 = String.valueOf(Math.abs(a * c - b * d));

        //  ROW 3
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = MathUtility.getRandomPositiveNumber_1Digit();
        d = MathUtility.getRandomPositiveNumber_1Digit();

        Number_3_1 = String.valueOf(a);
        Number_3_2 = String.valueOf(b);
        Number_3_3 = String.valueOf(c);
        Number_3_4 = String.valueOf(d);
        Diff_3 = String.valueOf(Math.abs(a * c - b * d));
    }

    private void createProblem_Level_64() {
        //  ROW 1
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = a + 1;
        int d = b + 1;

        Number_1_1 = String.valueOf(a);
        Number_1_2 = String.valueOf(b);
        Number_1_3 = String.valueOf(c);
        Number_1_4 = String.valueOf(d);
        Diff_1 = String.valueOf(Math.abs(a + b));

        //  ROW 2
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = a + 1;
        d = b + 1;

        Number_2_1 = String.valueOf(a);
        Number_2_2 = String.valueOf(b);
        Number_2_3 = String.valueOf(c);
        Number_2_4 = String.valueOf(d);
        Diff_2 = String.valueOf(Math.abs(a + b));

        //  ROW 3
        a = MathUtility.getRandomPositiveNumber_1Digit();
        b = MathUtility.getRandomPositiveNumber_1Digit();
        c = a + 1;
        d = b + 1;

        Number_3_1 = String.valueOf(a);
        Number_3_2 = String.valueOf(b);
        Number_3_3 = String.valueOf(c);
        Number_3_4 = String.valueOf(d);
        Diff_3 = String.valueOf(Math.abs(a + b));
    }

    @Override
    public void createQuestion() {
        switch (MathUtility.getRandomPositiveNumber_4Digit() % 15){
            case 0:
                RIGHT_ANSWER = Number_1_1;
                Number_1_1 = "?";
                break;
            case 1:
                RIGHT_ANSWER = Number_1_2;
                Number_1_2 = "?";
                break;
            case 2:
                RIGHT_ANSWER = Number_1_3;
                Number_1_3 = "?";
                break;
            case 3:
                RIGHT_ANSWER = Number_1_4;
                Number_1_4 = "?";
                break;
            case 4:
                RIGHT_ANSWER = Diff_1;
                Diff_1 = "?";
                break;
            case 5:
                RIGHT_ANSWER = Number_2_1;
                Number_2_1 = "?";
                break;
            case 6:
                RIGHT_ANSWER = Number_2_2;
                Number_2_2 = "?";
                break;
            case 7:
                RIGHT_ANSWER = Number_2_3;
                Number_2_3 = "?";
                break;
            case 8:
                RIGHT_ANSWER = Number_2_4;
                Number_2_4 = "?";
                break;
            case 9:
                RIGHT_ANSWER = Diff_2;
                Diff_2 = "?";
                break;
            case 10:
                RIGHT_ANSWER = Number_3_1;
                Number_3_1 = "?";
                break;
            case 11:
                RIGHT_ANSWER = Number_3_2;
                Number_3_2 = "?";
                break;
            case 12:
                RIGHT_ANSWER = Number_3_3;
                Number_3_3 = "?";
                break;
            case 13:
                RIGHT_ANSWER = Number_3_4;
                Number_3_4 = "?";
                break;
            case 14:
                RIGHT_ANSWER = Diff_3;
                Diff_3 = "?";
                break;
            default:
                RIGHT_ANSWER = Number_3_1;
                Number_3_1 = "?";
                break;
        }

        RightAnswer = Integer.parseInt(RIGHT_ANSWER);
    }

    @Override
    public void createAnswer(){
        ANSWER_A = String.valueOf(RightAnswer);
        ANSWER_B = String.valueOf(RightAnswer + 1);
        ANSWER_C = String.valueOf(RightAnswer + 2);
        ANSWER_D = String.valueOf(RightAnswer - 1);
    }

    public String getNumber_1_1(){
        return Number_1_1;
    }

    public String getNumber_1_2(){
        return Number_1_2;
    }

    public String getNumber_1_3(){
        return Number_1_3;
    }

    public String getNumber_1_4(){
        return Number_1_4;
    }

    public String getDiff_1(){
        return Diff_1;
    }

    public String getNumber_2_1(){
        return Number_2_1;
    }

    public String getNumber_2_2(){
        return Number_2_2;
    }

    public String getNumber_2_3(){
        return Number_2_3;
    }

    public String getNumber_2_4(){
        return Number_2_4;
    }

    public String getDiff_2(){
        return Diff_2;
    }

    public String getNumber_3_1(){
        return Number_3_1;
    }

    public String getNumber_3_2(){
        return Number_3_2;
    }

    public String getNumber_3_3(){
        return Number_3_3;
    }

    public String getNumber_3_4(){
        return Number_3_4;
    }

    public String getDiff_3(){
        return Diff_3;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getRightAnswer(){
        return RIGHT_ANSWER;
    }

    @Override
    public String getAnswerA(){
        return ANSWER_A;
    }

    @Override
    public String getAnswerB(){
        return ANSWER_B;
    }

    @Override
    public String getAnswerC(){
        return ANSWER_C;
    }

    @Override
    public String getAnswerD(){
        return ANSWER_D;
    }
}
