package pckMath.pckGame.pckRiddle;

/**
 * Created by Cong on 5/30/2017.
 */

public abstract class FigureQuestion {

    public abstract void createProblem();
    public abstract void createQuestion();
    public abstract void createAnswer();
    public abstract void swapAnswer();
    public abstract String getRightAnswer();
    public abstract String getAnswerA();
    public abstract String getAnswerB();
    public abstract String getAnswerC();
    public abstract String getAnswerD();
}
