package pckMath.pckMathForKid;

import pckMath.MathUtility;

/**
 * Created by Cong on 9/24/2017.
 */

public class EvenAndOdd {

    private String INSTRUCTION;
    private int FIRST_NUMBER, SECOND_NUMBER, RIGHT_ANSWER;

    public EvenAndOdd(){
        createProblem();
    }

    private void createProblem(){
        int NoProblem = 2;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
            case 0:
                createProblem_01();
                break;
            case 1:
                createProblem_02();
                break;
            default:
                createProblem_01();
                break;
        }
    }

    private void createProblem_01(){
        INSTRUCTION = "Tag the <b>EVEN</b> number:";

        FIRST_NUMBER = MathUtility.getRandomPositiveNumber_1Digit();
        SECOND_NUMBER = FIRST_NUMBER + 1;

        if (FIRST_NUMBER % 2 == 0){
            RIGHT_ANSWER = FIRST_NUMBER;
        }
        else {
            RIGHT_ANSWER = SECOND_NUMBER;
        }
    }

    private void createProblem_02(){
        INSTRUCTION = "Tag the <b>ODD</b> number:";

        FIRST_NUMBER = MathUtility.getRandomPositiveNumber_1Digit();
        SECOND_NUMBER = FIRST_NUMBER + 1;

        if (FIRST_NUMBER % 2 != 0){
            RIGHT_ANSWER = FIRST_NUMBER;
        }
        else {
            RIGHT_ANSWER = SECOND_NUMBER;
        }
    }

    public String getInstruction(){
        return INSTRUCTION;
    }

    public int getFirstNumber(){
        return FIRST_NUMBER;
    }

    public int getSecondNumber(){
        return SECOND_NUMBER;
    }

    public int getRightAnswer(){
        return RIGHT_ANSWER;
    }

}
