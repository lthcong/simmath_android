package pckMath.pckMathForKid;

import pckMath.MathUtility;

/**
 * Created by Cong on 9/24/2017.
 */

public class WhoAmI {
    
    private String FIRST_STATEMENT, SECOND_STATEMENT;
    private String ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D, RIGHT_ANSWER;
    
    public WhoAmI(){
        createProblem();
        swapAnswer();
    }
    
    private void createProblem(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = a + b;
        
        FIRST_STATEMENT = "<b>I</b> + " + String.valueOf(b) + " = " + String.valueOf(c);
        
        int d = a + MathUtility.getRandomPositiveNumber_1Digit();
        int e = d - a;

        SECOND_STATEMENT = String.valueOf(d) + " - <b>I</b> = " + String.valueOf(e);
        
        RIGHT_ANSWER = String.valueOf(a);
        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a + 1);
        ANSWER_C = String.valueOf(a + 2);
        
        if (a - 1 > 0) {
            ANSWER_D = String.valueOf(a - 1);
        }
        else {
            ANSWER_D = String.valueOf(a + 3);
        }
    }
    
    public String getFirstStatement(){
        return FIRST_STATEMENT;
    }
    
    public String getSecondStatement(){
        return SECOND_STATEMENT;
    }
    
    public String getRightAnswer(){
        return RIGHT_ANSWER;
    }

    private void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++){
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index){
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    public String getAnswerA() {
        return ANSWER_A;
    }

    public String getAnswerB() {
        return ANSWER_B;
    }

    public String getAnswerC() {
        return ANSWER_C;
    }

    public String getAnswerD() {
        return ANSWER_D;
    }
    
}
