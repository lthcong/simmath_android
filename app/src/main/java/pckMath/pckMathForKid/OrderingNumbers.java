package pckMath.pckMathForKid;

import pckMath.MathUtility;

/**
 * Created by Cong on 9/24/2017.
 */

public class OrderingNumbers {

    private int FIRST_NUMBER, SECOND_NUMBER, THIRD_NUMBER, FORTH_NUMBER, FIFTH_NUMBER, SIXTH_NUMBER;

    public OrderingNumbers(){
        createProblem();
    }

    private void createProblem(){
        int Temp = MathUtility.getRandomPositiveNumber_1Digit();

        FIRST_NUMBER = Temp;
        SECOND_NUMBER = Temp + 1;
        THIRD_NUMBER = Temp + 2;
        FORTH_NUMBER = Temp + 3;
        FIFTH_NUMBER = Temp + 4;
        SIXTH_NUMBER = Temp + 5;
    }

    public int getFirstNumber(){
        return FIRST_NUMBER;
    }

    public int getSecondNumber(){
        return SECOND_NUMBER;
    }

    public int getThirdNumber(){
        return THIRD_NUMBER;
    }

    public int getForthNumber(){
        return FORTH_NUMBER;
    }

    public int getFifthNumber(){
        return FIFTH_NUMBER;
    }

    public int getSixthNumber(){
        return SIXTH_NUMBER;
    }

}
