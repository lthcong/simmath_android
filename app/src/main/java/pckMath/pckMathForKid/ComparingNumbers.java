package pckMath.pckMathForKid;

import pckMath.MathUtility;

/**
 * Created by Cong on 9/24/2017.
 */

public class ComparingNumbers {

    private int FIRST_NUMBER, SECOND_NUMBER;
    private String RIGHT_ANSWER;

    public ComparingNumbers(){
        createProblem();
    }

    private void createProblem(){
        FIRST_NUMBER = MathUtility.getRandomPositiveNumber_1Digit();
        SECOND_NUMBER = MathUtility.getRandomPositiveNumber_1Digit();

        if (FIRST_NUMBER > SECOND_NUMBER) {
            RIGHT_ANSWER = " > ";
        }
        else {
            if (FIRST_NUMBER < SECOND_NUMBER) {
                RIGHT_ANSWER = " < ";
            }
            else {
                RIGHT_ANSWER = " = ";
            }
        }
    }

    public int getFirstNumber(){
        return FIRST_NUMBER;
    }

    public int getSecondNumber(){
        return SECOND_NUMBER;
    }

    public String getRightAnswer(){
        return RIGHT_ANSWER;
    }

}
