package pckMath.pckMathForKid;

import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;

/**
 * Created by Cong on 1/15/2018.
 */

public class FindTheRightOperation extends MathProblem {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D;

    public FindTheRightOperation() {
        createProblem();
        swapAnswer();
    }

    public FindTheRightOperation(String Question,
                                 String RightAnswer,
                                 String AnswerA,
                                 String AnswerB,
                                 String AnswerC,
                                 String AnswerD) {
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;

        swapAnswer();
    }

    @Override
    public MathProblem createProblem() {
        QUESTION = "Can you find the right operation?";

        switch (InfoCollector.getFindTheRightOperation()){
            case 1:
                createProblem_Add();
                break;
            case 2:
                createProblem_Sub();
                break;
            case 3:
                createProblem_Mul();
                break;
            case 4:
                createProblem_Div();
                break;
            case 5:
                createProblem_Mix();
                break;
            default:
                createProblem_Add();
                break;
        }
        

        return new FindTheRightOperation(QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D);
    }

    private void createProblem_Mix(){
        switch (MathUtility.getRandomPositiveNumber_1Digit() % 4){
            case 0:
                createProblem_Add();
                break;
            case 1:
                createProblem_Sub();
                break;
            case 2:
                createProblem_Mul();
                break;
            case 3:
                createProblem_Div();
                break;
            default:
                createProblem_Add();
                break;
        }
    }
    
    private void createProblem_Add(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = a + b;
        
        RIGHT_ANSWER = String.valueOf(a) + " + " + String.valueOf(b) + " = " + String.valueOf(c);
        
        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a + 1) + " + " + String.valueOf(b) + " = " + String.valueOf(c);
        ANSWER_C = String.valueOf(a) + " + " + String.valueOf(b + 1) + " = " + String.valueOf(c);
        ANSWER_D = String.valueOf(a) + " + " + String.valueOf(b) + " = " + String.valueOf(c + 1);
    }

    private void createProblem_Sub(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        a = a + b;
        int c = a - b;

        RIGHT_ANSWER = String.valueOf(a) + " - " + String.valueOf(b) + " = " + String.valueOf(c);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a + 1) + " - " + String.valueOf(b) + " = " + String.valueOf(c);
        ANSWER_C = String.valueOf(a) + " - " + String.valueOf(b + 1) + " = " + String.valueOf(c);
        ANSWER_D = String.valueOf(a) + " - " + String.valueOf(b) + " = " + String.valueOf(c + 1);
    }

    private void createProblem_Mul(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        int c = a * b;

        RIGHT_ANSWER = String.valueOf(a) + " x " + String.valueOf(b) + " = " + String.valueOf(c);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a + 1) + " x " + String.valueOf(b) + " = " + String.valueOf(c);
        ANSWER_C = String.valueOf(a) + " x " + String.valueOf(b + 1) + " = " + String.valueOf(c);
        ANSWER_D = String.valueOf(a) + " x " + String.valueOf(b) + " = " + String.valueOf(c + 1);
    }

    private void createProblem_Div(){
        int a = MathUtility.getRandomPositiveNumber_1Digit();
        int b = MathUtility.getRandomPositiveNumber_1Digit();
        a = a * b;
        int c = a / b;

        RIGHT_ANSWER = String.valueOf(a) + InfoCollector.getDivisionSign() + String.valueOf(b) + " = " + String.valueOf(c);

        ANSWER_A = RIGHT_ANSWER;
        ANSWER_B = String.valueOf(a + 1) + InfoCollector.getDivisionSign() + String.valueOf(b) + " = " + String.valueOf(c);
        ANSWER_C = String.valueOf(a) + InfoCollector.getDivisionSign() + String.valueOf(b + 1) + " = " + String.valueOf(c);
        ANSWER_D = String.valueOf(a) + InfoCollector.getDivisionSign() + String.valueOf(b) + " = " + String.valueOf(c + 1);
    }

    @Override
    public String getQuestion() {
        return QUESTION;
    }

    @Override
    public String getRightAnswer() {
        return RIGHT_ANSWER;
    }

    @Override
    public void swapAnswer() {
        int swap_time = MathUtility.getRandomPositiveNumber_2Digit();

        for (int i = 0; i < swap_time; i++) {
            String temp = ANSWER_A;

            int swap_index = MathUtility.getRandomPositiveNumber_2Digit() % 3;
            switch (swap_index) {
                case 0:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
                case 1:
                    ANSWER_A = ANSWER_C;
                    ANSWER_C = temp;
                    break;
                case 2:
                    ANSWER_A = ANSWER_D;
                    ANSWER_D = temp;
                    break;
                default:
                    ANSWER_A = ANSWER_B;
                    ANSWER_B = temp;
                    break;
            }
        }
    }

    @Override
    public String getAnswerA() {
        return ANSWER_A;
    }

    @Override
    public String getAnswerB() {
        return ANSWER_B;
    }

    @Override
    public String getAnswerC() {
        return ANSWER_C;
    }

    @Override
    public String getAnswerD() {
        return ANSWER_D;
    }

}