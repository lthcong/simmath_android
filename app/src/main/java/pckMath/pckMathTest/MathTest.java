package pckMath.pckMathTest;

import java.util.ArrayList;

import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckMathTopics.Topic;
import pckMath.pckMathTopics.TopicCollection;

/**
 * Created by Cong on 4/26/2017.
 */

public class MathTest {

    private String TEST_NAME;
    private final String REG_TEST = "Regular Test", ACT_TEST = "ACT Test", SAT_TEST = "SAT Test", GED_TEST = "GED Test";
    private int NO_QUESTION, NO_RIGHT;
    private double SCORE;
    private int NoQuestionPerTopic = 5;
    private ArrayList<Topic> SELECTED_TOPICS;
    private ArrayList<MathQuestion> TEST_QUESTIONS;

    public MathTest(){
        if (InfoCollector.getRegularTestSelected()){
            createRegularTest();
        }
        else {
            if (InfoCollector.getACTTestSelected()){
                createACTTest();
            }
            else {
                if (InfoCollector.getSATTestSelected()){
                    createSATTest();
                }
                else {
                    if (InfoCollector.getGEDTestSelected()){
                        createGEDTest();
                    }
                    else {
                        createRegularTestQuestion();
                    }
                }
            }
        }
    }

    public MathTest(ArrayList<MathQuestion> TestQuestion){
        TEST_QUESTIONS = TestQuestion;
        NO_QUESTION = TestQuestion.size();
    }

    private void createRegularTest(){
        TEST_NAME = REG_TEST;

        if (InfoCollector.getMathStageSelection()){
            InfoCollector.clearSelectedTopics();
            InfoCollector.addToSelectedTopics(TopicCollection.getTopic(InfoCollector.getMathStageTopic()));
        }
        SELECTED_TOPICS = InfoCollector.getSelectedTopics();

        TEST_QUESTIONS = new ArrayList<>();
        setNoQuestion();
        createRegularTestQuestion();
    }

    private void createACTTest(){
        ACTTest _ACTTest = new ACTTest();

        TEST_NAME = ACT_TEST;

        NO_QUESTION = _ACTTest.getNoQuestion();
        TEST_QUESTIONS = _ACTTest.getQuestionList();

        InfoCollector.addToSelectedTopics(new Topic("All Level"));
    }

    private void createSATTest(){
        SATTest _SATTest = new SATTest();

        TEST_NAME = SAT_TEST;

        NO_QUESTION = _SATTest.getNoQuestion();
        TEST_QUESTIONS = _SATTest.getQuestionList();

        SELECTED_TOPICS = new ArrayList<>();
        InfoCollector.addToSelectedTopics(new Topic("Mainly focus on Algebra"));
    }

    private void createGEDTest(){
        GEDTest _GEDTest = new GEDTest();

        TEST_NAME = GED_TEST;

        NO_QUESTION = _GEDTest.getNoQuestion();
        TEST_QUESTIONS = _GEDTest.getQuestionList();

        SELECTED_TOPICS = new ArrayList<>();
        InfoCollector.addToSelectedTopics(new Topic("Part I: Basic Arithmetic and Geometry. Part II: Algebra I and II."));
    }

    private void setNoQuestion(){
        int size = SELECTED_TOPICS.size();
        switch (size) {
            case 1:
                NO_QUESTION = 10;
                break;
            default:
                NO_QUESTION = size * NoQuestionPerTopic;
                break;
        }
    }

    private void createRegularTestQuestion(){
        TEST_NAME = "Regular Test";
        int size = SELECTED_TOPICS.size();
        for (int i = 0; i < NO_QUESTION; i++){
            int Topic_Index = MathUtility.getRandomPositiveNumber_4Digit() % size;
            TEST_QUESTIONS.add(new MathQuestion(SELECTED_TOPICS.get(Topic_Index).getProblem()));
        }
    }

    public void gradeTest(){
        if (InfoCollector.getRegularTestSelected()){
            gradeRegularTest();
        }
        else {
            if (InfoCollector.getACTTestSelected()){
                gradeACTTest();
            }
            else {
                if (InfoCollector.getSATTestSelected()){
                    gradeRegularTest();
                    gradeSATTest();
                }
            }
        }
    }
    
    private void gradeRegularTest(){
        for (int i = 0; i < NO_QUESTION; i++){
            if (TEST_QUESTIONS.get(i).getYourAnswer().equalsIgnoreCase(TEST_QUESTIONS.get(i).getRightAnswer())){
                TEST_QUESTIONS.get(i).setResult(true);
                NO_RIGHT++;
            }
            else {
                TEST_QUESTIONS.get(i).setResult(false);
            }
        }

        SCORE = (double) NO_RIGHT / (double) NO_QUESTION;
        SCORE = SCORE * 100.0;
        SCORE = MathUtility.round_2_Decimal((SCORE));
    }
    
    private void gradeACTTest(){
        gradeRegularTest();
    }
    
    private void gradeSATTest(){
        switch (NO_RIGHT){
            case 2:
                SCORE = 210;
                break;
            case 3:
                SCORE = 230;
                break;
            case 4:
                SCORE = 240;
                break;
            case 5:
                SCORE = 260;
                break;
            case 6:
                SCORE = 280;
                break;
            case 7:
                SCORE = 290;
                break;
            case 8:
                SCORE = 310;
                break;
            case 9:
                SCORE = 320;
                break;
            case 10:
                SCORE = 330;
                break;
            case 11:
                SCORE = 340;
                break;
            case 12:
                SCORE = 360;
                break;
            case 13:
                SCORE = 370;
                break;
            case 14:
                SCORE = 380;
                break;
            case 15:
                SCORE = 390;
                break;
            case 16:
                SCORE = 410;
                break;
            case 17:
                SCORE = 420;
                break;
            case 18:
                SCORE = 430;
                break;
            case 19:
                SCORE = 440;
                break;
            case 20:
                SCORE = 450;
                break;
            case 21:
                SCORE = 460;
                break;
            case 22:
                SCORE = 470;
                break;
            case 23:
                SCORE = 480;
                break;
            case 24:
                SCORE = 480;
                break;
            case 25:
                SCORE = 490;
                break;
            case 26:
                SCORE = 500;
                break;
            case 27:
                SCORE = 510;
                break;
            case 28:
                SCORE = 520;
                break;
            case 29:
                SCORE = 520;
                break;
            case 30:
                SCORE = 530;
                break;
            case 31:
                SCORE = 540;
                break;
            case 32:
                SCORE = 550;
                break;
            case 33:
                SCORE = 560;
                break;
            case 34:
                SCORE = 560;
                break;
            case 35:
                SCORE = 570;
                break;
            case 36:
                SCORE = 580;
                break;
            case 37:
                SCORE = 590;
                break;
            case 38:
                SCORE = 600;
                break;
            case 39:
                SCORE = 600;
                break;
            case 40:
                SCORE = 610;
                break;
            case 41:
                SCORE = 620;
                break;
            case 42:
                SCORE = 630;
                break;
            case 43:
                SCORE = 640;
                break;
            case 44:
                SCORE = 650;
                break;
            case 45:
                SCORE = 660;
                break;
            case 46:
                SCORE = 670;
                break;
            case 47:
                SCORE = 670;
                break;
            case 48:
                SCORE = 680;
                break;
            case 49:
                SCORE = 690;
                break;
            case 50:
                SCORE = 700;
                break;
            case 51:
                SCORE = 710;
                break;
            case 52:
                SCORE = 730;
                break;
            case 53:
                SCORE = 740;
                break;
            case 54:
                SCORE = 750;
                break;
            case 55:
                SCORE = 760;
                break;
            case 56:
                SCORE = 780;
                break;
            case 57:
                SCORE = 790;
                break;
            case 58:
                SCORE = 800;
                break;
            default:
                SCORE = 200;
                break;
        }
    }

    public void setTestName(String TestName){
        TEST_NAME = TestName;
    }

    public String getTestName(){
        return TEST_NAME;
    }

    public ArrayList<MathQuestion> getTestQuestion(){
        return TEST_QUESTIONS;
    }

    public int getNoQuestion(){
        return NO_QUESTION;
    }

    public int getNoRight(){
        return NO_RIGHT;
    }

    public int getNoWrong(){
        return (NO_QUESTION - NO_RIGHT);
    }

    public void setScore(double Score){
        SCORE = Score;
    }

    public double getScore(){
        return SCORE;
    }

}
