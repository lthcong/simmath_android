package pckMath.pckMathTest;

import java.util.ArrayList;

import pckMath.MathUtility;
import pckMath.pckMathTopics.Topic;
import pckMath.pckProblemCollection.pckAlgebraOne.ReviewAlgebraOne;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.ReviewLinearEquationAndInequality;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.ReviewRadicalAndRationalExpression;
import pckMath.pckProblemCollection.pckAlgebraOne.pckPolynomial.pckIntroToPolynomial.ReviewPolynomial;
import pckMath.pckProblemCollection.pckAlgebraTwo.ReviewAlgebra2Level;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckTrigonometry.ReviewTrigonometry;
import pckMath.pckProblemCollection.pckBasicMath.ReviewBasicLevel;
import pckMath.pckProblemCollection.pckIntermediateMath.ReviewIntermediateLevel;
import pckMath.pckProblemCollection.pckPreAlgebra.ReviewPreAlgebraLevel;
import pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry.ReviewGeometry;
import pckMath.pckProblemCollection.pckPreAlgebra.pckRatioAndPercentage.ReviewRatioAndPercentage;
import pckMath.pckProblemCollection.pckPreAlgebra.pckStatistic.ReviewStatisticAndProbability;

/**
 * Created by Cong on 10/12/2017.
 */

public class ACTTest {

    private int NUMBER_OF_QUESTION = 60;
    private int NUMBER_OF_QUESTION_PER_SECTION = 12;

    private ArrayList<MathQuestion> QuestionList;

    public ACTTest(){
        QuestionList = new ArrayList<>();
        createQuestionList();
    }

    private void createQuestionList(){
        createPartIQuestion();
        createPartIIQuestion();
        createPartIIIQuestion();
        createPartIVQuestion();
        createPartVQuestion();
    }

    private void createPartIQuestion(){
        for (int i = 0; i < NUMBER_OF_QUESTION_PER_SECTION; i++){
            QuestionList.add(new MathQuestion(new ReviewBasicLevel()));
        }
    }

    private void createPartIIQuestion(){
        for (int i = 0; i < NUMBER_OF_QUESTION_PER_SECTION; i++){
            QuestionList.add(new MathQuestion(new ReviewIntermediateLevel()));
        }
    }

    private void createPartIIIQuestion(){
        for (int i = 0; i < NUMBER_OF_QUESTION_PER_SECTION; i++){
            QuestionList.add(new MathQuestion(new ReviewPreAlgebraLevel()));
        }
    }

    private void createPartIVQuestion(){
        for (int i = 0; i < NUMBER_OF_QUESTION_PER_SECTION; i++){
            QuestionList.add(new MathQuestion(new ReviewAlgebraOne()));
        }
    }

    private void createPartVQuestion(){
        for (int i = 0; i < NUMBER_OF_QUESTION_PER_SECTION; i++){
            QuestionList.add(new MathQuestion(new ReviewAlgebra2Level()));
        }
    }

    public int getNoQuestion(){
        return NUMBER_OF_QUESTION;
    }

    public ArrayList<MathQuestion> getQuestionList(){
        return QuestionList;
    }

}
