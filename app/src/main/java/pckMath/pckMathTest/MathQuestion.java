package pckMath.pckMathTest;

import pckMath.MathProblem;

/**
 * Created by Cong on 4/26/2017.
 */

public class MathQuestion {

    private String QUESTION, RIGHT_ANSWER, ANSWER_A, ANSWER_B, ANSWER_C, ANSWER_D, YOUR_ANSWER;
    private static String NO_ANSWER = "- No Answer -";
    private boolean RESULT;

    public MathQuestion(MathProblem Problem){
        QUESTION = Problem.getQuestion();
        RIGHT_ANSWER = Problem.getRightAnswer();
        ANSWER_A = Problem.getAnswerA();
        ANSWER_B = Problem.getAnswerB();
        ANSWER_C = Problem.getAnswerC();
        ANSWER_D = Problem.getAnswerD();

        YOUR_ANSWER = NO_ANSWER;
    }

    public MathQuestion(String Question,
                       String RightAnswer,
                       String AnswerA,
                       String AnswerB,
                       String AnswerC,
                       String AnswerD) {
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = AnswerA;
        ANSWER_B = AnswerB;
        ANSWER_C = AnswerC;
        ANSWER_D = AnswerD;
    }

    public MathQuestion(String Question, String RightAnswer, String YourAnswer, boolean Result){
        QUESTION = Question;
        RIGHT_ANSWER = RightAnswer;
        ANSWER_A = "";
        ANSWER_B = "";
        ANSWER_C = "";
        ANSWER_D = "";

        YOUR_ANSWER = YourAnswer;
        RESULT = Result;
    }

    public String getQuestion(){
        return QUESTION;
    }

    public String getRightAnswer(){
        return RIGHT_ANSWER;
    }

    public String getAnswerA(){
        return ANSWER_A;
    }

    public String getAnswerB(){
        return ANSWER_B;
    }

    public String getAnswerC(){
        return ANSWER_C;
    }

    public String getAnswerD(){
        return ANSWER_D;
    }

    public void setYourAnswer(String YourAnswer){
        YOUR_ANSWER = YourAnswer;
    }

    public String getYourAnswer(){
        return YOUR_ANSWER;
    }

    public void setResult(boolean result){
        RESULT = result;
    }

    public boolean getResult(){
        return RESULT;
    }

    public static String getNoAnswer(){
        return NO_ANSWER;
    }
}
