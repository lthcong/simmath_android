package pckMath.pckMathTest;

import java.util.ArrayList;

import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckLinearFunction.FindingTheIntersectionOfLinearFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckFunction.pckIntroToFunction.pckLinearFunction.ReviewLinearFunction;
import pckMath.pckProblemCollection.pckAlgebraOne.pckLinearEquation.ReviewLinearEquationAndInequality;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckAnalyticGeometry.pckCircle.FindingEquationOfACircle;
import pckMath.pckProblemCollection.pckAlgebraTwo.pckTrigonometry.ReviewTrigonometry;
import pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry.ReviewGeometry;

/**
 * Created by Cong on 10/13/2017.
 */

public class SATTest {

    private int NUMBER_OF_QUESTION = 58;
    private ArrayList<MathQuestion> QUESTION_LIST;

    public SATTest(){
        createQuestionList();
    }

    private void createQuestionList(){
        QUESTION_LIST = new ArrayList<>();
        int NoProblem = 3;

        for (int i = 0; i < NUMBER_OF_QUESTION - 4; i++){
            switch (MathUtility.getRandomPositiveNumber_4Digit() % NoProblem){
                case 0:
                    QUESTION_LIST.add(new MathQuestion(new ReviewLinearEquationAndInequality()));
                    break;
                case 1:
                    QUESTION_LIST.add(new MathQuestion(new ReviewLinearFunction()));
                    break;
                case 2:
                    QUESTION_LIST.add(new MathQuestion(new FindingTheIntersectionOfLinearFunction()));
                    break;
            }
        }

        QUESTION_LIST.add(new MathQuestion(new ReviewGeometry()));
        QUESTION_LIST.add(new MathQuestion(new ReviewGeometry()));
        QUESTION_LIST.add(new MathQuestion(new ReviewTrigonometry()));
        QUESTION_LIST.add(new MathQuestion(new FindingEquationOfACircle()));
    }

    public int getNoQuestion(){
        return NUMBER_OF_QUESTION;
    }

    public ArrayList<MathQuestion> getQuestionList(){
        return QUESTION_LIST;
    }

}
