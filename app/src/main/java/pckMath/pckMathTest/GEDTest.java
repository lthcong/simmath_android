package pckMath.pckMathTest;

import java.util.ArrayList;

import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckAlgebraOne.ReviewAlgebraOne;
import pckMath.pckProblemCollection.pckAlgebraTwo.ReviewAlgebra2Level;
import pckMath.pckProblemCollection.pckBasicMath.ReviewBasicLevel;
import pckMath.pckProblemCollection.pckPreAlgebra.pckGeometry.ReviewGeometry;

/**
 * Created by Cong on 10/13/2017.
 */

public class GEDTest {

    private int NUMBER_OF_QUESTION = 46;
    private int NUMBER_OF_PART_ONE_QUESTION = 5;
    private int NUMBER_OF_PART_TWO_QUESTION = NUMBER_OF_QUESTION - NUMBER_OF_PART_ONE_QUESTION;
    private ArrayList<MathQuestion> QUESTION_LIST;

    public GEDTest(){
        createTestQuestion();
    }

    private void createTestQuestion(){
        QUESTION_LIST = new ArrayList<>();

        //  FIRST PART
        for (int i = 0; i < NUMBER_OF_PART_ONE_QUESTION; i++) {
            if (MathUtility.getRandomBooleanValue()) {
                QUESTION_LIST.add(new MathQuestion(new ReviewBasicLevel()));
            }
            else {
                QUESTION_LIST.add(new MathQuestion(new ReviewGeometry()));
            }
        }

        //  SECOND PART
        for (int i = 0; i < NUMBER_OF_PART_TWO_QUESTION; i++) {
            if (MathUtility.getRandomBooleanValue()) {
                QUESTION_LIST.add(new MathQuestion(new ReviewAlgebraOne()));
            }
            else {
                QUESTION_LIST.add(new MathQuestion(new ReviewAlgebra2Level()));
            }
        }
    }

    public int getNoQuestion(){
        return NUMBER_OF_QUESTION;
    }

    public ArrayList<MathQuestion> getQuestionList(){
        return QUESTION_LIST;
    }

}
