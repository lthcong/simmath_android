package pckMath.pckHelp;

import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.*;

/**
 * Created by Cong on 9/20/2017.
 */

public class CalculatingFraction {

    public static String getAdditionSolution(Fraction FractionA, Fraction FractionB){
        String Solution = "";
        Fraction Result;

        if (FractionA.getDenominator() == FractionB.getDenominator()){
            Result  = FractionMath.addFractionWithoutSimplify(FractionA, FractionB);
            Solution += FractionA.toFullString() + " + " + FractionB.toFullString() + " = " + Result.toFullString() + "<br><br>";
        }
        else {
            int LCM = MathUtility.findLCM(FractionA.getDenominator(), FractionB.getDenominator());
            
            int MultipleA = LCM / FractionA.getDenominator();
            int MultipleB = LCM / FractionB.getDenominator();
            
            Fraction EquivalentA = FractionMath.findEquivalentFraction(FractionA, MultipleA);
            Fraction EquivalentB = FractionMath.findEquivalentFraction(FractionB, MultipleB);
            Result = FractionMath.addFractionWithoutSimplify(EquivalentA, EquivalentB);
            
            Solution += FractionA.toFullString() + " = " + EquivalentA.toFullString() + "<br><br>";
            Solution += FractionB.toFullString() + " = " + EquivalentB.toFullString() + "<br><br>";
            
            Solution += EquivalentA.toFullString() + " + " + EquivalentB.toFullString() + " = " + Result.toFullString() + "<br><br>";
        }

        if (Result.isInteger()){
            Solution += Result.toFullString() + " = " + Result.toString() + "<br><br>";
            Solution += FractionA.toString() + " + " + FractionB.toString() + " = " + Result.toString() + "<br><br>";
        }
        else {
            int GCF = MathUtility.findGCF(Result.getNumerator(), Result.getDenominator());
            if (GCF > 1) {
                Fraction SimplifiedResult = FractionMath.getSimplifiedFraction(Result);
                Solution += Result.toFullString() + " = " + SimplifiedResult.toFullString() + "<br><br>";
                Solution += FractionA.toFullString() + " + " + FractionB.toFullString() + " = " + SimplifiedResult.toFullString() + "<br><br>";
            }
        }
        
        return Solution;
    }

    public static String getAdditionSolution(MixedNumber Number, Fraction FractionB){
        String Solution = "";
        Fraction FractionA = FractionMath.convertToFraction(Number);
        Fraction Result;

        Solution += Number.toFullString() + " = " + FractionA.toFullString() + "<br><br>";

        if (FractionA.getDenominator() == FractionB.getDenominator()){
            Result  = FractionMath.addFractionWithoutSimplify(FractionA, FractionB);
            Solution += FractionA.toFullString() + " + " + FractionB.toFullString() + " = " + Result.toFullString() + "<br><br>";
        }
        else {
            int LCM = MathUtility.findLCM(FractionA.getDenominator(), FractionB.getDenominator());

            int MultipleA = LCM / FractionA.getDenominator();
            int MultipleB = LCM / FractionB.getDenominator();

            Fraction EquivalentA = FractionMath.findEquivalentFraction(FractionA, MultipleA);
            Fraction EquivalentB = FractionMath.findEquivalentFraction(FractionB, MultipleB);
            Result = FractionMath.addFractionWithoutSimplify(EquivalentA, EquivalentB);

            Solution += FractionA.toFullString() + " = " + EquivalentA.toFullString() + "<br><br>";
            Solution += FractionB.toFullString() + " = " + EquivalentB.toFullString() + "<br><br>";

            Solution += EquivalentA.toFullString() + " + " + EquivalentB.toFullString() + " = " + Result.toFullString() + "<br><br>";
        }

        if (Result.isInteger()){
            Solution += Result.toFullString() + " = " + Result.toString() + "<br><br>";
            Solution += Number.toString() + " + " + FractionB.toString() + " = " + Result.toString();
        }
        else {
            int GCF = MathUtility.findGCF(Result.getNumerator(), Result.getDenominator());
            if (GCF > 1) {
                Fraction SimplifiedResult = FractionMath.getSimplifiedFraction(Result);
                Solution += Result.toFullString() + " = " + SimplifiedResult.toFullString() + "<br><br>";
                Result = SimplifiedResult;
            }

            MixedNumber MixedResult = FractionMath.convertToMixedNumber(Result);
            Solution += Result.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";

            Solution += Number.toFullString() + " + " + FractionB.toFullString() + " = " + MixedResult.toFullString();
        }

        return Solution;
    }

    public static String getAdditionSolution(Fraction FractionA, MixedNumber Number){
        String Solution = "";
        Fraction FractionB = FractionMath.convertToFraction(Number);
        Fraction Result;

        Solution += Number.toFullString() + " = " + FractionB.toFullString() + "<br><br>";

        if (FractionA.getDenominator() == FractionB.getDenominator()){
            Result  = FractionMath.addFractionWithoutSimplify(FractionA, FractionB);
            Solution += FractionA.toFullString() + " + " + FractionB.toFullString() + " = " + Result.toFullString() + "<br><br>";
        }
        else {
            int LCM = MathUtility.findLCM(FractionA.getDenominator(), FractionB.getDenominator());

            int MultipleA = LCM / FractionA.getDenominator();
            int MultipleB = LCM / FractionB.getDenominator();

            Fraction EquivalentA = FractionMath.findEquivalentFraction(FractionA, MultipleA);
            Fraction EquivalentB = FractionMath.findEquivalentFraction(FractionB, MultipleB);
            Result = FractionMath.addFractionWithoutSimplify(EquivalentA, EquivalentB);

            Solution += FractionA.toFullString() + " = " + EquivalentA.toFullString() + "<br><br>";
            Solution += FractionB.toFullString() + " = " + EquivalentB.toFullString() + "<br><br>";

            Solution += EquivalentA.toFullString() + " + " + EquivalentB.toFullString() + " = " + Result.toFullString() + "<br><br>";
        }

        if (Result.isInteger()){
            Solution += Result.toFullString() + " = " + Result.toString() + "<br><br>";
            Solution += FractionA.toString() + " + " + Number.toString() + " = " + Result.toString();
        }
        else {
            if (MathUtility.findGCF(Result.getNumerator(), Result.getDenominator()) != 1) {
                Solution += Result.toFullString() + " = " + FractionMath.getSimplifiedFraction(Result).toFullString() + "<br><br>";
                Result = FractionMath.getSimplifiedFraction(Result);
            }

            MixedNumber MixedResult = FractionMath.convertToMixedNumber(Result);
            Solution += Result.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";

            Solution += FractionA.toFullString() + " + " + Number.toFullString() + " = " + MixedResult.toFullString();
        }

        return Solution;
    }

    public static String getAdditionSolution(MixedNumber MixedNumberA, MixedNumber MixedNumberB){
        String Solution = "";

        Fraction FractionA = FractionMath.convertToFraction(MixedNumberA);
        Solution += MixedNumberA.toFullString() + " = " + FractionA.toFullString() + "<br><br>";

        Fraction FractionB = FractionMath.convertToFraction(MixedNumberB);
        Solution += MixedNumberB.toFullString() + " = " + FractionB.toFullString() + "<br><br>";

        Fraction Result;

        if (FractionA.getDenominator() == FractionB.getDenominator()){
            Result  = FractionMath.addFractionWithoutSimplify(FractionA, FractionB);
            Solution += FractionA.toFullString() + " + " + FractionB.toFullString() + " = " + Result.toFullString() + "<br><br>";
        }
        else {
            int LCM = MathUtility.findLCM(FractionA.getDenominator(), FractionB.getDenominator());

            int MultipleA = LCM / FractionA.getDenominator();
            int MultipleB = LCM / FractionB.getDenominator();

            Fraction EquivalentA = FractionMath.findEquivalentFraction(FractionA, MultipleA);
            Fraction EquivalentB = FractionMath.findEquivalentFraction(FractionB, MultipleB);
            Result = FractionMath.addFractionWithoutSimplify(EquivalentA, EquivalentB);

            Solution += FractionA.toFullString() + " = " + EquivalentA.toFullString() + "<br><br>";
            Solution += FractionB.toFullString() + " = " + EquivalentB.toFullString() + "<br><br>";

            Solution += EquivalentA.toFullString() + " + " + EquivalentB.toFullString() + " = " + Result.toFullString() + "<br><br>";
        }

        if (Result.isInteger()){
            Solution += Result.toFullString() + " = " + Result.toString() + "<br><br>";
            Solution += MixedNumberA.toString() + " + " + MixedNumberB.toString() + " = " + Result.toString();
        }
        else {
            if (MathUtility.findGCF(Result.getNumerator(), Result.getDenominator()) != 1) {
                Solution += Result.toFullString() + " = " + FractionMath.getSimplifiedFraction(Result).toFullString() + "<br><br>";
                Result = FractionMath.getSimplifiedFraction(Result);
            }

            MixedNumber MixedResult = FractionMath.convertToMixedNumber(Result);
            Solution += Result.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";

            Solution += MixedNumberA.toFullString() + " + " + MixedNumberB.toFullString() + " = " + MixedResult.toFullString();
        }

        return Solution;
    }

    public static String getSubtractionSolution(Fraction FractionA, Fraction FractionB){
        String Solution = "";
        Fraction Result;

        if (FractionA.getDenominator() == FractionB.getDenominator()){
            Result  = FractionMath.subtractFractionWithoutSimplify(FractionA, FractionB);
            Solution += FractionA.toFullString() + " - " + FractionB.toFullString() + " = " + Result.toFullString() + "<br><br>";
        }
        else {
            int LCM = MathUtility.findLCM(FractionA.getDenominator(), FractionB.getDenominator());

            int MultipleA = LCM / FractionA.getDenominator();
            int MultipleB = LCM / FractionB.getDenominator();

            Fraction EquivalentA = FractionMath.findEquivalentFraction(FractionA, MultipleA);
            Fraction EquivalentB = FractionMath.findEquivalentFraction(FractionB, MultipleB);
            Result = FractionMath.subtractFractionWithoutSimplify(EquivalentA, EquivalentB);

            Solution += FractionA.toFullString() + " = " + EquivalentA.toFullString() + "<br><br>";
            Solution += FractionB.toFullString() + " = " + EquivalentB.toFullString() + "<br><br>";

            Solution += EquivalentA.toFullString() + " - " + EquivalentB.toFullString() + " = " + Result.toFullString() + "<br><br>";
        }
        if (Result.isInteger()){
            Solution += Result.toFullString() + " = " + Result.toString() + "<br><br>";
            Solution += FractionA.toString() + " - " + FractionB.toString() + " = " + Result.toString() + "<br><br>";
        }
        else {
            if (MathUtility.findGCF(Result.getNumerator(), Result.getDenominator()) != 1) {
                Solution += Result.toFullString() + " = " + FractionMath.getSimplifiedFraction(Result).toFullString() + "<br><br>";
                Result = FractionMath.getSimplifiedFraction(Result);
                Solution += FractionA.toFullString() + " - " + FractionB.toFullString() + " = " + Result.toFullString();
            }
        }

        return Solution;
    }

    public static String getSubtractionSolution(MixedNumber Number, Fraction FractionB){
        String Solution = "";
        Fraction FractionA = FractionMath.convertToFraction(Number);
        Fraction Result;

        Solution += Number.toFullString() + " = " + FractionA.toFullString() + "<br><br>";

        if (FractionA.getDenominator() == FractionB.getDenominator()){
            Result  = FractionMath.subtractFractionWithoutSimplify(FractionA, FractionB);
            Solution += FractionA.toFullString() + " - " + FractionB.toFullString() + " = " + Result.toFullString() + "<br><br>";
        }
        else {
            int LCM = MathUtility.findLCM(FractionA.getDenominator(), FractionB.getDenominator());

            int MultipleA = LCM / FractionA.getDenominator();
            int MultipleB = LCM / FractionB.getDenominator();

            Fraction EquivalentA = FractionMath.findEquivalentFraction(FractionA, MultipleA);
            Fraction EquivalentB = FractionMath.findEquivalentFraction(FractionB, MultipleB);
            Result = FractionMath.subtractFractionWithoutSimplify(EquivalentA, EquivalentB);

            Solution += FractionA.toFullString() + " = " + EquivalentA.toFullString() + "<br><br>";
            Solution += FractionB.toFullString() + " = " + EquivalentB.toFullString() + "<br><br>";

            Solution += EquivalentA.toFullString() + " - " + EquivalentB.toFullString() + " = " + Result.toFullString() + "<br><br>";
        }

        if (Result.isInteger()){
            Solution += Result.toFullString() + " = " + Result.toString() + "<br><br>";
            Solution += Number.toString() + " - " + FractionB.toString() + " = " + Result.toString() + "<br><br>";
        }
        else {

            if (MathUtility.findGCF(Result.getNumerator(), Result.getDenominator()) != 1) {
                Solution += Result.toFullString() + " = " + FractionMath.getSimplifiedFraction(Result).toFullString() + "<br><br>";
                Result = FractionMath.getSimplifiedFraction(Result);
            }

            if (Result.getValue() > 1.0) {
                MixedNumber MixedResult = FractionMath.convertToMixedNumber(Result);
                Solution += Result.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";

                Solution += Number.toFullString() + " - " + FractionB.toFullString() + " = " + MixedResult.toFullString();
            } else {
                Solution += Number.toFullString() + " - " + FractionB.toFullString() + " = " + Result.toFullString();
            }
        }

        return Solution;
    }

    public static String getSubtractionSolution(Fraction FractionA, MixedNumber Number){
        String Solution = "";
        Fraction FractionB = FractionMath.convertToFraction(Number);
        Fraction Result;

        Solution += Number.toFullString() + " = " + FractionB.toFullString() + "<br><br>";

        if (FractionA.getDenominator() == FractionB.getDenominator()){
            Result  = FractionMath.subtractFractionWithoutSimplify(FractionA, FractionB);
            Solution += FractionA.toFullString() + " - " + FractionB.toFullString() + " = " + Result.toFullString() + "<br><br>";
        }
        else {
            int LCM = MathUtility.findLCM(FractionA.getDenominator(), FractionB.getDenominator());

            int MultipleA = LCM / FractionA.getDenominator();
            int MultipleB = LCM / FractionB.getDenominator();

            Fraction EquivalentA = FractionMath.findEquivalentFraction(FractionA, MultipleA);
            Fraction EquivalentB = FractionMath.findEquivalentFraction(FractionB, MultipleB);
            Result = FractionMath.subtractFractionWithoutSimplify(EquivalentA, EquivalentB);

            Solution += FractionA.toFullString() + " = " + EquivalentA.toFullString() + "<br><br>";
            Solution += FractionB.toFullString() + " = " + EquivalentB.toFullString() + "<br><br>";

            Solution += EquivalentA.toFullString() + " - " + EquivalentB.toFullString() + " = " + Result.toFullString() + "<br><br>";
        }

        if (Result.isInteger()){
            Solution += Result.toFullString() + " = " + Result.toString() + "<br><br>";
            Solution += FractionA.toString() + " - " + Number.toString() + " = " + Result.toString() + "<br><br>";
        }
        else {
            if (MathUtility.findGCF(Result.getNumerator(), Result.getDenominator()) != 1) {
                Solution += Result.toFullString() + " = " + FractionMath.getSimplifiedFraction(Result).toFullString() + "<br><br>";
                Result = FractionMath.getSimplifiedFraction(Result);
            }

            if (Result.getValue() > 1.0) {
                MixedNumber MixedResult = FractionMath.convertToMixedNumber(Result);
                Solution += Result.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";

                Solution += FractionA.toFullString() + " - " + FractionB.toFullString() + " = " + MixedResult.toFullString();
            } else {
                Solution += FractionA.toFullString() + " - " + Number.toFullString() + " = " + Result.toFullString();
            }
        }

        return Solution;
    }

    public static String getSubtractionSolution(MixedNumber MixedNumberA, MixedNumber MixedNumberB){
        String Solution = "";

        Fraction FractionA = FractionMath.convertToFraction(MixedNumberA);
        Solution += MixedNumberA.toFullString() + " = " + FractionA.toFullString() + "<br><br>";

        Fraction FractionB = FractionMath.convertToFraction(MixedNumberB);
        Solution += MixedNumberB.toFullString() + " = " + FractionB.toFullString() + "<br><br>";

        Fraction Result;

        if (FractionA.getDenominator() == FractionB.getDenominator()){
            Result  = FractionMath.subtractFractionWithoutSimplify(FractionA, FractionB);
            Solution += FractionA.toFullString() + " - " + FractionB.toFullString() + " = " + Result.toFullString() + "<br><br>";
        }
        else {
            int LCM = MathUtility.findLCM(FractionA.getDenominator(), FractionB.getDenominator());

            int MultipleA = LCM / FractionA.getDenominator();
            int MultipleB = LCM / FractionB.getDenominator();

            Fraction EquivalentA = FractionMath.findEquivalentFraction(FractionA, MultipleA);
            Fraction EquivalentB = FractionMath.findEquivalentFraction(FractionB, MultipleB);
            Result = FractionMath.subtractFractionWithoutSimplify(EquivalentA, EquivalentB);

            Solution += FractionA.toFullString() + " = " + EquivalentA.toFullString() + "<br><br>";
            Solution += FractionB.toFullString() + " = " + EquivalentB.toFullString() + "<br><br>";

            Solution += EquivalentA.toFullString() + " - " + EquivalentB.toFullString() + " = " + Result.toFullString() + "<br><br>";
        }

        if (Result.isInteger()){
            Solution += Result.toFullString() + " = " + Result.toString() + "<br><br>";
            Solution += FractionA.toString() + " - " + FractionB.toString() + " = " + Result.toString() + "<br><br>";
        }
        else {
            if (MathUtility.findGCF(Result.getNumerator(), Result.getDenominator()) != 1) {
                Solution += Result.toFullString() + " = " + FractionMath.getSimplifiedFraction(Result).toFullString() + "<br><br>";
                Result = FractionMath.getSimplifiedFraction(Result);
            }

            if (Result.getValue() > 1.0) {
                MixedNumber MixedResult = FractionMath.convertToMixedNumber(Result);
                Solution += Result.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";

                Solution += MixedNumberA.toFullString() + " - " + MixedNumberB.toFullString() + " = " + MixedResult.toFullString();
            } else {
                Solution += MixedNumberA.toFullString() + " - " + MixedNumberB.toFullString() + " = " + Result.toFullString();
            }
        }

        return Solution;
    }

    public static String getMultiplicationSolution(Fraction FractionA, Fraction FractionB){
        String Solution = "";

        Fraction Result = FractionMath.multiplyFractionWithoutSimplify(FractionA, FractionB);
        Solution += FractionA.toFullString() + " x " + FractionB.toFullString() + " = " + Result.toFullString() + "<br><br>";

        if (Result.isInteger()){
            Solution += Result.toFullString() + " = " + Result.toString() + "<br><br>";
            Solution += FractionA.toString() + " x " + FractionB.toString() + " = " + Result.toString() + "<br><br>";
        }
        else {
            int GCF = MathUtility.findGCF(Result.getNumerator(), Result.getDenominator());
            if (GCF > 1) {
                Solution += Result.toFullString() + " = " + FractionMath.getSimplifiedFraction(Result).toFullString() + "<br><br>";
                Solution += FractionA.toFullString() + " x " + FractionB.toFullString() + " = " + FractionMath.getSimplifiedFraction(Result).toFullString() + "<br><br>";
            }
        }

        return Solution;
    }

    public static String getMultiplicationSolution(MixedNumber Number, Fraction FractionB){
        String Solution = "";

        Fraction FractionA = FractionMath.convertToFraction(Number);
        Solution += Number.toFullString() + " = " + FractionA.toFullString() + "<br><br>";

        Fraction Result = FractionMath.multiplyFractionWithoutSimplify(FractionA, FractionB);
        Solution += FractionA.toFullString() + " x " + FractionB.toFullString() + " = " + Result.toFullString() + "<br><br>";

        if (Result.isInteger()){
            Solution += Result.toFullString() + " = " + Result.toString() + "<br><br>";
            Solution += Number.toString() + " x " + FractionB.toString() + " = " + Result.toString() + "<br><br>";
        }
        else {
            int GCF = MathUtility.findGCF(Result.getNumerator(), Result.getDenominator());
            Fraction SimplifiedResult;
            if (GCF > 1) {
                SimplifiedResult = FractionMath.getSimplifiedFraction(Result);
                Solution += Result.toFullString() + " = " + SimplifiedResult.toFullString() + "<br><br>";
            } else {
                SimplifiedResult = Result;
            }

            if (SimplifiedResult.getValue() > 1.0) {
                MixedNumber MixedResult = FractionMath.convertToMixedNumber(SimplifiedResult);
                Solution += SimplifiedResult.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";

                Solution += Number.toFullString() + " x " + FractionB.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";
            } else {
                Solution += Number.toFullString() + " x " + FractionB.toFullString() + " = " + SimplifiedResult.toFullString() + "<br><br>";
            }
        }

        return Solution;
    }

    public static String getMultiplicationSolution(Fraction FractionA, MixedNumber Number){
        String Solution = "";

        Fraction FractionB = FractionMath.convertToFraction(Number);
        Solution += Number.toFullString() + " = " + FractionB.toFullString() + "<br><br>";

        Fraction Result = FractionMath.multiplyFractionWithoutSimplify(FractionA, FractionB);
        Solution += FractionA.toFullString() + " x " + FractionB.toFullString() + " = " + Result.toFullString() + "<br><br>";

        if (Result.isInteger()){
            Solution += Result.toFullString() + " = " + Result.toString() + "<br><br>";
            Solution += FractionA.toString() + " x " + Number.toString() + " = " + Result.toString() + "<br><br>";
        }
        else {
            int GCF = MathUtility.findGCF(Result.getNumerator(), Result.getDenominator());
            Fraction SimplifiedResult;
            if (GCF > 1) {
                SimplifiedResult = FractionMath.getSimplifiedFraction(Result);
                Solution += Result.toFullString() + " = " + SimplifiedResult.toFullString() + "<br><br>";
            } else {
                SimplifiedResult = Result;
            }

            if (SimplifiedResult.getValue() > 1.0) {
                MixedNumber MixedResult = FractionMath.convertToMixedNumber(SimplifiedResult);
                Solution += SimplifiedResult.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";

                Solution += FractionA.toFullString() + " x " + Number.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";
            } else {
                Solution += FractionA.toFullString() + " x " + Number.toFullString() + " = " + SimplifiedResult.toFullString() + "<br><br>";
            }
        }

        return Solution;
    }

    public static String getMultiplicationSolution(MixedNumber NumberA, MixedNumber NumberB){
        String Solution = "";

        Fraction FractionA = FractionMath.convertToFraction(NumberA);
        Solution += NumberA.toFullString() + " = " + FractionA.toFullString() + "<br><br>";

        Fraction FractionB = FractionMath.convertToFraction(NumberB);
        Solution += NumberB.toFullString() + " = " + FractionB.toFullString() + "<br><br>";

        Fraction Result = FractionMath.multiplyFractionWithoutSimplify(FractionA, FractionB);
        Solution += FractionA.toFullString() + " x " + FractionB.toFullString() + " = " + Result.toFullString() + "<br><br>";

        if (Result.isInteger()){
            Solution += Result.toFullString() + " = " + Result.toString() + "<br><br>";
            Solution += NumberA.toString() + " x " + NumberB.toString() + " = " + Result.toString() + "<br><br>";
        }
        else {
            int GCF = MathUtility.findGCF(Result.getNumerator(), Result.getDenominator());
            Fraction SimplifiedResult;
            if (GCF > 1) {
                SimplifiedResult = FractionMath.getSimplifiedFraction(Result);
                Solution += Result.toFullString() + " = " + SimplifiedResult.toFullString() + "<br><br>";
            } else {
                SimplifiedResult = Result;
            }

            if (SimplifiedResult.getValue() > 1.0) {
                MixedNumber MixedResult = FractionMath.convertToMixedNumber(SimplifiedResult);
                Solution += SimplifiedResult.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";

                Solution += NumberA.toFullString() + " x " + NumberB.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";
            } else {
                Solution += NumberA.toFullString() + " x " + NumberB.toFullString() + " = " + SimplifiedResult.toFullString() + "<br><br>";
            }
        }

        return Solution;
    }

    public static String getDivisionSolution(Fraction FractionA, Fraction FractionB){
        String Solution = "";

        Fraction ReciprocalB = FractionMath.getReciprocal(FractionB);
        Solution += FractionB.toFullString() + " => " + ReciprocalB.toFullString() + "<br><br>";

        Fraction Result = FractionMath.multiplyFractionWithoutSimplify(FractionA, ReciprocalB);
        Solution += FractionA.toFullString() + " x " + ReciprocalB.toFullString() + " = " + Result.toFullString() + "<br><br>";

        if (Result.isInteger()){
            Solution += Result.toFullString() + " = " + Result.toString() + "<br><br>";
            Solution += FractionA.toString() + InfoCollector.getDivisionSign() + FractionB.toString() + " = " + Result.toString() + "<br><br>";
        }
        else {
            int GCF = MathUtility.findGCF(Result.getNumerator(), Result.getDenominator());
            if (GCF > 1) {
                Fraction SimplifiedAnswer = FractionMath.getSimplifiedFraction(Result);

                Solution += Result.toFullString() + " = " + SimplifiedAnswer.toFullString() + "<br><br>";
                Solution += FractionA.toFullString() + InfoCollector.getDivisionSign() + FractionB.toFullString() + " = " + SimplifiedAnswer.toFullString() + "<br><br>";
            } else {
                Solution += FractionA.toFullString() + InfoCollector.getDivisionSign() + FractionB.toFullString() + " = " + Result.toFullString() + "<br><br>";
            }
        }

        return Solution;
    }

    public static String getDivisionSolution(MixedNumber Number, Fraction FractionB){
        String Solution = "";

        Fraction FractionA = FractionMath.convertToFraction(Number);
        Solution += Number.toFullString() + " = " + FractionA.toFullString() + "<br><br>";

        Fraction ReciprocalB = FractionMath.getReciprocal(FractionB);
        Solution += FractionB.toFullString() + " => " + ReciprocalB.toFullString() + "<br><br>";

        Fraction Result = FractionMath.multiplyFractionWithoutSimplify(FractionA, ReciprocalB);
        Solution += FractionA.toFullString() + " x " + ReciprocalB.toFullString() + " = " + Result.toFullString() + "<br><br>";

        int GCF = MathUtility.findGCF(Result.getNumerator(), Result.getDenominator());
        Fraction SimplifiedResult;
        if (GCF > 1){
            SimplifiedResult = FractionMath.getSimplifiedFraction(Result);
            Solution += Result.toFullString() + " = " + SimplifiedResult.toFullString() + "<br><br>";
        }
        else {
            SimplifiedResult = Result;
        }

        if (SimplifiedResult.isInteger()){
            Solution += Result.toFullString() + " = " + Result.toString() + "<br><br>";
            Solution += Number.toString() + InfoCollector.getDivisionSign() + FractionB.toString() + " = " + SimplifiedResult.toString() + "<br><br>";
        }
        else {
            if (SimplifiedResult.getValue() > 1.0) {
                MixedNumber MixedResult = FractionMath.convertToMixedNumber(SimplifiedResult);
                Solution += SimplifiedResult.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";

                Solution += Number.toFullString() + InfoCollector.getDivisionSign() + FractionB.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";
            } else {
                Solution += Number.toFullString() + InfoCollector.getDivisionSign() + FractionB.toFullString() + " = " + SimplifiedResult.toFullString() + "<br><br>";
            }
        }

        return Solution;
    }

    public static String getDivisionSolution(Fraction FractionA, MixedNumber Number){
        String Solution = "";

        Fraction FractionB = FractionMath.convertToFraction(Number);
        Solution += Number.toFullString() + " = " + FractionB.toFullString() + "<br><br>";

        Fraction ReciprocalB = FractionMath.getReciprocal(FractionB);
        Solution += FractionB.toFullString() + " => " + ReciprocalB.toFullString() + "<br><br>";

        Fraction Result = FractionMath.multiplyFractionWithoutSimplify(FractionA, ReciprocalB);
        Solution += FractionA.toFullString() + " x " + ReciprocalB.toFullString() + " = " + Result.toFullString() + "<br><br>";

        int GCF = MathUtility.findGCF(Result.getNumerator(), Result.getDenominator());
        Fraction SimplifiedResult;
        if (GCF > 1){
            SimplifiedResult = FractionMath.getSimplifiedFraction(Result);
            Solution += Result.toFullString() + " = " + SimplifiedResult.toFullString() + "<br><br>";
        }
        else {
            SimplifiedResult = Result;
        }

        if (SimplifiedResult.isInteger()){
            Solution += Result.toFullString() + " = " + Result.toString() + "<br><br>";
            Solution += Number.toString() + InfoCollector.getDivisionSign() + FractionB.toString() + " = " + SimplifiedResult.toString() + "<br><br>";
        }
        else {
            if (SimplifiedResult.getValue() > 1.0) {
                MixedNumber MixedResult = FractionMath.convertToMixedNumber(SimplifiedResult);
                Solution += SimplifiedResult.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";

                Solution += FractionA.toFullString() + InfoCollector.getDivisionSign() + Number.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";
            } else {
                Solution += FractionA.toFullString() + InfoCollector.getDivisionSign() + Number.toFullString() + " = " + SimplifiedResult.toFullString() + "<br><br>";
            }
        }

        return Solution;
    }

    public static String getDivisionSolution(MixedNumber NumberA, MixedNumber NumberB){
        String Solution = "";

        Fraction FractionA = FractionMath.convertToFraction(NumberA);
        Solution += NumberA.toFullString() + " = " + FractionA.toFullString() + "<br><br>";

        Fraction FractionB = FractionMath.convertToFraction(NumberB);
        Solution += NumberB.toFullString() + " = " + FractionB.toFullString() + "<br><br>";

        Fraction ReciprocalB = FractionMath.getReciprocal(FractionB);
        Solution += FractionB.toFullString() + " => " + ReciprocalB.toFullString() + "<br><br>";

        Fraction Result = FractionMath.multiplyFractionWithoutSimplify(FractionA, ReciprocalB);
        Solution += FractionA.toFullString() + " x " + ReciprocalB.toFullString() + " = " + Result.toFullString() + "<br><br>";

        int GCF = MathUtility.findGCF(Result.getNumerator(), Result.getDenominator());
        Fraction SimplifiedResult;
        if (GCF > 1){
            SimplifiedResult = FractionMath.getSimplifiedFraction(Result);
            Solution += Result.toFullString() + " = " + SimplifiedResult.toFullString() + "<br><br>";
        }
        else {
            SimplifiedResult = Result;
        }

        if (SimplifiedResult.isInteger()){
            Solution += Result.toFullString() + " = " + Result.toString() + "<br><br>";
            Solution += NumberA.toString() + InfoCollector.getDivisionSign() + NumberB.toString() + " = " + SimplifiedResult.toString() + "<br><br>";
        }
        else {
            if (SimplifiedResult.getValue() > 1.0) {
                MixedNumber MixedResult = FractionMath.convertToMixedNumber(SimplifiedResult);
                Solution += SimplifiedResult.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";

                Solution += NumberA.toFullString() + InfoCollector.getDivisionSign() + NumberB.toFullString() + " = " + MixedResult.toFullString() + "<br><br>";
            } else {
                Solution += NumberA.toFullString() + InfoCollector.getDivisionSign() + NumberB.toFullString() + " = " + SimplifiedResult.toFullString() + "<br><br>";
            }
        }

        return Solution;
    }
    
}
