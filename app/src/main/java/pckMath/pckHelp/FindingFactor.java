package pckMath.pckHelp;

import pckMath.MathUtility;
import pckString.StringUtility;

/**
 * Created by Cong on 9/18/2017.
 */

public class FindingFactor {

    private int NUMBER;
    private String SOLUTION;

    public FindingFactor(int Number){
        NUMBER = Number;
        getFactor();
    }

    private void getFactor(){
        SOLUTION = "";
        for (int i = 1; i < NUMBER; i++){
            if (NUMBER % i == 0){
                int j = NUMBER / i;
                SOLUTION += String.valueOf(NUMBER) + " = " + String.valueOf(i) + " x " + String.valueOf(j) + "<br><br>";
                if (i > j){
                    break;
                }
            }
        }

        SOLUTION += "<br><br>"
                + "All factor of " + String.valueOf(NUMBER) + " is "
                + StringUtility.writeArrayList(MathUtility.findAllFactor(NUMBER));
    }

    public int getNumber(){
        return NUMBER;
    }

    public String getSolution(){
        return SOLUTION;
    }

}
