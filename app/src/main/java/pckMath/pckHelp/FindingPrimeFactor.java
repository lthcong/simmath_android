package pckMath.pckHelp;

import java.util.ArrayList;

import pckMath.MathUtility;
import pckString.StringUtility;

/**
 * Created by Cong on 9/19/2017.
 */

public class FindingPrimeFactor {

    private int NUMBER;
    private String SOLUTION;

    public FindingPrimeFactor(int Number){
        NUMBER = Number;
        getFactor();
    }

    private void getFactor(){
        SOLUTION = String.valueOf(NUMBER) + " = ";
        ArrayList<Integer> FactorList = MathUtility.findAllPrimeFactor(NUMBER);
        int size = FactorList.size();
        for(int i = 0; i < size; i++){
            SOLUTION += String.valueOf(FactorList.get(i)) + " * ";
        }

        SOLUTION = SOLUTION.trim().substring(0, SOLUTION.length() - 2);
    }

    public int getNumber(){
        return NUMBER;
    }

    public String getSolution(){
        return SOLUTION;
    }
}
