package pckMath.pckHelp;

import java.util.ArrayList;

import pckMath.MathUtility;

/**
 * Created by Cong on 9/19/2017.
 */

public class FindingGCF {

    private int FIRST_NUMBER, SECOND_NUMBER;
    private String SOLUTION;

    public FindingGCF(int FirstNumber, int SecondNumber){
        FIRST_NUMBER = FirstNumber;
        SECOND_NUMBER = SecondNumber;

        getGCF();
    }

    private int getPrimeFactor(){
        int PrimeFactor = 1;
        int SmallerNumber = (int) MathUtility.getSmallerNumber(FIRST_NUMBER, SECOND_NUMBER);
        for (int i = 2; i <= SmallerNumber; i++){
            if (FIRST_NUMBER % i == 0 && SECOND_NUMBER % i == 0){
                PrimeFactor = i;
                break;
            }
        }

        return PrimeFactor;
    }

    private void getGCF(){
        SOLUTION = "";
        ArrayList<Integer> FactorList = new ArrayList<>();

        while(true){
            int TempFactor = getPrimeFactor();
            FactorList.add(TempFactor);
            FIRST_NUMBER = FIRST_NUMBER / TempFactor;
            SECOND_NUMBER = SECOND_NUMBER / TempFactor;

            //  FACTOR STEP
            SOLUTION += String.valueOf(TempFactor) + " | " + String.valueOf(FIRST_NUMBER) + "  " + String.valueOf(SECOND_NUMBER) + "<br><br>";
            if (TempFactor == 1){
                break;
            }
        }

        //  FINAL SOLUTION
        int GCF = 1;
        SOLUTION += "<br><br> GCF = ";
        int size = FactorList.size();
        for (int i = 0; i < size; i++){
            GCF = GCF * FactorList.get(i);
            SOLUTION += String.valueOf(FactorList.get(i)) + " * ";
        }
        SOLUTION = SOLUTION.trim().substring(0, SOLUTION.length() - 2);
        SOLUTION += " = " + String.valueOf(GCF);
    }

    public String getSolution(){
        return SOLUTION;
    }

}
