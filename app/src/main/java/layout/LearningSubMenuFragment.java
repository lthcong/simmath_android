package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.R;
import pckInfo.InfoCollector;

/**
 * A simple {@link Fragment} subclass.
 */
public class LearningSubMenuFragment extends Fragment {

    private Button btnAssessment, btnRegularTest,
                    btnACTTest, btnSATTest, btnGEDTest;

    private void clearSubMenuSelection(){
        btnAssessment.setTextColor(Color.GRAY);
        btnRegularTest.setTextColor(Color.GRAY);
        btnACTTest.setTextColor(Color.GRAY);
        btnSATTest.setTextColor(Color.GRAY);
        btnGEDTest.setTextColor(Color.GRAY);
    }

    private void showAssessmentSelection(){
        InfoCollector.setAssessmentTestSelected();
        btnAssessment.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        Objects.requireNonNull(getActivity()).getSupportFragmentManager().
                beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0).
                replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new AssessmentTestSelection()).commit();
    }

    private void showRegTestInfo(){
        InfoCollector.setRegularTestSelected();
        btnRegularTest.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager().
                beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0).
                replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new RegularTestInfoFragment()).commit();
    }

    private void showACTTestInfo(){
        InfoCollector.setACTTestSelected();
        btnACTTest.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new ACTTestInfoFragment())
                .commit();
    }

    private void showSATTestInfo(){
        InfoCollector.setSATTestSelected();
        btnSATTest.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new SATTestInfoFragment())
                .commit();
    }

    private void showGEDTestInfo(){
        InfoCollector.setGEDTestSelected();
        btnGEDTest.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new GEDTestInfoFragment())
                .commit();
    }

    private void startUp(){
        InfoCollector.setCurrentSubmenu(this);
        //  SET UP TEST
        InfoCollector.setupMathTest();

        //  CLEAR SELECTION
        clearSubMenuSelection();

        if (InfoCollector.getRegularTestSelected()){
            showRegTestInfo();
        }
        else {
            if (InfoCollector.getAssessmentTestSelected()){
                showAssessmentSelection();
            }
            else {
                if (InfoCollector.getACTTestSelected()){
                    showACTTestInfo();
                }
                else {
                    if (InfoCollector.getSATTestSelected()){
                        showSATTestInfo();
                    }
                    else {
                        if (InfoCollector.getGEDTestSelected()){
                            showGEDTestInfo();
                        }
                    }
                }
            }
        }
    }

    public LearningSubMenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_learning_sub_menu, container, false);

        //  GET REFERENCES
        btnAssessment = rootView.findViewById(R.id.btnAssessmentTest_LearningSubMenuFragment);
        btnRegularTest = rootView.findViewById(R.id.btnRegularTest_LearningSubMenuFragment);
        btnACTTest = rootView.findViewById(R.id.btnACTSelection_LearningSubMenuFragment);
        btnSATTest = rootView.findViewById(R.id.btnSATSelection_LearningSubMenuFragment);
        btnGEDTest = rootView.findViewById(R.id.btnGEDSelection_LearningSubMenuFragment);

        //  SET LISTENER
        SubMenuSelectionListener SubMenuSelection = new SubMenuSelectionListener();
        btnAssessment.setOnClickListener(SubMenuSelection);
        btnRegularTest.setOnClickListener(SubMenuSelection);
        btnACTTest.setOnClickListener(SubMenuSelection);
        btnSATTest.setOnClickListener(SubMenuSelection);
        btnGEDTest.setOnClickListener(SubMenuSelection);

        //  START UP
        startUp();

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private class SubMenuSelectionListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  CLEAR SELECTION
            clearSubMenuSelection();

            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnAssessmentTest_LearningSubMenuFragment:
                    showAssessmentSelection();
                    break;
                case R.id.btnRegularTest_LearningSubMenuFragment:
                    showRegTestInfo();
                    break;
                case R.id.btnACTSelection_LearningSubMenuFragment:
                    showACTTestInfo();
                    break;
                case R.id.btnSATSelection_LearningSubMenuFragment:
                    showSATTestInfo();
                    break;
                case R.id.btnGEDSelection_LearningSubMenuFragment:
                    showGEDTestInfo();
                    break;
            }
        }
    }

}
