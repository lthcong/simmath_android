package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.ArrayList;
import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.MathUtility;
import pckMath.pckMathTest.MathQuestion;
import pckMath.pckMathTopics.Topic;
import pckMath.pckMathTopics.TopicCollection;
import pckString.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorkBookFragment extends Fragment {

    private Button btnDone;
    private TextView tvTopic, tvWorkTime, tvNoRemainingQuestion,
                tvQuestionNumber, tvQuestionContent,
                tvAnswerResult,
                tvAnswerA, tvAnswerB, tvAnswerC, tvAnswerD;

    private MathQuestion Problem;
    private ArrayList<Topic> SelectedTopics;
    private int QuestionNumber;
    private int WorkingTime;
    private int Score;
    private boolean isChecked = false;
    private String WorkbookID;
    private String YourAnswer;
    private CountDownTimer cdtWorkingTime, cdtCountDownTime;

    private boolean isMathStageSelected = InfoCollector.getMathStageSelection();
    private int NoRemainingMathStageQuestion = InfoCollector.getMathStageNoWorkbookQuestion();


    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathWorkbook_Name));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void setUp(){
        if (isMathStageSelected){
            tvNoRemainingQuestion.setVisibility(View.VISIBLE);
            btnDone.setVisibility(View.GONE);
        }
        else {
            tvNoRemainingQuestion.setVisibility(View.GONE);
            btnDone.setVisibility(View.VISIBLE);
        }
    }

    private void showWorkTime(){
        int MaxTime = InfoCollector.getMaxTestTime();
        cdtWorkingTime = new CountDownTimer(MaxTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                WorkingTime++;
                String StWorkingTime = "Time: " + String.valueOf(StringUtility.getTimeString(WorkingTime * 1000));
                tvWorkTime.setText(StWorkingTime);
                InfoCollector.setWorkbookTime(WorkingTime);
            }

            @Override
            public void onFinish() {
                showWorkBookSummary();
            }
        }.start();
    }

    private void getProblem(){
        //  GET PROBLEM
        int size = SelectedTopics.size();
        int random_index = MathUtility.getRandomPositiveNumber_4Digit() % size;
        Problem = new MathQuestion(SelectedTopics.get(random_index).getProblem());

        //  SHOW TOPIC
        String TopicName = "<u>Topic:</u> " + SelectedTopics.get(random_index).getName();
        StringUtility.writeString(tvTopic, TopicName);
    }

    private void showQuestion(){
        //  QUESTION NUMBER
        String StQuestionNumber = "Question " + String.valueOf(QuestionNumber) + ":";
        tvQuestionNumber.setText(String.valueOf(StQuestionNumber));

        //  NO. REMAINING QUESTION
        String StNoRemainingQuestion = "Remaining: " + String.valueOf(NoRemainingMathStageQuestion);
        tvNoRemainingQuestion.setText(String.valueOf(StNoRemainingQuestion));

        //  QUESTION CONTENT
        getProblem();
        StringUtility.writeString(tvQuestionContent, Problem.getQuestion());

        //  HIDE ANSWER RESULT
        tvAnswerResult.setVisibility(View.GONE);

        //  CLEAR ANSWER SELECTION
        clearAnswerSelection();

        //  CLEAR YOUR ANSWER
        YourAnswer = "";

        //  SHOW MULTIPLE CHOICES
        isChecked = false;

        String AnswerA = getResources().getString(R.string.stCircledA) + ": " + Problem.getAnswerA();
        StringUtility.writeString(tvAnswerA, AnswerA);

        String AnswerB = getResources().getString(R.string.stCircledB) + ": " + Problem.getAnswerB();
        StringUtility.writeString(tvAnswerB, AnswerB);

        String AnswerC = getResources().getString(R.string.stCircledC) + ": " + Problem.getAnswerC();
        StringUtility.writeString(tvAnswerC, AnswerC);

        String AnswerD = getResources().getString(R.string.stCircledD) + ": " + Problem.getAnswerD();
        StringUtility.writeString(tvAnswerD, AnswerD);
    }

    private void showNext(){
        cdtCountDownTime = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                QuestionNumber++;

                if (isMathStageSelected){
                    if (NoRemainingMathStageQuestion <= 0){
                        showTransfer();
                    }
                    else {
                        showQuestion();
                    }
                }
                else {
                    showQuestion();
                }
            }

        }.start();
    }

    private void clearAnswerSelection(){
        tvAnswerA.setTextColor(Color.GRAY);
        tvAnswerB.setTextColor(Color.GRAY);
        tvAnswerC.setTextColor(Color.GRAY);
        tvAnswerD.setTextColor(Color.GRAY);
    }

    private void getYourAnswer(TextView tvSelectedAnswer){
        //  CLEAR SELECTION
        clearAnswerSelection();

        //  SET SELECTION
        tvSelectedAnswer.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        //  GET YOUR ANSWER
        String SelectedAnswer = tvSelectedAnswer.getText().toString().trim();
        if (SelectedAnswer.contains(getResources().getString(R.string.stCircledA))) {
            YourAnswer = Problem.getAnswerA();
        }
        else {
            if (SelectedAnswer.contains(getResources().getString(R.string.stCircledB))) {
                YourAnswer = Problem.getAnswerB();
            }
            else {
                if (SelectedAnswer.contains(getResources().getString(R.string.stCircledC))) {
                    YourAnswer = Problem.getAnswerC();
                }
                else {
                    YourAnswer = Problem.getAnswerD();
                }
            }
        }
    }

    private String getCorrectAnswerMessage(){
        String CorrectAnswer;

        switch (MathUtility.getRandomPositiveNumber_2Digit() % 6){
            case 0:
                CorrectAnswer = "GREAT JOB.";
                break;
            case 1:
                CorrectAnswer = "WONDERFUL.";
                break;
            case 2:
                CorrectAnswer = "FANTASTIC.";
                break;
            case 3:
                CorrectAnswer = "NICE ANSWER.";
                break;
            case 4:
                CorrectAnswer = "GOOD WORK.";
                break;
            default:
                CorrectAnswer = "CORRECT.";
                break;
        }

        return CorrectAnswer;
    }

    private void checkAnswer(TextView tvSelectedAnswer){
        //  GET YOUR ANSWER
        getYourAnswer(tvSelectedAnswer);

        //  CHECK ANSWER SELECTION
        if (YourAnswer.equalsIgnoreCase(Problem.getRightAnswer())){
            tvAnswerResult.setVisibility(View.VISIBLE);
            tvAnswerResult.setText(getCorrectAnswerMessage());
            tvAnswerResult.setTextColor(Color.GREEN);

            //  DECREASE NO REMAINING QUESTION
            NoRemainingMathStageQuestion --;

            //  SCORE
            if (!isChecked) {
                Score++;
            }

            //  SAVE PROBLEM
            saveMathProblem();

            //  SHOW NEXT
            showNext();
        }
        else {
            String IncorrectAnswer = "NOT CORRECT. TRY AGAIN!";

            tvAnswerResult.setVisibility(View.VISIBLE);
            tvAnswerResult.setText(IncorrectAnswer);
            tvAnswerResult.setTextColor(Color.RED);

            //  INCREASE NO REMAINING QUESTION
            NoRemainingMathStageQuestion ++;
        }

        //  CHECKED
        isChecked = true;

        //  UPDATE WORKBOOK
        InfoCollector.setWorkbookNoQuestion(QuestionNumber);
        InfoCollector.setWorkbookScore(Score);
        DatabaseFile.updateMathWorkbook(WorkbookID, WorkingTime, Score, QuestionNumber);
    }

    private void saveMathProblem(){
        DatabaseFile.addToMathProblemTable(WorkbookID, Problem.getQuestion(), YourAnswer, Problem.getRightAnswer(), "Right");
    }

    private void showWorkBookSummary(){
        try {
            InfoCollector.setWorkbookHistorySelected();
            Objects.requireNonNull(getActivity())
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                    .replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new HistorySubMenuFragment())
                    .commit();
        }
        catch (IllegalStateException e){ }
    }

    private void showAds(){
        Ads.showAds(this.getActivity());
    }

    private void showTransfer(){
        InfoCollector.setMathStageLevel(2);

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathStageTransferFragment())
                .commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        //  SET UP
        setUp();

        WorkbookID = InfoCollector.getWorkbookID();
        QuestionNumber = InfoCollector.getWorkbookNoQuestion();
        WorkingTime = InfoCollector.getWorkbookTime();
        Score = InfoCollector.getWorkbookScore();

        SelectedTopics = InfoCollector.getWorkbookTopics();

        showWorkTime();
        showQuestion();
    }

    public WorkBookFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_work_book, container, false);

        //  GET REFERENCES
        btnDone = rootView.findViewById(R.id.btnDone_WorkbookFragment);
        btnDone.setTextColor(Color.GRAY);

        tvTopic = rootView.findViewById(R.id.tvTopicName_WorkbookFragment);
        tvWorkTime = rootView.findViewById(R.id.tvWorkTime_WorkbookFragment);
        tvNoRemainingQuestion = rootView.findViewById(R.id.tvNoRemainingQuestion_WorkbookFragment);
        tvQuestionNumber = rootView.findViewById(R.id.tvQuestionNumber_WorkbookFragment);
        tvQuestionContent = rootView.findViewById(R.id.tvQuestionContent_WorkbookFragment);
        tvAnswerResult = rootView.findViewById(R.id.tvAnswerResult_WorkbookFragment);
        tvAnswerA = rootView.findViewById(R.id.tvAnswerA_WorkbookFragment);
        tvAnswerB = rootView.findViewById(R.id.tvAnswerB_WorkbookFragment);
        tvAnswerC = rootView.findViewById(R.id.tvAnswerC_WorkbookFragment);
        tvAnswerD = rootView.findViewById(R.id.tvAnswerD_WorkbookFragment);

        //  SET LISTENER
        AnswerSelectionListener selection_listener = new AnswerSelectionListener();
        btnDone.setOnClickListener(selection_listener);
        tvAnswerA.setOnClickListener(selection_listener);
        tvAnswerB.setOnClickListener(selection_listener);
        tvAnswerC.setOnClickListener(selection_listener);
        tvAnswerD.setOnClickListener(selection_listener);

        //  START UP
        startUp();

        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();

        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }

        if (cdtWorkingTime != null){
            cdtWorkingTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }

        if (cdtWorkingTime != null){
            cdtWorkingTime.cancel();
        }
    }

    private class AnswerSelectionListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tvAnswerA_WorkbookFragment:
                    checkAnswer((TextView) v);
                    break;
                case R.id.tvAnswerB_WorkbookFragment:
                    checkAnswer((TextView) v);
                    break;
                case R.id.tvAnswerC_WorkbookFragment:
                    checkAnswer((TextView) v);
                    break;
                case R.id.tvAnswerD_WorkbookFragment:
                    checkAnswer((TextView) v);
                    break;
                case R.id.btnDone_WorkbookFragment:
                    showWorkBookSummary();

                    //  ADS
                    showAds();
                    break;
            }
        }
    }

}
