package layout;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckInfo.InfoCollector;
import pckMath.pckGame.pckRiddle.pckFigureQuestion.FigureQuestion_04_Problem;

/**
 * A simple {@link Fragment} subclass.
 */
public class Figure_04RiddleGame extends Fragment {

    private TextView tvLevel, tvNumber_1_1, tvNumber_1_2, tvNumber_1_3, tvNumber_1_4, tvNumber_1_5, tvNumber_1_6,
            tvNumber_2_1, tvNumber_2_2, tvNumber_2_3, tvNumber_2_4, tvNumber_2_5, tvNumber_2_6,
            tvNumber_3_1, tvNumber_3_2, tvNumber_3_3, tvNumber_3_4, tvNumber_3_5, tvNumber_3_6;
    private Button btnAnswerA, btnAnswerB, btnAnswerC, btnAnswerD;
    private CountDownTimer cdtCountDownTime;
    private FigureQuestion_04_Problem Problem;
    private String AnswerSelection = "";
    private String QUESTION_MARK = "?";

    private ArrayList<TextView> TextViewList;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stRiddle));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void setTextViewList(){
        TextViewList = new ArrayList<>();

        TextViewList.add(tvNumber_1_1);
        TextViewList.add(tvNumber_1_2);
        TextViewList.add(tvNumber_1_3);
        TextViewList.add(tvNumber_1_4);
        TextViewList.add(tvNumber_1_5);
        TextViewList.add(tvNumber_1_6);
        TextViewList.add(tvNumber_2_1);
        TextViewList.add(tvNumber_2_2);
        TextViewList.add(tvNumber_2_3);
        TextViewList.add(tvNumber_2_4);
        TextViewList.add(tvNumber_2_5);
        TextViewList.add(tvNumber_2_6);
        TextViewList.add(tvNumber_3_1);
        TextViewList.add(tvNumber_3_2);
        TextViewList.add(tvNumber_3_3);
        TextViewList.add(tvNumber_3_4);
        TextViewList.add(tvNumber_3_5);
        TextViewList.add(tvNumber_3_6);
    }

    private void highLightedQuestionMark(){
        setTextViewList();

        int size = TextViewList.size();
        for(int i = 0; i < size; i++){
            if (TextViewList.get(i).getText().toString().trim().equalsIgnoreCase(QUESTION_MARK)){
                TextViewList.get(i).setTextColor(Color.RED);
                break;
            }
        }
    }

    private void showProblem(){
        //  LEVEL
        String StLevel = "Level " + String.valueOf(InfoCollector.getRiddleGameLevel()) + ":";
        tvLevel.setText(StLevel);

        //  GET PROBLEM
        Problem = new FigureQuestion_04_Problem();

        //  SHOW PROBLEM
        tvNumber_1_1.setText(Problem.getNumber_1_1());
        tvNumber_1_2.setText(Problem.getNumber_1_2());
        tvNumber_1_3.setText(Problem.getNumber_1_3());
        tvNumber_1_4.setText(Problem.getNumber_1_4());
        tvNumber_1_5.setText(Problem.getNumber_1_5());
        tvNumber_1_6.setText(Problem.getNumber_1_6());

        tvNumber_2_1.setText(Problem.getNumber_2_1());
        tvNumber_2_2.setText(Problem.getNumber_2_2());
        tvNumber_2_3.setText(Problem.getNumber_2_3());
        tvNumber_2_4.setText(Problem.getNumber_2_4());
        tvNumber_2_5.setText(Problem.getNumber_2_5());
        tvNumber_2_6.setText(Problem.getNumber_2_6());

        tvNumber_3_1.setText(Problem.getNumber_3_1());
        tvNumber_3_2.setText(Problem.getNumber_3_2());
        tvNumber_3_3.setText(Problem.getNumber_3_3());
        tvNumber_3_4.setText(Problem.getNumber_3_4());
        tvNumber_3_5.setText(Problem.getNumber_3_5());
        tvNumber_3_6.setText(Problem.getNumber_3_6());

        //  HIGHLIGHT QUESTION MARK
        highLightedQuestionMark();

        //  SHOW ANSWER
        clearAnswerSelection();
        enableAnswerSelection();

        String AnswerA = getResources().getString(R.string.stCircledA) + ":\t\t" + Problem.getAnswerA();
        btnAnswerA.setText(AnswerA);

        String AnswerB = getResources().getString(R.string.stCircledB) + ":\t\t" + Problem.getAnswerB();
        btnAnswerB.setText(AnswerB);

        String AnswerC = getResources().getString(R.string.stCircledC) + ":\t\t" + Problem.getAnswerC();
        btnAnswerC.setText(AnswerC);

        String AnswerD = getResources().getString(R.string.stCircledD) + ":\t\t" + Problem.getAnswerD();
        btnAnswerD.setText(AnswerD);
    }

    private void clearAnswerSelection(){
        btnAnswerA.setTextColor(Color.GRAY);
        btnAnswerB.setTextColor(Color.GRAY);
        btnAnswerC.setTextColor(Color.GRAY);
        btnAnswerD.setTextColor(Color.GRAY);
    }

    private void enableAnswerSelection(){
        btnAnswerA.setEnabled(true);
        btnAnswerB.setEnabled(true);
        btnAnswerC.setEnabled(true);
        btnAnswerD.setEnabled(true);
    }

    private void disableAnswerSelection(){
        btnAnswerA.setEnabled(false);
        btnAnswerB.setEnabled(false);
        btnAnswerC.setEnabled(false);
        btnAnswerD.setEnabled(false);
    }

    private void setAnswerSelection(Button btnAnswer){
        AnswerSelection = btnAnswer.getText().toString().trim();
        AnswerSelection = AnswerSelection.substring(AnswerSelection.indexOf(":") + 1).trim();
    }

    private void checkAnswerSelection(){
        //  CHECK SELECTION
        if (AnswerSelection.equalsIgnoreCase(Problem.getRightAnswer())){
            InfoCollector.setGameLevelCompleteStatus(true);
            InfoCollector.setRiddleGameLevel(InfoCollector.getRiddleGameLevel() + 1);
        }
        else {
            InfoCollector.setGameLevelCompleteStatus(false);
            InfoCollector.setRiddleGameLevel(1);
        }

        //  SHOW END GAME
        cdtCountDownTime = new CountDownTimer(500, 500) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                showEndGame();
            }
        }.start();
    }

    private void showEndGame(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new GameEndFragment())
                .commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        showProblem();
    }

    private void onClickMethod(Button ClickedButton){
        //  DISABLE ANSWER SELECTION
        disableAnswerSelection();

        //  SET ANSWER SELECTION
        setAnswerSelection(ClickedButton);

        //  CHECK ANSWER SELECTION
        checkAnswerSelection();
    }

    public Figure_04RiddleGame() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_figure_04_riddle_game, container, false);

        //  GET REFERENCES
        tvLevel = rootView.findViewById(R.id.tvLevel_Figure_04RiddleGame);
        tvNumber_1_1 = rootView.findViewById(R.id.tvNumber_1_1_FigureQuestion_04RiddleGame);
        tvNumber_1_2 = rootView.findViewById(R.id.tvNumber_1_2_FigureQuestion_04RiddleGame);
        tvNumber_1_3 = rootView.findViewById(R.id.tvNumber_1_3_FigureQuestion_04RiddleGame);
        tvNumber_1_4 = rootView.findViewById(R.id.tvNumber_1_4_FigureQuestion_04RiddleGame);
        tvNumber_1_5 = rootView.findViewById(R.id.tvNumber_1_5_FigureQuestion_04RiddleGame);
        tvNumber_1_6 = rootView.findViewById(R.id.tvNumber_1_6_FigureQuestion_04RiddleGame);
        tvNumber_2_1 = rootView.findViewById(R.id.tvNumber_2_1_FigureQuestion_04RiddleGame);
        tvNumber_2_2 = rootView.findViewById(R.id.tvNumber_2_2_FigureQuestion_04RiddleGame);
        tvNumber_2_3 = rootView.findViewById(R.id.tvNumber_2_3_FigureQuestion_04RiddleGame);
        tvNumber_2_4 = rootView.findViewById(R.id.tvNumber_2_4_FigureQuestion_04RiddleGame);
        tvNumber_2_5 = rootView.findViewById(R.id.tvNumber_2_5_FigureQuestion_04RiddleGame);
        tvNumber_2_6 = rootView.findViewById(R.id.tvNumber_2_6_FigureQuestion_04RiddleGame);
        tvNumber_3_1 = rootView.findViewById(R.id.tvNumber_3_1_FigureQuestion_04RiddleGame);
        tvNumber_3_2 = rootView.findViewById(R.id.tvNumber_3_2_FigureQuestion_04RiddleGame);
        tvNumber_3_3 = rootView.findViewById(R.id.tvNumber_3_3_FigureQuestion_04RiddleGame);
        tvNumber_3_4 = rootView.findViewById(R.id.tvNumber_3_4_FigureQuestion_04RiddleGame);
        tvNumber_3_5 = rootView.findViewById(R.id.tvNumber_3_5_FigureQuestion_04RiddleGame);
        tvNumber_3_6 = rootView.findViewById(R.id.tvNumber_3_6_FigureQuestion_04RiddleGame);
        btnAnswerA = rootView.findViewById(R.id.btnChoice_01_FigureQuestion_04RiddleGame);
        btnAnswerB = rootView.findViewById(R.id.btnChoice_02_FigureQuestion_04RiddleGame);
        btnAnswerC = rootView.findViewById(R.id.btnChoice_03_FigureQuestion_04RiddleGame);
        btnAnswerD = rootView.findViewById(R.id.btnChoice_04_FigureQuestion_04RiddleGame);

        //  SET LISTENER
        AnswerSelectionListener button_listener = new AnswerSelectionListener();
        btnAnswerA.setOnClickListener(button_listener);
        btnAnswerB.setOnClickListener(button_listener);
        btnAnswerC.setOnClickListener(button_listener);
        btnAnswerD.setOnClickListener(button_listener);

        //  START UP
        startUp();
        
        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    private class AnswerSelectionListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            try {
                onClickMethod((Button) v);
            } catch (Exception e) {
                InfoCollector.setGameLevelCompleteStatus(false);
                showEndGame();
            }
        }
    }

}
