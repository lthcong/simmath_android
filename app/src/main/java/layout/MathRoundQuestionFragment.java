package layout;


import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckData.pckFirebase.LogEvents;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.pckMathTopics.RoundCollection;
import pckMath.pckMathTopics.Topic;
import pckString.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class MathRoundQuestionFragment extends Fragment {

    private TextView tvTopicName,
            tvQuestionOne, tvQuestionTwo, tvQuestionThree, tvQuestionFour, tvQuestionFive,
            tvQuestionContent,
            tvResult,
            tvAnswerA, tvAnswerB, tvAnswerC, tvAnswerD;
    private CountDownTimer cdtCountDownTime;
    private Topic SelectedTopic;
    private int CurrentRound, CurrentLevel, NoQuestion = 1;
    private MathProblem Problem;
    private String YourAnswer;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathRound));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void logMathRound(){
        LogEvents.writeToLog(this.getActivity(), "StartMathRound", "MathRound", "MathRound");
    }

    private void showAds(){
        Ads.showAds(this.getActivity(), CurrentLevel);
    }

    private void showTopicName(){
        String StLevelString = "<b>Round " + String.valueOf(CurrentRound) + ":</b> " + SelectedTopic.getName();
        StringUtility.writeString(tvTopicName, StLevelString);
    }

    private void clearQuestionNumber(){
        tvQuestionOne.setTextColor(Color.GRAY);
        tvQuestionTwo.setTextColor(Color.GRAY);
        tvQuestionThree.setTextColor(Color.GRAY);
        tvQuestionFour.setTextColor(Color.GRAY);
        tvQuestionFive.setTextColor(Color.GRAY);
    }

    private void showQuestionNumber(){
        switch (NoQuestion){
            case 1:
                clearQuestionNumber();
                tvQuestionOne.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                break;
            case 2:
                tvQuestionTwo.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                break;
            case 3:
                tvQuestionThree.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                break;
            case 4:
                tvQuestionFour.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                break;
            case 5:
                tvQuestionFive.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                break;
        }
    }

    private void showQuestion(){
        //  QUESTION NUMBER
        showQuestionNumber();

        //  QUESTION CONTENT
        Problem = SelectedTopic.getProblem();
        StringUtility.writeString(tvQuestionContent, Problem.getQuestion());

        //  RESULT
        tvResult.setVisibility(View.GONE);

        //  ANSWER SELECTION
        String AnswerA = getResources().getString(R.string.stCircledA) + ": " + Problem.getAnswerA();
        StringUtility.writeString(tvAnswerA, AnswerA);
        tvAnswerA.setTextColor(Color.GRAY);

        String AnswerB = getResources().getString(R.string.stCircledB) + ": " + Problem.getAnswerB();
        StringUtility.writeString(tvAnswerB, AnswerB);
        tvAnswerB.setTextColor(Color.GRAY);

        String AnswerC = getResources().getString(R.string.stCircledC) + ": " + Problem.getAnswerC();
        StringUtility.writeString(tvAnswerC, AnswerC);
        tvAnswerC.setTextColor(Color.GRAY);

        String AnswerD = getResources().getString(R.string.stCircledD) + ": " + Problem.getAnswerD();
        StringUtility.writeString(tvAnswerD, AnswerD);
        tvAnswerD.setTextColor(Color.GRAY);
    }

    private void showNext(){
        cdtCountDownTime = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                if (NoQuestion <= 5) {
                    showQuestion();
                }
                else {
                    Cursor MaxLevel = DatabaseFile.getUserLevel();
                    MaxLevel.moveToFirst();
                    int CurrentMaxLevel = Integer.parseInt(MaxLevel.getString(0));

                    if (CurrentLevel > CurrentMaxLevel) {
                        DatabaseFile.updateUserLevel(CurrentLevel + 1);
                        DatabaseFile.updateUserRound(CurrentRound + 1);

                        InfoCollector.getUserInfo();
                    }

                    InfoCollector.setMathRoundLevel(CurrentRound + 1);
                    showMathRoundTransfer();
                }
            }
        }.start();
    }

    private void getYourAnswer(TextView tvSelectedAnswer){
        tvSelectedAnswer.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        //  GET YOUR ANSWER
        String SelectedAnswer = tvSelectedAnswer.getText().toString().trim();
        if (SelectedAnswer.contains(getResources().getString(R.string.stCircledA))) {
            YourAnswer = Problem.getAnswerA();
        }
        else {
            if (SelectedAnswer.contains(getResources().getString(R.string.stCircledB))) {
                YourAnswer = Problem.getAnswerB();
            }
            else {
                if (SelectedAnswer.contains(getResources().getString(R.string.stCircledC))) {
                    YourAnswer = Problem.getAnswerC();
                }
                else {
                    YourAnswer = Problem.getAnswerD();
                }
            }
        }
    }

    private void checkYourAnswer(){
        //  SHOW RESULT
        tvResult.setVisibility(View.VISIBLE);
        if (YourAnswer.equalsIgnoreCase(Problem.getRightAnswer())){
            NoQuestion++;

            tvResult.setText("CORRECT");
            tvResult.setTextColor(Color.GREEN);
        }
        else {
            NoQuestion = 1;

            tvResult.setText("NOT CORRECT");
            tvResult.setTextColor(Color.RED);
        }
    }

    private void showMathRoundTransfer(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathRoundTransferFragment())
                .commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();

        CurrentLevel = InfoCollector.getMathRoundSelectedLevel();
        CurrentRound = InfoCollector.getMathRoundLevel();
        SelectedTopic = RoundCollection.getTopic(CurrentLevel);

        //  TOPIC NAME
        showTopicName();

        //  QUESTION
        showQuestion();

        //  ADS
        showAds();

        logMathRound();
    }

    public MathRoundQuestionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_round_question, container, false);

        //  GET REFERENCES
        tvTopicName = (TextView) rootView.findViewById(R.id.tvTopicName_MathRoundQuestionFragment);

        tvQuestionOne = rootView.findViewById(R.id.tvQuestionOne_MathRoundQuestionFragment);
        tvQuestionTwo = rootView.findViewById(R.id.tvQuestionTwo_MathRoundQuestionFragment);
        tvQuestionThree = rootView.findViewById(R.id.tvQuestionThree_MathRoundQuestionFragment);
        tvQuestionFour = rootView.findViewById(R.id.tvQuestionFour_MathRoundQuestionFragment);
        tvQuestionFive = rootView.findViewById(R.id.tvQuestionFive_MathRoundQuestionFragment);

        tvQuestionContent = rootView.findViewById(R.id.tvQuestionContent_MathRoundQuestionFragment);
        tvResult = rootView.findViewById(R.id.tvResult_MathRoundQuestionFragment);

        tvAnswerA = rootView.findViewById(R.id.tvAnswerA_MathRoundQuestionFragment);
        tvAnswerB = rootView.findViewById(R.id.tvAnswerB_MathRoundQuestionFragment);
        tvAnswerC = rootView.findViewById(R.id.tvAnswerC_MathRoundQuestionFragment);
        tvAnswerD = rootView.findViewById(R.id.tvAnswerD_MathRoundQuestionFragment);

        //  SET LISTENER
        AnswerSelectionListener AnswerListener = new AnswerSelectionListener();
        tvAnswerA.setOnClickListener(AnswerListener);
        tvAnswerB.setOnClickListener(AnswerListener);
        tvAnswerC.setOnClickListener(AnswerListener);
        tvAnswerD.setOnClickListener(AnswerListener);

        //  START UP
        startUp();

        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    private class AnswerSelectionListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  GET YOUR ANSWER
            switch (v.getId()){
                case R.id.tvAnswerA_MathRoundQuestionFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.tvAnswerB_MathRoundQuestionFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.tvAnswerC_MathRoundQuestionFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.tvAnswerD_MathRoundQuestionFragment:
                    getYourAnswer((TextView) v);
                    break;
            }

            //  CHECK ANSWER
            checkYourAnswer();

            //  SHOW NEXT
            showNext();
        }
    }

}
