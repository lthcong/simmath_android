package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckData.pckFirebase.LogEvents;
import pckInfo.InfoCollector;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorkbookInfoFragment extends Fragment {

    private Button btnStartWorkbook;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathWorkbook_Name));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void logMathWorkbook(){
        LogEvents.writeToLog(this.getActivity(), "MathWorkbookSelected", "MathWorkbook", "MathWorkbook");
    }

    private void showWorkbookTopics(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new WorkbookTopicSelection())
                .commit();
    }

    private void startup(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        InfoCollector.setupWorkbook();
        logMathWorkbook();
    }

    public WorkbookInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_workbook_info, container, false);

        //  GET REFERENCES
        btnStartWorkbook = rootView.findViewById(R.id.btnStart_WorkbookInfoFragment);

        //  SET LISTENER
        ButtonClickListener button_listener = new ButtonClickListener();
        btnStartWorkbook.setOnClickListener(button_listener);
        btnStartWorkbook.setTextColor(Color.GRAY);

        //  START UP
        startup();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnStart_WorkbookInfoFragment:
                    showWorkbookTopics();
                    break;
            }
        }
    }

}
