package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckData.pckFirebase.LogEvents;
import pckInfo.InfoCollector;
import pckMath.pckMathTest.MathQuestion;
import pckMath.pckMathTopics.Topic;

/**
 * A simple {@link Fragment} subclass.
 */
public class MathLadderInfoFragment extends Fragment {

    private Button btnStart;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathLadder_Name));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void logMathLadder(){
        LogEvents.writeToLog(this.getActivity(), "MathLadderSelected", "MathLadder", "MathLadder");
    }

    private void showTopicSelection(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathLadderTopicSelection())
                .commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        InfoCollector.setupMathLadder();
        logMathLadder();
    }

    public MathLadderInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_ladder_info, container, false);

        //  GET REFERENCES
        btnStart = rootView.findViewById(R.id.btnStart_MathLadderInfoFragment);
        btnStart.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener button_listener = new ButtonClickListener();
        btnStart.setOnClickListener(button_listener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnStart_MathLadderInfoFragment:
                    showTopicSelection();
                    break;
            }
        }
    }

}
