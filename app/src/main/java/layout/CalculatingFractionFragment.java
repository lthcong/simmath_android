package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckInfo.InfoCollector;
import pckMath.pckHelp.CalculatingFraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.Fraction;
import pckMath.pckProblemCollection.pckBasicMath.pckFraction.MixedNumber;
import pckString.StringUtility;


/**
 * A simple {@link Fragment} subclass.
 */
public class CalculatingFractionFragment extends Fragment {

    private EditText edtInputOne, edtInputTwo;
    private Button btnAdd, btnSub, btnMul, btnDiv;
    private TextView tvInstruction, tvSolution;
    
    private int OperationType = 1;
    private String SOLUTION;
    private Fraction FractionOne, FractionTwo;
    private MixedNumber NumberOne, NumberTwo;
    private String EmptySolution = "<i>No Data</i>";

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stCalculationFraction));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void addBackButton(){
        getChildFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyButtonBackContainer_CalculatingFractionFragment, new ButtonBackToMathPocketFragment())
                .commit();
    }

    private void showInstruction(){
        Fraction SampleFraction = new Fraction(1, 2);
        MixedNumber SampleMixedNumber = new MixedNumber(1, SampleFraction);

        String Instruction = "<b>Fraction:</b> enter " + SampleFraction.toSimpleString() + " for " + SampleFraction.toString() + "<br>";
        Instruction += "<b>Mixed number:</b> enter " + SampleMixedNumber.toSimpleString() + " for " + SampleMixedNumber.toString() + "<br>";

        StringUtility.writeString(tvInstruction, Instruction);
    }

    private void clearSelection(){
        btnAdd.setTextColor(Color.GRAY);
        btnSub.setTextColor(Color.GRAY);
        btnMul.setTextColor(Color.GRAY);
        btnDiv.setTextColor(Color.GRAY);
    }
    
    private void getFirstNumber(){
        String InputString = edtInputOne.getText().toString().trim();
        try {
            int SpaceIndex = InputString.indexOf(" ");
            int DashIndex = InputString.indexOf("/");
            if (SpaceIndex != -1) {
                int Whole = Integer.parseInt(InputString.substring(0, SpaceIndex));

                InputString = InputString.substring(SpaceIndex + 1);
                DashIndex = InputString.indexOf("/");

                int Numerator = Integer.parseInt(InputString.substring(0, DashIndex));
                int Denominator = Integer.parseInt(InputString.substring(DashIndex + 1));

                FractionOne = new Fraction(0, 0);
                NumberOne = new MixedNumber(Whole, new Fraction(Numerator, Denominator));
            } else {
                NumberOne = new MixedNumber(0, new Fraction(0, 0));
                if (DashIndex != -1) {
                    int Numerator = Integer.parseInt(InputString.substring(0, DashIndex));
                    int Denominator = Integer.parseInt(InputString.substring(DashIndex + 1));

                    FractionOne = new Fraction(Numerator, Denominator);
                } else {
                    FractionOne = new Fraction(Integer.parseInt(InputString), 1);
                }
            }
        }
        catch (Exception e){
            StringUtility.writeString(tvSolution, EmptySolution);
        }
    }

    private void getSecondNumber(){
        String InputString = edtInputTwo.getText().toString().trim();
        try {
            int SpaceIndex = InputString.indexOf(" ");
            int DashIndex = InputString.indexOf("/");
            if (SpaceIndex != -1) {
                int Whole = Integer.parseInt(InputString.substring(0, SpaceIndex));

                InputString = InputString.substring(SpaceIndex + 1);
                DashIndex = InputString.indexOf("/");

                int Numerator = Integer.parseInt(InputString.substring(0, DashIndex));
                int Denominator = Integer.parseInt(InputString.substring(DashIndex + 1));

                FractionTwo = new Fraction(0, 0);
                NumberTwo = new MixedNumber(Whole, new Fraction(Numerator, Denominator));
            } else {
                NumberTwo = new MixedNumber(0, new Fraction(0, 0));
                if (DashIndex != -1) {
                    int Numerator = Integer.parseInt(InputString.substring(0, DashIndex));
                    int Denominator = Integer.parseInt(InputString.substring(DashIndex + 1));

                    FractionTwo = new Fraction(Numerator, Denominator);
                } else {
                    FractionTwo = new Fraction(Integer.parseInt(InputString), 1);
                }
            }
        }
        catch (Exception e){
            StringUtility.writeString(tvSolution, EmptySolution);
        }
    }
    
    private void showAdditionSolution(){
        try {
            SOLUTION = CalculatingFraction.getAdditionSolution(NumberOne, NumberTwo);
        }
        catch (Exception e1){
            try {
                SOLUTION = CalculatingFraction.getAdditionSolution(NumberOne, FractionTwo);
            }
            catch (Exception e2){
                try {
                    SOLUTION = CalculatingFraction.getAdditionSolution(FractionOne, NumberTwo);
                }
                catch (Exception e3){
                    try {
                        SOLUTION = CalculatingFraction.getAdditionSolution(FractionOne, FractionTwo);
                    }
                    catch (Exception e4){
                        SOLUTION = "Check your input.";
                    }
                }
            }   
        }
    }

    private void showSubtractionSolution(){
        try {
            SOLUTION = CalculatingFraction.getSubtractionSolution(NumberOne, NumberTwo);
        }
        catch (Exception e1){
            try {
                SOLUTION = CalculatingFraction.getSubtractionSolution(NumberOne, FractionTwo);
            }
            catch (Exception e2){
                try {
                    SOLUTION = CalculatingFraction.getSubtractionSolution(FractionOne, NumberTwo);
                }
                catch (Exception e3){
                    try {
                        SOLUTION = CalculatingFraction.getSubtractionSolution(FractionOne, FractionTwo);
                    }
                    catch (Exception e4){
                        SOLUTION = "Check your input.";
                    }
                }
            }
        }
    }

    private void showMultiplicationSolution(){
        try {
            SOLUTION = CalculatingFraction.getMultiplicationSolution(NumberOne, NumberTwo);
        }
        catch (Exception e1){
            try {
                SOLUTION = CalculatingFraction.getMultiplicationSolution(NumberOne, FractionTwo);
            }
            catch (Exception e2){
                try {
                    SOLUTION = CalculatingFraction.getMultiplicationSolution(FractionOne, NumberTwo);
                }
                catch (Exception e3){
                    try {
                        SOLUTION = CalculatingFraction.getMultiplicationSolution(FractionOne, FractionTwo);
                    }
                    catch (Exception e4){
                        SOLUTION = "Check your input.";
                    }
                }
            }
        }
    }

    private void showDivisionSolution(){
        try {
            SOLUTION = CalculatingFraction.getDivisionSolution(NumberOne, NumberTwo);
        }
        catch (Exception e1){
            try {
                SOLUTION = CalculatingFraction.getDivisionSolution(NumberOne, FractionTwo);
            }
            catch (Exception e2){
                try {
                    SOLUTION = CalculatingFraction.getDivisionSolution(FractionOne, NumberTwo);
                }
                catch (Exception e3){
                    try {
                        SOLUTION = CalculatingFraction.getDivisionSolution(FractionOne, FractionTwo);
                    }
                    catch (Exception e4){
                        SOLUTION = "Check your input.";
                    }
                }
            }
        }
    }

    private void showSolution(){
        try {
            switch (OperationType) {
                case 1:
                    showAdditionSolution();
                    break;
                case 2:
                    showSubtractionSolution();
                    break;
                case 3:
                    showMultiplicationSolution();
                    break;
                case 4:
                    showDivisionSolution();
                    break;
                default:
                    showDivisionSolution();
                    break;
            }

            //  SHOW SOLUTION
            StringUtility.writeString(tvSolution, SOLUTION);
        }
        catch (Exception e){
            StringUtility.writeString(tvSolution, EmptySolution);
        }
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        addBackButton();
        edtInputOne.requestFocus();
        StringUtility.writeString(tvSolution, EmptySolution);
        clearSelection();
        showInstruction();
    }

    public CalculatingFractionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_calculating_fraction, container, false);

        //  GET REFERENCES
        edtInputOne = rootView.findViewById(R.id.edtInputOne_CalculatingFractionFragment);
        edtInputTwo = rootView.findViewById(R.id.edtInputTwo_CalculatingFractionFragment);
        btnAdd = rootView.findViewById(R.id.btnAdd_CalculatingFractionFragment);
        btnSub = rootView.findViewById(R.id.btnSub_CalculatingFractionFragment);
        btnMul = rootView.findViewById(R.id.btnMul_CalculatingFractionFragment);
        btnDiv = rootView.findViewById(R.id.btnDiv_CalculatingFractionFragment);
        tvInstruction = rootView.findViewById(R.id.tvInstruction_CalculatingFractionFragment);
        tvSolution = rootView.findViewById(R.id.tvSolutionDetail_CalculatingFractionFragment);

        //  SET LISTENER
        InputListener InputNumberListener = new InputListener();
        edtInputOne.addTextChangedListener(InputNumberListener);
        edtInputTwo.addTextChangedListener(InputNumberListener);
        
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnAdd.setOnClickListener(ButtonListener);
        btnSub.setOnClickListener(ButtonListener);
        btnMul.setOnClickListener(ButtonListener);
        btnDiv.setOnClickListener(ButtonListener);
        

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  CLEAR SELECTION
            clearSelection();

            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnAdd_CalculatingFractionFragment:
                    OperationType = 1;
                    break;
                case R.id.btnSub_CalculatingFractionFragment:
                    OperationType = 2;
                    break;
                case R.id.btnMul_CalculatingFractionFragment:
                    OperationType = 3;
                    break;
                case R.id.btnDiv_CalculatingFractionFragment:
                    OperationType = 4;
                    break;
            }

            //  SHOW SOLUTION
            showSolution();
        }
    }
    
    private class InputListener implements TextWatcher{

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            getFirstNumber();
            getSecondNumber();
        }

        @Override
        public void afterTextChanged(Editable s) {
            showSolution();
        }
    }
}
