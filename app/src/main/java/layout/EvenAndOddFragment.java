package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckData.pckFirebase.LogEvents;
import pckInfo.InfoCollector;
import pckMath.pckMathForKid.EvenAndOdd;
import pckString.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class EvenAndOddFragment extends Fragment {

    private TextView tvInstruction, tvResult;
    private Button btnFirstNumber, btnSecondNumber;
    private CountDownTimer cdtCountDownTime;
    private int NoProblem = 0;
    private EvenAndOdd Problem;
    private String YourAnswer;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stEvenAndOdd));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void addBackButton(){
        getChildFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyButtonBackContainer_EvenAndOddFragment, new ButtonBackToMathForKidFragment())
                .commit();
    }

    private void getProblem(){
        NoProblem++;
        Problem = new EvenAndOdd();

        //  INSTRUCTION
        StringUtility.writeString(tvInstruction, Problem.getInstruction());

        //  HIDE RESULT
        tvResult.setVisibility(View.GONE);

        //  SHOW NUMBERS
        btnFirstNumber.setTextColor(Color.GRAY);
        btnFirstNumber.setText(String.valueOf(Problem.getFirstNumber()));

        btnSecondNumber.setTextColor(Color.GRAY);
        btnSecondNumber.setText(String.valueOf(Problem.getSecondNumber()));
    }

    private void checkAnswer(Button btnSelectedAnswer){
        //  GET YOUR ANSWER
        YourAnswer = btnSelectedAnswer.getText().toString().trim();

        //  CHECK ANSWER
        tvResult.setVisibility(View.VISIBLE);
        if (YourAnswer.equalsIgnoreCase(String.valueOf(Problem.getRightAnswer()))){
            tvResult.setText("CORRECT");
            tvResult.setTextColor(Color.GREEN);
        }
        else {
            tvResult.setText("NOT CORRECT");
            tvResult.setTextColor(Color.RED);
        }

        //  SHOW NEW PROBLEM
        cdtCountDownTime = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                getProblem();
            }

        }.start();
    }

    private void showAds(){
        Ads.showAds(this.getActivity(), NoProblem);
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        addBackButton();
        getProblem();
    }

    public EvenAndOddFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_even_and_odd, container, false);

        //  GET REFERENCES
        tvInstruction = rootView.findViewById(R.id.tvInstruction_EvenAndOddFragment);
        tvResult = rootView.findViewById(R.id.tvResult_EvenAndOddFragment);
        btnFirstNumber = rootView.findViewById(R.id.btnFirstNumber_EvenAndOddFragment);
        btnSecondNumber = rootView.findViewById(R.id.btnSecondNumber_EvenAndOddFragment);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnFirstNumber.setOnClickListener(ButtonListener);
        btnSecondNumber.setOnClickListener(ButtonListener);

        //  START UP
        startUp();

        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnFirstNumber_EvenAndOddFragment:
                    checkAnswer((Button) v);
                    break;
                case R.id.btnSecondNumber_EvenAndOddFragment:
                    checkAnswer((Button) v);
                    break;
            }

            //  ADS
            showAds();
        }
    }

}
