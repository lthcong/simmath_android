package layout;


import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckMathTopics.TopicCollection;

/**
 * A simple {@link Fragment} subclass.
 */
public class MathStageTransferFragment extends Fragment {

    private TextView tvMathStageLevel, tvMathStageGoal;
    private CountDownTimer cdtCountDownTime;
    private String LEVEL_1 = "Stage 1: Math Workbook.",
                    GOAL_1 = "Goal: 50 correct problems.",
                    LEVEL_2 = "Stage 2: Math Ladder.",
                    GOAL_2 = "Goal: 100 points OR more.",
                    LEVEL_3 = "Stage 3: Math Test.",
                    GOAL_3 = "Goal: 90 percent OR more.";

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathStage_Name));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void showLevelInfo(){
        String MathStageLevel, MathStageGoal;
        switch (InfoCollector.getMathStageLevel()){
            case 1:
                MathStageLevel = LEVEL_1;
                MathStageGoal = GOAL_1;
                break;
            case 2:
                MathStageLevel = LEVEL_2;
                MathStageGoal = GOAL_2;
                break;
            case 3:
                MathStageLevel = LEVEL_3;
                MathStageGoal = GOAL_3;
                break;
            default:
                MathStageLevel = LEVEL_1;
                MathStageGoal = GOAL_1;
                break;
        }

        tvMathStageLevel.setText(MathStageLevel);
        tvMathStageGoal.setText(MathStageGoal);
    }

    private void showStageOne(){
        try {
            Objects.requireNonNull(getActivity())
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                    .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new WorkBookFragment())
                    .commit();
        }
        catch (Exception e){
            autoTransfer();
        }
    }

    private void showStageTwo(){
        try {
            Objects.requireNonNull(getActivity())
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                    .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathLadderFragment())
                    .commit();
        }
        catch (Exception e){
            autoTransfer();
        }
    }

    private void showStageThree(){
        try {
            Objects.requireNonNull(getActivity())
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                    .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new RegularTestFragment())
                    .commit();
        }
        catch (Exception e){
            autoTransfer();
        }
    }

    private void showNextStage(){
        switch (InfoCollector.getMathStageLevel()){
            case 1:
                showStageOne();
                break;
            case 2:
                showStageTwo();
                break;
            case 3:
                showStageThree();
                break;
            default:
                showStageOne();
                break;
        }
    }

    private void autoTransfer(){
        cdtCountDownTime = new CountDownTimer(1000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                showNextStage();
            }

        }.start();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        showLevelInfo();
        autoTransfer();
    }

    public MathStageTransferFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_stage_transfer, container, false);

        //  GET REFERENCES
        tvMathStageLevel = rootView.findViewById(R.id.tvMathStageLevel_MathStageTransferFragment);
        tvMathStageGoal = rootView.findViewById(R.id.tvMathStageGoal_MathStageTransferFragment);

        //  START UP
        startUp();

        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }
}
