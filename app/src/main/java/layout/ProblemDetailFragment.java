package layout;


import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckString.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProblemDetailFragment extends Fragment {

    private Button btnNext;
    private LinearLayout lnlyProblemList;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stProblemDetail));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void showProblemList(){
        Cursor ProblemList = DatabaseFile.getProblemByLinkedID(InfoCollector.getLinkedID());
        int Count = 0, AdsStep = Ads.WRAPPED_ADS_STEP;

        lnlyProblemList.removeAllViews();

        if (ProblemList.getCount() > 0){
            while(ProblemList.moveToNext()){
                LinearLayout.LayoutParams TestLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                LinearLayout lnlyTestLayout = new LinearLayout(this.getActivity());
                lnlyTestLayout.setLayoutParams(TestLayoutParams);
                lnlyTestLayout.setOrientation(LinearLayout.VERTICAL);

                //  ADS
                Count++;
                if (Count % AdsStep == 0){
                    Ads.showWrappedAds(this.getActivity(), lnlyProblemList);
                }

                TextView tvQuestion = new TextView(this.getActivity());
                tvQuestion.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvQuestion.setBackgroundColor(Color.WHITE);
                tvQuestion.setGravity(Gravity.CENTER|Gravity.START);
                tvQuestion.setMinHeight(150);
                tvQuestion.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                TextView tvYourAnswer = new TextView(this.getActivity());
                tvYourAnswer.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvYourAnswer.setBackgroundColor(Color.WHITE);
                tvYourAnswer.setGravity(Gravity.CENTER|Gravity.START);
                tvYourAnswer.setMinHeight(150);
                tvYourAnswer.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                TextView tvRightAnswer = new TextView(this.getActivity());
                tvRightAnswer.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvRightAnswer.setBackgroundColor(Color.WHITE);
                tvRightAnswer.setGravity(Gravity.CENTER|Gravity.START);
                tvRightAnswer.setMinHeight(150);
                tvRightAnswer.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                TextView tvResult = new TextView(this.getActivity());
                tvResult.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvResult.setBackgroundColor(Color.WHITE);
                tvResult.setGravity(Gravity.CENTER|Gravity.START);
                tvResult.setMinHeight(150);
                tvResult.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                //  QUESTION
                String StQuestion = "<b>Question: </b>" + ProblemList.getString(2);
                StringUtility.writeString(tvQuestion, StQuestion);
                lnlyTestLayout.addView(tvQuestion);

                //  YOUR ANSWER
                String StYourAnswer = "<b>Your answer: </b>" + ProblemList.getString(3);
                StringUtility.writeString(tvYourAnswer, StYourAnswer);
                lnlyTestLayout.addView(tvYourAnswer);

                //  RIGHT ANSWER
                String StRightAnswer = "<b>Right answer: </b>" + ProblemList.getString(4);
                StringUtility.writeString(tvRightAnswer, StRightAnswer);
                lnlyTestLayout.addView(tvRightAnswer);

                //  RESULT
                String StResult = "<b>Result: </b>" + ProblemList.getString(5);
                StringUtility.writeString(tvResult, StResult);
                lnlyTestLayout.addView(tvResult);

                //  SEPARATOR
                LinearLayout.LayoutParams SeparatorParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                SeparatorParams.setMargins(0, 20, 0, 20);

                LinearLayout lnlySeparator = new LinearLayout(this.getActivity());
                lnlySeparator.setLayoutParams(SeparatorParams);
                lnlySeparator.setBackgroundColor(Color.parseColor(InfoCollector.getTextColor()));
                lnlySeparator.setMinimumHeight(5);

                //  ADD VIEW
                lnlyProblemList.addView(lnlyTestLayout);
                lnlyProblemList.addView(lnlySeparator);
            }
        }
        else {
            TextView tvHistory = new TextView(this.getActivity());
            tvHistory.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            String StNoData = "<i>" + getResources().getString(R.string.stNoData) + "</i>";
            StringUtility.writeString(tvHistory, StNoData);
            tvHistory.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
            tvHistory.setTextSize(15);
            tvHistory.setGravity(Gravity.CENTER);
            tvHistory.setMinHeight(200);
            lnlyProblemList.addView(tvHistory);
        }
    }

    private void showTestHistory(){
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new HistorySubMenuFragment())
                .commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        //  SHOW PROBLEMS
        showProblemList();

        //  SHOW ADS
        Ads.showAds(this.getActivity());
    }

    public ProblemDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_problem_detail, container, false);

        //  GET REFERENCES
        lnlyProblemList = rootView.findViewById(R.id.lnlyProblemList_ProblemDetailFragment);
        btnNext = rootView.findViewById(R.id.btnNext_ProblemDetailFragment);
        btnNext.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnNext.setOnClickListener(ButtonListener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnNext_ProblemDetailFragment:
                    showTestHistory();
                    break;
            }
        }
    }

}
