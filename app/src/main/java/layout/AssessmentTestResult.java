package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.AssessmentConfirmationFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckMathTest.MathTest;
import pckMath.pckMathTopics.TopicCollection;
import pckString.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class AssessmentTestResult extends Fragment {

    private LinearLayout lnlyAllLevel;
    private TextView tvCurrentLevel;
    private Button btnContinue;
    private int CurrentLevel;
    private String TEST_NAME = "Assessment Test";

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stAssessmentResult));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void showConformation(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new AssessmentConfirmationFragment())
                .commit();
    }

    private void showCurrentTopic(){
        String StCurrentLevel;

        if (CurrentLevel < TopicCollection.getMaxTopicLevel()) {
            StCurrentLevel = "<b>Your current level: </b>" + TopicCollection.getTopic(CurrentLevel).getName();
        }
        else {
            StCurrentLevel = "You are ready for <b>Pre-Cal</b> or <b>Calculus</b>.";
        }

        StringUtility.writeString(tvCurrentLevel, StCurrentLevel);
    }

    private void showAllLevel(){
        int size = TopicCollection.getTopicList().size();
        for (int i = 0; i < size; i++){
            TextView tvTopics = new TextView(this.getActivity());
            tvTopics.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            tvTopics.setGravity(Gravity.START|Gravity.CENTER);
            tvTopics.setMinHeight(200);
            tvTopics.setBackgroundColor(Color.WHITE);
            if (i <= CurrentLevel) {
                String PassedTopic = Objects.requireNonNull(getActivity()).getResources().getString(R.string.stCheckMark) + "\t\t" + TopicCollection.getTopicList().get(i).getName();
                tvTopics.setText(PassedTopic);
                tvTopics.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
            }
            else {
                tvTopics.setText(TopicCollection.getTopicList().get(i).getName());
                tvTopics.setTextColor(Color.GRAY);
            }
            tvTopics.setTextSize(15);

            lnlyAllLevel.addView(tvTopics);
        }
    }

    private void saveTest(){
        MathTest CurrentTest = InfoCollector.getCurrentTest();

        //  ID
        String StID = MathUtility.getID();

        //  NAME
        String StName = TEST_NAME;

        //  DATE
        String StDate = InfoCollector.getDate();

        //  TOPIC
        String StTopics = "All level.";

        //  TIME
        String StTime = "Untimed";

        //  SCORE
        String StScore = TopicCollection.getTopic(CurrentLevel).getName();

        //  SAVE TEST
        DatabaseFile.addToMathTestTable(StID, StName, StDate, StTopics, StTime, StScore);

        //  QUESTIONS
        int size = CurrentTest.getTestQuestion().size();
        for (int i = 0; i < size; i++) {
            String StQuestion = CurrentTest.getTestQuestion().get(i).getQuestion();
            String StRightAnswer = CurrentTest.getTestQuestion().get(i).getRightAnswer();
            String StYourAnswer = CurrentTest.getTestQuestion().get(i).getYourAnswer();
            String StResult;
            if (CurrentTest.getTestQuestion().get(i).getResult()){
                StResult = "Right";
            }
            else {
                StResult = "Wrong";
            }

            //  SAVE QUESTION
            DatabaseFile.addToMathProblemTable(StID, StQuestion, StYourAnswer, StRightAnswer, StResult);
        }
    }

    private void saveUserLevel(){
        DatabaseFile.updateUserLevel(CurrentLevel + 1);
        InfoCollector.getUserInfo();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();

        btnContinue.setTextColor(Color.GRAY);
        CurrentLevel = InfoCollector.getAssessmentTopic();

        //  SHOW RESULT
        showCurrentTopic();
        showAllLevel();

        //  SAVE INFO
        saveTest();
        saveUserLevel();

        //  SHOW ADS
        Ads.showAds(this.getActivity());
    }

    public AssessmentTestResult() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_assessment_test_result, container, false);

        //  GET REFERENCES
        lnlyAllLevel = rootView.findViewById(R.id.lnlyAllLevelContainer_AssessmentTestResultFragment);
        tvCurrentLevel = rootView.findViewById(R.id.tvCurrentLevel_AssessmentTestResultFragment);
        btnContinue = rootView.findViewById(R.id.btnContinue_AssessmentTestResultFragment);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnContinue.setOnClickListener(ButtonListener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnContinue_AssessmentTestResultFragment:
                    showConformation();
                    break;
            }
        }
    }

}
