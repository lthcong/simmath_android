package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckData.pckFirebase.LogEvents;
import pckInfo.InfoCollector;

/**
 * A simple {@link Fragment} subclass.
 */
public class FractionGameInfoFragment extends Fragment {

    private Button btnStart;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stFractionGame));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void logMathGame(){
        LogEvents.writeToLog(this.getActivity(), "MathGame", "FractionGame", "FractionGame");
    }

    private void startNumberGame(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new ComparisonGameFragment())
                .commit();
    }

    private void startup(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
    }

    public FractionGameInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_fraction_game_info, container, false);

        //  GET REFERENCES
        btnStart = rootView.findViewById(R.id.btnStart_FractionGameInfo);
        btnStart.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnStart.setOnClickListener(ButtonListener);

        //  START UP
        startup();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnStart_FractionGameInfo:
                    logMathGame();
                    InfoCollector.setComparisonGameNumber(1);
                    startNumberGame();
                    break;
            }
        }
    }

}
