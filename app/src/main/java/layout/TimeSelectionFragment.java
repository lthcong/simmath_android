package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckInfo.InfoCollector;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimeSelectionFragment extends Fragment {

    private Button btnUntimed, btn15Minutes, btn30Minutes, btn45Minutes, btn60Minutes, btnNext;

    private void setAppTitle(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stTimeSelection));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void clearTestTimeSelection(){
        btnUntimed.setTextColor(Color.GRAY);
        btn15Minutes.setTextColor(Color.GRAY);
        btn30Minutes.setTextColor(Color.GRAY);
        btn45Minutes.setTextColor(Color.GRAY);
        btn60Minutes.setTextColor(Color.GRAY);
        btnNext.setTextColor(Color.GRAY);
    }

    private void showTimeSelection(){
        //  CLEAR SELECTION
        clearTestTimeSelection();

        //  SET SELECTION
        switch (InfoCollector.getTestTime()){
            case 0:
                btnUntimed.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                break;
            case 15:
                btn15Minutes.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                break;
            case 30:
                btn30Minutes.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                break;
            case 45:
                btn45Minutes.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                break;
            case 60:
                btn60Minutes.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                break;
        }
    }

    private void setTimeSelection(Button btnSelectedTime){
        //  CLEAR SELECTION
        clearTestTimeSelection();

        //  SET SELECTION
        btnSelectedTime.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showTopicsSelection(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0).
                replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new TopicsSelectionFragment()).commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setAppTitle();
        showTimeSelection();
    }

    public TimeSelectionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_time_selection, container, false);

        //  GET REFERENCES
        btnUntimed = rootView.findViewById(R.id.btnUnlimited_TimeSelectionFragment);
        btn15Minutes = rootView.findViewById(R.id.btn15Minutes_TimeSelectionFragment);
        btn30Minutes = rootView.findViewById(R.id.btn30Minutes_TimeSelectionFragment);
        btn45Minutes = rootView.findViewById(R.id.btn45Minutes_TimeSelectionFragment);
        btn60Minutes = rootView.findViewById(R.id.btn60Minutes_TimeSelectionFragment);
        btnNext = rootView.findViewById(R.id.btnNextSection_TimeSelectionFragment);

        //  SET LISTENER
        ButtonListener button_listener = new ButtonListener();
        btnUntimed.setOnClickListener(button_listener);
        btn15Minutes.setOnClickListener(button_listener);
        btn30Minutes.setOnClickListener(button_listener);
        btn45Minutes.setOnClickListener(button_listener);
        btn60Minutes.setOnClickListener(button_listener);
        btnNext.setOnClickListener(button_listener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btnUnlimited_TimeSelectionFragment:
                    setTimeSelection((Button) v);
                    InfoCollector.setTestTime(0);
                    showTopicsSelection();
                    break;
                case R.id.btn15Minutes_TimeSelectionFragment:
                    setTimeSelection((Button) v);
                    InfoCollector.setTestTime(15);
                    showTopicsSelection();
                    break;
                case R.id.btn30Minutes_TimeSelectionFragment:
                    setTimeSelection((Button) v);
                    InfoCollector.setTestTime(30);
                    showTopicsSelection();
                    break;
                case R.id.btn45Minutes_TimeSelectionFragment:
                    setTimeSelection((Button) v);
                    InfoCollector.setTestTime(45);
                    showTopicsSelection();
                    break;
                case R.id.btn60Minutes_TimeSelectionFragment:
                    setTimeSelection((Button) v);
                    InfoCollector.setTestTime(60);
                    showTopicsSelection();
                    break;
                case R.id.btnNextSection_TimeSelectionFragment:
                    ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                    showTopicsSelection();
                    break;
            }
        }
    }

}
