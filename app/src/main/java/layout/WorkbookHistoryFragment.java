package layout;


import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.ArrayList;
import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.ButtonClearHistoryFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckData.pckFirebase.LogEvents;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckString.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorkbookHistoryFragment extends Fragment {

    private LinearLayout lnlyWorkbookHistory;
    private Button btnMoreHistory;
    private EditText edtSearchHistory;
    private Cursor WorkbookHistory;
    private int NoShowedHistory = InfoCollector.getNoShowedTopic();
    private ArrayList<String> IDCollection;

    private void setAppTitle(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stWorkbookHistory));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void logWorkbookHistoryView(){
        LogEvents.writeToLog(this.getActivity(), "ViewHistory", "WorkingHistory", "WorkbookHistory");
    }

    private void showClearHistoryButton(){
        getChildFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlybtnClearHistoryContainer_WorkbookHistoryFragment, new ButtonClearHistoryFragment())
                .commit();
    }

    private void showHistory(){
        int ShowedHistory = 0, IDIndex = 0, Count = 0, AdsStep = Ads.WRAPPED_ADS_STEP;

        lnlyWorkbookHistory.removeAllViews();
        IDCollection = new ArrayList<>();

        if (WorkbookHistory.getCount() > 0){
            while(WorkbookHistory.moveToNext()){
                LinearLayout.LayoutParams WorkbookLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                LinearLayout lnlyWorkbookLayout = new LinearLayout(this.getActivity());
                lnlyWorkbookLayout.setLayoutParams(WorkbookLayoutParams);
                lnlyWorkbookLayout.setOrientation(LinearLayout.VERTICAL);

                //  ADS
                Count++;
                if (Count % AdsStep == 0){
                    Ads.showWrappedAds(this.getActivity(), lnlyWorkbookHistory);
                }

                TextView tvDate = new TextView(this.getActivity());
                tvDate.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvDate.setBackgroundColor(Color.WHITE);
                tvDate.setGravity(Gravity.CENTER|Gravity.START);
                tvDate.setMinHeight(150);
                tvDate.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                TextView tvTopic = new TextView(this.getActivity());
                tvTopic.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvTopic.setBackgroundColor(Color.WHITE);
                tvTopic.setGravity(Gravity.CENTER|Gravity.START);
                tvTopic.setMinHeight(150);
                tvTopic.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                TextView tvTime = new TextView(this.getActivity());
                tvTime.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvTime.setBackgroundColor(Color.WHITE);
                tvTime.setGravity(Gravity.CENTER|Gravity.START);
                tvTime.setMinHeight(150);
                tvTime.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                TextView tvScore = new TextView(this.getActivity());
                tvScore.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvScore.setBackgroundColor(Color.WHITE);
                tvScore.setGravity(Gravity.CENTER|Gravity.START);
                tvScore.setMinHeight(150);
                tvScore.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                TextView tvNoQuestion = new TextView(this.getActivity());
                tvNoQuestion.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tvNoQuestion.setBackgroundColor(Color.WHITE);
                tvNoQuestion.setGravity(Gravity.CENTER|Gravity.START);
                tvNoQuestion.setMinHeight(150);
                tvNoQuestion.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                Button btnShowDetail = new Button(this.getActivity());
                btnShowDetail.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                btnShowDetail.setBackgroundColor(Color.WHITE);
                btnShowDetail.setGravity(Gravity.CENTER|Gravity.START);
                btnShowDetail.setMinimumHeight(100);
                btnShowDetail.setMinimumWidth(350);
                btnShowDetail.setTextColor(Color.GRAY);

                //  ID
                IDCollection.add(WorkbookHistory.getString(0));

                //  DATE
                String StDate = "<b>Date: </b>" + WorkbookHistory.getString(1);
                StringUtility.writeString(tvDate, StDate);
                lnlyWorkbookLayout.addView(tvDate);

                //  TOPICS
                String StTopic = "<b>Topic(s): </b>" + WorkbookHistory.getString(2);
                StringUtility.writeString(tvTopic, StTopic);
                lnlyWorkbookLayout.addView(tvTopic);

                //  TIME
                String StTime = "<b>Time: </b>" + WorkbookHistory.getString(3);
                StringUtility.writeString(tvTime, StTime);
                lnlyWorkbookLayout.addView(tvTime);

                //  SCORE
                String StScore = "<b>Score: </b>" + WorkbookHistory.getString(4);
                StringUtility.writeString(tvScore, StScore);
                lnlyWorkbookLayout.addView(tvScore);

                //  NO QUESTION
                String StNoQuestion = "<b>No Question: </b>" + WorkbookHistory.getString(5);
                StringUtility.writeString(tvNoQuestion, StNoQuestion);
                lnlyWorkbookLayout.addView(tvNoQuestion);

                //  SHOW DETAIL
                String StDetail = "Show Detail";
                btnShowDetail.setText(StDetail);
                btnShowDetail.setId(IDIndex);
                btnShowDetail.setOnClickListener(new ShowDetailListener());
                lnlyWorkbookLayout.addView(btnShowDetail);
                IDIndex++;

                //  SEPARATOR
                LinearLayout.LayoutParams SeparatorParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                SeparatorParams.setMargins(0, 20, 0, 20);

                LinearLayout lnlySeparator = new LinearLayout(this.getActivity());
                lnlySeparator.setLayoutParams(SeparatorParams);
                lnlySeparator.setBackgroundColor(Color.parseColor(InfoCollector.getTextColor()));
                lnlySeparator.setMinimumHeight(5);

                //  ADD VIEW
                lnlyWorkbookHistory.addView(lnlyWorkbookLayout);
                lnlyWorkbookHistory.addView(lnlySeparator);

                ShowedHistory++;
                if (ShowedHistory >= NoShowedHistory){
                    break;
                }
            }
        }
        else {
            TextView tvHistory = new TextView(this.getActivity());
            tvHistory.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            String StNoData = "<i>" + getResources().getString(R.string.stNoData) + "</i>";
            StringUtility.writeString(tvHistory, StNoData);
            tvHistory.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
            tvHistory.setTextSize(15);
            tvHistory.setGravity(Gravity.CENTER);
            tvHistory.setMinHeight(200);
            lnlyWorkbookHistory.addView(tvHistory);
        }
    }

    private void showWorkbookDetail(){
        //  REMOVE SUB MENU
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new BlankFragment())
                .commit();

        //  SHOW DETAIL
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new ProblemDetailFragment())
                .commit();
    }

    private void searchHistory(){
        //  SEARCH HISTORY
        String SearchInfo = edtSearchHistory.getText().toString().trim();
        if (SearchInfo.length() == 0){
            WorkbookHistory = DatabaseFile.getMathWorkbook();
        }
        else {
            WorkbookHistory = DatabaseFile.searchWorkbookHistory(SearchInfo);
        }

        //  SHOW HISTORY
        showHistory();
    }

    private void setSelection(final Button ClickedButton){
        //  SET SELECTION
        ClickedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        //  CLEAR SELECTION
        new CountDownTimer(100, 100) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                ClickedButton.setTextColor(Color.GRAY);
            }
        }.start();
    }

    private void startUp(){
        logWorkbookHistoryView();
        setAppTitle();

        searchHistory();
        showClearHistoryButton();
    }

    public WorkbookHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_workbook_history, container, false);

        //  GET LISTENER
        lnlyWorkbookHistory = rootView.findViewById(R.id.lnlyHistoryContainer_WorkbookHistoryFragment);
        edtSearchHistory = rootView.findViewById(R.id.edtSearchHistory_WorkbookHistoryFragment);
        btnMoreHistory = rootView.findViewById(R.id.btnMoreHistory_WorkbookHistoryFragment);
        btnMoreHistory.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener button_listener = new ButtonClickListener();
        btnMoreHistory.setOnClickListener(button_listener);

        SearchListener HistorySearch = new SearchListener();
        edtSearchHistory.addTextChangedListener(HistorySearch);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            setSelection((Button) v);

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnMoreHistory_WorkbookHistoryFragment:
                    NoShowedHistory += 2;
                    searchHistory();
                    break;
            }
        }
    }

    private class ShowDetailListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  LINKED ID
            InfoCollector.setLinkedID(IDCollection.get(v.getId()));

            //  DETAIL
            InfoCollector.setWorkbookHistorySelected();
            showWorkbookDetail();
        }
    }

    private class SearchListener implements TextWatcher{

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            searchHistory();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

}
