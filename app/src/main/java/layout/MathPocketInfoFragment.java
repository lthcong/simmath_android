package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckData.pckFirebase.LogEvents;
import pckInfo.InfoCollector;
import pckMath.MathUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class MathPocketInfoFragment extends Fragment {

    private TextView tvOne, tvTwo, tvThree, tvFour, tvFive;
    private Button btnFindingFactor, btnFindingPrimeFactor, btnFindingGCF, btnFindingLCM, btnCalculationFraction;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathPocket));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void logMathPocket(){
        LogEvents.writeToLog(this.getActivity(), "MathPocket", "MathPocketInfo", "MathPocketInfo");
    }

    private void showFindingFactorSelection(){
        LogEvents.writeToLog(this.getActivity(), "MathPocket", "FindingFactor", "FindingFactor");

        tvOne.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnFindingFactor.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showFindingFactor(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new FindingFactorsFragment())
                .commit();
    }

    private void showFindingPrimeFactorSelection(){
        LogEvents.writeToLog(this.getActivity(), "MathPocket", "FindingPrimeFactor", "FindingPrimeFactor");

        tvTwo.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnFindingPrimeFactor.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showFindingPrimeFactor(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new FindingPrimeFactorFragment())
                .commit();
    }

    private void showFindingGCFSelection(){
        LogEvents.writeToLog(this.getActivity(), "MathPocket", "FindingGCF", "FindingGCF");

        tvThree.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnFindingGCF.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showFindingGCF(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new FindingGCFFragment())
                .commit();
    }

    private void showFindingLCMSelection(){
        LogEvents.writeToLog(this.getActivity(), "MathPocket", "FindingLCM", "FindingLCM");

        tvFour.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnFindingLCM.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showFindingLCM(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new FindingLCMFragment())
                .commit();
    }

    private void showCalculatingFractionSelection(){
        LogEvents.writeToLog(this.getActivity(), "MathPocket", "CalculatingFraction", "CalculatingFraction");

        tvFive.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnCalculationFraction.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showCalculatingFraction(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new CalculatingFractionFragment())
                .commit();
    }

    private void showAds(){
        Ads.showAds(this.getActivity(), MathUtility.getRandomBooleanValue());
    }

    private void startup(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        logMathPocket();
    }

    public MathPocketInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_pocket_info, container, false);

        //  GET REFERENCES
        tvOne = rootView.findViewById(R.id.tvOne_MathPocketInfoFragment);
        tvOne.setTextColor(Color.GRAY);

        tvTwo = rootView.findViewById(R.id.tvTwo_MathPocketInfoFragment);
        tvTwo.setTextColor(Color.GRAY);

        tvThree = rootView.findViewById(R.id.tvThree_MathPocketInfoFragment);
        tvThree.setTextColor(Color.GRAY);

        tvFour = rootView.findViewById(R.id.tvFour_MathPocketInfoFragment);
        tvFour.setTextColor(Color.GRAY);

        tvFive = rootView.findViewById(R.id.tvFive_MathPocketInfoFragment);
        tvFive.setTextColor(Color.GRAY);

        btnFindingFactor = rootView.findViewById(R.id.btnFindingFactor_MathPocketInfoFragment);
        btnFindingFactor.setTextColor(Color.GRAY);

        btnFindingPrimeFactor = rootView.findViewById(R.id.btnFindingPrimeFactor_MathPocketInfoFragment);
        btnFindingPrimeFactor.setTextColor(Color.GRAY);

        btnFindingGCF = rootView.findViewById(R.id.btnFindingGCF_MathPocketInfoFragment);
        btnFindingGCF.setTextColor(Color.GRAY);

        btnFindingLCM = rootView.findViewById(R.id.btnFindingLCM_MathPocketInfoFragment);
        btnFindingLCM.setTextColor(Color.GRAY);

        btnCalculationFraction = rootView.findViewById(R.id.btnCalculationFraction_MathPocketInfoFragment);
        btnCalculationFraction.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        tvOne.setOnClickListener(ButtonListener);
        tvTwo.setOnClickListener(ButtonListener);
        tvThree.setOnClickListener(ButtonListener);
        tvFour.setOnClickListener(ButtonListener);
        tvFive.setOnClickListener(ButtonListener);

        btnFindingFactor.setOnClickListener(ButtonListener);
        btnFindingPrimeFactor.setOnClickListener(ButtonListener);
        btnFindingGCF.setOnClickListener(ButtonListener);
        btnFindingLCM.setOnClickListener(ButtonListener);
        btnCalculationFraction.setOnClickListener(ButtonListener);

        //  START UP
        startup();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvOne_MathPocketInfoFragment:
                    showFindingFactorSelection();
                    showFindingFactor();
                    break;
                case R.id.tvTwo_MathPocketInfoFragment:
                    showFindingPrimeFactorSelection();
                    showFindingPrimeFactor();
                    break;
                case R.id.tvThree_MathPocketInfoFragment:
                    showFindingGCFSelection();
                    showFindingGCF();
                    break;
                case R.id.tvFour_MathPocketInfoFragment:
                    showFindingLCMSelection();
                    showFindingLCM();
                    break;
                case R.id.tvFive_MathPocketInfoFragment:
                    showCalculatingFractionSelection();
                    showCalculatingFraction();
                    break;
                case R.id.btnFindingFactor_MathPocketInfoFragment:
                    showFindingFactorSelection();
                    showFindingFactor();
                    break;
                case R.id.btnFindingPrimeFactor_MathPocketInfoFragment:
                    showFindingPrimeFactorSelection();
                    showFindingPrimeFactor();
                    break;
                case R.id.btnFindingGCF_MathPocketInfoFragment:
                    showFindingGCFSelection();
                    showFindingGCF();
                    break;
                case R.id.btnFindingLCM_MathPocketInfoFragment:
                    showFindingLCMSelection();
                    showFindingLCM();
                    break;
                case R.id.btnCalculationFraction_MathPocketInfoFragment:
                    showCalculatingFractionSelection();
                    showCalculatingFraction();
                    break;
            }

            //  SHOW ADS
            showAds();
        }

    }

}
