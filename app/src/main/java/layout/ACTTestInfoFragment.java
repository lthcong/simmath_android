package layout;


import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.MathTestStartingFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckData.pckFirebase.LogEvents;
import pckInfo.InfoCollector;

/**
 * A simple {@link Fragment} subclass.
 */
public class ACTTestInfoFragment extends Fragment {

    private Button btnStartTest;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stACTTest));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void logACTTest(){
        LogEvents.writeToLog(this.getActivity(), "MathTest", "ACTTest", "ACTTest");
    }

    private void startTest(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathTestStartingFragment())
                .commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        btnStartTest.setTextColor(Color.GRAY);
    }

    public ACTTestInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_acttest_info, container, false);

        //  GET REFERENCES
        btnStartTest = (Button) rootView.findViewById(R.id.btnStart_ACTTestInfoFragment);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnStartTest.setOnClickListener(ButtonListener);

        //  START UP
        startUp();


        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnStart_ACTTestInfoFragment:
                    logACTTest();
                    InfoCollector.setACTTestTime();
                    startTest();
                    break;

            }
        }
    }

}
