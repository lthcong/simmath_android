package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.R;
import pckInfo.InfoCollector;

/**
 * A simple {@link Fragment} subclass.
 */
public class ButtonBackToMathTipsAndTricks extends Fragment {

    private Button btnBack;

    private void backToMathTipsAndTrick(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathTipsAndTricksFragment())
                .commit();
    }

    public ButtonBackToMathTipsAndTricks() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_button_back_to_math_tips_and_tricks, container, false);

        //  GET REFERENCES
        btnBack = rootView.findViewById(R.id.btnBack_BackToMathTipsAndTricksFragment);
        btnBack.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnBack.setOnClickListener(ButtonListener);

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnBack_BackToMathTipsAndTricksFragment:
                    backToMathTipsAndTrick();
                    break;
            }
        }
    }
}
