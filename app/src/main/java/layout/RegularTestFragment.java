package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.*;

import java.util.ArrayList;
import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckData.pckFirebase.LogEvents;
import pckInfo.InfoCollector;
import pckMath.pckMathTest.MathQuestion;
import pckMath.pckMathTest.MathTest;
import pckString.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegularTestFragment extends Fragment {

    private LinearLayout lnlyQuestionContainer;
    private TextView tvRemainingTime;
    private Button btnSubmitTest;
    private CountDownTimer cdtTestTime;

    private MathTest CURRENT_TEST;
    private ArrayList<ArrayList<TextView>> ANSWER_GROUP;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stRegularTest_Name));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void setSelection(Button SelectedButton){
        SelectedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showTestTime(){
        int TestTime = InfoCollector.getTestTime() * 60 * 1000;

        if (TestTime > 0){
            tvRemainingTime.setVisibility(View.VISIBLE);

            cdtTestTime = new CountDownTimer(TestTime, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    String RemainingTime = "Remaining Time: " + StringUtility.getTimeString((int) millisUntilFinished);
                    tvRemainingTime.setText(RemainingTime);

                    if (millisUntilFinished < (3 * 60 * 1000)){
                        tvRemainingTime.setTextColor(Color.RED);
                    }
                }

                @Override
                public void onFinish() {
                    showTestResult();
                }
            }.start();
        }
        else {
            tvRemainingTime.setVisibility(View.GONE);

            cdtTestTime = new CountDownTimer(InfoCollector.getMaxTestTime(), 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    showTestResult();
                }
            }.start();
        }
    }

    private void showQuestion(){
        int size = CURRENT_TEST.getTestQuestion().size();
        int Count = 0, AdsStep = Ads.ADS_STEP;

        lnlyQuestionContainer.removeAllViews();

        for (int i = 0; i < size; i++) {

            LinearLayout.LayoutParams QuestionLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            QuestionLayoutParams.setMargins(0, 0, 0, 20);

            LinearLayout lnlyQuestion = new LinearLayout(this.getActivity());
            lnlyQuestion.setLayoutParams(QuestionLayoutParams);
            lnlyQuestion.setOrientation(LinearLayout.VERTICAL);

            //  ADS
            Count++;
            if (Count % AdsStep == 0){
                Ads.showWrappedAds(this.getActivity(), lnlyQuestionContainer);
            }

            //  QUESTION
            TextView tvQuestion = new TextView(this.getActivity());
            String Question = "<b>Question " + String.valueOf((i + 1)) + ":</b> " + CURRENT_TEST.getTestQuestion().get(i).getQuestion();
            tvQuestion.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            tvQuestion.setMinHeight(200);
            tvQuestion.setGravity(Gravity.CENTER|Gravity.START);
            StringUtility.writeString(tvQuestion, Question);
            tvQuestion.setTextSize(15);
            tvQuestion.setBackgroundColor(Color.WHITE);
            tvQuestion.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            lnlyQuestion.addView(tvQuestion);

            //  ANSWER A - B
            LinearLayout.LayoutParams AnswerAAndBParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            AnswerAAndBParams.setMargins(5, 5, 5, 5);

            LinearLayout lnlyAnswerAAndB = new LinearLayout(this.getActivity());
            lnlyAnswerAAndB.setLayoutParams(AnswerAAndBParams);
            lnlyAnswerAAndB.setOrientation(LinearLayout.VERTICAL);

            LinearLayout.LayoutParams tvAnswerLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            tvAnswerLayoutParams.setMargins(100, 0, 100, 0);

            String AnswerA = getResources().getString(R.string.stCircledA) + ": " + CURRENT_TEST.getTestQuestion().get(i).getAnswerA();
            TextView tvAnswerA = new TextView(this.getActivity());
            tvAnswerA.setLayoutParams(tvAnswerLayoutParams);
            StringUtility.writeString(tvAnswerA, AnswerA);
            tvAnswerA.setBackgroundColor(Color.WHITE);
            tvAnswerA.setTextColor(Color.GRAY);
            tvAnswerA.setTextSize(15);
            tvAnswerA.setMinHeight(200);
            tvAnswerA.setGravity(Gravity.CENTER | Gravity.START);
            tvAnswerA.setId(i);
            tvAnswerA.setOnClickListener(new AnswerSelectionListener());

            String AnswerB = getResources().getString(R.string.stCircledB) + ": " + CURRENT_TEST.getTestQuestion().get(i).getAnswerB();
            TextView tvAnswerB = new TextView(this.getActivity());
            tvAnswerB.setLayoutParams(tvAnswerLayoutParams);
            StringUtility.writeString(tvAnswerB, AnswerB);
            tvAnswerB.setBackgroundColor(Color.WHITE);
            tvAnswerB.setTextColor(Color.GRAY);
            tvAnswerB.setTextSize(15);
            tvAnswerB.setMinHeight(200);
            tvAnswerB.setGravity(Gravity.CENTER | Gravity.START);
            tvAnswerB.setId(i);
            tvAnswerB.setOnClickListener(new AnswerSelectionListener());

            lnlyAnswerAAndB.addView(tvAnswerA);
            lnlyAnswerAAndB.addView(tvAnswerB);

            //  ANSWER C - D
            LinearLayout.LayoutParams AnswerCAndDParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            AnswerCAndDParams.setMargins(5, 5, 5, 5);

            LinearLayout lnlyAnswerCAndD = new LinearLayout(this.getActivity());
            lnlyAnswerCAndD.setLayoutParams(AnswerAAndBParams);
            lnlyAnswerCAndD.setOrientation(LinearLayout.VERTICAL);

            String AnswerC = getResources().getString(R.string.stCircledC) + ": " + CURRENT_TEST.getTestQuestion().get(i).getAnswerC();
            TextView tvAnswerC = new TextView(this.getActivity());
            tvAnswerC.setLayoutParams(tvAnswerLayoutParams);
            StringUtility.writeString(tvAnswerC, AnswerC);
            tvAnswerC.setBackgroundColor(Color.WHITE);
            tvAnswerC.setTextColor(Color.GRAY);
            tvAnswerC.setTextSize(15);
            tvAnswerC.setMinHeight(200);
            tvAnswerC.setGravity(Gravity.CENTER | Gravity.START);
            tvAnswerC.setId(i);
            tvAnswerC.setOnClickListener(new AnswerSelectionListener());

            String AnswerD = getResources().getString(R.string.stCircledD) + ": " + CURRENT_TEST.getTestQuestion().get(i).getAnswerD();
            TextView tvAnswerD = new TextView(this.getActivity());
            tvAnswerD.setLayoutParams(tvAnswerLayoutParams);
            StringUtility.writeString(tvAnswerD, AnswerD);
            tvAnswerD.setBackgroundColor(Color.WHITE);
            tvAnswerD.setTextColor(Color.GRAY);
            tvAnswerD.setTextSize(15);
            tvAnswerD.setMinHeight(200);
            tvAnswerD.setGravity(Gravity.CENTER | Gravity.START);
            tvAnswerD.setId(i);
            tvAnswerD.setOnClickListener(new AnswerSelectionListener());

            lnlyAnswerCAndD.addView(tvAnswerC);
            lnlyAnswerCAndD.addView(tvAnswerD);

            //  GROUPING ANSWER
            LinearLayout lnlyAnswerGroup = new LinearLayout(this.getActivity());
            lnlyAnswerGroup.setOrientation(LinearLayout.HORIZONTAL);
            lnlyAnswerGroup.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            lnlyAnswerGroup.addView(lnlyAnswerAAndB);
            lnlyAnswerGroup.addView(lnlyAnswerCAndD);

            ArrayList<TextView> AnswerGroup = new ArrayList<>();
            AnswerGroup.add(tvAnswerA);
            AnswerGroup.add(tvAnswerB);
            AnswerGroup.add(tvAnswerC);
            AnswerGroup.add(tvAnswerD);
            ANSWER_GROUP.add(AnswerGroup);

            //  SCROLLING
            HorizontalScrollView hscQuestion = new HorizontalScrollView(this.getActivity());
            hscQuestion.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            hscQuestion.addView(lnlyAnswerGroup);

            //  ADD
            lnlyQuestion.addView(hscQuestion);
            lnlyQuestionContainer.addView(lnlyQuestion);
        }
    }

    private void hideSubMenu(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new BlankFragment())
                .commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        hideSubMenu();

        CURRENT_TEST = new MathTest();        
        ANSWER_GROUP = new ArrayList<>();

        showTestTime();
        showQuestion();
    }

    private void showTestResult(){
        try {
            InfoCollector.saveTest(CURRENT_TEST);

            Objects.requireNonNull(getActivity())
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                    .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new TestResultFragment())
                    .commit();
        }
        catch (Exception e){}
    }

    public RegularTestFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_regular_test, container, false);

        //  GET REFERENCES
        lnlyQuestionContainer = rootView.findViewById(R.id.lnlyRegularTestContainer_RegularTestFragment);
        tvRemainingTime = rootView.findViewById(R.id.tvRemainingTime_RegularTestFragment);
        btnSubmitTest = rootView.findViewById(R.id.btnSubmit_RegularTestFragment);
        btnSubmitTest.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener button_listener = new ButtonClickListener();
        btnSubmitTest.setOnClickListener(button_listener);

        //  START UP
        startUp();

        return rootView;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtTestTime != null){
            cdtTestTime.cancel();
        }
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            setSelection((Button) v);

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnSubmit_RegularTestFragment:
                    showTestResult();
                    break;
            }
        }
    }

    private class AnswerSelectionListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  GET INFO
            int Question_Index = v.getId();
            String SelectedAnswer = ((TextView) v).getText().toString().trim();

            //  CLEAR SELECTION
            ANSWER_GROUP.get(Question_Index).get(0).setTextColor(Color.GRAY);
            ANSWER_GROUP.get(Question_Index).get(1).setTextColor(Color.GRAY);
            ANSWER_GROUP.get(Question_Index).get(2).setTextColor(Color.GRAY);
            ANSWER_GROUP.get(Question_Index).get(3).setTextColor(Color.GRAY);

            //  SET SELECTION
            ((TextView) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET YOUR ANSWER
            if (SelectedAnswer.contains(getResources().getString(R.string.stCircledA))) {
                CURRENT_TEST.getTestQuestion().get(Question_Index).setYourAnswer(CURRENT_TEST.getTestQuestion().get(Question_Index).getAnswerA());
            }
            else {
                if (SelectedAnswer.contains(getResources().getString(R.string.stCircledB))) {
                    CURRENT_TEST.getTestQuestion().get(Question_Index).setYourAnswer(CURRENT_TEST.getTestQuestion().get(Question_Index).getAnswerB());
                }
                else {
                    if (SelectedAnswer.contains(getResources().getString(R.string.stCircledC))) {
                        CURRENT_TEST.getTestQuestion().get(Question_Index).setYourAnswer(CURRENT_TEST.getTestQuestion().get(Question_Index).getAnswerC());
                    }
                    else {
                        CURRENT_TEST.getTestQuestion().get(Question_Index).setYourAnswer(CURRENT_TEST.getTestQuestion().get(Question_Index).getAnswerD());
                    }
                }
            }
        }
    }

}
