package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.ArrayList;
import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckData.pckFirebase.LogEvents;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckMathTest.MathQuestion;
import pckMath.pckMathTopics.Topic;
import pckMath.pckMathTopics.TopicCollection;
import pckString.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class MathLadderFragment extends Fragment {

    private TextView    tvTopicName,
                        tvTime, tvScore,
                        tvQuestionNumber, tvQuestionContent,
                        tvResult,
                        tvAnswerA, tvAnswerB, tvAnswerC, tvAnswerD;
    private Button btnCheck, btnNext;

    private int MaxTime = InfoCollector.getMaxTestTime();
    private int WorkingTime;
    private CountDownTimer cdtWorkingTime;

    private int Score;

    private String LadderID;
    private Topic SelectedTopic;
    private int NoQuestion;
    private MathQuestion Question;
    private String YourAnswer = "";

    private boolean isMathStageSelected = InfoCollector.getMathStageSelection();

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathLadder_Name));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void logMathLadder(){
        LogEvents.writeToLog(this.getActivity(), "StartMathLadder", "MathLadder", "MathLadder");
    }

    private void showTopic(){
        String StTopic = "<u>Topic:</u> " + SelectedTopic.getName();
        StringUtility.writeString(tvTopicName, StTopic);
    }

    private void showWorkingTime(){
        cdtWorkingTime = new CountDownTimer(MaxTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                WorkingTime++;
                String StWorkingTime = "Time: " + StringUtility.getTimeString((WorkingTime * 1000));

                StringUtility.writeString(tvTime, StWorkingTime);
            }

            @Override
            public void onFinish() {

            }
        }.start();
    }

    private void showScore(){
        if (Score < 0){
            tvScore.setTextColor(Color.RED);
        }
        else {
            tvScore.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        }

        String StScore = "Score: " + String.valueOf(Score);
        StringUtility.writeString(tvScore, StScore);
    }

    private void showQuestion(){
        //  SHOW SCORE
        showScore();

        //  QUESTION NUMBER
        String StQuestionNumber = "Question " + String.valueOf(NoQuestion) + ":";
        StringUtility.writeString(tvQuestionNumber, StQuestionNumber);

        Question = new MathQuestion(TopicCollection.getProblemFromTopicName(SelectedTopic.getName()));

        //  QUESTION CONTENT
        StringUtility.writeString(tvQuestionContent, Question.getQuestion());

        //  RESULT
        tvResult.setVisibility(View.GONE);

        //  CLEAR ANSWER SELECTION
        clearAnswerSelection();
        enableAnswerSelection();

        //  SHOW MULTIPLE CHOICES
        String AnswerA = getResources().getString(R.string.stCircledA) + ": " + Question.getAnswerA();
        StringUtility.writeString(tvAnswerA, AnswerA);
        tvAnswerA.setTextColor(Color.GRAY);

        String AnswerB = getResources().getString(R.string.stCircledB) + ": " + Question.getAnswerB();
        StringUtility.writeString(tvAnswerB, AnswerB);
        tvAnswerB.setTextColor(Color.GRAY);

        String AnswerC = getResources().getString(R.string.stCircledC) + ": " + Question.getAnswerC();
        StringUtility.writeString(tvAnswerC, AnswerC);
        tvAnswerC.setTextColor(Color.GRAY);

        String AnswerD = getResources().getString(R.string.stCircledD) + ": " + Question.getAnswerD();
        StringUtility.writeString(tvAnswerD, AnswerD);
        tvAnswerD.setTextColor(Color.GRAY);

        //  CLEAR YOUR ANSWER
        YourAnswer = "";

        //  SHOW CHECK BUTTON
        btnCheck.setVisibility(View.VISIBLE);

        //  HIDE NEXT BUTTON
        btnNext.setVisibility(View.GONE);
    }

    private void showNewQuestion(){
        NoQuestion++;
        showQuestion();
    }

    private void disableAnswerSelection(){
        tvAnswerA.setEnabled(false);
        tvAnswerB.setEnabled(false);
        tvAnswerC.setEnabled(false);
        tvAnswerD.setEnabled(false);
    }

    private void enableAnswerSelection(){
        tvAnswerA.setEnabled(true);
        tvAnswerB.setEnabled(true);
        tvAnswerC.setEnabled(true);
        tvAnswerD.setEnabled(true);
    }

    private void clearAnswerSelection(){
        tvAnswerA.setTextColor(Color.GRAY);
        tvAnswerB.setTextColor(Color.GRAY);
        tvAnswerC.setTextColor(Color.GRAY);
        tvAnswerD.setTextColor(Color.GRAY);
    }

    private String getCorrectAnswerMessage(){
        String CorrectAnswer;

        switch (MathUtility.getRandomPositiveNumber_2Digit() % 6){
            case 0:
                CorrectAnswer = "GREAT JOB.";
                break;
            case 1:
                CorrectAnswer = "WONDERFUL.";
                break;
            case 2:
                CorrectAnswer = "FANTASTIC.";
                break;
            case 3:
                CorrectAnswer = "NICE ANSWER.";
                break;
            case 4:
                CorrectAnswer = "GOOD WORK.";
                break;
            default:
                CorrectAnswer = "CORRECT.";
                break;
        }

        return CorrectAnswer;
    }

    private void getYourAnswer(TextView tvSelectedAnswer){
        //  CLEAR SELECTION
        clearAnswerSelection();

        //  SET SELECTION
        tvSelectedAnswer.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        //  GET YOUR ANSWER
        String SelectedAnswer = tvSelectedAnswer.getText().toString().trim();
        if (SelectedAnswer.contains(getResources().getString(R.string.stCircledA))) {
            YourAnswer = Question.getAnswerA();
        }
        else {
            if (SelectedAnswer.contains(getResources().getString(R.string.stCircledB))) {
                YourAnswer = Question.getAnswerB();
            }
            else {
                if (SelectedAnswer.contains(getResources().getString(R.string.stCircledC))) {
                    YourAnswer = Question.getAnswerC();
                }
                else {
                    YourAnswer = Question.getAnswerD();
                }
            }
        }

        //  SAVE ANSWER
        Question.setYourAnswer(YourAnswer);
    }

    private void checkAnswer(){
        //  DISABLE ANSWER SELECTION
        disableAnswerSelection();

        //  CHECK ANSWER SELECTION
        if (YourAnswer.equalsIgnoreCase(Question.getRightAnswer())){
            Score = Score + InfoCollector.getMathLadderIncrease();

            tvResult.setVisibility(View.VISIBLE);
            tvResult.setText(getCorrectAnswerMessage());
            tvResult.setTextColor(Color.GREEN);

            Question.setResult(true);
        }
        else {
            Score = Score - InfoCollector.getMathLadderDecrease();
            String IncorrectAnswer = "NOT CORRECT. THE CORRECT ANSWER IS " + Question.getRightAnswer().toUpperCase();

            tvResult.setVisibility(View.VISIBLE);
            tvResult.setText(IncorrectAnswer);
            tvResult.setTextColor(Color.RED);

            Question.setResult(false);
        }

        //  HIDE CHECK BUTTON
        btnCheck.setVisibility(View.GONE);

        //  SHOW NEXT BUTTON
        btnNext.setVisibility(View.VISIBLE);
    }

    private void saveProblem(){
        String StResult;
        if (Question.getResult()){
            StResult = "Right";
        }
        else {
            StResult = "Wrong";
        }

        DatabaseFile.addToMathProblemTable(LadderID,
                Question.getQuestion(),
                Question.getYourAnswer(),
                Question.getRightAnswer(),
                StResult);
    }

    private void showHistory(){
        InfoCollector.setMathLadderHistorySelected();
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new HistorySubMenuFragment())
                .commit();
    }

    private void showAds(){
        Ads.showAds(this.getActivity());
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();

        LadderID = InfoCollector.getMathLadderID();
        SelectedTopic = InfoCollector.getMathLadderTopic();
        WorkingTime = InfoCollector.getMathLadderTime();
        Score = InfoCollector.getMathLadderScore();
        NoQuestion = InfoCollector.getMathLadderNoQuestion();

        showTopic();
        showWorkingTime();

        showQuestion();

        logMathLadder();
    }

    private void showMathStageNextLevel(){
        InfoCollector.setMathStageLevel(3);

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathStageTransferFragment())
                .commit();
    }

    public MathLadderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_ladder, container, false);

        //  GET REFERENCES
        tvTopicName = rootView.findViewById(R.id.tvTopicName_MathLadderFragment);
        tvTime = rootView.findViewById(R.id.tvWorkTime_MathLadderFragment);
        tvScore = rootView.findViewById(R.id.tvScore_MathLadderFragment);
        tvQuestionNumber = rootView.findViewById(R.id.tvQuestionNumber_MathLadderFragment);
        tvQuestionContent = rootView.findViewById(R.id.tvQuestionContent_MathLadderFragment);
        tvResult = rootView.findViewById(R.id.tvAnswerResult_MathLadderFragment);

        tvAnswerA = rootView.findViewById(R.id.tvAnswerA_MathLadderFragment);
        tvAnswerA.setTextColor(Color.GRAY);

        tvAnswerB = rootView.findViewById(R.id.tvAnswerB_MathLadderFragment);
        tvAnswerB.setTextColor(Color.GRAY);

        tvAnswerC = rootView.findViewById(R.id.tvAnswerC_MathLadderFragment);
        tvAnswerC.setTextColor(Color.GRAY);

        tvAnswerD = rootView.findViewById(R.id.tvAnswerD_MathLadderFragment);
        tvAnswerD.setTextColor(Color.GRAY);

        btnCheck = rootView.findViewById(R.id.btnCheck_MathLadderFragment);
        btnCheck.setTextColor(Color.GRAY);

        btnNext =rootView.findViewById(R.id.btnNext_MathLadderFragment);
        btnNext.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener button_listener = new ButtonClickListener();
        tvAnswerA.setOnClickListener(button_listener);
        tvAnswerB.setOnClickListener(button_listener);
        tvAnswerC.setOnClickListener(button_listener);
        tvAnswerD.setOnClickListener(button_listener);
        btnCheck.setOnClickListener(button_listener);
        btnNext.setOnClickListener(button_listener);

        //  START UP
        startUp();

        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtWorkingTime != null){
            cdtWorkingTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtWorkingTime != null){
            cdtWorkingTime.cancel();
        }
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tvAnswerA_MathLadderFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.tvAnswerB_MathLadderFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.tvAnswerC_MathLadderFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.tvAnswerD_MathLadderFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.btnCheck_MathLadderFragment:
                    checkAnswer();
                    showScore();

                    //  SAVE RECORD
                    saveProblem();
                    break;
                case R.id.btnNext_MathLadderFragment:
                    //  NEXT
                    if (Score < InfoCollector.getMathLadderMinRequirement()) {
                        showNewQuestion();
                    }
                    else {
                        if (isMathStageSelected){
                            //  SHOW MATH STAGE NEXT LEVEL
                            showMathStageNextLevel();
                        }
                        else {
                            //  SHOW HISTORY
                            showHistory();

                            //  ADS
                            showAds();
                        }
                    }
                    break;
            }
        }
    }

}
