package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckData.pckFirebase.LogEvents;
import pckInfo.InfoCollector;

/**
 * A simple {@link Fragment} subclass.
 */
public class MathGameSubMenu extends Fragment {

    private Button btnNumber, btnFraction, btnPuzzle, btnRiddle;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathGame_Name));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void logMathGame(){
        LogEvents.writeToLog(this.getActivity(), "MathGame", "GameInfo", "GameInfo");
    }

    private void hideMenu(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, InfoCollector.getCurrentFragment())
                .commit();
    }

    private void clearSelection(){
        btnNumber.setTextColor(Color.GRAY);
        btnFraction.setTextColor(Color.GRAY);
        btnPuzzle.setTextColor(Color.GRAY);
        btnRiddle.setTextColor(Color.GRAY);
    }

    private void showNumberGameInfo(){
        InfoCollector.setNumberGameSelected();

        clearSelection();
        btnNumber.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new NumberGameInfoFragment())
                .commit();
    }

    private void showFractionGameInfo(){
        InfoCollector.setFractionGameSelected();

        clearSelection();
        btnFraction.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new FractionGameInfoFragment())
                .commit();
    }

    private void showPuzzleGameInfo(){
        InfoCollector.setPuzzleGameSelected();

        clearSelection();
        btnPuzzle.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new PuzzleGameInfoFragment())
                .commit();
    }

    private void showRiddleGameInfo(){
        InfoCollector.setRiddleGameSelected();

        clearSelection();
        btnRiddle.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new RiddleGameInfo())
                .commit();
    }

    private void showGameSelection(){
        if (InfoCollector.getNumberGameSelected()){
            showNumberGameInfo();
        }
        else {
            if (InfoCollector.getFractionGameSelected()){
                showFractionGameInfo();
            }
            else {
                if (InfoCollector.getPuzzleGameSelected()){
                    showPuzzleGameInfo();
                }
                else {
                    if (InfoCollector.getRiddleGameSelected()){
                        showRiddleGameInfo();
                    }
                }
            }
        }
    }

    private void startUp(){
        InfoCollector.setCurrentSubmenu(this);
        setupAppTile();
        logMathGame();
        showGameSelection();
    }

    public MathGameSubMenu() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_game_sub_menu, container, false);

        //  GET REFERENCES
        btnNumber = rootView.findViewById(R.id.btnNumberGame_MathGameSubMenuFragment);
        btnFraction = rootView.findViewById(R.id.btnFractionGame_MathGameSubMenuFragment);
        btnPuzzle = rootView.findViewById(R.id.btnPuzzleGame_MathGameSubMenuFragment);
        btnRiddle = rootView.findViewById(R.id.btnRiddleGame_MathGameSubMenuFragment);

        //  SET LISTENER
        ButtonClickListener button_listener = new ButtonClickListener();
        btnNumber.setOnClickListener(button_listener);
        btnFraction.setOnClickListener(button_listener);
        btnPuzzle.setOnClickListener(button_listener);
        btnRiddle.setOnClickListener(button_listener);

        //  START UP
        startUp();

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            switch (v.getId()){
                case R.id.btnNumberGame_MathGameSubMenuFragment:
                    showNumberGameInfo();
                    break;
                case R.id.btnFractionGame_MathGameSubMenuFragment:
                    showFractionGameInfo();
                    break;
                case R.id.btnPuzzleGame_MathGameSubMenuFragment:
                    showPuzzleGameInfo();
                    break;
                case R.id.btnRiddleGame_MathGameSubMenuFragment:
                    showRiddleGameInfo();
                    break;
            }

            //  HIDE MENU
            hideMenu();
        }
    }

}
