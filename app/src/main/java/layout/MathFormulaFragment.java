package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckData.pckFirebase.LogEvents;
import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckMathFormula.FormulaCollection;
import pckMath.pckMathFormula.MathFormula;
import pckString.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class MathFormulaFragment extends Fragment {

    private EditText edtSearchForFormula;
    private LinearLayout lnlyFormulaContainer;

    private ArrayList<MathFormula> FormulaList;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathFormula));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void logMathFormula(){
        LogEvents.writeToLog(this.getActivity(), "MathFormula", "MathFormulaInfo", "MathFormulaInfo");
    }

    private void showAllFormula(){
        FormulaList = FormulaCollection.getFormulaList();
        showFormula();
    }

    private void searchForFormula(){
        FormulaList = FormulaCollection.searchFormula(edtSearchForFormula.getText().toString().trim());
        showFormula();
    }

    private void showFormula(){
        lnlyFormulaContainer.removeAllViews();
        int Count = 0, AdsStep = Ads.ADS_STEP;

        int size = FormulaList.size();
        for (int i = 0; i < size; i++){
            TextView tvFormulaTile = new TextView(this.getActivity());
            tvFormulaTile.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            tvFormulaTile.setGravity(Gravity.CENTER|Gravity.START);
            tvFormulaTile.setMinHeight(200);
            tvFormulaTile.setTextSize(15);
            tvFormulaTile.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
            StringUtility.writeString(tvFormulaTile, FormulaList.get(i).getTitle());

            TextView tvFormulaContent = new TextView(this.getActivity());
            tvFormulaContent.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            tvFormulaContent.setGravity(Gravity.CENTER);
            tvFormulaContent.setMinHeight(300);
            tvFormulaContent.setTextSize(15);
            tvFormulaContent.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
            StringUtility.writeString(tvFormulaContent, FormulaList.get(i).getContent());

            lnlyFormulaContainer.addView(tvFormulaTile);
            lnlyFormulaContainer.addView(tvFormulaContent);

            //  ADS
            Count++;
            if (Count % AdsStep == 0){
                Ads.showWrappedAds(this.getActivity(), lnlyFormulaContainer);
            }
        }
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        logMathFormula();
        Ads.showAds(this.getActivity(), MathUtility.getRandomBooleanValue());
        showAllFormula();
    }

    public MathFormulaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_formula, container, false);

        //  GET REFERENCES
        edtSearchForFormula = rootView.findViewById(R.id.edtSearchForFormula_MathFormulaFragment);
        lnlyFormulaContainer = rootView.findViewById(R.id.lnlyMathFormulaContainer_MathFormulaFragment);

        //  SET LISTENER
        SearchingListener FormulaSearchingListener = new SearchingListener();
        edtSearchForFormula.addTextChangedListener(FormulaSearchingListener);

        //  START UP
        startUp();

        return rootView;
    }

    private class SearchingListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            searchForFormula();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

}
