package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.*;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.ArrayList;
import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.pckMathTest.MathQuestion;
import pckMath.pckMathTest.MathTest;
import pckMath.pckMathTopics.Topic;
import pckMath.pckMathTopics.TopicCollection;
import pckString.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class AssessmentTestFragment extends Fragment {

    private TextView tvTopic, tvQuestionNumber, tvQuestionContent, tvAnswerResult, tvAnswerA, tvAnswerB, tvAnswerC, tvAnswerD;
    private Button btnCheckAnswer, btnNextQuestion;
    private String TestName = "Accuplacer Test";
    private int QuestionNumber = 0;
    private int NoWrongAnswer = 0;
    private int Topic_Index = 0, MaxTopicLevel;
    private MathProblem Problem;
    private ArrayList<MathQuestion> ListOfQuestion;
    private String YourAnswer;
    private boolean AnswerResult;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathAccuplacer_Name));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void hideSubMenu(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new BlankFragment())
                .commit();
    }

    private void showQuestion(){
        Topic CurrentTopic = TopicCollection.getTopic(Topic_Index);

        if (CurrentTopic.isAssessment()) {

            //  NEW QUESTION
            QuestionNumber++;
            Problem = CurrentTopic.getProblem();

            //  QUESTION TOPIC
            String Topic_Name = "<u>Topic:</u> " + CurrentTopic.getName();
            StringUtility.writeString(tvTopic, Topic_Name);

            //  QUESTION NUMBER
            tvQuestionNumber.setText("Question " + String.valueOf(QuestionNumber) + ": ");

            //  QUESTION
            StringUtility.writeString(tvQuestionContent, Problem.getQuestion());

            //  ANSWER RESULT
            hideAnswerResult();

            //  RESET YOUR ANSWER
            YourAnswer = "-No Answer-";

            //  SHOW MULTIPLE CHOICES
            String AnswerA = getResources().getString(R.string.stCircledA) + ": " + Problem.getAnswerA();
            StringUtility.writeString(tvAnswerA, AnswerA);
            tvAnswerA.setTextColor(Color.GRAY);

            String AnswerB = getResources().getString(R.string.stCircledB) + ": " + Problem.getAnswerB();
            StringUtility.writeString(tvAnswerB, AnswerB);
            tvAnswerB.setTextColor(Color.GRAY);

            String AnswerC = getResources().getString(R.string.stCircledC) + ": " + Problem.getAnswerC();
            StringUtility.writeString(tvAnswerC, AnswerC);
            tvAnswerC.setTextColor(Color.GRAY);

            String AnswerD = getResources().getString(R.string.stCircledD) + ": " + Problem.getAnswerD();
            StringUtility.writeString(tvAnswerD, AnswerD);
            tvAnswerD.setTextColor(Color.GRAY);

            //  ENABLE ANSWER SELECTION
            enableAnswerSelection();

            //  SHOW CHECK ANSWER
            showCheckAnswerButton();

            //  HIDE NEXT QUESTION
            hideNextQuestionButton();
        }
        else {
            Topic_Index ++;
            showQuestion();
        }
    }

    private void enableAnswerSelection(){
        tvAnswerA.setEnabled(true);
        tvAnswerB.setEnabled(true);
        tvAnswerC.setEnabled(true);
        tvAnswerD.setEnabled(true);
    }
    
    private void disableAnswerSelection(){
        tvAnswerA.setEnabled(false);
        tvAnswerB.setEnabled(false);
        tvAnswerC.setEnabled(false);
        tvAnswerD.setEnabled(false);
    }
    
    private void clearAnswerSelection(){
        tvAnswerA.setTextColor(Color.GRAY);
        tvAnswerB.setTextColor(Color.GRAY);
        tvAnswerC.setTextColor(Color.GRAY);
        tvAnswerD.setTextColor(Color.GRAY);
    }

    private void getYourAnswer(TextView tvSelectedAnswer){
        //  CLEAR SELECTION
        clearAnswerSelection();

        //  SET SELECTION
        tvSelectedAnswer.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        //  GET YOUR ANSWER
        String SelectedAnswer = tvSelectedAnswer.getText().toString().trim();
        if (SelectedAnswer.contains(getResources().getString(R.string.stCircledA))) {
            YourAnswer = Problem.getAnswerA();
        }
        else {
            if (SelectedAnswer.contains(getResources().getString(R.string.stCircledB))) {
                YourAnswer = Problem.getAnswerB();
            }
            else {
                if (SelectedAnswer.contains(getResources().getString(R.string.stCircledC))) {
                    YourAnswer = Problem.getAnswerC();
                }
                else {
                    YourAnswer = Problem.getAnswerD();
                }
            }
        }
    }

    private void checkAnswer(){
        //  CHECK ANSWER SELECTION
        if (YourAnswer.equalsIgnoreCase(Problem.getRightAnswer())){
            NoWrongAnswer = 0;
            Topic_Index++;
            AnswerResult = true;

            tvAnswerResult.setVisibility(View.VISIBLE);
            tvAnswerResult.setText(getResources().getString(R.string.stCorrect));
            tvAnswerResult.setTextColor(Color.GREEN);
        }
        else {
            NoWrongAnswer++;
            AnswerResult = false;

            tvAnswerResult.setVisibility(View.VISIBLE);
            tvAnswerResult.setText(getResources().getString(R.string.stNotCorrect));
            tvAnswerResult.setTextColor(Color.RED);
        }

        //  HIDE CHECK ANSWER
        hideCheckAnswerButton();

        //  SHOW NEW QUESTION
        showNextQuestionButton();
    }

    private void saveQuestion(){
        MathQuestion CurrentQuestion = new MathQuestion(Problem);
        CurrentQuestion.setYourAnswer(YourAnswer);
        CurrentQuestion.setResult(AnswerResult);

        ListOfQuestion.add(CurrentQuestion);
    }

    private void saveTest(){
        InfoCollector.setAssessmentTopic(Topic_Index);

        MathTest AssessmentTest = new MathTest(ListOfQuestion);
        AssessmentTest.setTestName(TestName);

        InfoCollector.saveTest(AssessmentTest);
    }

    private void showAssessmentTestResult(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new AssessmentTestResult())
                .commit();
    }

    private void hideAnswerResult(){
        tvAnswerResult.setVisibility(View.GONE);
    }

    private void showNextQuestionButton(){
        btnNextQuestion.setTextColor(Color.GRAY);
        btnNextQuestion.setVisibility(View.VISIBLE);
    }

    private void hideNextQuestionButton(){
        btnNextQuestion.setVisibility(View.GONE);
    }

    private void showCheckAnswerButton(){
        btnCheckAnswer.setTextColor(Color.GRAY);
        btnCheckAnswer.setVisibility(View.VISIBLE);
    }

    private void hideCheckAnswerButton(){
        btnCheckAnswer.setVisibility(View.GONE);
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        hideSubMenu();

        ListOfQuestion = new ArrayList<>();
        MaxTopicLevel = TopicCollection.getMaxTopicLevel();
        showQuestion();
    }

    public AssessmentTestFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_assessment_test, container, false);

        //  GET REFERENCES
        tvTopic = rootView.findViewById(R.id.tvQuestionTopic_AssessmentTestFragment);
        tvQuestionNumber = rootView.findViewById(R.id.tvQuestionNumber_AssessmentTestFragment);
        tvQuestionContent = rootView.findViewById(R.id.tvQuestionContent_AssessmentTestFragment);
        tvAnswerResult = rootView.findViewById(R.id.tvAnswerResult_AssessmentTestFragment);

        tvAnswerA = rootView.findViewById(R.id.tvAnswerA_AssessmentTestFragment);
        tvAnswerA.setTextColor(Color.GRAY);

        tvAnswerB = rootView.findViewById(R.id.tvAnswerB_AssessmentTestFragment);
        tvAnswerB.setTextColor(Color.GRAY);

        tvAnswerC = rootView.findViewById(R.id.tvAnswerC_AssessmentTestFragment);
        tvAnswerC.setTextColor(Color.GRAY);

        tvAnswerD = rootView.findViewById(R.id.tvAnswerD_AssessmentTestFragment);
        tvAnswerD.setTextColor(Color.GRAY);

        btnCheckAnswer = rootView.findViewById(R.id.btnCheckAnswer_AssessmentTestFragment);
        btnCheckAnswer.setTextColor(Color.GRAY);

        btnNextQuestion = rootView.findViewById(R.id.btnNextQuestion_AssessmentTestFragment);
        btnNextQuestion.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener button_listener = new ButtonClickListener();
        tvAnswerA.setOnClickListener(button_listener);
        tvAnswerB.setOnClickListener(button_listener);
        tvAnswerC.setOnClickListener(button_listener);
        tvAnswerD.setOnClickListener(button_listener);
        btnCheckAnswer.setOnClickListener(button_listener);
        btnNextQuestion.setOnClickListener(button_listener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tvAnswerA_AssessmentTestFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.tvAnswerB_AssessmentTestFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.tvAnswerC_AssessmentTestFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.tvAnswerD_AssessmentTestFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.btnCheckAnswer_AssessmentTestFragment:
                    ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                    disableAnswerSelection();
                    checkAnswer();
                    saveQuestion();
                    break;
                case R.id.btnNextQuestion_AssessmentTestFragment:
                    ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                    if (NoWrongAnswer >= 5){
                        Topic_Index = Topic_Index - 1;
                        if (Topic_Index < 0){
                            Topic_Index = 0;
                        }
                        saveTest();
                        showAssessmentTestResult();
                    }
                    else {
                        if (Topic_Index < MaxTopicLevel) {
                            showQuestion();
                        }
                        else {
                            saveTest();
                            showAssessmentTestResult();
                        }
                    }
                    break;
            }
        }
    }

}
