package layout;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckInfo.InfoCollector;
import pckMath.pckHelp.FindingLCM;
import pckString.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class FindingLCMFragment extends Fragment {

    private EditText edtInputOne, edtInputTwo;
    private TextView tvSolution;
    private String EmptySolution = "<i>No Data</i>";

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stFindingLCM));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void addBackButton(){
        getChildFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyButtonBackContainer_FindingLCMFragment, new ButtonBackToMathPocketFragment())
                .commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        edtInputOne.requestFocus();
        StringUtility.writeString(tvSolution, EmptySolution);
        addBackButton();
    }

    public FindingLCMFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_finding_lcm, container, false);

        //  GET REFERENCES
        edtInputOne = rootView.findViewById(R.id.edtInputOne_FindingLCMFragment);
        edtInputTwo = rootView.findViewById(R.id.edtInputTwo_FindingLCMFragment);
        tvSolution =  rootView.findViewById(R.id.tvSolution_FindingLCMFragment);

        //  SET LISTENER
        InputListener NumberInputListener = new InputListener();
        edtInputOne.addTextChangedListener(NumberInputListener);
        edtInputTwo.addTextChangedListener(NumberInputListener);

        //  START UP
        startUp();
        
        return rootView;
    }

    private class InputListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String InputOne = edtInputOne.getText().toString().trim();
            String InputTwo = edtInputTwo.getText().toString().trim();

            if (InputOne.length() > 0 && InputTwo.length() > 0){
                int IntegerOne = Integer.parseInt(InputOne);
                int IntegerTwo = Integer.parseInt(InputTwo);
                StringUtility.writeString(tvSolution, (new FindingLCM(IntegerOne, IntegerTwo)).getSolution());
            }
            else {
                StringUtility.writeString(tvSolution, EmptySolution);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
    
}
