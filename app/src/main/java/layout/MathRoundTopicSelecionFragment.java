package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckInfo.InfoCollector;
import pckMath.pckMathTopics.Round;
import pckMath.pckMathTopics.RoundCollection;
import pckMath.pckMathTopics.TopicCollection;

/**
 * A simple {@link Fragment} subclass.
 */
public class MathRoundTopicSelecionFragment extends Fragment {

    private LinearLayout lnlyTopicsContainer;
    private Button btnAll, btnBasic, btnIntermediate, btnPreAlgebra, btnAlgebra1, btnAlgebra2,
            btnBack;
    private EditText edtSearchYourTopic;

    private ArrayList<Round> RoundList;
    private int SelectedSection;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stTopicSelection));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void clearSectionSelection(){
        btnAll.setTextColor(Color.GRAY);
        btnBasic.setTextColor(Color.GRAY);
        btnIntermediate.setTextColor(Color.GRAY);
        btnPreAlgebra.setTextColor(Color.GRAY);
        btnAlgebra1.setTextColor(Color.GRAY);
        btnAlgebra2.setTextColor(Color.GRAY);
    }

    private void showMathRoundInfo(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathRoundInfoFragment())
                .commit();
    }

    private void showTopicsList(){
        int Count = 0, AdsStep = Ads.WRAPPED_ADS_STEP * 5;
        lnlyTopicsContainer.removeAllViews();

        int CurrentLevel = InfoCollector.getMathRoundLevel();
        int size = RoundList.size();
        for (int i = 0; i < size; i++){
            Round CurrentRound = RoundList.get(i);

            LinearLayout.LayoutParams TopicsListParams =
                    new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            LinearLayout lnlyTopicListLayout = new LinearLayout(this.getActivity());
            lnlyTopicListLayout.setLayoutParams(TopicsListParams);
            lnlyTopicListLayout.setOrientation(LinearLayout.HORIZONTAL);
            lnlyTopicListLayout.setBackgroundColor(Color.WHITE);

            //  ADS
            Count++;
            if (Count % AdsStep == 0){
                Ads.showWrappedAds(this.getActivity(), lnlyTopicsContainer);
            }

            String StLevelString = String.valueOf(CurrentRound.getMinLevel()) + " <=> " + String.valueOf(CurrentRound.getMaxLevel());
            TextView tvLevelNumber = new TextView(this.getActivity());
            tvLevelNumber.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            tvLevelNumber.setBackgroundColor(Color.WHITE);
            tvLevelNumber.setMinHeight(200);
            tvLevelNumber.setMinWidth(300);
            tvLevelNumber.setMaxWidth(300);
            tvLevelNumber.setGravity(Gravity.CENTER|Gravity.START);
            tvLevelNumber.setId(CurrentRound.getMinLevel());
            tvLevelNumber.setOnClickListener(new TopicSelectionListener());
            tvLevelNumber.setText(StLevelString);

            TextView tvLevelContent = new TextView(this.getActivity());
            tvLevelContent.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            tvLevelContent.setBackgroundColor(Color.WHITE);
            tvLevelNumber.setMinHeight(200);
            tvLevelContent.setGravity(Gravity.CENTER|Gravity.START);
            tvLevelContent.setId(CurrentRound.getMinLevel());
            tvLevelContent.setOnClickListener(new TopicSelectionListener());
            tvLevelContent.setText(CurrentRound.getLevelTopic().getName());

            if (CurrentRound.getMinLevel() <= CurrentLevel){
                tvLevelNumber.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                tvLevelContent.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
            }
            else {
                tvLevelNumber.setTextColor(Color.GRAY);
                tvLevelContent.setTextColor(Color.GRAY);
            }

            lnlyTopicListLayout.addView(tvLevelNumber);
            lnlyTopicListLayout.addView(tvLevelContent);

            lnlyTopicsContainer.addView(lnlyTopicListLayout);
        }
    }

    private void searchTopic(){
        //  START SEARCHING
        String SearchName = edtSearchYourTopic.getText().toString().trim();
        if (SearchName.length() > 0) {
            RoundList = RoundCollection.getRoundList(SearchName);
        }
        else {
            RoundList = RoundCollection.getRoundList();
        }

        //  SHOW RESULT
        showTopicsList();
    }

    private void searchBySection(){
        RoundList = RoundCollection.getRoundList(SelectedSection);
        showTopicsList();
    }

    private void showMathRound(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathRoundQuestionFragment())
                .commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();

        RoundList = RoundCollection.getRoundList();
        showTopicsList();
    }

    public MathRoundTopicSelecionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_round_topic_selecion, container, false);

        //  GET REFERENCES
        edtSearchYourTopic = rootView.findViewById(R.id.edtSearchYourTopic_MathRoundTopicSelectionFragment);
        btnAll = rootView.findViewById(R.id.btnAll_MathRoundTopicSelectionFragment);
        btnAll.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        btnBasic = rootView.findViewById(R.id.btnBasic_MathRoundTopicSelectionFragment);
        btnBasic.setTextColor(Color.GRAY);

        btnIntermediate = rootView.findViewById(R.id.btnIntermediate_MathRoundTopicSelectionFragment);
        btnIntermediate.setTextColor(Color.GRAY);

        btnPreAlgebra = rootView.findViewById(R.id.btnPreAlgebra_MathRoundTopicSelectionFragment);
        btnPreAlgebra.setTextColor(Color.GRAY);

        btnAlgebra1 = rootView.findViewById(R.id.btnAlgebra_1_MathRoundTopicSelectionFragment);
        btnAlgebra1.setTextColor(Color.GRAY);

        btnAlgebra2 = rootView.findViewById(R.id.btnAlgebra_2_MathRoundTopicSelectionFragment);
        btnAlgebra2.setTextColor(Color.GRAY);

        lnlyTopicsContainer = rootView.findViewById(R.id.lnlyTopicsContainer_MathRoundTopicSelectionFragment);

        btnBack = rootView.findViewById(R.id.btnBack_MathRoundTopicSelectionFragment);
        btnBack.setTextColor(Color.GRAY);

        //  SET LISTENER
        SectionSelectionListener SectionListener = new SectionSelectionListener();
        btnAll.setOnClickListener(SectionListener);
        btnBasic.setOnClickListener(SectionListener);
        btnIntermediate.setOnClickListener(SectionListener);
        btnPreAlgebra.setOnClickListener(SectionListener);
        btnAlgebra1.setOnClickListener(SectionListener);
        btnAlgebra2.setOnClickListener(SectionListener);

        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnBack.setOnClickListener(ButtonListener);

        SearchingListener search_listener = new SearchingListener();
        edtSearchYourTopic.addTextChangedListener(search_listener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnBack_MathRoundTopicSelectionFragment:
                    showMathRoundInfo();
                    break;
            }
        }
    }

    private class SectionSelectionListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            //  CLEAR SELECTION
            clearSectionSelection();

            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnAll_MathRoundTopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_All;
                    searchBySection();
                    break;
                case R.id.btnBasic_MathRoundTopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_Basic;
                    searchBySection();
                    break;
                case R.id.btnIntermediate_MathRoundTopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_Intermediate;
                    searchBySection();
                    break;
                case R.id.btnPreAlgebra_MathRoundTopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_PreAlgebra;
                    searchBySection();
                    break;
                case R.id.btnAlgebra_1_MathRoundTopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_Algebra_1;
                    searchBySection();
                    break;
                case R.id.btnAlgebra_2_MathRoundTopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_Algebra_2;
                    searchBySection();
                    break;
            }
        }
    }

    private class TopicSelectionListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET LEVEL
            int SelectedLevel = v.getId();
            InfoCollector.setMathRoundSelectedLevel((SelectedLevel / 5) + 1);
            InfoCollector.setMathRoundLevel(SelectedLevel);

            //  GO TO MATH ROUND
            showMathRound();
        }
    }

    private class SearchingListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            searchTopic();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

}
