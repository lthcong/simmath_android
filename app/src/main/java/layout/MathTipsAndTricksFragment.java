package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckData.pckFirebase.LogEvents;
import pckInfo.InfoCollector;

/**
 * A simple {@link Fragment} subclass.
 */
public class MathTipsAndTricksFragment extends Fragment {

    private Button btnAddOne, btnJumpStrategy,
                    btnAddTwo, btnAddUpToTen,
                    btnAddThree, btnTensLast,
                    btnAddFour, btnAimToTens;
    private Button btnMulOne, btnMultiplyByTens,
                    btnMulTwo, btnMultiplyByEleven,
                    btnMulThree, btnMultiplyByPart;
    private Button btnAlgebraOne, btnQuadraticEquation,
                    btnAlgebraTwo, btnQuadraticInequality;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathTips));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void logMathTips(){
        LogEvents.writeToLog(this.getActivity(), "MathTips", "MathTipsInfo", "MathTipsInfo");
    }

    private void showJumpStrategySelection(){
        LogEvents.writeToLog(this.getActivity(), "MathTips", "JumpStrategy", "JumpStrategy");

        InfoCollector.setTipsAndTrickSelection(getResources().getString(R.string.stJumpStrategy));

        btnAddOne.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnJumpStrategy.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showAddUpToTenSelection(){
        LogEvents.writeToLog(this.getActivity(), "MathTips", "AddUpToTen", "AddUpToTen");

        InfoCollector.setTipsAndTrickSelection(getResources().getString(R.string.stAddUpToTen));

        btnAddTwo.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnAddUpToTen.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showTensLastSelection(){
        LogEvents.writeToLog(this.getActivity(), "MathTips", "TensLast", "TensLast");

        InfoCollector.setTipsAndTrickSelection(getResources().getString(R.string.stTenLast));

        btnAddThree.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnTensLast.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showAimToTensSelection(){
        LogEvents.writeToLog(this.getActivity(), "MathTips", "AimToTens", "AimToTens");

        InfoCollector.setTipsAndTrickSelection(getResources().getString(R.string.stAimToTen));

        btnAddFour.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnAimToTens.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMultiplyByTensSelection(){
        LogEvents.writeToLog(this.getActivity(), "MathTips", "MultiplyByTens", "MultiplyByTens");

        InfoCollector.setTipsAndTrickSelection(getResources().getString(R.string.stMultiplyByTen));

        btnMulOne.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnMultiplyByTens.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMultiplyByElevenSelection(){
        LogEvents.writeToLog(this.getActivity(), "MathTips", "MultiplyByEleven", "MultiplyByEleven");

        InfoCollector.setTipsAndTrickSelection(getResources().getString(R.string.stMultiplyByEleven));

        btnMulTwo.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnMultiplyByEleven.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMultiplyByPartSelection(){
        LogEvents.writeToLog(this.getActivity(), "MathTips", "MultiplyByPart", "MultiplyByPart");

        InfoCollector.setTipsAndTrickSelection(getResources().getString(R.string.stMultiplyByPart));

        btnMulThree.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnMultiplyByPart.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }
    
    private void showQuadraticEquationSelection(){
        LogEvents.writeToLog(this.getActivity(), "MathTips", "QuadraticEquation", "QuadraticEquation");

        InfoCollector.setTipsAndTrickSelection(getResources().getString(R.string.stQuadraticEquation));

        btnAlgebraOne.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnQuadraticEquation.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showQuadraticInequalitySelection(){
        LogEvents.writeToLog(this.getActivity(), "MathTips", "QuadraticInequality", "QuadraticInequality");

        InfoCollector.setTipsAndTrickSelection(getResources().getString(R.string.stQuadraticInequality));

        btnAlgebraTwo.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnQuadraticInequality.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }
    
    private void showMathTipsAndTricksFragmentInfo(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathTipsAndTricksInfoFragment())
                .commit();
    }

    private void startup(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        logMathTips();
    }

    public MathTipsAndTricksFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_tips_and_tricks, container, false);

        //  GET REFERENCES
        btnAddOne = rootView.findViewById(R.id.btnAddOne_MathTipsAndTricksFragment);
        btnAddOne.setTextColor(Color.GRAY);

        btnAddTwo = rootView.findViewById(R.id.btnAddTwo_MathTipsAndTricksFragment);
        btnAddTwo.setTextColor(Color.GRAY);

        btnAddThree = rootView.findViewById(R.id.btnAddThree_MathTipsAndTricksFragment);
        btnAddThree.setTextColor(Color.GRAY);

        btnAddFour = rootView.findViewById(R.id.btnAddFour_MathTipsAndTricksFragment);
        btnAddFour.setTextColor(Color.GRAY);

        btnJumpStrategy = rootView.findViewById(R.id.btnJumpStrategy_MathTipsAndTricksFragment);
        btnJumpStrategy.setTextColor(Color.GRAY);

        btnAddUpToTen = rootView.findViewById(R.id.btnAddUpToTen_MathTipsAndTricksFragment);
        btnAddUpToTen.setTextColor(Color.GRAY);

        btnTensLast = rootView.findViewById(R.id.btnTenLast_MathTipsAndTricksFragment);
        btnTensLast.setTextColor(Color.GRAY);

        btnAimToTens = rootView.findViewById(R.id.btnAimToTen_MathTipsAndTricksFragment);
        btnAimToTens.setTextColor(Color.GRAY);

        btnMulOne = rootView.findViewById(R.id.btnMulOne_MathTipsAndTricksFragment);
        btnMulOne.setTextColor(Color.GRAY);

        btnMulTwo = rootView.findViewById(R.id.btnMulTwo_MathTipsAndTricksFragment);
        btnMulTwo.setTextColor(Color.GRAY);

        btnMulThree = rootView.findViewById(R.id.btnMulThree_MathTipsAndTricksFragment);
        btnMulThree.setTextColor(Color.GRAY);

        btnMultiplyByTens = rootView.findViewById(R.id.btnMultiplyByTen_MathTipsAndTricksFragment);
        btnMultiplyByTens.setTextColor(Color.GRAY);

        btnMultiplyByEleven = rootView.findViewById(R.id.btnMultiplyByEleven_MathTipsAndTricksFragment);
        btnMultiplyByEleven.setTextColor(Color.GRAY);

        btnMultiplyByPart = rootView.findViewById(R.id.btnMultiplyByPart_MathTipsAndTricksFragment);
        btnMultiplyByPart.setTextColor(Color.GRAY);

        btnAlgebraOne = rootView.findViewById(R.id.btnAlgebraOne_MathTipsAndTricksFragment);
        btnAlgebraOne.setTextColor(Color.GRAY);

        btnAlgebraTwo = rootView.findViewById(R.id.btnAlgebraTwo_MathTipsAndTricksFragment);
        btnAlgebraTwo.setTextColor(Color.GRAY);

        btnQuadraticEquation = rootView.findViewById(R.id.btnQuadraticEquation_MathTipsAndTricksFragment);
        btnQuadraticEquation.setTextColor(Color.GRAY);

        btnQuadraticInequality = rootView.findViewById(R.id.btnQuadraticInequality_MathTipsAndTricksFragment);
        btnQuadraticInequality.setTextColor(Color.GRAY);
        
        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnAddOne.setOnClickListener(ButtonListener);
        btnAddTwo.setOnClickListener(ButtonListener);
        btnAddThree.setOnClickListener(ButtonListener);
        btnAddFour.setOnClickListener(ButtonListener);
        btnJumpStrategy.setOnClickListener(ButtonListener);
        btnAddUpToTen.setOnClickListener(ButtonListener);
        btnTensLast.setOnClickListener(ButtonListener);
        btnAimToTens.setOnClickListener(ButtonListener);

        btnMulOne.setOnClickListener(ButtonListener);
        btnMulTwo.setOnClickListener(ButtonListener);
        btnMulThree.setOnClickListener(ButtonListener);
        btnMultiplyByTens.setOnClickListener(ButtonListener);
        btnMultiplyByEleven.setOnClickListener(ButtonListener);
        btnMultiplyByPart.setOnClickListener(ButtonListener);
        
        btnAlgebraOne.setOnClickListener(ButtonListener);
        btnAlgebraTwo.setOnClickListener(ButtonListener);
        btnQuadraticEquation.setOnClickListener(ButtonListener);
        btnQuadraticInequality.setOnClickListener(ButtonListener);

        //  START UP
        startup();

        return rootView;
    }
    
    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET FEATURE
            switch (v.getId()) {
                case R.id.btnAddOne_MathTipsAndTricksFragment:
                    showJumpStrategySelection();
                    break;
                case R.id.btnAddTwo_MathTipsAndTricksFragment:
                    showAddUpToTenSelection();
                    break;
                case R.id.btnAddThree_MathTipsAndTricksFragment:
                    showTensLastSelection();
                    break;
                case R.id.btnAddFour_MathTipsAndTricksFragment:
                    showAimToTensSelection();
                    break;
                case R.id.btnJumpStrategy_MathTipsAndTricksFragment:
                    showJumpStrategySelection();
                    break;
                case R.id.btnAddUpToTen_MathTipsAndTricksFragment:
                    showAddUpToTenSelection();
                    break;
                case R.id.btnTenLast_MathTipsAndTricksFragment:
                    showTensLastSelection();
                    break;
                case R.id.btnAimToTen_MathTipsAndTricksFragment:
                    showAimToTensSelection();
                    break;
                case R.id.btnMulOne_MathTipsAndTricksFragment:
                    showMultiplyByTensSelection();
                    break;
                case R.id.btnMulTwo_MathTipsAndTricksFragment:
                    showMultiplyByElevenSelection();
                    break;
                case R.id.btnMulThree_MathTipsAndTricksFragment:
                    showMultiplyByPartSelection();
                    break;
                case R.id.btnMultiplyByTen_MathTipsAndTricksFragment:
                    showMultiplyByTensSelection();
                    break;
                case R.id.btnMultiplyByEleven_MathTipsAndTricksFragment:
                    showMultiplyByElevenSelection();
                    break;
                case R.id.btnMultiplyByPart_MathTipsAndTricksFragment:
                    showMultiplyByPartSelection();
                    break;
                case R.id.btnAlgebraOne_MathTipsAndTricksFragment:
                    showQuadraticEquationSelection();
                    break;
                case R.id.btnAlgebraTwo_MathTipsAndTricksFragment:
                    showQuadraticInequalitySelection();
                    break;
                case R.id.btnQuadraticEquation_MathTipsAndTricksFragment:
                    showQuadraticEquationSelection();
                    break;
                case R.id.btnQuadraticInequality_MathTipsAndTricksFragment:
                    showQuadraticInequalitySelection();
                    break;
            }

            //  SHOW TIPS AND TRICKS INFO FRAGMENT
            showMathTipsAndTricksFragmentInfo();
        }
    }

}
