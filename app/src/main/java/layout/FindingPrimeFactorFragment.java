package layout;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckInfo.InfoCollector;
import pckMath.pckHelp.FindingPrimeFactor;
import pckString.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class FindingPrimeFactorFragment extends Fragment {

    private EditText edtInput;
    private TextView tvSolution;
    private String EmptySolution = "<i>No Data</i>";

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stFindingPrimeFactors));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void addBackButton(){
        getChildFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyButtonBackContainer_FindingPrimeFactorFragment, new ButtonBackToMathPocketFragment())
                .commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        edtInput.requestFocus();
        StringUtility.writeString(tvSolution, EmptySolution);
        addBackButton();
    }

    public FindingPrimeFactorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_finding_prime_factor, container, false);

        //  GET REFERENCES
        edtInput = rootView.findViewById(R.id.edtInput_FindingPrimeFactorFragment);
        tvSolution = rootView.findViewById(R.id.tvSolution_FindingPrimeFactorFragment);

        //  SET LISTENER
        InputListener TextListener = new InputListener();
        edtInput.addTextChangedListener(TextListener);

        //  START UP
        startUp();

        return rootView;
    }

    private class InputListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String InputString = edtInput.getText().toString().trim();
            if (InputString.length() > 0){
                int InputNumber = Integer.parseInt(InputString);
                tvSolution.setText(new FindingPrimeFactor(InputNumber).getSolution());
            }
            else {
                StringUtility.writeString(tvSolution, EmptySolution);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

}
