package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.*;
import adsfree.congla.android.cong.mymathapp.R;
import pckInfo.InfoCollector;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistorySubMenuFragment extends Fragment {

    private Button btnTestHistory,
            btnWorkbookHistory,
            btnMathLadderHistory,
            btnMathStageHistory,
            btnMathPracticeHistory,
            btnMathExerciseHistory;

    private void clearSelection(){
        btnTestHistory.setTextColor(Color.GRAY);
        btnWorkbookHistory.setTextColor(Color.GRAY);
        btnMathLadderHistory.setTextColor(Color.GRAY);
        btnMathStageHistory.setTextColor(Color.GRAY);
        btnMathPracticeHistory.setTextColor(Color.GRAY);
        btnMathExerciseHistory.setTextColor(Color.GRAY);
    }

    private void showFirstPage(){
        if (InfoCollector.getTestHistorySelected()){
            showTestHistory();
        }
        else {
            if (InfoCollector.getWorkbookHistorySelected()){
                showWorkbookHistory();
            }
            else {
                if (InfoCollector.getMathLadderHistorySelected()){
                    showMathLadderHistory();
                }
                else {
                    if (InfoCollector.getMathStageSelection()){
                        showMathStageHistory();
                    }
                    else {
                        if (InfoCollector.getMathPracticeHistorySelected()){
                            showMathPracticeHistory();
                        }
                        else {
                            if (InfoCollector.getMathExerciseHistorySelected()){
                                showMathExerciseHistory();
                            }
                        }
                    }
                }
            }
        }
    }

    private void showTestHistory(){
        clearSelection();
        btnTestHistory.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new TestHistoryFragment())
                .commit();
    }

    private void showWorkbookHistory(){
        clearSelection();
        btnWorkbookHistory.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new WorkbookHistoryFragment())
                .commit();
    }

    private void showMathLadderHistory(){
        clearSelection();
        btnMathLadderHistory.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathLadderHistoryFragment())
                .commit();
    }

    private void showMathStageHistory(){
        clearSelection();
        btnMathStageHistory.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathStageHistoryFragment())
                .commit();
    }

    private void showMathPracticeHistory(){
        clearSelection();
        btnMathPracticeHistory.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathPracticeHistoryFragment())
                .commit();
    }

    private void showMathExerciseHistory(){
        clearSelection();
        btnMathExerciseHistory.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathExerciseHistoryFragment())
                .commit();
    }

    private void startUp(){
        InfoCollector.setCurrentSubmenu(this);
        showFirstPage();
    }

    public HistorySubMenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_history_sub_menu, container, false);

        //  GET REFERENCES
        btnTestHistory = rootView.findViewById(R.id.btnTestHistory_HistorySubMenuFragment);
        btnWorkbookHistory = rootView.findViewById(R.id.btnWorkbookHistory_HistorySubMenuFragment);
        btnMathLadderHistory = rootView.findViewById(R.id.btnMathLadderHistory_HistorySubMenuFragment);
        btnMathStageHistory = rootView.findViewById(R.id.btnMathStageHistory_HistorySubMenuFragment);
        btnMathPracticeHistory = rootView.findViewById(R.id.btnMathPracticeHistory_HistorySubMenuFragment);
        btnMathExerciseHistory = rootView.findViewById(R.id.btnMathExerciseHistory_HistorySubMenuFragment);

        //  SET LISTENER
        ButtonClickListener button_listener = new ButtonClickListener();
        btnTestHistory.setOnClickListener(button_listener);
        btnWorkbookHistory.setOnClickListener(button_listener);
        btnMathLadderHistory.setOnClickListener(button_listener);
        btnMathStageHistory.setOnClickListener(button_listener);
        btnMathPracticeHistory.setOnClickListener(button_listener);
        btnMathExerciseHistory.setOnClickListener(button_listener);

        //  START UP
        startUp();

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            switch (v.getId()){
                case R.id.btnTestHistory_HistorySubMenuFragment:
                    showTestHistory();
                    break;
                case R.id.btnWorkbookHistory_HistorySubMenuFragment:
                    showWorkbookHistory();
                    break;
                case R.id.btnMathLadderHistory_HistorySubMenuFragment:
                    showMathLadderHistory();
                    break;
                case R.id.btnMathStageHistory_HistorySubMenuFragment:
                    showMathStageHistory();
                    break;
                case R.id.btnMathPracticeHistory_HistorySubMenuFragment:
                    showMathPracticeHistory();
                    break;
                case R.id.btnMathExerciseHistory_HistorySubMenuFragment:
                    showMathExerciseHistory();
                    break;
            }
        }
    }

}