package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.*;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckInfo.InfoCollector;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyMathAppIntroFragment extends Fragment {

    private Button btnMathForKidSign,
            btnMathRoundSign,
            btnMathConnectSign,
            btnMathStageSign,
            btnMathWorkbookSign,
            btnMathLadderSign,
            btnMathTestSign,
            btnAssessmentSign,
            btnMathPracticeSign,
            btnMathExerciseSign,
            btnMathPocketSign,
            btnMathGameSign,
            btnMathTipsAndTricksSign,
            btnMathFormulaSign;
    private Button btnMathForKid,
            btnMathRound,
            btnMathConnect,
            btnMathStage,
            btnMathWorkbook,
            btnMathLadder,
            btnMathTest,
            btnAssessment,
            btnMathPractice,
            btnMathExercise,
            btnMathPocket,
            btnMathGame,
            btnMathTipsAndTricks,
            btnMathFormula;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stAppList));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void clearSelection(){
        InfoCollector.setMathStageSelection(false);

        btnMathForKidSign.setTextColor(Color.GRAY);
        btnMathForKid.setTextColor(Color.GRAY);

        btnMathRoundSign.setTextColor(Color.GRAY);
        btnMathRound.setTextColor(Color.GRAY);

        btnMathConnectSign.setTextColor(Color.GRAY);
        btnMathConnect.setTextColor(Color.GRAY);

        btnMathStageSign.setTextColor(Color.GRAY);
        btnMathStage.setTextColor(Color.GRAY);

        btnMathWorkbookSign.setTextColor(Color.GRAY);
        btnMathWorkbook.setTextColor(Color.GRAY);

        btnMathLadderSign.setTextColor(Color.GRAY);
        btnMathLadder.setTextColor(Color.GRAY);

        btnMathTestSign.setTextColor(Color.GRAY);
        btnMathTest.setTextColor(Color.GRAY);

        btnAssessmentSign.setTextColor(Color.GRAY);
        btnAssessment.setTextColor(Color.GRAY);

        btnMathPracticeSign.setTextColor(Color.GRAY);
        btnMathPractice.setTextColor(Color.GRAY);

        btnMathExerciseSign.setTextColor(Color.GRAY);
        btnMathExercise.setTextColor(Color.GRAY);

        btnMathPocketSign.setTextColor(Color.GRAY);
        btnMathPocket.setTextColor(Color.GRAY);

        btnMathGameSign.setTextColor(Color.GRAY);
        btnMathGame.setTextColor(Color.GRAY);

        btnMathTipsAndTricksSign.setTextColor(Color.GRAY);
        btnMathTipsAndTricks.setTextColor(Color.GRAY);

        btnMathFormulaSign.setTextColor(Color.GRAY);
        btnMathFormula.setTextColor(Color.GRAY);
    }

    private void showMathForKidSelection(){
        btnMathForKid.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnMathForKidSign.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMathForKid(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathForKidInfoFragment());
        ft.commit();
    }

    private void showMathRoundSelection(){
        btnMathRound.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnMathRoundSign.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMathRound(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathRoundInfoFragment());
        ft.commit();
    }

    private void showMathConnectSelection(){
        InfoCollector.setupMathStage();

        btnMathConnect.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnMathConnectSign.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMathConnect(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathConnectInfoFragment());
        ft.commit();
    }

    private void showMathStageSelection(){
        InfoCollector.setupMathStage();

        btnMathStage.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnMathStageSign.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMathStage(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathStageInfoFragment());
        ft.commit();
    }

    private void showMathWorkbookSelection(){
        InfoCollector.setupSelectedTopics();

        btnMathWorkbook.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnMathWorkbookSign.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMathWorkbook(){
        InfoCollector.setupSelectedTopics();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new WorkbookInfoFragment());
        ft.commit();
    }

    private void showMathLadderSelection(){
        InfoCollector.setupMathLadder();

        btnMathLadder.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnMathLadderSign.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMathLadder(){
        InfoCollector.setupMathLadder();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathLadderInfoFragment());
        ft.commit();
    }

    private void showMathTestSelection(){
        InfoCollector.setupSelectedTopics();

        btnMathTest.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnMathTestSign.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMathTest(){
        InfoCollector.setRegularTestSelected();
        InfoCollector.setupSelectedTopics();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new LearningSubMenuFragment());
        ft.commit();
    }

    private void showAssessmentSelection(){
        InfoCollector.setupSelectedTopics();

        btnAssessment.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnAssessmentSign.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showAssessment(){
        InfoCollector.setAssessmentTestSelected();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new LearningSubMenuFragment());
        ft.commit();
    }

    private void showMathPracticeSelection(){
        InfoCollector.setupMathPractice();

        btnMathPractice.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnMathPracticeSign.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMathPractice(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathPracticeInfoFragment());
        ft.commit();
    }

    private void showMathExerciseSelection(){
        InfoCollector.setupMathExercise();

        btnMathExercise.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnMathExerciseSign.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMathExercise(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathExerciseInfoFragment());
        ft.commit();
    }

    private void showMathPocketSelection(){
        btnMathPocket.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnMathPocketSign.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMathPocket(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathPocketInfoFragment());
        ft.commit();
    }

    private void showMathGameSelection(){
        btnMathGame.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnMathGameSign.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMathGame(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new MathGameSubMenu());
        ft.commit();
    }

    private void showMathTipsAndTricksSelection(){
        btnMathTipsAndTricks.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnMathTipsAndTricksSign.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMathTipsAndTricks(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathTipsAndTricksFragment());
        ft.commit();
    }

    private void showMathFormulaSelection(){
        btnMathFormula.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnMathFormulaSign.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showMathFormula(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathFormulaFragment());
        ft.commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        clearSelection();
        Ads.showFullScreenAds(this.getActivity());
    }

    public MyMathAppIntroFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_my_math_app_intro, container, false);

        //  GET REFERENCES
        btnMathForKid = rootView.findViewById(R.id.btnMathForKid_MyMathAppIntro);
        btnMathRound = rootView.findViewById(R.id.btnMathRound_MyMathAppIntro);
        btnMathConnect = rootView.findViewById(R.id.btnMathConnect_MyMathAppIntro);
        btnMathStage = rootView.findViewById(R.id.btnMathStage_MyMathAppIntro);
        btnMathWorkbook = rootView.findViewById(R.id.btnMathWorkbook_MyMathAppIntro);
        btnMathLadder = rootView.findViewById(R.id.btnMathLadder_MyMathAppIntro);
        btnMathTest = rootView.findViewById(R.id.btnMathTest_MyMathAppIntro);
        btnAssessment = rootView.findViewById(R.id.btnMathAssessment_MyMathAppIntro);
        btnMathPractice = rootView.findViewById(R.id.btnMathPractice_MyMathAppIntro);
        btnMathExercise = rootView.findViewById(R.id.btnMathExercise_MyMathAppIntro);
        btnMathPocket = rootView.findViewById(R.id.btnMathPocket_MyMathAppIntro);
        btnMathGame = rootView.findViewById(R.id.btnMathGame_MyMathAppIntro);
        btnMathTipsAndTricks = rootView.findViewById(R.id.btnMathTipsAndTricks_MyMathAppIntro);
        btnMathFormula = rootView.findViewById(R.id.btnMathFormula_MyMathAppIntro);

        btnMathForKidSign = rootView.findViewById(R.id.btnMathForKidSign_MyMathAppIntro);
        btnMathRoundSign = rootView.findViewById(R.id.btnMathRoundSign_MyMathAppIntro);
        btnMathConnectSign = rootView.findViewById(R.id.btnMathConnectSign_MyMathAppIntro);
        btnMathStageSign = rootView.findViewById(R.id.btnMathStageSign_MyMathAppIntro);
        btnMathWorkbookSign = rootView.findViewById(R.id.btnMathWorkbookSign_MyMathAppIntro);
        btnMathLadderSign = rootView.findViewById(R.id.btnMathLadderSign_MyMathAppIntro);
        btnMathTestSign = rootView.findViewById(R.id.btnMathTestSign_MyMathAppIntro);
        btnAssessmentSign = rootView.findViewById(R.id.btnAssessmentSign_MyMathAppIntro);
        btnMathPracticeSign = rootView.findViewById(R.id.btnMathPracticeSign_MyMathAppIntro);
        btnMathExerciseSign = rootView.findViewById(R.id.btnMathExerciseSign_MyMathAppIntro);
        btnMathPocketSign = rootView.findViewById(R.id.btnMathPocketSign_MyMathAppIntro);
        btnMathGameSign = rootView.findViewById(R.id.btnMathGameSign_MyMathAppIntro);
        btnMathTipsAndTricksSign = rootView.findViewById(R.id.btnMathTipsAndTricksSign_MyMathAppIntro);
        btnMathFormulaSign = rootView.findViewById(R.id.btnMathFormulaSign_MyMathAppIntro);

        //  SET LISTENER
        AppsSelectionListener SelectionListener = new AppsSelectionListener();
        btnMathRound.setOnClickListener(SelectionListener);
        btnMathForKid.setOnClickListener(SelectionListener);
        btnMathConnect.setOnClickListener(SelectionListener);
        btnMathStage.setOnClickListener(SelectionListener);
        btnMathWorkbook.setOnClickListener(SelectionListener);
        btnMathLadder.setOnClickListener(SelectionListener);
        btnMathTest.setOnClickListener(SelectionListener);
        btnAssessment.setOnClickListener(SelectionListener);
        btnMathPractice.setOnClickListener(SelectionListener);
        btnMathExercise.setOnClickListener(SelectionListener);
        btnMathPocket.setOnClickListener(SelectionListener);
        btnMathGame.setOnClickListener(SelectionListener);
        btnMathTipsAndTricks.setOnClickListener(SelectionListener);
        btnMathFormula.setOnClickListener(SelectionListener);

        btnMathRoundSign.setOnClickListener(SelectionListener);
        btnMathForKidSign.setOnClickListener(SelectionListener);
        btnMathConnectSign.setOnClickListener(SelectionListener);
        btnMathStageSign.setOnClickListener(SelectionListener);
        btnMathWorkbookSign.setOnClickListener(SelectionListener);
        btnMathLadderSign.setOnClickListener(SelectionListener);
        btnMathTestSign.setOnClickListener(SelectionListener);
        btnAssessmentSign.setOnClickListener(SelectionListener);
        btnMathPracticeSign.setOnClickListener(SelectionListener);
        btnMathExerciseSign.setOnClickListener(SelectionListener);
        btnMathPocketSign.setOnClickListener(SelectionListener);
        btnMathGameSign.setOnClickListener(SelectionListener);
        btnMathTipsAndTricksSign.setOnClickListener(SelectionListener);
        btnMathFormulaSign.setOnClickListener(SelectionListener);

        //  START UP
        startUp();

        return rootView;
    }

    private class AppsSelectionListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  CLEAR SELECTION
            clearSelection();

            //  SET SELECTION
            switch (v.getId()){
                case R.id.btnMathForKid_MyMathAppIntro:
                    showMathForKidSelection();
                    showMathForKid();
                    break;
                case R.id.btnMathRound_MyMathAppIntro:
                    showMathRoundSelection();
                    showMathRound();
                    break;
                case R.id.btnMathConnect_MyMathAppIntro:
                    showMathConnectSelection();
                    showMathConnect();
                    break;
                case R.id.btnMathStage_MyMathAppIntro:
                    showMathStageSelection();
                    showMathStage();
                    break;
                case R.id.btnMathWorkbook_MyMathAppIntro:
                    showMathWorkbookSelection();
                    showMathWorkbook();
                    break;
                case R.id.btnMathLadder_MyMathAppIntro:
                    showMathLadderSelection();
                    showMathLadder();
                    break;
                case R.id.btnMathTest_MyMathAppIntro:
                    showMathTestSelection();
                    showMathTest();
                    break;
                case R.id.btnMathAssessment_MyMathAppIntro:
                    showAssessmentSelection();
                    showAssessment();
                    break;
                case R.id.btnMathPractice_MyMathAppIntro:
                    showMathPracticeSelection();
                    showMathPractice();
                    break;
                case R.id.btnMathExercise_MyMathAppIntro:
                    showMathExerciseSelection();
                    showMathExercise();
                    break;
                case R.id.btnMathPocket_MyMathAppIntro:
                    showMathPocketSelection();
                    showMathPocket();
                    break;
                case R.id.btnMathGame_MyMathAppIntro:
                    showMathGameSelection();
                    showMathGame();
                    break;
                case R.id.btnMathTipsAndTricks_MyMathAppIntro:
                    showMathTipsAndTricksSelection();
                    showMathTipsAndTricks();
                    break;
                case R.id.btnMathFormula_MyMathAppIntro:
                    showMathFormulaSelection();
                    showMathFormula();
                    break;
                case R.id.btnMathForKidSign_MyMathAppIntro:
                    showMathForKidSelection();
                    showMathForKid();
                    break;
                case R.id.btnMathRoundSign_MyMathAppIntro:
                    showMathRoundSelection();
                    showMathRound();
                    break;
                case R.id.btnMathConnectSign_MyMathAppIntro:
                    showMathConnectSelection();
                    showMathConnect();
                    break;
                case R.id.btnMathStageSign_MyMathAppIntro:
                    showMathStageSelection();
                    showMathStage();
                    break;
                case R.id.btnMathWorkbookSign_MyMathAppIntro:
                    showMathWorkbookSelection();
                    showMathWorkbook();
                    break;
                case R.id.btnMathLadderSign_MyMathAppIntro:
                    showMathLadderSelection();
                    showMathLadder();
                    break;
                case R.id.btnMathTestSign_MyMathAppIntro:
                    showMathTestSelection();
                    showMathTest();
                    break;
                case R.id.btnAssessmentSign_MyMathAppIntro:
                    showAssessmentSelection();
                    showAssessment();
                    break;
                case R.id.btnMathPracticeSign_MyMathAppIntro:
                    showMathPracticeSelection();
                    showMathPractice();
                    break;
                case R.id.btnMathExerciseSign_MyMathAppIntro:
                    showMathExerciseSelection();
                    showMathExercise();
                    break;
                case R.id.btnMathPocketSign_MyMathAppIntro:
                    showMathPocketSelection();
                    showMathPocket();
                    break;
                case R.id.btnMathGameSign_MyMathAppIntro:
                    showMathGameSelection();
                    showMathGame();
                    break;
                case R.id.btnMathTipsAndTricksSign_MyMathAppIntro:
                    showMathTipsAndTricksSelection();
                    showMathTipsAndTricks();
                    break;
                case R.id.btnMathFormulaSign_MyMathAppIntro:
                    showMathFormulaSelection();
                    showMathFormula();
                    break;
            }
        }
    }

}
