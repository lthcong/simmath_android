package layout;

import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckInfo.InfoCollector;
import pckMath.MathProblem;
import pckMath.pckTipsAndTricks.pckAdd.*;
import pckMath.pckTipsAndTricks.pckAlgebra.*;
import pckMath.pckTipsAndTricks.pckMul.*;
import pckString.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class MathTipsAndTricksInfoFragment extends Fragment {

    private String JUMP_STRATEGY, JUMP_STRATEGY_INFO,
                    ADD_UP_TO_TEN, ADD_UP_TO_TEN_INFO,
                    TENS_LAST, TENS_LAST_INFO,
                    AIM_TO_TENS, AIM_TO_TENS_INFO,
                    MULTIPLY_BY_TEN, MULTIPLY_BY_TEN_INFO,
                    MULTIPLY_BY_ELEVEN, MULTIPLY_BY_ELEVEN_INFO,
                    MULTIPLY_BY_PART, MULTIPLY_BY_PART_INFO,
                    QUADRATIC_EQUATION, QUADRATIC_EQUATION_INFO,
                    QUADRATIC_INEQUALITY, QUADRATIC_INEQUALITY_INFO;;
    private TextView tvTipsAndTrickName, tvHowThisWork,
                    tvQuestion, tvResult;
    private TextView tvAnswerA, tvAnswerB, tvAnswerC, tvAnswerD;
    private CountDownTimer cdtCountDownTime;
    private MathProblem Problem;
    private int NoProblem = 1;
    private String SelectedTipsAndTrick, YourAnswer;

    private void setupAppTile(){
        InfoCollector.setAppTitle(SelectedTipsAndTrick);

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void showAds(){
        Ads.showAds(this.getActivity(), NoProblem);
    }

    private void addBackButton(){
        getChildFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyButtonBackContainer_MathTipsAndTricksInfoFragment, new ButtonBackToMathTipsAndTricks())
                .commit();
    }

    private void setup(){
        JUMP_STRATEGY = getResources().getString(R.string.stJumpStrategy);
        JUMP_STRATEGY_INFO = getResources().getString(R.string.stJumpStrategy_Info);

        ADD_UP_TO_TEN = getResources().getString(R.string.stAddUpToTen);
        ADD_UP_TO_TEN_INFO = getResources().getString(R.string.stAddUpToTen_Info);

        TENS_LAST = getResources().getString(R.string.stTenLast);
        TENS_LAST_INFO = getResources().getString(R.string.stTenLast_Info);

        AIM_TO_TENS = getResources().getString(R.string.stAimToTen);
        AIM_TO_TENS_INFO = getResources().getString(R.string.stAimToTen_Info);

        MULTIPLY_BY_TEN = getResources().getString(R.string.stMultiplyByTen);
        MULTIPLY_BY_TEN_INFO = getResources().getString(R.string.stMultiplyByTen_Info);

        MULTIPLY_BY_ELEVEN = getResources().getString(R.string.stMultiplyByEleven);
        MULTIPLY_BY_ELEVEN_INFO = getResources().getString(R.string.stMultiplyByEleven_Info);

        MULTIPLY_BY_PART = getResources().getString(R.string.stMultiplyByPart);
        MULTIPLY_BY_PART_INFO = getResources().getString(R.string.stMultiplyByPart_Info);

        QUADRATIC_EQUATION = getResources().getString(R.string.stQuadraticEquation);
        QUADRATIC_EQUATION_INFO = getResources().getString(R.string.stQuadraticEquation_Info);

        QUADRATIC_INEQUALITY = getResources().getString(R.string.stQuadraticInequality);
        QUADRATIC_INEQUALITY_INFO = getResources().getString(R.string.stQuadraticInequality_Info);
    }

    private void showTipsAndTricks(){
        //  NAME
        StringUtility.writeString(tvTipsAndTrickName, SelectedTipsAndTrick);

        //  INFO
        String stHowThisWork = "";
        if (SelectedTipsAndTrick.equalsIgnoreCase(JUMP_STRATEGY)){
            stHowThisWork = JUMP_STRATEGY_INFO;
            Problem = new JumpStrategy();
        }
        else {
            if (SelectedTipsAndTrick.equalsIgnoreCase(ADD_UP_TO_TEN)){
                stHowThisWork = ADD_UP_TO_TEN_INFO;
                Problem = new AddUpToTen();
            }
            else {
                if (SelectedTipsAndTrick.equalsIgnoreCase(TENS_LAST)){
                    stHowThisWork = TENS_LAST_INFO;
                    Problem = new TensLast();
                }
                else {
                    if (SelectedTipsAndTrick.equalsIgnoreCase(AIM_TO_TENS)){
                        stHowThisWork = AIM_TO_TENS_INFO;
                        Problem = new AimToTens();
                    }
                    else {
                        if (SelectedTipsAndTrick.equalsIgnoreCase(MULTIPLY_BY_TEN)){
                            stHowThisWork = MULTIPLY_BY_TEN_INFO;
                            Problem = new MultiplyByTen();
                        }
                        else {
                            if (SelectedTipsAndTrick.equalsIgnoreCase(MULTIPLY_BY_ELEVEN)){
                                stHowThisWork = MULTIPLY_BY_ELEVEN_INFO;
                                Problem = new MultiplyByEleven();
                            }
                            else {
                                if (SelectedTipsAndTrick.equalsIgnoreCase(MULTIPLY_BY_PART)){
                                    stHowThisWork = MULTIPLY_BY_PART_INFO;
                                    Problem = new MultiplyByPart();
                                }
                                else {
                                    if (SelectedTipsAndTrick.equalsIgnoreCase(QUADRATIC_EQUATION)){
                                        stHowThisWork = QUADRATIC_EQUATION_INFO;
                                        Problem = new QuadraticEquation();
                                    }
                                    else {
                                        if (SelectedTipsAndTrick.equalsIgnoreCase(QUADRATIC_INEQUALITY)){
                                            stHowThisWork = QUADRATIC_INEQUALITY_INFO;
                                            Problem = new QuadraticInequality();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        tvHowThisWork.setText(stHowThisWork);
    }
    
    private void showProblem(){
        Problem = Problem.createProblem();
        StringUtility.writeString(tvQuestion, Problem.getQuestion());
        
        tvResult.setVisibility(View.GONE);
        
        String AnswerA = getResources().getString(R.string.stCircledA) + ": " + Problem.getAnswerA();
        StringUtility.writeString(tvAnswerA, AnswerA);
        tvAnswerA.setTextColor(Color.GRAY);

        String AnswerB = getResources().getString(R.string.stCircledB) + ": " + Problem.getAnswerB();
        StringUtility.writeString(tvAnswerB, AnswerB);
        tvAnswerB.setTextColor(Color.GRAY);

        String AnswerC = getResources().getString(R.string.stCircledC) + ": " + Problem.getAnswerC();
        StringUtility.writeString(tvAnswerC, AnswerC);
        tvAnswerC.setTextColor(Color.GRAY);

        String AnswerD = getResources().getString(R.string.stCircledD) + ": " + Problem.getAnswerD();
        StringUtility.writeString(tvAnswerD, AnswerD);
        tvAnswerD.setTextColor(Color.GRAY);
    }

    private void showNewProblem(){
        cdtCountDownTime = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                NoProblem++;
                showProblem();
            }
        }.start();

    }

    private void getYourAnswer(TextView tvSelectedAnswer){
        tvSelectedAnswer.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        //  GET YOUR ANSWER
        String SelectedAnswer = tvSelectedAnswer.getText().toString().trim();
        if (SelectedAnswer.contains(getResources().getString(R.string.stCircledA))) {
            YourAnswer = Problem.getAnswerA();
        }
        else {
            if (SelectedAnswer.contains(getResources().getString(R.string.stCircledB))) {
                YourAnswer = Problem.getAnswerB();
            }
            else {
                if (SelectedAnswer.contains(getResources().getString(R.string.stCircledC))) {
                    YourAnswer = Problem.getAnswerC();
                }
                else {
                    YourAnswer = Problem.getAnswerD();
                }
            }
        }
    }

    private void checkYourAnswer(){
        //  SHOW RESULT
        tvResult.setVisibility(View.VISIBLE);
        if (YourAnswer.equalsIgnoreCase(Problem.getRightAnswer())){
            tvResult.setText("CORRECT");
            tvResult.setTextColor(Color.GREEN);
        }
        else {
            tvResult.setText("NOT CORRECT");
            tvResult.setTextColor(Color.RED);
        }
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        SelectedTipsAndTrick = InfoCollector.getTipsAndTrickSelection();

        setupAppTile();
        addBackButton();
        setup();
        showTipsAndTricks();
        showProblem();
    }

    public MathTipsAndTricksInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_tips_and_tricks_info, container, false);

        //  GET REFERENCES
        tvTipsAndTrickName = rootView.findViewById(R.id.tvTipsAndTrickName_MathTipsAndTricksInfoFragment);
        tvHowThisWork = rootView.findViewById(R.id.tvHowThisWork_MathTipsAndTricksInfoFragment);
        tvQuestion = rootView.findViewById(R.id.tvQuestion_MathTipsAndTricksInfoFragment);
        tvResult = rootView.findViewById(R.id.tvResult_MathTipsAndTricksInfoFragment);
        tvAnswerA = rootView.findViewById(R.id.tvAnswerA_MathTipsAndTricksInfoFragment);
        tvAnswerA.setTextColor(Color.GRAY);

        tvAnswerB = rootView.findViewById(R.id.tvAnswerB_MathTipsAndTricksInfoFragment);
        tvAnswerB.setTextColor(Color.GRAY);

        tvAnswerC = rootView.findViewById(R.id.tvAnswerC_MathTipsAndTricksInfoFragment);
        tvAnswerC.setTextColor(Color.GRAY);

        tvAnswerD = rootView.findViewById(R.id.tvAnswerD_MathTipsAndTricksInfoFragment);
        tvAnswerD.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        tvAnswerA.setOnClickListener(ButtonListener);
        tvAnswerB.setOnClickListener(ButtonListener);
        tvAnswerC.setOnClickListener(ButtonListener);
        tvAnswerD.setOnClickListener(ButtonListener);

        //  START UP
        startUp();

        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET YOUR ANSWER
            switch (v.getId()){
                case R.id.tvAnswerA_MathTipsAndTricksInfoFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.tvAnswerB_MathTipsAndTricksInfoFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.tvAnswerC_MathTipsAndTricksInfoFragment:
                    getYourAnswer((TextView) v);
                    break;
                case R.id.tvAnswerD_MathTipsAndTricksInfoFragment:
                    getYourAnswer((TextView) v);
                    break;
            }

            //  SHOW RESULT
            checkYourAnswer();

            //  SHOW NEW PROBLEM
            showNewProblem();

            //  ADS
            showAds();
        }
    }

}
