package layout;


import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckInfo.InfoCollector;
import pckMath.MathUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class GameEndFragment extends Fragment {

    private TextView tvLevelCompleteStatus;
    private int NextGameTime = 2;

    private void showNextComparisonGame(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new ComparisonGameFragment())
                .commit();

        Ads.showAds(this.getActivity(), InfoCollector.getComparisonGameLevel());
    }

    private void showNextPuzzleGame(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new PuzzleGameFragment())
                .commit();
        Ads.showAds(this.getActivity(), InfoCollector.getPuzzleGameCurrentLevel());
    }

    private void showRandomRiddleGame(){
        int NoLevel = 65;
        int Level = MathUtility.getRandomPositiveNumber_4Digit() % NoLevel + 1;
        switch (MathUtility.getRandomPositiveNumber_4Digit() % Level) {
            case 1:
                loadMultipleChoiceQuestion();
                break;
            case 2:
                loadMultipleChoiceQuestion();
                break;
            case 3:
                loadMultipleChoiceQuestion();
                break;
            case 4:
                loadMultipleChoiceQuestion();
                break;
            case 5:
                loadMultipleChoiceQuestion();
                break;
            case 6:
                loadMultipleChoiceQuestion();
                break;
            case 7:
                loadMultipleChoiceQuestion();
                break;
            case 8:
                loadMultipleChoiceQuestion();
                break;
            case 9:
                loadMultipleChoiceQuestion();
                break;
            case 10:
                loadMultipleChoiceQuestion();
                break;
            case 11:
                loadMultipleChoiceQuestion();
                break;
            case 12:
                loadMultipleChoiceQuestion();
                break;
            case 13:
                loadMultipleChoiceQuestion();
                break;
            case 14:
                loadMultipleChoiceQuestion();
                break;
            case 15:
                loadMultipleChoiceQuestion();
                break;
            case 16:
                loadFigureQuestion_01();
                break;
            case 17:
                loadFigureQuestion_02();
                break;
            case 18:
                loadFigureQuestion_03();
                break;
            case 19:
                loadMultipleChoiceQuestion();
                break;
            case 20:
                loadFigureQuestion_01();
                break;
            case 21:
                loadFigureQuestion_01();
                break;
            case 22:
                loadFigureQuestion_01();
                break;
            case 23:
                loadFigureQuestion_02();
                break;
            case 24:
                loadFigureQuestion_04();
                break;
            case 25:
                loadFigureQuestion_01();
                break;
            case 26:
                loadMultipleChoiceQuestion();
                break;
            case 27:
                loadFigureQuestion_04();
                break;
            case 28:
                loadFigureQuestion_03();
                break;
            case 29:
                loadFigureQuestion_03();
                break;
            case 30:
                loadFigureQuestion_03();
                break;
            case 31:
                loadFigureQuestion_01();
                break;
            case 32:
                loadFigureQuestion_02();
                break;
            case 33:
                loadFigureQuestion_02();
                break;
            case 34:
                loadFigureQuestion_01();
                break;
            case 35:
                loadFigureQuestion_02();
                break;
            case 36:
                loadMultipleChoiceQuestion();
                break;
            case 37:
                loadMultipleChoiceQuestion();
                break;
            case 38:
                loadFigureQuestion_03();
                break;
            case 39:
                loadFigureQuestion_01();
                break;
            case 40:
                loadFigureQuestion_02();
                break;
            case 41:
                loadFigureQuestion_02();
                break;
            case 42:
                loadFigureQuestion_03();
                break;
            case 43:
                loadFigureQuestion_04();
                break;
            case 44:
                loadFigureQuestion_04();
                break;
            case 45:
                loadFigureQuestion_04();
                break;
            case 46:
                loadMultipleChoiceQuestion();
                break;
            case 47:
                loadMultipleChoiceQuestion();
                break;
            case 48:
                loadFigureQuestion_01();
                break;
            case 49:
                loadFigureQuestion_01();
                break;
            case 50:
                loadFigureQuestion_03();
                break;
            case 51:
                loadFigureQuestion_04();
                break;
            case 52:
                loadFigureQuestion_03();
                break;
            case 53:
                loadFigureQuestion_02();
                break;
            case 54:
                loadFigureQuestion_02();
                break;
            case 55:
                loadFigureQuestion_01();
                break;
            case 56:
                loadFigureQuestion_01();
                break;
            case 57:
                loadFigureQuestion_01();
                break;
            case 58:
                loadFigureQuestion_03();
                break;
            case 59:
                loadMultipleChoiceQuestion();
                break;
            case 60:
                loadMultipleChoiceQuestion();
                break;
            case 61:
                loadMultipleChoiceQuestion();
                break;
            case 62:
                loadMultipleChoiceQuestion();
                break;
            case 63:
                loadMultipleChoiceQuestion();
                break;
            case 64:
                loadFigureQuestion_03();
                break;
            case 65:
                loadFigureQuestion_02();
                break;
            default:

                break;
        }
    }

    private void showNextRiddleGame(){
        int Level = InfoCollector.getRiddleGameLevel();
        switch (Level) {
            case 1:
                loadMultipleChoiceQuestion();
                break;
            case 2:
                loadMultipleChoiceQuestion();
                break;
            case 3:
                loadMultipleChoiceQuestion();
                break;
            case 4:
                loadMultipleChoiceQuestion();
                break;
            case 5:
                loadMultipleChoiceQuestion();
                break;
            case 6:
                loadMultipleChoiceQuestion();
                break;
            case 7:
                loadMultipleChoiceQuestion();
                break;
            case 8:
                loadMultipleChoiceQuestion();
                break;
            case 9:
                loadMultipleChoiceQuestion();
                break;
            case 10:
                loadMultipleChoiceQuestion();
                break;
            case 11:
                loadMultipleChoiceQuestion();
                break;
            case 12:
                loadMultipleChoiceQuestion();
                break;
            case 13:
                loadMultipleChoiceQuestion();
                break;
            case 14:
                loadMultipleChoiceQuestion();
                break;
            case 15:
                loadMultipleChoiceQuestion();
                break;
            case 16:
                loadFigureQuestion_01();
                break;
            case 17:
                loadFigureQuestion_02();
                break;
            case 18:
                loadFigureQuestion_03();
                break;
            case 19:
                loadMultipleChoiceQuestion();
                break;
            case 20:
                loadFigureQuestion_01();
                break;
            case 21:
                loadFigureQuestion_01();
                break;
            case 22:
                loadFigureQuestion_01();
                break;
            case 23:
                loadFigureQuestion_02();
                break;
            case 24:
                loadFigureQuestion_04();
                break;
            case 25:
                loadFigureQuestion_01();
                break;
            case 26:
                loadMultipleChoiceQuestion();
                break;
            case 27:
                loadFigureQuestion_04();
                break;
            case 28:
                loadFigureQuestion_03();
                break;
            case 29:
                loadFigureQuestion_03();
                break;
            case 30:
                loadFigureQuestion_03();
                break;
            case 31:
                loadFigureQuestion_01();
                break;
            case 32:
                loadFigureQuestion_02();
                break;
            case 33:
                loadFigureQuestion_02();
                break;
            case 34:
                loadFigureQuestion_01();
                break;
            case 35:
                loadFigureQuestion_02();
                break;
            case 36:
                loadMultipleChoiceQuestion();
                break;
            case 37:
                loadMultipleChoiceQuestion();
                break;
            case 38:
                loadFigureQuestion_03();
                break;
            case 39:
                loadFigureQuestion_01();
                break;
            case 40:
                loadFigureQuestion_02();
                break;
            case 41:
                loadFigureQuestion_02();
                break;
            case 42:
                loadFigureQuestion_03();
                break;
            case 43:
                loadFigureQuestion_04();
                break;
            case 44:
                loadFigureQuestion_04();
                break;
            case 45:
                loadFigureQuestion_04();
                break;
            case 46:
                loadMultipleChoiceQuestion();
                break;
            case 47:
                loadMultipleChoiceQuestion();
                break;
            case 48:
                loadFigureQuestion_01();
                break;
            case 49:
                loadFigureQuestion_01();
                break;
            case 50:
                loadFigureQuestion_03();
                break;
            case 51:
                loadFigureQuestion_04();
                break;
            case 52:
                loadFigureQuestion_03();
                break;
            case 53:
                loadFigureQuestion_02();
                break;
            case 54:
                loadFigureQuestion_02();
                break;
            case 55:
                loadFigureQuestion_01();
                break;
            case 56:
                loadFigureQuestion_01();
                break;
            case 57:
                loadFigureQuestion_01();
                break;
            case 58:
                loadFigureQuestion_03();
                break;
            case 59:
                loadMultipleChoiceQuestion();
                break;
            case 60:
                loadMultipleChoiceQuestion();
                break;
            case 61:
                loadMultipleChoiceQuestion();
                break;
            case 62:
                loadMultipleChoiceQuestion();
                break;
            case 63:
                loadMultipleChoiceQuestion();
                break;
            case 64:
                loadFigureQuestion_03();
                break;
            case 65:
                loadFigureQuestion_02();
                break;
            default:
                showRandomRiddleGame();
                break;
        }

        Ads.showAds(this.getActivity(), InfoCollector.getRiddleGameLevel());
    }

    private void loadMultipleChoiceQuestion(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MultipleChoiceRiddleGame())
                .commit();
    }

    private void loadFigureQuestion_01(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new Figure_01RiddleGame())
                .commit();
    }

    private void loadFigureQuestion_02(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new Figure_02RiddleGame())
                .commit();
    }

    private void loadFigureQuestion_03(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new Figure_03RiddleGame())
                .commit();
    }

    private void loadFigureQuestion_04(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new Figure_04RiddleGame())
                .commit();
    }

    private void startNextGame(){
        new CountDownTimer(NextGameTime * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                try {
                    showNextGame();
                }
                catch (Exception e){}
            }
        }.start();
    }

    private void showNextGame(){
        if (InfoCollector.getNumberGameSelected()){
            showNextComparisonGame();
        }
        else {
            if (InfoCollector.getFractionGameSelected()){
                showNextComparisonGame();
            }
            else {
                if (InfoCollector.getPuzzleGameSelected()){
                    showNextPuzzleGame();
                }
                else {
                    if (InfoCollector.getRiddleGameSelected()){
                        showNextRiddleGame();
                    }
                }
            }
        }
    }

    private void showLevelCompleteStatus(){
        if (InfoCollector.getGameLevelCompleteStatus()){
            tvLevelCompleteStatus.setText(getResources().getString(R.string.stLevelCompleted));
        }
        else {
            tvLevelCompleteStatus.setText(getResources().getString(R.string.stLevelFailed));
        }
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        showLevelCompleteStatus();
        startNextGame();
    }

    public GameEndFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_game_end, container, false);

        //  GET REFERENCES
        tvLevelCompleteStatus = (TextView) rootView.findViewById(R.id.tvLevelCompletedStatus_GameEndFragment);

        //  START UP
        startUp();

        return rootView;
    }

}
