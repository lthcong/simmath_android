package layout;


import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.*;
import pckAds.Ads;
import pckData.pckFirebase.LogEvents;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckMathTest.MathTest;
import pckString.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class TestResultFragment extends Fragment {

    private LinearLayout lnlyTestDetail;
    private TextView tvNoQuestion, tvNoRight, tvNoWrong, tvScore;
    private Button btnNext;
    private MathTest CurrentTest;
    private double TestScore;
    private String StScore;
    private int CurrentFeature = InfoCollector.getUserSelectedFeature();
    private Dialog RatingDialog;
    private Button btnYes, btnLater;

    private void setAppTitle(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stTestResult));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void setSelection(Button SelectedButton){
        SelectedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showScore(){
        tvNoQuestion.setText(String.valueOf(CurrentTest.getNoQuestion()));
        tvNoRight.setText(String.valueOf(CurrentTest.getNoRight()));
        tvNoWrong.setText(String.valueOf(CurrentTest.getNoWrong()));

        TestScore = CurrentTest.getScore();
        if (InfoCollector.getSATTestSelected()){
            StScore = String.valueOf(TestScore);
        }
        else {
            StScore = String.valueOf(TestScore) + " %";
        }
        tvScore.setText(StScore);
        if (TestScore < 90.0){
            tvScore.setTextColor(Color.RED);
        }
    }

    private void showDetail(){
        int size = CurrentTest.getTestQuestion().size();
        int Count = 0, AdsStep = Ads.WRAPPED_ADS_STEP;

        for (int i = 0; i < size; i++){
            LinearLayout.LayoutParams QuestionLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            QuestionLayoutParams.setMargins(0, 0, 0, 10);

            LinearLayout lnlyQuestionDetail = new LinearLayout(this.getActivity());
            lnlyQuestionDetail.setLayoutParams(QuestionLayoutParams);
            lnlyQuestionDetail.setOrientation(LinearLayout.VERTICAL);

            LinearLayout.LayoutParams TextViewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            TextViewParams.setMargins(100, 0, 0, 0);

            //  ADS
            Count++;
            if (Count % AdsStep == 0){
                Ads.showWrappedAds(this.getActivity(), lnlyTestDetail);
            }

            TextView tvQuestion = new TextView(this.getActivity());
            String Question = "<b>Question " + String.valueOf((i + 1)) + ":</b> " + CurrentTest.getTestQuestion().get(i).getQuestion();
            tvQuestion.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            tvQuestion.setMinHeight(200);
            tvQuestion.setGravity(Gravity.CENTER|Gravity.START);
            StringUtility.writeString(tvQuestion, Question);
            tvQuestion.setTextSize(15);
            tvQuestion.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            TextView tvRightAnswer = new TextView(this.getActivity());
            String RightAnswer = "<b>Right Answer:</b> " + CurrentTest.getTestQuestion().get(i).getRightAnswer();
            tvRightAnswer.setLayoutParams(TextViewParams);
            tvRightAnswer.setMinHeight(200);
            tvRightAnswer.setGravity(Gravity.CENTER|Gravity.START);
            StringUtility.writeString(tvRightAnswer, RightAnswer);
            tvRightAnswer.setTextSize(15);
            tvRightAnswer.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            TextView tvYourAnswer = new TextView(this.getActivity());
            String YourAnswer = "<b>Your Answer:</b> " + CurrentTest.getTestQuestion().get(i).getYourAnswer();
            tvYourAnswer.setLayoutParams(TextViewParams);
            tvYourAnswer.setMinHeight(200);
            tvYourAnswer.setGravity(Gravity.CENTER|Gravity.START);
            StringUtility.writeString(tvYourAnswer, YourAnswer);
            tvYourAnswer.setTextSize(15);
            tvYourAnswer.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            TextView tvResult = new TextView(this.getActivity());
            String Result;
            if (CurrentTest.getTestQuestion().get(i).getResult()){
                Result = "<b>Result:</b> right";
            }
            else {
                Result = "<b>Result:</b> wrong";
            }
            tvResult.setLayoutParams(TextViewParams);
            tvResult.setMinHeight(200);
            tvResult.setGravity(Gravity.CENTER|Gravity.START);
            StringUtility.writeString(tvResult, Result);
            tvResult.setTextSize(15);
            tvResult.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            lnlyQuestionDetail.addView(tvQuestion);
            lnlyQuestionDetail.addView(tvRightAnswer);
            lnlyQuestionDetail.addView(tvYourAnswer);
            lnlyQuestionDetail.addView(tvResult);

            lnlyTestDetail.addView(lnlyQuestionDetail);
        }
    }

    private void saveToDatabase(){
        //  ID
        String CurrentTestID = InfoCollector.getMathTestID();

        //  NAME
        String StName = CurrentTest.getTestName();

        //  DATE
        String StDate = InfoCollector.getDate();

        //  TOPICS
        String StTopics = "";
        int size = InfoCollector.getSelectedTopics().size();
        for (int i = 0; i < size; i++){
            StTopics += InfoCollector.getSelectedTopics().get(i).getName() + "<br>";
        }

        //  TIME
        String StTime;
        if (InfoCollector.getTestTime() == 0){
            StTime = "Untimed";
        }
        else {
            StTime = String.valueOf(InfoCollector.getTestTime()) + " minutes.";
        }

        //  SCORE

        DatabaseFile.addToMathTestTable(CurrentTestID, StName, StDate, StTopics, StTime, StScore);
        switch (CurrentFeature){
            case InfoCollector.MATH_STAGE_INDEX:
                DatabaseFile.addTestToMathStageTable(InfoCollector.getCurrentMathStageID(), CurrentTestID);
                break;
            case InfoCollector.MATH_EXERCISE_INDEX:
                DatabaseFile.updateTestIDInMathExercise(InfoCollector.getMathExerciseID(), CurrentTestID);
                break;
        }

        size = CurrentTest.getTestQuestion().size();
        for (int i = 0; i < size; i++) {
            String StQuestion = CurrentTest.getTestQuestion().get(i).getQuestion();
            String StRightAnswer = CurrentTest.getTestQuestion().get(i).getRightAnswer();
            String StYourAnswer = CurrentTest.getTestQuestion().get(i).getYourAnswer();
            String StResult;
            if (CurrentTest.getTestQuestion().get(i).getResult()){
                StResult = "Right";
            }
            else {
                StResult = "Wrong";
            }

            DatabaseFile.addToMathProblemTable(CurrentTestID, StQuestion, StYourAnswer, StRightAnswer, StResult);
        }
    }

    private void saveMathAssignment(){
        DatabaseFile.updateCompleteAssignment(InfoCollector.getCurrentAssignment());
    }

    private void showAssignmentList(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new AssignmentListFragment())
                .commit();
    }

    private void showMathStageComplete(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathStageCompleteFragment())
                .commit();
    }

    private void showMathExerciseComplete(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathExerciseCompleteFragment())
                .commit();
    }

    private void showHistory(){
        switch (CurrentFeature){
            case InfoCollector.MATH_STAGE_INDEX:
                InfoCollector.setMathStageHistorySelected();
                break;
            case InfoCollector.MATH_EXERCISE_INDEX:
                InfoCollector.setMathExerciseHistorySelected();
                break;
            default:
                InfoCollector.setTestHistorySelected();
                break;
        }

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new HistorySubMenuFragment())
                .commit();
    }

    private void tryAgain(){
        InfoCollector.setMathTestID(MathUtility.getID());

        switch (CurrentFeature){
            case InfoCollector.MATH_STAGE_INDEX:
                InfoCollector.setMathStageLevel(3);

                Objects.requireNonNull(getActivity())
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                        .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathStageTransferFragment())
                        .commit();
                break;
            case InfoCollector.MATH_EXERCISE_INDEX:
                InfoCollector.setMathExerciseSection(2);

                Objects.requireNonNull(getActivity())
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                        .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathExerciseSectionFragment())
                        .commit();
                break;
            default:
                Objects.requireNonNull(getActivity())
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                        .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new RegularTestFragment())
                        .commit();
                break;
        }
    }

    private void showNext(){
        if (TestScore >= InfoCollector.getMathTestPassingScore()){
            //  SAVE INFO
            updateLevel();

            switch(CurrentFeature){
                case InfoCollector.MATH_STAGE_INDEX:
                    //  SAVE MATH ASSIGNMENT
                    if (InfoCollector.getMathConnectSelection()) {
                        saveMathAssignment();
                    }

                    showMathStageComplete();
                    break;
                case InfoCollector.MATH_EXERCISE_INDEX:
                    showMathExerciseComplete();
                    break;
            }

            //  SHOW RATING
            if (MathUtility.getRandomPositiveNumber_1Digit() % 5 == 0) {
                showRatingFragment();
            }
            else {
                //  SHOW MATH ASSIGNMENT
                if (InfoCollector.getMathConnectSelection()){
                    showAssignmentList();
                }
                else {
                    if (InfoCollector.getMathStageSelection()){
                        showMathStageComplete();
                    }
                    else {
                        //  SHOW HISTORY
                        showHistory();
                    }
                }

                //  ADS
                Ads.showAds(this.getActivity());
            }
        }
        else {
            if (InfoCollector.getRegularTestSelected()) {
                tryAgain();
            }
            else {
                showHistory();
            }
        }
    }

    private void logRating(){
        LogEvents.writeToLog(this.getActivity(), "AppRating", "AppRating", "AppRating");
    }

    private void updateLevel(){
        Cursor CurrentUserLevel = DatabaseFile.getUserLevel();
        CurrentUserLevel.moveToFirst();

        int CurrentLevel = Integer.parseInt(CurrentUserLevel.getString(0).trim());
        int CurrentTopicIndex = InfoCollector.getMathStageTopic();
        if (CurrentLevel > CurrentTopicIndex){
            DatabaseFile.updateUserLevel(CurrentTopicIndex + 1);
            InfoCollector.getUserInfo();
        }
        else {
            if (CurrentLevel == CurrentTopicIndex){
                DatabaseFile.updateUserLevel(CurrentLevel + 1);
                InfoCollector.getUserInfo();
            }
        }
    }

    private void setupRatingDialog(){
        RatingDialog = new Dialog(Objects.requireNonNull(this.getActivity()));
        RatingDialog.setContentView(R.layout.fragment_rating);

        btnYes = RatingDialog.findViewById(R.id.btnYes_RatingFragment);
        btnYes.setTextColor(Color.GRAY);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection((Button) v);
                logRating();
                goToGooglePlay();
                RatingDialog.dismiss();
                if (InfoCollector.getMathStageSelection()){
                    showMathStageComplete();
                }
                else {
                    showHistory();
                }
            }
        });

        btnLater = RatingDialog.findViewById(R.id.btnLater_RatingFragment);
        btnLater.setTextColor(Color.GRAY);
        btnLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection((Button) v);
                RatingDialog.dismiss();
                if (InfoCollector.getMathStageSelection()){
                    showMathStageComplete();
                }
                else {
                    showHistory();
                }
            }
        });
    }

    private void goToGooglePlay(){
        Intent MyMathAppIntent = new Intent(Intent.ACTION_VIEW);
        MyMathAppIntent.setData(Uri.parse("market://details?id=adsfree.congla.android.cong.mymathapp"));
        startActivity(MyMathAppIntent);
    }

    private void showRatingFragment(){
        setupRatingDialog();
        RatingDialog.show();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setAppTitle();
        CurrentTest = InfoCollector.getCurrentTest();
        CurrentTest.gradeTest();

        //  SHOW RESULT
        showScore();
        showDetail();

        //  SAVE DATA
        saveToDatabase();

        //  SHOW ADS
        Ads.showAds(this.getActivity());
    }

    public TestResultFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_test_result, container, false);

        //  GET REFERENCES
        lnlyTestDetail = rootView.findViewById(R.id.lnlyTestDetail_TestResultFragment);

        btnNext = rootView.findViewById(R.id.btnNext_TestResultFragment);
        btnNext.setTextColor(Color.GRAY);

        tvNoQuestion = rootView.findViewById(R.id.tvNoQuestion_TestResultFragment);
        tvNoRight = rootView.findViewById(R.id.tvNoRight_TestResultFragment);
        tvNoWrong = rootView.findViewById(R.id.tvNoWrong_TestResultFragment);
        tvScore = rootView.findViewById(R.id.tvScore_TestResultFragment);

        //  SET LISTENER
        ButtonClickListener ClickListener = new ButtonClickListener();
        btnNext.setOnClickListener(ClickListener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            setSelection((Button) v);

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnNext_TestResultFragment:
                    showNext();
                    break;
            }
        }
    }

}
