package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.ArrayList;
import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckData.pckFirebase.LogEvents;
import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckMathForKid.OrderingNumbers;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderingNumbersFragment extends Fragment {

    private TextView tvSmallestNumber, tvSmallerNumber, tvSmallNumber, 
                    tvBigNumber, tvBiggerNumber, tvBiggestNumber,
                    tvCompleteMessage;
    private Button btnSmallestNumber, btnSmallerNumber, btnSmallNumber,
            btnBigNumber, btnBiggerNumber, btnBiggestNumber;
    private CountDownTimer cdtCountDownTime;
    private ArrayList<Button> ButtonList;

    private OrderingNumbers Problem;
    private String YourAnswer;
    private int OrderOfNumber = 1;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stOrderingNumbers));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void addBackButton(){
        getChildFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyButtonBackContainer_OrderingNumbersFragment, new ButtonBackToMathForKidFragment())
                .commit();
    }

    private void showQuestionMark(){
        switch (OrderOfNumber){
            case 1:
                tvSmallestNumber.setText("?");
                tvSmallestNumber.setTextColor(Color.RED);
                break;
            case 2:
                tvSmallerNumber.setText("?");
                tvSmallerNumber.setTextColor(Color.RED);
                break;
            case 3:
                tvSmallNumber.setText("?");
                tvSmallNumber.setTextColor(Color.RED);
                break;
            case 4:
                tvBigNumber.setText("?");
                tvBigNumber.setTextColor(Color.RED);
                break;
            case 5:
                tvBiggerNumber.setText("?");
                tvBiggerNumber.setTextColor(Color.RED);
                break;
            case 6:
                tvBiggestNumber.setText("?");
                tvBiggestNumber.setTextColor(Color.RED);
                break;
            default:
                tvSmallestNumber.setText("?");
                tvSmallestNumber.setTextColor(Color.RED);
                break;
        }
    }

    private void setupButtonList(){
        ButtonList = new ArrayList<>();

        ButtonList.add(btnSmallestNumber);
        ButtonList.add(btnSmallerNumber);
        ButtonList.add(btnSmallNumber);
        ButtonList.add(btnBigNumber);
        ButtonList.add(btnBiggerNumber);
        ButtonList.add(btnBiggestNumber);

        swapButtonList();
    }

    private void swapButtonList(){
        int size = ButtonList.size();
        int swapTime = MathUtility.getRandomPositiveNumber_2Digit();
        for (int i = 0; i < swapTime; i++){
            int swapIndex = MathUtility.getRandomPositiveNumber_2Digit() % size;
            Button Temp = ButtonList.get(swapIndex);
            ButtonList.remove(swapIndex);
            ButtonList.add(Temp);
        }
    }

    private void showNumber(){
        setupButtonList();

        ButtonList.get(0).setText(String.valueOf(Problem.getFirstNumber()));
        ButtonList.get(1).setText(String.valueOf(Problem.getSecondNumber()));
        ButtonList.get(2).setText(String.valueOf(Problem.getThirdNumber()));
        ButtonList.get(3).setText(String.valueOf(Problem.getForthNumber()));
        ButtonList.get(4).setText(String.valueOf(Problem.getFifthNumber()));
        ButtonList.get(5).setText(String.valueOf(Problem.getSixthNumber()));
    }

    private void showProblem(){
        Problem = new OrderingNumbers();

        //  SHOW QUESTION MARK
        showQuestionMark();

        //  SHOW NUMBER
        showNumber();
        clearSelection();
    }

    private void clearSelection(){
        int size = ButtonList.size();
        for(int i = 0; i < size; i++){
            ButtonList.get(i).setTextColor(Color.GRAY);
        }
    }

    private void checkAnswer(Button btnSelectedAnswer){
        //  SET SELECTION
        clearSelection();
        btnSelectedAnswer.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        //  GET YOUR ANSWER
        YourAnswer = btnSelectedAnswer.getText().toString().trim();

        //  SHOW RESULT
        switch (OrderOfNumber){
            case 1:
                if (YourAnswer.equalsIgnoreCase(String.valueOf(Problem.getFirstNumber()))){
                    tvSmallestNumber.setText(YourAnswer);
                    tvSmallestNumber.setTextColor(Color.GREEN);

                    btnSelectedAnswer.setVisibility(View.GONE);

                    OrderOfNumber++;
                    showQuestionMark();
                }
                else {
                    tvSmallestNumber.setText(YourAnswer);
                    tvSmallestNumber.setTextColor(Color.RED);
                }
                break;
            case 2:
                if (YourAnswer.equalsIgnoreCase(String.valueOf(Problem.getSecondNumber()))){
                    tvSmallerNumber.setText(YourAnswer);
                    tvSmallerNumber.setTextColor(Color.GREEN);

                    btnSelectedAnswer.setVisibility(View.GONE);

                    OrderOfNumber++;
                    showQuestionMark();
                }
                else {
                    tvSmallerNumber.setText(YourAnswer);
                    tvSmallerNumber.setTextColor(Color.RED);
                }
                break;
            case 3:
                if (YourAnswer.equalsIgnoreCase(String.valueOf(Problem.getThirdNumber()))){
                    tvSmallNumber.setText(YourAnswer);
                    tvSmallNumber.setTextColor(Color.GREEN);

                    btnSelectedAnswer.setVisibility(View.GONE);

                    OrderOfNumber++;
                    showQuestionMark();
                }
                else {
                    tvSmallNumber.setText(YourAnswer);
                    tvSmallNumber.setTextColor(Color.RED);
                }
                break;
            case 4:
                if (YourAnswer.equalsIgnoreCase(String.valueOf(Problem.getForthNumber()))){
                    tvBigNumber.setText(YourAnswer);
                    tvBigNumber.setTextColor(Color.GREEN);

                    btnSelectedAnswer.setVisibility(View.GONE);

                    OrderOfNumber++;
                    showQuestionMark();
                }
                else {
                    tvBigNumber.setText(YourAnswer);
                    tvBigNumber.setTextColor(Color.RED);
                }
                break;
            case 5:
                if (YourAnswer.equalsIgnoreCase(String.valueOf(Problem.getFifthNumber()))){
                    tvBiggerNumber.setText(YourAnswer);
                    tvBiggerNumber.setTextColor(Color.GREEN);

                    btnSelectedAnswer.setVisibility(View.GONE);

                    OrderOfNumber++;
                    showQuestionMark();
                }
                else {
                    tvBiggerNumber.setText(YourAnswer);
                    tvBiggerNumber.setTextColor(Color.RED);
                }
                break;
            case 6:
                if (YourAnswer.equalsIgnoreCase(String.valueOf(Problem.getSixthNumber()))){
                    tvBiggestNumber.setText(YourAnswer);
                    tvBiggestNumber.setTextColor(Color.GREEN);

                    btnSelectedAnswer.setVisibility(View.GONE);

                    showMessage();
                    showNextGame();
                }
                else {
                    tvBiggestNumber.setText(YourAnswer);
                    tvBiggestNumber.setTextColor(Color.RED);
                }
                break;
        }
    }

    private void hideMessage(){
        tvCompleteMessage.setVisibility(View.GONE);
    }

    private void showMessage(){
        tvCompleteMessage.setVisibility(View.VISIBLE);

        String StMessage = "GREAT JOB.";
        tvCompleteMessage.setText(StMessage);
        tvCompleteMessage.setTextColor(Color.GREEN);
    }

    private void startNextGame(){
        try {
            getActivity()
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                    .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new OrderingNumbersFragment())
                    .commit();
        }
        catch (Exception e){
            startUp();
        }
    }

    private void showNextGame(){
        cdtCountDownTime = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                startNextGame();
                showAds();
            }

        }.start();
    }

    private void showAds(){
        Ads.showAds(this.getActivity());
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        addBackButton();
        hideMessage();
        showProblem();
    }
    
    public OrderingNumbersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_ordering_numbers, container, false);

        //  GET REFERENCES
        tvCompleteMessage = rootView.findViewById(R.id.tvCompleteMessage_OrderingNumbersFragment);
        tvCompleteMessage.setTextColor(Color.GRAY);

        tvSmallestNumber = rootView.findViewById(R.id.tvFirstNumber_OrderingNumbersFragment);
        tvSmallestNumber.setTextColor(Color.GRAY);

        tvSmallerNumber = rootView.findViewById(R.id.tvSecondNumber_OrderingNumbersFragment);
        tvSmallerNumber.setTextColor(Color.GRAY);

        tvSmallNumber = rootView.findViewById(R.id.tvThirdNumber_OrderingNumbersFragment);
        tvSmallNumber.setTextColor(Color.GRAY);

        tvBigNumber = rootView.findViewById(R.id.tvForthNumber_OrderingNumbersFragment);
        tvBigNumber.setTextColor(Color.GRAY);

        tvBiggerNumber = rootView.findViewById(R.id.tvFifthNumber_OrderingNumbersFragment);
        tvBiggerNumber.setTextColor(Color.GRAY);

        tvBiggestNumber = rootView.findViewById(R.id.tvSixthNumber_OrderingNumbersFragment);
        tvBiggestNumber.setTextColor(Color.GRAY);

        btnSmallestNumber = rootView.findViewById(R.id.btnFirstNumber_OrderingNumbersFragment);
        btnSmallerNumber = rootView.findViewById(R.id.btnSecondNumber_OrderingNumbersFragment);
        btnSmallNumber = rootView.findViewById(R.id.btnThirdNumber_OrderingNumbersFragment);
        btnBigNumber = rootView.findViewById(R.id.btnForthNumber_OrderingNumbersFragment);
        btnBiggerNumber = rootView.findViewById(R.id.btnFifthNumber_OrderingNumbersFragment);
        btnBiggestNumber = rootView.findViewById(R.id.btnSixthNumber_OrderingNumbersFragment);
        
        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnSmallestNumber.setOnClickListener(ButtonListener);
        btnSmallerNumber.setOnClickListener(ButtonListener);
        btnSmallNumber.setOnClickListener(ButtonListener);
        btnBigNumber.setOnClickListener(ButtonListener);
        btnBiggerNumber.setOnClickListener(ButtonListener);
        btnBiggestNumber.setOnClickListener(ButtonListener);
        
        //  START UP
        startUp();
        
        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btnFirstNumber_OrderingNumbersFragment:
                    checkAnswer((Button) v);
                    break;
                case R.id.btnSecondNumber_OrderingNumbersFragment:
                    checkAnswer((Button) v);
                    break;
                case R.id.btnThirdNumber_OrderingNumbersFragment:
                    checkAnswer((Button) v);
                    break;
                case R.id.btnForthNumber_OrderingNumbersFragment:
                    checkAnswer((Button) v);
                    break;
                case R.id.btnFifthNumber_OrderingNumbersFragment:
                    checkAnswer((Button) v);
                    break;
                case R.id.btnSixthNumber_OrderingNumbersFragment:
                    checkAnswer((Button) v);
                    break;
            }
        }
    }

}
