package layout;


import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckData.pckFirebase.LogEvents;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;
import pckMath.pckMathTopics.RoundCollection;

/**
 * A simple {@link Fragment} subclass.
 */
public class MathRoundInfoFragment extends Fragment {

    private Button btnTopicSelection, btnStartMathRound;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathRound));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void logMathRound(){
        LogEvents.writeToLog(this.getActivity(), "MathRoundSelected", "MathRound", "MathRound");
    }

    private void showTopicSelection(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathRoundTopicSelecionFragment())
                .commit();
    }

    private void startMathRound(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathRoundQuestionFragment())
                .commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        logMathRound();
    }

    public MathRoundInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_round_info, container, false);

        //  GET REFERENCES
        btnTopicSelection = rootView.findViewById(R.id.btnTopicSelection_MathRoundInfoFragment);
        btnTopicSelection.setTextColor(Color.GRAY);

        btnStartMathRound = rootView.findViewById(R.id.btnStart_MathRoundInfoFragment);
        btnStartMathRound.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnTopicSelection.setOnClickListener(ButtonListener);
        btnStartMathRound.setOnClickListener(ButtonListener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnTopicSelection_MathRoundInfoFragment:
                    showTopicSelection();
                    break;
                case R.id.btnStart_MathRoundInfoFragment:
                    startMathRound();
                    break;
            }
        }
    }

}
