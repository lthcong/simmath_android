package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckInfo.InfoCollector;
import pckMath.pckGame.pckComparison.ComparisonGame;
import pckString.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComparisonGameFragment extends Fragment {

    private TextView tvInstruction, tvFirstNumber, tvSecondNumber, tvThirdNumber;
    private ComparisonGame ComparingGame;
    private String YourAnswer;
    private CountDownTimer cdtCountDownTime;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stComparison));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void showInstruction(){
        StringUtility.writeString(tvInstruction, ComparingGame.getInstruction());
    }
    
    private void showNumbers(){
        StringUtility.writeString(tvFirstNumber, ComparingGame.getFirstNumber());
        StringUtility.writeString(tvSecondNumber, ComparingGame.getSecondNumber());
        StringUtility.writeString(tvThirdNumber, ComparingGame.getThirdNumber());
    }

    private void checkYourAnswer(){
        //  CHECK ANSWER
        if (YourAnswer.equalsIgnoreCase(ComparingGame.getRightAnswer())){
            InfoCollector.setGameLevelCompleteStatus(true);
            InfoCollector.setComparisonGameNumber(InfoCollector.getComparisonGameLevel() + 1);
        }
        else {
            InfoCollector.setGameLevelCompleteStatus(false);
            InfoCollector.setComparisonGameNumber(1);
        }

        //  SHOW GAME END
        cdtCountDownTime = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                showGameEnd();
            }

        }.start();
    }

    private void showGameEnd(){
        try {
            Objects.requireNonNull(getActivity())
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                    .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new GameEndFragment())
                    .commit();
        }
        catch (Exception e) {
            startUp();
        }
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        ComparingGame = new ComparisonGame();
        showInstruction();
        showNumbers();
    }
    
    public ComparisonGameFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_comparison_game, container, false);

        //  GET REFERENCES
        tvInstruction = rootView.findViewById(R.id.tvInstruction_ComparisonGameFragment);

        tvFirstNumber = rootView.findViewById(R.id.tvFirstNumber_ComparisonGameFragment);
        tvFirstNumber.setTextColor(Color.GRAY);

        tvSecondNumber = rootView.findViewById(R.id.tvSecondNumber_ComparisonGameFragment);
        tvSecondNumber.setTextColor(Color.GRAY);

        tvThirdNumber = rootView.findViewById(R.id.tvThirdNumber_ComparisonGameFragment);
        tvThirdNumber.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        tvFirstNumber.setOnClickListener(ButtonListener);
        tvSecondNumber.setOnClickListener(ButtonListener);
        tvThirdNumber.setOnClickListener(ButtonListener);
        
        //  START UP
        startUp();
        
        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((TextView) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.tvFirstNumber_ComparisonGameFragment:
                    YourAnswer = ComparingGame.getFirstNumber();
                    break;
                case R.id.tvSecondNumber_ComparisonGameFragment:
                    YourAnswer = ComparingGame.getSecondNumber();
                    break;
                case R.id.tvThirdNumber_ComparisonGameFragment:
                    YourAnswer = ComparingGame.getThirdNumber();
                    break;
            }

            //  CHECK ANSWER
            checkYourAnswer();
        }
    }

}
