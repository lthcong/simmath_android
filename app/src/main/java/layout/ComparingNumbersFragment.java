package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckData.pckFirebase.LogEvents;
import pckInfo.InfoCollector;
import pckMath.pckMathForKid.ComparingNumbers;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComparingNumbersFragment extends Fragment {

    private TextView tvResult;
    private Button btnFirstNumber, btnSecondNumber, btnGreaterThanSign, btnLessThanSign, btnEqualSign;
    private CountDownTimer cdtCountDownTime;
    private int NoProblem = 0;
    private ComparingNumbers Problem;
    private String YourAnswer;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stComparingNumbers));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void addBackButton(){
        getChildFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyButtonBackContainer_ComparingNumbersFragment, new ButtonBackToMathForKidFragment())
                .commit();
    }

    private void showSelection(){
        btnGreaterThanSign.setTextColor(Color.GRAY);
        btnGreaterThanSign.setText(" > ");

        btnLessThanSign.setTextColor(Color.GRAY);
        btnLessThanSign.setText(" < ");

        btnEqualSign.setTextColor(Color.GRAY);
        btnEqualSign.setText(" = ");
    }

    private void getProblem(){
        NoProblem++;
        Problem = new ComparingNumbers();

        //  HIDE RESULT
        tvResult.setVisibility(View.GONE);

        //  SHOW NUMBERS
        btnFirstNumber.setText(String.valueOf(Problem.getFirstNumber()));
        btnSecondNumber.setText(String.valueOf(Problem.getSecondNumber()));

        //  SHOW SELECTION
        showSelection();
    }

    private void checkAnswer(Button btnSelectedAnswer){
        //  GET YOUR ANSWER
        YourAnswer = btnSelectedAnswer.getText().toString().trim();

        //  CHECK ANSWER
        tvResult.setVisibility(View.VISIBLE);
        if (YourAnswer.equalsIgnoreCase(Problem.getRightAnswer().trim())){
            tvResult.setText("CORRECT");
            tvResult.setTextColor(Color.GREEN);
        }
        else {
            tvResult.setText("NOT CORRECT");
            tvResult.setTextColor(Color.RED);
        }

        //  SHOW NEW PROBLEM
        cdtCountDownTime = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                getProblem();
            }

        }.start();
    }

    private void showAds(){
        Ads.showAds(this.getActivity(), NoProblem);
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        addBackButton();
        getProblem();
    }

    public ComparingNumbersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_comparing_numbers, container, false);

        //  GET REFERENCES
        tvResult = rootView.findViewById(R.id.tvResult_ComparingNumbersFragment);

        btnFirstNumber = rootView.findViewById(R.id.btnFirstNumber_ComparingNumbersFragment);
        btnFirstNumber.setTextColor(Color.GRAY);

        btnSecondNumber = rootView.findViewById(R.id.btnSecondNumber_ComparingNumbersFragment);
        btnSecondNumber.setTextColor(Color.GRAY);

        btnGreaterThanSign = rootView.findViewById(R.id.btnGreaterThanSign_ComparingNumbersFragment);
        btnGreaterThanSign.setTextColor(Color.GRAY);

        btnLessThanSign = rootView.findViewById(R.id.btnLessThanSign_ComparingNumbersFragment);
        btnLessThanSign.setTextColor(Color.GRAY);

        btnEqualSign = rootView.findViewById(R.id.btnEqualSign_ComparingNumbersFragment);
        btnEqualSign.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnGreaterThanSign.setOnClickListener(ButtonListener);
        btnLessThanSign.setOnClickListener(ButtonListener);
        btnEqualSign.setOnClickListener(ButtonListener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnGreaterThanSign_ComparingNumbersFragment:
                    checkAnswer((Button) v);
                    break;
                case R.id.btnLessThanSign_ComparingNumbersFragment:
                    checkAnswer((Button) v);
                    break;
                case R.id.btnEqualSign_ComparingNumbersFragment:
                    checkAnswer((Button) v);
                    break;
            }

            //  ADS
            showAds();
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

}
