package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.FindTheCorrectNumberFragment;
import adsfree.congla.android.cong.mymathapp.FindTheRightOperation;
import adsfree.congla.android.cong.mymathapp.R;
import pckData.pckFirebase.LogEvents;
import pckInfo.InfoCollector;

/**
 * A simple {@link Fragment} subclass.
 */
public class MathForKidInfoFragment extends Fragment {

    private TextView tvFindTheCorrectNumber, tvOne, tvTwo, tvThree, tvFour, tvFindTheRightOperation;
    private Button btnFindTheCorrectNumber, btnEvenAndOdd, btnComparingNumbers, btnOrderingNumbers, btnWhoAmI, btnFindTheRightOperation;

    private void logMathForKid(){
        LogEvents.writeToLog(this.getActivity(), "MathForKidSelected", "MathForKid", "MathForKid");
    }

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMathForKid));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void showFindTheCorrectNumberSelection(){
        LogEvents.writeToLog(this.getActivity(), "MathForKidSelected", "FindTheCorrectNumber", "FindTheCorrectNumber");

        tvFindTheCorrectNumber.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnFindTheCorrectNumber.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showFindTheCorrectNumber(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new FindTheCorrectNumberFragment())
                .commit();
    }
    
    private void showEvenAndOddSelection(){
        LogEvents.writeToLog(this.getActivity(), "MathForKidSelected", "EvenAndOdd", "EvenAndOdd");

        tvOne.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnEvenAndOdd.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }
    
    private void showEvenAndOdd(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new EvenAndOddFragment())
                .commit();
    }

    private void showComparingNumbersSelection(){
        LogEvents.writeToLog(this.getActivity(), "MathForKidSelected", "ComparingNumbers", "ComparingNumbers");

        tvTwo.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnComparingNumbers.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showComparingNumbers(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new ComparingNumbersFragment())
                .commit();
    }

    private void showOrderingNumbersSelection(){
        LogEvents.writeToLog(this.getActivity(), "MathForKidSelected", "OrderingNumbers", "OrderingNumbers");

        tvThree.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnOrderingNumbers.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showOrderingNumbers(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new OrderingNumbersFragment())
                .commit();
    }

    private void showWhoAmISelection(){
        LogEvents.writeToLog(this.getActivity(), "MathForKidSelected", "WhoAmI", "WhoAmI");

        tvFour.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnWhoAmI.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showWhoAmI(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new WhoAmIFragment())
                .commit();
    }

    private void showFindTheRightOperationSelection(){
        LogEvents.writeToLog(this.getActivity(), "MathForKidSelected", "FindTheRightOperation", "FindTheRightOperation");

        tvFindTheRightOperation.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        btnFindTheRightOperation.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void showFindTheRightOperation(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new FindTheRightOperation())
                .commit();
    }
    
    private void startup(){
        InfoCollector.setCurrentFragment(this);
        logMathForKid();
        setupAppTile();
    }

    public MathForKidInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_math_for_kid_info, container, false);
        
        //  GET REFERENCES
        tvFindTheCorrectNumber = rootView.findViewById(R.id.tvFindTheCorrectNumber_MathForKidInfoFragment);
        tvFindTheCorrectNumber.setTextColor(Color.GRAY);
        
        tvOne = rootView.findViewById(R.id.tvOne_MathForKidInfoFragment);
        tvOne.setTextColor(Color.GRAY);

        tvTwo = rootView.findViewById(R.id.tvTwo_MathForKidInfoFragment);
        tvTwo.setTextColor(Color.GRAY);

        tvThree = rootView.findViewById(R.id.tvThree_MathForKidInfoFragment);
        tvThree.setTextColor(Color.GRAY);

        tvFour = rootView.findViewById(R.id.tvFour_MathForKidInfoFragment);
        tvFour.setTextColor(Color.GRAY);

        tvFindTheRightOperation = rootView.findViewById(R.id.tvFindTheRightOperation_MathForKidInfoFragment);
        tvFindTheRightOperation.setTextColor(Color.GRAY);

        btnFindTheCorrectNumber = rootView.findViewById(R.id.btnFindTheCorrectNumber_MathForKidInfoFragment);
        btnFindTheCorrectNumber.setTextColor(Color.GRAY);
        
        btnEvenAndOdd = rootView.findViewById(R.id.btnEvenOrOdd_MathForKidInfoFragment);
        btnEvenAndOdd.setTextColor(Color.GRAY);

        btnComparingNumbers = rootView.findViewById(R.id.btnComparingNumbers_MathForKidInfoFragment);
        btnComparingNumbers.setTextColor(Color.GRAY);

        btnOrderingNumbers = rootView.findViewById(R.id.btnOrderingNumbers_MathForKidInfoFragment);
        btnOrderingNumbers.setTextColor(Color.GRAY);

        btnWhoAmI = rootView.findViewById(R.id.btnWhoAmI_MathForKidInfoFragment);
        btnWhoAmI.setTextColor(Color.GRAY);

        btnFindTheRightOperation = rootView.findViewById(R.id.btnFindTheRightOperation_MathForKidInfoFragment);
        btnFindTheRightOperation.setTextColor(Color.GRAY);
        
        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        tvFindTheCorrectNumber.setOnClickListener(ButtonListener);
        tvOne.setOnClickListener(ButtonListener);
        tvTwo.setOnClickListener(ButtonListener);
        tvThree.setOnClickListener(ButtonListener);
        tvFour.setOnClickListener(ButtonListener);
        tvFindTheRightOperation.setOnClickListener(ButtonListener);

        btnFindTheCorrectNumber.setOnClickListener(ButtonListener);
        btnEvenAndOdd.setOnClickListener(ButtonListener);
        btnComparingNumbers.setOnClickListener(ButtonListener);
        btnOrderingNumbers.setOnClickListener(ButtonListener);
        btnWhoAmI.setOnClickListener(ButtonListener);
        btnFindTheRightOperation.setOnClickListener(ButtonListener);

        //  START UP
        startup();

        return rootView;
    }
    
    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tvFindTheCorrectNumber_MathForKidInfoFragment:
                    showFindTheCorrectNumberSelection();
                    showFindTheCorrectNumber();
                    break;
                case R.id.tvOne_MathForKidInfoFragment:
                    showEvenAndOddSelection();
                    showEvenAndOdd();
                    break;
                case R.id.tvTwo_MathForKidInfoFragment:
                    showComparingNumbersSelection();
                    showComparingNumbers();
                    break;
                case R.id.tvThree_MathForKidInfoFragment:
                    showOrderingNumbersSelection();
                    showOrderingNumbers();
                    break;
                case R.id.tvFour_MathForKidInfoFragment:
                    showWhoAmISelection();
                    showWhoAmI();
                    break;
                case R.id.tvFindTheRightOperation_MathForKidInfoFragment:
                    showFindTheRightOperationSelection();
                    showFindTheRightOperation();
                    break;
                case R.id.btnFindTheCorrectNumber_MathForKidInfoFragment:
                    showFindTheCorrectNumberSelection();
                    showFindTheCorrectNumber();
                    break;
                case R.id.btnEvenOrOdd_MathForKidInfoFragment:
                    showEvenAndOddSelection();
                    showEvenAndOdd();
                    break;
                case R.id.btnComparingNumbers_MathForKidInfoFragment:
                    showComparingNumbersSelection();
                    showComparingNumbers();
                    break;
                case R.id.btnOrderingNumbers_MathForKidInfoFragment:
                    showOrderingNumbersSelection();
                    showOrderingNumbers();
                    break;
                case R.id.btnWhoAmI_MathForKidInfoFragment:
                    showWhoAmISelection();
                    showWhoAmI();
                    break;
                case R.id.btnFindTheRightOperation_MathForKidInfoFragment:
                    showFindTheRightOperationSelection();
                    showFindTheRightOperation();
                    break;
            }
        }
    }

}
