package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.MathTestStartingFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckInfo.InfoCollector;
import pckMath.MathUtility;
import pckMath.pckMathTopics.Topic;
import pckMath.pckMathTopics.TopicCollection;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopicsSelectionFragment extends Fragment {

    private LinearLayout lnlyTopicsContainer;
    private Button btnAll, btnBasic, btnIntermediate, btnPreAlgebra, btnAlgebra1, btnAlgebra2,
            btnBack, btnStart;
    private EditText edtSearchYourTopic;
    private TextView tvError;
    private ArrayList<Topic> TopicList;
    private int SelectedSection;

    private void setAppTitle(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stTopicSelection));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void setButtonClickSelection(Button btnClickedButton){
        btnClickedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void clearSectionSelection(){
        btnAll.setTextColor(Color.GRAY);
        btnBasic.setTextColor(Color.GRAY);
        btnIntermediate.setTextColor(Color.GRAY);
        btnPreAlgebra.setTextColor(Color.GRAY);
        btnAlgebra1.setTextColor(Color.GRAY);
        btnAlgebra2.setTextColor(Color.GRAY);
    }

    private void searchBySection(Button btnSelectedSection){
        //  CLEAR SELECTION
        clearSectionSelection();

        //  SET SELECTION
        setButtonClickSelection(btnSelectedSection);

        //  SHOW TOPIC
        TopicList = TopicCollection.searchBySection(SelectedSection);
        showTopicsList();
    }

    private void searchTopic(){
        //  START SEARCHING
        String SearchName = edtSearchYourTopic.getText().toString().trim();
        if (SearchName.length() > 0) {
            TopicList = TopicCollection.searchTopic(SearchName);
        }
        else {
            TopicList = TopicCollection.getTopicList();
        }

        //  SHOW RESULT
        showTopicsList();
    }

    private void showTopicsList(){
        int Count = 0, AdsStep = Ads.WRAPPED_ADS_STEP * 5;
        lnlyTopicsContainer.removeAllViews();

        int size = TopicList.size();
        for (int i = 0; i < size; i++){
            LinearLayout.LayoutParams TopicsListParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            LinearLayout lnlyTopicListLayout = new LinearLayout(this.getActivity());
            lnlyTopicListLayout.setLayoutParams(TopicsListParams);
            lnlyTopicListLayout.setOrientation(LinearLayout.VERTICAL);
            lnlyTopicListLayout.setBackgroundColor(Color.parseColor(InfoCollector.getTextColor()));

            //  ADS
            Count++;
            if (Count % AdsStep == 0){
                Ads.showWrappedAds(this.getActivity(), lnlyTopicsContainer);
            }

            TextView tvTopicName = new TextView(this.getActivity());
            tvTopicName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            tvTopicName.setBackgroundColor(Color.WHITE);
            tvTopicName.setMinHeight(200);
            tvTopicName.setGravity(Gravity.CENTER|Gravity.START);
            tvTopicName.setId(i);
            tvTopicName.setOnClickListener(new TopicSelectionListener());

            String TopicName = TopicList.get(i).getName();
            if (InfoCollector.isTopicSelected(TopicName)){
                TopicName = getResources().getString(R.string.stCheckMark) + "\t\t" + TopicName;
                tvTopicName.setText(TopicName);
                tvTopicName.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
            }
            else {
                tvTopicName.setText(TopicName);
                tvTopicName.setTextColor(Color.GRAY);
            }

            tvTopicName.setTextSize(15);

            lnlyTopicListLayout.addView(tvTopicName);

            lnlyTopicsContainer.addView(lnlyTopicListLayout);
        }

    }

    private void showTimeSelection(){
        Objects.requireNonNull(getActivity()).getSupportFragmentManager().
                beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0).
                replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new TimeSelectionFragment()).commit();
    }

    private void hideSelectionError(){
        tvError.setVisibility(View.GONE);
    }

    private void showSelectionError(){
        tvError.setVisibility(View.VISIBLE);
    }

    private void startTest(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathTestStartingFragment())
                .commit();
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setAppTitle();

        InfoCollector.clearSelectedTopics();
        TopicList = TopicCollection.getTopicList();
        showTopicsList();

        hideSelectionError();
    }

    public TopicsSelectionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_topics_selection, container, false);

        //  GET REFERENCES
        edtSearchYourTopic = rootView.findViewById(R.id.edtSearchYourTopic_TopicSelectionFragment);
        tvError = rootView.findViewById(R.id.tvTopicSelectionError_TopicSelectionFragment);
        lnlyTopicsContainer = rootView.findViewById(R.id.lnlyTopicsContainer_TopicsSelectionFragment);

        btnAll = rootView.findViewById(R.id.btnAll_TopicSelectionFragment);
        btnAll.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        btnBasic = rootView.findViewById(R.id.btnBasic_TopicSelectionFragment);
        btnBasic.setTextColor(Color.GRAY);

        btnIntermediate = rootView.findViewById(R.id.btnIntermediate_TopicSelectionFragment);
        btnIntermediate.setTextColor(Color.GRAY);

        btnPreAlgebra = rootView.findViewById(R.id.btnPreAlgebra_TopicSelectionFragment);
        btnPreAlgebra.setTextColor(Color.GRAY);

        btnAlgebra1 = rootView.findViewById(R.id.btnAlgebra_1_TopicSelectionFragment);
        btnAlgebra1.setTextColor(Color.GRAY);

        btnAlgebra2 = rootView.findViewById(R.id.btnAlgebra_2_TopicSelectionFragment);
        btnAlgebra2.setTextColor(Color.GRAY);

        btnBack = rootView.findViewById(R.id.btnPreviousSelection_TopicsSelectionFragment);
        btnBack.setTextColor(Color.GRAY);

        btnStart = rootView.findViewById(R.id.btnStart_TopicsSelectionFragment);
        btnStart.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonListener button_listener = new ButtonListener();
        btnAll.setOnClickListener(button_listener);
        btnBasic.setOnClickListener(button_listener);
        btnIntermediate.setOnClickListener(button_listener);
        btnPreAlgebra.setOnClickListener(button_listener);
        btnAlgebra1.setOnClickListener(button_listener);
        btnAlgebra2.setOnClickListener(button_listener);
        btnBack.setOnClickListener(button_listener);
        btnStart.setOnClickListener(button_listener);

        SearchingListener search_listener = new SearchingListener();
        edtSearchYourTopic.addTextChangedListener(search_listener);

        //  START UP
        startUp();

        return rootView;
    }

    private class ButtonListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnAll_TopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_All;
                    searchBySection((Button) v);
                    break;
                case R.id.btnBasic_TopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_Basic;
                    searchBySection((Button) v);
                    break;
                case R.id.btnIntermediate_TopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_Intermediate;
                    searchBySection((Button) v);
                    break;
                case R.id.btnPreAlgebra_TopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_PreAlgebra;
                    searchBySection((Button) v);
                    break;
                case R.id.btnAlgebra_1_TopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_Algebra_1;
                    searchBySection((Button) v);
                    break;
                case R.id.btnAlgebra_2_TopicSelectionFragment:
                    SelectedSection = R.string.stMathSection_Algebra_2;
                    searchBySection((Button) v);
                    break;
                case R.id.btnPreviousSelection_TopicsSelectionFragment:
                    setButtonClickSelection((Button) v);
                    showTimeSelection();
                    break;
                case R.id.btnStart_TopicsSelectionFragment:
                    setButtonClickSelection((Button) v);
                    if (InfoCollector.getSelectedTopics().size() == 0){
                        showSelectionError();
                        ((Button) v).setTextColor(Color.GRAY);
                    }
                    else {
                        hideSelectionError();
                        InfoCollector.setMathTestID(MathUtility.getID());
                        startTest();
                    }
                    break;
            }
        }
    }

    private class TopicSelectionListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            String TopicName = ((TextView) v).getText().toString().trim();
            String CheckMark = getResources().getString(R.string.stCheckMark);
            Topic SelectedTopic = TopicList.get(v.getId());

            hideSelectionError();

            if (!TopicName.contains(CheckMark)){
                String StSelectedTopic = CheckMark + "\t\t" + TopicName;
                ((TextView) v).setText(StSelectedTopic);
                ((TextView) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                InfoCollector.addToSelectedTopics(SelectedTopic);
            }
            else {
                ((TextView) v).setText(TopicName.substring(TopicName.indexOf(CheckMark) + 1).trim());
                ((TextView) v).setTextColor(Color.GRAY);

                InfoCollector.removeFromSelectedTopics(SelectedTopic);
            }
        }
    }

    private class SearchingListener implements TextWatcher{

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            searchTopic();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

}
