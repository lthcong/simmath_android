package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import java.util.ArrayList;
import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckInfo.InfoCollector;
import pckMath.pckGame.pckPuzzle.*;
import pckString.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class PuzzleGameFragment extends Fragment {

    private TextView tvLevel, tvQuestion, tvAnswer;
    private Button btnCard_1, btnCard_2, btnCard_3,
            btnCard_4, btnCard_5, btnCard_6,
            btnCard_7, btnCard_8, btnCard_9,
            btnCard_10, btnCard_11, btnCard_12;
    private CountDownTimer cdtCountDownTime;
    private ArrayList<Button> ButtonCollection;
    private ArrayList<MathCard> MathCardCollection;

    private ArrayList<Button> SelectedButtons;
    private ArrayList<MathCard> SelectedCards;

    private int NoCard = InfoCollector.getNoPuzzleGameCard();

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stPuzzle));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void setButtonCollection(){
        SelectedButtons = new ArrayList<>();
        ButtonCollection = new ArrayList<>();
        ButtonCollection.add(btnCard_1);
        ButtonCollection.add(btnCard_2);
        ButtonCollection.add(btnCard_3);
        ButtonCollection.add(btnCard_4);
        ButtonCollection.add(btnCard_5);
        ButtonCollection.add(btnCard_6);
        ButtonCollection.add(btnCard_7);
        ButtonCollection.add(btnCard_8);
        ButtonCollection.add(btnCard_9);
        ButtonCollection.add(btnCard_10);
        ButtonCollection.add(btnCard_11);
        ButtonCollection.add(btnCard_12);
    }

    private void setMathCardCollection(){
        SelectedCards = new ArrayList<>();
        MathCardCollection = (new PuzzleCard()).getCardList();
    }

    private void setupButton(){
        int size = ButtonCollection.size();
        for(int i = 0; i < size; i++){
            ButtonCollection.get(i).setEnabled(true);
            ButtonCollection.get(i).setVisibility(View.VISIBLE);
            ButtonCollection.get(i).setBackgroundColor(Color.WHITE);
            ButtonCollection.get(i).setText(getResources().getString(R.string.stQuestionMark));
            ButtonCollection.get(i).setTextColor(Color.parseColor(InfoCollector.getTextColor()));
        }
    }

    private void disableSelection(){
        int size = ButtonCollection.size();
        for (int i = 0; i < size; i++){
            ButtonCollection.get(i).setEnabled(false);
        }
    }

    private void enableSelection(){
        int size = ButtonCollection.size();
        for (int i = 0; i < size; i++){
            ButtonCollection.get(i).setEnabled(true);
        }
    }

    private void showProblem(){
        if (SelectedCards.size() == 1){
            tvQuestion.setVisibility(View.VISIBLE);
            String temp;
            if (SelectedCards.get(0).getCardType().equalsIgnoreCase(PuzzleCard.getProblemCard())){
                temp = "<b>Question:</b> " + SelectedCards.get(0).getFrontSide();
            }
            else {
                temp = "<b>Answer:</b> " + SelectedCards.get(0).getFrontSide();
            }
            StringUtility.writeString(tvQuestion, temp);
        }
        else {
            tvAnswer.setVisibility(View.VISIBLE);
            String temp;
            if (SelectedCards.get(1).getCardType().equalsIgnoreCase(PuzzleCard.getProblemCard())){
                temp = "<b>Question:</b> " + SelectedCards.get(1).getFrontSide();
            }
            else {
                temp = "<b>Answer:</b> " + SelectedCards.get(1).getFrontSide();
            }
            StringUtility.writeString(tvAnswer, temp);
        }
    }

    private void hideProblem(){
        tvQuestion.setVisibility(View.GONE);
        tvAnswer.setVisibility(View.GONE);
    }

    private void checkSelection(){
        if (SelectedButtons.size() == 2) {
            //  DISABLE SELECTION
            disableSelection();

            cdtCountDownTime = new CountDownTimer(1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    //  CHECK ANSWER
                    if (!SelectedCards.get(0).getCardType().equalsIgnoreCase(SelectedCards.get(1).getCardType()) &&
                            (SelectedCards.get(0).getBackSide().equalsIgnoreCase(SelectedCards.get(1).getBackSide()) ||
                                    SelectedCards.get(0).getQuestionSide().equalsIgnoreCase(SelectedCards.get(1).getQuestionSide()))) {

                        NoCard = NoCard - 2;

                        //  HIDE SELECTION
                        SelectedButtons.get(0).setVisibility(View.GONE);
                        SelectedButtons.get(1).setVisibility(View.GONE);
                    } else {

                        //  DESELECTION
                        SelectedButtons.get(0).setBackgroundColor(Color.WHITE);
                        SelectedButtons.get(0).setText(getResources().getString(R.string.stQuestionMark));
                        SelectedButtons.get(0).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

                        SelectedButtons.get(1).setBackgroundColor(Color.WHITE);
                        SelectedButtons.get(1).setText(getResources().getString(R.string.stQuestionMark));
                        SelectedButtons.get(1).setTextColor(Color.parseColor(InfoCollector.getTextColor()));
                    }

                    //  REMOVE SELECTED BUTTON & CARDS
                    SelectedButtons.remove(0);
                    SelectedButtons.remove(0);

                    SelectedCards.remove(0);
                    SelectedCards.remove(0);

                    //  HIDE PROBLEM
                    hideProblem();

                    //  ENABLE SELECTION
                    enableSelection();

                    //  SHOW GAME END
                    if (NoCard == 0) {
                        int NextLevel = InfoCollector.getPuzzleGameCurrentLevel() + 1;
                        InfoCollector.setPuzzleGameCurrentLevel(NextLevel);
                        showPuzzleGameEnd();
                    }
                }
            }.start();
        }
    }

    private void showPuzzleGameEnd(){
        InfoCollector.setGameLevelCompleteStatus(true);
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new GameEndFragment())
                .commit();
    }

    private void showLevel(){
        String StLevel = "Level " + String.valueOf(InfoCollector.getPuzzleGameCurrentLevel()) + ":";
        tvLevel.setText(StLevel);
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        setButtonCollection();
        setMathCardCollection();

        setupButton();

        hideProblem();

        showLevel();
    }

    public PuzzleGameFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_puzzle_game, container, false);

        //  GET REFERENCES
        tvLevel = rootView.findViewById(R.id.tvLevel_PuzzleGame);
        tvQuestion = rootView.findViewById(R.id.tvQuestionContent_PuzzleGameFragment);
        tvAnswer = rootView.findViewById(R.id.tvAnswer_PuzzleGameFragment);
        btnCard_1 = rootView.findViewById(R.id.btnCard_1_PuzzleGameFragment);
        btnCard_2 = rootView.findViewById(R.id.btnCard_2_PuzzleGameFragment);
        btnCard_3 = rootView.findViewById(R.id.btnCard_3_PuzzleGameFragment);
        btnCard_4 = rootView.findViewById(R.id.btnCard_4_PuzzleGameFragment);
        btnCard_5 = rootView.findViewById(R.id.btnCard_5_PuzzleGameFragment);
        btnCard_6 = rootView.findViewById(R.id.btnCard_6_PuzzleGameFragment);
        btnCard_7 = rootView.findViewById(R.id.btnCard_7_PuzzleGameFragment);
        btnCard_8 = rootView.findViewById(R.id.btnCard_8_PuzzleGameFragment);
        btnCard_9 = rootView.findViewById(R.id.btnCard_9_PuzzleGameFragment);
        btnCard_10 = rootView.findViewById(R.id.btnCard_10_PuzzleGameFragment);
        btnCard_11 = rootView.findViewById(R.id.btnCard_11_PuzzleGameFragment);
        btnCard_12 = rootView.findViewById(R.id.btnCard_12_PuzzleGameFragment);

        //  SET LISTENER
        ButtonClickListener button_listener = new ButtonClickListener();
        btnCard_1.setOnClickListener(button_listener);
        btnCard_2.setOnClickListener(button_listener);
        btnCard_3.setOnClickListener(button_listener);
        btnCard_4.setOnClickListener(button_listener);
        btnCard_5.setOnClickListener(button_listener);
        btnCard_6.setOnClickListener(button_listener);
        btnCard_7.setOnClickListener(button_listener);
        btnCard_8.setOnClickListener(button_listener);
        btnCard_9.setOnClickListener(button_listener);
        btnCard_10.setOnClickListener(button_listener);
        btnCard_11.setOnClickListener(button_listener);
        btnCard_12.setOnClickListener(button_listener);

        //  START UP
        startUp();

        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.WHITE);
            ((Button) v).setBackgroundColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET SELECTED BUTTON
            SelectedButtons.add((Button) v);

            //  SET SELECTED CARD
            switch (v.getId()) {
                case R.id.btnCard_1_PuzzleGameFragment:
                    SelectedCards.add(MathCardCollection.get(0));
                    break;
                case R.id.btnCard_2_PuzzleGameFragment:
                    SelectedCards.add(MathCardCollection.get(1));
                    break;
                case R.id.btnCard_3_PuzzleGameFragment:
                    SelectedCards.add(MathCardCollection.get(2));
                    break;
                case R.id.btnCard_4_PuzzleGameFragment:
                    SelectedCards.add(MathCardCollection.get(3));
                    break;
                case R.id.btnCard_5_PuzzleGameFragment:
                    SelectedCards.add(MathCardCollection.get(4));
                    break;
                case R.id.btnCard_6_PuzzleGameFragment:
                    SelectedCards.add(MathCardCollection.get(5));
                    break;
                case R.id.btnCard_7_PuzzleGameFragment:
                    SelectedCards.add(MathCardCollection.get(6));
                    break;
                case R.id.btnCard_8_PuzzleGameFragment:
                    SelectedCards.add(MathCardCollection.get(7));
                    break;
                case R.id.btnCard_9_PuzzleGameFragment:
                    SelectedCards.add(MathCardCollection.get(8));
                    break;
                case R.id.btnCard_10_PuzzleGameFragment:
                    SelectedCards.add(MathCardCollection.get(9));
                    break;
                case R.id.btnCard_11_PuzzleGameFragment:
                    SelectedCards.add(MathCardCollection.get(10));
                    break;
                case R.id.btnCard_12_PuzzleGameFragment:
                    SelectedCards.add(MathCardCollection.get(11));
                    break;
            }

            //  SHOW PROBLEM
            showProblem();

            //  CHECK SELECTION
            checkSelection();
        }
    }

}
