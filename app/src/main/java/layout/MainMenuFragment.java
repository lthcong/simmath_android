package layout;


import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.*;
import pckData.pckLocalData.DatabaseFile;
import pckInfo.InfoCollector;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainMenuFragment extends Fragment {

    private Button btnHome, btnHomeText,
            btnMathForKid, btnMathForKidText,
            btnMathRound, btnMathRoundText,
            btnMathConnection, btnMathConnectionText,
            btnMathStage, btnMathStageText,
            btnLearning, btnLearningText,
            btnWorkbook, btnWorkbookText,
            btnMathLadder, btnMathLadderText,
            btnMathPractice, btnMathPracticeText,
            btnMathExercise, btnMathExerciseText,
            btnMathPocket, btnMathPocketText,
            btnMathGame, btnMathGameText,
            btnMathTipsAndTricks, btnMathTipsAndTricksText,
            btnMathFormula, btnMathFormulaText,
            btnActivityLog, btnActivityLogText,
            btnAppSetting, btnAppSettingText;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stMenuText));

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment());
        ft.commit();
    }

    private void setSelection(Button btnSelectedButton){
        btnSelectedButton.setTextColor(Color.parseColor(InfoCollector.getTextColor()));
    }

    private void clearMenuSelection(){
        btnHome.setTextColor(Color.GRAY);
        btnMathForKid.setTextColor(Color.GRAY);
        btnMathRound.setTextColor(Color.GRAY);
        btnMathConnection.setTextColor(Color.GRAY);
        InfoCollector.setMathStageSelection(false);
        btnMathStage.setTextColor(Color.GRAY);
        btnWorkbook.setTextColor(Color.GRAY);
        btnMathLadder.setTextColor(Color.GRAY);
        btnLearning.setTextColor(Color.GRAY);
        btnMathPractice.setTextColor(Color.GRAY);
        btnMathExercise.setTextColor(Color.GRAY);
        btnMathGame.setTextColor(Color.GRAY);
        btnMathPocket.setTextColor(Color.GRAY);
        btnMathTipsAndTricks.setTextColor(Color.GRAY);
        btnMathFormula.setTextColor(Color.GRAY);
        btnActivityLog.setTextColor(Color.GRAY);
        btnAppSetting.setTextColor(Color.GRAY);

        btnHomeText.setTextColor(Color.GRAY);
        btnMathForKidText.setTextColor(Color.GRAY);
        btnMathRoundText.setTextColor(Color.GRAY);
        btnMathConnectionText.setTextColor(Color.GRAY);
        btnMathStageText.setTextColor(Color.GRAY);
        btnWorkbookText.setTextColor(Color.GRAY);
        btnMathLadderText.setTextColor(Color.GRAY);
        btnLearningText.setTextColor(Color.GRAY);
        btnMathPracticeText.setTextColor(Color.GRAY);
        btnMathExerciseText.setTextColor(Color.GRAY);
        btnMathGameText.setTextColor(Color.GRAY);
        btnMathPocketText.setTextColor(Color.GRAY);
        btnMathTipsAndTricksText.setTextColor(Color.GRAY);
        btnMathFormulaText.setTextColor(Color.GRAY);
        btnActivityLogText.setTextColor(Color.GRAY);
        btnAppSettingText.setTextColor(Color.GRAY);
    }

    private void showComeback(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new ComeBackFragment());
        ft.commit();
    }

    private void showInitialSetup(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new InitialSetupFragment());
        ft.commit();
    }

    private void showHomeSelection(){
        setSelection(btnHome);
        setSelection(btnHomeText);
    }
    
    private void goHome(){
        removeSubMenu();

        Cursor CurrentUser = DatabaseFile.getUserLevel();
        CurrentUser.moveToFirst();
        int CurrentLevel = Integer.parseInt(CurrentUser.getString(0));

        if (CurrentLevel > 0){
            showComeback();
        }
        else {
            showInitialSetup();
        }
    }

    private void showMathForKidSelection(){
        setSelection(btnMathForKid);
        setSelection(btnMathForKidText);
    }
    
    private void showMathForKid(){
        removeSubMenu();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathForKidInfoFragment());
        ft.commit();
    }

    private void showRoundSelection(){
        setSelection(btnMathRound);
        setSelection(btnMathRoundText);
    }

    private void showMathRound(){
        removeSubMenu();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathRoundInfoFragment());
        ft.commit();
    }

    private void showLearningSelection(){
        setSelection(btnLearning);
        setSelection(btnLearningText);
    }

    private void addLearningSubMenu(){
        InfoCollector.setRegularTestSelected();
        InfoCollector.setupSelectedTopics();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new LearningSubMenuFragment());
        ft.commit();
    }

    private void removeSubMenu(){
        InfoCollector.setCurrentSubmenu(null);

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new BlankFragment());
        ft.commit();
    }

    private void showGameSelection(){
        setSelection(btnMathGame);
        setSelection(btnMathGameText);
    }

    private void addGameSubMenu(){
        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new MathGameSubMenu());
        ft.commit();
    }

    private void showMathConnectionSelection(){
        setSelection(btnMathConnection);
        setSelection(btnMathConnectionText);
    }

    private void showMathConnection(){
        removeSubMenu();
        InfoCollector.setupMathStage();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathConnectInfoFragment());
        ft.commit();
    }

    private void showMathStageSelection(){
        setSelection(btnMathStage);
        setSelection(btnMathStageText);
    }
    
    private void showMathStage(){
        removeSubMenu();
        InfoCollector.setupMathStage();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathStageInfoFragment());
        ft.commit();
    }

    private void showMathWorkbookSelection(){
        setSelection(btnWorkbook);
        setSelection(btnWorkbookText);
    }
    
    private void showWorkbook(){
        removeSubMenu();
        InfoCollector.setupSelectedTopics();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new WorkbookInfoFragment());
        ft.commit();
    }

    private void showMathLadderSelection(){
        setSelection(btnMathLadder);
        setSelection(btnMathLadderText);
    }

    private void showMathLadder(){
        removeSubMenu();
        InfoCollector.setupMathLadder();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathLadderInfoFragment());
        ft.commit();
    }

    private void showMathPracticeSelection(){
        setSelection(btnMathPractice);
        setSelection(btnMathPracticeText);
    }
    
    private void showMathPractice(){
        removeSubMenu();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathPracticeInfoFragment());
        ft.commit();
    }

    private void showMathExerciseSelection(){
        setSelection(btnMathExercise);
        setSelection(btnMathExerciseText);
    }

    private void showMathExercise(){
        removeSubMenu();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathExerciseInfoFragment());
        ft.commit();
    }

    private void showMathPocketSelection(){
        setSelection(btnMathPocket);
        setSelection(btnMathPocketText);
    }

    private void showMathPocket(){
        removeSubMenu();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathPocketInfoFragment());
        ft.commit();
    }

    private void showMathTipcsAndTricksSelection(){
        setSelection(btnMathTipsAndTricks);
        setSelection(btnMathTipsAndTricksText);
    }

    private void showMathTipsAndTricks(){
        removeSubMenu();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathTipsAndTricksFragment());
        ft.commit();
    }

    private void showMathFormulaSelection(){
        setSelection(btnMathFormula);
        setSelection(btnMathFormulaText);
    }
    
    private void showMathFormula(){
        removeSubMenu();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathFormulaFragment());
        ft.commit();
    }

    private void showActivityLogSelection(){
        setSelection(btnActivityLog);
        setSelection(btnActivityLogText);
    }

    private void showActivityLog(){
        InfoCollector.setWorkbookHistorySelected();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlySubMenuContainer_MyMathAppActivity, new HistorySubMenuFragment());
        ft.commit();
    }

    private void showAppSettingSelection(){
        setSelection(btnAppSetting);
        setSelection(btnAppSettingText);
    }

    private void showAppSetting(){
        removeSubMenu();

        FragmentManager fm = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(InfoCollector.getEnterAnimation(), 0);
        ft.replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new AppSettingFragment());
        ft.commit();
    }

    private void startUp(){
        InfoCollector.setMenuOpenStatus(true);
        clearMenuSelection();
        setupAppTile();
    }

    public MainMenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_main_menu, container, false);

        //  GET REFERENCES
        btnHome = rootView.findViewById(R.id.btnHome_MainMenuFragment);
        btnMathForKid = rootView.findViewById(R.id.btnMathForKid_MainMenuFragment);
        btnMathRound = rootView.findViewById(R.id.btnMathRound_MainMenuFragment);
        btnMathConnection = rootView.findViewById(R.id.btnMathConnection_MainMenuFragment);
        btnMathStage = rootView.findViewById(R.id.btnMathStage_MainMenuFragment);
        btnWorkbook = rootView.findViewById(R.id.btnWorkbook_MainMenuFragment);
        btnMathLadder = rootView.findViewById(R.id.btnMathLadder_MainMenuFragment);
        btnLearning = rootView.findViewById(R.id.btnLearning_MainMenuFragment);
        btnMathPractice = rootView.findViewById(R.id.btnMathPractice_MainMenuFragment);
        btnMathExercise = rootView.findViewById(R.id.btnMathExercise_MainMenuFragment);
        btnMathGame = rootView.findViewById(R.id.btnMathGame_MainMenuFragment);
        btnMathTipsAndTricks = rootView.findViewById(R.id.btnMathTipsAndTricks_MainMenuFragment);
        btnMathFormula = rootView.findViewById(R.id.btnMathFormula_MainMenuFragment);
        btnMathPocket = rootView.findViewById(R.id.btnMathPocket_MainMenuFragment);
        btnActivityLog = rootView.findViewById(R.id.btnActivityLog_MainMenuFragment);
        btnAppSetting = rootView.findViewById(R.id.btnAppSetting_MainMenuFragment);

        btnHomeText = rootView.findViewById(R.id.btnHomeText_MainMenuFragment);
        btnMathForKidText = rootView.findViewById(R.id.btnMathForKidText_MainMenuFragment);
        btnMathRoundText = rootView.findViewById(R.id.btnMathRoundText_MainMenuFragment);
        btnMathConnectionText = rootView.findViewById(R.id.btnMathConnectionText_MainMenuFragment);
        btnMathStageText = rootView.findViewById(R.id.btnMathStageText_MainMenuFragment);
        btnWorkbookText = rootView.findViewById(R.id.btnWorkbookText_MainMenuFragment);
        btnMathLadderText = rootView.findViewById(R.id.btnMathLadderText_MainMenuFragment);
        btnLearningText = rootView.findViewById(R.id.btnLearningText_MainMenuFragment);
        btnMathPracticeText = rootView.findViewById(R.id.btnMathPracticeText_MainMenuFragment);
        btnMathExerciseText = rootView.findViewById(R.id.btnMathExerciseText_MainMenuFragment);
        btnMathGameText = rootView.findViewById(R.id.btnMathGameText_MainMenuFragment);
        btnMathTipsAndTricksText = rootView.findViewById(R.id.btnMathTipsAndTricksText_MainMenuFragment);
        btnMathFormulaText = rootView.findViewById(R.id.btnMathFormulaText_MainMenuFragment);
        btnMathPocketText = rootView.findViewById(R.id.btnMathPocketText_MainMenuFragment);
        btnActivityLogText = rootView.findViewById(R.id.btnActivityLogText_MainMenuFragment);
        btnAppSettingText = rootView.findViewById(R.id.btnAppSettingText_MainMenuFragment);

        //  SET LISTENER
        MenuSelectionListener MenuListener = new MenuSelectionListener();
        btnHome.setOnClickListener(MenuListener);
        btnMathForKid.setOnClickListener(MenuListener);
        btnMathRound.setOnClickListener(MenuListener);
        btnMathConnection.setOnClickListener(MenuListener);
        btnMathStage.setOnClickListener(MenuListener);
        btnWorkbook.setOnClickListener(MenuListener);
        btnMathLadder.setOnClickListener(MenuListener);
        btnLearning.setOnClickListener(MenuListener);
        btnMathPractice.setOnClickListener(MenuListener);
        btnMathExercise.setOnClickListener(MenuListener);
        btnMathPocket.setOnClickListener(MenuListener);
        btnMathGame.setOnClickListener(MenuListener);
        btnMathFormula.setOnClickListener(MenuListener);
        btnMathTipsAndTricks.setOnClickListener(MenuListener);
        btnActivityLog.setOnClickListener(MenuListener);
        btnAppSetting.setOnClickListener(MenuListener);

        btnHomeText.setOnClickListener(MenuListener);
        btnMathForKidText.setOnClickListener(MenuListener);
        btnMathRoundText.setOnClickListener(MenuListener);
        btnMathConnectionText.setOnClickListener(MenuListener);
        btnMathStageText.setOnClickListener(MenuListener);
        btnWorkbookText.setOnClickListener(MenuListener);
        btnMathLadderText.setOnClickListener(MenuListener);
        btnLearningText.setOnClickListener(MenuListener);
        btnMathPracticeText.setOnClickListener(MenuListener);
        btnMathExerciseText.setOnClickListener(MenuListener);
        btnMathPocketText.setOnClickListener(MenuListener);
        btnMathGameText.setOnClickListener(MenuListener);
        btnMathFormulaText.setOnClickListener(MenuListener);
        btnMathTipsAndTricksText.setOnClickListener(MenuListener);
        btnActivityLogText.setOnClickListener(MenuListener);
        btnAppSettingText.setOnClickListener(MenuListener);

        //  START UP
        startUp();

        return rootView;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        InfoCollector.setMenuOpenStatus(false);
    }

    private class MenuSelectionListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            //  CLEAR SELECTION
            clearMenuSelection();

            //  REDIRECTING
            switch (v.getId()){
                case R.id.btnHome_MainMenuFragment:
                    showHomeSelection();
                    goHome();
                    break;
                case R.id.btnMathForKid_MainMenuFragment:
                    showMathForKidSelection();
                    showMathForKid();
                    break;
                case R.id.btnMathRound_MainMenuFragment:
                    showRoundSelection();
                    showMathRound();
                    break;
                case R.id.btnMathConnection_MainMenuFragment:
                    showMathConnectionSelection();
                    showMathConnection();
                    break;
                case R.id.btnMathStage_MainMenuFragment:
                    showMathStageSelection();
                    showMathStage();
                    break;
                case R.id.btnWorkbook_MainMenuFragment:
                    showMathWorkbookSelection();
                    showWorkbook();
                    break;
                case R.id.btnMathLadder_MainMenuFragment:
                    showMathLadderSelection();
                    showMathLadder();
                    break;
                case R.id.btnLearning_MainMenuFragment:
                    showLearningSelection();
                    addLearningSubMenu();
                    break;
                case R.id.btnMathPractice_MainMenuFragment:
                    showMathPracticeSelection();
                    showMathPractice();
                    break;
                case R.id.btnMathExercise_MainMenuFragment:
                    showMathExerciseSelection();
                    showMathExercise();
                    break;
                case R.id.btnMathPocket_MainMenuFragment:
                    showMathPocketSelection();
                    showMathPocket();
                    break;
                case R.id.btnMathGame_MainMenuFragment:
                    showGameSelection();
                    addGameSubMenu();
                    break;
                case R.id.btnMathTipsAndTricks_MainMenuFragment:
                    showMathTipcsAndTricksSelection();
                    showMathTipsAndTricks();
                    break;
                case R.id.btnMathFormula_MainMenuFragment:
                    showMathFormulaSelection();
                    showMathFormula();
                    break;
                case R.id.btnActivityLog_MainMenuFragment:
                    showActivityLogSelection();
                    showActivityLog();
                    break;
                case R.id.btnAppSetting_MainMenuFragment:
                    showAppSettingSelection();
                    showAppSetting();
                    break;
                case R.id.btnHomeText_MainMenuFragment:
                    showHomeSelection();
                    goHome();
                    break;
                case R.id.btnMathForKidText_MainMenuFragment:
                    showMathForKidSelection();
                    showMathForKid();
                    break;
                case R.id.btnMathRoundText_MainMenuFragment:
                    showRoundSelection();
                    showMathRound();
                    break;
                case R.id.btnMathConnectionText_MainMenuFragment:
                    showMathConnectionSelection();
                    showMathConnection();
                    break;
                case R.id.btnMathStageText_MainMenuFragment:
                    showMathStageSelection();
                    showMathStage();
                    break;
                case R.id.btnWorkbookText_MainMenuFragment:
                    showMathWorkbookSelection();
                    showWorkbook();
                    break;
                case R.id.btnMathLadderText_MainMenuFragment:
                    showMathLadderSelection();
                    showMathLadder();
                    break;
                case R.id.btnLearningText_MainMenuFragment:
                    showLearningSelection();
                    addLearningSubMenu();
                    break;
                case R.id.btnMathPracticeText_MainMenuFragment:
                    showMathPracticeSelection();
                    showMathPractice();
                    break;
                case R.id.btnMathExerciseText_MainMenuFragment:
                    showMathExerciseSelection();
                    showMathExercise();
                    break;
                case R.id.btnMathPocketText_MainMenuFragment:
                    showMathPocketSelection();
                    showMathPocket();
                    break;
                case R.id.btnMathGameText_MainMenuFragment:
                    showGameSelection();
                    addGameSubMenu();
                    break;
                case R.id.btnMathTipsAndTricksText_MainMenuFragment:
                    showMathTipcsAndTricksSelection();
                    showMathTipsAndTricks();
                    break;
                case R.id.btnMathFormulaText_MainMenuFragment:
                    showMathFormulaSelection();
                    showMathFormula();
                    break;
                case R.id.btnActivityLogText_MainMenuFragment:
                    showActivityLogSelection();
                    showActivityLog();
                    break;
                case R.id.btnAppSettingText_MainMenuFragment:
                    showAppSettingSelection();
                    showAppSetting();
                    break;
            }
        }

    }

}
