package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.MathTestStartingFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckData.pckFirebase.LogEvents;
import pckInfo.InfoCollector;

/**
 * A simple {@link Fragment} subclass.
 */
public class AssessmentTestSelection extends Fragment {

    private Button btnStartAssessmentTest;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stSAccuplacerTest));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void logAssessmentTest(){
        LogEvents.writeToLog(this.getActivity(), "MathTest", "AssessmentTest", "AssessmentTest");
    }

    private void startAssessmentTest(){
        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyMainFragmentContainer_MyMathAppActivity, new MathTestStartingFragment())
                .commit();
    }

    private void startup(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
    }

    public AssessmentTestSelection() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_assessment_test_selection, container, false);

        //  GET REFERENCES
        btnStartAssessmentTest = rootView.findViewById(R.id.btnStart_AssessmentTestSelectionFragment);
        btnStartAssessmentTest.setTextColor(Color.GRAY);

        //  SET LISTENER
        ButtonClickListener button_listener = new ButtonClickListener();
        btnStartAssessmentTest.setOnClickListener(button_listener);

        //  START UP
        startup();

        return rootView;
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  SET SELECTION
            ((Button) v).setTextColor(Color.parseColor(InfoCollector.getTextColor()));

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnStart_AssessmentTestSelectionFragment:
                    logAssessmentTest();
                    InfoCollector.clearSelectedTopics();
                    startAssessmentTest();
                    break;
            }
        }
    }

}
