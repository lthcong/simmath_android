package layout;


import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.Objects;

import adsfree.congla.android.cong.mymathapp.AppTileFragment;
import adsfree.congla.android.cong.mymathapp.R;
import pckAds.Ads;
import pckData.pckFirebase.LogEvents;
import pckInfo.InfoCollector;
import pckMath.pckMathForKid.WhoAmI;
import pckString.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class WhoAmIFragment extends Fragment {

    private TextView tvFirstStatement, tvSecondStatement, tvResult;
    private Button btnAnswerA, btnAnswerB, btnAnswerC, btnAnswerD;
    private CountDownTimer cdtCountDownTime;
    private WhoAmI Problem;
    private String YourAnswer;
    private int NoProblem = 0;

    private void setupAppTile(){
        InfoCollector.setAppTitle(getResources().getString(R.string.stWhoAmI));

        Objects.requireNonNull(getActivity())
                .getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyTitleContainer_MyMathAppActivity, new AppTileFragment())
                .commit();
    }

    private void addBackButton(){
        getChildFragmentManager()
                .beginTransaction()
                .setCustomAnimations(InfoCollector.getEnterAnimation(), 0)
                .replace(R.id.lnlyButtonBackContainer_WhoAmIFragment, new ButtonBackToMathForKidFragment())
                .commit();
    }

    private void clearSelection(){
        btnAnswerA.setTextColor(Color.GRAY);
        btnAnswerB.setTextColor(Color.GRAY);
        btnAnswerC.setTextColor(Color.GRAY);
        btnAnswerD.setTextColor(Color.GRAY);
    }

    private void getProblem(){
        NoProblem++;
        Problem = new WhoAmI();

        //  GET STATEMENT
        StringUtility.writeString(tvFirstStatement, Problem.getFirstStatement());
        StringUtility.writeString(tvSecondStatement, Problem.getSecondStatement());

        //  HIDE RESULT
        tvResult.setVisibility(View.GONE);

        //  GET SELECTION
        clearSelection();
        btnAnswerA.setText(Problem.getAnswerA());
        btnAnswerB.setText(Problem.getAnswerB());
        btnAnswerC.setText(Problem.getAnswerC());
        btnAnswerD.setText(Problem.getAnswerD());
    }

    private void getNewProblem(){
        cdtCountDownTime = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                getProblem();

                //  ADS
                showAds();
            }
        }.start();
    }

    private void checkAnswer(Button btnSelectedAnswer){
        //  SET SELECTION
        btnSelectedAnswer.setTextColor(Color.parseColor(InfoCollector.getTextColor()));

        //  GET YOUR ANSWER
        YourAnswer = btnSelectedAnswer.getText().toString().trim();

        //  SHOW RESULT
        tvResult.setVisibility(View.VISIBLE);
        if (YourAnswer.equalsIgnoreCase(Problem.getRightAnswer().trim())){
            tvResult.setText("CORRECT");
            tvResult.setTextColor(Color.GREEN);

            //  NEW PROBLEM
            getNewProblem();
        }
        else {
            tvResult.setText("NOT CORRECT");
            tvResult.setTextColor(Color.RED);
        }
    }

    private void showAds(){
        Ads.showAds(this.getActivity(), NoProblem);
    }

    private void startUp(){
        InfoCollector.setCurrentFragment(this);
        setupAppTile();
        addBackButton();
        getProblem();
    }

    public WhoAmIFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_who_am_i, container, false);

        //  GET REFERENCES
        tvFirstStatement = rootView.findViewById(R.id.tvFirstOperation_WhoAmIFragment);
        tvSecondStatement = rootView.findViewById(R.id.tvSecondOperation_WhoAmIFragment);
        tvResult = rootView.findViewById(R.id.tvResult_WhoIAmFragment);
        btnAnswerA = rootView.findViewById(R.id.btnAnswerA_WhoAnIFragment);
        btnAnswerB = rootView.findViewById(R.id.btnAnswerB_WhoAnIFragment);
        btnAnswerC = rootView.findViewById(R.id.btnAnswerC_WhoAnIFragment);
        btnAnswerD = rootView.findViewById(R.id.btnAnswerD_WhoAnIFragment);

        //  SET LISTENER
        ButtonClickListener ButtonListener = new ButtonClickListener();
        btnAnswerA.setOnClickListener(ButtonListener);
        btnAnswerB.setOnClickListener(ButtonListener);
        btnAnswerC.setOnClickListener(ButtonListener);
        btnAnswerD.setOnClickListener(ButtonListener);

        //  START UP
        startUp();

        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if (cdtCountDownTime != null){
            cdtCountDownTime.cancel();
        }
    }

    private class ButtonClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            //  CLEAR SELECTION
            clearSelection();

            //  SET FEATURE
            switch (v.getId()){
                case R.id.btnAnswerA_WhoAnIFragment:
                    checkAnswer((Button) v);
                    break;
                case R.id.btnAnswerB_WhoAnIFragment:
                    checkAnswer((Button) v);
                    break;
                case R.id.btnAnswerC_WhoAnIFragment:
                    checkAnswer((Button) v);
                    break;
                case R.id.btnAnswerD_WhoAnIFragment:
                    checkAnswer((Button) v);
                    break;
            }
        }
    }
}
